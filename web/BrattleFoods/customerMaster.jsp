<%-- 
    Document   : viewRouteMaster
    Created on : Nov 1, 2013, 5:01:47 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

            <script type="text/javascript">
        function submitPage(value){
            //alert(value);
            if(value == 'add'){
                document.routeMaster.action = '/throttle/BrattleFoods/addCustomer.jsp';
                document.routeMaster.submit();
            }
        }
</script>

    </head>
    <body onload="setImages(1,0,0,0,0,0);">
        <% String menuPath = "Customer Master";
            request.setAttribute("menuPath", menuPath);
        %>
        <script type="text/javascript">
            function submitWindow(val){
            document.routeMaster.action = '/throttle/BrattleFoods/customerMaster.jsp?reqFor='+val;
            document.routeMaster.submit();
            }
            <%
            String reqFor = "";
            if(request.getParameter("reqFor") != null && !"".equals(request.getParameter("reqFor"))){
            reqFor = request.getParameter("reqFor");
            if(reqFor != null){
            request.setAttribute("reqFor", reqFor);    
            }
            }
            %>
        </script>
        <form name="routeMaster"  method="post">
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="700" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                            </td></tr></table>
                    <!-- pointer table -->

                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:900;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">View Customer Details</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>
                                        <td height="30"><font color="red">*</font>Customer Code</td>
                                        <td><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td height="30"><font color="red">*</font>Customer Name</td>
                                        <td><input name="toDate" id="toDate" type="text" class="datepicker" onClick="ressetDate(this);"></td>

                                    </tr>
                                    <tr>
<!--                                        <td  height="25" >Route Code</td>
                                        <td  height="25"><input name="fromDate" type="text" class="textbox" value="" size="20"></td>-->
                                        <td>
                                        <input type="button"   value="Search" class="button" name="search" onClick="submitWindow('reqFor')">
                                        <input type="hidden"   value="reqFor" class="button" name="reqFor" >
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <% if(request.getAttribute("reqFor") != null && !"".equals(request.getAttribute("reqFor")) ){%>
            <table width="100%" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="40">
                        <th><h3>S.No</h3></th>
                        <th><h3>Customer Code</h3></th>
                        <th><h3>Customer Name</h3></th>
                        <th><h3>Billing Type</h3></th>
                        <th><h3>Location</h3></th>
                        <th><h3>Enrolled Date</h3></th>
                        <th><h3>Status</h3></th>
                        <th><h3>Contract</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <tr height="30">
                        <td align="left" class="text2">1</td>
                        <td align="left" class="text2">CC001</td>
                        <td align="left" class="text2">Dushi Foods</td>
                        <td align="left" class="text2">Actual KMs</td>
                        <td align="left" class="text2">Balwal</td>
                        <td align="left" class="text2">11/11/2007</td>
                        <td align="left" class="text2">Active</td>
                        <td align="left" class="text2"><a href="customerContractMaster1.jsp">create</a></td>
                    </tr>
                    <tr height="30">
                        <td align="left" class="text1">2</td>
                        <td align="left" class="text1">CC022</td>
                        <td align="left" class="text1">Kohinoor</td>
                        <td align="left" class="text1">Point to Point</td>
                        <td align="left" class="text1">Sonipat</td>
                        <td align="left" class="text1">11/02/2005</td>
                        <td align="left" class="text1">Active</td>
                        <td align="left" class="text2"><a href="customerContractMaster2.jsp">view</a></td>
                    </tr>
                    <tr height="30">
                        <td align="left" class="text2">3</td>
                        <td align="left" class="text2">CC042</td>
                        <td align="left" class="text2">Bakers Circle</td>
                        <td align="left" class="text2">Point to Point based on weight</td>
                        <td align="left" class="text2">Kashipur</td>
                        <td align="left" class="text2">07/02/2002</td>
                        <td align="left" class="text2">Active</td>
                        <td align="left" class="text2"><a href="customerContractMaster3.jsp">view</a></td>
                    </tr>
                    
                </tbody>
            </table>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
            <%}%>
            <br>
        </form>
    </body>
</html>
