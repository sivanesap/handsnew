<%-- 
    Document   : BalanceSheet
    Created on : Nov 7, 2013, 2:52:50 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <title>JSP Page</title>
    </head>
     <%
        String menuPath = "Finance >> Profit & Loss";
        request.setAttribute("menuPath", menuPath);
    %>
    <body>
        <form name="balanceSheet" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <table width="95%"  align="center" cellpadding="0" cellspacing="0" class="border">
                <thead>
                    <tr>
                <h3 style="text-align: center; font: normal; font-size: 30px; color: skyblue">Profit Loss Account (Rs Crore)</h3>
                    </tr>
                </thead>
                <br>
                <br>
                <tbody>
                <tr class="contenthead">
                    <td colspan="2">&nbsp;</td>
                    <td>Mar-13</td>
                    <td>Mar-12</td>
                    <td>Mar-11</td>
                    <td>Mar-10</td>
                    <td>Mar-09</td>
                </tr>
                <tr>
                    <td colspan="7">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="7" style="background-color: springgreen">Income</td>
                </tr>
                <tr>
                    <td colspan="2">Operating Income</td>
                    <td>6,558.13</td>
                    <td>5,883.06</td>
                    <td>5,230.15</td>
                    <td>4,402.83</td>
                    <td>3,692.43</td>
                </tr>
                <tr>
                    <td colspan="7">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="7" style="background-color: springgreen">Expenses</td>
                </tr>
                <tr>
                    <td colspan="2">Material consumed</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>6.87</td>
                </tr>
                <tr>
                    <td colspan="2">Manufacturing expenses</td>
                    <td>-</td>
                    <td>27.46</td>
                    <td>14.97</td>
                    <td>12.47</td>
                    <td>10.37</td>
                </tr>
                <tr>
                    <td colspan="2">Personnel expenses</td>
                    <td>384.76</td>
                    <td>369.48</td>
                    <td>358.21</td>
                    <td>225.08</td>
                    <td>200.54</td>
                </tr>
                <tr>
                    <td colspan="2">Selling expenses</td>
                    <td>-</td>
                    <td>10.9</td>
                    <td>97.1</td>
                    <td>59.09</td>
                    <td>88.41</td>
                </tr>
                <tr>
                    <td colspan="2">Adminstrative expenses</td>
                    <td>1,273.97</td>
                    <td>1,092.33</td>
                    <td>816.1</td>
                    <td>611.47</td>
                    <td>484.51</td>
                </tr>
                <tr>
                    <td colspan="2">Expenses capitalised</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td colspan="2">Cost of sales</td>
                    <td>1,658.73</td>
                    <td>1,500.16</td>
                    <td>1,286.38</td>
                    <td>908.11</td>
                    <td>790.69</td>
                </tr>
                <tr>
                    <td colspan="2">Operating profit</td>
                    <td>4,899.40</td>
                    <td>4,382.90</td>
                    <td>3,943.76</td>
                    <td>3,494.72</td>
                    <td>2,901.74</td>
                </tr>
                <tr>
                    <td colspan="2">Other recurring income</td>
                    <td>5.46</td>
                    <td>4.71</td>
                    <td>135.35</td>
                    <td>78.06</td>
                    <td>33.55</td>
                </tr>
                <tr>
                    <td colspan="2">Adjusted PBDIT</td>
                    <td>4,904.86</td>
                    <td>4,387.60</td>
                    <td>4,079.12</td>
                    <td>3,572.78</td>
                    <td>2,935.29</td>
                </tr>
                <tr>
                    <td colspan="2">Financial expenses</td>
                    <td>2,870.34</td>
                    <td>2,474.90</td>
                    <td>2,271.96</td>
                    <td>2,246.79</td>
                    <td>1,977.67</td>
                </tr>
                <tr>
                    <td colspan="2">Depreciation</td>
                    <td>18.33</td>
                    <td>13.46</td>
                    <td>10.82</td>
                    <td>14.96</td>
                    <td>34.81</td>
                </tr>
                <tr>
                    <td colspan="2">Other write offs</td>
                    <td>-</td>
                    <td>16.12</td>
                    <td>11.99</td>
                    <td>4.99</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td colspan="2">Adjusted PBT</td>
                    <td>2,016.19</td>
                    <td>1,883.12</td>
                    <td>1,784.35</td>
                    <td>1,306.04</td>
                    <td>922.82</td>
                </tr>
                <tr>
                    <td colspan="2">Tax charges</td>
                    <td>655.57</td>
                    <td>623.46</td>
                    <td>619.05</td>
                    <td>451.47</td>
                    <td>308.23</td>
                </tr>
                <tr>
                    <td colspan="2">Adjusted PAT</td>
                    <td>1,360.62</td>
                    <td>1,259.66</td>
                    <td>1,165.30</td>
                    <td>854.57</td>
                    <td>614.59</td>
                </tr>
                <tr>
                    <td colspan="2">Non recurring items</td>
                    <td>-</td>
                    <td>-2.21</td>
                    <td>64.58</td>
                    <td>18.55</td>
                    <td>-2.18</td>
                </tr>
                <tr>
                    <td colspan="2">Other non cash adjustments</td>
                    <td>-</td>
                    <td>0.35</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td colspan="2">Reported net profit</td>
                    <td>1,360.62</td>
                    <td>1,257.80</td>
                    <td>1,229.88</td>
                    <td>873.12</td>
                    <td>612.4</td>
                </tr>
                <tr>
                    <td colspan="2">Earnigs before appropriation</td>
                    <td>3,272.20</td>
                    <td>2,663.64</td>
                    <td>2,159.90</td>
                    <td>1,456.21</td>
                    <td>887.26</td>
                </tr>
                <tr>
                    <td colspan="2">Equity dividend</td>
                    <td>159</td>
                    <td>147.09</td>
                    <td>146.85</td>
                    <td>136.01</td>
                    <td>101.86</td>
                </tr>
                <tr>
                    <td colspan="2">Preference dividend</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td colspan="2">Dividend tax</td>
                    <td>26.5</td>
                    <td>23.86</td>
                    <td>24.39</td>
                    <td>22.77</td>
                    <td>17.31</td>
                </tr>
                <tr>
                    <td colspan="2">Retained earnings</td>
                    <td>3,086.70</td>
                    <td>2,492.70</td>
                    <td>1,988.66</td>
                    <td>1,297.44</td>
                    <td>768.09</td>
                </tr>
                </tbody>
        </form>
    </body>
</html>
