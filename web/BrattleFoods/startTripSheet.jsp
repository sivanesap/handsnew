<%@page import="java.text.SimpleDateFormat" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
<!--                <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>

       <script type="text/javascript" language="javascript">
           
            function submitPage(){
                if(isEmpty(document.getElementById("startDate").value)){
                    alert('please enter plan start date');
                    document.getElementById("startDate").focus();
                }
                if(isEmpty(document.getElementById("startOdometerReading").value)){
                    alert('please enter start odometer reading');
                    document.getElementById("startOdometerReading").focus();
                }
                if(isEmpty(document.getElementById("startHM").value)){
                    alert('please enter start HM');
                    document.getElementById("startHM").focus();
                }
                else{
                    document.trip.action = '/throttle/updateStartTripSheet.do';
                    document.trip.submit();
                }
            }
        </script>





    </head>


    <body onload="addRow1();">

        <form name="trip" method="post">
             <%
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String startDate = sdf.format(today);
        %>
            <%@ include file="/content/common/path.jsp" %>
            <br>

          <table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:300;">

                            <div id="first">
                                <c:if test = "${tripDetails != null}" >
                                    <c:forEach items="${tripDetails}" var="trip">
                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                        <td> <c:out value="${trip.orderRevenue}" /></td>

                                     </tr>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Projected Expense:</b></font></td>
                                        <td> <c:out value="${trip.orderExpense}" /></td>
                                        <input type="hidden" name="estimatedAdvancePerDay" value='<c:out value="${trip.estimatedAdvancePerDay}" />'>
                                        <input type="hidden" name="estimatedTransitDays" value='<c:out value="${trip.estimatedTransitDays}" />'>
                                        <input type="hidden" name="estimatedTransitHours" value='<c:out value="${trip.estimatedTransitHours}" />'>

                                     </tr>
                                    <c:set var="profitMargin" value="" />
                                     <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                     <c:set var="orderExpense" value="${trip.orderExpense}" />
                                     <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                     <%
                                     String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                     String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
                                     float profitPercentage = 0.00F;
                                     if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                         profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                     }


                                     %>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                        <td>  <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)
                                            <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                                        <td>

                                        <td>
                                     </tr>
                                </table>
                                </c:forEach>
                             </c:if>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>


            <br>
            <br>
            <br>
            <br>
            <table width="100%">
                <% int loopCntr = 0;%>
                <c:if test = "${tripDetails != null}" >
                    <c:forEach items="${tripDetails}" var="trip">
                        <% if(loopCntr == 0) {%>
                        <tr>
                            <td class="contenthead" >Trip Code: <c:out value="${trip.tripCode}"/></td>
                            <td class="contenthead" >Customer Name:&nbsp;<c:out value="${trip.customerName}"/></td>
                            <td class="contenthead" >Route: &nbsp;<c:out value="${trip.routeInfo}"/></td>
                            <td class="contenthead" >Status: <c:out value="${trip.status}"/></td>
                        </tr>
                        <% }%>
                        <% loopCntr++;%>
                    </c:forEach>
                </c:if>
            </table>
            <div id="tabs" >
                <ul>
                    <li><a href="#tripStart"><span>Trip Start </span></a></li>
                    <li><a href="#tripDetail"><span>Trip Details</span></a></li>
                    <li><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                    <li><a href="#preStart"><span>Trip Pre Start Details </span></a></li>
                    <li><a href="#statusDetail"><span>Status History</span></a></li>
                    <!--
                    <li><a href="#cleanerDetail"><span>Cleaner</span></a></li>-->
                    <!--                    <li><a href="#advDetail"><span>Advance</span></a></li>-->
                    <!--<li><a href="#expDetail"><span>Expense Details</span></a></li>-->
                    <!--                    <li><a href="#summary"><span>Remarks</span></a></li>-->
                </ul>

               <div id="tripDetail">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="6" >Trip Details</td>
                        </tr>

                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">


                        <tr>
<!--                            <td class="text1"><font color="red">*</font>Trip Sheet Date</td>
                            <td class="text1"><input type="text" name="tripDate" class="datepicker" value=""></td>-->
                            <td class="text1">Consignment No(s)</td>
                            <td class="text1">
                                <c:out value="${trip.cNotes}" />
                            </td>
                            <td class="text1">Billing Type</td>
                            <td class="text1">
                                <c:out value="${trip.billingType}" />
                            </td>
                        </tr>
                        <tr>
<!--                            <td class="text2">Customer Code</td>
                            <td class="text2">BF00001</td>-->
                            <td class="text2">Customer Name</td>
                            <td class="text2">
                                <c:out value="${trip.customerName}" />
                                <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${customerName}" />'>
                            </td>
                            <td class="text2">Customer Type</td>
                            <td class="text2" colspan="3" >
                                <c:out value="${trip.customerType}" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text1">Route Name</td>
                            <td class="text1">
                                <c:out value="${trip.routeInfo}" />
                            </td>
<!--                            <td class="text1">Route Code</td>
                            <td class="text1" >DL001</td>-->
                            <td class="text1">Reefer Required</td>
                            <td class="text1" >
                                <c:out value="${trip.reeferRequired}" />
                            </td>
                            <td class="text1">Order Est Weight (MT)</td>
                            <td class="text1" >
                                <c:out value="${trip.totalWeight}" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text2">Vehicle Type</td>
                            <td class="text2">
                                <c:out value="${trip.vehicleTypeName}" />
                            </td>
                            <td class="text2"><font color="red">*</font>Vehicle No</td>
                            <td class="text2">
                                <c:out value="${trip.vehicleNo}" />
                                <input type="hidden" name="vehicleno" value="<c:out value="${trip.vehicleNo}" />"/>

                            </td>
                            <td class="text2">Vehicle Capacity (MT)</td>
                            <td class="text2">
                                <c:out value="${trip.vehicleTonnage}" />

                            </td>
                        </tr>

                        <tr>
                            <td class="text1">Veh. Cap [Util%]</td>
                            <td class="text1">
                                <c:out value="${trip.vehicleCapUtil}" />
                            </td>
                            <td class="text1">Special Instruction</td>
                            <td class="text1">-</td>
                            <td class="text1">Trip Schedule</td>
                            <td class="text1"><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" /> </td>
                        </tr>


                        <tr>
                            <td class="text2"><font color="red">*</font>Driver </td>
                            <td class="text2" colspan="5" >
                                 <c:out value="${trip.driverName}" />
                            </td>

                        </tr>
                        <tr>
                            <td class="text1">Product Info </td>
                            <td class="text1" colspan="5" >
                                 <c:out value="${trip.productInfo}" />
                            </td>

                        </tr>
                            </c:forEach>
                       </c:if>
                    </table>
                    <br/>
                    <br/>

                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="4" >Vehicle Compliance Check</td>
                        </tr>

                        <tr>
                            <td class="text2">Vehicle FC Valid UpTo</td>
                            <td class="text2"><label><font color="green">21-08-2014</font></label></td>
                        </tr>
                        <tr>
                            <td class="text1">Vehicle Insurance Valid UpTo</td>
                            <td class="text1"><label><font color="green">11-04-2014</font></label></td>
                        </tr>
                        <tr>
                            <td class="text2">Vehicle Permit Valid UpTo</td>
                            <td class="text2"><label><font color="green">30-12-2013</font></label></td>
                        </tr>
                        <tr>
                            <td class="text2">Road Tax Valid UpTo</td>
                            <td class="text2"><label><font color="green">30-12-2013</font></label></td>
                        </tr>
                    </table>

                    <br/>
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>
                </div>
               <div id="routeDetail">

                    <c:if test = "${tripPointDetails != null}" >
                    <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                        <tr >
                                <td class="contenthead" height="30" >S No</td>
                                <td class="contenthead" height="30" >Point Name</td>
                                <td class="contenthead" height="30" >Type</td>
                                <td class="contenthead" height="30" >Route Order</td>
                                <td class="contenthead" height="30" >Address</td>
                                <td class="contenthead" height="30" >Planned Date</td>
                                <td class="contenthead" height="30" >Planned Time</td>
                      </tr>
                        <% int index2 = 1; %>
                        <c:forEach items="${tripPointDetails}" var="tripPoint">
                         <%
                                        String classText1 = "";
                                        int oddEven = index2 % 2;
                                        if (oddEven > 0) {
                                            classText1 = "text1";
                                        } else {
                                            classText1 = "text2";
                                        }
                            %>
                        <tr >
                                 <td class="<%=classText1%>" height="30" ><%=index2++%></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                      </tr>
                        </c:forEach >
                    </table>
                    </c:if>
                    <br>
                    <br>
                </div>
                <div id="preStart">

                    <c:if test = "${tripPreStartDetails != null}" >
                        <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <c:forEach items="${tripPreStartDetails}" var="preStartDetails">
                            <tr>
                            <td class="contenthead" colspan="4" >Trip Pre Start Details</td>
                        </tr>
                                <tr >
                                <td class="text1" height="30" >Trip Pre Start Date</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preStartDate}" /></td>
                              <td class="text1" height="30" >Trip Pre Start Time</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preStartTime}" /></td>
                            </tr>
                            <tr>
                                <td class="text2" height="30" >Trip Pre Start Odometer Reading</td>
                                    <td class="text2" height="30" ><c:out value="${preStartDetails.preOdometerReading}" /></td>
                            <c:if test = "${tripDetails != null}" >
                                    <td class="text2">Trip Pre Start Location / Distance</td>
                                    <c:forEach items="${tripDetails}" var="trip">
                            <td class="text2"> <c:out value="${trip.preStartLocation}" /> / <c:out value="${trip.preStartLocationDistance}" />KM</td>
                                    </c:forEach>
                            </c:if>
                            </tr>
                            <tr>
                                    <td class="text1" height="30" >Trip Pre Start Remarks</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preTripRemarks}" /></td>
                            </tr>
                            </c:forEach >
                        </table>
                    </c:if>
                    <br>
                    <br>
                </div>
                 <div id="statusDetail">
                    <% int index1 = 1; %>

                    <c:if test = "${statusDetails != null}" >
                        <table border="0endDetail" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <tr >
                                <td class="contenthead" height="30" >S No</td>
                                <td class="contenthead" height="30" >Status Name</td>
                                <td class="contenthead" height="30" >Remarks</td>
                                <td class="contenthead" height="30" >Created User Name</td>
                                <td class="contenthead" height="30" >Created Date</td>
                            </tr>
                            <c:forEach items="${statusDetails}" var="statusDetails">
                                 <%
                                        String classText = "";
                                        int oddEven1 = index1 % 2;
                                        if (oddEven1 > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }
                            %>
                                <tr >
                                    <td class="<%=classText%>" height="30" ><%=index1++%></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.statusName}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripRemarks}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.userName}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripDate}" /></td>
                                </tr>
                            </c:forEach >
                        </table>
                    </c:if>
                    <br>
                    <br>
                </div>
                <div id="tripStart">
                    <script>
                         var rowCount = 1;
        var sno = 0;
        var rowCount1 = 1;
        var sno1 = 0;
        var httpRequest;
        var httpReq;
        var styl = "";

            function addRow1() {
                        
            if (parseInt(rowCount1) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            sno1++;
            var tab = document.getElementById("addTyres1");
            //find current no of rows
            var rowCountNew = document.getElementById('addTyres1').rows.length;            
            rowCountNew--;            
            var newrow = tab.insertRow(rowCountNew);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' > " + sno1 + "</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            // Positions
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='productCodes' class='textbox' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            // TyreIds
            var cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='productNames' class='textbox' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            var cell = newrow.insertCell(3);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='productbatch' class='textbox' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(4);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='packagesNos'   onKeyPress='return onKeyPressBlockCharacters(event);' id='packagesNos' class='textbox' value='' onkeyup='calcTotalPacks(this.value)'>";

            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(5);
            var cell1 = "<td class='text1' height='30' ><select name='productuom' id='uom'><option value='0' selected>-select-</option> <option value='1'>Box</option> <option value='2'>Bag</option><option value='3'>Pallet</option><option value='4'>Each</option></select> </td>";

            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(6);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='weights'   onKeyPress='return onKeyPressBlockCharacters(event);' id='weights' class='textbox' value='' onkeyup='calcTotalWeights(this.value)' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(7);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='loadedpackages'   id='loadedpackages' class='textbox' value='' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            rowCount1++;
        }
                    </script>

                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                      <tr>
                            <td class="contenthead" colspan="4" >Vehicle Reporting Details</td>
                        </tr>
                        <tr>
                            <c:if test = "${tripDetails != null}" >
                                <c:forEach items="${tripDetails}" var="trip">
                                <td class="text1">Trip Planned Start Date</td>
                                <td class="text1"> <c:out value="${trip.tripScheduleDate}" /></td>

                                <td class="text1" height="25" >Trip Planned Start Time </td>
                                <td class="text1"> <c:out value="${trip.tripScheduleTime}" /></td>
                                </c:forEach>
                            </c:if>

                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Vehicle Actual Reporting Date</td>
                            <td class="text2"><input type="text" name="vehicleactreportdate" id="vehicleactreportdate" class="datepicker" value="<%=startDate%>"></td>
                            <td class="text2" height="25" ><font color="red">*</font>Vehicle Actual Reporting Time </td>
                            <td class="text2" colspan="3" align="left" height="25" >
                                HH:<select name="vehicleactreporthour" id="vehicleactreporthour" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                MI:<select name="vehicleactreportmin" id="vehicleactreportmin" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                        </tr>
                        <tr>
                            <td class="contenthead" colspan="4" >Vehicle Loading Details</td>
                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Vehicle Actual Loading Date</td>
                            <td class="text2"><input type="text" name="vehicleloadreportdate" id="vehicleloadreportdate" class="datepicker" value="<%=startDate%>"></td>
                            <td class="text2" height="25" ><font color="red">*</font>Vehicle Actual Loading Time </td>
                            <td class="text2" colspan="3" align="left" height="25" >HH:<select name="vehicleloadreporthour" id="vehicleloadreporthour" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                MI:<select name="vehicleloadreportmin" id="vehicleloadreportmin" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Vehicle Loading Temperature</td>
                            <td class="text2"><input type="text" name="vehicleloadtemperature" id="vehicleloadtemperature"  value="0"></td>
                            <td class="text2" height="25" ></td>
                            <td class="text2" colspan="3" align="left" height="25" ></td>
                        </tr>
                    </table>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="6" >Trip Start Details</td>
                        </tr>
                        <tr>
                            <c:if test = "${tripDetails != null}" >
                                <c:forEach items="${tripDetails}" var="trip">
                                <td class="text1">Trip Planned Start Date</td>
                                <td class="text1"> <c:out value="${trip.tripScheduleDate}" /></td>

                                <td class="text1" height="25" >Trip Planned Start Time </td>
                                <td class="text1"> <c:out value="${trip.tripScheduleTime}" /></td>
                                </c:forEach>
                            </c:if>

                        </tr>
                        <td class="text2"><font color="red">*</font>Trip Actual Start Date</td>
                            <td class="text2"><input type="text" name="startDate" id="startDate" class="datepicker" value="<%=startDate%>"></td>
                            <td class="text2" height="25" ><font color="red">*</font>Trip Actual Start Time </td>
                            <td class="text2" colspan="3" align="left" height="25" >HH:<select name="tripStartHour" id="tripStartHour" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                MI:<select name="tripStartMinute" id="tripStartMinute" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                            
                        <tr>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Trip Start Odometer Reading(KM)</td>
                            <c:if test = "${tripPreStartDetails != null}" >
                                <c:forEach items="${tripPreStartDetails}" var="tripPreStart">
                                    <c:if test = "${tripPreStart.preStartLocationStatus == 1}" >
                                        <td class="text1"><input type="text" name="startOdometerReading" value='<c:out value="${tripPreStart.preOdometerReading}" />' id="startOdometerReading" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" ></td>
                                    </c:if>
                                    <c:if test = "${tripPreStart.preStartLocationStatus != 1}" >
                                        <td class="text1"><input type="text" name="startOdometerReading" value='0' id="startOdometerReading" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" ></td>
                                    </c:if>
                                    
                                </c:forEach>
                            </c:if>

                            


                            <td class="text1"><font color="red">*</font> Trip Start Reefer Reading(HM)</td>
                            <td class="text1"><input type="text" name="startHM" id="startHM" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" value="0"></td>
                            <td class="text1" >Trip Remarks</td>
                            <td class="text1" >

                                <textarea rows="3" cols="30" class="textbox" name="startTripRemarks" id="startTripRemarks"   style="width:142px"></textarea>
                                <!--                         <td class="text1">Trip Status</td>-->
                                <!--                         <td>
                                                             <select name="statusId" id="status" class="textbox"  style="width:125px;">
                                                        <option value="0"> -Select- </option>
                                                        <option value="10" selected>Trip Started </option>
                                                        </select>
                                                            </td>-->
                                <input type="hidden" name="tripSheetId" value="<%=request.getParameter("tripSheetId")%>" />
                        </tr>

                    </table>
                    <br/>
                     <table border="0" class="border" align="left" width="700" cellpadding="0" cellspacing="0" id="addTyres1">
                                    <tr>
                                        <td width="20" class="contenthead" align="center" height="30" >Sno</td>
                                        <td class="contenthead" height="30" >Product/Article Code</td>
                                        <td class="contenthead" height="30" >Product/Article Name </td>
                                        <td class="contenthead" height="30" >Batch </td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>No of Packages</td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>Uom</td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>Total Weight (in Kg)</td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>Loaded Package Nos</td>
                                    </tr>
                                    
                                     <c:if test = "${tripPackDetails != null}" >
                                         <c:forEach items="${tripPackDetails}" var="trippack">
                                            <tr>
                                                <td></td>
                                                <td><input type="text" name="productCodes" value="<c:out value="${trippack.articleCode}"/>" readonly/></td>
                                                <td><input type="text" name="productNames" value="<c:out value="${trippack.articleName}"/>" readonly/></td>
                                                <td><input type="text" name="productbatch" value="<c:out value="${trippack.batch}"/>" readonly/></td>
                                                <td><input type="text" name="packagesNos" value="<c:out value="${trippack.packageNos}"/>" readonly/></td>
                                                <td><input type="text" name="productuom" value="<c:out value="${trippack.uom}"/>" readonly/></td>
                                                <td><input type="text" name="weights" value="<c:out value="${trippack.packageWeight}"/> " readonly/>
                                                <td><input type="text" name="loadedpackages" value="0"/>
                                                    <input type="text" name="consignmentId" value="<c:out value="${trippack.consignmentId}"/>"/>
                                                </td>
                                            </tr>                                    
                                         </c:forEach>
                                     </c:if>


                                    <br>

                                    <tr>
                                        <td colspan="5" align="left">
<!--                                            &nbsp;&nbsp;&nbsp;<input type="reset" class="button" value="Clear">-->
                                            <input type="button" class="button" value="Add Row" name="save" onClick="addRow1()">

                                        </td>
                                    </tr>
                                </table>
                    <br/> <br/>

                    <br/>

                    <center>
                        <input type="button" class="button" name="Save" value="Save" onclick="submitPage();" />
                    </center>
                </div>



                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
            </div>
        </form>
    </body>
</html>