<%--
    Document   : View Cnote
    Created on : Oct 29, 2013, 1:18:37 PM
    Author     : srinivasan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    <script language="javascript">
          function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}
         function submitWindow(){


                document.searchcnote.action = '/throttle/content/cnote/viewCnote.jsp';
                document.searchcnote.submit();
         }
    </script>
    <body>
         <form name="searchcnote" method="post">
             <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                </h2></td>
                <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:900;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Trip Planning</li>
    </ul>
            <div id="first">
<table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">


    <tr>


         <td width="80" height="30"><font color="red">*</font>From Date</td>
                    <td width="182"><input name="fromDate" type="text" class="textbox" value="" size="20">
                    <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.searchcnote.fromDate,'dd-mm-yyyy',this)"/></span></td>
                    <td width="80" height="30"><font color="red">*</font>To Date</td>
                     <td width="182"><input name="toDate" type="text" class="textbox" value="" size="20">
                    <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.searchcnote.toDate,'dd-mm-yyyy',this)"/></span></td>


    </tr>

                        <tr>
                            <td>Customer Name</td>
                            <td><input type="text" name="customerName" class="textbox"></td>
                            <td>Product</td>
                            <td><input type="text" name="product" class="textbox"></td>
                        </tr>
                        <tr>
                            <td>From Location</td>
                            <td><input type="text" name="fromnLocation" class="textbox"></td>
                            <td>To Location</td>
                            <td><input type="text" name="toLocation" class="textbox"></td>
                        </tr>
                        <tr>
                            <td>service Type</td>
                            <td><select name="vehicleId" onchange="fillData()" id="vehicleId" class="textbox" style="width:120px;">
                                            <option value="0"> -Select- </option>
                                            <option value="1"> FTL </option>
                                            <option value="2"> LTL</option>

                                        </select></td>
                            <td>Distance</td>
                            <td><input type="text" name="distance" class="textbox"></td>
                        </tr>


 <td><input type="button"   value="Search" class="button" name="search" onClick="submitWindow()">
                <input type="hidden" value="" name="reqfor"> </td>
    </table>
            </div>
            </div>
    </td>
    </tr>
    </table>

            <h2>Cnote List</h2>
            <br>
            <table width="80" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="40">
                        <th><h3>S.No</h3></th>
                        <th><h3>CNote.No</h3>
                        <th><h3>Customer Name</h3>
                        <th><h3>Customer Code</h3></th>
                        <th><h3>From City</h3></th>
                        <th><h3>To city</h3></th>
                        <th><h3>Pickup Date</h3></th>
                        <th><h3>End Delivery Date</h3></th>
                        <th><h3>Service Type</h3></th>

                    </tr>
                </thead>
                <tbody>
                    <tr height="30">
                        <td align="left" class="text2" width="10">1</td>
                        <td align="left" class="text2" width="10">fd</td>
                        <td align="left" class="text2" width="10">13</td>
                        <td align="left" class="text2" width="10">sfd</td>
                        <td align="left" class="text2" width="10">13</td>
                        <td align="left" class="text2" width="10">sgd</td>
                        <td align="left" class="text2" width="10">13</td>
                        <td align="left" class="text2" width="10">13</td>
                        <td align="left" class="text2" width="10">13</td>
                        
                    </tr>
                    <tr height="30">
                        <td align="left" class="text2" width="10">2</td>
                        <td align="left" class="text2">DL24</td>
                        <td align="left" class="text2">8</td>
                        <td align="left" class="text2">DL2</td>
                        <td align="left" class="text2">8</td>
                        <td align="left" class="text2">DL</td>
                        <td align="left" class="text2">8</td>
                        <td align="left" class="text2">8</td>
                        <td align="left" class="text2">8</td>
                        
                    </tr>
                </tbody>
            </table>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table",1);
            </script>


         </form>
    </body>
</html>
