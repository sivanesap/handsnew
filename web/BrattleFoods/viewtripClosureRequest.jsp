<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $( "#datepicker" ).datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $( ".datepicker" ).datepicker({

            /*altField: "#alternate",
                        altFormat: "DD, d MM, yy"*/
            changeMonth: true,changeYear: true
        });

    });
</script>

    </head>
    <script language="javascript">
        function submitPage(){
            document.approve.action = '/throttle/saveTripClosureapprove.do';
            document.approve.submit();
        }
        function setFocus(){
            document.approve.approveamt.focus();
        }
    </script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Trip Closure Approval" text="Trip Closure Approval"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.Trip Closure Approval" text="Trip Closure Approval"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

    <body onload="setFocus();">
        <form name="approve"  method="post" >
            <%
            request.setAttribute("menuPath"," Trip Closure >> Approval");
            String tripid = request.getParameter("tripid");
            %>
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <c:if test = "${viewTripClosureRequest != null}" >
            <table class="table table-info mb30 table-hover" id="bg" >
                <thead><tr align="center">
                    <th colspan="2">
                        Trip Closure Approval</td>
                </tr></thead>
                <c:forEach items="${viewTripClosureRequest}" var="fd">
                <tr>
                    <td  height="30">Cnote Name</td>
                    <td  height="30"><c:out value="${fd.cnoteName}"/>
                        <input type="hidden" name="cnote" value="<c:out value="${fd.cnoteName}"/>"/>
                    </td>
                </tr>
                <tr>
                    <td  height="30">Vehicle Type</td>
                    <td  height="30"><c:out value="${fd.vehicleTypeName}"/></td>
                </tr>
                <tr>
                    <td  height="30">Vehicle No</td>
                    <td  height="30"><c:out value="${fd.regNo}"/>
                        <input type="hidden" name="vehicleno" value="<c:out value="${fd.regNo}"/>"/>
                    </td>
                </tr>
                <tr>
                    <td  height="30">Route Name</td>
                    <td  height="30"><c:out value="${fd.routeName}"/>
                        <input type="hidden" name="routename" value="<c:out value="${fd.routeName}"/>"/>
                    </td>
                </tr>
                <tr>
                    <td  height="30">Driver Name</td>
                    <td  height="30"><c:out value="${fd.driverName}"/></td>
                </tr>
                <tr>
                    <td  height="30">Planned Date</td>
                    <td  height="30"><c:out value="${fd.planneddate}"/></td>
                </tr>
                
                <tr>
                    <td  height="30">Actual Advance Paid</td>
                    <td  height="30"><c:out value="${fd.actualadvancepaid}"/></td>
                </tr>

                <c:if test="${fd.requesttype==1}">
                <tr>
                    <td  height="30">Request Type</td>
                    <td  height="30">Using Odometer</td>
                </tr>
                <tr>
                    <td  height="30">Total Run Km</td>
                    <td  height="30"><c:out value="${fd.totalrunkm}"/></td>
                </tr>
                <tr>
                    <td  height="30">Total Run Hm</td>
                    <td  height="30"><c:out value="${fd.totalrunhm}"/></td>
                </tr>
                </c:if>
                <c:if test="${fd.requesttype==2}">
                <tr>
                    <td  height="30">Request Type</td>
                    <td  height="30">Expense Deviation</td>
                </tr>
                <tr>
                    <td  height="30">RCM Expense</td>
                    <td  height="30"><c:out value="${fd.rcmexpense}"/></td>
                </tr>
                <tr>
                    <td  height="30">System Expense</td>
                    <td  height="30"><c:out value="${fd.systemexpense}"/></td>
                </tr>
                </c:if>



                <input type="hidden" name="tripid" value="<%=tripid%>"/>
                <input type="hidden" name="tripclosureid" value="<c:out value="${fd.tripclosureid}"/>"/>

                <tr>
                    <td  height="30">Request On</td>
                    <td  height="30">
                        <input name="requeston" class="form-control" style="width:250px;height:40px" type="text" value='<c:out value="${fd.requeston}"/>' readonly></td>
                </tr>

                <tr>
                    <td  height="30"><font color="red">*</font>Status</td>
                    <td  height="30"><select name="approvestatus" class="form-control" style="width:250px;height:40px">
                            <option value="" >-Select Any One-</option>
                            <option value="1">Approved</option>
                            <option value="2">Rejected</option>
                </select></td>
                </tr>


                <tr>
                    <td  height="30"><font color="red">*</font>Approve Remarks</td>
                    <td  height="30"><textarea class="form-control" style="width:250px;height:40px" name="approveremarks"></textarea></td>
                </tr>
</c:forEach>
            </table>
                </c:if>
            <br>
            <center>
                <input type="button" value="Save"  class="btn btn-success"  onClick="submitPage();">
<!--                &emsp;<input type="reset" class="button" value="Clear">-->
            </center>
        </form>
    </body>
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>