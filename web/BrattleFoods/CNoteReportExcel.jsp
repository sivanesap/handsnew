<%-- 
    Document   : CNoteReportExcel
    Created on : Nov 4, 2013, 5:47:14 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.* "%>
<%@ page import="java.text.* "%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
            //System.out.println("Current Date: " + ft.format(dNow));
            String curDate = ft.format(dNow);
            String expFile = "Trip_Sheet_Report-" + curDate + ".xls";

            String fileName = "attachment;filename=" + expFile;
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setHeader("Content-disposition", fileName);
        %>
    </head>
    <style type="text/css">
        .contentsub {
            padding:3px;
            height:24px;
            text-align:left;
            font-weight:bold;
            font-size:14px;
            background:#129fd4;
            color:#ffffff;
            background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
        }

    </style>
    <body>
        <table width="95%"  align="center" cellpadding="0" cellspacing="0" class="border">
            <thead>
                <tr>
                    <td width="35" class="contentsub">Sno</td>
                    <td width="55" class="contentsub">CN No</td>
                    <td width="135" class="contentsub">Customer Name </td>
                    <td width="135" class="contentsub">Customer Code </td>
                    <td width="127" class="contentsub">Origin </td>
                    <td width="121" class="contentsub">Destination </td>
                    <td width="121" class="contentsub">Vehicle Type </td>
                    <td width="74" class="contentsub">Weight </td>
                    <td width="174" height="30" class="contentsub">Actual Pickup Date &amp; Time </td>
                    <td width="174" height="30" class="contentsub">Estimated Delivery Date</td>
                    <td width="104" height="30" class="contentsub">Business Type </td>
                    <td width="134" height="30" class="contentsub">Service Type </td>
                    <td width="134" height="30" class="contentsub">Status</td>
                    <td width="134" height="30" class="contentsub">&nbsp;</td>
                </tr>
            </thead>
            <tr>
                <td class="text1">1</td>
                <td class="text1"><a href="/throttle/BrattleFoods/editConsignment.jsp">AD16700</a></td>
                <td class="text1">Sundaram</td>
                <td class="text1">BF001</td>
                <td class="text1">Bangalore</td>
                <td class="text1">Chennai</td>
                <td class="text1">TATA </td>
                <td class="text1">10000</td>
                <td class="text1">10/05/2013&amp;07:30AM</td>
                <td class="text1">15/05/2013</td>
                <td class="text1">Primary</td>
                <td class="text1">LTL</td>
                <td class="text1"><font color="red">Closed</font></td>
                <td class="text1">Print</td>
            </tr>
            <tr>
                <td class="text2">2<img width="15px" height="15px" src="/throttle/images/flag2.gif" align="middle" border="0"></td>
                <td class="text2"><a href="/throttle/BrattleFoods/editConsignment.jsp">AD14500</a></td>
                <td class="text2">John</td>
                <td class="text2">BF019</td>
                <td class="text2">Bangalore</td>
                <td class="text2">Delhi</td>
                <td class="text2">TATA </td>
                <td class="text2">2340</td>
                <td class="text2">20/11/2011&amp;10:00AM</td>
                <td class="text2">11/11/2011</td>
                <td class="text2" >Secondary</td>
                <td class="text2">LTL</td>
                <td class="text2"><font color="yellow">Created</font></td>
                <td class="text2">Print</td>
            </tr>
            <tr>
                <td class="text1">3<img width="15px" height="15px" src="/throttle/images/flag2.gif" align="middle" border="0"></td>
                <td class="text1"><a href="/throttle/BrattleFoods/editConsignment.jsp">AD1034</a></td>
                <td class="text1">Balaji</td>
                <td class="text1">BF005</td>
                <td class="text1">Kerala</td>
                <td class="text1">Chennai</td>
                <td class="text1">TATA </td>
                <td class="text1">10000</td>
                <td class="text1">10/11/2013&amp;04:00PM</td>
                <td class="text1">15/11/2013</td>
                <td class="text1">Primary</td>
                <td class="text1">LTL</td>
                <td class="text1"><font color="green">Trip Planned</font></td>
                <td class="text1">Print</td>
            </tr>
            <tr>
                <td class="text2">4</td>
                <td class="text2"><a href="/throttle/BrattleFoods/editConsignment.jsp">AD100</a></td>
                <td class="text2" >Prakash</td>
                <td class="text2">BF147</td>
                <td class="text2">Andra Pradesh</td>
                <td class="text2">Chennai</td>
                <td class="text2">TATA </td>
                <td class="text2">10000</td>
                <td class="text2">10/05/2013&amp;11:00AM</td>
                <td class="text2">15/05/2013</td>
                <td class="text2">Primary</td>
                <td class="text2" >LTL</td>
                <td class="text2"><font color="red">Bill Generated</font></td>
                <td class="text2">Print</td>
            </tr>
            <tr>
                <td class="text1">5<img width="15px" height="15px" src="/throttle/images/flag2.gif" align="middle" border="0"></td>
                <td class="text1"><a href="/throttle/BrattleFoods/editConsignment.jsp">AD1036</a></td>
                <td class="text1">John</td>
                <td class="text1">BF019</td>
                <td class="text1">Kerala</td>
                <td class="text1">Chennai</td>
                <td class="text1">TATA </td>
                <td class="text1">10000</td>
                <td class="text1">05/11/2013&amp;05:00AM</td>
                <td class="text1">09/11/2013</td>
                <td class="text1">Primary</td>
                <td class="text1">LTL</td>
                <td class="text1"><font color="green">Trip Started</font></td>
                <td class="text1">Print</td>
            </tr>
            <tr>
                <td class="text2">6</td>
                <td class="text2"><a href="/throttle/BrattleFoods/editConsignment.jsp">AD100</a></td>
                <td class="text2" >Prakash</td>
                <td class="text2">BF147</td>
                <td class="text2">Andra Pradesh</td>
                <td class="text2">Chennai</td>
                <td class="text2">TATA </td>
                <td class="text2">10000</td>
                <td class="text2">10/05/2013&amp;09:00AM</td>
                <td class="text2">15/05/2013</td>
                <td class="text2">Primary</td>
                <td class="text2" >LTL</td>
                <td class="text2"><font color="red">Bill Generated</font></td>
                <td class="text2">Print</td>
            </tr>
            <tr>
                <td class="text1">7</td>
                <td class="text1"><a href="/throttle/BrattleFoods/editConsignment.jsp">AD1034</a></td>
                <td class="text1">John</td>
                <td class="text1">BF019</td>
                <td class="text1">Kerala</td>
                <td class="text1">Chennai</td>
                <td class="text1">TATA </td>
                <td class="text1">10000</td>
                <td class="text1">01/11/2013&amp;12:00PM</td>
                <td class="text1">04/11/2013</td>
                <td class="text1">Primary</td>
                <td class="text1">LTL</td>
                <td class="text1"><font color="red">Trip Settled</font></td>
                <td class="text1">Print</td>
            </tr>
        </table>

    </form>
</body>
</html>
