<%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>


<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    </head>
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Company" text="Mail Not Send"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Company" text="Mail Not Send"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    
    <%
                String menuPath = "Mail Details >> View Mail Details";
                request.setAttribute("menuPath", menuPath);
    %>
    <body>
        <form name="tripSheet" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            
            <%@include file="/content/common/message.jsp" %>

            
           <table class="table table-info mb30 table-hover" id="bg" style="width:20%">	
               <thead>
              
                    <% int i = 1;%> 
                    <% int index = 0;%> 
                    <%int oddEven = 0;%>
                    <%  String classText = "";%>   
                    <c:if test="${mailNotDeliveredList != null}">
                        <tr class="contentsub">
                            <th>S.No</th>
                            <th >Mail Subject</th>
                            <th >Resend</th>
                            <th >Mail Content</th>
                            <th >Mail To</th>
                            <th >Mail Cc</th>
                            <th>Mail Date</th>
                        </tr>
                        </thead>
                        <c:forEach items="${mailNotDeliveredList}" var="mail">
                            <% oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                             classText = "text1";
                                         }%>
                            <tr>
                                <td ><%=i%></td>
                                <td ><c:out value="${mail.mailSubjectTo}"/></td>
                                <td  width="40" align="left">
                                    <img src="images/mailSendingImage.jpg" alt="Mail Alert"   title="Mail Alert" style="width: 30px;height: 30px" onclick="mailTriggerPage('<c:out value="${mail.mailSendingId}"/>');"/>
                                </td>
                                <td >
                                    <!--<a href="#" onclick="viewMailContent('<c:out value="${mail.mailContentTo}"/>')">View Content</a></td>-->
                                    <a href="#" onclick="viewMailContent('<%=i%>')">View Content</a>
                                    <input type="hidden" id="mailContent<%=i%>" value="<c:out value="${mail.mailContentTo}"/>"/></td>
                                <td  ><c:out value="${mail.mailIdTo}"/></td>
                                <td  ><c:out value="${mail.mailIdCc}"/></td>
                                <td  ><c:out value="${mail.mailDate}"/></td>
                            </tr>
                            <%i++;%>
                        </c:forEach>
                    </c:if>

                </tbody>
            </table>
            <script type="text/javascript">
                function viewMailContent(sno){
                    var content = document.getElementById("mailContent"+sno).value;
                    var myWindow = window.open("", "MsgWindow", "width=500, height=1000");
                    myWindow.document.write(content);
                }

                function mailTriggerPage(mailSendingId){
                    window.open('/throttle/viewMailResendDetails.do?mailSendingId='+mailSendingId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                }
            </script>       
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
