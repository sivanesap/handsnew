<%--
    Document   : searchCustomerWiseProfitability
    Created on : Oct 30, 2013, 5:06:46 PM
    Author     : srinivasan
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="java.text.SimpleDateFormat"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!--<html>-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });


            function submitPage(date,active,type,tripType){
              document.customerWise.action = '/throttle/handlefinanceAdviceReportExcel.do?dateval='+date+"&active="+active+"&type="+type+"&tripType="+tripType;
              document.customerWise.submit();  
            }
        </script>
    </head>
    <div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Finance Advice" text="Finance Advice"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.Finance Advice" text="Finance Advice"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            
    <%
                String menuPath = "Finance >> Daily Advance Advice";
                request.setAttribute("menuPath", menuPath);
                String dateval = request.getParameter("dateval");
                String active = request.getParameter("active");
                String type = request.getParameter("type");
                String tripType = request.getParameter("tripType");
    %>
    <body onload="sorter.size(50)">
        <form name="customerWise" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>

            <br><br>
            Advice Date: <%=dateval%>
            <br>
            <br>


               <c:if test = "${financeAdviceDetails != null}" >
                   <center>  <input class="btn btn-success" type="button" name="ExportToExcel" id="ExportToExcel" value="ExportToExcel" onclick="submitPage('<%=dateval%>','<%=active%>','<%=type%>','<%=tripType%>')" /></center>
                   <br/>
                   <table class="table table-info mb30 table-hover" id="table" >
               <thead>                   
                    <tr height="40">
                        <th>S.No</th>
                        <th>Type</th>
                        <th>Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>CNoteNo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>TripNo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>VehicleNo</th>
                        <th>VehicleType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>Route</th>                        
                        <th>TripStart</th>
                        <th>Freight Rate</th>
                        <th>Nett Expenses</th>
                        <th>Profit</th>
                        
                        <th>Paid</th>
                        <th>action</th>
                    </tr>
                    </thead>
                    <tbody>

                        <% int index = 0;%>
                        <c:forEach items="${financeAdviceDetails}" var="FD">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }


                            %>
                            <tr height="30">
                                <td align="left"> <%= index + 1%> </td>
                                <td><c:out value="${FD.batchType}"/></td>
                                <td><c:out value="${FD.customerName}"/></td>
                                <td>
                                    <c:out value="${FD.cnoteName}"/>
                                </td>
                                <td align="left"> <c:out value="${FD.tripcode}" /></td>
                                <td align="left"><c:out value="${FD.regNo}" /></td>
                                <td><c:out value="${FD.vehicleTypeName}"/></td>
                                <td><c:out value="${FD.routeName}"/></td>
                                
                                <td align="left"> <c:out value="${FD.planneddate}"/> </td>
                                <td align="left"> <c:out value="${FD.freightcharges}"/> </td>
                                <td align="left"> <c:out value="${FD.estimatedexpense}"/> </td>
                                <td align="left"> 
                                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${FD.freightcharges-FD.estimatedexpense}" />
                                </td>
                               
                                
                                <%if(active.equals("1")){%>
                                <c:if test="${FD.approvalstatus==0}">
                                <td>-</td>
                                <td  align="left">Waiting for Approval</td>
                                </c:if>

                                <c:if test="${(FD.approvalstatus==3) && FD.paidstatus=='N'}">
                                    <%if(!type.equalsIgnoreCase("M")){%>
                                    <td>-</td>
                                <td  align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                   &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                   &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=0&dateval=<c:out value="${dateval}"/>&type=<c:out value="${type}"/>&active=<c:out value="${active}"/>&tripType=<c:out value="${tripType}"/>">pay</a></td>
                                                   <%}%>
                                 </c:if>
                                

                                 <c:if test="${(FD.paidstatus!='Y')&& (FD.approvalstatus==1)}">
                                 <%if(type.equalsIgnoreCase("M")){%>
                                 <td>-</td>
                                 <td  align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                   &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                   &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=1&dateval=<c:out value="${dateval}"/>&type=<c:out value="${type}"/>&active=<c:out value="${active}"/>&tripType=<c:out value="${tripType}"/>">pay</a></td>
                                 <%}%>
                                 </c:if>

                                <c:if test="${(FD.approvalstatus ==2) && FD.paidstatus !='Y' }">
                                    <td>-</td>
                                <td  align="left">
                                    <c:if test="${(FD.approvalstatus ==2)}">
                                    Rejected
                                    </c:if>
                                    <c:if test="${(FD.approvalstatus ==1)}">
                                    Approved
                                    </c:if>
                                </td>
                                </c:if>

                                <c:if test="${FD.paidstatus=='Y' && FD.approvalstatus != 0}">                                
                                <td  align="left"><c:out value="${FD.paidamt}"/></td>
                                <td  align="left">Paid</td>
                                </c:if>
                                <%}else{%>
                                <c:if test="${FD.paidstatus=='Y'}">
                                <td  align="left"><font color="green"><c:out value="${FD.paidamt}"/></font></td>
                                <td  align="left"><font color="green">Paid</font></td>
                                </c:if>
                                <c:if test="${FD.paidstatus == 'N'}">
                                    <td  align="center"><font color="red">-</font></td>
                                    <td  align="center"><font color="red">Not Paid</font></td>
                                </c:if>
                                <%}%>
                                    
                                <%--
                                <td  align="left"><a href="/throttle/manualfinanceadvice.do?tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>
                                                                           &actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&advicedate=<%=dateval%>&billtype=<%=type%>">Manual Request</a>&nbsp;&nbsp;
                                
                                </td>
                                --%>


                            </tr>
                            <% index++;%>
                        </c:forEach>
                    </c:if>


            </tbody>
            </table>

            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50" selected>50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>


        </form>

    </body>
      </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
