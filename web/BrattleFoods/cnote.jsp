<%--
    Document   : searchCustomerWiseProfitability
    Created on : Oct 30, 2013, 5:06:46 PM
    Author     : srinivasan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
            function submitPage(){
            document.customerWise.action = "/throttle/saveImportConsignmentNote.do";
            document.customerWise.submit();

            }
        </script>
    </head>
    <%
                String menuPath = "CNote >> XL Upload";
                request.setAttribute("menuPath", menuPath);
                String dateval = request.getParameter("dateval");
                String active = request.getParameter("active");
                String type = request.getParameter("type");
    %>
    <body>
        <form name="customerWise" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <br>
            <br>

            <%
            int index = 0;
            int cntr = 0;
            %>
            <c:if test = "${cNoteList != null}" >
                <table width="100%" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="50">
                            <th><h3>S.No</h3></th>
                            <th><h3>CustomerName</h3></th>
                            <th><h3>Status</h3></th>
                            <th><h3>CustomerCode</h3></th>
                            <th><h3>Status</h3></th>
                            <th><h3>Customer Ref No</h3></th>
                            <th><h3>Status</h3></th>
                            <th><h3>FromCity</h3></th>
                            <th><h3>Status</h3></th>
                            <th><h3>IterimPoint1</h3></th>
                            <th><h3>Status</h3></th>
                            <th><h3>Type</h3></th>
                            <th><h3>IterimPoint2</h3></th>
                            <th><h3>Status</h3></th>
                            <th><h3>Type</h3></th>
                            <th><h3>IterimPoint3</h3></th>
                            <th><h3>Status</h3></th>
                            <th><h3>Type</h3></th>
                            <th><h3>IterimPoint4</h3></th>
                            <th><h3>Status</h3></th>
                            <th><h3>Type</h3></th>
                            <th><h3>ToCity</h3></th>
                            <th><h3>Status</h3></th>
                            <th><h3>PickupDate</h3></th>
                            <th><h3>PickupTime</h3></th>
                            <th><h3>ConsignorName</h3></th>
                            <th><h3>ConsignorAddress</h3></th>
                            <th><h3>ConsignorPhone</h3></th>
                            <th><h3>ConsigneeName</h3></th>
                            <th><h3>ConsigneeAddress</h3></th>
                            <th><h3>ConsigneePhone</h3></th>
                            <th><h3>VehicleType</h3></th>
                            <th><h3>Status</h3></th>
                            <th><h3>ProdCategory</h3></th>
                            <th><h3>Status</h3></th>
                            <th><h3>Spl.Remark</h3></th>
                            <th><h3>OverallStatusResponse</h3></th>
                        </tr>
                    </thead>
                    <tbody>

                        
                        <c:forEach items="${cNoteList}" var="cus">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }


                            %>
                            <tr height="30">
                                <td class="<%=classText%>" align="left"> <%= index + 1%> </td>                                                                        
                                <td><c:out value="${cus.customerName}"/></td>
                                <c:if test = "${cus.customerNameStatus == 'N'}" >
                                    <td bgcolor="red">Not Valid</td>
                                </c:if>
                                <c:if test = "${cus.customerNameStatus == 'Y'}" >
                                    <td bgcolor="green">Valid</td>
                            <input type="hidden" name="customerId" value="<c:out value="${cus.customerId}"/>"/>
                            <input type="hidden" name="customerName" value="<c:out value="${cus.customerName}"/>"/>
                            <input type="hidden" name="customerAddress" value="<c:out value="${cus.customerAddress}"/>"/>
                            <input type="hidden" name="customerPhone" value="<c:out value="${cus.customerPhone}"/>"/>
                            <input type="hidden" name="customerMobile" value="<c:out value="${cus.customerMobile}"/>"/>
                            <input type="hidden" name="customerType" value="<c:out value="${cus.customerType}"/>"/>
                            <input type="hidden" name="customerEmail" value="<c:out value="${cus.customerEmail}"/>"/>
                            <input type="hidden" name="billingTypeId" value="<c:out value="${cus.billingTypeId}"/>"/>
                            <input type="hidden" name="contractId" value="<c:out value="${cus.contractId}"/>"/>
                                </c:if>
                                <c:if test = "${cus.customerNameStatus == ''}" >
                                    <td >&nbsp;</td>
                                </c:if>

                                <td><c:out value="${cus.customerCode}"/></td>
                                <c:if test = "${cus.customerCodeStatus == 'N'}" >
                                    <td bgcolor="red">Not Valid</td>
                                </c:if>
                                <c:if test = "${cus.customerCodeStatus == 'Y'}" >
                                    <td bgcolor="green">Valid</td>
                                    <input type="hidden" name="customerCode" value="<c:out value="${cus.customerCode}"/>"/>
                                </c:if>
                                <c:if test = "${cus.customerCodeStatus == ''}" >
                                    <td >&nbsp;</td>
                                </c:if>

                                <td><c:out value="${cus.customerRefNo}"/></td>
                                <input type="hidden" name="customerRefNo" value="<c:out value="${cus.customerRefNo}"/>"/>
                                <c:if test = "${cus.customerRefNoStatus == 'N'}" >
                                    <td bgcolor="red">Not Valid</td>
                                </c:if>
                                <c:if test = "${cus.customerRefNoStatus == 'Y'}" >
                                    <td bgcolor="green">Valid</td>
                                </c:if>
                                <c:if test = "${cus.customerRefNoStatus == ''}" >
                                    <td >&nbsp;</td>
                                </c:if>


                                <td><c:out value="${cus.fromCity}"/></td>
                                <c:if test = "${cus.fromCityStatus == 'N'}" >
                                    <td bgcolor="red">Not Valid</td>
                                </c:if>
                                <c:if test = "${cus.fromCityStatus == 'Y'}" >
                                    <td bgcolor="green">Valid</td>
                                    <input type="hidden" name="fromCity" value="<c:out value="${cus.fromCity}"/>"/>
                                    <input type="hidden" name="originId" value="<c:out value="${cus.originId}"/>"/>
                                </c:if>
                                <c:if test = "${cus.fromCityStatus == ''}" >
                                    <td >&nbsp;</td>
                                </c:if>

                                <td><c:out value="${cus.iterimPoint1}"/></td>
                                <input type="hidden" name="iterimPoint1" value="<c:out value="${cus.point1Id}"/>"/>

                                <c:if test = "${cus.iterimPoint1Status == 'N'}" >
                                    <td bgcolor="red">Not Valid</td>
                                </c:if>
                                <c:if test = "${cus.iterimPoint1Status == 'Y'}" >
                                    <td bgcolor="green">Valid</td>
                                
                                </c:if>
                                <c:if test = "${cus.iterimPoint1Status == ''}" >
                                    <td >&nbsp;</td>
                                </c:if>

                                    <td><c:out value="${cus.iterimPoint1Type}"/>&nbsp;</td>
                                    <input type="hidden" name="iterimPoint1Type" value="<c:out value="${cus.iterimPoint1Type}"/>"/>
                                

                                <td><c:out value="${cus.iterimPoint2}"/>&nbsp;</td>
                                <input type="hidden" name="iterimPoint2" value="<c:out value="${cus.point2Id}"/>"/>
                                <c:if test = "${cus.iterimPoint2Status == 'N'}" >
                                    <td bgcolor="red">Not Valid</td>
                                </c:if>
                                <c:if test = "${cus.iterimPoint2Status == 'Y'}" >
                                    <td bgcolor="green">Valid</td>                                    
                                </c:if>
                                <c:if test = "${cus.iterimPoint2Status == ''}" >
                                    <td >&nbsp;</td>
                                </c:if>

                                    <td><c:out value="${cus.iterimPoint2Type}"/>&nbsp;</td>
                                    <input type="hidden" name="iterimPoint2Type" value="<c:out value="${cus.iterimPoint2Type}"/>"/>
                                

                                <td><c:out value="${cus.iterimPoint3}"/>&nbsp;</td>
                                <input type="hidden" name="iterimPoint3" value="<c:out value="${cus.point3Id}"/>"/>
                                <c:if test = "${cus.iterimPoint3Status == 'N'}" >
                                    <td bgcolor="red">Not Valid</td>
                                </c:if>
                                <c:if test = "${cus.iterimPoint3Status == 'Y'}" >
                                    <td bgcolor="green">Valid</td>                                    
                                </c:if>
                                <c:if test = "${cus.iterimPoint3Status == ''}" >
                                    <td >&nbsp;</td>
                                </c:if>

                                <td><c:out value="${cus.iterimPoint3Type}"/></td>
                                <input type="hidden" name="iterimPoint3Type" value="<c:out value="${cus.iterimPoint3Type}"/>"/>
                                

                                <td><c:out value="${cus.iterimPoint4}"/></td>
                                <input type="hidden" name="iterimPoint4" value="<c:out value="${cus.point4Id}"/>"/>
                                <c:if test = "${cus.iterimPoint4Status == 'N'}" >
                                    <td bgcolor="red">Not Valid</td>
                                </c:if>
                                <c:if test = "${cus.iterimPoint4Status == 'Y'}" >
                                    <td bgcolor="green">Valid</td>                                    
                                </c:if>
                                <c:if test = "${cus.iterimPoint4Status == ''}" >
                                    <td >&nbsp;</td>
                                </c:if>

                                <td><c:out value="${cus.iterimPoint4Type}"/></td>
                                <input type="hidden" name="iterimPoint4Type" value="<c:out value="${cus.iterimPoint4Type}"/>"/>

                                <td><c:out value="${cus.toCity}"/></td>
                                <input type="hidden" name="toCity" value="<c:out value="${cus.toCity}"/>"/>
                                <input type="hidden" name="destinationId" value="<c:out value="${cus.destinationId}"/>"/>
                                <c:if test = "${cus.toCityStatus == 'N'}" >
                                    <td bgcolor="red">Not Valid</td>
                                </c:if>
                                <c:if test = "${cus.toCityStatus == 'Y'}" >
                                    <td bgcolor="green">Valid</td>                                    
                                </c:if>
                                <c:if test = "${cus.toCityStatus == ''}" >
                                    <td >&nbsp;</td>
                                </c:if>

                                <td><c:out value="${cus.pickupDate}"/></td>
                                 <input type="hidden" name="pickupDate" value="<c:out value="${cus.pickupDate}"/>"/>
                                <td><c:out value="${cus.pickupTime}"/></td>
                                 <input type="hidden" name="pickupTime" value="<c:out value="${cus.pickupTime}"/>"/>
                                <td><c:out value="${cus.consignorName}"/></td>
                                 <input type="hidden" name="consignorName" value="<c:out value="${cus.consignorName}"/>"/>
                                <td><c:out value="${cus.consignorAddress}"/></td>
                                 <input type="hidden" name="consignorAddress" value="<c:out value="${cus.consignorAddress}"/>"/>
                                <td><c:out value="${cus.consignorPhone}"/></td>
                                 <input type="hidden" name="consignorPhone" value="<c:out value="${cus.consignorPhone}"/>"/>
                                <td><c:out value="${cus.consigneeName}"/></td>
                                 <input type="hidden" name="consigneeName" value="<c:out value="${cus.consigneeName}"/>"/>
                                <td><c:out value="${cus.consigneeAddress}"/></td>
                                 <input type="hidden" name="consigneeAddress" value="<c:out value="${cus.consigneeAddress}"/>"/>
                                <td><c:out value="${cus.consigneePhone}"/></td>
                                 <input type="hidden" name="consigneePhone" value="<c:out value="${cus.consigneePhone}"/>"/>
                                <td><c:out value="${cus.vehicleType}"/></td>

                                <c:if test = "${cus.vehicleTypeStatus == 'N'}" >
                                    <td bgcolor="red">Not Valid</td>
                                </c:if>
                                <c:if test = "${cus.vehicleTypeStatus == 'Y'}" >
                                    <td bgcolor="green">Valid</td>
                                    <input type="hidden" name="VehicleTypeId" value="<c:out value="${cus.vehicleTypeId}"/>"/>
                                    <input type="hidden" name="totalKm" value="<c:out value="${cus.totalKm}"/>"/>
                                    <input type="hidden" name="totalHm" value="<c:out value="${cus.totalHm}"/>"/>
                                    <input type="hidden" name="routeContractId" value="<c:out value="${cus.routeContractId}"/>"/>
                                </c:if>
                                    <c:if test = "${cus.vehicleTypeStatus == ''}" >
                                    <td >&nbsp;</td>
                                </c:if>
                                <td><c:out value="${cus.prodCategory}"/></td>
                                <c:if test = "${cus.prodCategoryStatus == 'N'}" >
                                    <td bgcolor="red">Not Valid</td>
                                </c:if>
                                <c:if test = "${cus.prodCategoryStatus == 'Y'}" >
                                    <td bgcolor="green">Valid</td>
                                    <input type="hidden" name="prodCategory" value="<c:out value="${cus.productCategoryId}"/>"/>
                                    <input type="hidden" name="reeferRequired" value="<c:out value="${cus.reeferStatus}"/>"/>
                                </c:if>
                                    <c:if test = "${cus.prodCategoryStatus == ''}" >
                                    <td >&nbsp;</td>
                                </c:if>


                                <td><c:out value="${cus.splRemark}"/></td>
                                <input type="hidden" name="splRemark" value="<c:out value="${cus.splRemark}"/>"/>
                                    <%int status=0;%>
                                <c:if test = "${cus.errorMessage == 'all is well'}" >
                                    <td bgcolor="green">Success</td>
                                    <%status=1;%>
                                    <%cntr++;%>
                                </c:if>
                                <c:if test = "${cus.errorMessage != 'all is well'}" >
                                    <td bgcolor="red"><c:out value="${cus.errorMessage}"/></td>                                    
                                </c:if>
                                    <input type="hidden" name="status" value="<%=status%>"/>

                            </tr>
                            <% 
                            index++;
                            %>
                        </c:forEach>
                    </c:if>

                </tbody>
            </table>

            <%if(cntr > 0){%>
            <center>
                <%=cntr%>
                <input type="button" value="GenerateConsignment" name="" class="button" onclick="submitPage()"/>
            </center>
            <%}%>


        </form>

    </body>
</html>
