<%--
    Document   : searchCustomerWiseProfitability
    Created on : Oct 30, 2013, 5:06:46 PM
    Author     : srinivasan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>


 <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
    </head>
<%
        String menuPath = "Finance >> Daily Advance Advice";
        request.setAttribute("menuPath", menuPath);
    %>
    <body>
                 <form name="customerWise" method="post">
             <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>
            
            <br>
            <br>
             <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                <thead>
                    <tr height="30">

                        <td class="text1" colspan="14" >Advice Date: 01-11-2013</td>
                    </tr>
                    <tr height="30">

                        <td class="contenthead">S.No</td>
                        <td class="contenthead">Customer</td>
                        <td class="contenthead">CNote No</td>
                        <td class="contenthead">Trip Id</td>
                        <td class="contenthead">Vehicle No</td>
                        <td class="contenthead">Vehicle Type</td>
                        <td class="contenthead">Route Name</td>
                        <td class="contenthead">Driver Name</td>
                        <td class="contenthead">Trip Start Date</td>
                        <td class="contenthead">Nett Expenses</td>
                        <td class="contenthead">Already Paid Amount</td>
                        <td class="contenthead">Journey Day</td>
                        <td class="contenthead">To Be Paid Today</td>
                        <td class="contenthead">pay</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text1" >1</td>
                        <td class="text1" >M/s Doshi Foods</td>
                        <td class="text1" >CN/13-14/10023</td>
                        <td class="text1" >TS/13-14/10045</td>
                        <td class="text1" >TN 02 AA 2025</td>
                        <td class="text1" >AL / 2516 / 12 PLT</td>
                        <td class="text1" >Delhi-Chennai</td>
                        <td class="text1" >Mr.Ramu</td>
                        <td class="text1" >07-11-2013</td>
                        <td class="text1" >32,540</td>
                        <td class="text1" >27,540</td>
                        <td class="text1" >3</td>
                        <td class="text1" >9,250</td>
                        <td class="text1" ><a href="tripSheetAdvance.jsp">pay</a></td>
                    </tr>
                    <tr>
                        <td class="text2" >2</td>
                        <td class="text2" >M/s Bakers Circle</td>
                        <td class="text2" >CN/13-14/10321</td>
                        <td class="text2" >TS/13-14/10925</td>
                        <td class="text2" >DL 22 SR 4578</td>
                        <td class="text2" >AL / 2516 / 12 PLT</td>
                        <td class="text2" >Kashipur - Delhi</td>
                        <td class="text2" >Mr.Gurmeet</td>
                        <td class="text2" >07-11-2013</td>
                        <td class="text2" >22,540</td>
                        <td class="text2" >17,540</td>
                        <td class="text2" >2</td>
                        <td class="text2" >4,130</td>
                        <td class="text2" ><a href="tripSheetAdvance.jsp">pay</a></td>
                    </tr>
                    <tr>
                        <td class="text1" >3</td>
                        <td class="text1" >M/s Balaji & Co</td>
                        <td class="text1" >CN/13-14/10549</td>
                        <td class="text1" >TS/13-14/10741</td>
                        <td class="text1" >DL 02 WE 7658</td>
                        <td class="text1" >TATA / 3118 / 16 PLT</td>
                        <td class="text1" >Delhi - Chandigarh</td>
                        <td class="text1" >Mr.Rajat</td>
                        <td class="text1" >07-11-2013</td>
                        <td class="text1" >0</td>
                        <td class="text1" >1</td>
                        <td class="text1" >5,450</td>
                        <td class="text1" >2,450</td>
                        <td class="text1" ><a href="tripSheetAdvance.jsp">pay</a></td>
                    </tr>
                </tbody>
            </table>


        </form>
        
    </body>
</html>
