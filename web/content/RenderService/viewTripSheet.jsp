<%-- 
    Document   : viewTripSheet
    Created on : Mar 7, 2012, 6:59:45 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.ArrayList,java.util.Iterator,ets.domain.operation.business.OperationTO"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <body>
        

<table width="90%" id="maintable" align="center" border="0" cellpadding="0" cellspacing="0" class="table2">
    <tr height="30">
        <td colspan="4" align="left" class="texttitle"><h2>Trip Sheet </h2></td>
    </tr>
    <tr height="30">
            <td width="6%" align="left" class="contenthead">S.No</td>
            <td width="40%" align="left" class="contenthead">Route</td>
            <td width="22%" align="left" class="contenthead"> Vehicle No</td>
            <td width="22%" align="left" class="contenthead"> Trip Date</td>
            <td width="10%" align="left" class="contenthead"> Status</td>
            </tr>
        <%
int sno = 0;
String rowClass = "text1";
ArrayList tripDetails = new ArrayList();
if(request.getAttribute("tripDetails") != null) {
    tripDetails = (ArrayList) request.getAttribute("tripDetails");
}
OperationTO operationTo;
Iterator it = tripDetails.iterator();
while(it.hasNext()){

    operationTo = new OperationTO();
    operationTo  = (OperationTO) it.next();
    sno++;
    if((sno % 2) == 0){
        rowClass = "text2";
    } else {
        rowClass = "text1";
    }
%>
<tr height="30">

              <td align="left" class="<%=rowClass%>"><%=sno%></td>
              <td align="left" class="<%=rowClass%>"><%=operationTo.getTripRouteId()%></td>
              <td align="left" class="<%=rowClass%>"><%=operationTo.getTripVehicleId()%></td>
              <td align="left" class="<%=rowClass%>"><%=operationTo.getTripDate()%></td>
              <td align="left" class="<%=rowClass%>"><%=operationTo.getTripStatus()%></td>

            </tr>
        <%

}

%>
</table>
    </body>
</html>
