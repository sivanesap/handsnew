
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
 <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
            <script language="javascript" src="/throttle/js/validate.js"></script>
    </head>
    <script language="javascript">
        function submitPage(value)  {
            document.getElementById("saveBill").style.visibility = "hidden";
            var grnNo = document.getElementsByName("grnNo");
            for(var i=0;i<grnNo.length ; i++){
                if(grnNo[i].value!='0'){
                    alert("Please Receive Invoice For GRN : "+grnNo[i].value);
                    return;
                }
               
            }    
            if(floatValidation(document.jobCardBill.hike,"Hike" )){
                return;
            }
            if(floatValidation(document.jobCardBill.discount,"Discount")){
                return;
            }

            if(confirm("Are you sure to generate bill")){                
                document.jobCardBill.action = "jobCardBillStore.do";
                document.jobCardBill.submit();     
            }                
        }
        function submitPageForDraft(value)  {
            var grnNo = document.getElementsByName("grnNo");
            for(var i=0;i<grnNo.length ; i++){
                if(grnNo[i].value!='0'){
                    alert("Please Receive Invoice For GRN : "+grnNo[i].value);
                    returnlineItemAmount;
                }

            }
            if(floatValidation(document.jobCardBill.hike,"Hike" )){
                return;
            }
            if(floatValidation(document.jobCardBill.discount,"Discount")){
                return;
            }

            document.getElementById("saveBill").style.visibility = "hidden";
            document.jobCardBill.action = "/throttle/content/report/jcDraftBill.jsp";
            document.jobCardBill.submit();

        }
        function correctNett() {
            if(isFloat(document.jobCardBill.discount.value) ){
                alert("Please Enter Discount");
                document.jobCardBill.discount.focus();
                document.jobCardBill.discount.select();
                return;
            }    
            if (parseFloat(document.jobCardBill.discount.value) > parseFloat(document.jobCardBill.total.value)) {
                alert("Discount value cannot be greater than bill value");
                document.jobCardBill.discount.value="";
                document.jobCardBill.discount.focus();
            }else {
            var result = parseFloat(document.jobCardBill.total.value) - (parseFloat(document.jobCardBill.total.value) * (parseFloat(document.jobCardBill.discount.value)/100));            
            document.jobCardBill.nett.value = result.toFixed(2); 
        }
    }     
    
    function correctOthers(){
                
        var hike=parseFloat(document.jobCardBill.hike.value); 
        if( isFloat(hike) ){
            alert("Please Enter Hike Percentage");
            document.jobCardBill.hike.focus();
            document.jobCardBill.discount.focus();
            document.jobCardBill.hike.select();
            return;
        }    
        // Items
        var sparePrices = document.getElementsByName("price");
        var originalPrices = document.getElementsByName("originalPrice");
        var itemQty = document.getElementsByName("quantity");
        var spareAmounts = document.getElementsByName("lineItemAmount"); 
        var tax = document.getElementsByName("tax");
        var spareTotal = 0;
        
        //Labor
       // var activityAmnt = document.getElementsByName("activityAmount");
       // var OriginalActivityAmount = document.getElementsByName("OriginalActivityAmount");
       // bala var laborTotal = 0;

        
        for(var i=0;i<sparePrices.length;i++){
            sparePrices[i].value  = parseFloat( originalPrices[i].value ) + parseFloat( (originalPrices[i].value * hike)/100 );
            sparePrices[i].value = parseFloat(sparePrices[i].value).toFixed(2);
            spareAmounts[i].value = parseFloat( itemQty[i].value ) * (parseFloat( sparePrices[i].value ));
            spareAmounts[i].value = parseFloat(spareAmounts[i].value).toFixed(2);
            spareTotal = parseFloat(spareTotal) + parseFloat(spareAmounts[i].value )
        }
        document.jobCardBill.spares.value = spareTotal;
        
        <%--for(var i=0;i<activityAmnt.length;i++){
            activityAmnt[i].value  = parseFloat( OriginalActivityAmount[i].value ) + parseFloat( (OriginalActivityAmount[i].value * hike)/100 );            
           //bala laborTotal = parseFloat(laborTotal) + parseFloat(activityAmnt[i].value )
        }--%>
        
       //bala document.jobCardBill.labour.value = laborTotal ;
        
        //var spare=parseFloat(document.jobCardBill.spares.value) +parseFloat(document.jobCardBill.spares.value*hike/100);              
        //var labour=parseFloat(document.jobCardBill.labour.value) +parseFloat(document.jobCardBill.labour.value*hike/100);              
        //var total=parseFloat(document.jobCardBill.total.value) +parseFloat(document.jobCardBill.total.value*hike/100);              
        //var nett=parseFloat(document.jobCardBill.nett.value) +parseFloat(document.jobCardBill.nett.value*hike/100);                              
        document.jobCardBill.spares.value = parseFloat(document.jobCardBill.spares.value).toFixed(2); 
        //bala document.jobCardBill.labour.value = parseFloat(document.jobCardBill.labour.value).toFixed(2);
        
        document.jobCardBill.total.value = parseFloat(document.jobCardBill.labour.value) + parseFloat(document.jobCardBill.spares.value)
        
        document.jobCardBill.total.value = parseFloat(document.jobCardBill.total.value).toFixed(2); 
        correctNett();        
    }
    function correctLabor(){
        var labhike = parseFloat(document.jobCardBill.laborhike.value);
        if( isFloat(labhike) ){
            alert("Please Enter LabourHike Percentage");
            document.jobCardBill.laborhike.focus();
            return;
        }
        var laborTotal = 0;
        var activityAmnt = document.getElementsByName("activityAmount");
        var OriginalActivityAmount = document.getElementsByName("OriginalActivityAmount");
        for(var i=0;i<activityAmnt.length;i++){
            activityAmnt[i].value  = parseFloat( OriginalActivityAmount[i].value ) + parseFloat( (OriginalActivityAmount[i].value * labhike)/100 );
            laborTotal = parseFloat(laborTotal) + parseFloat(activityAmnt[i].value )
            <%--activityAmnt[i].value  = parseFloat(document.jobCardBill.activityAmount.toFixed(2));--%>
        }
        document.jobCardBill.labour.value = laborTotal ;
        document.jobCardBill.labour.value = parseFloat(laborTotal).toFixed(2);
        document.jobCardBill.total.value =  parseFloat(document.jobCardBill.labour.value) + parseFloat(document.jobCardBill.spares.value);
        correctNett();
        document.jobCardBill.total.value = total.toFixed(2);
    }
    function setTotal()
    {
        
        var labour=parseFloat(document.jobCardBill.labour.value);                 
        var total=parseFloat(document.jobCardBill.total.value) +parseFloat(labour);              
        var nett=parseFloat(document.jobCardBill.nett.value) +parseFloat(labour);   
        document.jobCardBill.total.value = total.toFixed(2); 
        document.jobCardBill.nett.value = nett.toFixed(2);
    }
            
    function refreshPage(){
            document.jobCardBill.action = "/throttle/closedJobCardBillDetails.do";
            document.jobCardBill.submit();        
    }

    function receiveContract(woId,jcId){           
        window.open('/throttle/searchBWBill.do?mrsJobCardNumber='+woId+'&jcId='+jcId , 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }        
        
    function addLabour(sectionId,probId,activityId){               
        window.open('/throttle/searchLabourRatesInJcBill.do?secId='+sectionId+'&probId='+probId+'&actvtyId='+activityId , 'PopupPage', 'height=600,width=600,scrollbars=yes,resizable=yes');
    }        

    
    function calculateTotLabour(ind){
        var activityLab = document.getElementsByName("activityAmount");
        var OriginalActivityAmount = document.getElementsByName("OriginalActivityAmount");
        if(isFloat( activityLab[ind].value )){
                alert("Please Enter Valid Activity Amount");
                activityLab[i].focus();
                activityLab[i].select();
                return;                
        }else{
                OriginalActivityAmount[ind].value = activityLab[ind].value;                
                <%--correctOthers();--%>
                correctLabor();
                <%--correctNett();--%>
        }
    }    
    
    
 /*   function calculateTotLabour(){
        var activityLab = document.getElementsByName("activityAmount");
        var OriginalActivityAmount = document.getElementsByName("OriginalActivityAmount");
        var totLab = 0;
        for(var i=0; i<activityLab.length ; i++){
            if(isFloat( activityLab[i].value )){
                alert("Please Enter Valid Activity Amount");
                activityLab[i].focus();
                activityLab[i].select();
                return;                
            }else{
                activityLab[i].value = parseFloat(activityLab[i].value ).toFixed(2);
                OriginalActivityAmount[i].value = activityLab[i].value;
                totLab = totLab + parseFloat(activityLab[i].value);
            }
        }
        totLab = parseFloat(totLab).toFixed(2);
        document.jobCardBill.labour.value = totLab;
        correctOthers();
        correctNett();
        return totLab;
    }    */
function setStyle()
{
    var userId = '<%= session.getAttribute("userId") %>'
    if(userId!=null)
        {
    
            if(userId!=1099)
                {
    
                    
                    document.getElementById("dateCal").style.visibility="hidden";
                    
                }
        }
}
    </script>
    
    <body onload="setStyle();">
        
        <form method="post" name="jobCardBill" action= "jobCardBillStore.do">
            <!-- copy there from end -->
        <div id="print" >
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
                    <!-- pointer table -->
                  
                </div>
            </div>
           
            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp" %>
                    </td>
                </tr>
            </table>
            <!-- message table -->
<!-- copy there  end -->
                      <%
            int c = 0;
                            %>
            <c:if test = "${jcList != null}" >
                <c:forEach items="${jcList}" var="list"> 	
                    
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" class="border">
                        <tbody><tr>
                                <td class="contenthead" colspan="6" height="30"><div class="contenthead">Generate Bill</div></td>
                            </tr>
                            
                            <tr>
                                
                                <td class="text1" height="30"><b>JobCardNo</b></td>
                                <td class="text1" height="30"><c:out value="${list.jcMYFormatNo}"/>
                                <input type="hidden" name="jcNo" value='<c:out value="${list.jcMYFormatNo}"/>'/>
                                <input type="hidden" name="jcCreatedDate" value='<c:out value="${list.createdDate}"/>'/>
                                <input type="hidden" name="jcModelName" value='<c:out value="${list.modelName}"/>'/>
                                <input type="hidden" name="jcMfrName" value='<c:out value="${list.mfrName}"/>'/>
                                <input type="hidden" name="jcVehicleNo" value='<c:out value="${list.vehicleNo}"/>'/>
                                <input type="hidden" name="jcCustomerName" value='<c:out value="${list.customerName}"/>'/>                                
                                
                                </td>

                                <td class="text1" height="30"><b>Vehicle No</b></td>
                                <td class="text1" height="30"><c:out value="${list.vehicleNo}"/></td>
                                <td class="text1" height="30"><b>Customer</b></td>
                                <td class="text1" height="30"><c:out value="${list.customerName}"/></td>
                            </tr>
                            <tr id="dateCal">
                                <td class="text1" height="30" id="date1">Date</td>
                                <td class="text1" colspan="2" height="30">
                                    <input type="text" name="date" id="dateTxt" value="<%=session.getAttribute("currentDate")%>" >
                                    <img   src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.jobCardBill.date,'dd-mm-yyyy',this);"  style="cursor:default; "/>
                                </td>
                                <td colspan="3" class="text1" height="30">&nbsp;</td>
                                
                        </tr>
                    </tbody></table>
                    <input type="hidden" name="jobCardId" value='<c:out value="${list.jobCardId}"/>'/>
                    <input type="hidden" name="sparesPercent" value='<c:out value="${list.sparesPercentage}"/>'/>
                    <input type="hidden" name="labourPercent" value='<c:out value="${list.labourPercentage}"/>'/>
                    
                </c:forEach >
                
            </c:if> 
    <br>
        <table width="90%" cellpadding="0" cellspacing="0" border="0" align="center" class="table5">
            <tr>
            <td class="bottom" width="15%" align="left"><img src="/throttle/images/left_status.jpg" alt=""  />Margin</td>
            <td  width="15%" align="left"><h2>Matl Rs</h2></td>
            <td  width="20%" align="left"><h2><div id="itemValue"><fmt:formatNumber value="${itemTotal}" pattern="##.00"/></div></h2>
                <input type="hidden" name="matlMargin" value="">
                <input type="hidden" name="matlMarginPercent" value="">
            </td>
            <td  width="15%" align="right"><h2>Labour Rs</h2></td>
            <td  width="10%" align="left"><h2><div id="laborValue"><fmt:formatNumber value="${labourTotal}" pattern="##.00"/></div></h2>
                <input type="hidden" name="laborMargin" value="">
                <input type="hidden" name="laborMarginPercent" value="">
            </td>
            <td  width="15%" align="right"><h2>Nett Rs</h2></td>
            <td  width="10%" align="left"><h2><div id="totalValue" style="" ><fmt:formatNumber value="${labourTotal}" pattern="##.00"/></div></h2>
                <input type="hidden" name="nettMargin" value="">
                <input type="hidden" name="nettMarginPercent" value="">
            </td>
            </tr>

        </table>
        <br>

            <%

            String classText = "";
            int oddEven = 0;

            int index = 0;%>    
                        <c:set var="check" value="true"/>
            <c:if test = "${jcList != null}" >
                <c:forEach items="${jcList}" var="jcl">            
                    <c:if test = "${jcl.itemsList != null}" >		
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" class="border">
                            <tr>
                                <td class="text2" colspan="8" align="center" height="30"><strong>Spare Particulars</strong></td>
                            </tr>
                            
                            <tr>
                                <td class="contentsub" height="30"><div class="contentsub">SNo</div></td>
                                <td class="contentsub" height="30"><div class="contentsub">Item Code</div></td>
                                <td class="contentsub" height="30"><div class="contentsub">Item Name</div></td>
                                <td class="contentsub" height="30"><div class="contentsub">Quantity</div></td>
                                <td class="contentsub" height="30"><div class="contentsub">Tax(%)</div></td>
                                <td class="contentsub" height="30"><div class="contentsub">Price(Rs.)</div></td>
                                <td class="contentsub" height="30"><div class="contentsub">Purchase Price(Rs.)</div></td>
                                <td class="contentsub" height="30"><div class="contentsub">Amount(Rs.)</div></td>
                                <td class="contentsub" height="30"><div class="contentsub">GRN No</div></td>
                            </tr>
                            
                            <c:set var="itemTotal" value="0"/>
                            <c:set var="purchaseItemTotal" value="0"/>
                            
                            <c:forEach items="${jcl.itemsList}" var="itemList"> 	
                                <%

            classText = "";
            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                                %>
                                <tr height="30">
                                    <td class="<%=classText %>" height="30"><%=index + 1%></td>
                                    <td class="<%=classText %>" height="30">
                                        <c:out value="${itemList.itemCode}"/>
                                        <input type="hidden" name="itemName" value='<c:out value="${itemList.itemName}"/>'/>
                                        <input type="hidden" name="uom" value='<c:out value="${itemList.uom}"/>'/>
                                    </td>
                                    <td class="<%=classText %>" height="30" height="30"><c:out value="${itemList.itemName}"/></td>
                                    <td class="<%=classText %>" height="30"><input type="text" size='10' class="textbox" readonly name="quantity" value='<c:out value="${itemList.quantity}"/>'/></td>
                                    <c:if test="${itemList.itemId==1393 || itemList.itemId==1394 || itemList.itemId==1397 || itemList.itemId==1400||itemList.itemId==6773}">
                                        <c:set var="check" value="false"/>
                                    <td class="<%=classText %>" height="30"><input type="text"  size='10'  class="textbox" readonly name="tax" value='14.5'/></td>
                                    </c:if>
                                    <c:if test="${check==true}">
                                    <td class="<%=classText %>" height="30"><input type="text"  size='10'  class="textbox" readonly name="tax" value='<c:out value="${itemList.tax}"/>'/></td>
                                    </c:if>
                                    <c:set var="check" value="true"/>
                                    <td class="<%=classText %>" height="30"><input type="text" size='10'   class="textbox" readonly name="price" value='<c:out value="${itemList.price}"/>'/> </td>
                                    <td class="<%=classText %>" height="30">
                                        <c:if test="${(itemList.purchasePrice - itemList.price) >= 0}">
                                            <font color="red"> <b><c:out value="${itemList.purchasePrice}"/></b></font>
                                        </c:if>
                                        <c:if test="${(itemList.purchasePrice - itemList.price) < 0}">
                                            <font color="green"> <b><c:out value="${itemList.purchasePrice}"/></b></font>
                                        </c:if>
                                    </td>
                                    <td class="<%=classText %>" height="30"><input type="text"  size='10' class="textbox"  readonly name="lineItemAmount" value='<c:out value="${itemList.lineItemAmount}"/>'/> </td>
                                    <td class="<%=classText %>" height="30"><input type="hidden" name="grnNo" value='<c:out value="${itemList.grnNo}"/>'/> 
                                        <c:if test="${itemList.grnNo != 0}" > 
                                             <a href='/throttle/grnDetailsFromBill.do?supplyId=<c:out value="${itemList.grnNo}"/>'  > <font color="red"> <c:out value="${itemList.grnNo}"/> </font> </a> 
                                        </c:if>
                                        <c:if test="${itemList.grnNo == 0}" > 
                                        <c:out value="${itemList.grnNo}"/> 
                                        </c:if>
                                        <c:out value="${itemList.purchaseAmount}"/>
                                    </td>
                                    <c:set var="itemTotal" value="${itemTotal + itemList.lineItemAmount}" />
                                    <c:set var="purchaseItemTotal" value="${purchaseItemTotal + itemList.purchaseAmount}" />
                                    <input type="hidden" name="itemId" value='<c:out value="${itemList.itemId}"/>'/>
                                    <input type="hidden" readonly name="originalPrice" value='<c:out value="${itemList.price}"/>'/>
                                </tr>
                                <%
            index++;
                                %>
                            </c:forEach >
                            
                        </table>
                        
                    </c:if> 
                    
                </c:forEach >
                
            </c:if>



            <br>
    <%  index = 0;%>
    <% int index1 = 0; %>
    <c:if test = "${jcList != null}" >
	<c:forEach items="${jcList}" var="jcardlist">
	    <c:if test = "${jcardlist.activityList != null}" >

		<table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" class="border">

		    <tr>
			<td class="text2" align="center" colspan="8" height="30"><strong>Service Particulars</strong></td>
		    </tr>

		    <tr>

			<td class="contentsub" height="30"><div class="contentsub">SNo</div></td>
			<td class="contentsub" height="30"><div class="contentsub">Code</div></td>
			<td class="contentsub" height="30"><div class="contentsub">Activities</div></td>
			<td class="contentsub" height="30"><div class="contentsub">Qty</div></td>
			<td class="contentsub" height="30"><div class="contentsub">Rate</div></td>
			<td class="contentsub" height="30"><div class="contentsub">Charge</div></td>
			<td>&nbsp;</td>
		    </tr>
		    <c:set var="labourTotal" value="0" />
		    <c:set var="bodyRepairTotal" value="0" />

		    <c:forEach items="${jcardlist.activityList}" var="paList">
			<%

			    classText = "";
			    oddEven = index % 2;
			    if (oddEven > 0) {
				classText = "text2";
			    } else {
				classText = "text1";
			    }

			%>
			    <tr height="30">
                                <c:if test = "${paList.sectionId == 1040}" >
                                    <c:set var="bodyRepairTotal" value="${bodyRepairTotal + paList.amount}" />
                                </c:if>
 			        <c:set var="labourTotal" value="${labourTotal + paList.amount}" />

				<td class="<%=classText %>" height="30"><%=index + 1%></td>
				<td class="<%=classText %>" height="30"> <c:out value="${paList.activityCode}"/></td>
				<td class="<%=classText %>" height="30"> <c:out value="${paList.activityName}"/></td>
				<td class="<%=classText %>" height="30"> <c:out value="${paList.qty}"/></td>
				<td class="<%=classText %>" height="30"> <c:out value="${paList.rate}"/></td>
                                <td class="<%=classText %>" height="30"><input type="text" class="textbox" size='10'   readonly name="activityAmount" value='<c:out value="${paList.amount}"/>'/>
                                <input type="hidden" readonly name="OriginalActivityAmount" value='<c:out value="${paList.amount}"/>'/></td>
                                <input type="hidden" name="activityId" value='<c:out value="${paList.activityId}"/>'/>


                                <input type="hidden" name="activityName" value='<c:out value="${paList.activityName}"/>'/>
                                <input type="hidden" name="activityQty" value='<c:out value="${paList.qty}"/>'/>
                                <input type="hidden" name="activityRate" value='<c:out value="${paList.rate}"/>'/>

                                <input type="hidden" name="qty" value='<c:out value="${paList.qty}"/>'/>
                                <input type="hidden" name="billNo" value="0"/>
			    </tr>
			    <%
			    index++;
			    %>

		    </c:forEach >


	    </c:if>


	</c:forEach >

    </c:if>

            <br>

            <% index1 = 0; %>
            <c:if test = "${jcList != null}" >
                <c:forEach items="${jcList}" var="jcardlist">             
                    <c:if test = "${jcardlist.problemActivityList != null}" >	     
                        
                            
                  
                            
                                             
                            <c:set var="name" value="vijay" />    
                            
                            <c:forEach items="${jcardlist.problemActivityList}" var="paList"> 		
                                <%

            classText = "";            
            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }

                                %>
                               
                                <c:if test = "${paList.activityName != name}" >
                                    <tr height="30">                            
                                        <c:if test = "${paList.activityId == 0}" >
                                            <c:set var="bodyRepairTotal" value="${bodyRepairTotal + paList.amount}" />
                                        </c:if>     
                                        
                                        <td class="<%=classText %>" height="30"><%=index + 1%></td>
                                        <td class="<%=classText %>" height="30"> &nbsp;</td>
                                        <td class="<%=classText %>" height="30"> <c:out value="${paList.activityName}"/></td>
                                        <td class="<%=classText %>" height="30"> 1</td>
                                        <td class="<%=classText %>" height="30"><c:out value="${paList.amount}"/></td>
                                        <c:if test = "${paList.amount != 0.0}" >
                                            
                                            <td class="<%=classText %>" height="30"><input type="text" class="textbox" size='10'   readonly name="activityAmount" value='<c:out value="${paList.amount}"/>'/>
                                            <input type="hidden" readonly name="OriginalActivityAmount" value='<c:out value="${paList.amount}"/>'/></td>                            
                                            <% index1++; %>
                                        </c:if>     
                                        
                                        <c:if test = "${paList.amount ==0.0}" >
                                        
                                            
                                            <c:if test="${ (paList.sectionId != 1016)   && (paList.workType != 'OUTSIDE' ) }" >
                                                <% c++; %>
                                            <td class="<%=classText %>" height="30">
                                                <font color="red"> <a href="#" onClick="addLabour('<c:out value="${paList.sectionId}"/>',<c:out value="${paList.problemId}"/>,<c:out value="${paList.activityId}"/>);" >Labour</a></font>
                                            </td>                            
                                            </c:if>   
                                            
                                            <c:if test="${ (paList.sectionId != 1016)  && (paList.workType == 'OUTSIDE' ) }" >
                                            
                                            <td class="<%=classText %>" height="30">
                                                <input type="text" class="textbox" size='10' name="activityAmount" value='0' onChange="calculateTotLabour('<%= index1 %>');" />
                                            <input type="hidden" readonly name="OriginalActivityAmount" value='0'/></td>
                                            <%--bala--%>
                                            <% index1++; %>
                                            </c:if>    
                                            <c:if test="${paList.sectionId == 1016}" >
                                                <% c++; %>
                                            <td class="<%=classText %>" height="30">
                                                <font color="red"> <a href="#" onClick="receiveContract('<c:out value="${paList.contractWoId}"/>', '<c:out value="${jcardlist.jobCardId}"/>');"  >Contract</a></font>                                            
                                            </td>                            
                                            </c:if>    
                                            
                                        </c:if>     
                                        <c:set var="labourTotal" value="${labourTotal + paList.amount}" />
                                        <input type="hidden" name="activityId" value='<c:out value="${paList.activityId}"/>'/>
                                        
                                        
                                        
                                        <input type="hidden" name="billNo" value='<c:out value="${paList.billNo}"/>'/>
                                        
                                        
                                        <c:set var="name" value="${paList.activityName}" />
                                        
                                        
                                    </tr>
                                    <%
            index++; 
                                    %>
                                </c:if>    
                                
                                
                                
                            </c:forEach >

                        </table>
                    </c:if> 
                    
                    
                </c:forEach >
                
            </c:if>  

            <br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" class="border">
                <tbody>
                    <tr>
                        <td class="text1" height="30" width="190"><b>Spares Amount</b></td>
                        <td class="text1" height="30" width="189">
                            <input name="spares"  class="textbox" readonly="readonly"  value='<fmt:formatNumber value="${itemTotal}" pattern="##.00"/>' type="text">
                            <input name="purchaseSpares"  class="textbox" readonly="readonly"  value='<fmt:formatNumber value="${purchaseItemTotal}" pattern="##.00"/>' type="hidden">
                        </td>
                    </tr>
                    
                    <c:if test = "${vehicleId!=0}" >
                        <tr>
                            <td class="text2" height="30" width="190"><b>Labour Amount</b></td>
                            <td class="text2" height="30" width="189">
                                <input name="labour"  class="textbox" readonly="readonly"  value='<fmt:formatNumber value="${labourTotal}" pattern="##.00"/>' type="text">
                                <input name="labourExpense"  class="textbox" readonly="readonly"  value='<fmt:formatNumber value="${laborExpense}" pattern="##.00"/>' type="hidden">
                                <input name="jobCost"  class="textbox" readonly="readonly"  value='<fmt:formatNumber value="${laborExpense + purchaseItemTotal }" pattern="##.00"/>' type="hidden">
                            </td>
                        </tr>
                    </c:if>

                <script>
                    document.getElementById("itemValue").innerHTML = (parseFloat(document.jobCardBill.spares.value) - parseFloat(document.jobCardBill.purchaseSpares.value)).toFixed(2);
                    document.getElementById("laborValue").innerHTML = (parseFloat(document.jobCardBill.labour.value) - parseFloat(document.jobCardBill.labourExpense.value)).toFixed(2);
                    document.getElementById("totalValue").innerHTML = (parseFloat(document.getElementById("itemValue").innerHTML) +
                                                                            parseFloat(document.getElementById("laborValue").innerHTML)).toFixed(2);

                    document.jobCardBill.matlMargin.value = document.getElementById("itemValue").innerHTML;
                    document.jobCardBill.laborMargin.value = document.getElementById("laborValue").innerHTML;
                    document.jobCardBill.nettMargin.value = document.getElementById("totalValue").innerHTML;

                    if(parseFloat(document.getElementById("itemValue").innerHTML) != 0) {
                        document.getElementById("itemValue").innerHTML = document.getElementById("itemValue").innerHTML + ":" + (parseFloat(document.getElementById("itemValue").innerHTML)*100 / parseFloat(document.jobCardBill.spares.value)).toFixed(2) +"%";
                        document.jobCardBill.matlMarginPercent.value = (parseFloat(document.getElementById("itemValue").innerHTML)*100 / parseFloat(document.jobCardBill.spares.value)).toFixed(2);
                    }else {
                        document.getElementById("itemValue").innerHTML = document.getElementById("itemValue").innerHTML + ":0%";
                        document.jobCardBill.matlMarginPercent.value = 0;
                    }

                    if(parseFloat(document.getElementById("laborValue").innerHTML) != 0) {
                        document.getElementById("laborValue").innerHTML = document.getElementById("laborValue").innerHTML + ":" + (parseFloat(document.getElementById("laborValue").innerHTML)*100 / parseFloat(document.jobCardBill.labour.value)).toFixed(2) +"%";
                        document.jobCardBill.laborMarginPercent.value = (parseFloat(document.getElementById("laborValue").innerHTML)*100 / parseFloat(document.jobCardBill.labour.value)).toFixed(2);
                    }else {
                        document.getElementById("laborValue").innerHTML = document.getElementById("laborValue").innerHTML + ":0%";
                        document.jobCardBill.laborMarginPercent.value = 0;
                    }

                    

//                    alert(parseFloat(document.getElementById("totalValue").innerHTML));
//                    alert(parseFloat(document.jobCardBill.spares.value) + parseFloat(document.jobCardBill.labour.value));
                    if ( (parseFloat(document.getElementById("totalValue").innerHTML) ) > 0 ) {
                        document.getElementById("totalValue").style.color='green';
                    }else {
                        document.getElementById("totalValue").style.color='red';
                    }


                    if(parseFloat(document.getElementById("totalValue").innerHTML) > 0) {
                        document.getElementById("totalValue").innerHTML = document.getElementById("totalValue").innerHTML + ":" + (parseFloat(document.getElementById("totalValue").innerHTML)*100 / (parseFloat(document.jobCardBill.spares.value) + parseFloat(document.jobCardBill.labour.value))).toFixed(2) +"%";
                        document.jobCardBill.nettMarginPercent.value = (parseFloat(document.getElementById("totalValue").innerHTML)*100 / (parseFloat(document.jobCardBill.spares.value) + parseFloat(document.jobCardBill.labour.value))).toFixed(2);
                    }else {
                        document.getElementById("totalValue").innerHTML = document.getElementById("totalValue").innerHTML + ":0%";
                        document.jobCardBill.nettMarginPercent.value = 0;
                    }

                    
                    //document.getElementById("totalValue").setAttribute('style', 'font-weight: bold; color: green;');
                    if ( (parseFloat(document.jobCardBill.spares.value) - parseFloat(document.jobCardBill.purchaseSpares.value)) > 0 ) {
                        document.getElementById("itemValue").style.color='green';
                    }else {
                        document.getElementById("itemValue").style.color='red';
                    }

                    if ( (parseFloat(document.jobCardBill.labour.value) - parseFloat(document.jobCardBill.labourExpense.value)) > 0 ) {
                        document.getElementById("laborValue").style.color='green';
                    }else {
                        document.getElementById("laborValue").style.color='red';
                    }

                </script>

                    
                    <c:if test = "${vehicleId==0}" >
                        <tr>
                            <td class="text2" height="30" width="190"><b>Labour Amount</b></td>
                            <td class="text2" height="30" width="189"><input name="labour" onchange="setTotal();"  class="textbox"  type="text"></td>
                        </tr>
                    </c:if>   
                    <tr>
                        <td class="text1" height="30" width="190"><b>Total Amount</b></td>
                        <td class="text1" height="30" width="189"><input  name="total" class="textbox" readonly="readonly"  value='<fmt:formatNumber value="${itemTotal + labourTotal}" pattern="##.00"/>' type="text"></td>
                    </tr>
                    <!--
                    <tr>
                        <td class="text2" height="30" width="190"><b>SpareHike(in %age)</b></td>
                        <td class="text2" height="30" width="189"><input name="hike" class="textbox" type="text" value="0" onchange="correctOthers();"/></td>
                    </tr>
                    
                    <tr>
                        <td class="text1" height="30" width="190"><b>LabourHike(in %age)</b></td>
                        <td class="text1" height="30" width="189"><input name="laborhike" class="textbox" type="text" value="0" onchange="correctLabor();"/></td>
                    </tr>
                    
                    <tr>
                        <td class="text2" height="30" width="190"><b>Discount(in %age)</b></td>
                        <td class="text2" height="30" width="189"><input name="discount" class="textbox" type="text" value="0" onchange="correctNett();"/></td>
                    </tr>
                    -->
                    <input name="discount" class="textbox" type="hidden" value="0" onchange="correctNett();"/>
                    <input name="laborhike" class="textbox" type="hidden" value="0" onchange="correctLabor();"/>
                    <input name="hike" class="textbox" type="hidden" value="0" onchange="correctOthers();"/>

                    <tr>
                        <td class="text2" height="30" width="190"><b>Nett Amount</b></td>
                        <td class="text2" height="30" width="189"><input class="textbox" readonly="readonly" name="nett" value='<fmt:formatNumber value="${itemTotal + labourTotal}" pattern="##.00"/>' type="text"></td>
                    </tr>
                        <tr>
                            <td class="text1" height="30" width="190"><b>Invoice Type</b></td>
                            <td class="text1" height="30" width="189">
                                <select name="inVoiceType" class="textbox" >
                                    <option value="CASH">CASH </option>
                                    <option value="CREDIT" selected>CREDIT </option>
                                </select>    
                            </td>
                        </tr>                     
                    
            </tbody></table>
            </div>                
            
            <center>
                <br>
                    <%
                    if(c==0){
                    %>
                    <div id="saveBill" style="visibility:visible;" align="center" >
                        <input class="button" type="button" value="Draft Bill" onClick="submitPageForDraft();"> &nbsp;&nbsp;
                        <input class="button" type="button" value="Save Bill" onClick="submitPage();"> </div>
                <%
                }else{
            %>
            <table align="center" border="1" cellpadding="0" cellspacing="0" width="700" class="border">
                        <tr>
                            <td class="text2" height="30" ><font color="red">Bill can not be generated now,rectify the points shown in red</font>
                            </td>
                    </tr>  
            </table><br>
                <!--<input class="button" type="button" value="Refresh" onClick="refreshPage();">    -->
            <%
            }
            %>  
            </center>
            <br>
        </form>
    </body>
</html>
