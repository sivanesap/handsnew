
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Parveen</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
            
    </head>
    <script language="javascript">

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        function submitPage(value)
        {
        if(value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value =='First' || value =='Last'){
                        if(value=='GoTo'){
                            var temp=document.jobCardCloseView.GoTo.value;       
                            document.jobCardCloseView.pageNo.value=temp;
                            document.jobCardCloseView.button.value=value;
                            document.jobCardCloseView.action = '/throttle/closeJobCardView.do';   
                            document.jobCardCloseView.submit();
                        }else if(value == "First"){
                        temp ="1";
                        document.jobCardCloseView.pageNo.value = temp; 
                        value='GoTo';
                    }else if(value == "Last"){
                    temp =document.jobCardCloseView.last.value;
                    document.jobCardCloseView.pageNo.value = temp; 
                    value='GoTo';
                }
                document.jobCardCloseView.button.value=value;
                document.jobCardCloseView.action = '/throttle/closeJobCardView.do';   
                document.jobCardCloseView.submit();
            }            

            document.desigDetail.submit();
        }
        
        function setValues(){
            if( '<%= request.getAttribute("regNo") %>' != 'null' ){
                document.jobCardCloseView.regNo.value = '<%= request.getAttribute("regNo") %>';
            }    
            if( '<%= request.getAttribute("jcId") %>' != 'null' ){
                document.jobCardCloseView.jobcardId.value = '<%= request.getAttribute("jcId") %>';
            }    
            if( '<%= request.getAttribute("compId") %>' != 'null' ){
                document.jobCardCloseView.serviceLocation.value = '<%= request.getAttribute("compId") %>';
            }    
            
         }   
function getVehicleNos(){
    //onkeypress='getList(sno,this.id)'         
    var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
}          
    </script>
     <c:if test="${jcList != null}">
        <body onLoad="setValues();getVehicleNos();">
        </c:if>
        <c:if test="${jcList == null}">
        <body onLoad="submitPage('search');setValues();getVehicleNos();">
        </c:if>
        
        <form method="post" name="jobCardCloseView">
            <!-- copy there from end -->
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
             
                </div>
            </div>

            
              
            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp" %>
                    </td>
                </tr>
            </table>
            <!-- message table -->
<!-- copy there  end -->


    <table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Close Job Card</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
         <td align="left" height="30"><font color="red">*</font>Reg.No</td>
            <td height="30"><input type="text" id="regno" class="textbox" name="regNo" value="" /></td>
            <td align="left" height="30">Job Card No</td>
            <td><input type="text" class="textbox" name="jobcardId" value="" /> </td>
            <td>Service Point</td>
            <td> <select class="textbox" name="serviceLocation"  style="width:125px;">
                    <option selected   value=0>---Select---</option>
                    <c:if test = "${servicePoints != null}" >
                    <c:forEach items="${servicePoints}" var="mfr">
                    <option value='<c:out value="${mfr.compId}" />'>
                    <c:out value="${mfr.compName}" />
                    </option>
                    </c:forEach >
                    </c:if>
                    </select>
            </td>
        <td><input type="button" class="button" name="search" value="search" onClick="submitPage(this.name);"  />   </td>
    </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>

            
          

 <%          
            int index = 0;      
            int pag = (Integer)request.getAttribute("pageNo");
            index = ((pag -1) *10) +1 ;
%>  
            <br>
            <c:if test = "${jcList != null}" >
		<table align="center" width="90%" border="0" cellspacing="0" cellpadding="0" class="border">
		
		<tr height="30">
		<th class="contenthead" align="left">SNo</th>
		<th class="contenthead" align="left">Job Card No</th>
		<th class="contenthead" align="left">Vehicle No</th>
		<th class="contenthead" align="left">Company Name</th>
		<th class="contenthead" align="left">Delivery Time</th>
		<th class="contenthead" align="left">Status</th>
		<th class="contenthead" align="left">Action</th>
		</tr>
                    <%
 
                    %>
                    
                    <c:forEach items="${jcList}" var="list"> 	
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr height="30">
                            <td class="<%=classText %>" height="30"><%=index %></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.jobCardId}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.vehicleNo}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.companyName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.jobCardPCD}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.status}"/></td>
                            <td class="<%=classText %>" height="30">
                                <a href='/throttle/handleCloseJobCardDetailsNew.do?jobcardId=<c:out value="${list.jobCardId}"/>&vehicleId=<c:out value="${list.vehicleId}"/>&workOrderId=<c:out value="${list.workOrderId}"/>&km=<c:out value="${list.km}"/>&hm=<c:out value="${list.hm}"/>&companyId=<c:out value="${list.operationPointId}"/>&reqDate=<c:out value="${list.jobCardPCD}"/>' target="frame">Close</a>
                            </td>
                           <%-- <td class="<%=classText %>" height="30">
                            
                            <a href='/throttle/closeJobCardDetails.do?jobCardId=<c:out value="${list.jobCardId}"/>' target="frame">Close</a>
                            &nbsp;&nbsp;<a href='/throttle/closeExtJobCardDetails.do?jobCardId=<c:out value="${list.jobCardId}"/>' target="frame">Close</a>
                            </td> --%>


                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    
                </table>
            </c:if>

           

            <br>
            
            <table align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <%@ include file="/content/common/pagination.jsp"%>  
                    </td>
                    
                </tr>
            </table>            
                           
                <input type="hidden" name="reqfor" value="">            
            <br>
        </form>
    </body>
</html>
