<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>  
</head>
<script>
    function submitPage()
    {
           
        if(textValidation(document.addRack.rackName,'Rack Name')){
   
        return;
        }  
        if(textValidation(document.addRack.rackDescription,'Rack Description')){
   
        return;
         }  
        document.addRack.action='/throttle/handleAddRack.do';
        document.addRack.submit();
    }
    function setFocus(){
        document.addRack.rackName.focus();
        }
   
</script>
<body onload="setFocus();">
<form name="addRack" method="post" >
<%@ include file="/content/common/path.jsp" %>


<%@ include file="/content/common/message.jsp" %>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="300" id="bg" class="border">
<tr>
<td colspan="2" class="contenthead" height="30"><div class="contenthead">Add Rack Types</div></td>
</tr>
<%
    String classText = "";
    classText = "text2";       
   
    %>
<tr>
<td class="<%=classText %>" height="30"><font color="red">*</font>Rack Name</td>
<td class="<%=classText %>" height="30"><input name="rackName" type="text" class="textbox" value=""></td>
</tr>
<%
   classText = "text1";
 %>
<tr>
<td class="<%=classText %>" height="30"><font color="red">*</font>Rack Description</td>
<td class="<%=classText %>" height="30"><textarea class="textbox"  name="rackDescription"></textarea></td>
</tr>
</table>
<br>
<center>
<input type="button" value="Add" class="button" onclick="submitPage();">
&emsp;<input type="reset" class="button" value="Clear">
</center>
</form>
</body>
</html>
