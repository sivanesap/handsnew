<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#cityFrom').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityFromName.do",
                    dataType: "json",
                    data: {
                        cityFrom: request.term,
                        cityToId: document.getElementById('cityToId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#cityFromId').val(tmp[0]);
                $('#cityFrom').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

        $('#cityTo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityToName.do",
                    dataType: "json",
                    data: {
                        cityTo: request.term,
                        cityFromId: document.getElementById('cityFromId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#cityToId').val(tmp[0]);
                $('#cityTo').val(tmp[1]);
                checkRouteCode();
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        }

    });


    var httpRequest;
    function checkRouteCode() {
        var cityFromId = document.getElementById('cityFromId').value;
        var cityToId = document.getElementById('cityToId').value;
        if (cityFromId != '' && cityToId != '') {
            var url = '/throttle/checkRoute.do?cityFromId=' + cityFromId + '&cityToId=' + cityToId;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);
        }
    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#routeStatus").text('Route Exists Code is :' + val);
                } else {
                    $("#routeStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

    //savefunction
    function submitPage(value) {
        $('#save').hide();
        var count = validateRouteDetails();
        if(document.getElementById('distance').value == '' ){
            alert("enter the travel distance");
            document.getElementById('distance').focus();
        }else if(count == 0){
        document.route.action = '/throttle/updateRoute.do';
        document.route.submit();
        }
    }


function validateRouteDetails(){
        var vehMileage = document.getElementsByName("vehMileage");
        var reefMileage =  document.getElementsByName("reefMileage");
        var fuelCostPerKms =  document.getElementsByName("fuelCostPerKms");
        var fuelCostPerHrs = document.getElementsByName("fuelCostPerHrs");
        var tollAmounts = document.getElementsByName("tollAmounts");
        var miscCostKm = document.getElementsByName("miscCostKm");
        var driverIncenKm = document.getElementsByName("driverIncenKm");
        var factor = document.getElementsByName("factor");
        var vehExpense = document.getElementsByName("vehExpense");
        var reeferExpense = document.getElementsByName("reeferExpense");
        var totExpense = document.getElementsByName("totExpense");

        var a = 0;
        var count=0;
        for(var i=0 ; i<vehMileage.length; i++ ){
            a = i+1;
            if(fuelCostPerKms[i].value == ''){
                alert("please fill fuel cost per km for row "+a);
                fuelCostPerKms[i].focus();
                count=1;
                return count;
            }else if(fuelCostPerHrs[i].value == ''){
                alert("please fill fuel cost per hrs for row "+a);
                fuelCostPerHrs[i].focus();
                count=1;
                return count;
            }else if(tollAmounts[i].value == ''){
                alert("please fill toll rate per km for row "+a);
                tollAmounts[i].focus();
                count=1;
                return count;
            }else if(miscCostKm[i].value == ''){
                alert("please fill miscellaneous cost per km for row "+a);
                miscCostKm[i].focus();
                count=1;
                return count;
            }else if(driverIncenKm[i].value == ''){
                alert("please fill driver incentive per km for row "+a);
                driverIncenKm[i].focus();
                count=1;
                return count;
            }else if(factor[i].value == ''){
                alert("please fill factor for row "+a);
                factor[i].focus();
                count=1;
                return count;
            }else if(vehExpense[i].value == ''){
                alert("please fill vehExpense for row "+a);
                vehExpense[i].focus();
                count=1;
                return count;
            }else if(reeferExpense[i].value == ''){
                alert("please fill reeferExpense for row "+a);
                reeferExpense[i].focus();
                count=1;
                return count;
            }else if(totExpense[i].value == ''){
                alert("please fill totExpense for row "+a);
                totExpense[i].focus();
                count=1;
            }
        }
        return count;
    }


//    function numbersOnly(oToCheckField, oKeyEvent) {
//    return oKeyEvent.charCode === 0 || /\d/.test(String.fromCharCode(oKeyEvent.charCode));
//  }
function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }


//
//    function numeralsOnly(evt) {
//       evt = (evt) ? evt : event;
//        var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
//           ((evt.which) ? evt.which : 0));
//        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
//           alert("Enter numerals only in this field.");
//           return false;
//          }
//           return true;
//   }


   
</script>

<!--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>-->
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.EditRoute" text="EditRoute"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.EditRoute" text="EditRoute"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    
    <body onload="sorter.size(10);calcFuleCostPerKm();setTollCost(1);">
        <form name="route"  method="post">
            
            <c:if test="${routeList != null}">


            <table class="table table-info mb30 table-hover" id="bg" >	
              
                <c:forEach items="${routeList}" var="rl">
                      <thead>
                <tr>
                    <th  colspan="6" >Route  Edit</th>
                </tr>
                </thead>
                <tr>
                    <td >Route Code</td>
                    <td ><input type="hidden" name="routeId" id='routeId' class="textbox" value="<c:out value="${rl.routeId}"/>"/><input type="hidden" name="routeCode" id='routeCode' class="textbox" value="<c:out value="${rl.routeCode}"/>" readonly><c:out value="${rl.routeCode}"/></td>
                    <!--<td >Toll Amount Type</td>-->
                    <td >
                        <input type="hidden" name="tollAmountType" id="tollAmountType" onclick="setTollCost(1);" value="1" checked  />
                        <!--Average-->
                       <%-- <c:if test="${rl.tollAmountType == '1'}">
                        <input type="radio" name="tollAmountType" id="tollAmountType" onclick="setTollCost(1);" value="1" checked  />Average
                        <input type="radio" name="tollAmountType" id="tollAmountType" value="2"  onclick="setTollCost(2);" value="2" />Fixed
                        </c:if>
                        <c:if test="${rl.tollAmountType == '2'}">
                        <input type="radio" name="tollAmountType" id="tollAmountType" onclick="setTollCost(1);" value="1" />Average
                        <input type="radio" name="tollAmountType" id="tollAmountType" value="2"  onclick="setTollCost(2);" value="2" checked  />Fixed
                        </c:if>  --%>
                        </td>
                </tr>
                <tr>
                    <td >From Location</td>
                    <td >
                        <input type="hidden" name="cityFromId" id="cityFromId" class="textbox" value="<c:out value="${rl.cityFromId}"/>"/>
                        <input type="hidden" name="cityFrom" id="cityFrom" class="textbox" value="<c:out value="${rl.cityFromName}"/>"/>
                        <c:out value="${rl.cityFromName}"/>
                    </td>
                    <td >To Location</td>
                    <td >
                        <input type="hidden" name="cityToId" id="cityToId" class="textbox" value="<c:out value="${rl.cityToId}"/>"/>
                        <input type="hidden" name="cityTo" id="cityTo" class="textbox" value="<c:out value="${rl.cityToName}"/>"/>
                        <c:out value="${rl.cityToName}"/>
                    </td>
                </tr>
                <tr>
                    <td >KM</td>
                    <td ><input type="text" name="distance" id="distance" class="form-control" style="width:250px;height:40px" onkeyup="calcFuleCostPerKm()" onkeypress="return onKeyPressBlockCharacters(event)" onpaste="return false;" value="<c:out value="${rl.distance}"/>"></td>
                    <td >Travel Time(Hrs)</td>
                    <td >HR:<input type="text"  style="width:38px" name="travelHour" id="travelHour" class="textbox" onkeyup="calcReferHours()" value="<c:out value="${rl.travelHour}"/>" readonly/>MI:<input type="text" name="travelMinute" style="width:38px" id="travelMinute" class="textbox" onkeyup="calcReferHours()" value="<c:out value="${rl.travelMinute}"/>" readonly/></td>
                </tr>
                <tr style="display:none">
                    <td >Reefer Running Hours</td>
                    <td >HR:<input type="text"  style="width:38px" name="reeferHour" id="reeferHour" class="textbox" value="<c:out value="${rl.reeferHour}"/>" readonly/>MI:<input type="text" name="reeferMinute" style="width:38px" id="reeferMinute" class="textbox" value="<c:out value="${rl.reeferMinute}"/>" readonly/></td>
                    <td >Road Type</td>
                    <td >
                        <select  style="width:250px;height:40px" name="roadType" id="roadType">
                            <c:if test="${rl.roadType == 'National Highway'}">
                            <option value="NH" selected>National Highway</option>
                            <option value="SH" >State Highway</option>
                            </c:if>
                            <c:if test="${rl.roadType == 'State Highway'}">
                            <option value="NH">National Highway</option>
                            <option value="SH" selected>State Highway</option>
                            </c:if>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td >Current Fuel Cost/Ltr (PAN INDIA)</td>
                    <td ><input type="hidden" name="fuelCost" id="fuelCost"   value="<%=request.getAttribute("currentFuelPrice")%>" readonly/><label><%=request.getAttribute("currentFuelPrice")%></label></td>
                     <td >Status</td>
                    <td >
                        <select style="width:250px;height:40px" name="status" >
                            <option value="Y">Active</option>
                            <option value="N">In Active</option>
                        </select>
                        <script>
                            document.getElementById('status').value = '<c:out value="${rl.status}"/>'
                        </script>
                    </td>
                    <td style="display:none" >Current CNG Cost/Kg (PAN INDIA)</td>
                    <td style="display:none" ><input type="hidden" name="cngCost" id="cngCost"  value="<%=request.getAttribute("currentCNGPrice")%>" readonly/><label><%=request.getAttribute("currentCNGPrice")%></label></td>
                </tr>
                <tr>
                   

                    <td colspan="4" >
                        <center>
                            <input type="button" class="btn btn-success" id="save" value="Save"  name='Save' onclick="submitPage()"/>
                        </center>
                    </td>
                </tr>
                 <tr>
                    <td><input type="hidden" id="avgTollAmount" name="avgTollAmount" value="<c:out value="${avgTollAmount}"/>" ></td>
                    <td><input type="hidden" name="avgMisCost" id="avgMisCost"  value="<c:out value="${avgMisCost}"/>"/></td>
                    <td><input type="hidden" name="avgDriverIncentive" id="avgDriverIncentive"  value="<c:out value="${avgDriverIncentive}"/>"/></td>
                    <td><input type="hidden" name="avgFactor" id="avgFactor" value="<c:out value="${avgFactor}"/>"/></td>
                    <td><input type="hidden" id="avgTollAmount3118" name="avgTollAmount3118" value="<c:out value="${avgTollAmount3118}"/>" ></td>
                    <td><input type="hidden" name="avgMisCost3118" id="avgMisCost3118"  value="<c:out value="${avgMisCost3118}"/>"/></td>
                    <td><input type="hidden" name="avgDriverIncentive3118" id="avgDriverIncentive3118"  value="<c:out value="${avgDriverIncentive3118}"/>"/></td>
                    <td><input type="hidden" name="avgFactor3118" id="avgFactor3118" value="<c:out value="${avgFactor3118}"/>"/></td>
                </tr>
                </c:forEach>
            </table>
            </c:if>
            <script type="text/javascript">
                //concard value get from the addrout configDetails
                function setTollCost(type) {
                    if (type == 1) {
                        var km = document.getElementById("distance").value;
                        var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                        var tollAmounts = document.getElementsByName("tollAmounts");
                        var misCost = document.getElementsByName("miscCostKm");
                        var driverIncentive = document.getElementsByName("driverIncenKm");
                        var reeferExpense = document.getElementsByName('reeferExpense');
                        var factor = document.getElementsByName('factor');
                        var vehExpense = document.getElementsByName('vehExpense');
                        var fuelTypeId = document.getElementsByName('fuelTypeId');
                        var vehicleTonnage = document.getElementsByName('vehicleTonnage');
                        for (var i = 0; i < tollAmounts.length; i++) {
                            if(vehicleTonnage[i].value == "13.50" && fuelTypeId[i].value == "1002"){
                            tollAmounts[i].value = document.getElementById('avgTollAmount').value;
                            misCost[i].value = document.getElementById('avgMisCost').value;
                            driverIncentive[i].value = document.getElementById('avgDriverIncentive').value;
                            factor[i].value = document.getElementById('avgFactor').value; 
                            driverIncentive[i].readOnly =false;
                            misCost[i].readOnly = false;
                          //  tollAmounts[i].readOnly =false;
                            factor[i].readOnly = true;
                            }else if(vehicleTonnage[i].value == "18.00" && fuelTypeId[i].value == "1002"){
                            tollAmounts[i].value = document.getElementById('avgTollAmount3118').value;
                            misCost[i].value = document.getElementById('avgMisCost3118').value;
                            driverIncentive[i].value = document.getElementById('avgDriverIncentive3118').value;
                            factor[i].value = document.getElementById('avgFactor3118').value;
                       //     driverIncentive[i].readOnly =false;
                        //    misCost[i].readOnly =false;
                          //  tollAmounts[i].readOnly =false;
                            factor[i].readOnly = true;    
                            }else if(vehicleTonnage[i].value == "0.00" && fuelTypeId[i].value == "1003"){
                            //alert(fuelTypeId[i].value);
                            tollAmounts[i].value = 0;
                            misCost[i].value = 0;
                            driverIncentive[i].value = 0;
                            factor[i].value = 0;
                            driverIncentive[i].readOnly = false;
                            misCost[i].readOnly = false;
                      //      tollAmounts[i].readOnly = false;
                            factor[i].readOnly = false; 
                            }
                            
                            if (fuelCostPerKms[i].value != '') {
                                var fuelAmnt = fuelCostPerKms[i].value * km;
                            } else {
                                var fuelAmnt = 0 * km;
                            }
                            if (tollAmounts[i].value != '') {
                                var tollAmnt = tollAmounts[i].value * km;
                            } else {
                                var tollAmnt = 0 * km;
                            }
                            if (misCost[i].value != '') {
                                var misCostPerKm = misCost[i].value * km;
                            } else {
                                var misCostPerKm = 0 * km;
                            }
//                            if (factor[i].value != '') {
//                                var factorPerKm = factor[i].value * km;
//                            } else {
//                                var factorPerKm = 0 * km;
//                            }
                            if (driverIncentive[i].value != '') {
                                var driverIncentivePerKm = driverIncentive[i].value * km;
                            } else {
                                var driverIncentivePerKm = 0 * km;
                            }
                            if (fuelCostPerKms[i].value != '') {
//                                var total = parseFloat(tollAmnt) + parseFloat(misCostPerKm) + parseFloat(driverIncentivePerKm) + parseFloat(fuelAmnt) + parseFloat(factorPerKm);
                                var total = parseFloat(tollAmnt) + parseFloat(misCostPerKm) + parseFloat(driverIncentivePerKm) + parseFloat(fuelAmnt);
                                vehExpense[i].value = total.toFixed(2);
                            } else {
                                var driverIncentivePerKm = 0 * km;
                            }
                            
                            calcTotExp();
                        }
                    } else if (type == 2) {
                        var km = document.getElementById("distance").value;
                        var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                        var vehExpense = document.getElementsByName('vehExpense');
                        var tollAmounts = document.getElementsByName("tollAmounts");
                        var misCost = document.getElementsByName("miscCostKm");
                        var driverIncentive = document.getElementsByName("driverIncenKm");
                        var factor = document.getElementsByName("factor");
                        for (var i = 0; i < fuelCostPerKms.length; i++) {
                            if (fuelCostPerKms[i].value != '') {
                                var total = fuelCostPerKms[i].value * km
                                vehExpense[i].value = total.toFixed(2);
                            } else {
                                vehExpense[i].value = 0.00;
                            }
                            tollAmounts[i].value = "";
                            misCost[i].value = '';
                            driverIncentive[i].value = '';
                            factor[i].value = '';
                            tollAmounts[i].readOnly = false;
                            driverIncentive[i].readOnly = false;
                            misCost[i].readOnly = false;
                            factor[i].readOnly = false;
                            calcTotExp();
                        }
                    }
                }

                //calculate fuelCostPerKm
                function calcFuleCostPerKm() {
                    var km = document.getElementById("distance").value;
                    var fuelCostPerKm = 0;
                    var fuelCost = document.getElementById('fuelCost').value;
                    var cngCost = document.getElementById('cngCost').value;
                    var fuelTypeId = document.getElementsByName('fuelTypeId');
                    var vehMileage = document.getElementsByName('vehMileage');
                    var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                    var factor = document.getElementsByName('factor');
                    if (km != '') {
                        for (var i = 0; i < vehMileage.length; i++) {
                            var totFuelCost = parseInt(km) / parseInt(vehMileage[i].value) * parseInt(fuelCost);
                            //fuelCostPerKm = parseInt(totFuelCost) / parseInt(km);
                            if(fuelTypeId[i].value == 1002){
                            fuelCostPerKm = parseFloat(fuelCost) / parseFloat(vehMileage[i].value);
                            fuelCostPerKms[i].value = fuelCostPerKm.toFixed(2);
                            }else if(fuelTypeId[i].value == 1003){
                            fuelCostPerKm = parseFloat(cngCost) / parseFloat(vehMileage[i].value);
                            fuelCostPerKms[i].value = fuelCostPerKm.toFixed(2);
                            }
                        }
                    } else {
                        for (var i = 0; i < vehMileage.length; i++) {
                            fuelCostPerKms[i].value = '';
                        }
                    }
                    calcVehExp();

                    //Calc Vehicle Running Hours
                    var perDayTravelKm = 450;
                    var travelKm = km / perDayTravelKm;
                    var hours = travelKm.toFixed(2) * 24;
                    var travelHrs = hours.toFixed(2);
                    //alert(travelHrs);
                    var temp = travelHrs.split(".");
                    travelHrs = temp[0];
                    //alert(travelHrs);
                    var travelMinute = temp[1];
                    var hour = 0;
                    var minute = 0;
                    if (temp[1] > 59) {
                        for (var travelHour = 0; travelMinute > 59; travelHrs++) {
                            hour = travelHour + 1;
                            travelMinute = parseInt(travelMinute) - 60;
                            minute = travelMinute;
                        }
                    } else {
                        hour = 0;
                        minute = travelMinute;
                    }
                    var tothour = hour + parseInt(temp[0]);
                    //alert(tothour);
                     if (tothour < 10) {
                        tothour = '0' + tothour;
                    }
                    //if (minute < 10 && (minute.indexOf("0") != -1) == 'false') {
                    if (minute < 10) {
                        minute = '0' + minute;
                    }
//                    alert(minute.indexOf("0") != -1);
                    document.getElementById("travelHour").value = tothour;
                    //alert(document.getElementById("travelHour").value);
                    document.getElementById("travelMinute").value = minute;
                    calcReferHours();
                }
                //calculate Vehicle Expenses cost
                
                //calculate reffer hours
                //calculate reefer hours
                function calcReferHours() {
                    var km = document.getElementById("distance").value;
                      //Calc reefer Running Hours
                    var perDayTravelKm1 = 600;
                    var travelKm1 = km / perDayTravelKm1;
                    var hours = travelKm1.toFixed(2) * 24;
                    var travelHrs = hours.toFixed(2);
                        var travelHour2 = travelHrs / 100;
                        var travelHour3 = (travelHour2 * 66).toFixed(2);
                        var refMinute = travelHour3;
                        var reeferHrs = travelHour3;
                        var temp = reeferHrs.split(".");
                        var reeferHrs1 = temp[0];
                        var reeferMinute = temp[1];
                        var hour = 0;
                        var minute = 0;
                        
                        hour = reeferHrs1;
                        minute = ((reeferMinute/100) * 60).toFixed(0);
                        /*
                        if (reeferMinute > 59) {
                            for (var reeferHour = 0; reeferMinute > 59; reeferHrs1++) {
                                var hour1 = reeferHour + 1;
                                reeferMinute = parseInt(reeferMinute) - 60;
                                minute = reeferMinute;
                            }
                                 hour=reeferHrs1 + hour
                        } else {
                            hour = reeferHrs1;
                            minute = reeferMinute;
                        }
                        */
                    if (hour < 10) {
                        hour = '0' + hour;
                    }

                    if (minute < 10) {
                        minute = '0' + minute;
                    }
                    if(minute == ''){
                        minute == '0';
                    }
                    document.getElementById("reeferHour").value = parseFloat(hour);
                    document.getElementById("reeferMinute").value = parseFloat(minute);


                    if (refMinute != 00) {
//                    alert(refMinute);
                        calcFuleCostPerHr(refMinute);
                    }
                }

                //calculate fuelCostPerHr
                function calcFuleCostPerHr(refMinute) {
//  
                    var fuelCost = document.getElementById('fuelCost').value;
                    var reefMileage = document.getElementsByName('reefMileage');
                    var fuelCostPerHrs = document.getElementsByName('fuelCostPerHrs');
                    var reeferExpense = document.getElementsByName('reeferExpense');
                    for (var i = 0; i < reefMileage.length; i++) {
                        //alert("refMinute==="+refMinute.toFixed(2));
                   /*     var totFuleCostPerMinute = parseInt(refMinute) / parseInt(reefMileage[i].value) * parseInt(fuelCost);
                        var fuleCostPerMinute = parseInt(totFuleCostPerMinute) / parseInt(refMinute);
                        var fuelCostPerHour = fuleCostPerMinute * 60;
                        fuelCostPerHrs[i].value = fuelCostPerHour.toFixed(2);
                        var totalReeferCastPerMinute = parseInt(refMinute) * parseInt(fuelCostPerHrs[i].value);
                        var refExpCal = totalReeferCastPerMinute / 60;
                        reeferExpense[i].value = refExpCal.toFixed(2);
                    */
                        //alert(reefMileage[i].value);
                        
                        var referExp =  reefMileage[i].value * fuelCost;
                        fuelCostPerHrs[i].value = referExp.toFixed(2);
                        //alert(fuelCostPerHrs[i].value);
                        //alert(refMinute.toFixed(2));
                        var totalReefExp = fuelCostPerHrs[i].value * refMinute;
                        //alert(totalReefExp.toFixed(2));
                        reeferExpense[i].value = totalReefExp.toFixed(2);


                    }
                    calcTotExp();
                }


               

                function calcFactorExp(index) {
                    var factorPerKm = document.getElementById('factor' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    var fuelTypeId = document.getElementById('fuelTypeId' + index).value;
                    //var reeferExp = document.getElementById('reeferExpense'+index).value;
                     var miscCostKm = document.getElementById('miscCostKm' + index).value;
                    if(miscCostKm == ""){
                        miscCostKm = 0;
                    }
                    var driverIncenKm = document.getElementById('driverIncenKm' + index).value;
                     if(driverIncenKm == ""){
                        driverIncenKm = 0;
                    }
                    var tollAmounts = document.getElementById('tollAmounts' + index).value;
                    if(tollAmounts == ""){
                        tollAmounts = 0;
                    }
                    var totExp = "";
                    if(factorPerKm !='' && fuelTypeId == 1002){
                        totExp = parseFloat(factorPerKm) * km + parseFloat(vehExp);
                        document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                    }else if(factorPerKm !='' && fuelTypeId == 1003){
                        totExp = parseFloat(factorPerKm) + parseFloat(vehExp) + parseFloat(driverIncenKm) + parseFloat(tollAmounts) + parseFloat(miscCostKm);
                        document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                    }

                }

            </script>
            <br>
            <br>
            <br>
            <br>
            <% int count = 0;%>
            <c:if test="${routeDetailsList != null}">
                <table class="table table-info mb30 table-hover sortable" id="table" style="display:none" >	
			<thead>
               
                        <tr >
                            <th>S.No</th>
                            <th>Vehicle Type </th>
                          
                            <th>Fuel Cost Per Km's</th>
                            <th>Fuel Cost Per Hr's </th>
                            <th>Toll Cost Per KM @@@@ </th>
                            <th>Misc Cost Per KM</th>
                            <th>Driver Incentive Per KM</th>
                            <th>Factor</th>
                            
                            <th>Vehicle Expense Cost</th>
                            <th>Reefer Expense Cost</th>
                            <th>Total Expense Cost</th>
                        </tr>
                    </thead>
                 <% int index = 0;
                        int sno = 1;
                    %>
                    <tbody>
                        <c:forEach items="${routeDetailsList}" var="rdl">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>

                            <tr height="30">
                                <td align="left" ><%=sno%></td>
                                <td align="left"  style="width: 200px">
                                    <input type="hidden" name="routeCostIds" id="routeCostId" value="<c:out value="${rdl.routeCostId}"/>" readonly style="width:150px;height:40px"/>
                                    <input type="hidden" name="vehTypeId" id="vehTypeId" value="<c:out value="${rdl.vehicleTypeId}"/>" readonly style="width:150px;height:40px"/><c:out value="${rdl.vehicleTypeName}"/>
                                    <input type="hidden" name="vehMileage" id="vehMileage" value="<c:out value="${rdl.vehicleMileage}"/>"/>
                                    <input type="hidden" name="reefMileage" id="reefMileage" value="<c:out value="${rdl.reeferMileage}"/>"/>
                                    <input type="hidden" name="fuelTypeId" id="fuelTypeId<%=index%>" value="<c:out value="${rdl.fuelTypeId}"/>"/>
                                    <input type="hidden" name="vehicleTonnage" id="vehicleTonnage<%=index%>" value="<c:out value="${rdl.vehicleTonnage}"/>"/>
                                </td>
                                <td align="left" ><input type="text" name="fuelCostPerKms" id="fuelCostPerKms<%=index%>"   style="width:150px;height:40px" value="<c:out value="${rdl.fuelCostKm}"/>" readonly/></td>
                                <td align="left" ><input type="text" name="fuelCostPerHrs" id="fuelCostPerHrs<%=index%>"  style="width:150px;height:40px" value="<c:out value="${rdl.fuelCostHr}"/>"/></td>
                                <td align="left" ><input type="text" name="tollAmounts" id="tollAmounts<%=index%>"  style="width:150px;height:40px"  value="<c:out value="${rdl.tollAmountperkm}"/>" onkeyup="calcTollExp('<%=index%>')" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" ><input type="text" name="miscCostKm" id="miscCostKm<%=index%>"   style="width:150px;height:40px" onkeyup="onchange('<%=index%>')" value="<c:out value="${rdl.miscCostperkm}"/>" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" ><input type="text" name="driverIncenKm" id="driverIncenKm<%=index%>"  style="width:150px;height:40px" onkeyup="onchange('<%=index%>')" value="<c:out value="${rdl.driverIncentperkm}"/>" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" ><input type="text" name="factor" id="factor<%=index%>"  style="width:150px;height:40px" onchange="calcFactorExp('<%=index%>')" value="<c:out value="${rdl.factors}"/>" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" ><input type="text" name="vehExpense" id="vehExpense<%=index%>"style="width:150px;height:40px"  value="<c:out value="${rdl.vehiExpense}"/>" readonly/>
                                 <input type="hidden" name="varExpense" id="varExpense" value="<c:out value="${rdl.variExpense}"/>"/></td>
                                <td align="left" ><input type="text" name="reeferExpense" id="reeferExpense<%=index%>"  style="width:150px;height:40px" value="<c:out value="${rdl.reefeExpense}"/>" readonly/></td>
                                <td align="left" ><input type="text" name="totExpense" id="totExpense<%=index%>"   style="width:150px;height:40px" value="<c:out value="${rdl.totaExpense}"/>" readonly/></td>
                            </tr>
                            <%sno++;%>
                            <%index++;%>
                        </c:forEach>
                           
                    </tbody>
                </table>

                    <script>
      
               function calcTollExp(index) {

                    var tollAmounts = document.getElementById('tollAmounts' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    var fuelTypeId = document.getElementById('fuelTypeId' + index).value;
                    var miscCostKm = document.getElementById('miscCostKm' + index).value;
                    if(miscCostKm ==""){
                        miscCostKm = 0;
                    }
                    var driverIncenKm = document.getElementById('driverIncenKm' + index).value;
                    if(driverIncenKm == ""){
                        driverIncenKm = 0;
                    }
                    var factorPerKm = document.getElementById('factor' + index).value;
                    if(factorPerKm == ""){
                        factorPerKm = 0;
                    }
                    //var reeferExp = document.getElementById('reeferExpense'+index).value;
//                    var totExp = "";
//                    if(tollAmounts != '' && fuelTypeId == 1002){
//                    totExp = parseFloat(tollAmounts) * km + parseFloat(vehExp);
//                    document.getElementById('totExpense' + index).value = totExp.toFixed(2);
//                    }else if(tollAmounts != '' && fuelTypeId == 1003 && miscCostKm != ""){
//                    totExp = parseFloat(tollAmounts) + parseFloat(vehExp) + parseFloat(miscCostKm) + parseFloat(driverIncenKm) +parseFloat(factorPerKm);
//                    document.getElementById('totExpense' + index).value = totExp.toFixed(2);
//                    }

                    calcVehExp();


                }


                function calcVehExp() {

                    var km = document.route.distance.value;
                    var tollAmounts = document.getElementsByName('tollAmounts');
                    var miscCostKm = document.getElementsByName('miscCostKm');
                    var driverIncenKm = document.getElementsByName('driverIncenKm');
                    var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                    var factorPerKm = document.getElementsByName('factor');
                    var vehExpense = document.getElementsByName('vehExpense');
                    var fuelCostPerHrs = document.getElementsByName('fuelCostPerHrs');
                    var reeferExpense = document.getElementsByName('reeferExpense');
                    var totExpense = document.getElementsByName('totExpense');
                    if (km != '') {
                        for (var i = 0; i < fuelCostPerKms.length; i++) {
                            var expCost = parseInt(km) * fuelCostPerKms[i].value;
                            if (tollAmounts[i].value != '') {
                                var tollAmnt = tollAmounts[i].value * km;
                            } else {
                                var tollAmnt = 0 * km;
                            }
                            if (miscCostKm[i].value != '') {
                                var misCost = miscCostKm[i].value * km;
                            } else {
                                var misCost = 0 * km;
                            }
                            if (driverIncenKm[i].value != '') {
                                var driverIncentive = driverIncenKm[i].value * km;
                            } else {
                                var driverIncentive = 0 * km;
                            }
//                            if (factorPerKm[i].value != '') {
//                                var factor = factorPerKm[i].value * km;
//                            } else {
//                                var factor = 0 * km;
//                            }
//                            var total = parseFloat(expCost) + parseFloat(tollAmnt) + parseFloat(misCost) + parseFloat(driverIncentive) + parseFloat(factor);
//                            alert("parseFloat(expCost)"+parseFloat(expCost));
//                            alert("parseFloat(tollAmnt)"+parseFloat(tollAmnt));
//                            alert("parseFloat(misCost)"+parseFloat(misCost));
//                            alert("parseFloat(driverIncentive)"+parseFloat(driverIncentive.toFixed(2)));
                            var total = parseFloat(expCost.toFixed(2)) + parseFloat(tollAmnt.toFixed(2)) + parseFloat(misCost.toFixed(2)) + parseFloat(driverIncentive.toFixed(2));
                            vehExpense[i].value = total.toFixed(2);
                        }
                    } else {
                        for (var i = 0; i < fuelCostPerKms.length; i++) {
                            vehExpense[i].value = '';
                            fuelCostPerHrs[i].value = '';
                            reeferExpense[i].value = '';
                            totExpense[i].value = '';
                        }
                    }
                    calcTotExp();
                }

                     function calcTotExp() {
                    var km = document.route.distance.value;
                    var reeferExpense = document.getElementsByName('reeferExpense');
                    var vehExpense = document.getElementsByName('vehExpense');
                    var totExpense = document.getElementsByName('totExpense');
                    for (var i = 0; i < vehExpense.length; i++) {
                        if (vehExpense[i].value != '') {
                            var vehExp = vehExpense[i].value;
                        } else {
                            var vehExp = 0;
                        }
                        if (reeferExpense[i].value != '') {
                            var refExp = reeferExpense[i].value;
                        } else {
                            var refExp = 0;
                        }
                        var totCost = parseFloat(vehExp) + parseFloat(refExp);
                        totExpense[i].value = totCost.toFixed(2);
                    }
                }


                       function calcMiscExp(index) {
                    var tollAmounts = document.getElementById('tollAmounts' + index).value;
                    var miscCostKm = document.getElementById('miscCostKm' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    var fuelTypeId = document.getElementById('fuelTypeId' + index).value;
                    var driverIncenKm = document.getElementById('driverIncenKm' + index).value;
                    if(tollAmounts == ""){
                        tollAmounts = 0;
                    }
                    if(driverIncenKm == ""){
                        driverIncenKm = 0;
                    }
                    var factorPerKm = document.getElementById('factor' + index).value;
                    if(factorPerKm == ""){
                        factorPerKm = 0;
                    }
                    //var reeferExp = document.getElementById('reeferExpense'+index).value;
                    var totExp = "";
//                    if(miscCostKm != '' && fuelTypeId == 1002){
//                        totExp = parseFloat(miscCostKm) * km + parseFloat(vehExp);
//                        document.getElementById('totExpense' + index).value = totExp.toFixed(2);
//                    }else if(miscCostKm != '' && fuelTypeId == 1003){
//                        totExp = parseFloat(miscCostKm) + parseFloat(vehExp) + parseFloat(tollAmounts) + parseFloat(driverIncenKm) +parseFloat(factorPerKm);
//                        document.getElementById('totExpense' + index).value = totExp.toFixed(2);
//                    }
                            calcVehExp();
                }
                function calcDriExp(index) {
                    var driverIncenKm = document.getElementById('driverIncenKm' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    //var reeferExp = document.getElementById('reeferExpense'+index).value;
                    var fuelTypeId = document.getElementById('fuelTypeId' + index).value;
                     if(driverIncenKm == ""){
                        driverIncenKm = 0;
                    }
                    var tollAmounts = document.getElementById('tollAmounts' + index).value;
                    if(tollAmounts == ""){
                        tollAmounts = 0;
                    }
                    var factorPerKm = document.getElementById('factor' + index).value;
                    if(factorPerKm == ""){
                        factorPerKm = 0;
                    }
                     var miscCostKm = document.getElementById('miscCostKm' + index).value;
                    if(miscCostKm == ""){
                        miscCostKm = 0;
                    }
                    var totExp = "";
//                    if(driverIncenKm != '' && fuelTypeId == 1002){
//                    totExp = parseFloat(driverIncenKm) * km + parseFloat(vehExp);
//                    document.getElementById('totExpense' + index).value = totExp.toFixed(2);
//                    }else if(driverIncenKm != '' && fuelTypeId == 1003){
//                    totExp = parseFloat(driverIncenKm) + parseFloat(vehExp) + parseFloat(tollAmounts) + parseFloat(miscCostKm) +parseFloat(factorPerKm);
//                    document.getElementById('totExpense' + index).value = totExp.toFixed(2);
//                    }
                          calcVehExp();

                }


                    </script>
                <script language="javascript" type="text/javascript">
                    setFilterGrid("table");</script>
                <div id="controls" style="display:none" >
                    <div id="perpage">
                        <select onchange="sorter.size(this.value)">
                            <option value="5" selected="selected">5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span>Entries Per Page</span>
                    </div>
                    <div id="navigation">
                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                    </div>
                    <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                </div>
                <script type="text/javascript">
                    var sorter = new TINY.table.sorter("sorter");
                    sorter.head = "head";
                    sorter.asc = "asc";
                    sorter.desc = "desc";
                    sorter.even = "evenrow";


















                    sorter.odd = "oddrow";
                    sorter.evensel = "evenselected";
                    sorter.oddsel = "oddselected";
                    sorter.paginate = true;
                    sorter.currentid = "currentpage";
                    sorter.limitid = "pagelimit";
                    sorter.init("table", 1);
                </script>
            </c:if>
            <br>
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
