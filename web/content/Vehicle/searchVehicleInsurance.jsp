

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<script language="javascript" src="/throttle/js/validate.js"></script>  
<%@ page import="java.util.*" %>      
<%@ page import="java.http.*" %>      
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>   
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display = 'none';
            }

            function show_exp() {
                document.getElementById('exp_table').style.display = 'block';
            }

            function show_close() {
                document.getElementById('exp_table').style.display = 'none';
            }
        </script>
<script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
$(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#regno').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getVehicleNo.do",
                    dataType: "json",
                    data: {
                        vehicleNo: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                        alert("Invalid Vehicle No");
                        $('#regno').val('');
                        $('#vehicleId').val('');    
                        $('#regno').fous();
                        }else{
                        response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                $('#regno').val(value);
                $('#vehicleId').val(id);
                
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
           });

        </script>
                              <style>
                        #index td {
   color:white;
}
</style>
         <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.VehicleInsurance"  text="Vehicle Insurance"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Trucks"/></a></li>
          <li class="active"><spring:message code="stores.label.VehicleInsurance"  text="Vehicle Insurance"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
    <!--setImages(1,0,0,0,0,0);-->
    <body onLoad="setDefaultVals('<%= request.getAttribute("regNo")%>', '<%= request.getAttribute("typeId")%>', '<%= request.getAttribute("mfrId")%>', '<%= request.getAttribute("usageId")%>', '<%= request.getAttribute("groupId")%>');">
        <form name="viewVehicleDetails"  method="post" >
            <%@ include file="/content/common/message.jsp" %>
<table class="table table-bordered" >
<!--              <tr height="30" id="index">
                             <td colspan="4"  style="background-color:#5BC0DE;"><b>View Vehicle Insurance</b></td>&nbsp;&nbsp;&nbsp;                    
                    </tr>-->
            <tr>
                <td align="center" style="border-color:#5BC0DE;padding:16px;">
                         <table>
                            <tr>
                                <td><spring:message code="trailers.label.VehicleNumber" text="default text"/></td>
                                <td>
                                    <input type="hidden" name="vehicleId" id="vehicleId" class="textbox" value=""  >
                                   <input type="textbox" id="regno" name="regno" value="<c:out value="${regNo}" />" class="form-control" style="width:260px;height: 38px;">
                                </td>
                                <td><spring:message code="trailers.label.InsuranceCompany" text="default text"/></td>
                                <td>
                                    <select class="form-control" name="vendorId" id="vendorId" type="textbox" style="width:260px;height: 38px;">
                                                <option value="">---<spring:message code="trailers.label.Select" text="default text"/>---</option>
                                                <c:if test = "${vendorList != null}" >
                                                    <c:forEach items="${vendorList}" var="list">
                                                        <option value='<c:out value="${list.vendorId}" />'><c:out value="${list.vendorName}" /></option>
                                                    </c:forEach >
                                                </c:if>
                                            </select>
                                             <script>
                                                 document.getElementById("vendorId").value ='<c:out value="${vendorId}"/>'
                                             </script>
                                </td>  
                            </tr>
                            <tr>
                                <td></td> 
                                <td></td> 
                                <td><input type="button" class="btn btn-success" name="Search" value="<spring:message code="trailers.label.Action" text="default text"/>Search" onclick="submitPage(this.name);" style="width:100px;height:35px;">
                                </td>  
                                <td></td>  
                            </tr>
                        </table>
                </td>
            </tr>
        </table>
        
            <br>

            <%
                        int index = 1;

            %>


            <c:if test="${vehiclesList == null }" >
                <br>
                <center><font color="red" size="2"><spring:message code="trailers.label.Norecordsfound" text="default text"/>  </font></center>
            </c:if>
            <c:if test="${vehiclesList != null }" >
                 <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                              <th><spring:message code="trailers.label.SNo" text="default text"/></th>
                            <th><spring:message code="trailers.label.VehicleNumber" text="default text"/></th>
                            <th><spring:message code="trailers.label.CompanyName" text="default text"/></th>
                            <th><spring:message code="trailers.label.InsuredName" text="default text"/></th>
                            <th><spring:message code="trailers.label.InsuranceDate" text="default text"/></th>
                            <th><spring:message code="trailers.label.RenewableDate" text="default text"/></th>
                            <th><spring:message code="trailers.label.InsuranceAmount" text="default text"/></th>
                            <th><spring:message code="trailers.label.ElapsedDays" text="default text"/></th>
                            <th><spring:message code="trailers.label.InsuranceAction" text="default text"/></th>
                            
                    </thead>
                    <tbody>
                    <c:forEach items="${vehiclesList}" var="veh" >
                        
                        <tr>
                            <td  height="30" style="padding-left:30px; "> <%= index++%> </td>
                            <td  height="30" style="padding-left:30px; "> <c:out value="${veh.regNo}" /> </td>
                            <td  height="30" style="padding-left:30px; "> <c:out value="${veh.companyId}" /> </td>
                            <td  height="30" style="padding-left:30px; "> <c:out value="${veh.insName}" /> </td>
                            <td  height="30" style="padding-left:30px; "><c:out value="${veh.fromDate1}" /></td>
                            <td  height="30" style="padding-left:30px; "><c:out value="${veh.toDate1}" /></td>
                            <td  height="30" style="padding-left:30px; "><c:out value="${veh.premiunAmount}" /></td>
                            <c:if test="${veh.elapsedDays == null }">
                            <td   height="30" style="padding-left:30px;"><font color="red" size="2"> </font></td>
                            </c:if>
                            <c:if test="${veh.elapsedDays < '0' }">
                            <td   height="30" style="padding-left:30px;"><font color="red" size="2"> <c:out value="${veh.elapsedDays}" /> </font></td>
                            </c:if>
                            <c:if test="${veh.elapsedDays >= '0' }">
                            <td   height="30" style="padding-left:30px;"><font color="green" size="2"> <c:out value="${veh.elapsedDays}" /> </font></td>
                            </c:if>
                            <td>
                                <%--
                            <c:if test="${veh.insuranceid != '0'}">
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&editStatus=1&listId=1'  ><span class="label label-warning">Edit</span> </a>
                                &nbsp;
                                &nbsp;
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&editStatus=0&listId=1'  ><span class="label label-info">View</span> </a>
                                <br>
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&editStatus=3&listId=1'  ><span class="label label-warning">PostInsurance</span> </a>
                                &nbsp;
                                <br>
                                   &nbsp;
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&editStatus=2&listId=1'  ><span class="label label-info">Renew</span> </a>
                            </c:if>
                                --%>
                            <c:if test="${veh.insuranceid == '0'}">
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&listId=<c:out value="${listId}" />&fleetTypeId=1'>
                                <img src="/throttle/images/addlogos.png" height="20px;"/> </a>
                                </c:if>
                            </td>
                           
                            
                        </tr>
                    </c:forEach>
                        </tbody>
                </table>
            </c:if>
                <br>
            <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <br>
         <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="trailers.label.EntriesPerPage" text="default text"/></span>
            </div>
            <div id="navigation" >
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text"><spring:message code="trailers.label.DisplayingPage" text="default text"/> <span id="currentpage"></span><spring:message code="trailers.label.Of" text="default text"/>  <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script type="text/javascript">
        function submitPage(value) {
         
            if (value == 'add') {
                document.viewVehicleDetails.action = '/throttle/addVehiclePage.do?fleetTypeId=1';
                document.viewVehicleDetails.submit();
            } else {
                if (value == 'ExportExcel') {
                  
                    document.viewVehicleDetails.action = '/throttle/vehicleList.do?param=ExportExcel';
                document.viewVehicleDetails.submit();
                } else {
                    document.viewVehicleDetails.action = '/throttle/searchVehiclePage.do?listId=1';
                    document.viewVehicleDetails.submit();
            }
        }
        }


        function setDefaultVals(regNo, typeId, mfrId, usageId, groupId) {

            if (regNo != 'null') {
                document.viewVehicleDetails.regNo.value = regNo;
            }
            if (typeId != 'null') {
                document.viewVehicleDetails.typeId.value = typeId;
            }
            if (mfrId != 'null') {
                document.viewVehicleDetails.mfrId.value = mfrId;
            }
            if (usageId != 'null') {
                document.viewVehicleDetails.usageId.value = usageId;
            }
            if (groupId != 'null') {
                document.viewVehicleDetails.groupId.value = groupId;
            }
        }

//       function getVehicleNos() {
//            //onkeypress='getList(sno,this.id)'
//            var oTextbox = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
//        }

    </script>

</div>
    </div>
    </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>


