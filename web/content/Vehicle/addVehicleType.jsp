<%-- 
    Document   : addVehicleType
    Created on : Feb 26, 2009, 10:27:56 AM
    Author     : karudaiyar Subramaniam
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import="ets.domain.company.business.CompanyTO" %>
<%@ page import="ets.domain.mrs.business.MrsTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <title>PAPL</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>-->
        <script type="text/javascript" src="/throttle/js/jquery-1.10.2.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            /*            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";*/
        </style>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

</head>
<script>
    function submitpage()
    {
     if(textValidation(document.dept.typeName,"Vehicle Type Name")){
          return;
      }
       if(textValidation(document.dept.description,"description")){
         return;
      }
       document.dept.action= "/throttle/addVehicleType.do";
       document.dept.submit();
    }

   
</script>

<script>

  var axletypeId=$("#axletypeId:selected").text();
  alert(axletypeId);
  document.getElementById("axletypeNmae").value = axletypeId;

</script>
<body>
<form name="dept"  method="post" >
    <%@ include file="/content/common/path.jsp" %>
      

<%@ include file="/content/common/message.jsp" %>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="340" id="bg" class="border">
<tr height="30">
                    <td colspan="5" class="contenthead">Add Vehicle Type</td>
                </tr>
<tr>
<td class="text1" height="20"><font color=red>*</font>Vehicle Type Name</td>
<td class="text1" height="20"><input name="typeName" type="text" class="form-control" value=""></td>
</tr>
<tr>
    <td class="text1" height="20"><font color=red>*</font>Axle Type</td>
<td class="text2"  width="20%"><input type="hidden" name="axletypeNmae" id="axletypeNmae"><select class="form-control" name="axletypeId" id="axletypeId"   style="width:125px;">

                                    <option value="0">---Select---</option>
                                    <c:if test = "${AxleList != null}" >
                                        <c:forEach items="${AxleList}" var="Type">
                                            <option value='<c:out value="${Type.vehicleAxleID}" />'><c:out value="${Type.axleTypeName}" /></option>
                                        </c:forEach>
                                    </c:if>
                                </select></td>
</tr>
<tr>
<td class="text1" height="20"><font color=red>*</font>Vehicle Tonnage</td>
<td class="text1" height="20"><input name="tonnage" type="text" class="form-control" value=""></td>
</tr>
<tr>
<td class="text1" height="20"><font color=red>*</font>Vehicle Capacity(CBM)</td>
<td class="text1" height="20"><input name="capacity" type="text" class="form-control" value=""></td>
</tr>
<tr>
<td class="text2" height="20"><font color=red>*</font> Description</td>
<td class="text2" height="20"><textarea class="form-control" name="description"></textarea></td>
</tr>
</table>
<br>
<center>
<input type="button" value="Add" class="button" onclick="submitpage();">
&emsp;<input type="reset" class="button" value="Clear">
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>

