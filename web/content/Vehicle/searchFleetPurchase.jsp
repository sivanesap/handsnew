<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

        <%@page language="java" contentType="text/html; charset=UTF-8"%>
        <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
        <%@page import="java.util.Locale"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        
        <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
        <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
        
           <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display = 'none';
            }

            function show_exp() {
                document.getElementById('exp_table').style.display = 'block';
            }

            function show_close() {
                document.getElementById('exp_table').style.display = 'none';
            }
        </script>
<script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
    </head>

    <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="trailers.label.FleetPurchase"  text="fleetPurchase"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
          <li class="active"><spring:message code="trailers.label.FleetPurchase"  text="fleetPurchase"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">
      <div class="panel-body">
          
    <body onLoad="getVehicleNos();
            setImages(0, 0, 0, 0, 0, 0);
            setDefaultVals('<%= request.getAttribute("regNo")%>', '<%= request.getAttribute("typeId")%>', '<%= request.getAttribute("mfrId")%>', '<%= request.getAttribute("usageId")%>', '<%= request.getAttribute("groupId")%>');">
        <form name="viewVehicleDetails"  method="post" >
            
            <%@ include file="/content/common/message.jsp" %>
            
                <!--<table class="table table-bordered">-->
                <table  class="table table-info mb30 table-hover" >
    <thead>
    
    <th colspan="6"  height="30"><spring:message code="trailers.label.FleetPurchase" text="default text"/> </th>
</thead>
                    <tr >
                        <td><spring:message code="trailers.label.VehicleNumber" text="default text"/></td><td><input type="text" id="regno" name="regno" value='<c:out value="${regNo}" />' style="width:260px;height:40px;"  class="form-control" ></td>

                        <td><spring:message code="trailers.label.CompanyName" text="default text"/></td><td><select style="width:260px;height:40px;"  class="form-control" name="companyId" id="companyId" >
                                <option value="">---<spring:message code="trailers.label.Select" text="default text"/>---</option>
                                <c:if test = "${vendorList != null}" >
                                    <c:forEach items="${vendorList}" var="list">
                                        <option value='<c:out value="${list.vendorId}" />'><c:out value="${list.vendorName}" /></option>
                                    </c:forEach >
                                </c:if>
                            </select>
                             <script>
                                 document.getElementById("companyId").value ='<c:out value="${companyId}"/>'
                             </script>
                        </td>
                    </tr>

                    <tr><td><spring:message code="trailers.label.FromDate" text="default text"/></td>
                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:260px;height:40px;"    onclick="ressetDate(this);" value=""</td>
                        <td><spring:message code="trailers.label.ToDate" text="default text"/></td><td height="30"><input name="toDate" id="toDate" style="width:260px;height:40px;" type="text" class="datepicker" onclick="ressetDate(this);" value=""></td>
                    </tr>
                    <tr>
                    <td colspan="8" align="center" ><input type="button" class="btn btn-success" name="Search" value="<spring:message code="trailers.label.SEARCH" text="default text"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                    </tr>
                    <td></td>
                </table>
                </td>
                            

            <%
                        int index = 1;

            %>


            <c:if test="${fleetPurchaseList == null }" >
                <br>
                <center><font color="red" size="2"><spring:message code="trailers.label.Norecordsfound" text="default text"/>  </font></center>
            </c:if>
            <c:if test="${fleetPurchaseList != null }" >

                    <table class="table table-info mb30 table-hover sortable" id="table" >
                    <thead>
                        <tr height="50">
                            <th  style="width: 10px"><h3><spring:message code="trailers.label.SNo" text="default text"/></h3></th>
                            <th><h3><spring:message code="trailers.label.Type" text="default text"/></h3></th>
                            <th><h3><spring:message code="trailers.label.PlateNumber/TrailerNo" text="default text"/></h3></th>
                            <th><h3><spring:message code="trailers.label.DoorNo/TrailerNo" text="default text"/></h3></th>
                            <th><h3><spring:message code="trailers.label.BoughtFrom" text="default text"/></h3></th>
                            <th><h3><spring:message code="trailers.label.TotalCost" text="default text"/></h3></th>
                            <th><h3><spring:message code="trailers.label.IsitFinanced" text="default text"/>?</h3></th>
                            <th><h3><spring:message code="trailers.label.Banker" text="default text"/></h3></th>
                            <th><h3><spring:message code="trailers.label.Action" text="default text"/></h3></th>
                           
                        </tr>
                    </thead>
                    <tbody>
                    <%
                    String style = "text1";%>
                    <c:forEach items="${fleetPurchaseList}" var="veh" >
                        <%
                                    if ((index % 2) == 0) {
                                        style = "text1";
                                    } else {
                                        style = "text2";
                                    }%>
                        <tr>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "> <%= index++%> </td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "> 
                                <c:if test="${veh.trailerId == 0 }" >
                                    <spring:message code="trailers.label.Truck" text="default text"/>
                                </c:if>
                                <c:if test="${veh.trailerId != 0 }" >
                                    <spring:message code="trailers.label.Trailer" text="default text"/>
                                </c:if>
                            </td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "> <c:out value="${veh.regNo}" /> </td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "> <c:out value="${veh.doorNo}" /> </td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "> <c:out value="${veh.companyId}" /> </td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "> <c:out value="${veh.totalCost}" /> </td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${veh.financeStatus}" /></td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${veh.bankerName}" /></td>
                            
                           
                            <td class="<%= style%>" height="30" style="padding-left:30px; ">
                                <c:if test="${veh.companyId == null }" >
                                    <a href='/throttle/handleFleetPurchase.do?make=<c:out value="${veh.vehicleMake}" />&regNo=<c:out value="${veh.regNo}" />&doorNo=<c:out value="${veh.doorNo}" />&model=<c:out value="${veh.vehicleModel}" />&trailerId=<c:out value="${veh.trailerId}" />&vehicleId=<c:out value="${veh.vehicleId}" />&addStatus=1&fleetTypeId=1'><span class="label label-success"><spring:message code="trailers.label.ADD" text="default text"/> </span></a>
                                </c:if>
                                <c:if test="${veh.companyId != null }" >
                                    <a href='/throttle/handleFleetPurchase.do?make=<c:out value="${veh.vehicleMake}" />&regNo=<c:out value="${veh.regNo}" />&doorNo=<c:out value="${veh.doorNo}" />&model=<c:out value="${veh.vehicleModel}" />&trailerId=<c:out value="${veh.trailerId}" />&vehicleId=<c:out value="${veh.vehicleId}" />&addStatus=0&fleetTypeId=1'><span class="label label-info"><spring:message code="trailers.label.VIEW" text="default text"/> </span></a>
                                </c:if>


                            </td>
                           
                            
                        </tr>
                    </c:forEach>
                        </tbody>
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="trailers.label.EntriesPerPage" text="default text"/></span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text"><spring:message code="trailers.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="trailers.label.Of" text="default text"/> <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 7);
        </script>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script type="text/javascript">
        function submitPage(value) {
         
            if (value == 'add') {
                document.viewVehicleDetails.action = '/throttle/addVehiclePage.do?fleetTypeId=1';
                document.viewVehicleDetails.submit();
            } else {
                if (value == 'ExportExcel') {
                  
                    document.viewVehicleDetails.action = '/throttle/vehicleList.do?param=ExportExcel';
                document.viewVehicleDetails.submit();
                } else {
                    document.viewVehicleDetails.action = '/throttle/searchVehiclePage.do?listId=1';
                    document.viewVehicleDetails.submit();
            }
        }
        }


        function setDefaultVals(regNo, typeId, mfrId, usageId, groupId) {

            if (regNo != 'null') {
                document.viewVehicleDetails.regNo.value = regNo;
            }
            if (typeId != 'null') {
                document.viewVehicleDetails.typeId.value = typeId;
            }
            if (mfrId != 'null') {
                document.viewVehicleDetails.mfrId.value = mfrId;
            }
            if (usageId != 'null') {
                document.viewVehicleDetails.usageId.value = usageId;
            }
            if (groupId != 'null') {
                document.viewVehicleDetails.groupId.value = groupId;
            }
        }

       function getVehicleNos() {
            //onkeypress='getList(sno,this.id)'
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
        }

    </script>
</div>


        </div>
        </div>



<%@ include file="/content/common/NewDesign/settings.jsp" %>
