<%-- 
    Document   : VehicleAccidentDetails
    Created on : 19 Mar, 2012, 4:38:51 PM
    Author     : kannan
--%>

<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>      
    <%@ page import="java.util.*" %>
    <%@ page import="java.http.*" %>
    <title>Vehicle Accident Update</title>                
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
    <script type="text/javascript" src="/throttle/js/suggestions.js"></script>

     <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//alert('hai');
		$( "#datepicker" ).datepicker({
			showOn: "button",
			buttonImage: "calendar.gif",
			buttonImageOnly: true

		});



	});

	$(function() {
	//	alert("cv");
					$( ".datepicker" ).datepicker({

			/*altField: "#alternate",
			altFormat: "DD, d MM, yy"*/
                            changeMonth: true,changeYear: true
		});

	});
</script>
        <script language="javascript">

        function show_src() {
            document.getElementById('exp_table').style.display='none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display='block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display='none';
        }

 function getVehicleNos(){

   // var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
    //getVehicleDetails(document.getElementById("regNo"));
    
}


var httpRequest;
function getVehicleDetails(regNo)
{

 if(regNo != "") {
        var url = "/throttle/getVehicleDetailsInsurance.do?regNo1="+ regNo;
        url = url+"&sino="+Math.random();
        if (window.ActiveXObject)  {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else if (window.XMLHttpRequest)  {
            httpRequest = new XMLHttpRequest();
        }
        httpRequest.open("GET", url, true);
        httpRequest.onreadystatechange = function() { processRequest(); } ;
        httpRequest.send(null);
    }
}


function processRequest()
{
    if (httpRequest.readyState == 4)  {

        if(httpRequest.status == 200) {
            if(httpRequest.responseText.valueOf()!=""){
                var detail = httpRequest.responseText.valueOf();
                var vehicleValues = detail.split("~");
               document.accidentVehicle.vehicleId.value = vehicleValues[0]
               document.accidentVehicle.chassisNo.value = vehicleValues[2]
               document.accidentVehicle.engineNo.value = vehicleValues[2]
               document.accidentVehicle.vehicleMake.value = vehicleValues[4]
               document.accidentVehicle.vehicleModel.value = vehicleValues[5]

            }
        }
        else
        {
        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
        }
    }
}

        </script>

    </head>

    <body onLoad="getVehicleNos();setImages(1,0,0,0,0,0);">
        <form name="accidentVehicle" action="/throttle/saveVehicleAccident.do">
             <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
        <h2 align="center">Vehicle Accident Details</h2>

        <table width="800" align="center" class="table2" cellpadding="0" cellspacing="0">
            <tr>
                <td class="contenthead" colspan="4">Vehicle Details</td>
            </tr>
            <tr>
                <td class="texttitle1">Vehicle No</td>
                <td class="text1"><input type="text" name="regNo" id="regno" class="form-control" onchange="getVehicleDetails(this.value);" /><input type="hidden" name="vehicleId" id="vehicleId" /></td>
                <td class="texttitle1">Make</td>
                <td class="text1"><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" /></td>
            </tr>
            <tr>
                <td class="texttitle2">Model</td>
                <td class="text2"><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" /></td>
                <td class="texttitle2">Usage</td>
                <td class="text2"><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control" /></td>
            </tr>
            <tr>
                <td class="texttitle1">Engine No</td>
                <td class="text1"><input type="text" name="engineNo" id="engineNo" class="form-control" /></td>
                <td class="texttitle1">Chassis No</td>
                <td class="text1"><input type="text" name="chassisNo" id="chassisNo" class="form-control" /></td>
            </tr>

            <tr>
                <td class="contenthead" colspan="4">Accident Details</td>
            </tr>
            <tr>
                <td class="texttitle1">Accident Date</td>
                <td class="text1"><input type="text" name="accidentDate" id="accidentDate" class="datepicker" />&nbsp;<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.accidentVehicle.accidentDate,'dd-mm-yyyy',this)"/></td>
                <td class="texttitle1">Accident Spot</td>
                <td class="text1"><input type="text" name="accidentSpot" id="accidentSpot" class="form-control" /></td>
            </tr>
            <tr>
                <td class="texttitle2">Driver Name</td>
                <td class="text2"><input type="text" name="driverName" id="driverName" class="form-control" /></td>
                <td class="texttitle2">Police Station</td>
                <td class="text2"><input type="text" name="policeStation" id="policeStation" class="form-control" /></td>
            </tr>
            <tr>
                <td class="texttitle1">FIR No</td>
                <td class="text1"><input type="text" name="firNo" id="firNo" class="form-control" /></td>
                <td class="texttitle1">FIR Date</td>
                <td class="text1"><input type="text" name="firDate" id="firDate" class="datepicker" /><!--&nbsp;<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.accidentVehicle.firDate,'dd-mm-yyyy',this)"/> --></td>
            </tr>
            <tr>
                <td class="texttitle2">FIR Prepared By</td>
                <td class="text2"><input type="text" name="firPreparedBy" id="firPreparedBy" class="form-control" /></td>
                <td class="texttitle2">Remarks</td>
                <td class="text2"><input type="text" name="accidentRemarks" id="accidentRemarks" class="form-control" /></td>
                
            </tr>
        </table>
        <br>
        <center>
            Upload Copy of Documents(if Any)&emsp;<input type="file" />
                <br>
                <br>
            <input type="submit" class="button" value=" Update " />
        </center>
    </body>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</html>