<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@page import="java.sql.*,java.text.DecimalFormat" %>


    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        
        
        
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>

        <script type="text/javascript">



            function validate(){
                var errMsg = "";
                var financeStatus = document.VehicleFinance.financeStatus.value;
                var vendorId = document.VehicleFinance.vendorId.value;

                if(vendorId == 0){
                    errMsg = errMsg+"Please select fleet seller / vendor status\n";
                }else if(financeStatus == 0){
                    errMsg = errMsg+"Please select finance status\n";
                }else if(financeStatus == 2){

                    if(isEmpty(document.VehicleFinance.fleetCost.value)){
                        errMsg = errMsg+"Vehicle Registration No is not filled\n";
                    }

                }else {                    
                    if(isEmpty(document.VehicleFinance.fleetCost.value)){
                        errMsg = errMsg+"Vehicle Registration No is not filled\n";
                    }
                    if(isEmpty(document.VehicleFinance.bankId.value)){
                        errMsg = errMsg+"Banker Name is not filled\n";
                    }

                    if(isEmpty(document.VehicleFinance.fleetCost.value)){
                        errMsg = errMsg+"Fleet Cost is not filled\n";
                    }
                    if(isEmpty(document.VehicleFinance.initialAmount.value)){
                        errMsg = errMsg+"Initial Amount is not filled\n";
                    }
                    if(isEmpty(document.VehicleFinance.financeAmount.value)){
                        errMsg = errMsg+"Finance Amount is not filled\n";
                    }
                    if(isEmpty(document.VehicleFinance.processingFee.value)){
                        errMsg = errMsg+"Bank processingFee is not filled\n";
                    }
                    if(isEmpty(document.VehicleFinance.otherCharges.value)){
                        errMsg = errMsg+"Bank otherCharges is not filled\n";
                    }
                    if(isEmpty(document.VehicleFinance.roi.value)){
                        errMsg = errMsg+"Rate Of Interest is not filled\n";
                    }
                    if(isEmpty(document.VehicleFinance.emiMonths.value)){
                        errMsg = errMsg+"EMI Total Months is not filled\n";
                    }
                    if(isEmpty(document.VehicleFinance.emiAmount.value)){
                        errMsg = errMsg+"EMI Amount is not filled\n";
                    }
                    if(isEmpty(document.VehicleFinance.emiStartDate.value)){
                        errMsg = errMsg+"EMI Start Date is not filled\n";
                    }
                    if(isEmpty(document.VehicleFinance.emiEndDate.value)){
                        errMsg = errMsg+"EMI End Date is not filled\n";
                    }
                }

                if(errMsg != "") {
                    alert(errMsg);
                    return false;
                }else {
                    return true;
                }
            }


            


            function submitPage()
            {
                if(validate()){
                    document.VehicleFinance.action = '/throttle/saveFleetPurchase.do';
                    document.VehicleFinance.submit();
                }
            }


            function computeTotalCost(){
                var fleetCost = document.getElementById("fleetCost").value;
                var otherCost = document.getElementById("otherCost").value;
                if(fleetCost == ''){
                    fleetCost = 0;
                }
                if(otherCost == ''){
                    otherCost = 0;
                }
                document.getElementById("totalCost").value = parseFloat(fleetCost) + parseFloat(otherCost);
            }

            function checkFinanceStatus(){
                var status = document.getElementById("financeStatus").value;
                if(status == '1'){
                    document.getElementById("financeDetails").style.display = 'block';
                }else{
                    document.getElementById("financeDetails").style.display = 'none';
                }
            }
        </script>

    <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="general.label.fleetPurchase"  text="fleetPurchase"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
          <li class="active"><spring:message code="general.label.fleetPurchase"  text="fleetPurchase"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">

    <body onLoad="getVehicleNos();document.VehicleInsurance.regno.focus();">
        <form name="VehicleFinance" method="post">
            
            <%@ include file="/content/common/message.jsp"%>
            <!--            <span id="exMsg" style="color: green; text-align: center; font-weight: bold;"></span>-->
            <!--
            <table class="table table-info mb30 table-hover">-->
            <table  class="table table-info mb30 table-hover">

               <thead>
                <tr>
                    <c:if test="${trailerId != 0}">
                        <th colspan="4"><spring:message code="trailers.label.TrailerDetails" text="default text"/></th>
                    </c:if>
                    <c:if test="${trailerId == 0}">
                        <th colspan="4"><spring:message code="trailers.label.TruckDetails" text="default text"/></th>
                    </c:if>
                </tr>
                </thead>
                <tr>
                    <td ><spring:message code="trailers.label.PlateNumber/TrailerNo" text="default text"/></td>
                    <td>
                        <input type="text" name="regNo" id="regno" style="width:260px;height:40px;"  class="form-control" readonly value='<c:out value="${regNo}"/>' autocomplete="off" />
                        
                        <input type="hidden" name="vehicleId" id="vehicleId" value='<c:out value="${vehicleId}"/>' />
                        <input type="hidden" name="trailerId" id="trailerId" value='<c:out value="${trailerId}"/>' />
                    </td>
                    <td ><spring:message code="trailers.label.DoorNo/TrailerNo" text="default text"/></td>
                    <td>
                        <input type="text" name="doorNo" id="doorNo" style="width:260px;height:40px;"  class="form-control" readonly value='<c:out value="${doorNo}"/>' autocomplete="off" />

                    </td>
                </tr>
                <tr>
                    <td ><spring:message code="trailers.label.Make" text="default text"/></td>
                    <td><input type="text" name="vehicleMake" id="vehicleMake" style="width:260px;height:40px;"  class="form-control"  readonly value='<c:out value="${make}"/>' /></td>
                
                    <td><spring:message code="trailers.label.Model" text="default text"/></td>
                    <td><input type="text" name="vehicleModel" id="vehicleModel" readonly style="width:260px;height:40px;"  class="form-control"  readonly value='<c:out value="${model}"/>' /></td>
                </tr>
            </td>
            </table>

            <!--<table class="table table-info mb30 table-hover">-->
              <table  class="table table-info mb30 table-hover">

                                <thead>
                                <tr>
                                    <th colspan="4"><spring:message code="trailers.label.PurchaseDetails" text="default text"/></th>
                                </tr>
                                </thead>
                                <tr>
                                    <td  ><spring:message code="trailers.label.FleetDealer/Vendor" text="default text"/></td>
                                    <td >
                                        <select name="vendorId" id="vendorId" style="width:260px;height:40px;"  class="form-control">
                                        <c:if test="${vendorList != null}">
                                            <option value="0" selected>--<spring:message code="trailers.label.Select" text="default text"/>--</option>
                                            <c:forEach items="${vendorList}" var="vendor">
                                                <option value='<c:out value="${vendor.vendorId}"/>'><c:out value="${vendor.vendorName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                        </select>
                                    </td>
                                    <td  ><spring:message code="trailers.label.FleetCost" text="default text"/></td>
                                    <td ><input type="text" name="fleetCost" id="fleetCost" value="" onblur="computeTotalCost();" style="width:260px;height:40px;"  class="form-control"/></td>
                                </tr>
                                
                                <tr>
                                    <td ><spring:message code="trailers.label.OtherCost" text="default text"/></td>
                                    <td><input type="text" name="otherCost" id="otherCost" value="0" onblur="computeTotalCost();" style="width:260px;height:40px;"  class="form-control" /></td>
                                    <td ><spring:message code="trailers.label.TotalCost" text="default text"/></td>
                                    <td><input type="text" name="totalCost" id="totalCost" readonly  value=""style="width:260px;height:40px;"  class="form-control" /></td>
                                </tr>
                                <tr>
                                    <td ><spring:message code="trailers.label.IsitFinanced" text="default text"/>?</td>
                                    <td><select style="width:260px;height:40px;"  class="form-control" name="financeStatus" id="financeStatus" onchange="checkFinanceStatus();" >
                                            <option value="0" selected >-<spring:message code="trailers.label.Select" text="default text"/>-</option>
                                            <option value="1"><spring:message code="trailers.label.Yes" text="default text"/></option>
                                            <option value="2"><spring:message code="trailers.label.No" text="default text"/></option>
                                        </select>
                                    </td>
                                    <td><spring:message code="trailers.label.Remarks" text="default text"/></td>
                                    <td><textarea name="remarks"  value="" style="width:260px;height:40px;"  class="form-control"></textarea></td>
                                </tr>
              </td>
            </table>
            <div id="financeDetails" style="display:none;">
            <table class="table table-info mb30 table-hover">
                                <thead>
                                <tr>
                                    <th colspan="4"><spring:message code="trailers.label.FinanceDetails" text="default text"/></th>
                                </tr>
                                </thead>
                                <tr>
                                    <td ><spring:message code="trailers.label.Banker" text="default text"/></td>
                                    <td>

                                        <select name="bankId" id="bankId" style="width:260px;height:40px;"  style="width:260px;height:40px;"  class="form-control" onchange="getBankBranch(this.value)" height:20px;">
                                            <c:if test="${primarybankList != null}">
                                                <option value="0" selected>--<spring:message code="trailers.label.Select" text="default text"/>--</option>
                                                <c:forEach items="${primarybankList}" var="bankVar">
                                                    <option value='<c:out value="${bankVar.primaryBankId}"/>'><c:out value="${bankVar.primaryBankName}"/></option>

                                                </c:forEach>
                                            </c:if>
                                            </select>
                                        
                                    </td>
                                    <td colspan="2" >&nbsp;</td>

                                </tr>
                                <tr>
                                    <td><spring:message code="trailers.label.InitialAmount" text="default text"/></td>
                                    <td><input type="text" name="initialAmount" id="initialAmount" value="0" style="width:260px;height:40px;"  class="form-control" /></td>
                                    
                                    <td><spring:message code="trailers.label.FinanceAmount" text="default text"/></td>
                                    <td><input type="text" name="financeAmount" id="financeAmount" value="0" style="width:260px;height:40px;"  class="form-control" /></td>

                                </tr>
                                <tr>
                                    <td><spring:message code="trailers.label.LoanProcessingFee" text="default text"/></td>
                                    <td><input type="text" name="processingFee" id="processingFee" value="0" style="width:260px;height:40px;"  class="form-control" /></td>

                                    <td><spring:message code="trailers.label.OtherCharges" text="default text"/></td>
                                    <td><input type="text" name="otherCharges" id="otherCharges" value="0" style="width:260px;height:40px;"  class="form-control" /></td>

                                </tr>
                                <tr>
                                    
                                    <td ><spring:message code="trailers.label.InterestType" text="default text"/></td>
                                    <td>
                                        <select name="interestType" style="width:260px;height:40px;"  class="form-control">
                                            <option value="Simple Interest"><spring:message code="trailers.label.SimpleInterest" text="default text"/></option>
                                            <option value="Compound Interest"><spring:message code="trailers.label.CompoundInterest" text="default text"/></option>
                                            <option value="Diminishing Balance Interest"><spring:message code="trailers.label.DiminishingBalanceInterest" text="default text"/></option>
                                        </select>
                                    </td>
                                    <td ><spring:message code="trailers.label.RateofInterest" text="default text"/></td>
                                    <td><input type="text" name="roi" id="roi" value="0" style="width:260px;height:40px;"  class="form-control" /></td>
                                </tr>
                                <tr>
                                    <td><spring:message code="trailers.label.EMITenure(Months)" text="default text"/></td>
                                    <td><input type="text" name="emiMonths" id="emiMonths" value="0" style="width:260px;height:40px;"  class="form-control" /></td>
                                    <td><spring:message code="trailers.label.EMIAmount" text="default text"/></td>
                                    <td><input type="text" name="emiAmount" id="emiAmount" value="0" style="width:260px;height:40px;"  class="form-control" /></td>
                                </tr>
                                <tr>
                                    <td ><spring:message code="trailers.label.EMIStartDate" text="default text"/></td>
                                    <td><input type="text" name="emiStartDate" id="emiStartDate" value="" class="form-control datepicker" /><!--&nbsp;<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.VehicleFinance.premiumPaidDate,'dd-mm-yyyy',this)"/>--></td>
                                    <td ><spring:message code="trailers.label.EMIEndDate" text="default text"/></td>
                                    <td><input type="text" name="emiEndDate" id="emiEndDate" value="" class="form-control datepicker" /><!--&nbsp;<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.VehicleFinance.premiumPaidDate,'dd-mm-yyyy',this)"/>--></td>
                                </tr>
                                <tr>
                                    <td><spring:message code="trailers.label.EMIMonthlyPayByDate" text="default text"/></td>
                                    <td>
                                        <select name="emiPayDay" style="width:260px;height:40px;"  style="width:260px;height:40px;"  class="form-control">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="10">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>

                                        </select>
                                    </td>
                                    <td><spring:message code="trailers.label.PayMode" text="default text"/></td>
                                    <td>
                                        <select name="payMode" style="width:260px;height:40px;"  style="width:260px;height:40px;"  class="form-control">
                                            <option value="ECS"><spring:message code="trailers.label.ECS" text="default text"/></option>
                                            <option value="PDC"><spring:message code="trailers.label.PDC" text="default text"/></option>
<!--                                            <option value="PDC">RTGS</option>-->
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            </div>

            


                            <center>
                                <input type="button" value="<spring:message code="trailers.label.SAVE" text="default text"/>" name="generate" id="generate" class="btn btn-success"  class="button" onclick="submitPage();">                                
                            </center>
                            
                            

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
   

</div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>
