<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<head>
    <title>Vehicle Insurance Update</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
    <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
    <script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
    <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>      
    <script src="/throttle/js/jquery.ui.core.js"></script>
  <script type="text/javascript">  

      function alphanumeric_only(e) {

            var keycode;
            if (window.event) keycode = window.event.keyCode;
            else if (event) keycode = event.keyCode;
            else if (e) keycode = e.which;

            else return true;
           if ((keycode >= 47 && keycode <= 57) || (keycode >= 65 && keycode <= 90) || (keycode >= 95 && keycode <= 122)) {

                return true;
            }

            else {
                alert("Please do not use special characters")
                return false;
            }

            return true;
        }

</script>
    
    <script type="text/javascript">
        $(document).ready(function() {
            //alert('hai');
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });



        });

        $(function() {
            //	alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });

        });
    </script>

    <script language="javascript">

        function show_src() {
            document.getElementById('exp_table').style.display = 'none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display = 'block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display = 'none';
        }



        function validate() {
            var errMsg = "";
            if (document.VehicleInsurance.regNo.value) {
                errMsg = errMsg + "Vehicle Registration No is not filled\n";
            }
            if (document.VehicleInsurance.insuranceCompanyName.value) {
                errMsg = errMsg + "Insurance Company is not filled\n";
            }
            if (document.VehicleInsurance.premiumNo.value) {
                errMsg = errMsg + "Premium No is not filled\n";
            }
            if (document.VehicleInsurance.premiumPaidAmount.value) {
                errMsg = errMsg + "Premium paid Amount is not filled\n";
            }
            if (document.VehicleInsurance.premiumPaidDate.value) {
                errMsg = errMsg + "Premium paid Date is not filled\n";
            }
            if (document.VehicleInsurance.vehicleValue.value) {
                errMsg = errMsg + "Vehicle Value is not filled\n";
            }
            if (document.VehicleInsurance.premiumExpiryDate.value) {
                errMsg = errMsg + "Premium expiry Date is not filled\n";
            }
            if (errMsg != "") {
                alert(errMsg);
                return false;
            } else {
                return true;
            }
        }


        var httpRequest;
        function getVehicleDetails(regNo)
        {

            if (regNo != "") {
                var url = "/throttle/getVehicleDetailsInsurance.do?regNo1=" + regNo;
                url = url + "&sino=" + Math.random();
                if (window.ActiveXObject) {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest) {
                    httpRequest = new XMLHttpRequest();
                }
                httpRequest.open("GET", url, true);
                httpRequest.onreadystatechange = function() {
                    processRequest();
                };
                httpRequest.send(null);
            }
        }


        function processRequest()
        {
            if (httpRequest.readyState == 4) {

                if (httpRequest.status == 200) {
                    if (httpRequest.responseText.valueOf() != "") {
                        var detail = httpRequest.responseText.valueOf();
                        var vehicleValues = detail.split("~");
                        document.VehicleInsurance.vehicleId.value = vehicleValues[0]
                        document.VehicleInsurance.chassisNo.value = vehicleValues[2]
                        document.VehicleInsurance.engineNo.value = vehicleValues[2]
                        document.VehicleInsurance.vehicleMake.value = vehicleValues[4]
                        document.VehicleInsurance.vehicleModel.value = vehicleValues[5]

                    }
                }
                else
                {
                    alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                }
            }
        }
    </script>

</head>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Alter Vehicle Permit" text="Alter Vehicle Permit"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Vehicle" text="Vehicle"/></a></li>
            <li class=""><spring:message code="hrms.label.Alter Vehicle Permit" text="Alter Vehicle Permit"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="VehicleInsurance" action="/throttle/alterVehiclePermit.do" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <!--<h2 align="center">Vehicle Permit Details</h2>-->
                    <table width="800" align="center" class="table table-info mb30 table-hover" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th  colspan="4">Vehicle Details</th>
                            </tr>
                        </thead>
                        <c:if test="${vehicleDetail != null}">
                            <c:forEach items="${vehicleDetail}" var="VDet" >
                                <tr>
                                    <td >Vehicle No</td>
                                    <td ><input type="text" name="regNo" id="regNo" onkeypress="return alphanumeric_only(this);" maxlength="12" class="form-control" value='<c:out value="${VDet.regno}" />' readonly style="width:240px;height:40px"    ><input type="hidden" name="vehicleId" id="vehicleId" value='<c:out value="${VDet.vehicleId}" />' readonly ></td>
                                    <td >Make</td>
                                    <td ><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" value='<c:out value="${VDet.mfrName}" />' readonly  style="width:240px;height:40px"   ></td>
                                </tr>
                                <tr>
                                    <td >Model</td>
                                    <td ><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" value='<c:out value="${VDet.modelName}" />' readonly  style="width:240px;height:40px"   ></td>
                                    <td >Usage</td>
                                    <td ><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control" readonly style="width:240px;height:40px" ></td>
                                </tr>
                                <tr>
                                    <td >Engine No</td>
                                    <td ><input type="text" name="engineNo" id="engineNo" class="form-control" value='<c:out value="${VDet.engineNo}" />' readonly  style="width:240px;height:40px"    ></td>
                                    <td >Chassis No</td>
                                    <td ><input type="text" name="chassisNo" id="chassisNo" class="form-control" value='<c:out value="${VDet.chassisNo}" />' readonly  style="width:240px;height:40px"    ></td>
                                </tr>
                            </c:forEach>
                        </c:if>
                        <thead>
                            <tr>
                                <th  colspan="4">Permit Details</th>
                            </tr>
                        </thead>
                        <c:if test="${vehiclePermit != null}">
                            <c:forEach items="${vehiclePermit}" var="VIns" >
                                <tr>
                                    <td >Permit Type</td>
                                    <td >

                                        <select name="permitType" id="permitType" class="form-control"  style="width:240px;height:40px"   >
                                            <c:choose>
                                                <c:when test="${VIns.permitType == 'State permit'}" >
                                                    <option value="State permit" selected>State permit</option>
                                                    <option value="National permit">National permit</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="State permit" >State permit</option>
                                                    <option value="National permit" selected>National permit</option>
                                                </c:otherwise>
                                            </c:choose>
                                        </select>
                                        <input type="hidden" name="permitid" id="permitid" value='<c:out value="${VIns.permitid}" />'  style="width:240px;height:40px"   >
                                    </td>
                                    <td >Permit No</td>
                                    <td ><input type="text" name="permitNo" id="permitNo" class="form-control" value='<c:out value="${VIns.permitNo}" />' style="width:240px;height:40px"   ></td>
                                </tr>
                                <tr>
                                    <td >Permit Paid Date</td>
                                    <td ><input type="text" name="permitPaidDate" id="permitPaidDate" class="datepicker form-control"  value='<c:out value="${VIns.permitPaidDate}" />'  style="width:240px;height:40px"    ></td>
                                    <td >Permit Amount</td>
                                    <td ><input type="text" name="permitAmount" id="permitAmount"  class="form-control"  value='<c:out value="${VIns.permitAmount}" />' style="width:240px;height:40px"    ></td>
                                </tr>
                                <tr>
                                    <td >Permit Expiry Date</td>
                                    <td ><input type="text" name="permitExpiryDate" id="permitExpiryDate" class="datepicker form-control"  value='<c:out value="${VIns.permitExpiryDate}" />' style="width:240px;height:40px"    ></td>
                                    <td >remarks</td>
                                    <td ><input type="text" name="remarks" id="remarks"  class="form-control"  value='<c:out value="${VIns.remarks}" />' style="width:240px;height:40px"    ></td>
                                </tr>

                            </c:forEach>
                        </c:if>
<!--                        <tr>
                            <td align="center"  colspan="4"></td>
                        </tr>-->
                    </table>
                    <br>
                        <table class="table table-info mb30 table-hover"  >  
                            <thead>
                        <tr>
                            <th colspan="2"> Upload Copy Of Bills&emsp;</th>
                        </tr>
                        </thead>
                        <tr>
                        <td align="center"><input type="file" /></td>
                        </tr>
                    </table>
                    <br>
                    <center>
                        <input type="submit"  class="btn btn-success" value=" Update " />
                    </center>
            </body>
            </form>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>  


