<%--
    Document   : VehicleProfitAndLossReport
    Created on : 23 Aug, 2012, 11:00:23 PM
      Author     : Thirukumaran
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="JavaScript" src="/throttle/FusionCharts.js"></script>
        <script type="text/javascript" src="/throttle/ui/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/prettify/prettify.js"></script>
        <script type="text/javascript" src="/throttle/ui/js/json2.js"></script>

 <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>


<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $( "#datepicker" ).datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

    });
});

$(function() {

    $( ".datepicker" ).datepicker({
        /*altField: "#alternate",
        altFormat: "DD, d MM, yy"*/
        changeMonth: true,changeYear: true
    });

});
</script>
<script type="text/javascript">
    function getVehicleNos(){
       var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
}

    function viewReport() {


        document.vehiclePandL.action = "/throttle/viewVehicleProfitAndLossReport.do";
            document.vehiclePandL.method = "post";
            document.vehiclePandL.submit();

    }
</script>
<style type="text/css">
.link {
	font: normal 12px Arial;
	text-transform:uppercase;
	padding-left:10px;
	font-weight:bold;
}

.link a  {
	color:#7f8ba5;
	text-decoration:none;
}

.link a:hover {
		color:#7f8ba5;
	text-decoration:underline;

}

</style>
	<style type="text/css">
            #expand {
                width:100%;
}
	.column { width: 250px; float: left; }
	.portlet {width: 410px; margin: 0 1em 1em 0; border:1px solid #CCCCCC;}
	.portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; cursor:move;font-weight: bold;color:#FFFFFF; background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;height:25px;font-size: 14px; vertical-align: middle; }
	.portlet-header .ui-icon { float: right; }
	.portlet-content { padding: 0.4em; }
	.ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
	.ui-sortable-placeholder * { visibility: hidden; }
	</style>

    </head>
    <body onload="getVehicleNos();">
        <form name="vehiclePandL">

<%

String regno = (String) request.getAttribute("regno");
if(regno == null) {
    regno = "";
}

String fromDate = (String) request.getAttribute("fromDate");
if(fromDate == null){
    fromDate = "";
}

String toDate = (String) request.getAttribute("toDate");
if(toDate == null) {
    toDate = "";
}

%>
        <center>
        <h2>Vehicle Profit and Loss Report</h2>

        </center>

            <table width="90%" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
<!--        <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
        </h2></td>
        <td align="right"><div style="height:17px;margin-top:0px;"><img src="/throttle/images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="/throttle/images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
        </tr>-->
        <tr id="exp_table" >
        <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
            <div class="tabs" align="left" style="width:850;">
        <ul class="tabNavigation">
            <li style="background:#76b3f1">Vehicle P &amp; L Report</li>
	</ul>
        <div id="first">
        <table width="90%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
        <tr>
            <td>Vehicle Number</td><td>
                <input type="text" class="form-control" name="regno" id="regno" value="<%=regno%>" autocomplete="off" />
            </td>

            <td><font color="red">*</font>From Date</td>
            <td><input name="fromDate" id="fromDate" value="<%=fromDate%>"  readonly  class="datepicker"  type="text" value="" size="20">
                </td>
            <td><font color="red">*</font>To Date</td>
            <td><input name="toDate" id="toDate" value="<%=toDate%>" readonly  class="datepicker"  type="text" value="" size="20">
            </td>
            <td colspan="2"><input type="button" name="search" value="View List"  class="button" onclick="viewReport();" /></td>
        </tr>
        </table>
        </div></div>
        </td>
        </tr>
        </table>


        <table width="90%" class="table2" cellspacing="0" align="center">

            <tr><td colspan="4" class="contenthead">Vehicle Details</td></tr>
            <c:if test = "${vehicleProfileList != null}" >
                <c:forEach items="${vehicleProfileList}" var="vp">
            <tr>
                <td class="texttitle1">Vehicle Number</td><td class="text1"><c:out value="${vp.regNo}" /></td>
                <td class="texttitle1">Registration Date</td><td class="text1"><c:out value="${vp.registrationDate}" /></td>
            </tr>
            <tr>
                <td class="texttitle2">Usage</td><td class="text2">Cargo</td>
                <td class="texttitle2">Warranty Period</td><td class="text2"><c:out value="${vp.war_period}" /></td>
            </tr>
            <tr>
                <td class="texttitle1">MFRs</td><td class="text1"><c:out value="${vp.mfrName}" /></td>
                <td class="texttitle1">Vehicle Class</td><td class="text1"><c:out value="${vp.className}" /></td>
            </tr>
            <tr>
                <td class="texttitle2">Model</td><td class="text2"><c:out value="${vp.modelName}" /></td>
                <td class="texttitle2">Seating Capacity</td><td class="text2"><c:out value="${vp.seatCapacity}" /></td>
            </tr>
            <tr>
                <td class="texttitle1">Engine No</td><td class="text1"><c:out value="${vp.engineNo}" /></td>
                <td class="texttitle1">Chassis No</td><td class="text1"><c:out value="${vp.chassisNo}" /></td>
            </tr>
            <tr>
                <td class="texttitle2">Operation Point</td><td class="text2"><c:out value="${vp.companyName}" /></td>
                <td class="texttitle2">Next FC Date</td><td class="text2"><c:out value="${vp.nextFCDate}" /></td>
            </tr>
            <tr>
                <td class="texttitle1">KM Reading</td><td class="text1"><c:out value="${vp.kmReading}" /></td>
                <td class="texttitle1">HM Reading</td><td class="text1"><c:out value="${vp.hmReading}" /></td>
            </tr>
            <tr>
                <td class="texttitle2">Customer</td><td class="text2"><c:out value="${vp.custName}" /></td>
                <td class="texttitle2">Daily Running KM</td><td class="text2"><c:out value="${vp.dailyKm}" /></td>
            </tr>
            <tr>
                <td class="texttitle1">Daily Running HM</td><td class="text1"><c:out value="${vp.dailyHm}" /></td>
                <td class="texttitle1">&nbsp;</td><td class="text1">&nbsp;</td>
            </tr>

        </table>


        <br>

        <table width="90%" class="table2" cellspacing="0" align="center">
            <tr><td colspan="4" class="contenthead">Fixed Expenses</td></tr>
            <tr>
                <td class="texttitle1">Insurance</td><td class="text1"></td>
                <td class="texttitle1">FC Amount</td><td class="text1"></td>
            </tr>


            <tr><td colspan="4" class="contenthead">Operational Expenses</td></tr>
            <tr>
                <td class="texttitle1">IC</td><td class="text1"><c:out value="${vp.premiumPaidAmount}" /></td>
                <td class="texttitle1">FC Cost</td><td class="text1"><c:out value="${vp.fcAmount}" /></td>
            </tr>
            <tr>
                <td class="texttitle2">Road Tax</td><td class="text2"><c:out value="${vp.roadTaxPaidAmount}" /></td>
                <td class="texttitle2">&nbsp;</td><td class="text2">&nbsp;</td>
            </tr>

            <tr><td colspan="4" class="contenthead">Fuel Cost</td></tr>
            <tr>
                <td class="texttitle1">KM Run</td><td class="text1"><c:out value="${vp.tripRunKm}" /></td>
                <td class="texttitle1">Fuel Consumed</td><td class="text1"><c:out value="${vp.fuelLiters}" /></td>
            </tr>
            <tr>
                <td class="texttitle2">Total Fuel Cost</td><td class="text2"><c:out value="${vp.fuelAmount}" /></td>
                <td class="texttitle2">&nbsp;</td><td class="text2">&nbsp;</td>
            </tr>




        </table>


                    </div>
                </td>
            </tr>
        </table>

        <br>
        <%

String serviceCost = "<chart caption='Service Cost' bgColor='FFFFFF,CCCCCC' showPercentageValues='0' plotBorderColor='FFFFFF' numberPrefix='' isSmartLineSlanted='0' showValues='1' showLabels='0' showLegend='1'>"+
                          "<set value='30000' label='Engine'  color='3EA99F' alpha='60'/>"+
                          "<set value='30000' label='Body Works'  color='99CC00' alpha='60'/>"+
                          "<set value='10000' label='Transmission'  color='357EC7' alpha='60'/>"+
                          "<set value='60000' label='Tyres'  color='F535AA' alpha='60'/>"+
                          "<set value='5000' label='Others'  color='FFEA00' alpha='60'/>"+
                          "</chart>";


%>
<!--<div id="expand">
        <center>
            <div class="portlet" >
            <div class="portlet-header" style="width:405px;">&nbsp;&nbsp;Service Cost</div>
                <div class="portlet-content">
                <table align="center"   cellspacing="0px" >
                <tr>
                    <td>
                        <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                            <jsp:param name="chartSWF" value="/throttle/swf/Pie2D.swf" />
                            <jsp:param name="strURL" value="" />
                            <jsp:param name="strXML" value="<%=serviceCost %>" />
                            <jsp:param name="chartId" value="productSales" />
                            <jsp:param name="chartWidth" value="400" />
                            <jsp:param name="chartHeight" value="250" />
                            <jsp:param name="debugMode" value="false" />
                            <jsp:param name="registerWithJS" value="false" />
                        </jsp:include>

                    </td>
                </tr>
                </table>
                </div>
	</div>
        </center>
    </div>-->
</c:forEach></c:if>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
