

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>     
    <link href="/throttle/css/parveen.css" rel="stylesheet"/>
    <%@ page import="ets.domain.mrs.business.MrsTO" %>
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
    <title>RCWO</title>




</head>
<body>


<script>
function submitPage(){
    if( document.mpr.vendorId.value == '0' ){
        alert("Please select Vendor");
        document.mpr.vendorId.focus();
        return;
    }
    if( document.mpr.remarks.value == '' ){
        alert("Please enter remarks");
        document.mpr.remarks.focus();
        return;
    }
    document.mpr.action="/throttle/handleAlterRCWO.do";
    document.mpr.submit();
}            


function cancelWo(){
    
}


</script>

<form name="mpr"  method="post" >                        
    <br>

<% int cntr = 0; %>
    <c:if test="${woDetail != null}" >

    <c:forEach items="${woDetail}" var="work">
      <% if(cntr == 0 ){ %>

    <table width="700" align="center" border="0" cellpadding="0" cellspacing="0"  >
        <tr>
            <Td HEIGHT="30" class="text1" width="175"  > Work order No </td>
            <Td HEIGHT="30"  class="text1" width="175"> <input type="hidden" name="woId" value='<c:out value="${work.woId}" />' > <c:out value="${work.woId}" />  </td>
            <Td HEIGHT="30"  class="text1" width="175"> Vendor Name </td>
            <Td HEIGHT="30"  class="text1" width="175"  >
                <select name="vendorId" class="textbox" >
                    <option value="0" >-Select-</option>
                    <c:if test="${vendorList != null}" >
                        <c:forEach items="${vendorList}" var="vend" >
                            <c:choose>
                                <c:when test="${vend.vendorId == work.vendorId}" >
                                    <option selected value='<c:out value="${vend.vendorId}" />' > <c:out value="${vend.vendorName}" /> </option>
                                </c:when>
                                <c:otherwise>
                                    <option value='<c:out value="${vend.vendorId}" />' > <c:out value="${vend.vendorName}" /> </option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </c:if>
                </select>
            </td>
        </tr>

        <tr>
            <Td HEIGHT="30"  class="text1" width="175"  > Work order Date </td>
            <Td HEIGHT="30"  class="text1" width="175"  >
                <input type="text" name="createdDate" readonly class="textbox" value='<c:out value="${work.createdDate}" />' >
                    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.mpr.createdDate,'dd-mm-yyyy',this)"/>
            </td>
            <Td HEIGHT="30"  class="text1" width="175"  > Remarks </td>
            <Td HEIGHT="30"  class="text1" width="175"  >
             <textarea name="remarks" class="textbox" ><c:out value="${work.remarks}" /></textarea>
            </td>
        </tr>

        <% cntr++; } %>

        </c:forEach>

    </table>
        </c:if>



    <br>
    <br>

    
    <% int indx = 0; %>
    <c:if test="${woDetail != null}" >

    <table width="700" align="center" border="0" cellpadding="0" cellspacing="0"  >
           
        <tr>
        <td class="contentsub" height="30">SNo</td>
                    <td class="contentsub" height="30">MFR Item code</td>
                    <td class="contentsub" height="30">Item Code</td>
                    <td class="contentsub" height="30">Item Name</td>
                    <td class="contentsub" height="30">UOM</td>
                    <td class="contentsub" height="30">Quantity</td>


        </tr>
         
    <c:forEach items="${woDetail}" var="work">        
                    <%
            String classText = "";
            int oddEven = indx % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                    %>
        
        <tr>
            <Td HEIGHT="30" class="<%= classText %>" width="25" align="right" > <%= indx+1 %> </td>
            <Td HEIGHT="30"  class="<%= classText %>" width="90"  align="center"> <input type="text"  class="textbox" name="mfrCode" value='' >  </td>
            <Td HEIGHT="30"  class="<%= classText %>" width="90"  align="center"><input type="text"  class="textbox"  name="itemCode" value='<c:out value="${work.paplCode}" />' >   </td>
            <Td HEIGHT="30"  class="<%= classText %>" width="400" align="left"><input type="text"  class="textbox"  name="itemName" value='<c:out value="${work.itemName}" />' >  </td>
            <Td HEIGHT="30"  class="<%= classText %>" width="60"  align="center"><input name="uom" size='5'  class='textbox'  type='text' value='<c:out value="${work.uomName}" />' >   </td>
            <Td HEIGHT="30"  class="<%= classText %>" width="60"  align="right"><input name="qty" size='5'  class='textbox'  type='text' value='<c:out value="${work.quantity}" />' >  </td>
            
        </tr>
            <% indx++; %>
        </c:forEach>
        
    </table>
        </c:if>     
    <center>   
        <input type="button" class="button" name="Save" value="Save" onClick="submitPage();" >
        <%--<input type="button" class="button" name="Cancel" value="Cancel" onClick="" >--%> 
    </center>

</form>
</body>
</html>
