
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

<%@ page import="java.util.*" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
<title>PAPL</title>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
</head>
<body>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">
function validate(value){
if(isEmpty(document.addGrade.gradeName.value)){
alert("Grade Name Field Should Not Be Empty");
document.addGrade.gradeName.focus();
}
else if(isEmpty(document.addGrade.description.value)){
alert("Description Should Not Be Empty");
document.addGrade.description.focus();
}
else if(isSpecialCharacter(document.addGrade.gradeName.value)){
alert("Special Characters are not Allowed");
document.addGrade.gradeName.focus();
}
else if(value == 'add'){
document.addGrade.gradeName.value = trim(document.addGrade.gradeName.value);
document.addGrade.description.value = trim(document.addGrade.description.value);
document.addGrade.action = '/throttle/AddGrade.do';
document.addGrade.submit();
}
}


</script>

<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

<form name="addGrade"  method="post">
    
    <%--<%@ include file="/content/common/path.jsp" %>--%>
    

<%@ include file="/content/common/message.jsp" %>
    
                    
                    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.AddGrade" text="AddGrade"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="HRMS"/></a></li>
		                    <li class=""><spring:message code="hrms.label.AddGrade" text="AddGrade"/></li>
		
		                </ol>
		            </div>
        </div>
                                    
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">

 <table class="table table-info mb30 table-hover" id="bg" >
     <thead>
<tr>
<th colspan="3" ><div >Designation Name</div></th>
<th  ><div ><%= request.getAttribute("desigName") %></div></th>
<input type="hidden" name="desigName" value="<%= request.getAttribute("desigName") %>" >
</tr>
</thead>
<tr >
<td  ><font color=red>*</font>Grade Name</td>
<td  ><input type="text" name="gradeName" class="form-control" style="width:250px;height:40px"></td>

<td  ><font color=red>*</font>Grade Description</td>
<td  ><textarea  name="description" class="form-control" style="width:250px;height:40px"></textarea></td>
</tr>
<tr>
<td><input type="hidden" name="designationId" value='<%= request.getAttribute("DesigId") %>'></td>
</tr>
</table>

<center>

<input type="button" value="Add" name="add" onClick="validate(this.name)" class="btn btn-success">
&emsp;<input type="reset" class="btn btn-success" value="Clear">
</center>

</form>
</body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>
