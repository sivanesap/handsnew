<%-- 
    Document   : connectPickListItems
    Created on : 5 Aug, 2021, 12:11:49 PM
    Author     : Roger
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<style type="text/css">

</style>
<script type="text/javascript">
    function uploadContract() {
        document.upload.action = "/throttle/uploadconnectInvoice.do";
        document.upload.method = "post";
        document.upload.submit();
    }

    function uploadPage() {
        var qty = document.getElementsByName("totQty");
        var sno = 1;
        var stockQty = 0;
        var count = 0;
        for (var i = 0; i < qty.length; i++) {
            if (document.getElementById("stockQty" + sno).value != "" && document.getElementById("stockQty" + sno).value != null) {
                stockQty = document.getElementById("stockQty" + sno).value;
            }else{
                stockQty = 0;
            }
            if (parseInt(stockQty)<parseInt(qty[i].value)){
                count++;
            }
            sno++;
        }
        if(count==0){
        var result = confirm("Do you want to continue?");
        if (result == true) {
            document.upload.action = "/throttle/connectPicklist.do?&param=save";
            document.upload.method = "post";
            document.upload.submit();
        }
    }else{
        alert("Stock is Insufficient. Please remove those invoices or Add stock Inward");
    }
    }
    function checkSelectStatus(sno, obj, qty, stk) {
        var val = document.getElementsByName("selectedStatus");
//        alert(stk);
        if (parseInt(qty) > parseInt(stk)) {
            alert("Not Enough Stock available");
            obj.checked = false;
        } else {
            if (obj.checked == true) {
                document.getElementById("selectedStatus" + sno).value = 1;
            } else if (obj.checked == false) {
                document.getElementById("selectedStatus" + sno).value = 0;
            }
            var vals = 0;
            for (var i = 0; i <= val.length; i++) {
            }
        }
    }
    function checkAll(element) {
        var checkboxes = document.getElementsByName('selectedIndex');
        if (element.checked == true) {
            for (var x = 0; x < checkboxes.length; x++) {
                checkboxes[x].checked = true;
                $('#selectedStatus' + (x + 1)).val('1');
            }
        } else {
            for (var x = 0; x < checkboxes.length; x++) {
                checkboxes[x].checked = false;
                $('#selectedStatus' + (x + 1)).val('0');
            }
        }
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="PickList Items"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Connect" text="HS Connect"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="PickList Items"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form name="upload"  method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>



                    <div id="ptp">
                        <div class="inpad" style=" overflow-x: visible;">



                            <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                <thead style="font-size:12px">
                                    <tr height="40">
                                        <th  height="30" >S No</th>
                                        <th  height="30" >Dispatch No</th>
                                        <th  height="30" >Product Name</th>
                                        <th  height="30" >Qty</th>
                                        <th  height="30" >Available Stock</th>
                                        <th  height="30" >Material Code</th>
                                        <th height="30">Action <input type="checkbox" name="all" id="all" onclick="checkAll(this)"></th>
                                    </tr>
                                </thead>
                                <% int index = 0; 
                                 int sno = 1;
                                 int count=0;
                                %> 
                                <tbody>

                                    <c:forEach items= "${pickList}" var="pick">
                                        <tr height="30">
                                            <td align="left"><%=sno%></td>
                                            <td align="left"   ><c:out value="${pick.dispatchNo}"/></td>
                                            <td align="left"   ><c:out value="${pick.productName}"/></td>
                                            <td align="left"   ><c:out value="${pick.qty}"/></td>
                                            <td align="left"   ><c:out value="${pick.stockQty}"/></td>
                                            <td align="left"   ><c:out value="${pick.materialCode}"/></td>
                                            <td>
                                                <input type="hidden" name="totQty" id="totQty<%=sno%>" value="<c:out value="${pick.qty}"/>"/>
                                                <input type="hidden" name="stockQty" id="stockQty<%=sno%>" value="<c:out value="${pick.stockQty}"/>"/>
                                                <input type="checkbox" id="selectedIndex<%=sno%>" name="selectedIndex" value="1" checked onclick="checkSelectStatus('<%=sno%>', this, '<c:out value="${pick.qty}"/>', '<c:out value="${pick.stockQty}"/>');">
                                                <input type="hidden" id="selectedStatus<%=sno%>" name="selectedStatus" value="1">
                                                <input type="hidden" id="productId<%=sno%>" name="productId" value="<c:out value="${pick.productId}"/>">
                                                <input type="hidden" id="dispatchId<%=sno%>" name="dispatchId" value="<c:out value="${pick.dispatchId}"/>">
                                                <!--<a href="/throttle/connectPicklist.do?param=pick&dispatchId=<c:out value="${pick.dispatchId}"/>&productId=<c:out value="${pick.productId}"/>&qty=<c:out value="${pick.qty}"/>">Pick Items</a>-->
                                            </td>

                                        </tr> 
                                        <%
                                        sno++;
                                        index++;
                                        %>
                                    </c:forEach>
                                <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                </tbody>
                            </table>

                            <center>
                                <td colspan="0"><input type="button" class="btn btn-success" value="Submit" name="submitPage" id="submitPage" onclick="uploadPage();"></td>
                            </center>

                        </div>
                    </div>     

                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>


