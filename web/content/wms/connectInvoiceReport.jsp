<%-- 
    Document   : connectInvoiceReport
    Created on : 2 Sep, 2021, 11:08:36 AM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Invoice Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You Are Here</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="general-forms.html">Connect</a></li>
            <li class="">Invoice Report</li>

        </ol>
    </div>
</div>
<script>
    function showProducts(invoiceId) {
        open("/throttle/connectInvoiceReport.do?&param=view&invoiceId=" + invoiceId, "PopupPage", "height=1000, width=1000");
    }
    function exportExcel() {
        document.invoiceSearch.action = "/throttle/connectInvoiceReport.do?param=ExportExcel";
        document.invoiceSearch.submit();
    }
    function searchPage() {
        document.invoiceSearch.action = "/throttle/connectInvoiceReport.do?param=search";
        document.invoiceSearch.submit();
    }
    function cancelInvoice(invoiceId) {
        document.invoiceSearch.action = "/throttle/connectInvoiceReport.do?param=cancel&invoiceId=" + invoiceId;
        document.invoiceSearch.submit();
    }
    function updateAppointment(invoiceId, sno) {
        var date = document.getElementById("appointmentDate" + sno).value;
        if (date == "") {
            alert("Choose Appointment Date");
        } else {
            document.invoiceSearch.action = "/throttle/connectInvoiceReport.do?param=update&invoiceId=" + invoiceId + "&appointment=" + date;
            document.invoiceSearch.submit();
        }
    }
</script>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="invoiceSearch" method="post">
                    <table class="table table-info mb30 table-hover" id="table1">
                        <tr>
                            <td>From Date</td>
                            <td><input type="text" autocomplete="off" class="form-control datepicker" id="fromDate" name="fromDate" value="<c:out value="${fromDate}"/>" style="width:240px;height:40px"/></td>
                            <td>To Date</td>
                            <td><input type="text" autocomplete="off" class="form-control datepicker" id="toDate" name="toDate" value="<c:out value="${toDate}"/>" style="width:240px;height:40px"/></td>
                        </tr>
                        <tr>
                            <td>Plant</td>
                            <td>
                                <select id="plantCode" name="plantCode" class="form-control" style="width:240px;height:40px">
                                    <option value="">------Select Warehouse------</option>
                                    <c:forEach items="${whList}" var="wh">
                                        <option value="<c:out value="${wh.whId}"/>"><c:out value="${wh.whName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>Status</td>
                            <td>
                                <select id="newStatus" name="newStatus" class="form-control" style="width:240px;height:40px">
                                    <option value="">------Select Status------</option>
                                    <option value="1">Completed</option>
                                    <option value="2">Pending</option>
                                    <option value="3">Cancelled</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <center>
                        <input type="button" class="btn btn-success" id="search" name="search" value="Search" onclick="searchPage()"/>&emsp;
                        <input type="button" class="btn btn-success" id="ExportExcel" onclick="exportExcel()" name="ExportExcel" value="Export Excel"/>
                    </center>
                    <br><br>
                    <table id="table" class="table table-info mb30 table-hover">
                        <thead>
                        <th>S. No</th>
                        <th>Plant Code</th>
                        <th>Customer</th>
                        <th>City</th>
                        <th>Pincode</th>
                        <th>Invoice Date</th>
                        <th>Invoice Number</th>
                        <th>Pre-Tax Value</th>
                        <th>Total Qty</th>
                        <th>Status</th>
                        <th>LR Number</th>
                        <th>Details</th>
                        <th>POD Status</th>
                        <th>Appointment Date</th>
                        <th>Cancel Invoice</th>
                        </thead>
                        <%int sno = 1;%>
                        <tbody>
                            <c:forEach items="${connectInvoiceReport}" var="in">
                                <tr>
                                    <td><%=sno%></td>
                                    <td><c:out value="${in.plantCode}"/></td>
                                    <td><c:out value="${in.customerName}"/></td>
                                    <td><c:out value="${in.city}"/></td>
                                    <td><c:out value="${in.pincode}"/></td>
                                    <td><c:out value="${in.invoiceDate}"/></td>
                                    <td><c:out value="${in.invoiceNo}"/></td>
                                    <td><c:out value="${in.preTaxValue}"/></td>
                                    <td><c:out value="${in.qty}"/></td>
                                    <td><c:out value="${in.status}"/></td>
                                    <td><c:out value="${in.lrNo}"/></td>
                                    <td>
                                        <span class="label label-success" style="cursor: pointer" onclick="showProducts(<c:out value="${in.invoiceId}"/>)">View Products</span>
                                    </td>
                                    <td>
                                        <c:if test="${in.podStatus=='0'}">
                                            Pending
                                        </c:if>
                                        <c:if test="${in.podStatus=='1'}">
                                            Approval Pending
                                        </c:if>
                                        <c:if test="${in.podStatus=='2'}">
                                            Approved
                                        </c:if>
                                        <c:if test="${in.podStatus=='3'}">
                                            Rejected
                                        </c:if>
                                    </td>
                                    <td align="center" >
                                        <c:if test="${in.appointmentDate!=null && in.appointmentDate != ''}">
                                            <c:out value="${in.appointmentDate}"/>
                                        </c:if>
                                        <c:if test="${in.appointmentDate==null || in.appointmentDate == ''}">
                                            <input type="text" autocomplete="off" class="datepicker form-control" name="appointmentDate" id="appointmentDate<%=sno%>" style="width:130px;height:30px"/>
                                            <br><span class="label label-Primary" style="cursor: pointer" onclick="updateAppointment('<c:out value="${in.invoiceId}"/>', '<%=sno%>')">Save</span>
                                        </c:if>
                                    </td>
                                    <td>
                                        <c:if test="${in.status!='Cancelled' && (in.lrNo=='' || in.lrNo==null)}">
                                            <span class="label label-success" style="cursor: pointer" onclick="cancelInvoice(<c:out value="${in.invoiceId}"/>)">Cancel</span>
                                        </c:if>
                                        <c:if test="${in.status=='Cancelled'}">
                                            Already Cancelled
                                        </c:if>
                                    </td>
                                </tr>
                                <%sno++;%>
                            </c:forEach>
                        </tbody>
                    </table>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
