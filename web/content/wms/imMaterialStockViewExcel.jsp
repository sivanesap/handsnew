<%-- 
    Document   : imMaterialStockViewExcel
    Created on : 22 Mar, 2022, 11:55:49 AM
    Author     : mahendiran
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
    <form name="tripSheet" method="post">
        <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "MaterialStockView-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

            <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                 <thead>

                                <tr height="30" style="color: white">
                                    <th>S.No</th>
                                    <th>Material Name</th>
                                    <th>Material Code</th>
                                    <th>Material Description</th>
                                    <th>Stock Qty</th>
                                    <th>Floor Qty</th>
                                    <th>Used Qty</th>
                                    <th>Warehouse</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                            <c:if test = "${stockView != null}">
                                <c:forEach items="${stockView}" var="pc">
                                    <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>

                                  <tr>
                                    <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.material}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.materialCode}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.materialDescription}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.qty}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.floorQty}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.usedQty}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.whName}" /></td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </c:if>
                        </table>
   </form>
</body>
</html>
