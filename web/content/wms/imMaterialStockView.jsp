<%-- 
    Document   : imMaterialStockView
    Created on : 10 Nov, 2021, 10:53:36 AM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script>
                        function submitPage() {
                            var id = document.getElementById("whId").value;
                            document.material.action = '/throttle/imMaterialStockView.do?param=search';                           
                            document.material.submit();
                        }
                        function savePage() {
                            document.material.action = '/throttle/imMaterialStockView.do?param=excel';
                            document.material.submit();
                        }
                        
                    </script>                              
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Material Stock View</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html">InstaMart</a></li>
                <li class="">Material Stock View</li>

            </ol>
        </div>
    </div>
                
 
  
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="material"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        
                        
                        
                       <table class="table table-info mb30 table-hover" id="pincode">
                        <tr>
                            <td>Warehouse</td>
                            <td>
                                <select id="whId" name="whId" style="width:240px;height:40px" class="form-control">
                                    <option value="">------Select Warehouse------</option>
                                    <c:forEach items="${getWareHouseList}" var="wh">
                                        <option value="<c:out value="${wh.warehouseId}"/>"><c:out value="${wh.whName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <script>
                        document.getElementById("whId").value='<c:out value="${whId}"/>'
                    </script>
                    </table>  
                     <table>
                        <center>
                            <input type="button" class="btn btn-success" name="Search"  id="Search"  value="Search" onclick="submitPage()">&nbsp;&nbsp;   
                            <input type="button" value="Export Excel" id="Export" class="btn btn-success" onclick="savePage()">&nbsp;&nbsp;
                        </center><br><br>

                    </table>


                     <c:if test = "${stockView != null}">
                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30" style="color: white">
                                    <th>S.No</th>
                                    <th>Material Name</th>
                                    <th>Material Code</th>
                                    <th>Material Description</th>
                                    <th>Stock Qty</th>
                                    <th>Floor Qty</th>
                                    <th>Used Qty</th>
                                    <th>Warehouse</th>
                                </tr>
                            </thead>
                            


                                <% int sno = 0;%>
                                <tbody>
                            
                                <c:forEach items="${stockView}" var="pc">
                                    <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>

                                  <tr>
                                    <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.material}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.materialCode}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.materialDescription}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.qty}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.floorQty}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.usedQty}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.whName}" /></td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </c:if>
                       
                                
                        </table>


                      

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                                        setFilterGrid("table");
                        </script>
                       

                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>