<%-- 
    Document   : grnDetails
    Created on : Jan 21, 2021, 12:47:26 PM
    Author     : throttle
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Barcode Master"  text="Barcode Master"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Barcode Master"  text="Master"/></a></li>
            <li class="active">Barcode Master</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="setBarcode('<c:out value="${barcodeMaster}"/>');">
                <form name="saveGtnDetails" method="post">
                <table class="table table-info mb30 table-hover" id="serial">
                      <thead><tr>
                    <th class="contenthead" colspan="8">Barcode Master</th>
                          </tr></thead>          
                    <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>
                       <tbody>
                            
                        <tr>
                           <td align="center"><text style="color:red">*</text>
                                Customer Name
                            </td>
                            <td><select id="custId" name="custId" style="width:200px;height:30px" onchange="getItemList(this.value);">
                              <option value="">---Select One ---</option>    <c:forEach items="${custList}" var="cust">
                                      <option value="<c:out value="${cust.custId}"/>"><c:out value="${cust.custName}"/></option>
                            </c:forEach>
                              </select></td>
                            <td align="center"><text style="color:red">*</text>
                                Product Code
                            </td>
                            <td><select id="itemId" name="itemId" style="width:200px;height:30px">
                              
                              </select></td>
                            
                            </tr>
                             <tr>
                            <td align="center"><text style="color:red">*</text>
                                Barcode
                            </td>
                            <td>
                                <input type="text" id="barCode" name="barCode" value="" style="width:200px;height:30px"/> 
                                <input type="hidden" id="barCodeNo" name="barCodeNo" value=""/> 
                            </td>
                            <td align="center"><text style="color:red">*</text>Count</td>
                            <td height="30"><input name="count" id="count" type="text" style="width:200px;height:30px"  onclick="ressetDate(this);" value="" autocomplete="off"></td>
                            
                        </tr>
                        </tbody>
                    </table>  
                     <c:if test="${barCodeDetails!=null}">
                 <table class="table table-info mb30 table-bordered" id="table" >
                        <thead >
                           
                            <tr >
                                <th>S.No</th>
                                <th>Barcode</th>
                                <th>Item Name</th>
                                <th>Customer Name</th>
                                
                            </tr>

                        </thead>
                            <% int index = 0;
                                    int sno = 1;
                            %>
                            <tbody>

                                <c:forEach items="${barCodeDetails}" var="mh">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td ><%=sno%></td>
                                        <td><c:out value="${mh.barcode}"/></td>
                                        <td><c:out value="${mh.itemName}"/></td>
                                        <td><c:out value="${mh.custName}"/></td>
                                        
                                        </tr>

                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach> 
                                    </tr>
                            </tbody>
                            
                    </table> 
                            </c:if>
                                    
 <script>
   
   function getItemList(value){
    $.ajax({
                                        url: "/throttle/getItemList.do",
                                        dataType: "json",
                                        data: {
                                            custId: value
                                        },
                                        success: function(data) {
                                            if (data != '') {
                                                $('#itemId').empty();
                                                $('#itemId').append(
                                                        $('<option></option>').val(0).html('--select--'))
                                                $.each(data, function(i, data) {
                                                    $('#itemId').append(
                                                            $('<option style="width:90px"></option>').attr("value", data.Id).text(data.Name)
                                                            )
                                                });
                                            } else {
                                                $('#itemId').empty();
                                                $('#itemId').append(
                                                        $('<option></option>').val(0).html('--select--'))
                                            }
                                        }
                                    });
       
   }
     function submitPage(value) {
         if(document.getElementById("barCode").value==""){
             alert("Enter First Barcode");
             document.getElementById("barCode").focus();
         }else if(document.getElementById("count").value==""){
             alert("Enter Barcode Count");
             document.getElementById("count").focus();
         }else if(document.getElementById("itemId").value==""){
             alert("Select Item");
             document.getElementById("itemId").focus();
         }else if(document.getElementById("custId").value==""){
             alert("Select Customer Name");
             document.getElementById("custId").focus();
         }else{
            document.saveGtnDetails.action = '/throttle/addBarcodeMaster.do?param=Generate';
            document.saveGtnDetails.submit();
        }
    }
    function setBarcode(barcode){
        document.getElementById("barCode").value='BAR/2021/'+barcode;
        document.getElementById("barCodeNo").value=barcode;
    }
    function searchPage(){
        var custId = document.getElementById("custId").value;
        var itemId = document.getElementById("itemId").value;
        document.saveGtnDetails.action = '/throttle/addBarcodeMaster.do?param=Search&itemId='+itemId+'&custId='+custId;
        document.saveGtnDetails.submit();
        
    }
    
    
                </script>
                            
                       <table>
                            <center>
                                <input type="button" value="Generate" id="insert" class="btn btn-success" onClick="submitPage(this.value);">
                                &emsp;<input type="button" id="search" class="btn btn-success" value="Search" onclick="searchPage()">
                            </center>

                        </table>
               </form>
        </body>
                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
                   
        </div>
    </div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

