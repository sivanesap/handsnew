<%-- 
    Document   : rpStockReceiveConfirm
    Created on : 23 Sep, 2021, 4:43:44 PM
    Author     : alan
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>
<link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>

<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Stock Receive </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Repose</a></li>
                <li class="">Stock Receive</li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body >
                    <form name="connect"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>



                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30">
                                    <th>S.No</th>
                                    <th>Transfer No</th>
                                    <th>From Warehouse Name</th>
                                    <th>To Warehouse Name</th>
                                    <th>Vehicle No</th>
                                    <th>Driver Name</th>
                                    <th>Driver Mobile</th>
                                    <th>Quantity</th>

                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                                <c:if test = "${rpStockReceive != null}">
                                    <c:forEach items="${rpStockReceive}" var="pc">
                                        <%
                                            sno++;
                                            String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.transferNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.fromWhName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.toWhName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.vehicleNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.driverName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.mobileNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.qty}" /></td>
                                    <input type="hidden" name="transferId" id="transferId" value='<c:out value="${pc.transferId}"/>'/>
                                    <input type="hidden" name="fromWhId" id="fromWhId" value='<c:out value="${pc.fromWhId}"/>'/>
                                    <input type="hidden" name="toWhId" id="toWhId" value='<c:out value="${pc.toWhId}"/>'/>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </c:if>
                        </table>
                        <br>
                        <br>
                        <br>
                        <br>
                        <table  class="table table-info mb30 table-hover" id="table2" >
                            <tr>
                                <td><b>Scan Serial No: </b></td>
                                <td>
                                    <input type="text" id="serialScan" name="serialScan" onchange="checkSerialNo(this.value)" class="form-control" style="width:240px;height:40px"/>
                                </td>
                            </tr>
                        </table>

                        <table class="table table-info mb30 table-hover">

                            <tr style="display:none">

                                <td>
                                    <input type="text" id="test" name="test" value="" class="form-control" style="width:250px" onkeypress="return RestrictSpace()" onchange="checkExists(this.value);"/> 
                                </td>
                            </tr>
                        </table>

                        <table class="table table-info mb30 table-hover" id="table1" >	
                            <thead>
                                <tr height="30">
                                    <th>S.No</th>
                                    <th>Product Name</th>
                                    <th>Serial No</th>
                                    <th>Qty</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno1 = 0;%>
                                <c:if test = "${rpStockReceiveView != null}">
                                    <c:forEach items="${rpStockReceiveView}" var="pc">
                                        <%
                                            sno1++;
                                            String className = "text1";
                                            if ((sno1 % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%= sno1%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.productName}" /></td>
                                            <td class="<%=className%>"  align="left"> xxxxxxxxxxxxxx</td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.qty}" /></td>
                                            <td class="<%=className%>"  align="left"><input type="checkbox" disabled name="selectedIndex" id="selectedIndex<%=sno1%>">
                                                <input type="hidden" value="0" name="selectedStatus" id="selectedStatus<%=sno1%>"/>
                                                <input type="hidden" value="<c:out value="${pc.serialNo}" />" name="serialNo" id="serialNo<%=sno1%>"/>
                                                <input type="hidden" value="<c:out value="${pc.serialId}" />" name="serialId" id="serialId<%=sno1%>"/>
                                                <input type="hidden" value="<c:out value="${pc.productId}" />" name="productId" id="productId<%=sno1%>"/>
                                                <input type="hidden" value="<c:out value="${pc.transferDetailId}" />" name="transferDetailId" id="transferDetailId<%=sno1%>"/>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>
                        <center>
                            <input type="button" name="confirm" id="confirm" value="Save" class="btn btn-success" onclick="savePage()"/>
                        </center>
                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                            setFilterGrid("table1");
                        </script>
                        <script>
//                            $("#serial").keypress(function(event) {
//                                if (event.keyCode == 13) {
//
//                                }
//                            });
                            function savePage() {
                                var sel = document.getElementsByName("selectedIndex");
                                var y = 0;
                                for (var i = 0; i < sel.length; i++) {
                                    if (sel[i].checked == false) {
                                        y = 1;
                                    }
                                }
                                if (y != 0) {
                                    alert("Scan All the Serial");
                                } else {
                                    document.connect.action = "/throttle/rpStockReceive.do?param=save";
                                    document.connect.submit();
                                }
                            }
                            function checkSerialNo(value) {
                                var serialNos = document.getElementsByName("serialNo");
                                var x = 0;
                                for (var i = 0; i < serialNos.length; i++) {
                                    if (serialNos[i].value == value) {
                                        x = parseInt(i) + 1;
                                    }
                                }
                                if (x != 0) {
                                    if (document.getElementById("selectedIndex" + x).checked == true) {
                                        document.getElementById("serialScan").value = "";
                                    } else {
                                        document.getElementById("selectedIndex" + x).checked = true;
                                        document.getElementById("selectedStatus" + x).value = 1;
                                        document.getElementById("serialScan").value = "";
                                    }
                                } else {
                                    alert("Wrong Serial No");
                                    document.getElementById("serialScan").value = "";
                                }
                            }
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>