<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>


<script type="text/javascript">

    function alphanumeric_only(e) {

        var keycode;
        if (window.event)
            keycode = window.event.keyCode;
        else if (event)
            keycode = event.keyCode;
        else if (e)
            keycode = e.which;

        else
            return true;
        if ((keycode >= 47 && keycode <= 57) || (keycode >= 65 && keycode <= 90) || (keycode >= 95 && keycode <= 122)) {

            return true;
        }

        else {
            alert("Please do not use special characters")
            return false;
        }

        return true;
    }

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $(function() {
            $("#fromDate").datepicker({
                defaultDate: "-d",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd-mm-yy",
//                                        maxDate:"-d",
                onClose: function(selectedDate) {
                    var d = selectedDate.split("-");
                    var da = d[0];
                    var mo = d[1];
                    var yr = parseInt(d[2]) + 1;
                    var nd = da + "-" + mo + "-" + yr;
                    $("#toDate").datepicker("setDate", nd);
                }
            });
            $("#toDate").datepicker({
                changeMonth: true,
                dateFormat: "dd-mm-yy",
            });
        });
    });
</script>

<script>
    $(document).ready(function() {
        $(function() {
            $("#fcDate").datepicker({
                defaultDate: "-d",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd-mm-yy",
//                                        maxDate:"-d",
                onClose: function(selectedDate) {
                    var d = selectedDate.split("-");
                    var da = d[0];
                    var mo = d[1];
                    var yr = parseInt(d[2]) + 1;
                    var nd = da + "-" + mo + "-" + yr;
                    $("#fcExpiryDate").datepicker("setDate", nd);
                }
            });
            $("#fcExpiryDate").datepicker({
                changeMonth: true,
                dateFormat: "dd-mm-yy",
            });
        });
    });
</script>

<script>
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });

    });

    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true,
            dateFormat: 'dd-mm-yy'
        });

    });


</script>

<script language="javascript">

    function CalWarrantyDate(selDate) {

        var seleDate = selDate.split('-');
        var dd = seleDate[0];
        var mm = seleDate[1];
        var yyyy = seleDate[2];

        var today = new Date();
        var dd1 = today.getDate();
        var mm1 = today.getMonth() + 1; //January is 0!
        var yyyy1 = today.getFullYear();

        //                if(dd<10){dd='0'+dd}if(mm<10){mm='0'+mm}today = mm+'-'+dd+'-'+yyyy;alert("today"+today);alert("value"+value);

        var selecedDate = new Date(yyyy, mm, dd);
        var currentDate = new Date(yyyy1, mm1, dd1);
        var Days = Math.floor((selecedDate.getTime() - currentDate.getTime()) / (1000 * 60 * 60 * 24));
        //alert("DAYS--"+Days);
        document.addVehicle.war_period.value = Days;

    }



    function submitPage2(value) {
        if (value == "Save") {
            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }

    }
    function submitPage(value) {
        if (value == "save") {
            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }
    }
    function submitPageDedicate(value) {

        var owner = document.getElementById("asset").value;
        document.getElementById("asset").value = owner;

        if (validate() == 'fail') {
            return;
        } else if (document.addVehicle.vendorId.value == '') {
            alert('Please Enter Vendor name');
            document.addVehicle.vendorId.focus();
            return;
        } else if (document.addVehicle.regNo.value == '') {
            alert('Please Enter Vehicle Registration Number');
            document.addVehicle.regNo.focus();
            return;
        } else if (document.addVehicle.regNoCheck.value == 'exists') {
            alert('Vehicle RegNo already Exists');
            return;
            //                }else if(isSelect(document.addVehicle.usageId,'Usage Type')){
            //                    return;

        } else if (isSelect(document.addVehicle.mfrId, 'Manufacturer')) {
            return;
        } else if (isSelect(document.addVehicle.modelId, 'Vehicle Model')) {
            return;
        } else if (isSelect(document.addVehicle.typeId, 'Vehicle type')) {
            return;
        }
        if (value == "save") {

            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }
    }
    function submitPage1(value) {

        var owner = document.getElementById("asset").value;
        document.getElementById("asset").value = owner;

        if (validate() == 'fail') {
            return;
        } else if (document.addVehicle.vendorId.value == '') {
            alert('Please Enter Vendor name');
            document.addVehicle.vendorId.focus();
            return;
        } else if (document.addVehicle.regNo.value == '') {
            alert('Please Enter Vehicle Registration Number');
            document.addVehicle.regNo.focus();
            return;
        } else if (document.addVehicle.regNoCheck.value == 'exists') {
            alert('Vehicle RegNo already Exists');
            return;
            //                }else if(isSelect(document.addVehicle.usageId,'Usage Type')){
            //                    return;

        } else if (isSelect(document.addVehicle.mfrId, 'Manufacturer')) {
            return;
        } else if (isSelect(document.addVehicle.modelId, 'Vehicle Model')) {
            return;
        } else if (isSelect(document.addVehicle.typeId, 'Vehicle type')) {
            return;
        }

        if (value == "save1") {
            // alert("cgsd77f");
            //                    document.addVehicle.nextFCDate.value = dateFormat(document.addVehicle.nextFCDate);
            //     document.addVehicle.dateOfSale.value = dateFormat(document.addVehicle.dateOfSale);
            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }
    }


    var httpRequest;
    function getVehicleDetails() {
        var vehicleId = $("#vehicleId").val();
        if (document.addVehicle.regNo.value != '' && vehicleId == '') {
            var url = '/throttle/checkVehicleExists.do?regno=' + document.addVehicle.regNo.value;

            if (window.ActiveXObject)
            {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("POST", url, true);
            httpRequest.onreadystatechange = function() {
                go1();
            };
            httpRequest.send(null);
        }
    }


    function go1() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var response = httpRequest.responseText;
                var temp = response.split('-');
                if (response != "") {
                    alert('Vehicle Already Exists');
                    document.getElementById("StatusMsg").innerHTML = httpRequest.responseText.valueOf() + " Already Exists";
                    document.addVehicle.regNo.focus();
                    document.addVehicle.regNo.select();
                    document.addVehicle.regNoCheck.value = 'exists';
                } else
                {
                    document.addVehicle.regNoCheck.value = 'Notexists';
                    document.getElementById("StatusMsg").innerHTML = "";
                }
            }
        }
    }

</script>

<script>

    function deleteRows(sno) {
        if (sno != 1) {
            rowCount--;
            document.getElementById("addTyres").deleteRow(sno);
        } else {
            alert("Can't Delete The Row");
        }
    }

    var httpReq;
    var temp = "";
    function getVehicleType(mfrId) { //alert(str);
        var fleetTypeId = $("#fleetTypeId").val();
        $.ajax({
            url: "/throttle/getMfrVehicleType.do",
            dataType: "json",
            data: {
                mfrId: mfrId,
                fleetTypeId: fleetTypeId
            },
            success: function(temp) {
                // alert(data);
                if (temp != '') {
                    $('#vehicleTypeId').empty();
                    $('#vehicleTypeId').append(
                            $('<option style="width:150px"></option>').val(0).html('--Select--')
                            )
                    $.each(temp, function(i, data) {
                        $('#vehicleTypeId').append(
                                $('<option style="width:150px"></option>').val(data.Id + "~" + data.AxleTypeId).html(data.Name)
                                )
                    });
                } else {
                    $('#vehicleTypeId').empty();
                }
            }
        });

    }



    function getModelAndTonnage(str) {
        var fleetTypeId = $("#fleetTypeId").val();
        var vehicleTypeId = str.split("~")[0];
        document.getElementById("typeId").value = vehicleTypeId;
        var fleetTypeId = $("#fleetTypeId").val();
        $.ajax({
            url: "/throttle/getModels1.do",
            dataType: "text",
            data: {
                typeId: vehicleTypeId,
                mfrId: document.addVehicle.mfrId.value,
                fleetTypeId: fleetTypeId
            },
            success: function(temp) {
                // alert(data);
                if (temp != '') {
                    setOptions1(temp, document.addVehicle.modelId);
                    document.getElementById("modelId").value = '<c:out value="${modelId}"/>';
                    getVehAxleName(str);
                } else {
                    setOptions1(temp, document.addVehicle.modelId);
                    alert('There is no model based on Vehicle Type Please add first');
                }
            }
        });
        // for tonnage now
        $.ajax({
            url: "/throttle/getTonnage.do",
            dataType: "text",
            data: {
                typeId: vehicleTypeId,
                fleetTypeId: fleetTypeId
            },
            success: function(temp) {
                if (temp != '') {
                    setOptions2(temp, document.addVehicle.seatCapacity);
                } else {

                    alert('Tonnage is not there please set first in vehicle type');
                }
            }
        });
    }


    function setOptions1(text, variab) {
//                                                                alert("setOptions on page")
        //
//                                                                                      alert("variab = "+variab)
        variab.options.length = 0;
        //                                alert("1")
        var option0 = new Option("--select--", '0');
        //                                alert("2")
        variab.options[0] = option0;
        //                                alert("3")


        if (text != "") {
//                                                                        alert("inside the condition")
            var splt = text.split('~');
            var temp1;
            variab.options[0] = option0;
            for (var i = 0; i < splt.length; i++) {
//                                                                            alert("splt.length ="+splt.length)
//                                                                            alert("for loop ="+splt[i])
                temp1 = splt[i].split('-');
                option0 = new Option(temp1[1], temp1[0])
//                                        alert("option0 ="+option0)
                variab.options[i + 1] = option0;
            }
        }
    }

    function setOptions2(text) {
        //                                alert("setOptions on page")
        //                               alert("text = "+text)
        if (text != "") {
            //                                        alert(text);
            document.getElementById('seatCapacity').value = text;

        }
    }


    function getInsCompanyName(val) {

        var issueCmpId = document.getElementById("IssuingCmnyName").value;
        //alert(issueCmpId);
        $.ajax({
            url: '/throttle/getInsCompanyName.do?issueCmpId=' + issueCmpId,
            // alert(url);
            data: {issueCmpId: issueCmpId},
            dataType: 'json',
            success: function(data) {

                if (data !== '') {
                    $.each(data, function(i, data) {
                        var temp = data.Name.split('~');
                        document.getElementById("ins_AgentName").value = temp[0];
                        document.getElementById("ins_AgentCode").value = temp[1];
                        document.getElementById("ins_AgentMobile").value = temp[2];
                    });
                }
            }
        });
    }





    function validate()
    {
        var tyreIds = document.getElementsByName("tyreIds");
        var tyreDate = document.getElementsByName("tyreDate");
        var positionIds = document.getElementsByName("positionIds");
        var itemIds = document.getElementsByName("itemIds");
        var tyreExists = document.getElementsByName("tyreExists");

        var cntr = 0;
        for (var i = 0; i < tyreIds.length; i++) {
            for (var j = 0; j < tyreIds.length; j++) {
                if ((tyreIds[i].value == tyreIds[j].value) && (tyreIds[i].value == tyreIds[j].value) && (i != j) && (tyreIds[i].value != '')) {
                    cntr++;
                }
            }
            if (parseInt(cntr) > 0) {
                alert("Same Tyre Number should not exists twice");
                return "fail";
                break;
            }
        }

        for (var i = 0; i < positionIds.length; i++) {
            for (var j = 0; j < positionIds.length; j++) {
                if ((positionIds[i].value == positionIds[j].value) && (positionIds[i].value == positionIds[j].value) && (i != j) && (positionIds[i].value != '0')) {
                    cntr++;
                }
            }
            if (parseInt(cntr) > 0) {
                alert("Tyre Positions should not be repeated");
                return "fail";
                break;
            }
        }

        for (var i = 0; i < tyreIds.length; i++) {

            if (itemIds[i].value != '0') {
                if (positionIds[i].value == '0') {
                    alert('Please Select Position');
                    positionIds[i].focus();
                    return "fail";
                } else if (tyreIds[i].value == '') {
                    alert('Please Enter Tyre No');
                    tyreIds[i].focus();
                    return "fail";
                } else if (tyreExists[i].value == 'exists') {
                    alert('Tyre number ' + tyreIds[i].value + ' is already fitted to another vehicle');
                    tyreIds[i].focus();
                    tyreIds[i].select();
                    return "fail";
                }
            }
        }

        return "pass";
    }




    var httpRequest;
    function checkTyreId(val)
    {
        val = val - 1;
        var tyreNo = document.getElementsByName("tyreIds");
        var tyreExists = document.getElementsByName("tyreExists");
        if (tyreNo[val].value != '') {
            var url = '/throttle/checkVehicleTyreNo.do?tyreNo=' + tyreNo[val].value;
            if (window.ActiveXObject)
            {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest(tyreNo[val].value, val);
            };
            httpRequest.send(null);
        }
    }


    function processRequest(tyreNo, index)
    {
        if (httpRequest.readyState == 4)
        {
            if (httpRequest.status == 200)
            {
                var tyreExists = document.getElementsByName("tyreExists");
                if (httpRequest.responseText.valueOf() != "") {
                    document.getElementById("userNameStatus").innerHTML = "Tyre No " + tyreNo + " Already Exists in " + httpRequest.responseText.valueOf();
                    tyreExists[index].value = 'exists';
                } else {
                    document.getElementById("userNameStatus").innerHTML = "";
                    tyreExists[index].value = 'notExists';
                }
            } else
            {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

    function openAttachedInfo() {
        document.getElementById("asset").value = "2";
        document.getElementById("attachInfo").style.display = "block";
    }
    function closeAttachedInfo() {
        document.getElementById("asset").value = "1";
        document.getElementById("attachInfo").style.display = "none";
    }

    function blockNonNumbers(obj, e, allowDecimal, allowNegative)
    {
        var key;
        var isCtrl = false;
        var keychar;
        var reg;

        if (window.event) {
            key = e.keyCode;
            isCtrl = window.event.ctrlKey
        } else if (e.which) {
            key = e.which;
            isCtrl = e.ctrlKey;
        }

        if (isNaN(key))
            return true;

        keychar = String.fromCharCode(key);

        // check for backspace or delete, or if Ctrl was pressed
        if (key == 8 || isCtrl)
        {
            return true;
        }

        reg = /\d/;
        var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
        var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;

        return isFirstN || isFirstD || reg.test(keychar);
    }


</script>

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();

        $("#insurance").focus();
        $("#vehicleInsurane").focus();
    });
    function selectTabssf(listId) {
        if (listId == '1') {
//                                        alert(listId)
            $("#insurance").focus();

        }
    }
    function selectTabs(listId) {
        var selectedId = getHash(this.getAttribute('href'));

// Highlight the selected tab, and dim all others.
// Also show the selected content div, and hide all others.
        for (var id in contentDivs) {
            if (id == selectedId) {
                tabLinks[id].className = 'selected';
                contentDivs[id].className = 'tabContent';
            } else {
                tabLinks[id].className = '';
                contentDivs[id].className = 'tabContent hide';
            }
        }

// Stop the browser following the link
        return false;
    }

    function vehicleValidation() {//pavi
//                            $("#vehicleDetailUpdateButton").hide();
        var owner = document.getElementById("ownerShips").value;
        document.getElementById("asset").value = owner;
// alert("owner:"+owner);
        if (owner == 1 || owner == 5) {
            if (document.addVehicle.regNo.value == '') {
                alert('Please Enter Vehicle Registration Number');
                document.addVehicle.regNo.focus();
                return false;
            } else if (document.addVehicle.regNoCheck.value == 'exists') {
                alert('Vehicle RegNo already Exists');
                document.addVehicle.regNoCheck.focus();
                return false;
            } else if (document.addVehicle.mfrId.value == 0) {
                alert('Please select Manufacturer');
                document.addVehicle.mfrId.focus();
                return false;
            } else if (document.addVehicle.typeId.value == 0) {
                alert('Please select Vehicle Type');
                document.addVehicle.typeId.focus();
                return false;
            } else if (document.addVehicle.modelId.value == 0) {
                alert('Please select Vehicle Model');
                document.addVehicle.modelId.focus();
                return false;
            } else if (document.addVehicle.dateOfSale.value == '') {
                alert("Please select Registration Date");
                document.addVehicle.dateOfSale.focus();
                return false;
            } else if (document.addVehicle.engineNo.value == '') {
                alert("Please enter Engine Number");
                document.addVehicle.engineNo.focus();
                return false;
            } else if (document.addVehicle.chassisNo.value == '') {
                alert("Please enter Chassis Number");
                document.addVehicle.chassisNo.focus();
                return false;
            } else if (document.addVehicle.seatCapacity.value == '' || document.addVehicle.seatCapacity.value == 0) {
                alert("Please enter Vehicle Tonnage");
                document.addVehicle.seatCapacity.focus();
                return false;
            } else if (document.addVehicle.warrantyDate.value == '') {
                alert("Please select Warranty Date");
                document.addVehicle.warrantyDate.focus();
                return false;
            } else if (document.addVehicle.war_period.value == '' || document.addVehicle.war_period.value == 0) {
                alert("Please select Warranty Period");
                document.addVehicle.war_period.focus();
                return false;
            } else if (document.addVehicle.vehicleCost.value == '') {
                alert("Please select Vehicle Cost");
                document.addVehicle.vehicleCost.focus();
                return false;
            } else if (document.addVehicle.vehicleDepreciation.value == '') {
                alert("Please select Vehicle Depreciation");
                document.addVehicle.vehicleDepreciation.focus();
                return false;
                //                }else if(document.addVehicle.classId == 0){
                //                    alert("Please select Vehicle Class");
                //                    document.addVehicle.classId.focus();
                //                    return false;
            } else if (document.addVehicle.kmReading.value == '') {
                alert("Please select Km Reading");
                document.addVehicle.kmReading.focus();
                return false;
            } else if (document.addVehicle.dailyKm.value == '') {
                alert("Please select Daily Km Reading");
                document.addVehicle.dailyKm.focus();
                return false;
            } else if (document.addVehicle.dailyHm.value == '') {
                alert('Please select Daily Hm reading');
                document.addVehicle.dailyHm.focus();
                return false;
            } else if (document.addVehicle.vehicleColor.value == '') {
                alert('Please fill vehicle color');
                document.addVehicle.vehicleColor.focus();
                return false;
            } else {
                //alert("am here :2");
                saveVehicleDetails();
                return true;

            }
        } else {
            if (document.addVehicle.regNo.value == '') {
                alert('Please Enter Vehicle Registration Number');
                document.addVehicle.regNo.focus();
                return false;
            } else if (document.addVehicle.regNoCheck.value == 'exists') {
                alert('Vehicle RegNo already Exists');
                document.addVehicle.regNoCheck.focus();
                return false;
            } else {
                saveVehicleDetails();
                return true;

            }
        }


    }
    function exportExcel(val) {
        var fromDate=document.getElementById("fromDate").value;
        var toDate=document.getElementById("toDate").value;
        document.addVehicle.action = "/throttle/viewCycleCountReport.do?param="+val+"&fromDate="+fromDate+"&toDate="+toDate;
        document.addVehicle.submit();
    }
   


    function saveVehicleDetails() {
//alert("a m here 3");
//$("#vehicleDetailSaveButton").hide();
        var usageId = document.addVehicle.usageId.value;
        var warPeriod = document.addVehicle.war_period.value;
        var mfrId = document.addVehicle.mfrId.value;
        var classId = document.addVehicle.classId.value;
        var modelId = document.addVehicle.modelId.value;
        var dateOfSale = document.addVehicle.dateOfSale.value;
        var regNo = document.addVehicle.regNo.value;
        var description = document.addVehicle.description.value;
        var engineNo = document.addVehicle.engineNo.value;
        var chassisNo = document.addVehicle.chassisNo.value;
        var opId = document.addVehicle.opId.value;
        var seatCapacity = document.addVehicle.seatCapacity.value;
        var kmReading = document.addVehicle.kmReading.value;
        var vehicleCost = document.addVehicle.vehicleCost.value;
        var vehicleColor = document.addVehicle.vehicleColor.value;
        var groupId = document.addVehicle.groupId.value;
        var gpsSystem = document.addVehicle.gpsSystem.value;
        var warrantyDate = document.addVehicle.warrantyDate.value;
        var asset = document.addVehicle.asset.value;
        var axles = document.addVehicle.axles.value;
        var vehicleDepreciation = document.addVehicle.vehicleDepreciation.value;
        var dailyKm = document.addVehicle.dailyKm.value;
        var dailyHm = document.addVehicle.dailyHm.value;
        var gpsSystemId = document.addVehicle.gpsSystemId.value;
        var ownerShips = document.addVehicle.ownerShips.value;
        var typeId = document.addVehicle.typeId.value;
        var vendorId = document.addVehicle.vendorId.value;
        var axleTypeId = document.addVehicle.axleTypeId.value;
        var vehicleId = document.addVehicle.vehicleId.value;
        var activeInd = document.addVehicle.activeInd.value;
// alert("csh")
        var insertStatus = 0;
        if (vendorId == '') {
            vendorId = 0;
        }
//                alert("typeId == "+typeId);
        var vehicleId = $("#vehicleId").val();
        if (vehicleId == '') {
            vehicleId = 0;
        }
        var statusName = "";
        var url = "";
        if (vehicleId == 0) {
            //alert("added")
            url = "./saveVehicleDetails.do";
            statusName = "added";
        } else {
//                            alert("updated")
            url = "./updateVehicleDetails.do";
            statusName = "updated";
        }
//  alert("test url");
        $.ajax({
            url: url,
            data: {usageId: usageId,
                war_period: warPeriod, mfrId: mfrId, classId: classId, modelId: modelId,
                dateOfSale: dateOfSale, regNo: regNo, description: description, regNo: regNo,
                        engineNo: engineNo, chassisNo: chassisNo, opId: opId, seatCapacity: seatCapacity,
                kmReading: kmReading, vehicleColor: vehicleColor, groupId: groupId,
                gpsSystem: gpsSystem, warrantyDate: warrantyDate, asset: asset, axles: axles, vehicleCost: vehicleCost,
                vehicleDepreciation: vehicleDepreciation, dailyKm: dailyKm, dailyHm: dailyHm,
                gpsSystemId: gpsSystemId, ownerShips: ownerShips, typeId: typeId, vendorId: vendorId,
                axleTypeId: axleTypeId, vehicleId: vehicleId, activeInd: activeInd
            },
            type: "GET",
            success: function(response) {

                var vehicleDetails = response.toString().trim();
                var temp = vehicleDetails.split("~");
                vehicleId = parseInt(temp[0]);
                // alert(vehicleId);
                if (vehicleId == 0) {
                    insertStatus = 0;
                    $("#StatusMsg").text("Vehicle " + regNo + " " + statusName + " failed ").css("color", "red");
                } else {
                    insertStatus = vehicleId;
                    $("#StatusMsg").text("Vehicle " + regNo + " " + statusName + " sucessfully. Door No is " + temp[1]).css("color", "green");
                    $("#vehicleId").val(vehicleId);
                    $("#vehicleDetailSaveButton").hide();
                    // $("#tyreSave").show();
                    $("#depreciationSave").show();
                    $("#uploadBtn").show();
                    $("#oEmNext").show();
                    alert("Vehicle " + regNo + " " + statusName + " sucessfully. Door No is " + temp[1]);
                }
            },
            error: function(xhr, status, error) {
            }
        });
        return insertStatus;
    }
    function checkaddRow()
    {
        if ('<%=request.getAttribute("vehicleUploadsDetails")%>' != 'null') {
        } else {
            addRow();
        }

    }
//                   function testPage(){
//                                        var str = new String("Hello world");
//         alert(str.css("color", "green"));
//                                    }
</script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.Cycle Count Report"  text="Cycle Count Report"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Reports"  text="Reports"/></a></li>
            <li class="active"><spring:message code="stores.label.Cycle Count Report"  text="Cycle Count Report"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

            <!--    <body onLoad="addRow();setImages(0,0,0,0,0,0);hideAdds(1),hideAdds1(1);advanceFuelTabHide()">-->
            <body onLoad="checkaddRow();
                    addOem(0, 1, '', '', '', 0, 0, 0);
                    vehicleStepneyDetails(1);
                  ">
<!--                        <body onLoad="checkaddRow();checkPaymentMode();getBankBranch('<c:out value="${bankId}"/>');getBankBranchRoadTax('<c:out value="${bankIdRT}"/>');
                   getBankBranchFC('<c:out value="${bankIdFC}"/>');getBankBranchPermit('<c:out value="${bankIdPermit}"/>');selectTabs('<c:out value="${listId}"/>');
                   addOem(0, 1, '', '', '', 0, 0, 0);
                   vehicleOEMRow();vehicleStepneyDetails(1);
                   addRow1();
                   setImages(0, 0, 0, 0, 0, 0);
                   hideAdds(1);hideAdds1(1);">-->

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>
                <form name="addVehicle" method="post" enctype="multipart/form-data" >

                    <%@ include file="/content/common/message.jsp" %>
                    <input type="hidden" name="fleetTypeId" id="fleetTypeId" value="<c:out value="${fleetTypeId}"/>" />
                    <font  style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;">
                    <div align="center" style="height:30px;" id="StatusMsg">&nbsp;&nbsp;
                    </div>
                    </font>

                    <input type="hidden" name="fleetTypeId" id="fleetTypeId" value="1" />
                    <table border="0" class="border" align="center" width="1000" cellpadding="0" cellspacing="0" id="bg">
                      <tr>
                            <td align="center"><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                            <td height="30"><input name="fromDate" id="fromDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                            <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                            <td height="30"><input name="toDate" id="toDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                            <td>
                                <input type="button" class="btn btn-success" id="Search" onclick="exportExcel(this.value)" name="Search" value="view"/></td>
                        </tr>
                        </table>
                            <br>
                            <br>
                            <br>
                          <center>
                                <input type="button" class="btn btn-success" id="ExportExcel" onclick="exportExcel(this.value)" name="ExportExcel" value="Ongoing Cycle Count Excel"/></td>
                       &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;
                                <input type="button" class="btn btn-success" id="ExportExcel" onclick="exportExcel(this.value)" name="ExportExcel" value="Completed Cycle Count Excel"/></td>
                            </center>
                    <br>
                    <br>
                    
                    <div id="tabs">
                        <ul class="nav nav-tabs">
                            <li class="active" data-toggle="tab"><a href="#vehicleInfo"><span><spring:message code="trucks.label.Accepted Cycle Count"  text="Completed Cycle Count"/></span></a></li>
                            <li  data-toggle="tab"><a href="#vehicleFileAttachments"><span><spring:message code="trucks.label.Requested Cycle Count"  text="Ongoing Cycle Count"/></span></a></li>
                        </ul>
                        <div id="vehicleInfo">
                            <input type="hidden" name="regNoCheck" id="regNoCheck" value='' >

                            <table border="0" class="border" align="center" width="1000" cellpadding="0" cellspacing="0" id="bg">
                                <tr>
                                    <td colspan="6">
                                        <table class="table table-info mb30 table-hover" >
                                            <thead>
                                            <th>S. No</th>
                                            <th>Crate No.</th>
                                            <th>Crate Code</th>
                                            <th>Warehouse Name</th>
                                            <th>Barcode</th>
                                            <th>Status</th>
                                            <th>Created By</th>
                                            <th>Created Date</th>
                                            <th>Created Time</th>
                                            </thead>
                                            <!--<td colspan="4">-->
                                            <%int sno = 1;%>
                                            <tbody>
                                                <c:forEach items="${getCrateDetailsReportPer}" var="in">
                                                    <tr>
                                                        <td><%=sno%></td>
                                                        <td><c:out value="${in.crateNo}"/></td>
                                                        <td><c:out value="${in.crateCode}"/></td>
                                                        <td><c:out value="${in.whName}"/></td>
                                                        <td><c:out value="${in.barCode}"/></td>
                                                        <c:if test="${in.status == 0}">
                                                            <td> Not Mapped</td>
                                                        </c:if>
                                                        <c:if test="${in.status == 1}">
                                                            <td> Inward Pending</td>
                                                        </c:if>
                                                        <c:if test="${in.status == 2}">
                                                            <td> Outward Pending</td>
                                                        </c:if>
                                                        <c:if test="${in.status == 3}">
                                                            <td> Accepted</td>
                                                        </c:if>
                                                        <td><c:out value="${in.name}"/></td>
                                                        <td><c:out value="${in.createdDate}"/></td>
                                                        <td><c:out value="${in.createdTime}"/></td>
                                                    </tr>
                                                    <%sno++;%>
                                                </c:forEach>
                                            </tbody>

                                        </table>

                                    </td>
                                </tr>
                            </table>
                            <br>
                          

                        </div>

                        <div id="vehicleFileAttachments">
                            <input type="hidden" name="regNoCheck" id="regNoCheck" value='' >
                            <table border="0" class="border" align="center" width="1000" cellpadding="0" cellspacing="0" id="bg">
                                <tr>
                                    <td colspan="6">
                                        <table class="table table-info mb30 table-hover" >
                                            <thead>
                                            <th>S. No</th>
                                            <th>Crate No.</th>
                                            <th>Barcode No.</th>
                                            <th>Created Date</th>
                                            <th>Start Time</th>
                                            <th>End Time</th>
                                            <th>Status</th>
                                            <th>Warehouse Name</th>
                                            <th>Created By</th>
                                            </thead>
                                            <!--<td colspan="4">-->
                                            <%int snoo = 1;%>
                                            <tbody>
                                                <c:forEach items="${getCrateDetailsReport}" var="in">
                                                    <tr>
                                                        <td><%=snoo%></td>
                                                        <td><c:out value="${in.crateNo}"/></td>
                                                        <td><c:out value="${in.barCode}"/></td>
                                                        <td><c:out value="${in.createdDate}"/></td>
                                                        <td><c:out value="${in.startTime}"/></td>
                                                        <td><c:out value="${in.endTime}"/></td>
                                                        <c:if test="${in.status == 0}">
                                                            <td> Not Mapped</td>
                                                        </c:if>
                                                        <c:if test="${in.status == 1}">
                                                            <td> Inward Pending</td>
                                                        </c:if>
                                                        <c:if test="${in.status == 2}">
                                                            <td> Outward Pending</td>
                                                        </c:if>
                                                        <c:if test="${in.status == 3}">
                                                            <td> Accepted</td>
                                                        </c:if>
                                                        <td><c:out value="${in.whName}"/></td>
                                                        <td><c:out value="${in.name}"/></td>
                                                    </tr>
                                                    <%snoo++;%>
                                                </c:forEach>
                                            </tbody>

                                        </table>
                                    </td>
                                </tr>

                            </table>
                       

                        </div>
                  





                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>

        </div>


    </div>
</div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>


