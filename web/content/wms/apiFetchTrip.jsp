<%-- 
    Document   : apiFetchTrip.jsp
    Created on : 21 Oct, 2021, 6:10:28 PM
    Author     : Roger
--%> 
<%@page import="java.text.SimpleDateFormat"%>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>
    <%@ page import="java.text.DecimalFormat" %>
    <%@ page import="java.text.NumberFormat" %>

    <script language="javascript" src="/throttle/js/validate.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->
    <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>

    <link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

    <link rel="stylesheet" type="text/css" href="/throttle/css/select2-bootstrap.css"/>


    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#tabs").tabs();
        });
    </script>

</head>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.RunSheet Planning" text="RunSheet Planning"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.RunSheet Planning" text="RunSheet Planning"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="" >
                <form name="trip" method="post">
                    <%                        Date today = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                        String startDate = sdf.format(today);
                        Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.DATE, 7);
                        String nextDate = sdf.format(cal.getTime());
                    %>

                    <script>
                        checkTrackable();
                        function checkTrackable() {
                            var tripId = '<c:out value="${apiTripId}"/>';
                            var isTrackable = false;
                            var publicLink="";

                            $.ajax({"url": "https://sct.intutrack.com/api/test/trips?tripId="+tripId+"&running=true&limit=10",
                                "method": "GET",
                                "mode": "no-cors",
                                "headers": {
                                    "Authorization": "Basic ZTNBWmFCT1F2bUtKWU5palh6Tmx2eVJtV2RLdTVhZFkybTB6UXhFVDRZNUFBZ2s0VE5pdUR2bDZ5UGp3VzB0WjpIdHJzRnIyR0FyQUloc05obk9BQXNORHZ5TjBBSVdqaElLSDZMRVJGR3JMTnl1NHVRaTVWU2FzMExBUmVNRnQw"
                                },
                                success: function(data) {
                                    console.log(data);
                                    $.each(data,function(i,data){
                                        console.log(data.public_link);
                                        publicLink = data.public_link;
                                        document.getElementById("sctDiv").src=publicLink;
                                    })
                                },
                                error: function(r, e) {
                                    console.log(r.responseJSON.message);
                                    alert(r.responseJSON.message);
                                }
                            });
                        }
                    </script>
                    <iframe id="sctDiv" style="width:100%;height:100%"></iframe>
                </form>
            </body>
        </div>
    </div>
</div>




