<%-- 
    Document   : imMaterialPOView
    Created on : 20 Oct, 2021, 4:35:45 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Material PO View</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html">InstaMart</a></li>
                <li class="">Material PO View</li>

            </ol>
        </div>
    </div>
    <script>

        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });


    </script>
    <script>
        function openView(poId) {
            window.open('/throttle/imMaterialPOView.do?param=view&poId=' + poId, 'PopupPage', 'height = 400, width = 800, scrollbars = yes, resizable = yes');
        }
        function approveRequest(poId) {
            document.material.action = '/throttle/imMaterialPOView.do?param=approve&poId=' + poId;
            document.material.submit();

        }
        function authRequest(poId) {
            document.material.action = '/throttle/imMaterialPOView.do?param=auth&poId=' + poId;
            document.material.submit();
        }
        function searchPage() {
            document.material.action = '/throttle/imMaterialPOView.do';
            document.material.submit();
        }
    </script>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="material"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <table class="table table-info mb30 table-hover">
                            <thead><tr><th colspan="4">Search Material PO</th></tr></thead>
                            <tr>
                                <td>From Date</td>
                                <td><input type="text" autocomplete="off" class="form-control datepicker" style="width:240px;height:40px" name="fromDate" id="fromDate" value="<c:out value="${fromDate}"/>"/></td>
                                <td>To Date</td>
                                <td><input type="text" autocomplete="off" class="form-control datepicker" style="width:240px;height:40px" name="toDate" id="toDate" value="<c:out value="${toDate}"/>"/></td>
                            </tr>
                            <tr>
                                <td>Warehouse</td>
                                <td>
                                    <select type="text" autocomplete="off" class="form-control" style="width:240px;height:40px" name="whId" id="whId">
                                        <option value="">---------Select One---------</option>
                                        <option value="0">All Warehouse</option>
                                        <c:forEach items="${whList}" var="wh">
                                            <option value="<c:out value='${wh.whId}'/>"><c:out value='${wh.whName}'/></option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                        </table>
                            <center>
                                <input type="buttton" class="btn btn-success" value="Search" onclick="searchPage()"/>
                            </center>
                            <br><br>
                            <script>
                                document.getElementById("whId").value= '<c:out value="${whId}"/>'
                            </script>
                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30" style="color: white">
                                    <th>S.No</th>
                                    <th>PO No</th>
                                    <th>PO Date</th>
                                    <th>Customer Name</th>
                                    <th>Total Qty</th>
                                    <th>Expected Delivery Date</th>
                                    <th>Remarks</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                                <c:if test = "${poView != null}">
                                    <c:forEach items="${poView}" var="pc">
                                        <%
                                            sno++;
                                            String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.poNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.poDate}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.custName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.qty}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.deliveryDate}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.remarks}" /></td>
                                            <td class="<%=className%>"  align="left"> 
                                                <c:out value="${pc.status}" />
                                            </td>
                                            <td class="<%=className%>"> <a onclick="openView('<c:out value="${pc.poId}"/>')"><b>View</b></a><br><br>
                                                <c:if test = "${RoleId == 1076}" >
                                                    <c:if test = "${pc.authStatus == '0'}" >
                                                        <a style="color: green;" onclick="approveRequest('<c:out value="${pc.poId}"/>')"><b>Approve</b></a><br><br>
                                                    </c:if>
                                                </c:if>
                                                <c:if test = "${RoleId == 1077}" >
                                                    <c:if test = "${pc.authStatus == '1'}" >
                                                        <a style="color: red;" onclick="authRequest('<c:out value="${pc.poId}"/>')"><b>Authorize</b></a>
                                                    </c:if>
                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>


                        <input type="hidden" name="count" id="count" value="<%=sno%>" />

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>

                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
