<%-- 
    Document   : grnDetails
    Created on : Jan 21, 2021, 12:47:26 PM
    Author     : throttle
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Pincode Master"  text="Pincode Master"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Pincode Master"  text="Master"/></a></li>
            <li class="active">Pincode Master</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="setValues(<c:out value="${stateId}"/>,<c:out value="${cityId}"/>);checkExistPin('<c:out value="${existPin}"/>');">
                <form name="pincode" method="post" enctype="multipart/form-data">
                    <c:if test="${pincodeTemp==null}">
                <table class="table table-info mb30 table-hover" id="pincode">
                            <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>
                    <thead><tr>
                    <th class="contenthead" colspan="8">Pincode Master</th>
                          </tr></thead>  
                        <tr>
                           <td align="center"><br><text style="color:red">*</text>
                                State
                            </td>
                            <td><br><select id="stateId" name="stateId" style="width:200px;height:30px">
                              <option value="">---Select One ---</option>    <c:forEach items="${stateList}" var="st">
                                      <option value="<c:out value="${st.stateId}"/>"><c:out value="${st.stateName}"/></option>
                            </c:forEach>
                              </select></td>
                           <td align="center"><br><text style="color:red">*</text>
                                City
                            </td>
                            <td><br><select id="cityId" name="cityId" style="width:200px;height:30px" >
                              <option value="">---Select One ---</option>    <c:forEach items="${cityList}" var="ct">
                                      <option value="<c:out value="${ct.cityId}"/>"><c:out value="${ct.cityName}"/></option>
                            </c:forEach>
                              </select></td>
                            
                            </tr>
                             <tr>
                           <td align="center"><br><text style="color:red">*</text>
                                Warehouse Name
                            </td>
                            <td><br><select id="whId" name="whId" style="width:200px;height:30px" >
                              <option value="">---Select One ---</option>    <c:forEach items="${whList}" var="wh">
                                      <option value="<c:out value="${wh.whId}"/>"><c:out value="${wh.whName}"/></option>
                            </c:forEach>
                              </select></td>
                             <script>
                                 document.getElementById("whId").value=<c:out value="${hubId}"/>
                             </script>
                                 <td align="center"><br>Pincode Excel</td>
                                 <td><br>
                                <input colspan="2" type="file" name="pinFile" id="pinFile">
                                </td>
                        </tr>
                         </table>  
                        <table>
                            <center>
                                <input type="button" value="Save" id="insert" class="btn btn-success" onClick="submitPage(this.value);">
                                &emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">
                            </center>

                        </table>
                        </c:if>
                       
                    <c:if test="${pincodeTemp!=null}">
                 <table class="table table-info mb30 table-bordered" id="table" >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>Pincode</th>
                                <th>Place</th>
                                <th>City</th>
                                <th>Action</th>
                            </tr>

                        </thead>
                            <% int index = 0;
                                    int sno = 1;
                            %>
                            <tbody>

                                <c:forEach items="${pincodeTemp}" var="pin">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td>
                                            <%=sno%>
                                        </td>
                                        <td ><c:out value="${pin.pincode}"/></td>
                                        <td ><c:out value="${pin.place}"/></td>
                                        <td><c:out value="${pin.cityName}"/></td>
                                        <td>
                                           <input type="button" id="pinTempId" name="pinTempId" value="Delete" class="btn btn-success" onclick="inactivePincode(<c:out value="${pin.stateId}"/>,<c:out value="${pin.cityId}"/>,<c:out value="${pin.tempId}"/>)">  
                                            
                                        </td>
                                    </tr>

                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach> 
                                    <input type="hidden" id="stateId1" name="stateId1" value="">
                                    <input type="hidden" id="cityId1" name="cityId1" value="">
                            </tbody>
                            <table>
                            <center>
                                <input type="button" value="Confirm" id="confirm" class="btn btn-success" onClick="updatePincodeMaster(this.value);">
                                &emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">
                            </center>
                             <table>

                    </table>   
                 </c:if>
                <div id="existing" style="display: none">
                    <table class="table table-info mb30 table-hover" id="existPinB" style="width:40%">
                            
                    <tbody><tr><br><br>
                    <th colspan="8">Existing Pincodes:</th>
                          </tr></tbody>   
                         </table>  
                        
                       
                    <table class="table table-info mb30 table-bordered" id="existPin" style="width:35%" >
                        <thead>
                            <tr >
                                <th>S.No</th>
                                <th>Pincode</th>
                            </tr>
                        </thead>
                            <tbody>

                            </tbody>
                            </table>
                                </div>
 <script>
     function checkExistPin(val){
       var table = document.getElementById("existPin");
         var x=val.split('-');
         x.pop();
         if(x.length>0){
             $('#existing').show();
             for(var i=1;i<=x.length;i++){
             table.insertRow(i).innerHTML= "<td>"+i+"</td>"+"<td>"+x[i-1]+"</td>"
         }
     }
     }
     function submitPage(value) {
         if(document.getElementById("stateId").value==""){
             alert("Select State");
             document.getElementById("stateId").focus();
         }else if(document.getElementById("cityId").value==""){
             alert("Select City");
             document.getElementById("cityId").focus();
         }else if(document.getElementById("pinFile").value==""){
             alert("Upload file");
             document.getElementById("pinFile").focus();
         }else{
            document.pincode.action = '/throttle/savePincodeMaster.do?&param=save';
            document.pincode.submit();
        }
    }
    function inactivePincode(stateId,cityId,tempId){
        document.getElementById("stateId1").value=stateId;
        document.getElementById("cityId1").value=cityId;
        document.pincode.action = '/throttle/savePincodeMaster.do?&param=inactive&tempId='+tempId+'&stateId='+stateId+'&cityId='+cityId;
        document.pincode.submit();
    }
    function setValues(stateId,cityId){
        
        document.getElementById("stateId1").value=stateId;
        document.getElementById("cityId1").value=cityId;
    }
    function updatePincodeMaster(Confirm){
       if(window.confirm("Do you want to confirm?")){
        var stateId1=document.getElementById("stateId1").value;
        var cityId1=document.getElementById("cityId1").value;
        document.pincode.action = '/throttle/savePincodeMaster.do?&param=update&stateId='+stateId1+'&cityId='+cityId1;
        document.pincode.submit();
    }
    }
                </script>
                            
                       
               </form>
        </body>
                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
                   
        </div>
    </div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

