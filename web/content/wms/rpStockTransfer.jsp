<%-- 
    Document   : rpStockTransfer
    Created on : 23 Sep, 2021, 11:07:12 AM
    Author     : Roger
--%>
<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>


<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>
<link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>

<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Gate In Details"  text="Stock Transfer"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Stock Transfer</a></li>
            <li class="active">Stock Transfer</li>
        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="">

                <form name="stock" method="post">

                    <table class="table table-info mb30 table-hover">
                        <div align="center" style="height:15px;" id="statusMsg">&nbsp;&nbsp

                        </div>
                        <tr>
                            <td><text style="color:red">*</text>
                                From  Warehouse
                            </td>
                            <td>
                                <select  style="width:240px;height:40px" class="form-control" id="fromWhId" disabled="" name="fromWhId">
                                    <option value="">------Select One------</option>
                                    <c:forEach items="${whList}" var="war">
                                        <option value="<c:out value="${war.whId}"/>">
                                            <c:out value="${war.whName}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </td>

                            <td><text style="color:red">*</text>
                                To Warehouse
                            </td>
                            <td>
                                <select  style="width:240px;height:40px" class="form-control" id="toWhId" name="toWhId">
                                    <option value="">------select one------</option>
                                    <c:forEach items="${whList}" var="war">
                                        <option value="<c:out value="${war.whId}"/>">
                                            <c:out value="${war.whName}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><text style="color:red">*</text>
                                Product Name 
                            </td>
                            <td>
                                <select  style="width:240px;height:40px" class="form-control"  id="productId" name="productId" onchange="setval(this.value);"  >
                                    <option value="">------Select One------</option>
                                    <c:forEach items="${productList}" var="list">
                                        <option value="<c:out value="${list.productId}"/>-<c:out value="${list.stockId}"/>-<c:out value="${list.stockQty}"/>"><c:out value="${list.productName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                        <script>
                            document.getElementById("fromWhId").value = '<c:out value="${hubId}"/>';
                            $('#productId').select2({placeholder: "Please Choose Product"});
                            $('#productId').val('');
                        </script>
                        <td><text style="color:red">*</text>
                            Available Stock
                        </td>
                        <td>
                            <input style="width:240px;height:40px" readonly type="text" class="form-control" id="stockQty" name="stockQty" value=""/>
                            <input style="width:200px;height:30px" type="hidden" class="form-control" id="stockId" name="stockId"/>
                            <input style="width:200px;height:30px" type="hidden" class="form-control" id="itemId" name="itemId"/>
                        </td>
                        </tr>
                        <tr>
                            <td><text style="color:red">*</text>
                                Transfer Quantity
                            </td>
                            <td>
                                <input style="width:240px;height:40px" type="text" disabled="" class="form-control" value="0" id="transferQty" name="transferQty"/>
                            </td>
                            <td><text style="color:red">*</text>Vehicle No</td>
                            <td><input type="text" name="vehicleNo" id="vehicleNo" class="form-control" style="width:240px;height:40px"/></td>
                        </tr>
                        <tr>
                            <td><text style="color:red">*</text>Driver Name</td>
                            <td><input type="text" name="driverName" id="driverName" class="form-control" style="width:240px;height:40px"/></td>
                            <td><text style="color:red">*</text>Driver Mobile</td>
                            <td><input type="text" name="driverMobile" id="driverMobile" class="form-control" style="width:240px;height:40px"/></td>
                        </tr>
                        <script>
                            function setval(val) {
                                var value = val.split("-");
                                var itemId = value[0];
                                var Qty = value[2];
                                var stockId = value[1];
                                document.getElementById("stockQty").value = Qty;
                                document.getElementById("stockId").value = stockId;
                                document.getElementById("itemId").value = itemId;
                            }
                        </script>

                        <tr>

                            <td class="classText1">Serial No</td>
                            <td><input name="serial" id="serial" type="text" style="font-size:15px;width:240px;height:40px" class="form-control" style="width:240px;height:40px" onchange="addNewRow(this.value);"></td>

                        </tr>
                        <tr>

                        </tr>
                    </table>

                    <table class="table table-info mb30 table-bordered" id="table2" >
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Product Name</th>
                                <th>Stock Quantity</th>
                                <th>Transfer Quantity</th>
                            </tr>
                        </thead>
                        <tbody id="productTab">
                        <input type="hidden" name="prodCount" id="prodCount" value="0">
                        </tbody>
                    </table>      

                    <table class="table table-info mb30 table-bordered" id="table3" >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>Serial No</th>
                                <th>Product Name</th>
                                <th>Qty</th>

                            </tr>

                        </thead>
                        <tbody id="serialTab">
                        <input type="hidden" name="serCount" id="serCount" value="0">
                        </tbody>
                    </table>      
                </form>


            <center>
                <input type="button" value="save"  class= "btn btn-success" onclick="submitPage();">

            </center>
            <script>
                function submitPage() {
                    if (document.getElementById("toWhId").value == "") {
                        alert("Please Select To Warehouse");
                        document.getElementById("toWhId").focus();
                    } else if (document.getElementById("driverName").value == "") {
                        alert("Please Enter Driver Nama");
                        document.getElementById("driverName").focus();
                    } else if (document.getElementById("driverName").value == "") {
                        alert("Please Enter Driver Name");
                        document.getElementById("driverName").focus();
                    } else if (document.getElementById("driverMobile").value == "") {
                        alert("Please Enter Driver Mobile");
                        document.getElementById("driverMobile").focus();
                    } else if(parseInt($('#serialTab').find('tr').length)==0){
                        alert("Please Scan Serial Nos to Transfer");
                    }else{
                        document.stock.action = '/throttle/rpStockTransfer.do?param=save';
                        document.stock.submit();
                    }
                }

                function addNewRow(serialNo) {
                    var productId = document.getElementById("itemId").value;
                    var stockQty = document.getElementById("stockQty").value;
                    var stockId = document.getElementById("stockId").value;
                    var prodCount = document.getElementById("prodCount").value;
                    var serialNos = document.getElementsByName("serialNos");
                    var serialExistCount = 0;
                    for (var i = 0; i < serialNos.length; i++) {
                        if (serialNos[i].value == serialNo) {
                            serialExistCount = 1;
                        }
                    }
                    if (serialExistCount == 0) {    
                        var productName = $("#productId option:selected").text();
                        $.ajax({
                            url: "/throttle/getRpExistSerialNo.do",
                            dataType: 'json',
                            data: {
                                productId: productId,
                                serialNo: serialNo
                            },
                            success: function(data, textStatus, jqXHR) {

                                if (data == '0') {
                                    alert("Serial Number Not Found");
                                    document.getElementById("serial").value = "";
                                } else {
                                    var serialId = data;
                                    if (parseInt(prodCount) > 0) {
                                        var checkExistProductId = 0;
                                        var prodIds = document.getElementsByName("prodIds");
                                        for (var i = 0; i < prodIds.length; i++) {
                                            if (parseInt(prodIds[i].value) == parseInt(productId)) {
                                                checkExistProductId = parseInt(i) + 1;
                                            }
                                        }
                                        if (parseInt(checkExistProductId) == 0) {
                                            var sno = $('#productTab').find('tr').length;
                                            var sno = parseInt(sno) + 1;
                                            $("#productTab").append('<tr><td>' + sno + '</td><td>' + productName + '</td><td>' + stockQty + '</td><td><input type="hidden" name="prodIds" id="prodIds' + sno + '" value="' + productId + '"/><input type="text" readonly class="form-control" name="transferedQty" value="1" id="transferedQty' + sno + '"/></td></tr>');
                                            document.getElementById("prodCount").value = parseInt(document.getElementById("prodCount").value) + 1;
                                        } else {
                                            document.getElementById("transferedQty" + checkExistProductId).value = parseInt(document.getElementById("transferedQty" + checkExistProductId).value) + 1;
                                            document.getElementById("prodCount").value = parseInt(document.getElementById("prodCount").value) + 1;
                                        }
                                    } else {
                                        var sno = $('#productTab').find('tr').length;
                                        var sno = parseInt(sno) + 1;
                                        $("#productTab").append('<tr><td id="sno1">' + sno + '</td><td>' + productName + '</td><td>' + stockQty + '</td><td><input type="hidden" name="prodIds" id="prodIds' + sno + '" value="' + productId + '"/><input type="text" readonly class="form-control" name="transferedQty" value="1" id="transferedQty' + sno + '"/></td></tr>');
                                        document.getElementById("prodCount").value = parseInt(document.getElementById("prodCount").value) + 1;
                                    }

                                    var sno1 = $('#serialTab').find('tr').length;
                                    var sno1 = parseInt(sno1) + 1;
                                    $("#serialTab").append('<tr><td id="sno1">' + sno1 + '</td><td>' + serialNo + '</td><td>' + productName + '</td><td>1</td><td><input type="hidden" name="serialNos" id="serialNos' + sno1 + '" value="' + serialNo + '"/><input type="hidden" name="serialIds" id="serialIds' + sno1 + '" value="' + serialId + '"/><input type="hidden" name="stockId" id="stockId' + sno1 + '" value="' + stockId + '"/><input type="hidden" name="productIds" id="productIds' + sno1 + '" value="' + productId + '"/></td></tr>');
                                }
                                document.getElementById("serial").value = "";
                            }

                        });


                    } else {
                        alert("Already Scanned Serial No");
                        document.getElementById("serial").value = "";
                    }
                }
            </script>


            <script>

            </script>

            <script>

            </script>


            </body>


        </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>