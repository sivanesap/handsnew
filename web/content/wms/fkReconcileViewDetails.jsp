<%-- 
    Document   : fkReconcileViewDetails
    Created on : 14 Jun, 2021, 7:13:03 PM
    Author     : Roger
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script>

        function ChangeDropdowns(sno) {
            var shipmentType = document.getElementById('shipmentType' + sno).value;
            if (shipmentType == 'PP') {
                document.getElementById("deCod" + sno).value = '0';
                document.getElementById("cardAmount" + sno).value = '0';
                document.getElementById("prexoAmount" + sno).value = '0';

                document.getElementById("deCod" + sno).readOnly = true;
                document.getElementById("cardAmount" + sno).readOnly = true;
                document.getElementById("prexoAmount" + sno).readOnly = true;


                var deCod = document.getElementsByName("deCod");
                var deCodTot = 0;
                for (var i = 0; i < deCod.length; i++) {
                    deCodTot = parseFloat(deCodTot) + parseInt(deCod[i].value);
                }
                $('#totalDeCod').val(deCodTot);

                var cardAmount = document.getElementsByName("cardAmount");
                var cardAmountTot = 0;
                for (var i = 0; i < deCod.length; i++) {
                    var cardAmountTot = parseFloat(cardAmountTot) + parseInt(cardAmount[i].value);
                }
                $('#totalCard').val(cardAmountTot);
                var prexoAmount = document.getElementsByName("prexoAmount");
                var prexoAmountTot = 0;
                for (var i = 0; i < prexoAmount.length; i++) {
                    var prexoAmountTot = parseFloat(prexoAmountTot) + parseInt(prexoAmount[i].value);
                }
//                                     alert(deCodTot);
                $('#totalPrexo').val(prexoAmountTot);
            }
            if (shipmentType == 'COD') {


                var deCod = document.getElementsByName("deCod");
                var deCodTot = 0;
                for (var i = 0; i < deCod.length; i++) {
                    deCodTot = parseFloat(deCodTot) + parseInt(deCod[i].value);
                }
                $('#totalDeCod').val(deCodTot);

                var cardAmount = document.getElementsByName("cardAmount");
                var cardAmountTot = 0;
                for (var i = 0; i < deCod.length; i++) {
                    var cardAmountTot = parseFloat(cardAmountTot) + parseInt(cardAmount[i].value);
                }
                $('#totalCard').val(cardAmountTot);
                var prexoAmount = document.getElementsByName("prexoAmount");
                var prexoAmountTot = 0;
                for (var i = 0; i < prexoAmount.length; i++) {
                    var prexoAmountTot = parseFloat(prexoAmountTot) + parseInt(prexoAmount[i].value);
                }
//                                     alert(deCodTot);
                $('#totalPrexo').val(prexoAmountTot);
            }

        }


        function submitPage() {
            document.reconcile.action = "viewFkReconcile.do?&param=save&statusId=14";
            document.reconcile.submit();
        }
    </script>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.RunSheet" text="Trip Reconciliation Details"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Flipkart" text="Flipkart"/></a></li>
                <li class=""><spring:message code="hrms.label.Reconcile" text="Reconciliation"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">
                    <form name="reconcile" method="post">
                        <%@include file="/content/common/message.jsp" %>
                        <table class="table table-info mb30 table-bordered" id="table"  style="width:98%;" >
                            <thead>
                                <tr >
                                    <th >Sno</th>                                        
                                    <th >Shipment Id</th>                                        
                                    <th >Shipment Type</th>                                        
                                    <th >DE Name</th>
                                    <th >Product Detail</th>
                                    <th >Pickup/Prexo Remarks</th>
                                    <th >Pincode</th>
                                    <th >status</th>
                                    <th >Order Type</th>
                                    <th >Rejection Remarks</th>
                                    <th >Product Value</th>
                                    <th >COD Amount</th>                        
                                    <th >DE COD</th>    
                                    <th >Card Amount</th>
                                    <th >Prexo Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 1;%>
                                <c:forEach items="${reconcileMaster}" var="re">
                                    <%
                                        String className = "text1";
                                        if ((index % 2) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>

                                    <tr height="30">
                                        <td><%=index%></td>                                      
                                        <td><c:out value="${re.shipmentId}"/></td>                                      
                                        <td align="center"><c:if test="${re.shipmentType=='COD'}">COD<select id="shipmentType<%=index%>" name="shipmentType" class="form-control" onchange="ChangeDropdowns(<%=index%>);"  style="width:130px;height:40px;display:none">
                                                    <option value="">------Select------</option>
                                                    <option value="COD">COD</option>
                                                    <option value="PP">PP</option>
                                                </select>
                                            </c:if>
                                            <c:if test="${re.shipmentType=='PP'}">PP<select id="shipmentType<%=index%>" name="shipmentType" style="display:none" class="form-control" onchange="ChangeDropdowns(<%=index%>);"  style="width:130px;height:40px">
                                                    <option value="">------Select------</option>
                                                    <option value="COD">COD</option>
                                                    <option selected value="PP">PP</option>
                                                </select></c:if>
                                             <c:if test="${(re.shipmentType!='PP' && re.shipmentType!='COD')||re.orderType==3}">PP<select id="shipmentType<%=index%>" name="shipmentType" style="display:none" class="form-control" onchange="ChangeDropdowns(<%=index%>);"  style="width:130px;height:40px">
                                            <option value="">------Select------</option>
                                            <option value="COD">COD</option>
                                            <option selected value="PP">PP</option>
                                            </select>
                                            </c:if>
                                            </td>

                                    <script>
                                        $('#shipmentType<%=index%>').val('<c:out value="${re.shipmentType}"/>');
                                </script>
                                <td><c:out value="${re.empName}"/></td>                                                    
                                <td><c:out value="${re.materialDescription}"/></td>                                      
                                <td><c:out value="${re.remarks}"/></td>                                      
                                <td><c:out value="${re.pincode}"/></td>  
                                <c:if test="${re.status==4}">
                                    <td>Delivered</td>
                                </c:if>
                                <c:if test="${re.status==3}">
                                    <td>Rejected</td>  
                                </c:if>
                                <c:if test="${re.status==2}">
                                    <td>Not Attempted</td>  
                                </c:if>
                                <td>
                                    <c:if test="${re.orderType==1}">Delivery</c:if>
                                    <c:if test="${re.orderType==2}">Prexo</c:if>
                                    <c:if test="${re.orderType==3}">RVP</c:if>
                                    </td>
                                    <td><c:out value="${re.reason}"/></td>  
                                <td align="right" ><c:out value="${re.price}"/></td>                                      
                                <td align="right" ><c:out value="${re.codAmount}"/></td>                                      
                                <c:if test="${re.shipmentType=='COD' && re.status==4}">
                                    <td align="right" > <input type="text" class="form-control" style="width:120px;height:30px;display:none" name="deCod" id="deCod<%=index%>" onchange="ChangeDropdowns(<%=index%>);" value="<c:out value="${re.deCod}"/>"><c:out value="${re.deCod}"/></td>                                      
                                    <td align="right" > <input type="text" class="form-control" style="width:120px;height:30px;display:none" name="cardAmount" id="cardAmount<%=index%>"  onchange="ChangeDropdowns(<%=index%>);" value="<c:out value="${re.cardAmount}"/>"><c:out value="${re.cardAmount}"/></td>                                      
                                    <td align="right" > <input type="text" class="form-control" style="width:120px;height:30px;display:none" name="prexoAmount" id="prexoAmount<%=index%>" onchange="ChangeDropdowns(<%=index%>);" value="<c:out value="${re.prexoAmount}"/>"><c:out value="${re.prexoAmount}"/></td>                                      
                                    </c:if>
                                    <c:if test="${re.shipmentType=='PP' || re.status==3}">
                                    <td align="right" > <input type="text" readonly class="form-control" style="width:120px;height:30px;display:none" name="deCod" id="deCod<%=index%>" onchange="ChangeDropdowns(<%=index%>);" value="<c:out value="${re.deCod}"/>"><c:out value="${re.deCod}"/></td>                                      
                                    <td align="right" > <input type="text" readonly class="form-control" style="width:120px;height:30px;display:none" name="cardAmount" id="cardAmount<%=index%>"  onchange="ChangeDropdowns(<%=index%>);" value="<c:out value="${re.cardAmount}"/>"><c:out value="${re.cardAmount}"/></td>                                      
                                    <td align="right" > <input type="text" readonly class="form-control" style="width:120px;height:30px;display:none" name="prexoAmount" id="prexoAmount<%=index%>" onchange="ChangeDropdowns(<%=index%>);" value="<c:out value="${re.prexoAmount}"/>"><c:out value="${re.prexoAmount}"/></td>                                      
                                    </c:if>
                                    <c:if test="${(re.shipmentType!='PP' && re.shipmentType!='COD')||re.orderType==3}">PP<select id="shipmentType<%=index%>" name="shipmentType" style="display:none" class="form-control" onchange="ChangeDropdowns(<%=index%>);"  style="width:130px;height:40px">
                                            <td align="right" > <input type="text" readonly class="form-control" style="width:120px;height:30px" name="deCod" id="deCod<%=index%>" onchange="ChangeDropdowns(<%=index%>);" value="<c:out value="${re.deCod}"/>"></td>                                      
                                            <td align="right" > <input type="text" readonly class="form-control" style="width:120px;height:30px" name="cardAmount" id="cardAmount<%=index%>"  onchange="ChangeDropdowns(<%=index%>);" value="<c:out value="${re.cardAmount}"/>"></td>                                      
                                            <td align="right" > <input type="text" readonly class="form-control" style="width:120px;height:30px" name="prexoAmount" id="prexoAmount<%=index%>" onchange="ChangeDropdowns(<%=index%>);" value="<c:out value="${re.prexoAmount}"/>"></td>                                      
                                        </c:if>
                                </tr>
                                <input type="hidden" name="orderId" id="orderId<%=index%>" value="<c:out value="${re.orderId}"/>">
                                <c:if test="${re.shipmentType == 'COD'}">
                                    <c:set var="totalPrice" value="${totalPrice+re.price}"/>
                                    <c:set var="totalCod" value="${totalCod+re.codAmount}"/>
                                    <c:set var="totalDeCod" value="${totalDeCod+re.deCod}"/>
                                    <c:set var="totalCard" value="${totalCard+re.cardAmount}"/>
                                    <c:set var="totalPrexo" value="${totalPrexo+re.prexoAmount}"/>
                                </c:if>
                                <%index++;%>
                            </c:forEach>
                            <tr>
                                <td colspan="9" align="right" style="padding-right:30px"><b> Total Amount</b></td>
                                <td align="right" id=""><b><c:out value="${totalPrice}"/>
                                        <input type="hidden" name="totalPrice" id="totalPrice" value="<c:out value="${totalPrice}"/>"></b></td>
                                <td align="right"><b><c:out value="${totalCod}"/>
                                        <input type="hidden" name="totalCod" id="totalCod" value="<c:out value="${totalCod}"/>"></b></td>
                                <td align="right"><b>
                                        <input type="text" class="form-control" style="width:150px;height:40px;display:none" name="totalDeCod" id="totalDeCod" readonly value="<c:out value="${totalDeCod}"/>"></b></td>
                                <td align="right"><b> <input type="text" style="width:150px;height:40px;display:none" class="form-control" name="totalCard" id="totalCard" readonly value="<c:out value="${totalCard}"/>"><c:out value="${totalCard}"/></b>
                                </td>
                                <td align="right"><b>
                                        <input type="text" class="form-control" style="width:150px;height:40px;display:none" name="totalPrexo" readonly id="totalPrexo" value="<c:out value="${totalPrexo}"/>"><c:out value="${totalPrexo}"/></b></td>
                            <input type="hidden" name="tripId" id="tripId" value="<c:out value="${tripId}"/>">
                            </tr>
                            <tr>
                                <td colspan="12" align="right" style="font-size:15"><b>Total Amount to Desposit</b></td>
                                <td align="right" colspan="2" id="grandTotal" style="font-size:15;font-weight: bold">Rs. <c:out value="${totalDeCod+totalPrexo}"/></td>
                            </tr>
                            </tbody>
                        </table>
                        <c:if test="${statusId != 14}">
                            <!--<center><input type="button" value="Update" onclick="submitPage()" class="btn btn-success"></center>-->
                        </c:if>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>
