<%-- 
    Document   : findSerialNumber.jsp
    Created on : Mar 12, 2021, 7:56:08 PM
    Author     : throttle
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Gate In Details"  text="Find Serial"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Serial Number</a></li>
            <li class="active">Find Serial Number</li>
        </ol>
    </div>
</div>
           
            <div class="contentpanel">
                <div class="panel panel-default">
                    <div class="panel-body">
                         <%@ include file="/content/common/message.jsp" %>
                         <body onload="reset('<c:out value="${serialNumber}"/>');">
                              <form name="stockDetails" method="post">
                                  
                                  <table class="table table-info mb30 table-hover" id="serial">
                                      <div align="center" style="height:15px;" id="statusMsg">&nbsp;&nbsp
                                          
                                      </div>
                                      <tr>
                                          <td class="classText1">Serial No</td>
                                            <td><input name="serial" id="serial" type="text" style="font-size:15px;width:200px;height:45px" value="<c:out value="${serialNumber}"/>" ></td>
                                             
                                      </tr>
                                  </table>
                                   <center>
                                          <input type="button" value="search"  class= btn btn-success onclick="searchval();" >
                            
                         </center>
                                   <c:forEach items="${serialNumberDetails}" var="stock">
                                    <table class="table table-info mb10 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th colspan="2" height="30" >Inward Details</th>
                                    </tr>
                                </thead>
                            </table>
                                  
                                  <table class="table table-info mb30 table-hover" id="serial">
                                     
                                      
                                       <tr>
                                          
                                               <td class="classText1">GRN NO</td>
                                            <td><input name="serial" id="serial" type="text" style="font-size:15px;width:200px;height:45px" class="form-control" style="width:250px;height:40px" value="<c:out value="${stock.grnNo}"/>" size="20"></td>
                                             
                                             <td class="classText1">GRN DATE</td>
                                            <td><input name="serial" id="serial" type="text" style="font-size:15px;width:200px;height:45px" class="form-control" style="width:250px;height:40px" value="<c:out value="${stock.grnDate}"/>" size="20"></td>
                                             
                                             <td class="classText1">PO Number</td>
                                            <td><input name="serial" id="serial" type="text" style="font-size:15px;width:200px;height:45px" class="form-control" style="width:250px;height:40px" value="<c:out value="${stock.poNumber}"/>" size="20"></td>
                                             
                                             <td class="classText1">PO Request ID</td>
                                            <td><input name="serial" id="serial" type="text" style="font-size:15px;width:200px;height:45px" class="form-control" style="width:250px;height:40px"  value="<c:out value="${stock.asnid}"/>" size="20"></td>
                                      </tr>
                                      
                                  </table>
                                          
                                            </center>
                                    <table class="table table-info mb10 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th colspan="2" height="30" >Current Outward Details</th>
                                    </tr>
                                </thead>
                            </table>
                                  
                                  <table class="table table-info mb30 table-hover" id="serial">
                                       <tr>
                                          <td class="classText1">Loan Id</td>
                                          <td><input name="serial" id="serial" type="text" style="font-size:15px;width:200px;height:45px" class="form-control" style="width:250px;height:40px" value="<c:out value="${stock.loanid}"/>"  size="20"></td>
                                          
                                             
                                             <td class="classText1">Item Name</td>
                                            <td><input name="serial" id="serial" type="text" style="font-size:15px;width:200px;height:45px" class="form-control" style="width:250px;height:40px" value="<c:out value="${stock.itemName}"/>"  size="20"></td>
                                             
                                             <td class="classText1">Loan ID Status</td>
                                             <td><input name="serial" id="serial" type="text" style="font-size:15px;width:200px;height:45px" class="form-control" style="width:250px;height:40px" value="<c:out value="${stock.statusName}" />"  size="20"></td>
                                             
                                              </c:forEach>
                                      </tr>
                                  </table>
                                 </form>
                             
                             
                             
                                 
                                 <script>
                                     function searchval(){
                                         
                                         document.stockDetails.action='/throttle/viewFindSerialNumber.do?&param=search';
                                        document.stockDetails.submit();
                                         
                                     }
                                     </script>
                             
                             
                         </body>
                       
                        
                    </div>
                         <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
                </div>
            </div>
                <%@ include file="/content/common/NewDesign/settings.jsp" %>

