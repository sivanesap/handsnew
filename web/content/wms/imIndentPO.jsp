<%-- 
    Document   : imIndentPO
    Created on : 5 Feb, 2022, 5:55:47 PM
    Author     : alan
--%>



<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth()+1;
    var year = date.getFullYear();
    if(day.toString().length==1){
        day="0"+day;
    }
    if(month.toString().length==1){
        month="0"+month;
    }
    if(year.toString().length==1){
        year="0"+year;
    }
    finalDate = day+"-"+month+"-"+year;
    $(document).ready(function () {
    $('#inDate').val(finalDate);
//        $('#inDate').datepicker('setDate', 'today');
//        $("#datepicker").datepicker({
//            showOn: "button",
//            buttonImage: "calendar.gif",
//            buttonImageOnly: true
//
//        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Indent PO </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">InstaMart</a></li>
            <li class="active">Indent PO</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="">
                <form name="saveGtnDetails" method="post">
                    <table class="table table-info mb30 table-hover" id="serial">
                        <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                        </div>
                        <tr>
                            <!--                            <td align="center"><text style="color:red">*</text>
                                                            Customer Name
                                                        </td>
                                                        <td>
                                                            <select id="custId" name="custId" value="" class="form-control" style="width:240px;height:40px" onchange="checkInvoice(this.value);">
                                                                <option value="">-------Select Customer------</option>
                            <c:forEach items="${customerMaster}" var="cm">
                                <option value="<c:out value="${cm.custId}"/>"><c:out value="${cm.custName}"/></option>
                            </c:forEach>
                        </select>
                    </td>-->
                            <td align="center"><text style="color:red">*</text>Indent Date</td>
                            <td height="30"><input readonly name="inDate" id="inDate"  class="form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="" autocomplete="off"></td>
                            <td align="center"><text style="color:red">*</text>Requirement Date</td>
                            <td height="30"><input name="deliveryDate" id="deliveryDate" type="text"  class="datepicker , form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${gtnDate}"/>" autocomplete="off"></td>

                        </tr>
                        <tr>
                              <td align="center">
                                Remarks
                            </td>
                            <td>
                                <textarea type="text" id="remarks" name="remarks" value="" class="form-control" style="width:240px; height:60px"/></textarea> 
                            </td>

                            <td align="center">Total Quantity</td>
                            <td><input type="number" readonly name="totQty" id="totQty" class="form-control" value="0" style="width:240px;height:40px"></td>
                        </tr>
                      

                    </table>                 
                    <table class="table table-info mb30 table-hover sortable" id="addIndent" align="center" width="90%">
                        <thead >
                            <tr>
                                <th>S.No</th>
                                <th>Material Name</th>  
                                <th>Material Code</th>
                                <th>UOM</th>
                                <th>Material Description</th>
                                <th>Indent Qty</th>
                                <th>Delete</th>

                            </tr>
                        </thead>

                    </table>

                    <script>
                        var rowCount = 0;
                        var sno = 0;
                        var httpRequest;
                        var httpReq;
                        var rowIndex = 0;
                        var httpRequest;


                        function go1() {
                            if (httpRequest.readyState == 4) {
                                if (httpRequest.status == 200) {
                                    var response = httpRequest.responseText;
                                    if (response != "") {
                                        alert(response);
                                        resetTheDetails();

                                    } else
                                    {

                                    }
                                }
                            }
                        }
                        function checkInvoice(invoiceNo) {
                            var url = "";
                            if (invoiceNo != "") {
                                url = "./checkInvoiceExist.do?invoiceNo=" + invoiceNo;

                                if (window.ActiveXObject)
                                {
                                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                } else if (window.XMLHttpRequest)
                                {
                                    httpRequest = new XMLHttpRequest();
                                }
                                httpRequest.open("POST", url, true);
                                httpRequest.onreadystatechange = function () {
                                    go1();
                                };
                                httpRequest.send(null);
                            }

                        }

                        function addRow()
                        {


                            var tab = document.getElementById("addIndent");
                            var iRowCount = tab.getElementsByTagName('tr').length;

                            rowCount = iRowCount;
                            var newrow = tab.insertRow(rowCount);
                            //        alert(iRowCount);
                            sno = rowCount;
                            rowIndex = rowCount;
                            //        alert("rowIndex:"+rowIndex);
                            var cell = newrow.insertCell(0);
                            var cell0 = "<td class='text1' height='25' > <input type='hidden'  name='Sno' value='' > " + sno + "</td>";
                            cell.innerHTML = cell0;


                            cell = newrow.insertCell(1);
                            var cell2 = "<td class='text1' height='25'><select class='form-control' maxlength='4' onchange='checkExist(this.value," + sno + ");' name='materialId' id='materialId" + sno + "' style='width:200px;height:40px'><option value='0'>--Select--</option><c:forEach items='${materialMaster}' var='pro'><option value='<c:out value='${pro.materialId}'/>~<c:out value='${pro.material}'/>~<c:out value='${pro.materialCode}'/>~<c:out value='${pro.unit}'/>~<c:out value='${pro.materialDescription}'/>'><c:out value='${pro.material}'/></option></c:forEach></select><input type='hidden' name='materialIdTemp' id='materialIdTemp" + sno + "'><input type='hidden' name='materialName' id='materialName" + sno + "'></td>"
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(2);
                            var cell2 = "<td class='text1' height='30'><input type='text'class='form-control' name='materialCode' style='width:130px;height:40px' id='materialCode" + sno + "'  value='' readonly></td>"
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(3);
                            var cell2 = "<td class='text1' height='30'><input type='text'class='form-control' name='uom' style='width:130px;height:40px' id='uom" + sno + "'  value='' readonly></td>"
                            cell.innerHTML = cell2;
                            
                            cell = newrow.insertCell(4);
                            var cell2 = "<td class='text1' height='30'><input type='text'class='form-control' name='materialDescription' style='width:130px;height:40px' id='materialDescription" + sno + "'  value='' readonly></td>"
                            cell.innerHTML = cell2;


                            cell = newrow.insertCell(5);
                            cell2 = "<div id='test1'><td class='text1' height='25'><input type='text'class='form-control' name='inQty' style='width:130px;height:40px' id='inQty" + sno + "' onchange='checkqty();'  value='0'> </td></div>";
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(6);
                            var cell2 = "<td class='text1' height='25' ><input type='button' class='btn btn-info'   name='delete'  id='delete'   value='Delete Row' onclick='deleteRow(this," + sno + ");' ></td>";
                            cell.innerHTML = cell2;


                            rowIndex++;
                            rowCount++;
                            sno++;

                        }

                        function deleteRow(src) {
                            sno--;
                            var oRow = src.parentNode.parentNode;
                            var dRow = document.getElementById('addIndent');
                            dRow.deleteRow(oRow.rowIndex);
                            document.getElementById("selectedRowCount").value--;
                        }
                        function submitPage(value) {

                            if (document.getElementById("inDate").value == "") {
                                alert("Enter PO Date");
                                document.getElementById("inDate").focus();
                            } else if (document.getElementById("deliveryDate").value == "") {
                                alert("Enter Expected Delivery Date");
                                document.getElementById("deliveryDate").focus();

                            } else {

                                document.saveGtnDetails.action = '/throttle/imIndentPO.do?param=save';
                                document.saveGtnDetails.submit();
                            }
                        }
                        function checkqty() {
                            var qty = document.getElementsByName("inQty");
                            var totQty = 0;
                            for (var i = 0; i < qty.length; i++) {
                                if (qty[i].value != "") {
                                    totQty = parseInt(qty[i].value) + totQty;
                                }
                            }
                            document.getElementById("totQty").value = totQty;
                        }
                        function checkExist(value, sno) {
                            var arr = value.split("~");
                            var materialId = arr[0];
                            var materialName = arr[1];
                            var materialCode = arr[2];
                            var uom = arr[3];
                            var materialDescription = arr[4];
                            var count = 0;
                            var matTemp = document.getElementsByName("materialIdTemp");
                            for (var i = 0; i < matTemp.length; i++) {
                                if (matTemp[i].value == materialId) {
                                    count++;
                                }
                            }
                            if (count == 0) {
                                document.getElementById("materialIdTemp" + sno).value = materialId;
                                document.getElementById("materialName" + sno).value = materialName;
                                document.getElementById("materialCode" + sno).value = materialCode;
                                document.getElementById("uom" + sno).value = uom;
                                document.getElementById("materialDescription" + sno).value = materialDescription;
                            } else {
                                alert("Already Choosed Product");
                                document.getElementById("materialIdTemp" + sno).value = "";
                                document.getElementById("materialId" + sno).value = "";
                            }
                        }
                        </script>

                        <table>
                            <center>
                                <input value="Add Row" class="btn btn-success" id="countRow" type="button" onClick="addRow();" >
                                &emsp;<input type="button" value="Save" id="insert" class="btn btn-success" onClick="submitPage(this.value);">
                                &emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">
                            </center>

                        </table>
                    </form>
                </body>
            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>

    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>
