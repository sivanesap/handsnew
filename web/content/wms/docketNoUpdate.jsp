<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>


<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script type="text/javascript">
    function submitPage(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            if (value == 'Submit') {
                var chk = document.getElementsByName("selectedStatus");
                for (var i = 0; i < chk.length; i++) {
                    if (chk[i].value == 1) {

                        document.docketNoUpdate.action = '/throttle/docketNoUpdate.do?param=Submit';
                        document.docketNoUpdate.submit();
                    } else {

                    }
                }

            }
            else if(value == 'ExportExel') {
                document.docketNoUpdate.action = '/throttle/docketNoUpdate.do?param=ExportExel';
                document.docketNoUpdate.submit();
            }
            else {
                document.docketNoUpdate.action = '/throttle/docketNoUpdate.do?param=Search';
                document.docketNoUpdate.submit();
            }
        }
    }
    function checkSelectStatus(sno, obj) {
        var val = document.getElementsByName("selectedStatus");
        if (obj.checked == true) {
            document.getElementById("selectedStatus" + sno).value = 1;
        } else if (obj.checked == false) {
            document.getElementById("selectedStatus" + sno).value = 0;
        }

    }

</script>


<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Docket No. Update</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="general-forms.html">Connect Operation</a></li>
            <li class="active">Docket No. Update</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="docketNoUpdate" method="post">

                    <table class="table table-info mb30 table-hover" >
                        <tr>

                            <td align="center"><font color="red">*</font>Plant</td>
                            <td height="30"> <select name="fleetCenterId" id="fleetCenterId" class="form-control" style="width:260px;height:40px;" >
                                    <c:if test="${whList != null}">
                                        <option value="" selected>--<spring:message code="operations.reports.label.Select" text="default text"/>--</option>
                                        <c:forEach items="${whList}" var="wh">
                                            <option value='<c:out value="${wh.whId}"/>'><c:out value="${wh.whName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select></td>
                            <td><font color="red">*</font>Status</td>
                            <td height="30">
                                <select name="statusType" id="statusType" style="width:260px;height:40px" class="form-control" value="">
                                    <option value="">-----All-----</option>
                                    <option value="1">Complete</option>
                                    <option value="0">Pending</option>
                                </select>
                            </td>
                        <script>
                                        document.getElementById("statusType").value = '<c:out value="${statusType}"/>';
                        </script>
                        </tr>
                        <tr>
                            <td align="center"><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                            <td height="30"><input name="fromDate" id="fromDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                            <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                            <td height="30"><input name="toDate" id="toDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                        </tr>
                    </table>
                    <center>

                        <input type="button" class="btn btn-success" name="ExportExel"  id="ExportExel"  value="<spring:message code="operations.reports.label.ExportExel" text="ExportExel"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;
                        <input type="button" class="btn btn-success" name="Search"  id="Search"  value="<spring:message code="operations.reports.label.Search" text="Search"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;

                    </center>
                    <br>
                    <br>

                    <table class="table table-info mb30 table-bordered" id="table" class="sortable" width="100%">
                        <thead>
                            <tr >
                                <th>SNo</th>
                                <th>LRN No</th>
                                <th>Date</th>
                                <th>To Location</th>
                                <th>Docket No.</th>
                                <th>Consignee Name</th>
                                <th>Transporter Name</th>
                                <th>Vehicle Type</th>
                                <th>Vehicle No</th>
                                <th>Transfer Type</th>
                                <th>Invoice No</th>
                                <th>Pre Tax Value</th>
                                <th>Delivery Date</th>
                                <th>Action</th>
                            </tr>

                        </thead>
                        <tbody>
                            <% int index = 1;%>

                            <c:forEach items="${docketNoUpdateDetails}" var="dc">
                                <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                                %>



                                <tr >
                                    <td class="<%=classText%>"  align="center"><%=index%></td>


                                    <td class="<%=classText%>"  align="center"><input type="hidden" id="subLr<%=index%>" name="subLr" value="<c:out value="${dc.lrnNo}"/> " style="width:100px;height:30px;"><c:out value="${dc.lrnNo}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.lrDate}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.city}"/></td>
                                    <td class="<%=classText%>"  align="center"><input type="text" name="docketNo" id="docketNo<%=index%>" value="<c:out value="${dc.docketNo}"/>" onkeyup="totalCharges(<%=index%>);" style="width:100px;height:30px;"></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.consigneeName}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.transporterName}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.vehicleType}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.vehicleNo}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.transferType}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.invoiceNo}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.preTaxValue}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.deliveryDate}"/></td>
                                    <td class="<%=classText%>"  align="center"><input type="checkbox" id="selectedIndex<%=index%>" name="selectedIndex" value="" onclick="checkSelectStatus('<%=index%>', this)">
                                        <input type="hidden" name="selectedStatus" id="selectedStatus<%=index%>" value="0" />
                                    </td>
                                    <%index++;%>

                                </c:forEach>

                        </tbody>
                    </table> 
                    <center>

                        <input type="button" class="btn btn-success" name="Submit"  id="Submit"  value="<spring:message code="operations.reports.label.Submit" text="Submit"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;

                    </center>




                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>


                    <br/>
                    <br/>

                    <br/>
                    <br/>
                    <br/>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>