<%-- 
    Document   : fkReconcileViewExcel
    Created on : 14 Jun, 2021, 5:12:22 PM
    Author     : Roger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Excel";
        request.setAttribute("menuPath", menuPath);
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "ReconcileReport-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                <thead>
                    <tr height="40">
                        <th>S.No</th>                                    
                        <th>Reconcile Code</th>  
                        <th>Runsheet Id</th>
                        <th>Runsheet Date</th>                                                                                                         
                        <th>DE Name</th>                                                                                                         
                        <th>Hub Name</th>
                        <th>Total Orders</th>
                        <th>Delivered</th>
                        <th>Rejected</th>
                        <th>Ship COD</th>
                        <th>Prexo COD</th>
                        <th>DE COD</th>
                        <th>Status</th>
                        <th>View</th>
                    </tr>
                </thead>
                <% int index = 0;
                    int sno = 1;
                %>
                <tbody>
                    <c:forEach items= "${fkRunsheetReport}" var="ifb">
                        <tr height="30">
                            <td align="left"><%=sno%></td>
                            <td align="left" ><c:out value="${cnl.reconcileCode}"/></td>
                            <td align="left" ><c:out value="${cnl.tripCode}"/></td>
                            <td align="left" ><c:out value="${cnl.date}"/></td>
                            <td align="left" ><c:out value="${cnl.empName}"/></td>
                            <td align="left" ><c:out value="${cnl.whName}"/></td>                            
                            <td align="left" ><c:out value="${cnl.qty}"/></td> 
                            <td align="left" ><c:out value="${cnl.delivered}"/></td> 
                            <td align="left" ><c:out value="${cnl.returned}"/></td> 
                            <td align="left" ><c:out value="${cnl.totalCod}"/></td> 
                            <td align="left" ><c:out value="${cnl.totalPrexoAmount}"/></td> 
                            <td align="left" ><c:out value="${cnl.totalDeCod}"/></td> 
                            <td align="left" ><c:if test="${cnl.status==1}">Deposit Pending</c:if><c:if test="${cnl.status==2}">Deposited</c:if></td> 

                                    <td align="left" ><a href="viewFkReconcile.do?tripId=<c:out value="${cnl.tripId}"/>&statusId=14&param=view">View</a></td> 

                        </tr>
                        <%index++;%>
                        <%sno++;%>
                    </c:forEach>
                </tbody>
            </table>

            <br>
            <br>

        </form>
    </body>
</html>