<%-- 
    Document   : connectVendorChange
    Created on : 22 Oct, 2021, 9:19:55 AM
    Author     : alan
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>
<link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>

<!-- jQuery libs -->

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script type="text/javascript">


    function viewConnectLr(elem, val) {
        if (elem.checked) {
            document.getElementById("vendorId" + val).disabled = false;
            document.getElementById("spanDiv" + val).style.display = "";
        } else {
            document.getElementById("vendorId" + val).disabled = true;
            document.getElementById("spanDiv" + val).style.display = "none";
        }
    }

    function setValueVendor(val) {
        var vendorId = document.getElementById("vendorId" + val).value;
        var dispatchId = document.getElementById("dispatchId" + val).value;
        if (vendorId != "") {
            document.connect.action = '/throttle/alterConnectLrList.do?param=update&vendorId=' + vendorId + '&dispatchId=' + dispatchId;
            document.connect.submit();
        }
        else {
            alert("please Select Vendor Name")
        }
    }

    function submitPage(val) {
        document.connect.action = "/throttle/alterConnectLrList.do";
        document.connect.submit();
    }

</script>


<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>LR View</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="general-forms.html">Connect Operation</a></li>
            <li class="active">LR View</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="connect" method="post">
                    <table class="table table-info mb30 table-hover" >
                        <tr>
                            <td align="center"><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                            <td height="30"><input name="fromDate" id="fromDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                            <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                            <td height="30"><input name="toDate" id="toDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                        </tr>
                        <tr>
                            <td>Warehouse</td>
                            <td>
                                <select id="whId" class="form-control" style="width:240px;height: 40px" name="whId" onchange="showData(this.value);">
                                    <option value="">------Select Warehouse------</option>
                                    <c:forEach items="${getWareHouse}" var="wh">
                                        <option value="<c:out value="${wh.whId}"/>"><c:out value="${wh.whName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <center>

                        <input type="button" class="btn btn-success" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);">&nbsp;&nbsp;
                        <input type="button" class="btn btn-success" name="Export"  id="Export"  value="Export Excel" onclick="exportExcel();">&nbsp;&nbsp;

                        <br><br>
                    </center>

                    <br>
                    <br>
                    <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                        <thead style="font-size:12px">
                            <tr height="40">
                                <th  height="30" >S No</th>
                                <th  height="30" >Vendor Name</th>
                                <th height="30">Action</th>
                                <th  height="30" >LR No</th>
                                <th  height="30" >LR Date</th>
                                <th  height="30" >Supervisor Name</th>
                                <th  height="30" >Vehicle No</th>
                                <th  height="30" >Invoice Nos</th>
                                <th  height="30" >vehicle Type</th>
                                <th  height="30" >Transfer Type</th>

                            </tr>
                        </thead>
                        <% int index = 0;
                            int sno = 1;
                            int count = 0;
                        %> 
                        <tbody>

                            <c:forEach items= "${getConnectVendorDetails}" var="list">
                                <tr height="30">
                                    <td align="left"><%=sno%></td>
                                    <td align="left"   >
                                        <select id="vendorId<%=sno%>" name="vendorId" class="select2-search form-control" style="width:200px;">
                                            <c:forEach items="${vendorList}" var="del">
                                                <option value="<c:out value="${del.vendorId}"/>">
                                                    <c:out value="${del.vendorName}"/></option>
                                            </c:forEach> 
                                        </select>
                                        <script>
                                document.getElementById("vendorId<%=sno%>").disabled=true;
                                document.getElementById("vendorId<%=sno%>").value=<c:out value="${list.vendorId}"/>;
                                        </script>
                                        <input type="hidden" name="dispatchId" id="dispatchId<%=sno%>" value="<c:out value="${list.dispatchId}"/>">
                            <br><br><center><span class="label label-Primary" id="spanDiv<%=sno%>" style="cursor: pointer;display:none" onclick="setValueVendor('<%=sno%>')">Save</span></center>
                            </td>
                            <td>
                                <input type="checkbox" class="btn btn-success" name="checkVendor" onclick="viewConnectLr(this,<%=sno%>)">
                            </td>
                            <td align="left"   ><c:out value="${list.lrnNo}"/></td>
                            <td align="left"   ><c:out value="${list.lrDate}"/></td>
                            <td align="left"   ><c:out value="${list.supervisorName}"/></td>
                            <td align="left"   ><c:out value="${list.vehicleNo}"/></td>
                            <td align="left"   ><c:out value="${list.invoiceNo}"/></td>
                            <td align="left"   ><c:out value="${list.typeName}"/></td>
                            <td align="left"   ><c:out value="${list.transferType}"/></td>
                            </tr> 
                            <%
                                sno++;
                                index++;
                            %>
                        </c:forEach>
                        <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                        </tbody>
                    </table>






                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>


                    <br/>
                    <br/>

                    <br/>
                    <br/>
                    <br/>


                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>