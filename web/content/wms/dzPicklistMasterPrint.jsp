


<%-- 
    Document   : dzPickListMasterView
    Created on : 24 Dec, 2021, 4:16:57 PM
    Author     : alan
--%>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <title>Throttle - Leading Transport Management System</title>

    <link href="content/NewDesign/css/style.default.css" rel="stylesheet">
    <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">




    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style type="text/css">

            .main{
                border: 1px solid black;

                margin-top: 2%;
            }


            .td1{
                border: 1px solid black;
                /* margin: 0%;
                        padding: 0%;*/
            }

            .tab{

                margin-top: 2%;
            }
            .tabdata{
                border: 1px solid black;
                height: 30%;

            }
            .table-bordered{
                border-color: black;
            }

            .eventdwidth{
                width: 100px;
                border: 1px solid black;
            }
            .cols{
                display: inline-block;
                padding: 2% 8%;

            }
            th{
                border-color: black;
            }
            .carb{
                resize: both;
                overflow: auto;
            }



        </style>

    </head>


    <script>
        function callme() {
            document.getElementById("print").style.display = "none"
            print();
            location.reload();
        }
    </script>


    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <form name="upload"  method="post" enctype="multipart/form-data">
                    <div style="text-align: center;">
                        <div id="ptp">
                        </div>     
                    </div>
                </form>


                <div class="container , main" style="width:100%;">



                    <div>Party Name : <c:out value="${dzPickListMasterView[0].partyName}"/>  </div>
                    <div>Transfer Order No : <c:out value="${dzPickListMasterView[0].toNo}"/> </div>


                    <table>
                        <th><img  width = "90px" src = "images/HSSupplyLogo.png"></th>
                        <th> <h5 style = " text-align:center;">Picklist Slip</h5>
                            <br>
                            <h5 style = "text-align:center;" class="carn" > Warehouse Address : <c:out value="${dzPickListMasterView[0].address}"/> </h5>
                        </th>
                        <th> <p style = "padding-right:100px">Date:</p></th>
                    </table>

                    <div class="secondSection" >
                        <table class="table table-bordered" style="margin-top: 20px;">
                            <tr>
                                <th>Sno</th>
                                <th>Category</th>
                                <th>Product Name</th>
                                <th>To Qty</th>
                                <th>Send Amount</th>
                                <th>Bin</th>
                                <th>MFD Date</th>
                                <th>Picked Qty</th>
                            </tr>
                            <% int index = 0;
                                   int sno = 1;
                                   int count = 0;
                            %> 

                            <c:if test = "${dzPickListMasterView != null}" >

                                <tbody>

                                    <c:forEach items="${dzPickListMasterView}" var="pc">


                                        <tr >
                                            <td align="left" class="td1"> <%=sno%> </td>
                                            <td align="left" class="td1"><c:out value="${pc.categoryName}"/></td>
                                            <td align="left" class="td1"><c:out value="${pc.productName}"/></td>
                                            <td align="left" class="td1"><c:out value="${pc.qty}"/></td>
                                            <td align="left" class="td1"><c:out value="${pc.amount}"/></td>
                                            <td align="left" class="td1"><c:out value="${pc.binName}"/></td>
                                            <td align="left" class="td1"></td>
                                            <td align="left" class="td1"></td>

                                        </tr>

                                        <%
                                            sno++;
                                            index++;
                                        %>
                                    </c:forEach>
                                <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                </tbody>
                            </table>

                        </c:if>

                        </table>
                        <br>
                        <br>
                        <table style="width: 100%; margin-buttom:2%;" >
                            <tr>
                                <td colspan="1">Picker Supervisor</td>
                                <td colspan="3"></td>

                                <td colspan="0" style="text-align: right"> Verified By </td>
                            </tr>
                        </table>

                    </div>
                    <br>

                </div>
            </div>
            <br>
        </div>
    </div>

    <center>

        <input type="button" class="btn btn-success" name="print"  id="print"  value="Print" onclick="callme()">&nbsp;&nbsp;

    </center> 


    <%@include file="/content/common/NewDesign/settings.jsp"%>
</html>


