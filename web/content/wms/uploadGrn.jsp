<%-- 
    Document   : orderApproval.jsp
    Created on : Mar 9, 2021, 8:02:14 PM
    Author     : DELL
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<style type="text/css">

</style>
<script type="text/javascript">
    function uploadContract() {
        var upload = document.getElementById('importCnote').value
        if (upload) {
            document.upload.action = "/throttle/connectUploadGrn.do";
            document.upload.method = "post";
            document.upload.submit();

        }
        else {
            alert("Please Select a File To Upload")
        }

    }

    function uploadPage() {
        var countStatus = document.getElementById("countStatus").value;
        if (countStatus == 0) {
            var tatId = document.getElementById("vehicleTatId").value;
            if (tatId == "" || tatId == null) {
                alert("Choose a Vehicle");
            } else {
                document.upload.action = "/throttle/grnUpload.do?&param=save&tatId=" + tatId;
                document.upload.method = "post";
                document.upload.submit();
            }
        } else {
            alert("Please Update Given Errors and Then Upload");
        }

    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="GRN Upload"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.INBOUND" text="INBOUND"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="GRN Upload"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form name="upload"  method="post" enctype="multipart/form-data">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    <table class="table table-info mb30 table-hover" id="uploadTable"  >
                        <thead> 
                            <tr>
                                <th  colspan="3">GRN Upload Details</th>
                            </tr>
                        </thead>
                        <tr>

                            <td ><input type="file" name="importCnote" id="importCnote"  class="importCnote"><font size="1"><u><a href="uploadedxls/grnTemplateConnect.xls" download>sample file</a></u></font></td>                             
                            <td colspan="0"><input type="button" class="btn btn-success" value="Upload" name="UploadContract" id="UploadContract" onclick="uploadContract();"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <c:if test = "${getConnectgrnDetails != null}" >
                            <tr>

                                <td>Vehicle TAT</td>
                                <td><select id="vehicleTatId" name="vehicleTatId" class="form-control" style="width:240px;height:40px;">
                                        <option value="">------Choose A Vehicle------</option>
                                        <c:forEach items="${getVehicleTatList}" var="tat">
                                            <option value="<c:out value="${tat.tatId}"/>"><c:out value="${tat.vehicleNo}"/></option>
                                        </c:forEach>


                                    </select></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </c:if>
                    </table>
                    <div id="ptp">
                        <div class="inpad" style=" overflow-x: visible;">

                            <c:if test = "${getConnectgrnDetails != null}" >

                                <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                    <thead style="font-size:12px">
                                        <tr height="40">
                                            <th  height="30" >S No</th>
                                            <th>gate Inward Date</th>
                                            <th>Gate Inward No</th>
                                            <th>Process Type</th>
                                            <th>Mode of Transport</th>
                                            <th>Vehicle No</th>
                                            <th>Transporter Code</th>
                                            <th>Transporter Name</th>
                                            <th>Material Code</th>
                                            <th>Material Description</th>
                                            <th>Serial No</th>
                                            <th>GI Quantity</th>
                                            <th>UOM</th>
                                            <th>Invoice Number</th>
                                            <th>Invoice Date</th>
                                            <th>Bill Num</th>
                                            <th>Bill Date</th>
                                            <th>Mat Doc No</th>
                                            <th>Mat Doc Date</th>
                                            <th>Mat Doc Year</th>
                                            <th>Posting Date</th>
                                            <th>Supplier Plant Code</th>
                                            <th>Supplier Plant Desc</th>
                                            <th>Rec Plant Desc</th>
                                            <th>Rec Plant Code</th>
                                            <th>GR Qty</th>
                                            <th>Storage Location</th>
                                            <th>Purchase Order No</th>
                                            <th>Movement Type</th>
                                            <th>Created By</th>
                                            <th>Cancelled No</th>
                                            <th>status</th>
                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                     int count=0;
                                    %> 
                                    <tbody>

                                        <c:forEach items= "${getConnectgrnDetails}" var="contractList">
                                            <tr height="30">
                                                <td align="left"><%=sno%></td>
                                                <td align="left"   ><c:out value="${contractList.gateInwardDate}"/></td>
                                                <td align="left"   ><c:out value="${contractList.gateInwardNo}"/></td>
                                                <td align="left"   ><c:out value="${contractList.processType}"/></td>
                                                <td align="left"   ><c:out value="${contractList.modeofTransport}"/></td>
                                                <td align="left"   ><c:out value="${contractList.vehicleNo}"/></td>
                                                <td align="left"   ><c:out value="${contractList.transporterCode}"/></td>
                                                <td align="left"   ><c:out value="${contractList.transporterName}"/></td>
                                                <td align="left"   ><c:out value="${contractList.materialCode}"/></td>
                                                <td align="left"   ><c:out value="${contractList.materialDescription}"/></td>
                                                <td align="left"   ><c:out value="${contractList.serialNo}"/></td>
                                                <td align="left"   ><c:out value="${contractList.giQuantity}"/></td>
                                                <td align="left"   ><c:out value="${contractList.uom1}"/></td>
                                                <td align="left"   ><c:out value="${contractList.invoiceNumber}"/></td>
                                                <td align="left"   ><c:out value="${contractList.invoiceDate}"/></td>
                                                <td align="left"   ><c:out value="${contractList.billNum}"/></td>
                                                <td align="left"   ><c:out value="${contractList.billDate}"/></td>
                                                <td align="left"   ><c:out value="${contractList.matDocNo}"/></td>
                                                <td align="left"   ><c:out value="${contractList.matDocDate}"/></td>
                                                <td align="left"   ><c:out value="${contractList.matDocYear}"/></td>
                                                <td align="left"   ><c:out value="${contractList.postingDate}"/></td>
                                                <td align="left"   ><c:out value="${contractList.suppPlantCode}"/></td>
                                                <td align="left"   ><c:out value="${contractList.suppPlantDesc}"/></td>
                                                <td align="left"   ><c:out value="${contractList.recPlantDesc}"/></td>
                                                <td align="left"   ><c:out value="${contractList.recPlantCode}"/></td>
                                                <td align="left"   ><c:out value="${contractList.grQty}"/></td>
                                                <td align="left"   ><c:out value="${contractList.storageLocation}"/></td>
                                                <td align="left"   ><c:out value="${contractList.purchaseOrderNo}"/></td>
                                                <td align="left"   ><c:out value="${contractList.movementType}"/></td>
                                                <td align="left"   ><c:out value="${contractList.createdBy}"/></td>
                                                <td align="left"   ><c:out value="${contractList.cancelledNo}"/></td>
                                                <c:if test="${contractList.status==0}">
                                                    <td align="left"><font color="green">Ready To Submit</font></td>
                                                        </c:if>
                                                        <c:if test="${contractList.status!=0}">
                                                            <%count++;%>
                                                        </c:if>
                                                        <c:if test="${contractList.status==1}">
                                                    <td align="left"><font color="red">Material does not Exist</font></td>
                                                        </c:if>

                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                    </tbody>
                                </table>

                                <center>

                                    <td colspan="0"><input type="button" class="btn btn-success" value="Submit" name="submitPage" id="submitPage" onclick="uploadPage();"></td>
                                </center>
                            </c:if>
                        </div>
                    </div>     

                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>


