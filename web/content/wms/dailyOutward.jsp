<%-- 
    Document   : dailyOutwards
    Created on : 20 Dec, 2021, 1:04:44 AM
    Author     : alan
--%>



<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript"></script>

    
    
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>


        <script type="text/javascript">
   
        function dailyOutward() {
            var warehouse = document.getElementById("warehouse").value;
            var date = document.getElementById("date").value;
            var packets = document.getElementById("packets").value;
            var tons = document.getElementById("tons").value;
            var milk = document.getElementById("milk").value;
            var fandv = document.getElementById("FAndV").value;
           
        

                if (warehouse == ""){
                    alert("Enter The Warehouse Data");
                }
                else if (date == ""){
                    alert("Enter The Date Data");
                }
                else if (packets == ""){
                    alert("Enter The Packets Data");
                }
                else if (tons == ""){
                    alert("Enter The Tons Data");
                    
                }
                else if (milk == ""){
                    alert("Enter The Milk Data");
                    
                }
                else if (fandv == ""){
                    alert("Enter The F & V Data");
                    
                }
                else 
                {
                    document.instaMart.action = "/throttle/imDailyOutward.do?param=save";
                    document.instaMart.submit();
                }

        }



        function updateInstaMart(sno, productName, skuCode, productDescription, minimumStockQty, uom, temperature, storage, activeInd, productId) {

            document.getElementById("productName").value = productName;
            document.getElementById("skuCode").value = skuCode;
            document.getElementById("productDescription").value = productDescription;
            document.getElementById("minimumStockQty").value = minimumStockQty;
            document.getElementById("uom").value = uom;
            document.getElementById("temperature").value = temperature;
            document.getElementById("storage").value = storage;
            document.getElementById("activeInd").value = activeInd;
            document.getElementById("productId").value = productId;


            var count = parseInt(document.getElementById("count").value);
            for (i = 1; i <= parseInt(count); i++) {
                if (i != sno) {
                    document.getElementById("edit" + i).checked = false;
                } else {
                    document.getElementById("edit" + i).checked = true;
                }
            }
        }
        
        
// function insertTo(){
//    
//            
//            var result = confirm("Some errors are not Updated. Do you want to continue?");
//            if (result == true) {
//                document.upload.action = "/throttle/imProductMasterToMainTable.do?";
//                document.upload.method = "post";
//                document.upload.submit();
//            }
//            else {
//            
//            document.upload.action = "/throttle/imProductMasterToMainTable.do?";
//            document.upload.method = "post";
//            document.upload.submit();
//        }
//    }
       
        
        

    </script>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Daily Outward</h2>

        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Instamart</a></li>
                <li class="">Daily Outward</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
<!--                <form name="upload"  method="post" enctype="multipart/form-data">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    <table class="table table-info mb30 table-hover" id="uploadTable"  >
                        <thead> 
                            <tr>
                                <th  colspan="3">Dealer Master Upload</th>
                            </tr>

                        </thead>


                        <tr>
                            <td >Select Excel</td>
                            <td ><input type="file" name="importCnote" id="importCnote"  class="importCnote"><font size="1"><u><a href="uploadedxls/ifbFormat.xls" download>sample file</a></u></font></td>                             
                            <td colspan="0"><input type="button" class="btn btn-success" value="Upload" name="UploadPage" id="UploadPage" onclick="uploadPage();"></td>
                        </tr>

                    </table>
                    <div id="ptp">
                        <div class="inpad" style=" overflow-x: visible;">

                            <c:if test = "${getDailyOutWard != null}" >

                                <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                    <thead style="font-size:12px">
                                        <tr height="40">
                                            <th>Sno</th>
                                            <th>Order Date</th>
                                            <th>Customer Id</th>
                                            <th>Customer Name</th>
                                            <th>Product Id</th>
                                            <th>Product Name</th>
                                            <th>Qty</th>
                                            <th>Cod Amount</th>
                                            <th>Pin Code</th>
                                            <th>Estimate Delivery Time</th>
                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                     int count=0;
                                    %> 
                                    <tbody>

                                        <c:forEach items="${getDailyOutWard}" var="pc">


                                            <tr>
                                                <td align="left"> <%=sno%> </td>
                                                <td align="left"> <c:out value="${pc.whId}"/></td>
                                                <td align="left"> <c:out value="${pc.date}"/> </td>
                                                <td align="left"> <c:out value="${pc.fandv}"/></td>
                                                <td align="left"> <c:out value="${pc.packets}"/> </td>
                                                <td align="left"> <c:out value="${pc.tons}"/> </td>
                                                <td align="left"> <c:out value="${pc.milk}"/> </td>
                                                <td align="left"> <c:out value="${pc.createdOn}"/> </td>
                                                <td align="left">
                                                    <c:if test="${pc.activeInd=='Y'}">
                                                        Active
                                                    </c:if>
                                                    <c:if test="${pc.activeInd=='N'}">
                                                        In-Active
                                                    </c:if>
                                                </td>



                                                <c:if test="${pc.status==0}">
                                                    <td align="left"><font color="green">Ready To Submit</font></td>
                                                        </c:if>
                                                        <c:if test="${pc.status==1}">
                                                    <td align="left"><font color="red">Dealer Code Already Exits Temp</font></td>
                                                            <%count++;%>
                                                        </c:if>
                                                    <c:if test="${pc.status==2}">
                                                    <td align="left"><font color="red">Dealer Code Already Exits Main Table</font></td>
                                                            <%count++;%>
                                                        </c:if>


                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                    </tbody>
                                </table>

                                <center>

                                    <td colspan="0"><input type="button" class="btn btn-success" value="Submit" name="submitPage" id="submitPage" onclick="insertTo();"></td>
                                </center>
                            </c:if>
                        </div>
                    </div>     

                </form>-->






                <form name="instaMart" method="post">
                    <%@include file = "/content/common/message.jsp"%>
                    <table class="table table-info mb30 table-hover">
                        <input type="hidden" name="productId" id="productId" value=""/>
                        <tr>

                            <td> Warehouse   &emsp;</td>
                            <td>
                             <select name="warehouse" id="warehouse"  class="form-control" style="width:240px;height:40px" style="height:20px; width:122px;" >
                                        <c:if test="${getWareHouseList != null}">
                                            <option value="" selected>--Select--</option>
                                            <c:forEach items="${getWareHouseList}" var="companyList">
                                                <option value='<c:out value="${companyList.warehouseId}"/>'><c:out value="${companyList.whName}"/></option>
                                            </c:forEach>
                                        </c:if>
                             </select>
                            </td>

                            <td> Date   &emsp;</td>
                            <td><input  name="date" id="date" class="datepicker form-control" style="width:240px;height:40px" value=""></td>

                        </tr>

                        <tr>

                            <td> Packets   &emsp;</td>
                            <td><input  name="packets" class="form-control" id="packets" style="width:240px;height:40px" value=""></td>

                            <td> Tons   &emsp;</td>
                            <td><input name="tons" class="form-control" id="tons" style="width:240px;height:40px" value=""></td>

                        </tr>

                        <tr>

                            <td> Milk   &emsp;</td>
                            <td><input  name="milk" id="milk" class="form-control" style="width:240px;height:40px" value=""></td>

                            <td> F & V  &emsp;</td>
                            <td><input  name="FAndV" id="FAndV" class="form-control" style="width:240px;height:40px" value=""></td>

                        </tr>

                    
                    </table>
                    <center><input type="button" class="btn btn-success"  name="save" id="save"  value="Save" onclick="dailyOutward()"></center>

                    </table>

                </form>

                <br>


                <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                        <tr height="30" style="colour:blue">
                            <th>Sno</th>
                            <th> Warehouse </th>
                            <th> Date </th>
                            <th> Packets </th>
                            <th> Tons </th>
                            <th> Milk </th>
                            <th> F & V </th>
<!--                            <th>Action</th>-->


                    </thead>

                    <%int sno1 = 1;%>
                    <tbody>
                        <c:if test="${getDailyOutWard !=null}">
                            <c:forEach items="${getDailyOutWard}" var="pc">


                                <tr>
                                    <td align="left"> <%=sno1%> </td>
                                      <td align="left"> <c:out value="${pc.whId}"/></td>
                                                <td align="left"> <c:out value="${pc.date}"/> </td>
                                                <td align="left"> <c:out value="${pc.fandv}"/></td>
                                                <td align="left"> <c:out value="${pc.packets}"/> </td>
                                                <td align="left"> <c:out value="${pc.tons}"/> </td>
                                                <td align="left"> <c:out value="${pc.milk}"/> </td>
                                                <td align="left"> <c:out value="${pc.createdOn}"/> </td>
                                    <!--<td> <input type="checkbox"  value="save" id="edit<%=sno1%>" onclick="updateInstaMart(<%=sno1%>, '<c:out value="${pc.productName}"/>', '<c:out value="${pc.skuCode}"/>', '<c:out value="${pc.productDescription}"/>', '<c:out value="${pc.minimumStockQty}"/>', '<c:out value="${pc.uom2}"/>', '<c:out value="${pc.temperature}"/>', '<c:out value="${pc.storage}"/>', '<c:out value="${pc.activeInd}"/>', '<c:out value="${pc.productId}"/>');"/></td>-->
                                </tr>
                                <%sno1++;%>
                            </c:forEach>
                        </tbody>
                    </c:if>
                </table>
            </div>
        </div>
    </div>
    <input type = "hidden" name="count" id="count" value="<%=sno1%>"/>
    <%@include file="/content/common/NewDesign/settings.jsp"%>
</html>