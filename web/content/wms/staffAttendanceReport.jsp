<%-- 
    Document   : warehouseOrderReport
    Created on : 22 Feb, 2021, 11:44:36 AM
    Author     : entitle
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.WarehouseWise Order report"  text="WarehouseWise Order report"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.WarehouseWise Order report"  text="Report"/></a></li>
            <li class="active">WarehouseWise Order report</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="">
                <form name="whreport" method="post">

                    <table class="table table-info mb30 table-hover" id="pincode">
                        <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                        </div>
                        <thead><tr>
                                <th class="contenthead" colspan="8">WarehouseWise Order report</th>
                            </tr></thead>  
                        <tr>
                            <td>From Date</td>
                            <td><input type="text" class="datepicker" style="width:200px;height:35px" id="fromDate" name="fromDate" value='<c:out value="${fromDate}"/>'></td>
                            <td>To Date</td>
                            <td><input type="text" class="datepicker" style="width:200px;height:35px" id="toDate" name="toDate"  value='<c:out value="${toDate}"/>'></td>
                        </tr>
                        <tr>
                            <td>WareHouse</td>
                            <td> <select id="whId" name="whId" value="" style="width:200px;height:40px">
                                    <option value="">---select---</option>
                                    <c:forEach items="${AddwhList}" var="wh"><option value="<c:out value="${wh.whId}"/>"><c:out value="${wh.whName}"/></option>
                                    </c:forEach></select></td>
                        </tr>
                        <script>document.getElementById("whId").value =<c:out value="${whId}"/></script>
                    </table>  
                    <table>
                        <center>
                            <input type="button" value="search" id="search" class="btn btn-success" onclick="submitPage(this.value);">
                            &emsp;   <input type="button" value="Export Excel" id="search" class="btn btn-success" onclick="submitPage(this.value);">
                        </center><br><br>

                    </table>

                    <c:if test="${staffAttendanceReport!=null}">
                        <table class="table table-info mb30 table-bordered" id="table" >
                            <thead >
                                <tr >
                                    <th>Warehouse Name</th>
                                    <th>Attendance Date</th>
                                    <th>Support Staff</th>
                                    <th>House Keeping</th>
                                    <th>Security</th>
                                    <th>Delivery Executive</th>
                                    <th>Date Entry Operator</th>
                                    <th>Executive</th>
                                    <th>Other</th>
                                    <th>Total</th>
                                    <th>Updated By</th>
                                    <th>Updated On</th>
                                </tr>

                            </thead>
                            <% int index = 0;
                                    int sno = 1;
                            %>
                            <tbody>

                                <c:forEach items="${staffAttendanceReport}" var="wor">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <c:set var="staff" value="${staff+wor.staff}"/>
                                        <c:set var="houseKeeping" value="${houseKeeping+wor.houseKeeping}"/>
                                        <c:set var="security" value="${security+wor.security}"/>
                                        <c:set var="deliveryExecutive" value="${deliveryExecutive+wor.deliveryExecutive}"/>
                                        <c:set var="dataEntryOperator" value="${dataEntryOperator+wor.dataEntryOperator}"/>
                                        <c:set var="executive" value="${executive+wor.executive}"/>
                                        <c:set var="others" value="${others+wor.others}"/>
                                        <c:set var="totalStaff" value="${totalStaff+wor.others+wor.executive+wor.dataEntryOperator+wor.deliveryExecutive+wor.security+wor.houseKeeping+wor.staff}"/>

                                        <td ><c:out value="${wor.whName}"/></td>
                                        <td><c:out value="${wor.attendanceDate}"/></td>
                                        <td><c:out value="${wor.staff}"/></td>
                                        <td><c:out value="${wor.houseKeeping}"/></td>
                                        <td><c:out value="${wor.security}"/></td>
                                        <td><c:out value="${wor.deliveryExecutive}"/></td>
                                        <td><c:out value="${wor.dataEntryOperator}"/></td>
                                        <td><c:out value="${wor.executive}"/></td>
                                        <td><c:out value="${wor.others}"/></td>
                                        <td><c:out value="${wor.others+wor.executive+wor.dataEntryOperator+wor.deliveryExecutive+wor.security+wor.houseKeeping+wor.staff}"/></td>
                                        <td ><c:out value="${wor.name}"/></td>
                                        <td><c:out value="${wor.createdDate}"/>: <c:out value="${wor.createdTime}"/></td>
                                    </tr>

                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach> 
                                <tr>
                                    <td><b>Total</b></td>
                                    <td><b></b></td>
                                    <td><b><c:out value="${staff}"/></b></td>
                                    <td><b><c:out value="${houseKeeping}"/></b></td>
                                    <td><b><c:out value="${security}"/></b></td>
                                    <td><b><c:out value="${deliveryExecutive}"/></b></td>
                                    <td><b><c:out value="${dataEntryOperator}"/></b></td>
                                    <td><b><c:out value="${executive}"/></b></td>
                                    <td><b><c:out value="${others}"/></b></td>
                                    <td><b><c:out value="${totalStaff}"/></b></td>
                                    <td><b></b></td>
                                    <td><b></b></td>
                                    <td><b></b></td>

                                </tr>
                            </tbody>

                        </table> 
                    </c:if>  
                    <script>
                        function submitPage(val) {
                            document.whreport.action = '/throttle/viewStaffAttendanceReport.do?&param=' + val;
                            document.whreport.submit();
                        }
                    </script>


                </form>
            </body>
        </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>

    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

