<%-- 
    Document   : cratesReport
    Created on : 27 Dec, 2021, 11:24:46 AM
    Author     : alan
--%>



<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript"></script>
    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

    <script>
        function insertInstaMart() {
            var skuCode = document.getElementById("skuCode").value;
            var productName = document.getElementById("productName").value;
            var productDescription = document.getElementById("productDescription").value;
            var minimumStockQty = document.getElementById("minimumStockQty").value;
            var uom = document.getElementById("uom").value;
            var temperature = document.getElementById("temperature").value;
            var storage = document.getElementById("storage").value;
            var activeInd = document.getElementById("activeInd").value;
            if (activeInd != "0") {

                console.log(skuCode, productName, productDescription, minimumStockQty, uom, temperature, storage, activeInd);
                if (skuCode != "" && productName != "" && productDescription != "" && minimumStockQty != "" && uom != "" && temperature != "" && storage != "" && activeInd != "")
                {
                    document.instaMart.action = "/throttle/instaMartProductMaster.do?param=save";
                    document.instaMart.submit();
                }

                else {
                    alert("Enter The Value");
                }
            } else {
                alert("Please Select Active Not Active ");
            }
        }

    function uploadPage() {
            document.upload.action = "/throttle/dzProductUpload.do?param=upload";
            document.upload.submit();
    
    }

        function updateInstaMart(sno, productName, skuCode, productDescription, minimumStockQty, uom, temperature, storage, activeInd, productId) {

            document.getElementById("productName").value = productName;
            document.getElementById("skuCode").value = skuCode;
            document.getElementById("productDescription").value = productDescription;
            document.getElementById("minimumStockQty").value = minimumStockQty;
            document.getElementById("uom").value = uom;
            document.getElementById("temperature").value = temperature;
            document.getElementById("storage").value = storage;
            document.getElementById("activeInd").value = activeInd;
            document.getElementById("productId").value = productId;


            var count = parseInt(document.getElementById("count").value);
            for (i = 1; i <= parseInt(count); i++) {
                if (i != sno) {
                    document.getElementById("edit" + i).checked = false;
                } else {
                    document.getElementById("edit" + i).checked = true;
                }
            }
        }
        
        
 function insertTo(){
    
            
            var result = confirm("Some errors are not Updated. Do you want to continue?");
            if (result == true) {
                document.upload.action = "/throttle/dzInsertIntoProductMaster.do?";
                document.upload.method = "post";
                document.upload.submit();
            }
            else {
            
            document.upload.action = "/throttle/dzInsertIntoProductMaster.do?";
            document.upload.method = "post";
            document.upload.submit();
        }
    }
       
     function checkSelectStatus(sno, obj) {
        var val = document.getElementsByName("selectedStatus");
        if (obj.checked == true) {
            document.getElementById("selectedStatus" + sno).value = 1;
        } else if (obj.checked == false) {
            document.getElementById("selectedStatus" + sno).value = 0;
        }
        var vals = 0;
        for (var i = 0; i <= val.length; i++) {
        }
    }
    
     function printIt(val) {
        document.upload.action = "/throttle/dzPickListMasterView.do?param=print&invoiceId="+val;
        document.upload.submit();

    }
    
      function generatePage() {
        document.upload.action = "/throttle/dzPickListMaster.do?";
        document.upload.submit();
    }
       
        

    </script>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Crates Report</h2>

        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Report</a></li>
                <li class="">Crates Report</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <form name="upload"  method="post" enctype="multipart/form-data">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>
                    
                    
           
                    
                    

                    <table class="table table-info mb30 table-hover" id="uploadTable"  >
                        

                    
                    </table>
              
                    
                    <div id="ptp">
                        <div class="inpad" style=" overflow-x: visible;">
                            <table class="table table-info mb30 table-hover" style="width:100%">
                                <thead>
                                    <tr><th colspan="7">Crates Report</th></tr>
                                    
                                </thead>
                                <br>
                                <tr><th colspan="7"></th></tr>
                                <tr>
                                    <td>From Date</td>
                                    <td><input type="text" class="datepicker form-control" autocomplete="off" name="fromDate" id="fromDate" style="width:200px;height:40px"/></td>
                                    <td>To Date</td>
                                    <td><input type="text" class="datepicker form-control" autocomplete="off" name="toDate" id="toDate" style="width:200px;height:40px"/></td>
                                </tr>
                                <tr>
                                    <td  ><font color="red">*</font>Crates Status</td>
                                    <td>
                                        <select class="form-control" id="orderType" name="orderType" style="width:200px;height: 40px">
                                            <option value="">------Select One------</option>
                                            <option value="1">Pending</option>
                                            <option value="2">Inward</option>
                                            <option value="2">Outward</option>
                                            <option value="3">All</option>
                                        </select>
                                    </td>
                                    <td>option</td>
                                    <td><input type="text" class="form-control" name="vehicleNo" id="vehicleNo" style="width:200px;height:40px"/></td>
                                </tr>
                                <tr>
                                    <td><font color="red">*</font>option</td>
                                    <td>
                                        <select class="form-control" id="empId" name="empId" style="width:200px;height: 40px">
                                            <option value="">------Select One------</option>
                                            <c:forEach items="${deliveryExecutiveList}" var="de">
                                                <option value="<c:out value="${de.empId}"/>"><c:out value="${de.empName}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                            </table>

                            
                            
               <center>
                    <input type="button" class="btn btn-success" name="Print"  id="Print"  value="Search" onclick="printIt('<c:out value="${invoiceIds}"/>');">&nbsp;&nbsp;

                    <input type="button" class="btn btn-success" name="Print"  id="Print"  value="Export Excel" onclick="printIt('<c:out value="${invoiceIds}"/>');">&nbsp;&nbsp;

                </center>  
                    
                    <br>
                            
                                <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                    <thead style="font-size:12px">
                                        <tr height="40">
                                            <th>Sno</th>
                                            <th>Outward Date</th>
                                            <th>Dealer Name</th>
                                            <th>Open Stock</th>
                                            <th>Outward</th>
                                            <th>Inward</th>
                                            <th>Closing</th>
                                          
                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                     int count=0;
                                    %> 
                                    
                                    <c:if test = "${getCratesReportDealerBased != null}" >
                                    
                                    <tbody>

                                        <c:forEach items="${getCratesReportDealerBased}" var="pc">


                                            <tr>
                                                <td align="left"> <%=sno%> </td>
                                                <td align="left"><c:out value="${pc.date}"/></td>
                                                <td align="left"><c:out value="${pc.dealerName}"/></td>
                                                <td align="left"><c:out value="${pc.pendingQty1 + pc.outwardQty + pc.inwardQty}"/></td>
                                                <td align="left"><c:out value="${pc.outwardQty}"/></td>
                                                <td align="left"><c:out value="${pc.inwardQty}"/></td>
                                                <td align="left"><c:out value="${pc.pendingQty1 + pc.outwardQty + pc.inwardQty -pc.inwardQty - pc.outwardQty}"/></td>
                                               

                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                    </tbody>
                                </table>
                              
                            </c:if>

                        </div>
                    </div>     
                                    
                                    
                 
                    
                         

                </form>

                <br>
                      </div>
        </div>
    </div>
    <%@include file="/content/common/NewDesign/settings.jsp"%>
</html>
