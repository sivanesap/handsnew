<%-- 
    Document   : ninja
    Created on : 20 Dec, 2021, 1:05:16 AM
    Author     : alan
--%>





<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript"></script>

    
    
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>


        <script type="text/javascript">
   
        function dailyOutward() {
            var type = document.getElementById("type").value;
            var date = document.getElementById("date").value;
            var grnQty = document.getElementById("grnQty").value;
            var remarks = document.getElementById("remarks").value;
  
           
        

                if (type == ""){
                    alert("Enter The Warehouse Data");
                }
                else if (date == ""){
                    alert("Enter The Date Data");
                }
                else if (grnQty == ""){
                    alert("Enter The Packets Data");
                }
                else if (remarks == ""){
                    alert("Enter The Tons Data");
                    
                }
             
                else 
                {
                    document.instaMart.action = "/throttle/imDailyLUDetails.do?param=save";
                    document.instaMart.submit();
                }

        }



        function updateInstaMart(sno, productName, skuCode, productDescription, minimumStockQty, uom, temperature, storage, activeInd, productId) {

            document.getElementById("productName").value = productName;
            document.getElementById("skuCode").value = skuCode;
            document.getElementById("productDescription").value = productDescription;
            document.getElementById("minimumStockQty").value = minimumStockQty;
            document.getElementById("uom").value = uom;
            document.getElementById("temperature").value = temperature;
            document.getElementById("storage").value = storage;
            document.getElementById("activeInd").value = activeInd;
            document.getElementById("productId").value = productId;


            var count = parseInt(document.getElementById("count").value);
            for (i = 1; i <= parseInt(count); i++) {
                if (i != sno) {
                    document.getElementById("edit" + i).checked = false;
                } else {
                    document.getElementById("edit" + i).checked = true;
                }
            }
        }
        
        
 
        

    </script>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Daily Lotting And Unloading Details</h2>

        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Instamart</a></li>
                <li class="">Daily Lotting And Unloading Details</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">







                <form name="instaMart" method="post">
                    <%@include file = "/content/common/message.jsp"%>
                    <table class="table table-info mb30 table-hover">
                        <input type="hidden" name="productId" id="productId" value=""/>
                        <tr>

                            <td> Type   &emsp;</td>
                            <td>
                             <select name="type" id="type"  class="form-control" style="width:240px;height:40px" style="height:20px; width:122px;" >
                                       
                                            <option value="" selected>--Select--</option>
                                            <option value="Lotting" selected>Lotting</option>
                                            <option value="Unloading" selected>Unloading</option>
                                
                             </select>
                            </td>

                            <td> Date   &emsp;</td>
                            <td><input  name="date" id="date" class="datepicker form-control" style="width:240px;height:40px" value=""></td>

                        </tr>

                        <tr>

                            <td> GRN Qty   &emsp;</td>
                            <td><input  name="grnQty" class="form-control" id="grnQty" style="width:240px;height:40px" value=""></td>

                            <td> Remarks  &emsp;</td>
                            <td><input type = "text" name="remarks" class="form-control" id="remarks" style="width:240px;height:80px" value=""></td>

                        </tr>

                  

                    
                    </table>
                    <center><input type="button" class="btn btn-success"  name="save" id="save"  value="Save" onclick="dailyOutward()"></center>

                    </table>

                </form>

                <br>


                <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                        <tr height="30" style="colour:blue">
                            <th>Sno</th>
                            <th> Type </th>
                            <th> GRN Qty </th>
                            <th> Date </th>
                            <th> Remarks </th>
                           
<!--                            <th>Action</th>-->


                    </thead>

                    <%int sno1 = 1;%>
                    <tbody>
                        <c:if test="${imDailyLUDetails !=null}">
                            <c:forEach items="${imDailyLUDetails}" var="pc">
                                <tr>
                                    <td align="left"> <%=sno1%> </td>
                                      <td align="left"> <c:out value="${pc.type}"/></td>
                                                <td align="left"> <c:out value="${pc.grnQty}"/></td>
                                                <td align="left"> <c:out value="${pc.date}"/> </td>
                                                <td align="left"> <c:out value="${pc.remarks}"/> </td>
                                              
                                </tr>
                                <%sno1++;%>
                            </c:forEach>
                        </tbody>
                    </c:if>
                </table>
            </div>
        </div>
    </div>
    <input type = "hidden" name="count" id="count" value="<%=sno1%>"/>
    <%@include file="/content/common/NewDesign/settings.jsp"%>
</html></html>