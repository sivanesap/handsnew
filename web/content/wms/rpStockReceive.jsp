<%-- 
    Document   : rpStockTransfer
    Created on : 23 Sep, 2021, 12:56:04 PM
    Author     : alan
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });



  
   

</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Stock Receive </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Repose</a></li>
                <li class="">Stock Receive</li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="connect"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                    
                     

                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30">
                                    <th>S.No</th>
                                    <th>Transfer No</th>
                                    <th>From Warehouse Name</th>
                                    <th>To Warehouse Name</th>
                                    <th>Vehicle No</th>
                                    <th>Driver Name</th>
                                    <th>Driver Mobile</th>
                                    <th>Quantity</th>
                                    <th>View</th>
                                  
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                                <c:if test = "${rpStockReceive != null}">
                                    <c:forEach items="${rpStockReceive}" var="pc">
                                        <%
                                            sno++;
                                            String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.transferNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.fromWhName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.toWhName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.vehicleNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.driverName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.mobileNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.qty}" /></td>
                                            <td class="<%=className%>"  align="center"> <input type="button" class="btn btn-success" value = "View" id="edit<%=sno%>" onclick="viewPage('<c:out value="${pc.transferId}" />',<%= sno%>);" /></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>
                        <input type="hidden" name="count" id="count" value="<%=sno%>" />
                        <script>
                            function viewPage(transferId, sno){
                            document.connect.action="/throttle/rpStockReceive.do?param=view&transferId="+transferId;
                            document.connect.submit();
                        }
                        </script>
                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>