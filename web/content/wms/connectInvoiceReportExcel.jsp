<%-- 
    Document   : connectInvoiceReportExcel
    Created on : 2 Sep, 2021, 2:41:47 PM
    Author     : Roger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Excel";
        request.setAttribute("menuPath", menuPath);
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "Connect Invoice Report-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test = "${connectInvoiceReport != null}" >
                <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                    <thead>
                        <tr height="40">
                            <th>S. No</th>
                            <th>Plant Code</th>
                            <th>Customer</th>
                            <th>City</th>
                            <th>Pincode</th>
                            <th>Invoice Date</th>
                            <th>Invoice Number</th>
                            <th>Pre-Tax Value</th>
                            <th>Total Qty</th>
                            <th>Status</th>
                            <th>LR Number</th>
                            <th>POD Status</th>
                            <th>Appointment Date</th>
                        </tr>
                    </thead>
                    <% int index = 0;
                        int sno = 1;
                    %>
                    <tbody>
                        <c:forEach items="${connectInvoiceReport}" var="in">

                        <td><%=sno%></td>
                        <td><c:out value="${in.plantCode}"/></td>
                        <td><c:out value="${in.customerName}"/></td>
                        <td><c:out value="${in.city}"/></td>
                        <td><c:out value="${in.pincode}"/></td>
                        <td><c:out value="${in.invoiceDate}"/></td>
                        <td><c:out value="${in.invoiceNo}"/></td>
                        <td><c:out value="${in.preTaxValue}"/></td>
                        <td><c:out value="${in.qty}"/></td>
                        <td><c:out value="${in.status}"/></td>
                        <td><c:out value="${in.lrNo}"/></td>
                        <td>
                                        <c:if test="${in.podStatus=='0'}">
                                            Pending
                                        </c:if>
                                        <c:if test="${in.podStatus=='1'}">
                                            Approval Pending
                                        </c:if>
                                        <c:if test="${in.podStatus=='2'}">
                                            Approved
                                        </c:if>
                                        <c:if test="${in.podStatus=='3'}">
                                            Rejected
                                        </c:if>
                         </td>
                        
                        <td><c:out value="${in.appointmentDate}"/></td>
                            </tr>
                            <%index++;%>
                            <%sno++;%>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>

            <br>
            <br>

        </form>
    </body>
</html>