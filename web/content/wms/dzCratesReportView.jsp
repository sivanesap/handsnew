<%-- 
    Document   : dzCratesReportView
    Created on : 4 Jan, 2022, 5:00:08 PM
    Author     : alan
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>


<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Crates report view</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="general-forms.html">Shop Tree</a></li>
            <li class="active">Crates reports view</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="connect" method="post">
                    <br>
                    <br>
                    <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                        <thead style="font-size:12px">
                            <tr height="40">
                                <th  height="30" >S No</th>
                                <th  height="30" >Sub LR code</th>
                                <th  height="30" >Crate Number</th>
                                <th  height="30" >Outward Date</th>
                                <th  height="30" >Outward Time</th>
                                <th  height="30" >Inward Date</th>
                                <th  height="30" >Inward Time</th>
                                <th  height="30" >Inward vehicle number</th>


                            </tr>
                        </thead>
                        <% int index = 0;
                            int sno = 1;
                            int count = 0;
                        %> 
                        <tbody>

                            <c:forEach items= "${cratesReportView}" var="report">
                                <tr height="30">
                                    <td align="left"><%=sno%></td>
                                    <td align="left"   ><c:out value="${report.subLr}"/></td>
                                    <td align="left"   ><c:out value="${report.crateNo}"/></td>
                                    <td align="left"   ><c:out value="${report.outwardDate}"/></td>
                                    <td align="left"   ><c:out value="${report.outwardTime}"/></td>
                                    <td align="left"   ><c:out value="${report.inwardDate}"/></td>
                                    <td align="left"   ><c:out value="${report.inwardTime}"/></td>
                                    <td align="left"   ><c:out value="${report.vehicleNo}"/></td>
                                </tr> 
                                <%
                                    sno++;
                                    index++;
                                %>
                            </c:forEach>
                        <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                        </tbody>
                    </table>




                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>


                    <br/>
                    <br/>

                    <br/>
                    <br/>
                    <br/>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>