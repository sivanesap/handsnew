<%-- 
    Document   : grnDetails
    Created on : Jan 21, 2021, 12:47:26 PM
    Author     : throttle
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Receive GRN"  text="Receive GRN"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Receive GRN"  text="Receive GRN"/></a></li>
            <li class="active">Receive GRN</li>
        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="">
                
                    <table class="table table-info mb30 table-hover" id="serial">
                            <div align="center" style="height:20px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>
                        <tr>
                            <td align="center">Product Code</td>
                           <td> <input type="hidden" name="proId" id="proId" value="<c:out value="${proId}"/>"/>
                           <input type="hidden" name="proName" id="proName" value="<c:out value="${proName}"/>"/>
                           <input type="text" name="proCode" id="proCode" value="<c:out value="${proCode}"/>"  class="form-control" style="width:250px;height:40px"/>
                           </td>
                           <td align="center">Product Name</td>
                           <td><input type="text" name="proCode" id="proCode" value="<c:out value="${proName}"/>"  class="form-control" style="width:250px;height:40px"/></td>
                        </tr>
                        <tr>
                            <td align="center">Input Qty</td>
                            <td><input type="text" id="inpQty" name="inpQty" value="" class="form-control" style="width:250px; height:30px"> 
                       </td>
                        </tr>
                        <tr>
                            <td>
                                Product Condition
                            </td>
                            <td><select id="proCon" name="proCon" style="width:250px;height:40px">
                                <option value="Good">Good</option>
                                <option value="Damaged">Damaged</option>>
                            </select></td>
                            
                            <td>
                                UOM
                            </td>
                            <td><select id="Uom" name="Uom" style="width:250px;height:40px">
                                <option value="Pack">Pack</option>
                                <option value="Piece">Piece</option>>
                            </select></td>
                        </tr>
                        <tr>
                            <td align="center">
                                SERIAL NO : 
                            </td>
                            <td>
                                <input type="text" id="serialNo" name="serialNo" value="" class="form-control" style="width:250px;height:30px" onchange="saveTempSerialNos();"/> 
                            </td>
                        </tr>
                        
                    </table>                        
                    
                    <br> 
                 <div id="tableContent"></div>
                        <script>
                            var serial = [];
                            var ipqt=[];
                            var uomarr=[];
                            var sno=[];
                            var procon=[];
                            var arr=[];
                            var i=0;
                            function saveTempSerialNos() {

                                   serial[i]=serialNo.value;
                                   ipqt[i]=inpQty.value;
                                   uomarr[i]=Uom.value;
                                   procon[i]=proCon.value;
                                   sno[i]=i+1;
                                   i++;
                                   const tmpl = (sno,serial,uomarr,procon,ipqt) => `
                                   <table class="table table-info mb30 table-bordered" id="table"><thead>
                                   <tr><th>S.No<th>Serial No<th>UOM<th>Product Condition<th>Input Qty<tbody>
                                   ${sno.map( (cell, index) => `
                                   <tr><td><input type="hidden" id="sNo" name="sNo" value="${cell}" />${cell}<td><input type="hidden" id="serialNos" name="serialNos" value="${serial[index]}" />${serial[index]}<td><input type="hidden" id="uom" name="uom" value="${uomarr[index]}" />${uomarr[index]}<td><input type="hidden" id="proCond" name="proCond" value="${procon[index]}" />${procon[index]}<td><input type="hidden" id="ipQty" name="ipQty" value="${ipqt[index]}"/>${ipqt[index]}</tr>
                                   `).join('')}
                                   </table>`;
                                    tableContent.innerHTML=tmpl(sno,serial,uomarr,procon,ipqt);
                               }
                              
                        </script>
                        <script type="text/javascript">
    //auto com

                 $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
                 $('#proCode').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getProductList.do",
                    dataType: "json",
                    data: {
                        procode: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Code;
                var tmp = value.split('-');
                $('#proCode').val(tmp[0]);
                $('#proId').val(tmp[1]);
                $('#proName').val(tmp[2]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Code;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[0] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


</script>
                         <script>
                            function getProductName(value){
                                alert(value);
                            }
                            function resetTheDetails() {
                                $('#receivedGRNTempDetails').html('');
                            }
                            function saveGrnSerialNos() {
                                document.grn.action = '/throttle/saveGrnSerialDetails.do';
                                document.grn.submit();
                            }
                         </script>
                         
                        <center>
                            <input type="button" class="btn btn-success" name="Update" value="Update" onclick="saveGrnSerialNos(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;
                        </center>
                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        </div>
    </div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

