<%-- 
    Document   : imInvoiceViewDetails
    Created on : 30 Nov, 2021, 6:56:31 PM
    Author     : Vishal
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>


<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });


    function exportExcel() {
        document.connect.action = '/throttle/connectLrView.do?param=Export';
        document.connect.submit();
    }


</script>

<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Invoice View</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="general-forms.html">Instamart</a></li>
            <li class="active">Invoice View</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="connect" method="post">
                    <br>
                    <br>
                    <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                        <thead style="font-size:12px">
                            <tr height="40">
                                <th  height="30" >S No</th>
                                <th  height="30" >LR No</th>
                                <th  height="30" >Invoice No</th>
                                <th  height="30" >Invoice Date</th>
                                <th  height="30" >Customer Name</th>
                                <th  height="30" >Billed Qty</th>
                                <th  height="30" >Dispatch Qty</th>
                                <th  height="30" >No Of Crates</th>
                                <th  height="30" >Invoice Amount</th>
                            </tr>
                        </thead>
                        <% int index = 0;
                            int sno = 1;
                            int count = 0;
                        %> 
                        <tbody>

                            <c:forEach items= "${getImInvoiceDetails}" var="list">
                                <tr height="30">
                                    <td align="left"><%=sno%></td>
                                    <td align="left"   ><c:out value="${list.lrNo}"/></td>
                                    <td align="left"   ><c:out value="${list.invoiceNo}"/></td>
                                    <td align="left"   ><c:out value="${list.invoiceDate}"/></td>
                                    <td align="left"   ><c:out value="${list.dealerName}"/></td>
                                    <td align="left"   ><c:out value="${list.qty}"/></td>
                                    <td align="left"   ><c:out value="${list.quantity}"/></td>
                                     <td align="left"   ><c:out value="${list.noOfCrates}"/></td>
                                    <td align="left"   ><c:out value="${list.invoiceAmount}"/></td>
                                </tr> 
                                <%
                                    sno++;
                                    index++;
                                %>
                            </c:forEach>
                        <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                        </tbody>
                    </table>




                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>


                    <br/>
                    <br/>

                    <br/>
                    <br/>
                    <br/>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
