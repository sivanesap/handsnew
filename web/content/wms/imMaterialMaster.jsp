<%-- 
    Document   : imMaterialMaster
    Created on : 19 Oct, 2021, 12:46:24 PM
    Author     : Roger
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    function submitPage() {
        if (document.getElementById("materialName").value == "") {
            alert("Enter Material Name");
            document.getElementById("materialName").focus();
        } else if (document.getElementById("materialCode").value == "") {
            alert("Enter Material Code");
            document.getElementById("materialCode").focus();
        } else if (document.getElementById("materialDescription").value == "") {
            alert("Enter Material Group");
            document.getElementById("materialDescription").focus();
        } else if (document.getElementById("size").value == "") {
            alert("Enter Piece Per Unit");
            document.getElementById("size").focus();
        } else if (document.getElementById("uom").value == "") {
            alert("Enter UOM/Unit");
            document.getElementById("uom").focus();
        } else if (document.getElementById("qty").value == "") {
            alert("Enter Unit Quantity");
            document.getElementById("qty").focus();
        } else {
            document.material.action = " /throttle/imMaterialMaster.do?param=save";
            document.material.method = "post";
            document.material.submit();
        }
    }
    function setValues(sno, materialId, materialName, materialCode, materialDescription, size, unit, price, activeInd) {
        var count = parseInt(document.getElementById("count").value);
        for (var i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        unit = unit.replace(" - ","~");
        document.getElementById("materialId").value = materialId;
        document.getElementById("materialName").value = materialName;
        document.getElementById("materialCode").value = materialCode;
        document.getElementById("materialDescription").value = materialDescription;
        document.getElementById("size").value = size;
        document.getElementById("uom").value = unit;
        document.getElementById("qty").value = price;
        document.getElementById("activeInd").value = activeInd;
    }







    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }



    var httpRequest;
    function checkproductCategoryName() {

        var productCategoryName = document.getElementById('productCategoryName').value;

        var url = '/throttle/checkProductCategory.do?productCategoryName=' + productCategoryName;
        if (window.ActiveXObject) {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest) {
            httpRequest = new XMLHttpRequest();
        }
        httpRequest.open("GET", url, true);
        httpRequest.onreadystatechange = function() {
            processRequest();
        };
        httpRequest.send(null);

    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#productCategoryNameStatus").text('Please Check Product Category Name: ' + val + ' is Already Exists');
                } else {
                    $("#nameStatus").hide();
                    $("#productCategoryNameStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Material Master </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html">InstaMart</a></li>
                <li class="">Material Master</li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="document.material.materialName.focus();">


                    <form name="material"  method="post" >
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <br>
                        <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                            <input type="hidden" name="materialId" id="materialId" value=""  />
                            <tr>
                                <!--<table  border="0" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">-->
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="8" >Material Master</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Material Name</td>
                                    <td><input type="text" id="materialName" name="materialName" class="form-control" style="width:240px;height:40px"></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Material Code</td>
                                    <td class="text1"><input type="text" name="materialCode" id="materialCode" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Material Group</td>
                                    <td class="text1">
                                        <select name="materialDescription" id="materialDescription" class="form-control" style="width:240px;height:40px" autocomplete="off">
                                            <option value="">------Select One------</option>
                                            <c:forEach items="${materialGroup}" var="gr">
                                                <option value="<c:out value="${gr.groupName}"/>"><c:out value="${gr.groupName}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>
                                        UOM/Unit
                                    </td>
                                    <td class="text1">
                                        <select name="uom" id="uom" class="form-control" style="width:240px;height:40px" autocomplete="off">
                                            <option value="">------Select One------</option>
                                            <c:forEach items="${materialUom}" var="uom">
                                                <option value="<c:out value="${uom.uomCode}"/>~<c:out value="${uom.uomName}"/>"><c:out value="${uom.uomCode}"/> - <c:out value="${uom.uomName}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Piece/Unit</td>
                                    <td class="text1"><input type="text" name="size" id="size" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Unit Qty</td>
                                    <td class="text1"><input type="text" name="qty" id="qty" value="1" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Status</td>
                                    <td class="text1">
                                        <select  align="center" class="form-control" style="width:240px;height:40px" name="activeInd" id="activeInd" >
                                            <option value='Y'>Active</option>
                                            <option value='N' id="inActive" style="display: none">In-Active</option>
                                        </select>
                                    </td>
                                </tr>

                            </table>
                            </tr>
                            <tr>
                                <td>
                                    <br>
                                    <center>
                                        <input type="button" class="btn btn-success" value="Save" name="Submit" onClick="submitPage()">
                                    </center>
                                </td>
                            </tr>
                        </table>
                        <br>

                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30" style="color: white">
                                    <th>S.No</th>
                                    <th>Material Name</th>
                                    <th>Material Code</th>
                                    <th>Material Description</th>
                                    <th>UOM/Unit</th>
                                    <th>Unit Qty</th>
                                    <th>Piece/Unit</th>
                                    <th>Active</th>
                                    <th>Select</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                                <c:if test = "${materialMaster != null}">
                                    <c:forEach items="${materialMaster}" var="pc">
                                        <%
                                                    sno++;
                                                    String className = "text1";
                                                    if ((sno % 1) == 0) {
                                                        className = "text1";
                                                    } else {
                                                        className = "text2";
                                                    }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.material}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.materialCode}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.materialDescription}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.unit}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.qty}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.size}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.activeType}" /></td>
                                            <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${pc.materialId}" />', '<c:out value="${pc.material}" />', '<c:out value="${pc.materialCode}" />', '<c:out value="${pc.materialDescription}" />', '<c:out value="${pc.size}" />', '<c:out value="${pc.unit}" />', '<c:out value="${pc.qty}" />', '<c:out value="${pc.activeType}" />');" /></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>


                        <input type="hidden" name="count" id="count" value="<%=sno%>" />

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>

                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
