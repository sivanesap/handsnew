<%-- 
    Document   : ifbPodApproval
    Created on : 27 May, 2021, 11:34:17 AM
    Author     : Roger
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script>
        function uploadPod(dispatchId,subLr,subLrid) {
            window.open('/throttle/connectPodUpload.do?&param=upload&dispatchId=' + dispatchId+'&subLr='+subLr+'&subLrId='+subLrid);
        }
        function approvePod(depositId) {
            document.reconcile.action = '/throttle/deliveryPodUpload.do?&param=approve&depositId=' + depositId;
            document.reconcile.submit()
        }
        function showImage(name) {
            window.open("/throttle/uploadFiles/pod/" + name, "POD Image", "width=900, height=800");
        }
    </script>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Pod" text=" Pod"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Connect" text="Connect"/></a></li>
                <li class=""><spring:message code="hrms.label.Pod" text=" Pod"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">
                    <form name="reconcile" method="post">
                        <%@include file="/content/common/message.jsp" %>
                        <table class="table table-info mb30 table-bordered" id="table"  style="width:98%;" >
                            <thead>
                                <tr >
                                    <th >Sno</th>                                        
                                    <th >Dispatch Number</th>                                        
                                    <th >Lr.No</th>                                        
                                    <th >subLr.No</th>                                        
                                    <th >customer</th>                                        
                                    <th >Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 1;%>
                                <c:forEach items="${getSubLrList}" var="re">
                                    <%
                                        String className = "text1";
                                        if ((index % 2) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>

                                    <tr height="30">
                                        <td><%=index%></td>                                      
                                        <td><c:out value="${re.dispatchNo}"/></td>                                      
                                        <td><c:out value="${re.lrNumber}"/></td>                                      
                                        <td><c:out value="${re.subLr}"/></td>                                      
                                        <td><c:out value="${re.dealerName}"/></td>                                      
                                        <td>
                                            <c:if test="${re.status == 5}">
                                            <button class="label label-Primary" onclick="uploadPod('<c:out value="${dispatchId}"/>','<c:out value="${re.subLr}"/>','<c:out value="${re.subLrId}"/>');">
                                                    upload
                                                </button> 
                                            </c:if>
                                             <c:if test="${re.status == 0}">
                                                    waiting for approval
                                                    
                                                </c:if>
                                             <c:if test="${re.status == 1}">
                                                    approved
                                                    
                                                </c:if>
                                             <c:if test="${re.status == 2}">
                                                    rejected
                                                    
                                                </c:if>
                                           
                                            </td>
                                            

                                        <%index++;%>
                                    </c:forEach>
                            </tbody>
                        </table>

                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>
