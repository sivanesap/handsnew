<%-- 
    Document   : connectDeTat
    Created on : 23 Oct, 2021, 11:26:30 AM
    Author     : getDeTatEmpIdalan
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>


<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script type="text/javascript">






    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script type="text/javascript">
    function submitPage() {
        document.connect.action = "/throttle/connectDeTat.do?param=insert";
        document.connect.submit();
    }

    function setEmpName() {
        var empName = $('#empId option:selected').text();
        $('#empName').val(empName);
    }
        function setEmpName1() {
        var empName = document.getElementById("empId1").value;
        $('#empName').val(empName);
    }
    function updateTatStatus(tatId,sno){
        var outTime = document.getElementById("outTime"+sno).value;
        document.connect.action="/throttle/connectDeTat.do?param=update&outTime="+outTime+"&tatId="+tatId;
        document.connect.submit();
    }
    function selectEmpName() {

        var designationId = document.getElementById('desigId').value;
        $.ajax({
            url: "/throttle/getDeTatEmpId.do",
            dataType: "json",
            data: {
                item: designationId
            },
            success: function (data) {
                if (data != '') {
                    $('#empId').empty();
                    $('#empId').append(
                            $('<option></option>').val(0).html('--select--'))
                    $.each(data, function (i, data) {
                        $('#empId').append(
                                $('<option style="width:90px"></option>').attr("value", data.empIds).text(data.empNames)
                                )
                    });
                } else {
                    $('#empId').empty();
                    $('#empId').append(
                            $('<option></option>').val(0).html('--select--'))
                }
            }
        });
    }
 var valueChange = 1;


function changeFiled(){
   
      
    if (valueChange == 1){
        valueChange = 0
        document.getElementById("empId").style.display = "none";
        document.getElementById("empId1").style.display = "block";
        document.getElementById("empId").value = "";
        
        
    }
    else{
        document.getElementById("empId").style.display = "block";
        document.getElementById("empId1").style.display = "none";
    
     valueChange = 1;
    
        
    }
    
    
}

</script>


<style>
    #index td {
        color:white;
    }
</style>

<!--                                        <input type = "time" id = "bala" value="<c:out value="${list.time}"/>" onchange = "updateOutTime()">-->

<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Connect delivery executive TAT</h2>
    <h2 id="sombu"></h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="general-forms.html">Connect Operation</a></li>
            <li class="active">Connect delivery executive TAT</li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>

                <form name="connect" method="post">
                    <table class="table table-info mb30 table-hover" >
                        <tr>

                            <td><font color="red">*</font>Designation Name</td>
                            <td height="30">
                                <select id="desigId" class="form-control" style="width:240px;height: 40px" onchange="selectEmpName()" name="desigId">
                                    <option value="">------Select One------</option>
                                    <c:forEach items="${designationName}" var="wh">
                                        <option value="<c:out value="${wh.designationId}"/>"><c:out value="${wh.designationName}"/></option>
                                    </c:forEach>
                                </select>


                            </td>


                            <td><font color="red">*</font>Employee Name <input type="checkbox" onclick="changeFiled()"></td>
                            <td height="30">
                                  <input id="empId1" type = "text" name="empId1" style="width:240px;height:40px;display:none;"class="form-control" onchange="setEmpName1()"/>
                               
                                <select id="empId" name="empId" style="width:240px;height:40px"class="form-control" onchange="setEmpName()">
                                </select>
                                <input type="hidden" name="empName" id="empName" value="">

                        </tr>
                        <tr>
                            <td><font color="red">*</font>In-Date</td>
                            <td height="30"><input name="inDate" id="inDate"  class="form-control" style="width:240px;height:40px;"></td>
                            <td><font color="red">*</font>In-Time</td>
                            <td height="30"><input name="inTime" id="inTime" class="form-control" style="width:240px;height:40px;" type="time" value=""></td>
                        </tr>

                    </table>
                    <center>

                        <input type="button" class="btn btn-success" name="save"  id="Search"  value="Set Entry" onclick="submitPage(this.name);">

                    </center>
                    <br>
                    <script>
                        const date = new Date();
                        var d = date.getDate()+'-'+(date.getMonth()+1)+'-'+date.getFullYear();
                        document.getElementById("inDate").value=d;
                    </script>
                    <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                        <thead style="font-size:12px">
                            <tr height="40">
                                <th  height="30" >S No</th>
                                <!--                                <th  height="30" >Delivery Executive Id</th>-->
                                <th  height="30" >Employee Name</th>
                                <th  height="30" >Date</th>
                                <th  height="30" >In-Time</th>
                                <th  height="30" >Out-Time</th>


                            </tr>
                        </thead>
                        <% int index = 0;
                            int sno = 1;
                            int count = 0;
                        %> 
                        <tbody>

                            <c:forEach items= "${deliveryExecutiveDetails}" var="list">
                                <tr height="30">
                                    <td align="left"><%=sno%></td>
                                    <td align="left"   ><c:out value="${list.empName}"/></td>
                                    <td align="left"   ><c:out value="${list.date}"/></td>
                                      <td align="left"   ><c:out value="${list.inTime}"/></td>
                                      <td align="center"   >
                                          <c:if test="${list.status!=0&&list.status!=''}">
                                              <c:out value="${list.outTime}"/>
                                    </c:if>
                                          <c:if test="${list.status==0||list.status==''}">
                                              <input type="time" name="outTime" id="outTime<%=sno%>" style="width:180px;height:30px"/>
                                              &emsp;<span class="label label-Primary" style="cursor: pointer" onclick="updateTatStatus('<c:out value="${list.tatId}"/>', '<%=sno%>')">Save</span>
                                          </c:if>
                                        </td>                                                              


                                </tr> 
                                <%
                                    sno++;
                                    index++;
                                %>
                            </c:forEach>
                        <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                        </tbody>
                    </table>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>