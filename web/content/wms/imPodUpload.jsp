<%-- 
    Document   : imPodUpload
    Created on : 5 Jan, 2022, 12:28:40 PM
    Author     : alan
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>


<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script type="text/javascript">
    function submitPage(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            document.podUpload.action = '/throttle/imPodUpload.do?param=' + value;
            document.podUpload.submit();
        }
    }
    function checkSelectStatus(sno, obj) {
        var val = document.getElementsByName("selectedStatus");
        if (obj.checked == true) {
            document.getElementById("selectedStatus" + sno).value = 1;
        } else if (obj.checked == false) {
            document.getElementById("selectedStatus" + sno).value = 0;
        }

    }
    function updateDeliveryDate(lrNo, sno) {
        var deliveryDate = document.getElementById("deliveryDate" + sno).value;
        document.podUpload.action = '/throttle/imPodUpload.do?param=save&lrNo='+lrNo+ '&deliveryDate='+deliveryDate;
        document.podUpload.submit();
    }
    function uploadExcel() {
        document.upload.action = "/throttle/imPodUploadExcel.do?param=upload";
        document.upload.submit();
    }

</script>


<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>POD Upload</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="general-forms.html">InstaMart</a></li>
            <li class="active">POD Upload</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="upload" method="post" enctype="multipart/form-data">

<!--                    <table style="width:70%" class="table table-info table-hover" id="table3">
                        <tr >
                            <td >Delivery Date Update</td>
                            <td><input type="file" name="file1" id="file1"></td>
                            <td>
                                <input type="button" class="btn btn-success" name="Upload"  id="Upload" text="Upload" value = "Upload" onclick="uploadExcel();"/>
                            </td>
                        </tr>
                    </table>-->
                    <br>
                </form>
                <form name="podUpload" method="post" >

                    <table class="table table-info mb30 table-hover"  >
                        <thead>
                            <tr>
                                <th colspan="4">POD Upload</th>
                            </tr>
                        </thead>
                        <tr>

<!--                            <td align="center"><font color="red">*</font>Plant</td>
                            <td height="30"> <select name="whId" id="whId" class="form-control" style="width:260px;height:40px;" >
                                    <c:if test="${whList != null}">
                                        <option value="" selected>--<spring:message code="operations.reports.label.Select" text="default text"/>--</option>
                                        <c:forEach items="${whList}" var="wh">
                                            <option value='<c:out value="${wh.whId}"/>'><c:out value="${wh.whName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select></td>-->
                            <td><font color="red">*</font>Status</td>
                            <td height="30" >
                                <select name="statusType" id="statusType" style="width:260px;height:40px" class="form-control">
                                    <option value="">-----All-----</option>
                                    <option value="0">Pending</option>
                                    <option value="1">Upload</option>
                                    <option value="2">Approved</option>
                                    <option value="3">Rejected</option>
                                </select>
                            </td>
                        <script>
                            document.getElementById("statusType").value = '<c:out value="${statusType}"/>';
                        </script>
                        </tr>
                        <tr>
                            <td align="center"><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                            <td height="30"><input name="fromDate" id="fromDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                            <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                            <td height="30"><input name="toDate" id="toDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                        </tr>
                    </table>
                    <center>

                        <input type="button" class="btn btn-success" name="Search"  id="Search"  value="<spring:message code="operations.reports.label.Search" text="Search"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;
                        &emsp;
                        <input type="button" class="btn btn-success" name="ExportExcel"  id="ExportExcel"  value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;

                    </center>
                    <br>
                    <br>

                    <table  class="table table-info mb30 table-hover" id="table" >
                        <thead>
                            <tr >
                                <th>SNo</th>
                                <th style="width:200px;height:30px;">LRN No</th>
                                <th>Date</th>
                                <th>Consignee Name</th>
                                <th>Transporter Name</th>
                                <th>To Location</th>
                                <th>Vehicle Type</th>
                                <th>Invoice No</th>
                                <th>Invoice Amount</th>
                                <th>Action</th>
                            </tr>

                        </thead>
                        <tbody>
                            <% int index = 1;%>
                            <c:forEach items="${imPodUploadDetails}" var="dc">
                                <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                                %>



                                <tr >
                                    <td class="<%=classText%>"  align="center"><%=index++%></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.lrnNo}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.lrDate}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.consigneeName}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.vendorName}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.city}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.vehicleType}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.invoiceNo}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.invoiceAmount}"/></td>
                            
<!--                            <c:if test="${dc.deliveryDate!=''}">
                                <td class="<%=classText%>"  align="center">
                                    <c:out value="${dc.deliveryDate}"/>
                                </td>
                            </c:if>
                            <c:if test="${dc.deliveryDate==''}">
                                <td class="<%=classText%>"  align="center">
                                    <input type="text" class="form-control datepicker" style="width:150px;height:30px" name="deliveryDate" id="deliveryDate<%=index%>"/>
                                    <br><span class="label label-Primary" style="cursor: pointer" onclick="updateDeliveryDate('<c:out value="${dc.lrnNo}"/>', '<%=index%>')">Save</span>
                                </td>
                            </c:if>
                            <td class="<%=classText%>">
                                <c:out value="${dc.remarks}"/>
                            </td>-->
                                
                            <td>
                                <c:if test="${dc.podStatus != '3'}">
                                    <a onclick='uploadPage("<c:out value='${dc.sublrId}'/>", "<c:out value='${dc.dispatchid}'/>", "<c:out value='${dc.lrnNo}'/>", "<c:out value='${dc.dispatchDetailId}'/>", "<c:out value='${dc.lrDate}'/>", "<c:out value='${dc.custName}'/>", "<c:out value='${dc.invoiceNo}'/>", "<c:out value='${dc.invoiceDate}'/>")'>Upload POD</a>
                                </c:if>
                                <c:if test="${dc.podStatus == '3'}">
                                    <a onclick='uploadPage("<c:out value='${dc.sublrId}'/>", "<c:out value='${dc.dispatchid}'/>", "<c:out value='${dc.lrnNo}'/>", "<c:out value='${dc.dispatchDetailId}'/>", "<c:out value='${dc.lrDate}'/>", "<c:out value='${dc.custName}'/>", "<c:out value='${dc.invoiceNo}'/>", "<c:out value='${dc.invoiceDate}'/>")'>Upload POD</a>
                                    <br>
                                    <font color="red" style="font-weight:bold">(Rejected)</font>
                                </c:if>
                                <input type="hidden" name="selectedStatus" id="selectedStatus<%=index%>" value="0" />
                        <input type="hidden" name="dispatchid" id="dispatchid" value=""/>
                        <input type="hidden" name="subLrId" id="subLrId" value=""/>
                        <input type="hidden" name="lrnNo" id="lrnNo" value=""/>
                        <input type="hidden" name="dispatchDetailId" id="dispatchDetailId" value=""/>
                        <input type="hidden" name="lrdate" id="lrdate" value=""/>
                        <input type="hidden" name="CustomerName" id="CustomerName" value=""/>
                        <input type="hidden" name="invoiceNo" id="invoiceNo" value=""/>
                        <input type="hidden" name="invoiceDate" id="invoiceDate" value=""/>
                            </td>
                        </c:forEach>
                        </tbody>
                    </table>

                    <script>
                        function uploadPage(sublrId, dispatchId, lrnNo, dispatchDetailId, lrDate, customerName, invoiceNo,invoiceDate) {
//                            alert(sublrId)
                            $('#dispatchid').val(dispatchId);
                            $('#subLrId').val(sublrId);
                            $('#lrnNo').val(lrnNo);
                            $('#dispatchDetailId').val(dispatchDetailId);
                            $('#lrdate').val(lrDate);
                            $('#CustomerName').val(customerName);
                            $('#invoiceNo').val(invoiceNo);
                            $('#invoiceDate').val(invoiceDate);
                            document.podUpload.action = "/throttle/imUploadPage.do";
                            document.podUpload.submit();
                        }
                    </script>


                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>


                    <br/>
                    <br/>

                    <br/>
                    <br/>
                    <br/>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>