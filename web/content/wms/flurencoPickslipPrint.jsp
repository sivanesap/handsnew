<%-- 
    Document   : flurencoPickslipPrint
    Created on : 16 Aug, 2021, 8:46:07 AM
    Author     : Roger
--%>
<%--
    Document   : flurencoPickslipPrint
    Created on : 16 Aug, 2021, 2:40:14 PM
    Author     : entitle
--%>
<html>
    <head>
        <%@page import="java.util.Iterator"%>
        <%@page import="java.util.ArrayList"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script src="/throttle/js/JsBarcode.all.min.js"></script>
    <script src="JsBarcode.all.min.js"></script>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="JsBarcode.codabar.min.js"></script>


    <style type="text/css">
        @media print {
            #printbtn {
                display :  none;
            }
        }
    </style>



    <style type="text/css" media="print">
        @media print
        {
            @page {
                margin-top: 0;
                margin-bottom: 0;
            }
        }
    </style>
</head>

<body>
    <%int sno = 1;%>
    <table style="width:75mm;height:50mm">
        <tbody >
            <tr style="height:10px;"><td colspan="2" style="width:350px;text-align: center"><b>H&S Supply Chain Services Pvt Ltd</b></td></tr>
        <br>
        <tr style="height:30px;">
            <td colspan="1" style="width:50%;font-size:13px;text-align: center;padding-top: 30px;padding-bottom: 30px">NB00001</td>
            <td colspan="1" style="width:50%;text-align: center"><svg id="barcode<%=sno%>"  style="width:150px;height:50px"></svg></td>
</tr>
<tr>
    <td colspan="1" style="width:50%;font-size:12px;text-align: left;padding-top: 10px;padding-right:10px;padding-bottom: 10px;padding-left:10px;border-right: 2px solid black">
            <b>Order No</b>: OR1234<br>      
           PICO Dining Chair<br>

           <b> Mobile</b>: 9655381930<br>

            <b>Volume</b>: 40<br>

            <b>Tot Units</b>: 2<br>
    </td>
    <td colspan="1" style="font-size:12px;width:50%;text-align: left;padding-top: 0px;padding-bottom: 20px;padding-left: 20px;line-height:15px">
        <b>Delivery Address</b>;<br>
        <br><b>Name: Rajesh</b>
        <br>25, big wh in the city
        <br>Bangalore - 560034
        <br>Pincode : 560034
    </td>
</tr>
</tbody>
</table>


<script>
    var num = "NB00095-02/02";

    JsBarcode("#barcode<%=sno%>", num, {
        textAlign: "top",
        textPosition: "bottom",
        font: "Times",
        fontOptions: "bold",
        fontSize: 25,
        displayValue: num,
        padding: 0,
        margin: 0
    });

</script>

<script>
    JsBarcode("#barcodes<%=sno%>", num, {
        textAlign: "top",
        textPosition: "bottom",
        font: "Times",
        fontOptions: "bold",
        fontSize: 25,
        displayValue: num,
        padding: 0,
        margin: 0
    });
</script>

<br>
<%sno++;%>


    <table style="width:600px;padding-left:150px;padding-top: 10px">
        <tbody >
            <tr style="height:10px;"><td colspan="2" style="width:350px;text-align: center"><b>H&S Supply Chain Services Pvt Ltd</b></td></tr>
        <br>
        <tr style="height:30px;">
            <td colspan="1" style="width:50%;text-align: center;padding-top: 30px;padding-bottom: 30px">NB00095-02/02</td>
            <td colspan="1" style="width:50%;text-align: center"><svg id="barcode<%=sno%>"  style="width:150px;height:50px"></svg></td>
</tr>
<tr>
    <td colspan="1" style="width:50%;font-size:15px;text-align: left;padding-top: 10px;padding-bottom: 10px;padding-left:40px;border-right: 2px solid black">
            <b>Order No</b>: OR1234<br>      
           PICO Dining Chair<br>

           <b> Mobile</b>: 9655381930<br>

            <b>Volume</b>: 40<br>

            <b>Tot Units</b>: 2<br>
    </td>
    <td colspan="1" style="width:50%;text-align: left;padding-top: 0px;padding-bottom: 20px;padding-left: 20px">
        <b>Delivery Address</b>;<br>
        <br><b>Name: Rajesh</b>
        <br>25, big wh in the city
        <br>Bangalore - 560034
        <br>Pincode : 560034
    </td>
</tr>
</tbody>
</table>


<script>
    var num = "NB00095-02/02";

    JsBarcode("#barcode<%=sno%>", num, {
        textAlign: "top",
        textPosition: "bottom",
        font: "Times",
        fontOptions: "bold",
        fontSize: 25,
        displayValue: num,
        padding: 0,
        margin: 0
    });

</script>

<script>
    JsBarcode("#barcodes<%=sno%>", num, {
        textAlign: "top",
        textPosition: "bottom",
        font: "Times",
        fontOptions: "bold",
        fontSize: 25,
        displayValue: num,
        padding: 0,
        margin: 0
    });
</script>

<br>
<%sno++;%>

</body>