<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%> 

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>



<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.connectSerialNumber"  text="ConnectSerialNumber"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.ConnectSerialNumber"  text="ConnectSerialNumber"/></a></li>
            <li class="active">Connect Serial Number</li>
        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="sorter.size(5);">
                <form name="gtn" method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <br>

                    </table>  

                    <table class="table table-info mb30 table-bordered" id="table2"  >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>Serial No</th>
                                <th>Product Condition</th>
                                <th>UOM</th>
                                <th>Quantity</th>
                                <th>Storage Location</th>
                                <th>Rack</th>
                                <th>bin</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                    int snoo = 1;
                            %>
                            <c:forEach items="${getConnectSerialNumberDetails}" var="sdet">
                                <tr >
                                    <td><%=snoo%>
                                    </td>
                                    <td ><c:out value="${sdet.serials}"/></td>
                                    <c:if test="${sdet.proCons==1}"><td >Good</td></c:if>
                                    <c:if test="${sdet.proCons==2}"><td >Bad</td></c:if>
                                    <c:if test="${sdet.proCons==3}"><td >Excess</td></c:if>
                                    <c:if test="${sdet.uoms==1}"><td >Pack</td></c:if>
                                    <c:if test="${sdet.uoms==2}"><td >Piece</td></c:if>
                                    <td><c:out value="${sdet.qty}"/></td>
                                    <td>
                                        <c:out value="${sdet.storageName}"/> 
                                    </td>
                                    <td>
                                        <c:out value="${sdet.rackName}"/> 
                                    </td>
                                    <td>
                                        <c:out value="${sdet.binName}"/> 
                                    </td>

                                    <%
                                    snoo++;
                                    %>

                                </c:forEach>
                        </tbody>
                        <input type="hidden" name="count" id="count" value="<%=snoo%>" />    
                    </table>
                    <table class="table table-info mb30 table-bordered" id="table2">
                        <th>
                        <tr>Remarks</tr>
                        <tr>
                        <input type="text" class="form-control" name="remarks" id="remarks" value=""  style="width:100px;height:35px;">&nbsp;&nbsp;

                        <input type="hidden" name="grnId" id="grnId" value="<c:out value="${grnId}"/>"/>
                        <input type="hidden" name="itemId" id="itemId" value="<c:out value="${itemId}"/>"/>
                        </tr>
                        </th> 

                    </table>

                    <div>


                        <center>
                            <input type="button" class="btn btn-success" name="Update" id="update1" value="Update" onclick="editGrnSerialNos(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;
                        </center>

                    </div>


                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                        function editGrnSerialNos() {
                            var y = document.getElementById("grnId").value;
                            var x = document.getElementById("itemId").value;


                            document.gtn.action = '/throttle/viewSerialNumberSubmit.do?param=save&grnId=' + y + '&itemId=' + x;
                            document.gtn.submit();
                            this.disabled = true;
                            this.value = 'updating';


                        }
                    </script>



                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

