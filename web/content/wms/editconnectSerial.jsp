<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%> 

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>



<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.SerialNumber Scanning"  text="SerialNumber Scanning"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.SerialNumber Scanning"  text="SerialNumber Scanning"/></a></li>
            <li class="active">Serial Number Scanning</li>
        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="sorter.size(5);">
                <form name="gtn" method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <table  class="table table-info mb30 table-hover"  align="left" style="width:50%;">
                        <tr style="display:none;" >
                            <td>From Date</td>
                            <td>
                                <input name="fromDate" id="fromDate" type="text" class="form-control datepicker" onclick="ressetDate(this);" value="">
                            </td>

                            <td>To Date</td>
                            <td>
                                <input name="toDate" id="toDate" type="text" class="form-control datepicker"  onClick="ressetDate(this);" value="">
                            </td>
                            <td colspan="4" >&nbsp;</td>
                        </tr>
                    </table>
                    <c:if test="${getGrnDetails == null }" >
                        <center><font color="red" size="2"><spring:message code="trucks.label.NoRecordsFound"  text="default text"/>  </font></center>
                        </c:if>
                    <table class="table table-info mb30 table-bordered" id="table" >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>Vendor Name</th>
                                <th>Product Name</th>
                                <th>Purchase Order No</th>
                                <th>Scannable</th>
                                <th>Qty</th>
                            </tr>

                        </thead>
                        <c:if test="${getGrnDetails != null}">
                            <% int index = 0;
                                    int sno = 1;
                            %>
                            <tbody>

                                <c:forEach items="${getGrnDetails}" var="grn">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td>
                                            <%=sno%>
                                        </td>
                                        <td ><c:out value="${grn.vendorName}"/></td>
                                        <td><c:out value="${grn.productName}"/></td>
                                        <td><c:out value="${grn.purchaseOrderNo}"/></td>
                                        <td><c:out value="${grn.scannable}"/>
                                            <input type="hidden" id="scanStatus" name="scanStatus" value="<c:out value="${grn.scannable}"/>"/></td>
                                        <td><c:out value="${grn.qty}"/>
                                            <input type="hidden" value="<c:out value="${grn.stockQty}"/>" id="packQty" name="packQty">
                                            <input type="hidden" value="<c:out value="${grn.qty}"/>" id="poQuantity" name="poQuantity">
                                            <input type="hidden" value="<c:out value="${grn.productID}"/>" id="itemId" name="itemId">
                                        </td>

                                    </tr>

                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>  

                            <input type="hidden" value="" id="size" name="size">
                            </tbody>

                        </c:if>


                    </table>


                    <br>
                    <br>
                    <br>
                    </table>  
                    <c:if test="${getConnectSerialNumberDetails !=null}">
                        <table class="table table-info mb30 table-bordered" id="table2"  >
                            <thead >
                                <tr >
                                    <th>S.No</th>
                                    <th>Serial No</th>
                                    <th>Product Condition</th>
                                    <th>UOM</th>
                                    <th>Storage Location</th>
                                    <th>Rack</th>
                                    <th>Quantity</th>
                                    <th>bin</th>
                                    <th>remove</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                        int snoo = 1;
                                %>
                                <c:forEach items="${getConnectSerialNumberDetails}" var="sdet">
                                    <tr >
                                        <td><%=snoo%>
                                        </td>
                                        <td ><c:out value="${sdet.serials}"/></td>
                                        <c:if test="${sdet.proCons==1}"><td >Good</td></c:if>
                                        <c:if test="${sdet.proCons==2}"><td >Bad</td></c:if>
                                        <c:if test="${sdet.proCons==3}"><td >Excess</td></c:if>
                                        <c:if test="${sdet.uoms==1}"><td >Pack</td></c:if>
                                        <c:if test="${sdet.uoms==2}"><td >Piece</td></c:if>
                                            <td>
                                            <c:out value="${sdet.storageName}"/> 

                                        </td>
                                        <td>
                                            <c:out value="${sdet.rackName}"/> 
                                        </td>
                                        <td><c:out value="${sdet.qty}"/><input type="hidden" name="saveQty" id="saveQty<%=snoo%>" value="<c:out value="${sdet.qty}"/>"></td>
                                        <td>
                                            <c:out value="${sdet.binName}"/> 
                                        </td>

                                        <td id="edit"><input type="button" class="btn btn-success" value="Delete"  name="<c:out value="${sdet.serialId}"/>" onclick="deleteSerial(this.name);"></td>

                                        <td>
                                            <input type="checkbox"  id="edit<%=snoo%>" onclick="setValue(<%=snoo%>,<c:out value="${sdet.proCons}"/>,<c:out value="${sdet.uoms}"/>,<c:out value="${sdet.storageId}"/>,<c:out value="${sdet.rackId}"/>,<c:out value="${sdet.binId}"/>,<c:out value="${sdet.serials}"/>,<c:out value="${sdet.serialId}"/>)"/>
                                        </td>
                                    </tr>
                                    <%
                                    snoo++;
                                    %>

                                </c:forEach>
                            </tbody>
                            <input type="hidden" name="count" id="count" value="<%=snoo%>" />    
                        </table>
                        <script>
                            function setValue(sno, proCons, uoms, storageId, rackId, binId, serialNumber, serialId) {

                                var count = parseInt(document.getElementById("count").value);
                                if (document.getElementById("edit" + sno).checked == false) {


                                    $("#update1").show();
                                    $("#update2").hide();
                                    setrack("", "");
                                    setbin("", "");
                                    document.getElementById("storageId").value = "0";

                                    document.getElementById("rackId").value = "0";

                                    document.getElementById("binId").value = "0";

                                    document.getElementById("serialNo").value = "";
                                    document.getElementById("Uom").value = "Piece";
                                    document.getElementById("proCon").value = "Good";
                                    document.getElementById("serialId").value = "";
                                }

                                else {
                                    setrack(storageId, rackId);
                                    setbin(rackId, binId);
                                    $("#update2").show();
                                    $("#update1").hide();
                                    for (i = 1; i < count; i++) {
                                        if (i != sno) {
                                            document.getElementById("edit" + i).checked = false;
                                        } else {
                                            document.getElementById("edit" + i).checked = true;
                                        }
                                    }

                                    document.getElementById("storageId").value = storageId;
                                    document.getElementById("rackId").value = rackId;
                                    document.getElementById("binId").value = binId;
                                    document.getElementById("serialNo").value = serialNumber;
                                    if (uoms === 1) {
                                        document.getElementById("Uom").value = "Pack";
                                    }
                                    if (uoms === 2) {
                                        document.getElementById("Uom").value = "Piece";
                                    }
                                    if (proCons === 1) {
                                        document.getElementById("proCon").value = "Good";
                                    }
                                    if (proCons === 2) {
                                        document.getElementById("proCon").value = "Damaged";
                                    }
                                    if (proCons === 3) {
                                        document.getElementById("proCon").value = "Excess";
                                    }
                                    document.getElementById("serialId").value = serialId;
                                }
                            }
                            var saveQty = document.getElementsByName("saveQty");
                            var saveQtyNum = 0;
                            for (var x = 0; x < saveQty.length; x++) {
                                saveQtyNum = parseInt(saveQtyNum) + parseInt(saveQty[x].value);
                            }
                            document.getElementById("size").value = saveQtyNum;
//                            function checkQty(val){
//                                alert(val);
//                                alert(document.getElementById("size").value);
//                                alert(parseInt(document.getElementById("saveQty").value)-parseInt(document.getElementById("size").value));
//                                if(val>(parseInt(document.getElementById("saveQty").value)-parseInt(document.getElementById("size").value))){
//                                    alert("Excess Qty");
//                                    document.getElementById("quantity").value="";
//                                }
//                            }
                        </script>


                        <table class="table table-info mb30 table-hover" id="table4">
                            <div align="center" style="height:20px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>
                            <tr>
                                <td align="center">
                                    Product 
                                </td>
                                <td>


                                    <select id="productId" name="productId" style="width:250px;height:40px"  onchange  ="serachProductInfo();">
                                        <option value="0">----select---</option>
                                        <c:forEach items="${getGrnproductDetails}" var="pro">
                                            <option value="<c:out value="${pro.productID}"/>"><c:out value="${pro.productName}"/></option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            <script>
                                document.getElementById("productId").value =<c:out value="${itemId}"/>
                            </script>

                            <td align="center">
                                Storage Location
                            </td>
                            <td>


                                <select id="storageId" name="storageId" style="width:250px;height:40px" onchange="setrack(this.value, 0);">
                                    <option value="0">----select---</option>
                                    <c:forEach items="${getStorageDetails}" var="loc">
                                        <option value="<c:out value="${loc.storageId}"/>"><c:out value="${loc.storageName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td align="center">Rack</td>
                            <td><select id="rackId" name="rackId" style="width:250px;height:40px"class="form-control" onchange="setbin(this.value, 0);"></select></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    Bin
                                </td>
                                <td>
                                    <select id="binId" name="binId" style="width:250px;height:40px"> </select>
                                </td>
                                <td align="center">
                                    Qty
                                </td>
                                <td>
                                    <input id="quantity" name="quantity" type="text" style="width:250px;height:40px" onchange=""/>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    Product Condition
                                </td>
                                <td><select id="proCon" name="proCon" style="width:250px;height:40px">
                                        <option value="Good">Good</option>
                                        <option value="Damaged">Damaged</option>
                                        <option value="Excess">Excess</option>
                                    </select></td>

                                <td align="center">
                                    UOM
                                </td>
                                <td><select id="Uom" name="Uom" style="width:250px;height:40px" onchange="changePieceQty(this.value)">

                                        <option value="Pack">Pack</option>
                                        <option value="Piece">Piece</option>
                                    </select></td>
                            </tr>
                            <tr id="tb4">
                                <td align="center">
                                    SERIAL NO : 
                                </td>
                                <td>
                                    <input type="hidden" name="grnId" id="grnId" value="<c:out value="${grnId}"/>"/>

                                    <input type="text" id="serialNo" name="serialNo" value="" class="form-control" style="width:250px" onchange="checkExists(this.value);"/> 
                                    <input type="hidden" id="serialId" name="serialId" value="" class="form-control" style="width:250px" /> 
                                </td>


                            </tr>

                        </c:if>
                    </table>
                    <div id="tableContent"></div>

                    <script>

                        var serial = [];
                        var ipqt = [];
                        var uomarr = [];
                        var sno = [];
                        var procon = [];
                        var arr = [];
                        var binIds = [];
                        var rackIds = [];
                        var locationIds = [];
                        var i = 0;
                        var totQty = 0;





                        function go1() {
                            if (httpRequest.readyState == 4) {
                                if (httpRequest.status == 200) {
                                    var response = httpRequest.responseText;
                                    if (response != "") {
                                        alert(response);
                                        resetTheDetails();

                                    } else {
                                        saveTempSerialNos();
                                    }
                                }
                            }
                        }


                        function checkExists(serialNo) {
                            var y = document.getElementById("grnId").value;

                            var url = "";
                            if (serialNo != "") {
                                url = "./checkconnectSerialNo.do?serialNo=" + serialNo + "&grnId=" + y;

                                if (window.ActiveXObject)
                                {
                                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                } else if (window.XMLHttpRequest)
                                {
                                    httpRequest = new XMLHttpRequest();
                                }
                                httpRequest.open("POST", url, true);
                                httpRequest.onreadystatechange = function() {
                                    go1();
                                };
                                httpRequest.send(null);
                            }

                        }


                        function saveTempSerialNos() {
                            if (serial.indexOf(serialNo.value) != -1) {
                                alert("Already Scanned");
                            } else {
                                var mk = document.getElementById("size").value;
                                var u = document.getElementById("poQuantity").value;
                                if ((parseInt(totQty) + parseInt(quantity.value)) <= (u - mk)) {
                                    serial[i] = serialNo.value;
                                    ipqt[i] = quantity.value;
                                    binIds[i] = binId.value;
                                    locationIds[i] = storageId.value;
                                    rackIds[i] = rackId.value;
                                    uomarr[i] = Uom.value;
                                    procon[i] = proCon.value;
                                    i++;
                                    totQty = parseInt(totQty) + parseInt(quantity.value);
                                    table();
                                }
                                else {
                                    alert("quanity Exceed")
                                }

                            }
                        }

                        function table(){
                                   if(serial.length>0){
                                   for(x=1;x<=serial.length;x++){
                            sno.push(x);
                            }}
                                   const tmpl = (sno,serial,uomarr,procon,ipqt,binIds,locationIds,rackIds) => `
                                   <table class="table table-info mb30 table-bordered" id="table"><thead>
                                   <tr><th>S.No<th>Serial No<th>UOM<th>Product Condition<th>Qty<th>Delete<tbody>
                        ${sno.map( (cell, index) => `
                          <tr><td><input type="hidden" id="sNo" name="sNo" value="${cell}"/>${cell}<td><input type="hidden" id="serialNos"+sno name="serialNos" value="${serial[index]}" />${serial[index]}<td><input type="hidden" id="uom" name="uom" value="${uomarr[index]}" />${uomarr[index]}<td><input type="hidden" id="proCond" name="proCond" value="${procon[index]}" />${procon[index]}<td><input type="hidden" id="ipQty" name="ipQty" value="${ipqt[index]}"/>${ipqt[index]}<td><input type="hidden" id="bin" name="bins" value="${binIds[index]}"/><input type="hidden" id="stoLocation" name="stoLocation" value="${locationIds[index]}"/><input type="hidden" id="racks" name="racks" value="${rackIds[index]}"/><input type="button" class="btn btn-success" name="${sno[index]}" value="Delete" onclick="deleteRow(this.name,sno, serial, uomarr, ipqt, procon,binIds,locationIds,rackIds);" style="width:100px;height:35px;"></tr>
                          `).join('')}
                                   </table><p align = "center" style="font-size:16px"><b>Scanned Qty:</b><input type="text" id="qty" name="qty" value="${serial.length}" class="form-control" style="width:100px;height=30px" readonly/><br><br></p>`;
                                    tableContent.innerHTML=tmpl(sno,serial,uomarr,procon,ipqt,binIds,locationIds,rackIds);
                                    sno.splice(0,sno.length);
                                    document.getElementById("serialNo").value='';
                        }


                        function deleteRow(name, sno, serial, uomarr, ipqt, procon, binIds, locationIds, rackIds) {

                            var x = name - 1;
                            serial.splice(x, 1);
                            uomarr.splice(x, 1);
                            ipqt.splice(x, 1);
                            procon.splice(x, 1);
                            binIds.splice(x, 1);
                            locationIds.splice(x, 1);
                            rackIds.splice(x, 1);
                            sno.splice(0, sno.length);
                            name = 0;
                            i--;
                            table();


                        }

                        function editGrnSerialNos() {
                            var scannable = document.getElementById("scanStatus").value;
                            if(scannable =="N"){
                                var saveQty=0;
                            var po = document.getElementById("poQuantity").value;
                            var saveQtyArr = document.getElementsByName("saveQty");
                            for(var j=0;j<saveQtyArr.length;j++){
                                saveQty=parseInt(saveQty)+parseInt(saveQtyArr[j].value);
                            }
                            var qty = document.getElementById("quantity").value;
                            if(parseInt(po)<(parseInt(saveQty)+parseInt(qty))){
                                alert("Exceeded Quantity")
                            }else{
                            var y = document.getElementById("grnId").value;
                            document.gtn.action = '/throttle/editConnectSerialNumber.do?param=save&grnId=' + y;
                            document.gtn.submit();
                            this.disabled = true;
                            this.value = 'updating';
                        }
                        }else{
                            var y = document.getElementById("grnId").value;
                            document.gtn.action = '/throttle/editConnectSerialNumber.do?param=save&grnId=' + y;
                            document.gtn.submit();
                            this.disabled = true;
                            this.value = 'updating';
                        }


                        }

                        function editupdateGrnSerialNos() {
                            var y = document.getElementById("grnId").value;
                            var serialId = document.getElementById("serialId").value;
                            document.gtn.action = '/throttle/editConnectSerialNumber.do?param=edit&grnId=' + y + '&serialId=' + serialId;
                            document.gtn.submit();
                            this.disabled = true;
                            this.value = 'updating';


                        }

                        function serachProductInfo() {
                            var y = document.getElementById("grnId").value;
//                            alert("herer");
                            var productId = document.getElementById("productId").value;
                            document.gtn.action = '/throttle/editConnectSerialNumber.do?param=search&grnId=' + y + '&itemId=' + productId;
                            document.gtn.submit();
                            this.disabled = true;
                            this.value = 'updating';


                        }


                        function deleteSerial(x) {

                            var grnId = document.getElementById("grnId").value;
                            var itemId = document.getElementById("itemId").value;
                            document.gtn.action = '/throttle/editConnectSerialNumber.do?param=remove&grnId=' + grnId + '&serialid=' + x + '&itemId=' + itemId;
                            document.gtn.submit();
                            this.disabled = true;
                            this.value = 'Searching';
                        }

                    </script>

                    <script>
                        document.getElementById("quantity").value = document.getElementById("packQty").value;
                        document.getElementById("quantity").readOnly = true;
                        var scan = document.getElementById("scanStatus").value;
                        if (scan == "N") {
                            $('#tb4').hide();
                            $("#quantity").attr("readonly", false);
//                             var quant = $('#quantity').val();
                            var quant2 = $('#poQuantity').val();
                            $('#quantity').val(parseInt(quant2));
                        } else {

                        }
                        function changePieceQty(val) {
                            if (val == "Piece") {
                                document.getElementById("quantity").value = "1";
                                if (document.getElementById("scanStatus").value == "Y") {
                                    document.getElementById("quantity").readOnly = true;
                                }else{
                                    document.getElementById("quantity").readOnly = false;
                                }
                            } else {
//                                alert(val);
                                document.getElementById("quantity").value = document.getElementById("packQty").value;
                                if (document.getElementById("scanStatus").value == "Y") {
                                    document.getElementById("quantity").readOnly = true;
                                }else{
                                    document.getElementById("quantity").readOnly = false;
                                }
                            }
                        }
                    </script>
                    <script>
                        function setrack(value, val1) {

                            $.ajax({
                                url: "/throttle/getrackList.do",
                                dataType: "json",
                                data: {
                                    locationId: value,
                                },
                                success: function(data) {
                                    if (data != '') {
                                        $('#rackId').empty();
                                        $('#rackId').append(
                                                $('<option></option>').val(0).html('--select--'))
                                        $.each(data, function(i, data) {
                                            $('#rackId').append(
                                                    $('<option style="width:90px"></option>').attr("value", data.rackId).text(data.rackName)
                                                    )
                                            document.getElementById("rackId").value = val1;


                                        });
                                    } else {

                                    }
                                }
                            });
                        }


                        function setbin(value, val2) {

                            $.ajax({
                                url: "/throttle/getbinList.do",
                                dataType: "json",
                                data: {
                                    binId: value,
                                },
                                success: function(data) {
                                    if (data != '') {
                                        $('#binId').empty();
                                        $('#binId').append(
                                                $('<option></option>').val(0).html('--select--'))
                                        $.each(data, function(i, data) {
                                            $('#binId').append(
                                                    $('<option style="width:90px"></option>').attr("value", data.binId).text(data.binName)
                                                    )
                                            document.getElementById("binId").value = val2;
                                        });
                                    } else {

                                    }
                                }
                            });
                        }
                    </script>

                    <div>
                        <center>

                            <!--<input type="button" class="btn btn-success" name="Update" value="Update" onclick="searchPage(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;-->
                        </center>

                        <center>
                            <input type="button" class="btn btn-success" name="Update" id="update1" value="Update" onclick="editGrnSerialNos(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;
                        </center>
                        <center>
                            <input type="button" class="btn btn-success"  style="display:none" name="Edit" id="update2" value="edit" onclick="editupdateGrnSerialNos(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;
                        </center>
                    </div>


                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>



                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

