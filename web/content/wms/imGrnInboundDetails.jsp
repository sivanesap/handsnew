<%-- 
    Document   : imGrnInboundDetails
    Created on : 3 Dec, 2021, 8:44:10 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>GRN Inbound View</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html">InstaMart</a></li>
                <li class="">GRN Inbound View</li>

            </ol>
        </div>
    </div>
    <script>

        var dateTemp = new Date();
        var day = dateTemp.getDate();
        var month = dateTemp.getMonth() + 1;
        var year = dateTemp.getFullYear();
        if (day.toString().length == 1) {
            day = '0' + day;
        }
        if (month.toString().length == 1) {
            month = '0' + month;
        }
        var date = day + '-' + month + '-' + year;
        function checkSelectedStatus(elem, sno) {
            if (elem.checked == true) {
                document.getElementById("acceptedQty" + sno).readOnly = false;
                document.getElementById("rejectedQty" + sno).readOnly = false;
                document.getElementById("updatedTime" + sno).readOnly = false;
                document.getElementById("selectedStatus" + sno).value = "1";
            } else {
                document.getElementById("acceptedQty" + sno).readOnly = true;
                document.getElementById("rejectedQty" + sno).readOnly = true;
                document.getElementById("updatedTime" + sno).readOnly = true;
                document.getElementById("selectedStatus" + sno).value = "0";
            }
        }
        function submitPage() {
            document.material.action = "/throttle/imGrnInboundView.do?param=update";
            document.material.submit();
        }
        function checkQty(sno, status) {
            var qty = document.getElementById("qty" + sno).value;
            var acceptedQty = document.getElementById("acceptedQty" + sno).value;
            var rejectedQty = document.getElementById("rejectedQty" + sno).value;
            if (status == '1') {
                document.getElementById("rejectedQty" + sno).value = parseFloat(parseFloat(qty) - parseFloat(acceptedQty));
            } else {
                document.getElementById("acceptedQty" + sno).value = parseFloat(parseFloat(qty) - parseFloat(rejectedQty));
            }
        }
    </script>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">
                    <form name="material"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>

                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30" style="color: white">
                                    <th>S.No</th>
                                    <th>Grn No</th>
                                    <th>Grn Date</th>
                                    <th>Grn Time</th>
                                    <th>Invoice No</th>
                                    <th>Invoice Date</th>
                                    <th>Vendor Name</th>
                                    <th>Vehicle No</th>
                                    <th>Total Qty (KG)</th>
                                    <th>Total Qty (PC)</th>
                                    <th>Total Qty (PACKET)</th>
                                    <th>Received By</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int sno = 0;%>
                                <c:if test = "${grnMaster != null}">
                                    <c:forEach items="${grnMaster}" var="pc">
                                        <%
                                            sno++;
                                            String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.grnNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.grnDate}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.grnTime}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.invoiceNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.invoiceDate}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.vendorName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.vehicleNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.totKgQty}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.totPcQty}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.totPacketQty}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.receivedBy}" /></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>
                        <br><br>

                        <table class="table table-info mb30 table-hover" id="table1" >	
                            <thead>

                                <tr height="30" style="color: white">
                                    <th>S.No</th>
                                    <th>SKU Code</th>
                                    <th>SKU Name</th>
                                    <th>SKU Description</th>
                                    <th>UOM</th>
                                    <th>Quantity</th>
                                    <th>Accepted Qty</th>
                                    <th>Rejected Quantity</th>
                                    <th>Update Date</th>
                                    <th>Update Time</th>
                                    <th>Action
                                        <!--<input type="checkbox" name="selectAll" onclick="selectAll(this);">-->
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int sno1 = 0;%>
                                <c:if test = "${grnDetails != null}">
                                    <c:forEach items="${grnDetails}" var="de">
                                        <%
                                            sno1++;
                                            String className1 = "text1";
                                            if ((sno1 % 1) == 0) {
                                                className1 = "text1";
                                            } else {
                                                className1 = "text2";
                                            }
                                        %>

                                        <tr>
                                            <td class="<%=className1%>"  align="left"> <%=sno1%> </td>
                                            <td class="<%=className1%>"  align="left"> <c:out value="${de.skuCode}" /></td>
                                            <td class="<%=className1%>"  align="left"> <c:out value="${de.productName}" /></td>
                                            <td class="<%=className1%>"  align="left"> <c:out value="${de.productDescription}" /></td>
                                            <td class="<%=className1%>"  align="left"> <c:out value="${de.uomName}" /></td>
                                            <td class="<%=className1%>"  align="left"> <c:out value="${de.qty}" /></td>
                                            <td class="<%=className1%>"  align="left"> <input type="text" onchange="checkQty('<%=sno1%>', 1)" value="<c:out value="${de.grnQty}"/>" name="acceptedQty" id="acceptedQty<%=sno1%>" class="form-control" style="width:130px;height:40px" readonly/></td>
                                            <td class="<%=className1%>"  align="left"> <input type="text" onchange="checkQty('<%=sno1%>', 2)" value="<c:out value="${de.damaged}"/>" name="rejectedQty" id="rejectedQty<%=sno1%>" class="form-control" style="width:130px;height:40px" readonly/></td>
                                            <td class="<%=className1%>"  align="left"> <input type="text" value="<c:out value="${de.date}"/>" name="updatedDate" id="updatedDate<%=sno1%>" class="datepicker form-control" style="width:130px;height:40px" readonly/></td>
                                            <td class="<%=className1%>"  align="left"> <input type="time" value="<c:out value="${de.time}"/>" name="updatedTime" id="updatedTime<%=sno1%>" class="form-control" style="width:130px;height:40px" readonly/></td>
                                            <td class="<%=className1%>"  align="left"> 
                                                <input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno1%>" onclick="checkSelectedStatus(this, '<%=sno1%>');">
                                                <input type="hidden" name="selectedStatus" id="selectedStatus<%=sno1%>" value="0"/>
                                                <input type="hidden" name="detailId" id="detailId<%=sno1%>" value="<c:out value="${de.orderDetailId}"/>"/>
                                                <input type="hidden" name="qty" id="qty<%=sno1%>" value="<c:out value="${de.qty}"/>"/>
                                            </td>
                                        </tr>
                                    <script>
                                if (document.getElementById("updatedDate<%=sno1%>").value == '') {
                                    document.getElementById("updatedDate<%=sno1%>").value = date;
                                }
                                        <c:if test="${de.status=='1'}">
                                document.getElementById("selectedIndex<%=sno1%>").style.display = "none";
                                        </c:if>
                                    </script>
                                </c:forEach>
                                </tbody>
                            </c:if>
                        </table>
                        <center>
                            <input type="button" class="btn btn-success" value="Update" name="save" id="save" onclick="submitPage()"/>
                        </center>
                        <input type="hidden" name="count" id="count" value="<%=sno1%>" />

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>

                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>