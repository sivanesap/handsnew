<%-- 
    Document   : ifbPodApproval
    Created on : 27 May, 2021, 11:34:17 AM
    Author     : Roger
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script>
        function uploadPod(podId, invoiceNo) {
            window.open('/throttle/ifbPodApproval.do?&param=upload&podId=' + podId + '&invoiceNo=' + invoiceNo, 'PopupPage', 'height = 600, width = 800, scrollbars = yes, resizable = yes');
        }
        function approvePod(podId) {
            document.reconcile.action = '/throttle/ifbPodApproval.do?&param=approve&podId=' + podId;
            document.reconcile.submit()
        }
        function podDownload() {
            var selectedConsignment = document.getElementsByName('selectedIndex');
            var podcopy = [];
            var cntr = 1;
            for (var i = 0; i < selectedConsignment.length; i++) {
                if (selectedConsignment[i].checked == true) {
                    podcopy.push(document.getElementById("podIds" +cntr).value);
                    cntr++;
                }
            }
            document.reconcile.action = '/throttle/zipDownloadTest.do?&param=download&podCopys=' + podcopy;
            document.reconcile.submit();
        }
        function showImage(name) {
            window.open(name, "POD Image", "width=900, height=800");
        }
        function searchPage(){
            var from_date = document.getElementById('fromDate').value
            var to_date = document.getElementById('toDate').value
            if (from_date && to_date){
                document.reconcile.action='/throttle/ifbPodApproval.do';
                document.reconcile.submit();
            }
            else{
                alert("select the from date and to date")
            }

        }
    </script>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Pod" text="Ifb Pod Approval"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Ifb" text="Ifb"/></a></li>
                <li class=""><spring:message code="hrms.label.Pod" text="Ifb Pod Approval"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">
                    <form name="reconcile" method="post">
                        <%@include file="/content/common/message.jsp" %>
                        <table class="table table-info mb30 table-hover" id="report" >
                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:250px;height:40px" autocomplete="off" value="<c:out value="${fromDate}"/>"></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" autocomplete="off" value="<c:out value="${toDate}"/>"></td>
                        </tr>


                        <tr>         
                            <td align="center" colspan="4">
                                <input type="button" class="btn btn-success" style="width:120px"  value="Search" onclick="searchPage()"/>
                                 </td>
                        </tr>
                    </table>
                        <table class="table table-info mb30 table-bordered" id="table"  style="width:98%;" >
                            <thead>
                                <tr >
                                    <th >Sno</th>                                        
                                    <th >Invoice No</th>                                        
                                    <th >Invoice Date</th>                                     
                                    <th >Material Description</th>                                        
                                    <th >DE Name</th>
                                    <th >File Name</th>
                                    <th >Pod Status</th>
                                    <th >Order Type</th>
                                    <th >Select
                                        <input type="checkbox" name="selectAll" id="selectAll" style="width:15px;height:12px;" onclick="checkAll();"/>
                                    </th>
                                    <th >Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 1;%>
                                <c:forEach items="${podDetails}" var="re">
                                    <%
                                        String className = "text1";
                                        if ((index % 2) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>

                                    <tr height="30">
                                        <td><%=index%></td>                                      
                                        <td><c:out value="${re.invoiceNo}"/></td>                                      
                                        <td><c:out value="${re.invoiceDate}"/></td>                                      
                                        <td><c:out value="${re.materialDescription}"/></td>                                      
                                        <td><c:out value="${re.empName}"/></td>                                                    
                                        <td><a onclick="showImage('<c:out value="${re.filePath}"/>')"><c:out value="${re.fileName}"/></a></td>                                      
                                        <td><c:if test="${re.podStatus==0}">Waiting For Approval</c:if>
                                            <c:if test="${re.podStatus==1}">Approved</c:if></td>                                      
                                        <td><c:if test="${re.orderType==1}">Delivery</c:if>
                                            <c:if test="${re.orderType==2}">Exchange</c:if>
                                            <c:if test="${re.orderType==3}">Return</c:if></td>  
                                            <td>
                                                <input type="checkbox" id="selectedIndex<%=index%>" name="selectedIndex" onclick="checkSelectStatus('<%=index%>', this);">
                                            <input type="hidden" id="selectedStatus<%=index%>" name="selectedStatus" value="0">
                                            <input type="hidden" id="podIds<%=index%>" name="podIds" value="<c:out value="${re.podId}"/>">
                                        </td>
                                        <td><c:if test="${re.podStatus==0}">
                                                <button class="label label-Primary" onclick="uploadPod('<c:out value="${re.podId}"/>', '<c:out value="${re.invoiceNo}"/>');">
                                                    Re-Upload
                                                </button>  <br><br>
                                                <button class="label label-success" onclick="approvePod('<c:out value="${re.podId}"/>', '<c:out value="${re.invoiceNo}"/>');">
                                                    Approve
                                                </button>
                                            </c:if>
                                            <c:if test="${re.podStatus==1}"><span class="label label-Primary">Approved</span></c:if>
                                            </td>

                                        <%index++;%>
                                    </c:forEach>
                                    <!--                            <tr>
                                                                    <td colspan="9" align="right" style="padding-right:30px"><b> Total Amount</b></td>
                                                                    <td align="right"><b><c:out value="${totalPrice}"/></b></td>
                                                                    <td align="right"><b><c:out value="${totalCod}"/></b></td>
                                                                    <td align="right"><b><c:out value="${totalDeCod}"/></b></td>
                                                                    <td align="right"><b><c:out value="${totalCard}"/></b></td>
                                                                    <td align="right"><b><c:out value="${totalPrexo}"/></b></td>
                                                                <input type="hidden" name="tripId" id="tripId" value="<c:out value="${tripId}"/>">
                                                                <input type="hidden" name="totalPrice" id="totalPrice" value="<c:out value="${totalPrice}"/>">
                                                                <input type="hidden" name="totalCod" id="totalCod" value="<c:out value="${totalCod}"/>">
                                                                <input type="hidden" name="totalDeCod" id="totalDeCod" value="<c:out value="${totalDeCod}"/>">
                                                                <input type="hidden" name="totalCard" id="totalCard" value="<c:out value="${totalCard}"/>">
                                                                <input type="hidden" name="totalPrexo" id="totalPrexo" value="<c:out value="${totalPrexo}"/>">
                                                                </tr>-->
                            </tbody>
                        </table>
                        <center>
                            <input type="button" class="btn btn-success" value="Download Pod" style="width:200px;" id="save" name="Save" onclick="podDownload();" />
                        </center>
                        </center>

                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");

                            function checkSelectStatus(sno, obj) {
//                            alert(sno)
                                var val = document.getElementsByName("selectedStatus");
                                if (obj.checked == true) {
                                    document.getElementById("selectedStatus" + sno).value = 1;
                                } else if (obj.checked == false) {
                                    document.getElementById("selectedStatus" + sno).value = 0;
                                }
                                var vals = 0;
                                for (var i = 0; i <= val.length; i++) {
                                }
                            }
                            function checkAll() {
                                var inputs = document.getElementsByName("selectedIndex");
                                for (var i = 0; i < inputs.length; i++) {
                                    if (inputs[i].type == "checkbox") {
                                        if (inputs[i].checked == true) {
                                            inputs[i].checked = false;
                                            document.getElementById("selectedStatus" + (i + 1)).value = 0;
                                        } else if (inputs[i].checked == false) {
                                            inputs[i].checked = true;
                                            document.getElementById("selectedStatus" + (i + 1)).value = 1;
                                        }
                                    }
                                }
                            }

                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>