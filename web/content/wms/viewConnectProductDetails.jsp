<%-- 
    Document   : viewConnectProductDetails
    Created on : 2 Sep, 2021, 2:38:33 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Invoice Product Details</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You Are Here</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="general-forms.html">Connect</a></li>
            <li class="">Invoice Product Details</li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="invoiceSearch" method="post">
                    <table id="table" class="table table-info mb30 table-hover">
                        <thead>
                        <th>S. No</th>
                        <th>Category Name</th>
                        <th>Product Description</th>
                        <th>Quantity</th>
                        <th>Pending Qty</th>
                        <th>Status</th>
                        </thead>
                        <%int sno = 1;%>
                        <tbody>
                            <c:forEach items="${getConnectInvoiceProductDetails}" var="in">
                                <tr>
                                    <td><%=sno%></td>
                                    <td><c:out value="${in.categoryName}"/></td>
                                    <td><c:out value="${in.materialDescription}"/></td>
                                    <td><c:out value="${in.qty}"/></td>
                                    <td><c:out value="${in.pendingQuantity}"/></td>
                                    <td><c:out value="${in.status}"/></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>

                    </table>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>

