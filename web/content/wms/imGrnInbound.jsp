
<%--
    Document   : imGrnInbound
    Created on : 3 Dec, 2021, 8:43:30 PM
    Author     : Roger
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> GRN Inbound</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">InstaMart</a></li>
            <li class="active">GRN Inbound</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body>
                <form name="grn" method="post">
                    <table class="table table-info mb30 table-hover" id="serial">
                        <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                        </div>
                        <tr>
                            <td align="center"><text style="color:red">*</text>GRN Date</td>
                            <td height="30"><input name="grnDate" id="grnDate" type="text"  class="datepicker form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="" autocomplete="off"></td>
                            <td align="center"><text style="color:red">*</text>GRN Time</td>
                            <td height="30"><input name="grnTime" id="grnTime" type="time"  class="form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="" autocomplete="off"></td>

                        </tr>
                        <tr>
                            <td align="center"><text style="color:red">*</text>Vendor Name</td>
                            <td height="30">
                                <input type="text" name="vendorName" id="vendorName" class="form-control" style="width:240px;height:40px;" />
                                <select name="vendorId" id="vendorId" class="form-control" style="width:240px;height:40px; display: none"  onclick="" >
                                    <option value="">------Select Vendor------</option>
                                    <c:forEach items="${vendorMaster}" var="vn">
                                        <option value="<c:out value="${vn.vendorId}"/>"><c:out value="${vn.vendorName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td align="center"><text style="color:red">*</text>Vehicle No</td>
                            <td height="30"><input name="vehicleNo" id="vehicleNo" type="text"  class="form-control" style="width:240px;height:40px"  autocomplete="off"></td>

                        </tr>
                        <tr>
                            <td align="center"><text style="color:red">*</text>Invoice No</td>
                            <td height="30"><input name="invoiceNo" id="invoiceNo" type="text"  class="form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="" autocomplete="off"></td>
                            <td align="center"><text style="color:red">*</text>Invoice Date</td>
                            <td height="30"><input name="invoiceDate" id="invoiceDate" type="text"  class="datepicker form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="" autocomplete="off"></td>
                        </tr>
                        <tr>
                            <td align="center">Total Quantity (KG)</td>
                            <td><input type="text" name="totKgQty" id="totKgQty" class="form-control" value="0" readonly="" style="width:240px;height:40px"></td>
                            <td align="center">Total Quantity (PC)</td>
                            <td><input type="text" name="totPcQty" id="totPcQty" class="form-control" value="0" readonly="" style="width:240px;height:40px"></td>
                        </tr>
                        <tr>
                            <td align="center">Total Quantity (PACKET)</td>
                            <td><input type="text" name="totPacketQty" id="totPacketQty" class="form-control" value="0" readonly="" style="width:240px;height:40px"></td>
                            <td align="center">
                                Received By
                            </td>
                            <td>
                                <input type="text" id="receivedBy" name="receivedBy" value="" class="form-control" style="width:240px; height:40px"/></textarea> 
                            </td>
                        </tr>

                    </table>                 
                    <table class="table table-info mb30 table-hover sortable" id="addIndent" align="center" width="90%">
                        <thead >
                            <tr>
                                <th>S.No</th>
                                <th>Sku Code</th>
                                <th>Product Name</th>  
                                <th>Product Description</th>  
                                <th>Quantity</th>
                                <th>UOM</th>
                                <th>Delete</th>

                            </tr>
                        </thead>

                    </table>

                    <script>
                        var rowCount = 0;
                        var sno = 0;
                        var rowIndex = 0;

                        function addRow() {
                            var tab = document.getElementById("addIndent");
                            var iRowCount = tab.getElementsByTagName('tr').length;
                            rowCount = iRowCount;
                            var newrow = tab.insertRow(rowCount);
                            sno = rowCount;
                            rowIndex = rowCount;
                            var cell = newrow.insertCell(0);
                            var cell0 = "<td class='text1' height='25' > <input type='hidden'  name='Sno' value='' > " + sno + "</td>";
                            cell.innerHTML = cell0;

                            cell = newrow.insertCell(1);
                            var cell1 = "<td class='text1' height='30'><select class='form-control' name='skuName' style='width:200px;height:40px' onchange='setSkuId(this.value," + sno + ")' id='skuName" + sno + "'  value=''><option value=''>-----Select Sku-----</option><c:if test='${imSkuDetails!=null}'><c:forEach items='${imSkuDetails}' var='po'><option value='<c:out value='${po.productId}'/>~<c:out value='${po.productName}'/>~<c:out value='${po.productDescription}'/>'><c:out value='${po.skuCode}'/> - <c:out value='${po.productName}'/></option></c:forEach></c:if></select></td>"
                            cell.innerHTML = cell1;

                            cell = newrow.insertCell(2);
                            var cell2 = "<td class='text1' height='25'><input type='text' class='form-control' name='productName' style='width:130px;height:40px' id='productName" + sno + "'  readonly><input type='hidden' name='skuId' id='skuId" + sno + "'/></td>"
                            cell.innerHTML = cell2;


                            cell = newrow.insertCell(3);
                            var cell3 = "<td class='text1' height='30'><input type='text' class='form-control' name='skuDescription' style='width:160px;height:40px' id='skuDescription" + sno + "' readonly></td>"
                            cell.innerHTML = cell3;


                            cell = newrow.insertCell(4);
                            var cell4 = "<td class='text1' height='30'><input type='text' class='form-control' name='qty' onchange='checkqty()' style='width:130px;height:40px' id='qty" + sno + "' value='0.00'></td>"
                            cell.innerHTML = cell4;


                            cell = newrow.insertCell(5);
                            var cell5 = "<td class='text1' height='30'><select class='form-control' name='uom' style='width:130px;height:40px' onchange='checkqty()' id='uom" + sno + "' ><option value='KG'>KG</option><option value='PC'>PC</option><option value='PACKET'>PACKET</option></select></td>"
                            cell.innerHTML = cell5;

                            cell = newrow.insertCell(6);
                            var cell6 = "<td class='text1' height='25' ><input type='button' class='btn btn-info'   name='delete'  id='delete'   value='Delete Row' onclick='deleteRow(this," + sno + ");' ></td>";
                            cell.innerHTML = cell6;


                            rowIndex++;
                            rowCount++;
                            sno++;

                        }

                        function deleteRow(src) {
                            sno--;
                            var oRow = src.parentNode.parentNode;
                            var dRow = document.getElementById('addIndent');
                            dRow.deleteRow(oRow.rowIndex);
                            document.getElementById("selectedRowCount").value--;
                        }
                        function submitPage(value) {
                            if (document.getElementById("grnDate").value == "") {
                                alert("Enter GRN Date");
                                document.getElementById("grnDate").focus();
                            } else if (document.getElementById("grnTime").value == "") {
                                alert("Enter GRN Time");
                                document.getElementById("grnTime").focus();
                            } else if (document.getElementById("vendorName").value == "") {
                                alert("Enter Vehcle Name");
                                document.getElementById("vendorName").focus();
                            } else if (document.getElementById("vehicleNo").value == "") {
                                alert("Enter Vehicle No");
                                document.getElementById("vehicleNo").focus();
                            } else if (document.getElementById("invoiceNo").value == "") {
                                alert("Enter Invpice No");
                                document.getElementById("invoiceNo").focus();
                            } else if (document.getElementById("invoiceDate").value == "") {
                                alert("Enter Invoice Date");
                                document.getElementById("invoiceDate").focus();
                            } else {
                                document.grn.action = '/throttle/imGrnInbound.do?param=save';
                                document.grn.submit();
                            }
                        }
                        function checkqty() {
                            var qty = document.getElementsByName("qty");
                            var uom = document.getElementsByName("uom");
                            var totKgQty = 0;
                            var totPcQty = 0;
                            var totPacketQty = 0;
                            for (var i = 0; i < qty.length; i++) {
                                if (uom[i].value == 'KG') {
                                    if (qty[i].value != "") {
                                        totKgQty = parseInt(qty[i].value) + totKgQty;
                                    }
                                }
                                if (uom[i].value == 'PC') {
                                    if (qty[i].value != "") {
                                        totPcQty = parseInt(qty[i].value) + totPcQty;
                                    }
                                }
                                if (uom[i].value == 'PACKET') {
                                    if (qty[i].value != "") {
                                        totPacketQty = parseInt(qty[i].value) + totPacketQty;
                                    }
                                }
                            }
                            document.getElementById("totKgQty").value = totKgQty;
                            document.getElementById("totPcQty").value = totPcQty;
                            document.getElementById("totPacketQty").value = totPacketQty;
                        }
                        function setSkuId(value, sno) {
                            if (value != '') {
                                var skuId = value.split('~')[0];
                                var skuName = value.split('~')[1];
                                var skuDescription = value.split('~')[2];
                                document.getElementById("skuId" + sno).value = skuId;
                                document.getElementById("productName" + sno).value = skuName;
                                document.getElementById("skuDescription" + sno).value = skuDescription;
                            } else {
                                document.getElementById("skuId" + sno).value = "";
                                document.getElementById("productName" + sno).value = "";
                                document.getElementById("skuDescription" + sno).value = "";
                            }
                        }
                            </script>

                            <table>
                                <center>
                                    <input value="Add Row" class="btn btn-success" id="countRow" type="button" onClick="addRow();" >
                                    &emsp;<input type="button" value="Save" id="insert" class="btn btn-success" onClick="submitPage(this.value);">
                                    &emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">
                                </center>

                            </table>
                        </form>
                    </body>
                </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>

    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>