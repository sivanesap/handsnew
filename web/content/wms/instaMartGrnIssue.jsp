
<%-- 
    Document   : instaMartGrnIssue
    Created on : 29 Oct, 2021, 3:57:01 PM
    Author     : alan
--%>


<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    function grnIssuePrint() {
        window.open('/throttle/instaMartssuetPrint.do?');
    }

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> GRN issue</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">InstaMart</a></li>
            <li class="active">GRN issue</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="">
                <form name="saveGtnDetails" method="post">
                    <table class="table table-info mb30 table-hover" id="serial">
                        <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                        </div>
                        <tr>
                            <td align="center"><text style="color:red">*</text>Issue Date</td>
                            <td height="30"><input name="issueDate" id="issueDate" type="text"  class="datepicker form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="" autocomplete="off"></td>
                            <td align="center"><text style="color:red">*</text>Issue Time</td>
                            <td height="30"><input name="issueTime" id="issueTime" type="time"  class="form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="" autocomplete="off"></td>

                        </tr>

                        <tr>
                            <td align="center">Total Quantity</td>
                            <td><input type="text" name="totQty" id="totQty" class="form-control" value="0" style="width:240px;height:40px"></td>
                            <td align="center">Received By</td>
                            <td><input type="text" name="receivedBy" id="receivedBy" class="form-control" value="" style="width:240px;height:40px"></td>
                        </tr>
                        <tr>
                            <td align="center">
                                Remarks
                            </td>
                            <td>
                                <textarea type="text" id="remarks" name="remarks" value="" class="form-control" style="width:240px; height:60px"/></textarea> 
                            </td>
                        </tr>

                    </table>                 
                    <table class="table table-info mb30 table-hover sortable" id="addIndent" align="center" width="90%">
                        <thead >
                            <tr>
                                <th>S.No</th>
                                <th>Material Name</th>  
                                <th>Sku Code</th>
                                <th>UOM</th>
                                <th>Stock Qty</th>
                                <th>Issue Qty</th>
                                <th>Action</th>

                            </tr>
                        </thead>

                    </table>

                    <script>
                        var rowCount = 0;
                        var sno = 0;
                        var rowIndex = 0;
                        function addRow() {
                            var tab = document.getElementById("addIndent");
                            var iRowCount = tab.getElementsByTagName('tr').length;

                            rowCount = iRowCount;
                            var newrow = tab.insertRow(rowCount);
                            //        alert(iRowCount);
                            sno = rowCount;
                            rowIndex = rowCount;
                            //        alert("rowIndex:"+rowIndex);
                            var cell = newrow.insertCell(0);
                            var cell0 = "<td class='text1' height='25' > <input type='hidden'  name='Sno' value='' > " + sno + "</td>";
                            cell.innerHTML = cell0;

                            cell = newrow.insertCell(1);
                            var cell2 = "<td class='text1' height='30'><select class='form-control' name='materialId' style='width:200px;height:40px' onchange='changeStockQty(this.value," + sno + ")' id='materialId" + sno + "'  value=''><option value=''>-----Select Material-----</option><c:if test='${materialMaster!=null}'><c:forEach items='${materialMaster}' var='po'><option value='<c:out value='${po.materialId}'/>~<c:out value='${po.materialCode}'/>~<c:out value='${po.stockQty}'/>~<c:out value='${po.unit}'/>'><c:out value='${po.material}'/></option></c:forEach></c:if></select></td>"
                            cell.innerHTML = cell2;



                            cell = newrow.insertCell(2);
                            cell2 = "<div id='test1'><td class='text1' height='25'><input type='text' readonly class='form-control' name='skuCode' style='width:130px;height:40px' id='skuCode" + sno + "' onchange='checkqty();'  value=''> </td></div>";
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(3);
                            cell2 = "<div id='test1'><td class='text1' height='25'><input readonly type='text'class='form-control' name='uom' style='width:130px;height:40px' id='uom" + sno + "' onchange='checkqty();'  value=''> </td></div>";
                            cell.innerHTML = cell2;
                            cell = newrow.insertCell(4);
                            cell2 = "<div id='test1'><td class='text1' height='25'><input type='text' readonly class='form-control' name='stockQty' style='width:130px;height:40px' id='stockQty" + sno + "' onchange='checkqty();'  value=''> </td></div>";
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(5);
                            cell2 = "<div id='test1'><td class='text1' height='25'><input type='text' class='form-control' name='issueQty' style='width:130px;height:40px' id='issueQty" + sno + "' onchange='checkqty(" + sno + ");'  value='0'> </td></div>";
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(6);
                            var cell2 = "<td class='text1' height='25' ><input type='button' class='btn btn-info'   name='delete'  id='delete'   value='Delete Row' onclick='deleteRow(this," + sno + ");' ></td>";
                            cell.innerHTML = cell2;


                            rowIndex++;
                            rowCount++;
                            sno++;

                        }

                        function deleteRow(src) {
                            sno--;
                            var oRow = src.parentNode.parentNode;
                            var dRow = document.getElementById('addIndent');
                            dRow.deleteRow(oRow.rowIndex);
                            document.getElementById("selectedRowCount").value--;
                        }
                        function submitPage(value) {

                            if (document.getElementById("issueDate").value == "") {
                                alert("Enter Issue Date");
                                document.getElementById("issueDate").focus();
                            } else if (document.getElementById("issueTime").value == "") {
                                alert("Enter Issue Time");
                                document.getElementById("issueTime").focus();
                            } else {
                                document.saveGtnDetails.action = '/throttle/instaMartGrnIssue.do?param=save';
                                document.saveGtnDetails.submit();
                            }
                        }
                        function checkqty(sno) {
                            var stockQty = parseInt(document.getElementById("stockQty" + sno).value);
                            var qty = document.getElementsByName("issueQty");
                            var totQty = 0;
                            var issueQty = parseInt(document.getElementById("issueQty" + sno).value);
                            if (issueQty <= stockQty) {
                                for (var i = 0; i < qty.length; i++) {
                                    if (qty[i].value != "") {
                                        totQty = parseInt(qty[i].value) + totQty;
                                    }
                                }
                                document.getElementById("totQty").value = totQty;
                            } else {
                                document.getElementById("issueQty"+sno).value=0;
                                alert("Issue Qty greater than Stock Qty!");
                                for (var i = 0; i < qty.length; i++) {
                                    if (qty[i].value != "") {
                                        totQty = parseInt(qty[i].value) + totQty;
                                    }
                                }
                                document.getElementById("totQty").value = totQty;
                            }
                        }
                        function changeStockQty(mat, sno) {
                            if (mat != '') {
                                var matId = mat.split('~')[0];
                                var materialCode = mat.split('~')[1];
                                var stockQty = mat.split('~')[2];
                                var uom = mat.split('~')[3];
                                document.getElementById("stockQty" + sno).value = stockQty;
                                document.getElementById("uom" + sno).value = uom;
                                document.getElementById("skuCode" + sno).value = materialCode;
                            } else {
                                document.getElementById("stockQty" + sno).value = 0;
                                document.getElementById("skuCode" + sno).value = "";
                                document.getElementById("uom" + sno).value = "";
                            }
                        }
                            </script>
                    <%--<c:out value='${materialMaster}'/>--%>
                    <table>
                        <center>
                            <input value="Add Row" class="btn btn-success" id="countRow" type="button" onClick="addRow();" >
                            &emsp;<input type="button" value="Save" id="insert" class="btn btn-success" onClick="submitPage(this.value);">
                            &emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">
                        </center>

                    </table>
                </form>
            </body>
        </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>

    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>