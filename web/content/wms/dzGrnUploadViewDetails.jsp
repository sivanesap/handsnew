<%-- 
    Document   : dzGrnUploadViewDetails
    Created on : 14 Dec, 2021, 1:17:41 PM
    Author     : alan
--%>



<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>  Grn View details</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html">dunzo</a></li>
                <li class="">Grn View details</li>

            </ol>
        </div>
    </div>
              
                
              
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="material"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>

                   
                            <c:if test = "${getDzGrnDetails != null}" >

                                <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                    <thead style="font-size:12px">
                                        <tr height="40">
                                            <th>Sno</th>
                                            <th>Product Line Entry Id</th>
                                            <th>Category</th>
                                            <th>Product Id</th>
                                            <th>Product Full Name</th>
                                            <th>Ordered Qty</th>
                                            <th>Received Qty</th>
                                            <th>Difference Qty</th>
                                            <th>Price</th>
                                            <th>Tax</th>
                                            <th>PO No</th>
                                            <th>Ordered Total</th>
                                            <th>Received Total</th>
                                            <th>Diff Total</th>
                                            <th>Tax Name</th>
                                            <th>Bill To Location Id</th>
                                            <th>Ship To Location Id</th>
                                            <th>Bill To Location Name</th>
                                            <th>Ship To Location Name</th>
                                            <th>PO Date</th>
                                            <th>PO Reference Number</th>
                                            <th>PO Supplier Id</th>
                                            <th>PO Supplier Name</th>
                                            <th>PO Ordered Qty</th>
                                            <th>PO Received Qty</th>
                                            <th>Bin</th>


                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                     int count=0;
                                    %> 
                                    <tbody>

                                        <c:forEach items="${getDzGrnDetails}" var="pc">


                                            <tr>
                                                <td align="left"> <%=sno%> </td>
                                                <td align="left"> <c:out value="${pc.productLineEntryId}"/></td>
                                                <td align="left"> <c:out value="${pc.categoryName}"/> </td>
                                                <td align="left"> <c:out value="${pc.productId}"/></td>
                                                <td align="left"> <c:out value="${pc.name}"/> </td>
                                                <td align="left"> <c:out value="${pc.orderedQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.receivedQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.diffQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.price}"/> </td>
                                                <td align="left"> <c:out value="${pc.taxValue}"/></td>
                                                <td align="left"> <c:out value="${pc.poNo}"/></td>
                                                <td align="left"> <c:out value="${pc.totalOrderedQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.totalReceivedQty}"/></td>
                                                <td align="left"> <c:out value="${pc.totalDiffQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.taxVol}"/> </td>
                                                <td align="left"> <c:out value="${pc.billToLocationId}"/> </td>
                                                <td align="left"> <c:out value="${pc.shipToLocationId}"/> </td>
                                                <td align="left"> <c:out value="${pc.billToLocationName}"/> </td>
                                                <td align="left"> <c:out value="${pc.shipToLocationName}"/></td>
                                                <td align="left"> <c:out value="${pc.poDate}"/> </td>
                                                <td align="left"> <c:out value="${pc.referenceNo}"/></td>
                                                <td align="left"> <c:out value="${pc.supplierId}"/> </td>
                                                <td align="left"> <c:out value="${pc.supplierName}"/> </td>
                                                <td align="left"> <c:out value="${pc.poOrderedQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.poReceivedQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.binName}"/> </td>

                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                    </tbody>
                                </table>

                              
                            </c:if>



                        <br>
                        <br>
                   

                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
