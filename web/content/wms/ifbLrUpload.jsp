
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>
<script type="text/javascript">

    function savePage() {
        document.upload.action = "/throttle/ifbLrPrint.do?&param=save";
        document.upload.submit();
//        window.open('/throttle/ifbLrPrint.do?&param=save', 'PopupPage', 'height = 600, width = 800, scrollbars = yes, resizable = yes');
    }
    function Searchpage() {
        document.upload.action = "/throttle/ifbLrUpload.do";
        document.upload.method = "post";
        document.upload.submit();
    }
    function viewMaterialDetails(lrId) {
        window.open('/throttle/ifbHubOut.do?lrId=' + lrId + '&param=serial', 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="IFB LR Report"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="wms"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="IFB LR Report"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >
                <form name="upload"  method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <div id="ptp" style="overflow: auto">
                        <div class="inpad">
                            <table class="table table-info mb30 table-hover" style="width:100%">
                                <thead><tr><th colspan="7">IFB LR Report </th></tr></thead>
                                <tr>
                                    <td  ><font color="red">*</font>From Date</td>
                                    <td ><input name="fromDate" id="fromDate" type="text" class="datepicker form-control" autocomplete="off" style="width:175px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                                    <td  ><font color="red">*</font>To Date</td>
                                    <td ><input name="toDate" id="toDate" type="text" class="datepicker form-control" autocomplete="off" style="width:175px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                                </tr>
                            </table>
                            <center>
                                <input type="button" class="btn btn-success"   value="search" onclick="Searchpage()">
                            </center>

                            <div id="ptp" style="overflow: auto">
                                <div class="inpad">
                                    <table class="table table-info mb30 table-hover" id="table" >

                                        <thead>
                                            <tr >
                                                <th  >S.No</th>
                                                <th  >Invoice</th>
                                                <th  >LR No</th>
                                                <th  >Upload Date</th>
                                                <th  >Ship From</th>
                                                <th  >Hub Name</th>
                                                <th  >Customer</th>
                                                <th  >City To</th>
                                                <th  >Pincode To</th>
                                                <th  >Quantity</th>
                                                <th  >Amount</th>
                                                <th  >E-Way Bill No</th>
                                                <th  >E-Way Bill Expiry Date</th>
                                                <th align="center">Print<input type="checkbox" name="all" id="all" onclick="checkAll(this)"></th>

                                            </tr>
                                        </thead>
                                        <% int index = 0;
                                            int sno = 1;
                                        %>
                                        <tbody>

                                            <c:if test = "${ifbLRUploadList != null}" >
                                                <c:forEach items= "${ifbLRUploadList}" var="ifb">
                                                    <tr height="30">
                                                        <td align="left"><%=sno%></td>
                                                        <td align="left"   > <a href="#" onclick="viewMaterialDetails('<c:out value="${ifb.lrId}" />');"><u><c:out value="${ifb.invoiceNo}" /></u></a></td>
                                                        <td align="left"   ><c:out value="${ifb.lrNo}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.createdDate}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.currentHub}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.deliveryHub}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.customerName}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.city}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.pincode}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.qty}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.amount}"/></td>
                                                        <c:if test="${ifb.ewayBillNo=='0'}">
                                                            <td align="left">-</td>
                                                            <td align="left">-</td>
                                                        </c:if>
                                                        <c:if test="${ifb.ewayBillNo!='0'}">
                                                            <td align="left"> <c:out value="${ifb.ewayBillNo}"/></td>
                                                            <td align="left"><c:out value="${ifb.ewayExpiry}"/> </td>
                                                        </c:if>
                                                        <td align="left"   ><input type="checkbox" id="selectedIndex<%=sno%>" name="selectedIndex" value="<c:out value="${pc.lrId}"/>" onclick="checkSelectStatus('<%=sno%>', this)">
                                                            <input type="hidden" name="selectedStatus" id="selectedStatus<%=sno%>" value="0" />
                                                            <input type="hidden" name="lrId" id="lrId<%=sno%>" value="<c:out value="${ifb.lrId}"/>" /> 
                                                            <input type="hidden" name="type" id="type<%=sno%>" value="<c:out value="${ifb.type}"/>" /> </td>
                                                                                        <!--<td>   <input type="button" class="btn btn-success"   value="LR Print" onclick="savePage('<c:out value="${ifb.lrId}"/>','<c:out value="${ifb.type}"/>')"></td>--> 
                                                    </tr>
                                                    <%
                                                        sno++;
                                                        index++;
                                                    %>
                                                </c:forEach>
                                            <script>

                                                function checkSelectStatus(sno, obj) {
                                                    var val = document.getElementsByName("selectedStatus");
                                                    if (obj.checked == true) {
                                                        document.getElementById("selectedStatus" + sno).value = 1;
                                                    } else if (obj.checked == false) {
                                                        document.getElementById("selectedStatus" + sno).value = 0;
                                                    }
                                                    var vals = 0;
                                                    for (var i = 0; i <= val.length; i++) {
                                                    }
                                                }
                                                function checkAll(element) {
                                                    var checkboxes = document.getElementsByName('selectedIndex');
                                                    if (element.checked == true) {
                                                        for (var x = 0; x < checkboxes.length; x++) {
                                                            checkboxes[x].checked = true;
                                                            $('#selectedStatus' + (x + 1)).val('1');
                                                        }
                                                    } else {
                                                        for (var x = 0; x < checkboxes.length; x++) {
                                                            checkboxes[x].checked = false;
                                                            $('#selectedStatus' + (x + 1)).val('0');
                                                        }
                                                    }
                                                }
                                            </script>
                                            <input type="hidden" name="sno" id="sno" value="<%=sno%>">
                                        </c:if>
                                        </tbody>
                                    </table>
                                    <br><br><center>
                                        <input type="button" class="btn btn-success"   value="LR Print" onclick="savePage()">
                                    </center>
                                    <center>

                                    </center>

                                </div>
                            </div>    
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>
                            </form>
                            </body>
                        </div>
                    </div>
        </div>


        <%@ include file="/content/common/NewDesign/settings.jsp" %>