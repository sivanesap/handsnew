<%-- 
    Document   : rpWorkOrder
    Created on : 17 Sep, 2021, 11:58:03 AM
    Author     : alan
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>
<link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>

<script type="text/javascript">
    $(document).ready(function () {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function () {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });

    function isNumberKey(e) {
        var key = (e.which) ? e.which : e.keyCode;
        if ((key != 46 && key > 31) && (key < 48 || key > 57)) {
            return false;
        }
        return true;
    }

    function add() {
        document.connect.action = " /throttle/rpWorkOrder.do?param=save";
        document.connect.submit();
    }



</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Work Order</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Repose </a></li>
                <li class="">Work Order</li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="connect"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <br>
                        <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                            <input type="hidden" name="indentId" id="indentId" value=""  />
                            <tr>
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="8" >Repose Work Order</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="text1" ><font color="red">*</font>Primary Warehouse</td>
                                    <td class="text1" >
                                        <select name="warehouse" id="warehouse" class="form-control" style="width:240px;height:40px;" >

                                            <option value="" selected>------Select Location------</option>
                                            <option value="90" selected>Coimbatore Factory</option>
                                            <option value="91" selected>Pune Factory</option>

                                        </select>
                                    </td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Work order Date</td>
                                    <td class="text1"><input type="text" name="workOrderDate" id="workOrderDate" class="form-control datepicker" style="width:240px;height:40px" autocomplete="off"/></td>


                                </tr>


                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Expected Date</td>
                                    <td class="text1"><input type="text" name="expectedDate" id="expectedDate" class="form-control datepicker" style="width:240px;height:40px"/></td>

                                </tr>


                            </table>


                            <table class="table table-info mb30 table-hover sortable" id="addIndent" align="center" width="90%">
                                <thead >
                                    <tr>
                                        <th>S.No</th>
                                        <th>Product Name</th>  
                                        <th>Required Quantity</th>
                                        <th>Delete</th>

                                    </tr>
                                </thead>
                            </table>
                            <table>
                                <center>
                                    <input value="Add Row" class="btn btn-success" id="countRow" type="button" onClick="addRow();" >
                                    &emsp;<input type="button" value="Save" id="insert" class="btn btn-success" onClick="submitPage();">
                                    &emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">
                                </center>

                            </table>
                            <script>
                                var rowCount = 0;
                                var sno = 0;
                                var rowIndex = 0;
                                var filled = document.getElementById("filledQty").value;
                                var qty = document.getElementById("qty").value;

                                function addRow() {
                                    var tab = document.getElementById("addIndent");
                                    var iRowCount = tab.getElementsByTagName('tr').length;
                        //                                if ((parseInt(filled) + parseInt(sno)) <= parseInt(qty)) {
                                    rowCount = iRowCount;
                                    var newrow = tab.insertRow(rowCount);
                                    sno = rowCount;
                                    rowIndex = rowCount;
                                    var cell = newrow.insertCell(0);
                                    var cell0 = "<td class='text1' height='25' > <input type='hidden'  name='Sno' value='' > " + sno + "</td>";
                                    cell.innerHTML = cell0;

                                    cell = newrow.insertCell(1);
                                    var cell2 = "<td class='text1' height='25'><select name='itemId' id='itemId" + sno + "' class ='form-control' style='width:240px;height:40px;'><option value=''>-----Select One-----</option><c:forEach items='${productList}' var='pl'><option value='<c:out value='${pl.itemId}'/>'><c:out value='${pl.itemName}'/></option></c:forEach></select></td>"
                                    cell.innerHTML = cell2;

                                    cell = newrow.insertCell(2);
                                    var cell2 = "<td class='text1' height='25'><input class='form-control' name='qty' id='qty" + sno + "' style='width:180px;height:35px'/></td>"
                                    cell.innerHTML = cell2;

                                    cell = newrow.insertCell(3);
                                    var cell2 = "<td class='text1' height='25' ><input type='button' class='btn btn-info'   name='delete'  id='delete'   value='Delete Row' onclick='deleteRow(this," + sno + ");' ></td>";
                                    cell.innerHTML = cell2;

                                    $('#itemId' + sno).select2({placeholder: 'Fill Product Name'});
                                    $('#itemId' + sno).val('');

                                    rowIndex++;
                                    rowCount++;

                                    sno++;


                                }
                                function submitPage() {
                                    document.connect.action = '/throttle/rpWorkOrder.do?param=save';
                                    document.connect.submit();
                                }
                                function deleteRow(src) {
                                    sno--;
                                    var oRow = src.parentNode.parentNode;
                                    var dRow = document.getElementById('addIndent');
                                    dRow.deleteRow(oRow.rowIndex);
                                    document.getElementById("selectedRowCount").value--;
                                }
                                </script>
                                <br>
                                <br>
                                <script language="javascript" type="text/javascript">
                                    setFilterGrid("table");
                                </script>



                        </form>
                    </body>
                </div>
            </div>
        </div>                        


    <%@ include file="/content/common/NewDesign/settings.jsp" %>