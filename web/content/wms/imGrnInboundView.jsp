<%-- 
    Document   : imGrnInboundView
    Created on : 3 Dec, 2021, 8:43:49 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>GRN Inbound View</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html">InstaMart</a></li>
                <li class="">GRN Inbound View</li>

            </ol>
        </div>
    </div>
                <script>
                    function openView(grnId){
                        document.material.action='/throttle/imGrnInboundView.do?param=view&grnId=' + grnId;
                        document.material.submit();
                    }
                </script>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">
                    <form name="material"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>

                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30" style="color: white">
                                    <th>S.No</th>
                                    <th>Grn No</th>
                                    <th>Grn Date</th>
                                    <th>Grn Time</th>
                                    <th>Invoice No</th>
                                    <th>Invoice Date</th>
                                    <th>Vendor Name</th>
                                    <th>Vehicle No</th>
                                    <th>Total Qty (KG)</th>
                                    <th>Total Qty (PC)</th>
                                    <th>Total Qty (PACKET)</th>
                                    <th>Received By</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                            <c:if test = "${grnMaster != null}">
                                <c:forEach items="${grnMaster}" var="pc">
                                    <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>

                                    <tr>
                                        <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                        <td class="<%=className%>"  align="left"> <c:out value="${pc.grnNo}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.grnDate}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.grnTime}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.invoiceNo}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.invoiceDate}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.vendorName}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.vehicleNo}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.totKgQty}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.totPcQty}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.totPacketQty}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.receivedBy}" /></td>
                                
                                 
                                    <td class="<%=className%>"> <a onclick="openView('<c:out value="${pc.grnId}"/>')">View</a></td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </c:if>
                        </table>


                        <input type="hidden" name="count" id="count" value="<%=sno%>" />

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                                        setFilterGrid("table");
                        </script>

                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
