<%-- 
    Document   : AddStorage
    Created on : 25 Sep, 2021, 11:17:16 AM
    Author     : alan
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%@page import="java.text.SimpleDateFormat" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });


    function checkStorage() {
        var url = "";
        var Storage = document.getElementById("storageName").value
        if (Storage != "") {
            url = "./checkStorageName.do?storageName=" + Storage;

            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("POST", url, true);
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var response = httpRequest.responseText;
                        if (response != "") {
                            alert(response);
                            //console.log(response)
                            document.getElementById("storageName").value = ""
                        }
                    }
                }
            };
            httpRequest.send(null);
        } else {
            alert("storageName not should be empty")
        }

    }
    function checkRack() {
        var url = "";
        var rack = document.getElementById("rackName").value
        if (rack != "") {
            url = "./checkRackName.do?rackName=" + rack;

            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("POST", url, true);
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var response = httpRequest.responseText;
                        if (response != "") {
                            alert(response);
                            //console.log(response)
                            document.getElementById("rackName").value = ""
                        }
                    }
                }
            };
            httpRequest.send(null);
        }

    }
    function checkBin() {
        var url = "";
        var Bin = document.getElementById("binName").value
        if (Bin != "") {
            url = "./checkBinName.do?binName=" + Bin;

            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("POST", url, true);
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var response = httpRequest.responseText;
                        if (response != "") {
                            alert(response);
                            //console.log(response)
                            document.getElementById("binName").value = ""
                        }
                    }
                }
            };
            httpRequest.send(null);
        }

    }


    function storageCheck() {
        var Storage = document.getElementById("storageName").value;
        if (Storage != "") {
            document.connect1.action = "/throttle/connectAddStorage.do?param=save1";
            document.connect1.submit();
        }
        else {
            alert("Storage Name should Not Be Empty");
        }
    }


    function binCheck() {


        var Bin = document.getElementById("binName").value
        if (Bin != "") {
            document.connect3.action = " /throttle/connectAddStorage.do?param=save3";
            document.connect3.submit();
        }
        else {
            alert("Bin Name should Not Be Empty")
        }

    }

    function rackCheck() {

        var Rack = document.getElementById("rackName").value
        if (Rack != "") {
            document.connect2.action = " /throttle/connectAddStorage.do?param=save2";
            document.connect2.submit();
        }
        else {
            alert("Rack Name should Not Be Empty")
        }


    }


    function check(sno3, binName, binId) {
        var count = parseInt(document.getElementById("count").value);


//        document.getElementById('inActive').style.display = 'block';

        for (i = 1; i <= count; i++) {

            if (i != sno3) {

                document.getElementById("bin" + i).checked = false;
            } else {
                document.getElementById("bin" + i).checked = true;
            }
        }

        document.getElementById("binName").value = binName;
        document.getElementById("binIdval").value = binId;
        console.log(document.getElementById("binIdval").value)



    }

    function check2(sno2, rackName, rackId) {
        var count = parseInt(document.getElementById("count2").value);


//        document.getElementById('inActive').style.display = 'block';

        for (i = 1; i <= count; i++) {

            if (i != sno2) {

                document.getElementById("rack" + i).checked = false;
            } else {
                document.getElementById("rack" + i).checked = true;
            }
        }

        document.getElementById("rackName").value = rackName;
        document.getElementById("rackIdval").value = rackId;
        console.log(document.getElementById("rackIdval").value)



    }


    function
            check1(sno1, storageName, storageId) {
        var count = parseInt(document.getElementById("count1").value);


//        document.getElementById('inActive').style.display = 'block';

        for (i = 1; i <= count; i++) {

            if (i != sno1) {

                document.getElementById("storage" + i).checked = false;
            } else {
                document.getElementById("storage" + i).checked = true;
            }
        }

        document.getElementById("storageName").value = storageName;
        document.getElementById("storageIdval").value = storageId;
        console.log(document.getElementById("storageIdval").value)

    }
</script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<%
    Date today = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    String todayDate = sdf.format(today);
%>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });</script>


<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<style>
    .margint{
        margin-bottom: 400px;
    }


</style>>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Add Stotage Details" text="Add Stotage Details"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Connect Master"/></a></li>
            <li class=""><spring:message code="hrms.label.Add Stotage Details" text="Add Stotage Details"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body"> 

            <body onload="">
                <br> 

                <div id="tabs" >
                    <ul>
                        <li><a href="#storage" style="width: auto"><span>Storage Details</span></a></li>
                        <li><a href="#rack"><span>Rack Details</span></a></li>                            
                        <li><a href="#bin"><span>Bin Details</span></a></li>

                    </ul>

                    <div id="storage">
                        <form name="connect1"  method="post" >

                            <%@ include file="/content/common/message.jsp" %>

                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="" colspan="8" >Storage</th>
                                    </tr>
                                </thead>
                                <tr class="margint">

                                    <td class="text1" width = "200px" >&nbsp;&nbsp;<font color="red">*</font>Storage Name</td>
                                    <td class="text1" width = "500px">
                                        <input type="text" name="storageName" id="storageName" onchange="checkStorage()" class="form-control" style="width:240px;height:40px" maxlength="50" autocomplete="off"/>
                                        <input type="hidden" name="storageIdval" id="storageIdval" class="form-control" style="width:240px;height:40px" maxlength="50" autocomplete="off"/>
                                    </td>
                                    <td>


                                        <input type="button" value="Submit" id="save1" class="btn btn-success btn-sm" onclick="storageCheck();">
                                    </td>
                                </tr>

                            </table>
                        </form>    
                        <br>


                        <form name="connectData1"  method="post" >  

                            <table class="table table-info mb30 table-hover" id="table" >
                                <thead>

                                    <tr height="30" >
                                        <th>S.No</th>
                                        <th>Storage</th>
                                        <th><p style=" margin-left: 40px">Action</p></th>


                                </tr>
                                </thead>
                                <tbody>
                                    <% int sno1 = 0;%>
                                    <c:if test = "${getStorageDetails != null}">
                                        <c:forEach items="${getStorageDetails}" var="pc">
                                            <%
                                                sno1++;
                                                String className1 = "text1";
                                                if ((sno1 % 1) == 0) {
                                                    className1 = "text1";
                                                } else {
                                                    className1 = "text2";
                                                }
                                            %>

                                            <tr>
                                                <td class="<%=className1%>"  align="left"> <%= sno1%> </td>
                                                <td class="<%=className1%>"  align="left"> <c:out value="${pc.storageName}" /></td>
                                                <td><input align="left" type="checkbox" id="storage<%= sno1%>" onclick="check1(<%= sno1%>, '<c:out value="${pc.storageName}" />', '<c:out value="${pc.storageId}" />')"></th>
                                                    <input type="hidden" name="storageId" id="storageId" value="'<c:out value="${pc.storageId}" />'" />
                                            </tr>
                                        </c:forEach>

                                    </c:if>

                                </tbody>
                                <input type="hidden" name="count1" id="count1" value="<%=sno1%>" />
                            </table>

                        </form> 
                    </div>

                    <div id="rack">

                        <form name="connect2"  method="post" >

                            <%@ include file="/content/common/message.jsp" %>

                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="" colspan="8" >Rack</th>
                                    </tr>
                                </thead>
                                <tr class="margint">

                                    <td class="text1" width = "200px" >&nbsp;&nbsp;<font color="red">*</font>Rack Name</td>
                                    <td class="text1" width = "500px"><input type="text" name="rackName" id="rackName" onchange="checkRack()" class="form-control" style="width:240px;height:40px" maxlength="50" autocomplete="off"/></td>
                                    <td><input type="button" value="Submit" id="save2" class="btn btn-success btn-sm" onClick="rackCheck();"></td>
                                <input type="hidden" name="rackIdval" id="rackIdval" class="form-control" style="width:240px;height:40px" maxlength="50" autocomplete="off"/>
                                </tr>

                            </table>
                        </form>   

                        <br>
                        <form name="connectData2"  method="post" >  
                            <table class="table table-info mb30 table-hover" id="table" >
                                <thead>

                                    <tr height="30" >
                                        <th>S.No</th>
                                        <th>Rack</th>
                                        <th><p style=" margin-left: 40px">Action</p></th>


                                </tr>
                                </thead>
                                <tbody>
                                    <% int sno2 = 0;%>
                                    <c:if test = "${getRackDetails != null}">
                                        <c:forEach items="${getRackDetails}" var="pc">
                                            <%
                                                sno2++;
                                                String className2 = "text1";
                                                if ((sno2 % 1) == 0) {
                                                    className2 = "text1";
                                                } else {
                                                    className2 = "text2";
                                                }
                                            %>

                                            <tr>
                                                <td class="<%=className2%>"  align="left"> <%= sno2%> </td>
                                                <td class="<%=className2%>"  align="left"> <c:out value="${pc.rackName}" /></td>
                                                <td><input align="left" type="checkbox" id="rack<%= sno2%>" onclick="check2(<%= sno2%>, '<c:out value="${pc.rackName}" />', '<c:out value="${pc.rackId}" />')"></th>
                                                    <input type="hidden" name="rackId" id="binId" value="'<c:out value="${pc.rackId}" />'" />
                                            </tr>
                                        </c:forEach>

                                    </c:if>

                                </tbody>
                                <input type="hidden" name="count" id="count2" value="<%=sno2%>" />
                            </table>

                        </form>                        

                    </div>            

                    <div id="bin">

                        <form name="connect3"  method="post" >

                            <%@ include file="/content/common/message.jsp" %>

                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="" colspan="8" >Bin</th>
                                    </tr>
                                </thead>
                                <tr class="margint">

                                    <td class="text1" width = "200px" >&nbsp;&nbsp;<font color="red">*</font>Bin Name</td>
                                    <td class="text1" width = "500px">
                                        <input type="text" name="binName" id="binName" onchange="checkBin()" class="form-control" style="width:240px;height:40px" maxlength="50" autocomplete="off"/>
                                        <input type="hidden" name="binIdval" id="binIdval" class="form-control" style="width:240px;height:40px" maxlength="50" autocomplete="off"/>
                                    </td>
                                    <td>


                                        <input type="button" value="Submit" id="save" class="btn btn-success btn-sm" onclick="binCheck();">
                                    </td>
                                </tr>

                            </table>
                        </form>



                        <form name="connectData3"  method="post" >  
                            <table class="table table-info mb30 table-hover" id="table" >
                                <thead>

                                    <tr height="30" >
                                        <th>S.No</th>
                                        <th>Bin</th>
                                        <th><p style=" margin-left: 40px">Action</p></th>


                                </tr>
                                </thead>
                                <tbody>
                                    <% int sno3 = 0;%>
                                    <c:if test = "${getBinDetails != null}">
                                        <c:forEach items="${getBinDetails}" var="pc">
                                            <%
                                                sno3++;
                                                String className3 = "text1";
                                                if ((sno3 % 1) == 0) {
                                                    className3 = "text1";
                                                } else {
                                                    className3 = "text2";
                                                }
                                            %>

                                            <tr>
                                                <td class="<%=className3%>"  align="left"> <%= sno3%> </td>
                                                <td class="<%=className3%>"  align="left"> <c:out value="${pc.binName}" /></td>
                                                <td><input align="left" type="checkbox" id="bin<%= sno3%>" onclick="check(<%= sno3%>, '<c:out value="${pc.binName}" />', '<c:out value="${pc.binId}" />')"></th>
                                                    <input type="hidden" name="binId" id="binId" value="'<c:out value="${pc.binId}" />'" />
                                            </tr>
                                        </c:forEach>

                                    </c:if>

                                </tbody>
                                <input type="hidden" name="count" id="count" value="<%=sno3%>" />
                            </table>

                        </form>                       


                    </div>

                </div>

            </body>      


            <script>
                $(".nexttab").click(function() {
                    var selected = $("#tabs").tabs("option", "selected");
                    $("#tabs").tabs("option", "selected", selected + 1);
                });
            </script>



            <%@ include file="../common/NewDesign/settings.jsp" %>