<%-- 
    Document   : productWarehouseMaster
    Created on : 14 Jun, 2021, 7:04:11 PM
    Author     : MAHENDIRAN R
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    function submitPage() {
//        var option = document.getElementsByClassName("opt").selectedIndex
//        if(option == 0){    
            document.productMaster.action = " /throttle/connectStorageMaster.do?param=save";
            document.productMaster.method = "post";
            document.productMaster.submit();
//        }
//        else{
//            alert("enter the ")
//        }

        
        
        

    }
    function setValues(sno, storageId, rackId, binId,status,storageWareHouseId) {
       
        var count = parseInt(document.getElementById("count").value);
    

//        document.getElementById('inActive').style.display = 'block';
        
        for (i = 1; i <= count; i++) {
          
            if (i != sno) {
               
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("storageId").value = storageId;
        document.getElementById("rackId").value = rackId;
        document.getElementById("binId").value = binId;
        document.getElementById("status").value = status;
        document.getElementById("storageWareHouseId").value = storageWareHouseId;
     
    }

</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Product Master" text="Storage Master"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Master"/></a></li>
                <li class=""><spring:message code="hrms.label.Product Master" text="Storage Master"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">
                    <form name="productMaster"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <br>
                        <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                            <input type="hidden" name="supplierId" id="supplierId" value=""  />
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="8" >Storage Master</th>
                                    </tr>
                                </thead>
                                
                                <tr>
                                    
                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Storage Name</td>
                                    <td class="text1"><select  align="center" class="form-control" style="width:240px;height:40px" name="storageId" id="storageId" >
                                            <option value='0' class="opt">--select--</option>                            
                                           
                                               <c:forEach items="${getStorageDetails}" var="list">
                                                   <option class="opt" value='<c:out value="${list.storageId}"/>'><c:out value="${list.storageName}"/></option>   
                                               </c:forEach>
                                               
                                           
                                        </select></td>
                                    
                                     <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Rack Name</td>
                                    <td class="text1"><select  align="center" class="form-control" style="width:240px;height:40px" name="rackId" id="rackId" >
                                            <option class="opt" value='0'>--select--</option>                            
                                           
                                               <c:forEach items="${getrackDetails}" var="list">
                                                   <option class="opt" value='<c:out value="${list.rackId}"/>'><c:out value="${list.rackName}"/></option>   
                                               </c:forEach>
                                               
                                           
                                        </select></td>
                                     <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Bin Name</td>
                                    <td class="text1"><select  align="center" class="form-control" style="width:240px;height:40px" name="binId" id="binId" >
                                            <option class="opt" value='0'>--select--</option>                            
                                           
                                               <c:forEach items="${getbinDetails}" var="list">
                                                   <option class="opt" value='<c:out value="${list.binId}"/>'><c:out value="${list.binName}"/></option>   
                                               </c:forEach>
                                               
                                           
                                        </select></td>
                                        
                                     
                                    
                                </tr>
                                <tr>
                                    
                                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Status</td>
                                     <td class="text1">
                                         <select  class="form-control"  style="width:240px;height:40px" name="statusid" id="statusid" >
                                             <option value='Y'>Active</option>     
                                             <option value='N'>InActive</option>     
                                         </select>
                                         
                                     </td>
                                </tr>
                                
                               
                                    <input type="hidden" name="storageWareHouseId" id="storageWareHouseId" value="">
                            </table>
                            <table>
                                <tr>
                                <center><a>
                                        <input  type="button" class="btn btn-success" align="center" value="Save" name="Submit" onClick="submitPage()">      
                                        
                                    </a>
                                </center>
                                </tr>
                            </table>
                            <br>
                            <table class="table table-info mb30 table-hover" id="table" >	
                                <thead>

                                    <tr height="30" style="color: white">
                                        <th>S No</th>
                                        
                                        <th>Storage Name</th>
                                        <th>Rack Name</th>
                                        <th>Bin Name</th>
                                        <th>Status</th>
                                        <th>Select</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>


                                    <% int sno = 0;%>
                                    <c:if test = "${getStorageWareHouse != null}">
                                        <c:forEach items="${getStorageWareHouse}" var="Pro">
                                            <%
                                                sno++;
                                                String className = "text1";
                                                if ((sno % 1) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                            %>

                                            <tr>
                                                <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.storageName}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.rackName}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.binName}" /></td>
                                                <td class="<%=className%>"  align="left"> 
                                                    <c:if test="${Pro.status == 'Y'}">Active</c:if>
                                                    <c:if test="${Pro.status == 'N'}">InActive</c:if>
                                                    </td>
                                               
                                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${Pro.storageId}" />', '<c:out value="${Pro.rackId}" />', '<c:out value="${Pro.binId}" />','<c:out value="${Pro.status}" />', '<c:out value="${Pro.storageWhid}" />' );"/></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </c:if>
                            </table>


                            <input type="hidden" name="count" id="count" value="<%=sno%>" />

                            <br>
                            <br>
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>

