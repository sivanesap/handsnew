
<%-- 
    Document   : imInvoiceUpload
    Created on : 29 Nov, 2021, 12:03:48 PM
    Author     : Vishal
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


    <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
    <script language="javascript" src="/throttle/js/validate.js"></script>  
    <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
    <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
    <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
    <script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
    <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <script src="/throttle/js/select2.js" defer></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/select2.css"/>
    <link rel="stylesheet" type="text/css" href="/throttle/css/select2-bootstrap.css"/>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                dateFormat: 'dd-mm-yy',
                yearRange: '1900:' + new Date().getFullYear(),
                changeMonth: true, changeYear: true
            });
        });

        function setNoOfCrates(val) {
            var noOfCrates1 = document.getElementsByName("noOfCrates1");
            var totalNoOfCrates = 0;
            for (var i = 0; i < noOfCrates1.length; i++) {
                if (noOfCrates1[i].value != "") {
                    totalNoOfCrates = parseInt(noOfCrates1[i].value) + totalNoOfCrates;
                }
            }
            document.getElementById("noOfCrates").value = totalNoOfCrates;
        }

    </script>

    <script>
        var rowCount = 0;
        var sno = 0;
        var rowIndex = 0;
        function addRow() {
            var tab = document.getElementById("addIndent");
            var iRowCount = tab.getElementsByTagName('tr').length;
            rowCount = iRowCount;
            var newrow = tab.insertRow(rowCount);
//        alert(iRowCount);
            sno = rowCount;
            rowIndex = rowCount;
//        alert("rowIndex:"+rowIndex);
            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' > <input type='hidden'  name='Sno' value='' > " + sno + "</td>";
            cell.innerHTML = cell0;


            cell = newrow.insertCell(1);
            var cell1 = "<td class='text1' height='25'><input type='text' class='form-control'  name='invoiceNo' id='invoiceNo" + sno + "' style='width:130px;height:40px' value=''></td>"
            cell.innerHTML = cell1;

            cell = newrow.insertCell(2);
            var cell2 = "<td class='text1' height='30'><input type='text' class='form-control datepicker' name='invoiceDate' style='width:130px;height:40px' id='invoiceDate" + sno + "'  value=''></td>"
            cell.innerHTML = cell2;

            cell = newrow.insertCell(3);
            var cell3 = "<td class='text1' align='right' height='30'><input type='hidden' name='customerId' id='customerId" + sno + "'><select class='form-control' name='customerIdTemp' onchange='setCrates(this.value," + sno + ")' style='width:180px;height:40px' id='customerIdTemp" + sno + "'><option value=''>-------Select------</option><c:forEach items="${getImDealerList}" var="de"><option value='<c:out value="${de.dealerId}"/>-<c:out value="${de.qty}"/>'><c:out value="${de.dealerName}"/></option></c:forEach></select><a onclick='addCustomer(" + sno + ")'>Add Customer</a>&emsp;<a><icon id='iconRef" + sno + "' class='fa fa-refresh' onclick='refreshCust(" + sno + ")' style='color:red;cursor:pointer'></icon></a></td>"
            cell.innerHTML = cell3;

            cell = newrow.insertCell(4);
            cell2 = "<td class='text1' height='25'><input type='text'class='form-control' name='billedQty' style='width:130px;height:40px' id='billedQty" + sno + "'  value='1.00'> </td>";
            cell.innerHTML = cell2;

            cell = newrow.insertCell(5);
            var cell4 = "<td class='text1' height='25' ><input type='text' class='form-control' name='dispatchQty' style='width:130px;height:40px' id='dispatchQty" + sno + "'   value='1.00' ></td>";
            cell.innerHTML = cell4;

            cell = newrow.insertCell(6);
            var cell5 = "<td class='text1' height='25' ><input type='text' class='form-control' name='noOfCrates1' style='width:130px;height:40px' id='noOfCrates1" + sno + "'   value='0'  onchange='setNoOfCrates(this)'><a onclick='viewCrates(" + sno + ")'>View Crates</a></td>";
            cell.innerHTML = cell5;

            cell = newrow.insertCell(7);
            var cell6 = "<td class='text1' height='25' ><input type='text' class='form-control' name='invoiceAmt' style='width:130px;height:40px' id='invoiceAmt" + sno + "'   value='' ></td>";
            cell.innerHTML = cell6;

            cell = newrow.insertCell(8);
            var cell7 = "<td class='text1' height='25' ><input type='button' class='btn btn-info' name='delete'  id='delete'   value='Delete Row' onclick='deleteRow(this," + sno + ");' ></td>";
            cell.innerHTML = cell7;
            var x = sno;
            $('.datepicker').datepicker();
            $('#customerIdTemp' + x).select2({placeholder: 'Fill Customer Name'});
            rowIndex++;
            rowCount++;
            sno++;
        }
        function deleteRow(src) {
            sno--;
            var oRow = src.parentNode.parentNode;
            var dRow = document.getElementById('addIndent');
            dRow.deleteRow(oRow.rowIndex);
            document.getElementById("selectedRowCount").value--;
        }
        function addCustomer(sno) {
            window.open('/throttle/imDealerMaster.do', 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function viewCrates(sno) {
            var dealerId = document.getElementById("customerId" + sno).value;
            window.open('/throttle/imInvoiceUpload.do?param=crate&dealerId=' + dealerId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function refreshCust(sno) {
            $.ajax({
                url: "/throttle/getImDealerMaster.do",
                dataType: 'json',
                success: function(data) {
                    if (data != '') {
                        $('#customerIdTemp' + sno).empty();
                        $('#customerIdTemp' + sno).append('<option value="">------Select Customer------</option>');
                        $.each(data, function(i, data) {
                            $('#customerIdTemp' + sno).append('<option value="' + data.custId + '-' + data.qty + '">' + data.custName + '</option>');
//                            document.getElementById('#iconRef' + sno).style.transform = "rotate(90deg)";
                        });
                    }
                }
            })
        }
        function submitPage(value) {
            if (document.getElementById("supplierId").value == "") {
                alert("Enter Supplier Name");
                document.getElementById("supplierId").focus();
            } else if (document.getElementById("vendorId").value == "") {
                alert("Enter Transporter Name");
                document.getElementById("vendorId").focus();
            } else if (document.getElementById("vehicleNo").value == "") {
                alert("Enter Vehicle No");
                document.getElementById("vehicleNo").focus();
            } else if (document.getElementById("vehicleType").value == "") {
                alert("Enter Vehicle Type");
                document.getElementById("vehicleType").focus();
            } else if (document.getElementById("driverName").value == "") {
                alert("Enter Driver Name");
                document.getElementById("driverName").focus();
            } else if (document.getElementById("driverMobile").value == "") {
                alert("Enter Driver Mobile");
                document.getElementById("driverMobile").focus();
            } else if (document.getElementById("dispatchDate").value == "") {
                alert("Enter Dispatch Date");
                document.getElementById("dispatchDate").focus();
            } else if (document.getElementById("tripStartTime").value == "") {
                alert("Enter Trip Start Time");
                document.getElementById("tripStartTime").focus();
            } else {
                if (sno == 0) {
                    alert("Select The Add Row")
                    return;
                }
                var count = 0;
                console.log(name);
                var invoiceNo = document.getElementsByName("invoiceNo");
                var invoiceDate = document.getElementsByName("invoiceDate");
                var customerId = document.getElementsByName("customerIdTemp");
                var invoiceAmt = document.getElementsByName("invoiceAmt");

                for (var i = 0; i < invoiceNo.length; i++) {
                    if (invoiceNo[i].value == "") {
                        alert("Enter All invoice No");
                        return;
                    }
                    if (invoiceDate[i].value == "") {
                        alert("Enter All Invoice Date")
                        return;
                    }
                    if (customerId[i].value == "") {
                        alert("Enter All Customer Name")
                        return;
                    }
                    if (invoiceAmt[i].value == "") {
                        alert("Enter All Invoice Amount")
                        return;
                    }

                }
                document.invoice.action = "/throttle/imInvoiceUpload.do?param=Save";
                document.invoice.submit();

            }
        }
//            }

        function setCrates(val, sno) {
            var custId = val.split('-')[0];
            var no = val.split('-')[1];
            $('#customerId' + sno).val(custId);
            $('#noOfCrates1' + sno).val(no);
//            $('#noOfCrates1'+sno).attr("readonly",true);
            setNoOfCrates('');
        }


//                function submitPage(value) {
        //                    for (var i = 0; i < links.length; i++) {
        //                    links[i].onclick = alert(links[i].title);
//                    }
        //                }

        function checkTrackable(val) {
            var deviceId = val.split('-')[1];
            var isTrackable = false;
            document.getElementById("lastDeviceId").value = deviceId;

            $.ajax({"url": "https://sct.intutrack.com/api/test/devices/is_trackable?device_id=" + deviceId,
                "method": "GET",
                "mode": "no-cors",
                "headers": {
                    "Authorization": "Basic ZTNBWmFCT1F2bUtKWU5palh6Tmx2eVJtV2RLdTVhZFkybTB6UXhFVDRZNUFBZ2s0VE5pdUR2bDZ5UGp3VzB0WjpIdHJzRnIyR0FyQUloc05obk9BQXNORHZ5TjBBSVdqaElLSDZMRVJGR3JMTnl1NHVRaTVWU2FzMExBUmVNRnQw"
                },
                success: function(data) {
                    console.log(data);
                    console.log(data.is_trackable);
                    if (!data.is_trackable) {
                        document.getElementById("lastResponse").value = "Device is Offline";
                        alert("Device is Offline");
                        document.getElementById("deviceId").value = "";
                    }
                },
                error: function(r, e) {
                    document.getElementById("deviceId").value = "";
                    alert(r.responseJSON.message);
                    console.log(r.responseJSON.message);
                    document.getElementById("lastResponse").value = r.responseJSON.message;
                }
            });
        }
        function addSelect2() {
            $('#vehicleType').select2();
            $('#deviceId').select2();
            $('#vendorId').select2();
            $('#supplierId').select2();
        }

        </script>

        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        </head>
        <div class="pageheader">
            <h2><i class="fa fa-edit"></i> LR Generate</h2>

            <div class="breadcrumb-wrapper">
                <span class="label">You Are Here</span>
                <ol class="breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="general-forms.html">Instamart</a></li>
                    <li class="">LR Generate</li>
                </ol>
            </div>
        </div>

        <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">

                    <body onload="addSelect2()">
                        <form name="invoice" method="post">
                        <%@include file = "/content/common/message.jsp"%>
                        <table class="table table-info mb30 table-hover">


                            <tr>
                                <td><text style="color:red">*</text>Supplier Name : &emsp;</td>
                            <td><select  type="text" name="supplierId" id="supplierId" class="form-control" style="width:240px;height:40px">
                                    <option value="">----Select----</option>
                                    <c:if test = "${getSupplierList != null}" >
                                        <c:forEach items="${getSupplierList}" var="Dept"> 
                                            <option value='<c:out value="${Dept.supplierId}" />'><c:out value="${Dept.supplierName}" /></option>
                                        </c:forEach >
                                    </c:if>  
                                </select>
                            </td>

                            <td><text style="color:red">*</text>Transporter Name : &emsp;</td>
                            <td><select  type="text" name="vendorId" id="vendorId" class="form-control" style="width:240px;height:40px">
                                    <option value="">----Select----</option>
                                    <c:forEach items="${getImVendorList}" var="vn">
                                        <option value="<c:out value="${vn.vendorId}"/>"><c:out value="${vn.vendorName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>

                            </tr>

                            <tr>

                                <td><text style="color:red">*</text>Device No : &emsp;</td>
                            <td>

                                <select id="deviceId" name="deviceId" onchange="checkTrackable(this.value)" class="form-control" style="width:240px;height:35px;">
                                    <option value="">-------Select One------</option>
                                    <c:forEach items="${deviceList}" var="did">
                                        <option value="<c:out value="${did.deviceId}"/>-<c:out value="${did.deviceNo}"/>"><c:out value="${did.deviceNo}"/></option>
                                    </c:forEach>
                                </select>
                                <input type="hidden" name="lastDeviceId" class="form-control" id="lastDeviceId" value="">
                                <input type="hidden" name="lastResponse" class="form-control" id="lastResponse" value="">
                            </td>
                            <td style><text style="color:red">*</text>Vehicle No : &emsp;</td>
                            <td>
                                <input type="text" name="vehicleNo" class="form-control" style="width:240px;height:40px" id="vehicleNo" value="">
                            </td>
                            </tr>

                            <tr>

                                <td style>No of Crates : &emsp;</td>
                                <td>
                                    <input type="text" name="noOfCrates" class="form-control" style="width:240px;height:40px" disabled ="true" id="noOfCrates" value="0">
                                </td>

                                <td style><text style="color:red">*</text>Vehicle Type : &emsp;</td>
                            <td>
                                <select name="vehicleType" class="form-control" style="width:240px;height:40px" id="vehicleType" >
                                    <option value="">-----Select Vehicle Type-----</option>
                                    <c:forEach items="${getVehicleTypeList}" var="vt">
                                        <option value="<c:out value="${vt.typeId}"/>"><c:out value="${vt.typeName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                            </tr>
                            <tr>
                                <td><text style="color:red">*</text>Driver Name : &emsp;</td>
                            <td>
                                <input type="text" name="driverName" class="form-control" style="width:240px;height:40px" id="driverName" value="">
                            </td>


                            <td><text style="color:red">*</text> Driver Mobile : &emsp;</td>
                            <td>
                                <input type="text" id="driverMobile" name="driverMobile" style="width:240px;height:40px;"  class="form-control" value="">
                            </td>
                            </tr>

                            <tr>
                                <td><text style="color:red">*</text> Dispatch Date :&emsp;</td>
                            <td>
                                <input type="text" id="dispatchDate" name="dispatchDate" style="width:240px;height:40px;" class="datepicker form-control" >
                            </td>

                            <td><text style="color:red">*</text>Trip Start Time :  &emsp;</td>
                            <td><input type="time" name="tripStartTime" id="tripStartTime" style="width:240px;height:40px;" class="form-control"></td>

                            </tr>
                            <tr>
                                <td>Remarks : &emsp;</td>
                                <td><input type="text" name="remarks" id="reamrks" style="width:240px;height:40px;"  class="form-control" ></td>
                            </tr>

                        </table>

                        <table class="table table-info mb30 table-hover sortable" id="addIndent" align="center" width="90%">
                            <thead >
                                <tr>
                                    <th>S.No</th>
                                    <th>Invoice No</th>
                                    <th>Invoice Date</th>  
                                    <th>Customer Name</th>
                                    <th>Billed Qty</th>
                                    <th>Dispatch Qty</th>
                                    <th>No of Crates</th>
                                    <th>Invoice Amount</th>
                                    <th>Delete</th>



                                </tr>
                            </thead>

                        </table>

                        <table>
                            <center>
                                <input value="Add Row" class="btn btn-success" id="countRow" type="button" onClick="addRow();" >
                                &emsp;<input type="button" value="Save" id="insert" class="btn btn-success" onClick="submitPage(this.value);">
                                &emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">
                            </center>

                        </table>

                    </form>
                </body>
                <%@include file="/content/common/NewDesign/settings.jsp"%>
                </html>
