<%-- 
    Document   : storageCreation
    Created on : 15 Sep, 2021, 2:05:03 PM
    Author     : alan
--%>


<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.IFB Shipment Details"  text="IFB Shipment Details"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Shipment Details"  text="Ifb"/></a></li>
            <li class="active">Storage Creation Details</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="">
                <form name="hubout" method="post" style="width:100%">

                    <table class="table table-info mb30 table-hover"  >

                        <thead>
                        <th colspan="4">Storage Creation</th>
                        </thead><tr>
                            <td><font color="red">*</font>Rack</td>
                            <td><input type="text" id="rack" class="form-control" style="width:240px;height: 40px" name="vehicleNo" value=""></td>
                            <td><font color="red">*</font>Bin </td>
                            <td><input type="text" id="bin" class="form-control" style="width:240px;height: 40px" name="driverName" value=""></td>
                            <td><font color="red">*</font>Local</td>
                            <td><input type="text" id="local" class="form-control" style="width:240px;height: 40px" name="vehicleNo" value=""></td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Driver Mobile</td>
                            <td><input type="number" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" id="driverMobile" class="form-control" style="width:240px;height: 40px" name="driverMobile" value=""></td>
                            <td><font color="red">*</font>Transporter</td>
                            <td>
                                <select id="vendorId" class="form-control" style="width:240px;height: 40px" name="vendorId">
                                    <c:forEach items="${vendorList}" var="ven">
                                        <option value="<c:out value="${ven.vendorId}"/>"><c:out value="${ven.vendorName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                   
                    </table>

                    

               
                  
                   
                    <center>
                        <input type="button" class="btn btn-success" name="hub" id="hub" value="Update" onclick="submitPage()" />
                    </center>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>


                </form>
            </body>
        </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>

    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

