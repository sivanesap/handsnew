<%-- 
    Document   : connectLrViewExcel
    Created on : 27 Oct, 2021, 10:59:06 AM
    Author     : Roger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Daily Advance Advice";
        request.setAttribute("menuPath", menuPath);
        String dateval = request.getParameter("dateval");
        String active = request.getParameter("active");
        String type = request.getParameter("type");
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "lrViewExcelExport-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>



            <br>
            <br>
            <br>
            <c:if test="${getDispatchDetails != null}">
                <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                    <thead style="font-size:12px">
                        <tr height="40">
                            <th  height="30" >S No</th>
                            <th  height="30" >Vendor Name</th>
                            <th  height="30" >Driver Name</th>
                            <th  height="30" >Driver Mobile</th>
                            <th  height="30" >LR No</th>
                            <th  height="30" >LR Date</th>
                            <th  height="30" >Supervisor Name</th>
                            <th  height="30" >No. of Boxes/Crates</th>
                            <th  height="30" >Remarks</th>
                            <th  height="30" >Vehicle No</th>
                            <th  height="30" >Invoice Nos</th>
                            <th  height="30" >vehicle Type</th>
                            <th  height="30" >Transfer Type</th>
                        </tr>
                    </thead>
                    <% int index = 0;
                        int sno = 1;
                        int count = 0;
                    %> 
                    <tbody>

                        <c:forEach items= "${getDispatchDetails}" var="list">
                            <tr height="30">
                                <td align="left"><%=sno%></td>
                                <td align="left"   ><c:out value="${list.vendorName}"/></td>
                                <td align="left"   ><c:out value="${list.driverName}"/></td>
                                <td align="left"   ><c:out value="${list.mobile}"/></td>
                                <td align="left"   ><c:out value="${list.lrnNo}"/></td>
                                <td align="left"   ><c:out value="${list.lrDate}"/></td>
                                <td align="left"   ><c:out value="${list.supervisorName}"/></td>
                                <td align="left"   ><c:out value="${list.boxQty}"/></td>
                                <td align="left"   ><c:out value="${list.remarks}"/></td>
                                <td align="left"   ><c:out value="${list.vehicleNo}"/></td>
                                <td align="left"   ><c:out value="${list.invoiceNo}"/></td>
                                <td align="left"   ><c:out value="${list.typeName}"/></td>
                                <td align="left"   ><c:out value="${list.transferType}"/></td>

                            </tr> 
                            <%
                                sno++;
                                index++;
                            %>
                        </c:forEach>
                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                    </tbody>
                </table>

            </c:if>
        </form>
    </body>
</html>
