<%-- 
    Document   : imMaterialPoPrint
    Created on : 17 Feb, 2022, 11:48:03 AM
    Author     : alan
--%>




<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <title>H&S - SquareCloud</title>

    <link href="content/NewDesign/css/style.default.css" rel="stylesheet">
    <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">




    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style type="text/css">

            .main{
                border: 1px solid black;

                margin-top: 2%;
            }


            .td1{
                border: 1px solid black;
                /* margin: 0%;
                        padding: 0%;*/
            }

            .tab{

                margin-top: 2%;
            }
            .tabdata{
                border: 1px solid black;
                height: 30%;

            }
            .table-bordered{
                border-color: black;
            }

            .eventdwidth{
                width: 100px;
                border: 1px solid black;
            }
            .cols{
                display: inline-block;
                padding: 2% 8%;

            }
            th{
                border-color: black;
            }
            .carb{
                resize: both;
                overflow: auto;
            }



        </style>

    </head>


    <script>

        var amount = "";



        function callme() {
            document.getElementById("print").style.display = "none"
            print();
            location.reload();
        }
    </script>


    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <form name="upload"  method="post" enctype="multipart/form-data">
                    <div style="text-align: center;">
                        <div id="ptp">
                        </div>     
                    </div>
                </form>

                <div class="container main" style="width:100%;">
                    <h2 style = " text-align:center;">Purchase Order</h2>
                    <div>
                        <table>
                            <tr>
                                <th style="padding:2px; border: 1px solid black;width:30%">Billing Address</th>
                                <th style="padding:2px; border: 1px solid black;width:30%">Shipping Address</th>
                            </tr>
                            <tr>
                                <td style="padding:2px; border: 1px solid black;width:20%">H&S Supply Chain Services Pvt Ltd,
                                    old No 12, New No 30 ,Chandralaya Apartments
                                    judge jambulingam Road , GST No 33AADCH546M1ZE
                                </td>
                                <td  style="padding:2px; border: 1px solid black;width:20%">
                                    <c:out value="${poViewDetails[0].wareHouseAddress}" />
                                </td>
                                <td style="padding-right:2px;text-align:right; ">

                                    <div style = "text-align:right;"><img  width = "90px" src = "images/HSSupplyLogo.png"></div>
                                    <div style="text-align:right;">PO: <c:out value="${poViewDetails[0].poNo}" /> </div>

                                    <p  style = " text-align:right;" id="demo"></p>

                                        <!--<div style="text-align:right;">Tax: <c:out value="${poViewDetails[0].taxType}" /> % </div>-->
                                    <script>

                                        var d = new Date().toLocaleDateString('en-us', {weekday: "long", year: "numeric", month: "short", day: "numeric"});
                                        //const day = d.getDay()
                                        document.getElementById("demo").innerHTML = "Date :" + d;
                                    </script>

                                </td>
                            </tr>

                        </table>  

                        <br>
                        <table>
                            <tr>
                                <th style="padding:2px; border: 1px solid black;">Vendor</th>
                            </tr>
                            <tr>
                                <td width="183px" style="padding:2px; border: 1px solid black;">
                                    <c:out value="${poViewDetails[0].custAddress}" />
                                </td>
                            </tr>
                        </table>   
                    </div>




                    <div class="secondSection" >
                        <table class="table table-bordered" style="margin-top: 20px;">
                            <tr>
                                <th>Sno</th>
                                <th>Material Name</th>
                                <th>UOM</th>
                                <th>Unit Price</th>
                                <th>Qty</th>
                                <th>Total Price</th>
                            </tr>
                            <% int index = 0;
                                   int sno = 1;
                                   int count = 0;
                            %> 

                            <c:if test = "${poViewDetails != null}" >

                                <tbody>


                                    <c:forEach items="${poViewDetails}" var="pc">


                                        <tr >
                                            <td align="left" class="td1"> <%=sno%> </td>
                                            <td align="left" class="td1"><c:out value="${pc.materialName}"/></td>
                                            <td align="left" class="td1"><c:out value="${pc.uoms}"/></td>
                                            <td align="left" class="td1"><c:out value="${pc.unitPrice}"/></td>
                                            <td align="left" class="td1"><c:out value="${pc.qty}"/></td>
                                            <td align="left" class="td1"><c:out value="${pc.amount}"/></td>
                                    <input type="hidden" name="calulate" id="calulate" value="<c:out value="${pc.amount}"/>"/>
                                    </tr>

                                    <%
                                        sno++;
                                        index++;
                                    %>
                                </c:forEach>
                                <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                </tbody>
                            </table>
                        </c:if>
                        </table>
                        <table style="width: 100%; float: left" >


                            <tr>
                                <td colspan="1"></td>
                                <td colspan="3"></td>

                                <td colspan="0" style="text-align: left">  <div style="text-align:left;" >Created By:<b> <c:out value="${poViewDetails[0].createdBy}" /> </b><br>Created Date:<b><c:out value="${poViewDetails[0].createdDate}" /></b></div></td>
                            </tr>
                            <tr>
                                <td colspan="1"></td>
                                <td colspan="3"></td>

                                <td colspan="0" style="text-align: left">  <div style="text-align:left;" >Approved By:<b> <c:out value="${poViewDetails[0].approvedUser}" /> </b><br>Approved Date:<b><c:out value="${poViewDetails[0].approvedDate}" /></b></div></td>
                            </tr>

                            <tr>

                                <td colspan="1"></td>
                                <td colspan="3"></td>
                                <td colspan="0" style="text-align: left">  <div style="text-align:left;" >Authorized By:<b> <c:out value="${poViewDetails[0].authUser}" /> </b><br>Authorized Date:<b><c:out value="${poViewDetails[0].authDate}" /></b></div></td>
                            <input type="hidden" name="tacco" id="tacco" value="<c:out value="${poViewDetails[0].taxType}" />"/>

                            </tr>


                        </table>

                        <table style="width: 100%; float: right" >
                            <tr>
                                <td colspan="1"></td>
                                <td colspan="3"></td>

                                <td colspan="0" style="text-align: left">  <div style="text-align:right;" id="sub">Sub Total: </div></td>
                            </tr>

                            <tr>
                                <td colspan="1"></td>
                                <td colspan="3"></td>

                                <td colspan="0" style="text-align: left">  <div style="text-align:right;" id="taxt">Tax: <c:out value="${poViewDetails[0].taxType}" /> % </div></td>
                            </tr>

                            <tr>
                                <td colspan="1"></td>
                                <td colspan="3"></td>
                            <input type="hidden" name="tacco" id="tacco" value="<c:out value="${poViewDetails[0].taxType}" />"/>

                            <td colspan="0" style="text-align: left">  <div style="text-align:right;" id="grand">Grand Total: </div></td>
                            </tr>


                        </table>
                        <br>
                        <br>

                        <table style="width: 100%;" >
                            <br>
                            <br><br>

                            <tr>
                                <td colspan="1"></td>
                                <td colspan="3"></td>

                                <td colspan="0" style="text-align: right;margin-bottom:2%"> Authorized Signatory</td>
                            </tr>


                        </table>
                        <table style="width: 100%; margin-bottom:1%;" >

                            <tr>
                                <td colspan="1"></td>
                                <td colspan="3"></td>

                                <td colspan="0" style="text-align: right">For H&S Supply Chain Services P Ltd</td>
                            </tr>
                        </table>



                    </div>
                    <br>

                </div>
            </div>
            <br>
        </div>

        <script>


            var qty = document.getElementsByName("calulate");
//                            alert(qty)
            let
            totQty = 0.00;
            for (var i = 0; i < qty.length; i++) {
                if (qty[i].value != "") {
                    totQty = parseFloat(qty[i].value) + totQty;
                }
            }
            document.getElementById("sub").innerHTML = "Sub Total :" + totQty.toFixed(2);

            var vart = document.getElementById("tacco").value;


            var price = document.getElementsByName("calulate");
            var priceTotQty = 0;
            for (var i = 0; i < price.length; i++) {
                if (price[i].value != "") {
                    priceTotQty = parseFloat(price[i].value) + priceTotQty;
                }
            }


            if (vart == "18") {
                var taxed = priceTotQty * 18 / 100;
                document.getElementById("taxt").innerHTML = "Tax 18%: " + taxed.toFixed(2);
                document.getElementById("grand").innerHTML = "Grand Total :" + (parseFloat(taxed) + parseFloat(priceTotQty)).toFixed(2);

            } else {

                document.getElementById("taxt").innerHTML = "Tax 0%";
                document.getElementById("grand").innerHTML = "Grand Total" + priceTotQty.toFixed(2);
            }


        </script>



    </div>

    <center>

        <input type="button" class="btn btn-success" name="print"  id="print"  value="Print" onclick="callme()">&nbsp;&nbsp;

    </center> 


</html>


