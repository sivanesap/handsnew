<%-- 
    Document   : SupplierWarehouseMaster
    Created on : 11 Jun, 2021, 1:26:47 PM
    Author     : MAHENDIRAN R
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    function submitPage() {
        document.SupplierWarehouseMaster.action = " /throttle/saveSupplierWarehouseMaster.do";
        document.SupplierWarehouseMaster.method = "post";
        document.SupplierWarehouseMaster.submit();
    }
    function setValues(sno, supplierWhId, whId, supplierId, plant, address, address1, city, pincode, contactNo, emailId, gstNo, budget1, budget2, budget3) {
//        alert("hi")
        var count = parseInt(document.getElementById("count").value);
        alert(supplierId);

        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("supplierWhId").value = supplierWhId;
        document.getElementById("whId").value = whId;
        document.getElementById("plant").value = plant;
        document.getElementById("address").value = address;
        document.getElementById("address1").value = address1;
        document.getElementById("city").value = city;
        document.getElementById("pincode").value = pincode;
        document.getElementById("contactNo").value = contactNo;
        document.getElementById("emailId").value = emailId;
        document.getElementById("gstNo").value = gstNo;
        document.getElementById("budget1").value = budget1;
        document.getElementById("budget2").value = budget2;
        document.getElementById("budget3").value = budget3;
        document.getElementById("supplierId").value = supplierId;
    }

</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Supplier Warehouse Master" text="Supplier Warehouse Master "/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Master"/></a></li>
                <li class=""><spring:message code="hrms.label.Supplier Warehouse Master" text="Supplier Warehouse Master"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">
                    <form name="SupplierWarehouseMaster"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <br>
                        <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                            <input type="hidden" name="supplierWhId" id="supplierWhId" value=""  />
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="8" >Supplier Warehouse Master</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>
                                        <font color="red">*</font>Warehouse
                                    </td>
                                    <td>
                                        <select id="whId" name="whId" align="center" style="width:240px;height:40px" class="form-control" >
                                            <option value="">-----Select Warehouse------</option>
                                            <c:forEach items="${getWhList}" var="wh">
                                                <option value="<c:out value="${wh.whId}"/>"><c:out value="${wh.whName}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Supplier Name</td>
                                    <td class="text1">
                                        <select  align="center" class="form-control" style="width:240px;height:40px" name="supplierId" id="supplierId" >
                                            <option value='0'>--select--</option>                            
                                           
                                               <c:forEach items="${getSupplierList}" var="list">
                                                   <option value='<c:out value="${list.supplierId}"/>'><c:out value="${list.supplierName}"/></option>   
                                               </c:forEach>
                                               
                                           
                                        </select>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Plant</td>
                                    <td class="text1"><input type="text" name="plant" id="plant" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checksupplierCategoryName();" autocomplete="off"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Address 1</td>
                                    <td class="text1"><input type="text" name="address" id="address" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checksupplierCategoryName();" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Address 2</td>
                                    <td class="text1"><input type="text" name="address1" id="address1" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checksupplierCategoryName();" autocomplete="off"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>City</td>
                                    <td class="text1"><input type='text' align="center" class="form-control" style="width:240px;height:40px" name="city" id="city" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Postal Code</td>
                                    <td class="text1"><input type="text" name="pincode" id="pincode" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Contact No</td>
                                    <td class="text1"><input type="text" name="contactNo" id="contactNo" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>E-mail id</td>
                                    <td class="text1"><input type="text" name="emailId" id="emailId" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>GST Number</td>
                                    <td class="text1"><input type="text" name="gstNo" id="gstNo" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Budget1</td>
                                    <td class="text1"><input type="text" name="budget1" id="budget1" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Budget2</td>
                                    <td class="text1"><input type="text" name="budget2" id="budget2" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Budget3</td>
                                    <td class="text1"><input type="text" name="budget3" id="budget3" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Status</td>
                                    <td class="text1">
                                        <select  align="center" class="form-control" style="width:240px;height:40px" name="ActiveInd" id="ActiveInd" >
                                            <option value='Y'>Active</option>
                                            <option value='N' id="inActive" style="display: none">In-Active</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                <center><a>
                                        <input  type="button" class="btn btn-success" align="center" value="Save" name="Submit" onClick="submitPage()">
                                    </a>
                                </center>
                                </tr>
                            </table>
                            <br>
                            <table class="table table-info mb30 table-hover" id="table" >	
                                <thead>

                                    <tr height="30" style="color: white">
                                        <th>S No</th>
                                        <!--<th>Supplier Id</th>-->
                                        <th>Supplier</th>
                                        <th>Plant</th>
                                        <th>Address 1</th>
                                        <th>Address 2</th>
                                        <th>City</th>
                                        <th>Postal Code</th>
                                        <th>Contact No</th>
                                        <th>E-mail id</th>
                                        <th>GST Number</th>
                                        <th>Budget1</th>
                                        <th>Budget2</th>
                                        <th>Budget3</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>


                                    <% int sno = 0;%>
                                    <c:if test = "${SupplierWarehouseMaster != null}">
                                        <c:forEach items="${SupplierWarehouseMaster}" var="War">
                                            <%
                                                sno++;
                                                String className = "text1";
                                                if ((sno % 1) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                            %>

                                            <tr>
                                                <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                                <!--<td class="<%=className%>"  align="left"> <c:out value="${War.supplierId}" /></td>-->
                                                <td class="<%=className%>"  align="left"> <c:out value="${War.supplierName}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${War.plant}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${War.address}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${War.address1}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${War.city}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${War.pincode}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${War.contactNo}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${War.emailId}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${War.gstNo}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${War.budget1}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${War.budget2}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${War.budget3}" /></td>
                                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${War.supplierWhId}" />', '<c:out value="${War.whId}" />', '<c:out value="${War.supplierId}" />', '<c:out value="${War.plant}" />', '<c:out value="${War.address}" />', '<c:out value="${War.address1}" />', '<c:out value="${War.city}" />', '<c:out value="${War.pincode}" />', '<c:out value="${War.contactNo}" />', '<c:out value="${War.emailId}" />', '<c:out value="${War.gstNo}" />', '<c:out value="${War.budget1}" />', '<c:out value="${War.budget2}" />', '<c:out value="${War.budget3}" />');" /></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </c:if>
                            </table>


                            <input type="hidden" name="count" id="count" value="<%=sno%>" />

                            <br>
                            <br>
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" selected="selected">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span>Entries Per Page</span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("table", 1);
                            </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
