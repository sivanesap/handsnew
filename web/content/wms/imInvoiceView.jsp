<%-- 
    Document   : imInvoiceView
    Created on : 30 Nov, 2021, 6:56:21 PM
    Author     : Alan
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>


<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });


    function exportExcel() {
        document.connect.action = '/throttle/connectLrView.do?param=Export';
        document.connect.submit();
    }


</script>

<script type="text/javascript">
    function submitPage(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            document.connect.action = '/throttle/imInvoiceView.do?param=Search';
            document.connect.submit();
        }
    }
    function checkSelectStatus(sno, obj) {
        var val = document.getElementsByName("selectedStatus");
        if (obj.checked == true) {
            document.getElementById("selectedStatus" + sno).value = 1;
        } else if (obj.checked == false) {
            document.getElementById("selectedStatus" + sno).value = 0;
        }

    }

    function apiFetchTrip(deviceNo, apiTripId) {
        window.open('/throttle/apiFetchTrip.do?apiTripId=' + apiTripId, 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
    }
    function viewInvoice(lrId) {
        document.connect.action = "/throttle/imInvoiceView.do?param=view&lrId=" + lrId;
        document.connect.submit();
    }
    function viewInvoicePrint(lrId) {
        window.open("/throttle/imInvoiceView.do?param=print&lrId=" + lrId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function closeApiTrip(deviceNo, tripId) {
        var tripId = tripId;
        var x = confirm("Confirm to Close Trip");
        if (x) {
            $.ajax({"url": "https://sct.intutrack.com/api/test/trips/end/" + tripId,
                "method": "POST",
                "mode": "no-cors",
                "headers": {
                    "Authorization": "Basic ZTNBWmFCT1F2bUtKWU5palh6Tmx2eVJtV2RLdTVhZFkybTB6UXhFVDRZNUFBZ2s0VE5pdUR2bDZ5UGp3VzB0WjpIdHJzRnIyR0FyQUloc05obk9BQXNORHZ5TjBBSVdqaElLSDZMRVJGR3JMTnl1NHVRaTVWU2FzMExBUmVNRnQw"
                },
                success: function(data) {
                    console.log(data);
                    alert(data.msg);
                },
                error: function(r, e) {
                    console.log(r.responseJSON.message);
                    alert(r.responseJSON.message);
                }
            });
        }
    }
    
    function closeApiTrip(deviceNo, tripId) {
        var tripId = tripId;
        var x= confirm("Confirm to Close Trip");
        if(x){
        $.ajax({"url": "https://sct.intutrack.com/api/test/trips/end/" + tripId,
            "method": "POST",
            "mode": "no-cors",
            "headers": {
                "Authorization": "Basic ZTNBWmFCT1F2bUtKWU5palh6Tmx2eVJtV2RLdTVhZFkybTB6UXhFVDRZNUFBZ2s0VE5pdUR2bDZ5UGp3VzB0WjpIdHJzRnIyR0FyQUloc05obk9BQXNORHZ5TjBBSVdqaElLSDZMRVJGR3JMTnl1NHVRaTVWU2FzMExBUmVNRnQw"
            },
            success: function(data) {
                console.log(data);
                alert(data.msg);
            },
            error: function(r, e) {
                console.log(r.responseJSON.message);
                alert(r.responseJSON.message);
            }
        });
    }
    }
</script>


<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Invoice View</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="general-forms.html">Instamart</a></li>
            <li class="active">Invoice View</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="connect" method="post">
                    <!--                    <table class="table table-info mb30 table-hover" >
                                            <tr>
                                                <td align="center"><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                                <td height="30"><input name="fromDate" id="fromDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                                <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                                <td height="30"><input name="toDate" id="toDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                            </tr>
                                            <tr>
                                                <td>Warehouse</td>
                                                <td>
                                                    <select id="whId" class="form-control" style="width:240px;height: 40px" name="whId" onchange="showData(this.value);">
                                                        <option value="">------Select Warehouse------</option>
                    <c:forEach items="${getWareHouse}" var="wh">
                        <option value="<c:out value="${wh.whId}"/>"><c:out value="${wh.whName}"/></option>
                    </c:forEach>
                </select>
            </td>
        </tr>
    </table>
    <center>

        <input type="button" class="btn btn-success" name="Search"  id="Search"  value="<spring:message code="operations.reports.label.Search" text="Search"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;
        <input type="button" class="btn btn-success" name="Export"  id="Export"  value="Export Excel" onclick="exportExcel();">&nbsp;&nbsp;

        <br><br>
    </center>-->
                    
                       <table class="table table-info mb30 table-hover" >
                      <tr>
                            <td align="center"><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                            <td height="30"><input name="fromDate" id="fromDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                            <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                            <td height="30"><input name="toDate" id="toDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                        </tr>
                    </table>
                          <center>

                        <input type="button" class="btn btn-success" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);">&nbsp;&nbsp;

                    </center>

                    <br>
                    <br>
                    <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                        <thead style="font-size:12px">
                            <tr height="40">
                                <th  height="30" >S No</th>
                                <th  height="30" >LR No</th>
                                <th  height="30" >LR Date</th>
                                <th  height="30" >Trip Start Time</th>
                                <th  height="30" >Vendor Name</th>
                                <th  height="30" >Vehicle No.</th>
                                <th  height="30" >Driver Name</th>
                                <th  height="30" >Driver Mobile</th>
                                <th  height="30" >Supplier Name</th>
                                <th  height="30" >Device No</th>
                                <th  height="30" >Remarks</th>
                                <th height="30">Action</th>
                            </tr>
                        </thead>
                        <% int index = 0;
                            int sno = 1;
                            int count = 0;
                        %> 
                        <tbody>

                            <c:forEach items= "${getImInvoiceMaster}" var="list">
                                <tr height="30">
                                    <td align="left"><%=sno%></td>
                                    <td align="left"   ><a onclick="viewInvoice('<c:out value="${list.lrId}"/>')"><c:out value="${list.lrNo}"/></a></td>
                                    <td align="left"   ><c:out value="${list.dispatchDate}"/></td>
                                    <td align="left"   ><c:out value="${list.tripStartTime}"/></td>
                                    <td align="left"   ><c:out value="${list.vendorName}"/></td>
                                    <td align="left"   ><c:out value="${list.vehicleNo}"/></td>
                                    <td align="left"   ><c:out value="${list.driverName}"/></td>
                                    <td align="left"   ><c:out value="${list.driverMobileNo}"/></td>
                                    <td align="left"   ><c:out value="${list.supplierName}"/></td>
                                    <td align="left"   ><c:out value="${list.deviceNo}"/></td>
                                    <td align="left"   ><c:out value="${list.remarks}"/></td>
                                    <td>
                                        <!--<input type="button" class="btn btn-success" name="Print" value="Print" onclick="viewInvoicePrint('<c:out value="${list.lrId}"/>')">-->
                                        <span class="label label-success" style="cursor: pointer" onclick="viewInvoicePrint('<c:out value="${list.lrId}"/>')">Print</span>
                                        <br>
                                        <c:if test="${list.tripId!=''&&list.tripId!=null}">
                                            <a href="#" style="color:grey;"  onclick="apiFetchTrip('<c:out value="${list.deviceNo}"/>', '<c:out value="${list.tripId}"/>')" >
                                                <br>
                                                <span class="label label-warning">Fetch Location</span> 
                                                <br>
                                            </a>
                                            <a href="#" style="color:grey;"  onclick="closeApiTrip('<c:out value="${list.deviceNo}"/>', '<c:out value="${list.tripId}"/>')" >
                                                <br>
                                                <span class="label label-warning">Close Trip</span> 
                                                <br>
                                            </a>
                                        </c:if>
                                    </td>

                                </tr> 
                                <%
                                    sno++;
                                    index++;
                                %>
                            </c:forEach>
                        <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                        </tbody>
                    </table>




                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>


                    <br/>
                    <br/>

                    <br/>
                    <br/>
                    <br/>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
