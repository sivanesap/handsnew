<%-- 
    Document   : productWarehouseMaster
    Created on : 14 Jun, 2021, 7:04:11 PM
    Author     : MAHENDIRAN R
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    function checksapCode() {
        var url = "";
        var sapcheck = document.getElementById("sapCode").value
        if (sapcheck != "") {
            url = "./checkExistSap.do?sapCode=" + sapcheck;

            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("POST", url, true);
            httpRequest.onreadystatechange = function () {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var response = httpRequest.responseText;
                        if (response != "") {
                            alert(response);
                            //console.log(response)
                            document.getElementById("sapCode").value = ""
                        }
                    }
                }
            };
            httpRequest.send(null);
        }

    }

    function checkskuCode() {
        var url = "";
        var skucheck = document.getElementById("skuCode").value
        if (skucheck != "") {
            url = "./checkExistSku.do?skuCode=" + skucheck;

            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("POST", url, true);
            httpRequest.onreadystatechange = function () {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var response = httpRequest.responseText;
                        if (response != "") {
                            alert(response);
                            //console.log(response)
                            document.getElementById("skuCode").value = ""
                        }
                    }
                }
            };
            httpRequest.send(null);
        }

    }
    function submitPage() {
        if (document.getElementById("categoryName").value == "") {
            alert("Choose Category");
            document.getElementById("categoryName").focus();
        } else if (document.getElementById("supplierName").value == "") {
            alert("Choose Supplier");
            document.getElementById("supplierName").focus();
        } else if (document.getElementById("sapCode").value == "") {
            alert("Enter Sap Code");
            document.getElementById("sapCode").focus();
        } else if (document.getElementById("multiSap").value == "") {
            alert("Enter Multi Sap");
            document.getElementById("multiSap").focus();
        } else if (document.getElementById("skuCode").value == "") {
            alert("Enter Sku Code");
            document.getElementById("skuCode").focus();
        } else if (document.getElementById("productName").value == "") {
            alert("Enter Product Name");
            document.getElementById("productName").focus();
        } else if (document.getElementById("mrp").value == "") {
            alert("Enter MRP");
            document.getElementById("mrp").focus();
        } else if (document.getElementById("packQty").value == "") {
            alert("Enter Pack Quantity");
            document.getElementById("packQty").focus();
        } else if (document.getElementById("actualWeight").value == "") {
            alert("Enter Actual Weight");
            document.getElementById("actualWeight").focus();
        } else {
            document.productMaster.action = " /throttle/saveProductWarehouseMaster.do";
            document.productMaster.method = "post";
            document.productMaster.submit();
        }




    }
    function setValues(sno, productId, categoryId, supplierId, sapCode, multiSap, skuCode, productName, mrp, packQty, volumeWeight, actualWeight, scannable) {

        var count = parseInt(document.getElementById("count").value);

        for (i = 1; i <= count; i++) {

            if (i != sno) {

                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("productId").value = productId;
        document.getElementById("categoryName").value = categoryId;
        document.getElementById("supplierName").value = supplierId;
        document.getElementById("sapCode").value = sapCode;
        document.getElementById("multiSap").value = multiSap;
        document.getElementById("skuCode").value = skuCode;
        document.getElementById("productName").value = productName;
        document.getElementById("mrp").value = mrp;
        document.getElementById("packQty").value = packQty;
        document.getElementById("volumeWeight").value = volumeWeight;
        document.getElementById("actualWeight").value = actualWeight;
        document.getElementById("scannable").value = scannable;
    }


    function uploadPage() {

        var supplierName = document.getElementById("supplier").value;
        var categoryName = document.getElementById("category").value;
        alert(supplierName)
        alert(categoryName)
        if (supplierName == "") {
            alert("Please Select Supplier Name");
        } else if (categoryName == "") {
            alert("Please Enter Category Name");
        } else {

            document.upload.action = "/throttle/productMasterUpload.do?param=upload&cat="+categoryName;
            document.upload.submit();

        }
    }

    function insertTo() {


        var result = confirm("Some errors are not Updated. Do you want to continue?");
        if (result == true) {
            document.upload.action = "/throttle/productMasterUploads.do?";
            document.upload.method = "post";
            document.upload.submit();
        }
        else {

            document.upload.action = "/throttle/productMasterUploads.do?";
            document.upload.method = "post";
            document.upload.submit();
        }
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Product Master" text="Product Master "/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Master"/></a></li>
                <li class=""><spring:message code="hrms.label.Product Master" text="Product Master"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">

                    <form name="upload"  method="post" enctype="multipart/form-data">
                        <%--<%@ include file="/content/common/path.jsp" %>--%>

                        <%@ include file="/content/common/message.jsp" %>


                        <table class="table table-info mb30 table-hover" id="uploadTable"  >
                            <thead> 
                                <tr>
                                    <th  colspan="3">Product Master Upload</th>
                                </tr>

                            </thead>

                            <tr> 
                                <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Supplier Name</td>
                                <td class="text1">
                                    <select  align="center" class="form-control" style="width:240px;height:40px" name="supplier" id="supplier" >
                                        <option value=''>--select--</option>                            
                                        <c:if test = "${supplierList != null}" >
                                            <c:forEach items="${supplierList}" var="cust"> 
                                                <option value='<c:out value="${cust.supplierId}" />'><c:out value="${cust.supplierName}" /></option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                </td>

                            </tr>
                            <tr> 
                                <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Category Name Name</td>
                                <td class="text1">
                                    <select  align="center" class="form-control" style="width:240px;height:40px" name="category" id="category" >
                                        <option value=''>--select--</option>                            
                                        <c:if test = "${categoryMaster != null}" >
                                            <c:forEach items="${categoryMaster}" var="cust"> 
                                                <option value='<c:out value="${cust.categoryId}" />'><c:out value="${cust.categoryName}" /></option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td >Select Excel</td>
                                <td ><input type="file" name="importCnote" id="importCnote"  class="importCnote"><font size="1"><u><a href="uploadedxls/connectProductMaster.xls" download>sample file</a></u></font></td>                             
                                <td colspan="0"><input type="button" class="btn btn-success" value="Upload" name="UploadPage" id="UploadPage" onclick="uploadPage();"></td>
                            </tr>

                        </table>
                        <div id="ptp">
                            <div class="inpad" style=" overflow-x: visible;">

                                <c:if test = "${getProductList != null}" >

                                    <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                        <thead style="font-size:12px">
                                            <tr height="40">
                                                <th  height="30" >S No</th>
                                                <th  height="30" >Sap Code</th>
                                                <th  height="30" >MulitSap Code</th>
                                                <th  height="30" >Sku Code</th>
                                                <th  height="30" >Product Name</th>
                                                <th  height="30" >Mrp</th>
                                                <th  height="30" >Product Qty</th>
                                                <th  height="30" >Volume Weight</th>
                                                <th  height="30" >Actual Weight</th>  
                                                <th  height="30" >Active Indent</th> 
                                                <th  height="30" >Scannable</th>
                                                <th  height="30" >Supplier Name</th>
                                                <th  height="30" >Category Name</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <% int index = 0; 
                                         int sno = 1;
                                         int count=0;
                                        %> 
                                        <tbody>

                                            <c:forEach items= "${getProductList}" var="contractList">
                                                <tr height="30">
                                                    <td align="left"><%=sno%></td>
                                                    <td align="left"   ><c:out value="${contractList.sapCode}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.mulitSap}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.skuCode}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.productName}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.mrp}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.qty}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.volumeWeight}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.actualWeight}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.activeInd}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.scannable}"/></td>

                                                    <td align="left"   ><c:out value="${contractList.supplierName}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.categoryName}"/></td>


                                                    <c:if test="${contractList.status==0}">
                                                        <td align="left"><font color="green">Ready To Submit</font></td>
                                                            </c:if>
                                                            <c:if test="${contractList.status==2}">
                                                        <td align="left"><font color="red">Sku Code and Sap Code already Exists in main Table.</font></td>
                                                                <%count++;%>
                                                            </c:if>
                                                            <c:if test="${contractList.status==3}">
                                                        <td align="left"><font color="red">Sku Code and Sap Code already Exists in Temp Table.</font></td>
                                                                <%count++;%>
                                                            </c:if>

                                                    <c:if test="${contractList.status==4}">
                                                        <td align="left"><font color="red">Sap Code Already Exists</font></td>
                                                                <%count++;%>
                                                            </c:if>
                                                            <c:if test="${contractList.status==5}">
                                                        <td align="left"><font color="red">Sku Code Already Exists</font></td>
                                                                <%count++;%>
                                                            </c:if>

                                                    <c:if test="${contractList.status==6}">
                                                        <td align="left"><font color="red">Sku Code and Sap Code already Exists.</font></td>
                                                                <%count++;%>
                                                     </c:if>
                                                           


                                                </tr> 
                                                <%
                                                sno++;
                                                index++;
                                                %>
                                            </c:forEach>
                                        <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                        </tbody>
                                    </table>

                                    <center>

                                        <td colspan="0"><input type="button" class="btn btn-success" value="Submit" name="submitPage" id="submitPage" onclick="insertTo();"></td>
                                    </center>
                                </c:if>
                            </div>
                        </div>     

                    </form>
                    <form name="productMaster"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <br>
                        <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                            <input type="hidden" name="productId" id="productId" value=""  />
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="8" >Product Master</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Category Name</td>
                                    <td class="text1">
                                        <select type="text" name="categoryName" id="categoryName" class="form-control" style="width:240px;height:40px" maxlength="50">
                                            <option value="">-----Select Category-----</option>
                                            <c:forEach items="${categoryMaster}" var="cat">
                                                <option value="<c:out value="${cat.categoryId}"/>"><c:out value="${cat.categoryName}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Supplier Name</td>
                                    <td class="text1">
                                        <select type="text" name="supplierName" id="supplierName" class="form-control" style="width:240px;height:40px" maxlength="50" >
                                            <option value="">-----Select Supplier-----</option>
                                            <c:forEach items="${supplierList}" var="sup">
                                                <option value="<c:out value="${sup.supplierId}"/>"><c:out value="${sup.supplierName}"/></option>    
                                            </c:forEach>
                                        </select>
                                    </td>

                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Sap Code</td>
                                    <td class="text1"><input type="text" name="sapCode" id="sapCode" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checksapCode()"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Multi Sap</td>
                                    <td class="text1"><input type="text" name="multiSap" id="multiSap" class="form-control" style="width:240px;height:40px" maxlength="50"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Sku Code</td>
                                    <td class="text1"><input type='text' align="center" class="form-control" style="width:240px;height:40px" name="skuCode" id="skuCode" autocomplete="off" onchange="checkskuCode()"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Product Name</td>
                                    <td class="text1"><input type="text" name="productName" id="productName" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>MRP</td>
                                    <td class="text1"><input type="text" name="mrp" id="mrp" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Pack Qty</td>
                                    <td class="text1"><input type="text" name="packQty" id="packQty" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Volume Weight</td>
                                    <td class="text1"><input type="text" name="volumeWeight" id="volumeWeight" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Actual Weight</td>
                                    <td class="text1"><input type="text" name="actualWeight" id="actualWeight" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td>Scannable</td>
                                    <td>
                                        <select name="scannable" id="scannable" style="width:240px;height:40px" class="form-control">
                                            <option value="Y">Yes</option>
                                            <option value="N">No</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                <center><a>
                                        <input  type="button" class="btn btn-success" align="center" value="Save" name="Submit" onClick="submitPage()">      
                                        &emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">
                                    </a>
                                </center>
                                </tr>
                            </table>
                            <br>
                            <table class="table table-info mb30 table-hover" id="table" >   
                                <thead>

                                    <tr height="30" style="color: white">
                                        <th>S.No</th>
                                        <th>Category Name</th>
                                        <th>Supplier Name</th>
                                        <th>Sap Code</th>
                                        <th>Multi Sap</th>
                                        <th>Sku Code</th>
                                        <th>Product Name</th>
                                        <th>MRP</th>
                                        <th>Pack Qty</th>
                                        <th>Volume Weight</th>
                                        <th>Actual Weight</th>
                                        <th>Scannable</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>


                                    <% int sno = 0;%>
                                    <c:if test = "${ProductMasters != null}">
                                        <c:forEach items="${ProductMasters}" var="Pro">
                                            <%
                                                sno++;
                                                String className = "text1";
                                                if ((sno % 1) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                            %>

                                            <tr>
                                                <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.categoryName}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.supplierName}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.sapCode}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.multiSap}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.skuCode}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.productName}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.mrp}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.packQty}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.volumeWeight}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.actualWeight}" /></td>
                                                <td class="<%=className%>"  align="left"> 
                                                    <c:if test="${Pro.scannable == 'Y'}">Yes</c:if>
                                                    <c:if test="${Pro.scannable == 'N'}">No</c:if>
                                                    </td>
                                                    <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${Pro.productID}" />', '<c:out value="${Pro.categoryId}" />', '<c:out value="${Pro.supplierId}" />', '<c:out value="${Pro.sapCode}" />', '<c:out value="${Pro.multiSap}" />', '<c:out value="${Pro.skuCode}" />', '<c:out value="${Pro.productName}" />', '<c:out value="${Pro.mrp}" />', '<c:out value="${Pro.packQty}" />', '<c:out value="${Pro.volumeWeight}" />', '<c:out value="${Pro.actualWeight}" />', '<c:out value="${Pro.scannable}" />');" /></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </c:if>
                            </table>


                            <input type="hidden" name="count" id="count" value="<%=sno%>" />

                            <br>
                            <br>
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" selected="selected">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span>Entries Per Page</span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("table", 1);
                            </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>

