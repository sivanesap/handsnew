<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<!--<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>-->
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<style>
    .form-control:focus{border-color: #5cb85c;  box-shadow: none; -webkit-box-shadow: none;} 
    .has-error .form-control:focus{box-shadow: none; -webkit-box-shadow: none;}
</style>

<style>
    .opci{
        background:rgba(101, 237, 221,0.5);
        min-height:100px;
        max-width:180px;
        padding:20px;
        border-radius: 12px;
        font-size: 300%;
        alignment-adjust: 60%;

    }

    /*            table {
                    float:left;
                    background:yellow;
                    margin-left:10px;
                }*/
</style>

<meta http-equiv="ConConsignment Notent-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<script type='text/javascript' src='https://widget.freshworks.com/widgets/60000001575.js' async defer></script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<style>
    #rcorners1 {
        border-radius: 35px;
        background: url(paper.gif);
        background-position: left top;
        background-repeat: repeat;
        padding: 5px; 
        width: 400px;
        height: 150px;
    }
</style>


</head>

<body>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Flipkart Finance Approval" text="Finance Approval"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Flipkart" text="Flipkart"/></a></li>
                <li class=""><spring:message code="hrms.label.Flipkart Finance Approval" text="Flipkart Finance Approval"/></li>
            </ol>
        </div>
    </div>
    <form name="schedule" method="post" >
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <%@include file="/content/common/message.jsp" %>
                    <div class="contentpanel">
                        <div class="row" align="center"style="overflow:auto">

                            <table class="table table-info mb30 table-hover" id="table" >
                                <thead>
                                    <tr height="40">
                                        <th>S.No</th>                
                                        <th>Deposit Code</th>
                                        <th>From Date</th>
                                        <th>To Date</th>                                                                                                         
                                        <th>Deposit Type</th>                                                                                                         
                                        <th>File Name</th>                                                                                                         
                                        <th>Current Hub</th>       
                                        <th id="shows" style="display: none">Finace rejected remarks</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <% int index = 0;
                                   int sno = 1;
                                %>
                                <tbody>
                                    <c:forEach items="${fkFinanceApproval}" var="cnl">

                                        <tr height="30">
                                            <td align="left"><%=sno%>
                                                <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                            </td>
                                            <td align="left" >
                                                <a style="font-size:15px" onclick="showReconcile('<c:out value="${cnl.depositId}"/>', '<c:out value="${cnl.depositCode}"/>');">
                                                    <c:out value="${cnl.depositCode}"/>
                                                </a> 
                                            </td>
                                            <td align="left" ><c:out value="${cnl.fromDate}"/></td>
                                            <td align="left" ><c:out value="${cnl.toDate}"/></td>
                                            <td align="left" >
                                                <c:if test="${cnl.depositType==1}">Bank Deposit</c:if>
                                                <c:if test="${cnl.depositType==2}">Writer's Pickup</c:if>
                                                </td>                            
                                                <td align="left" >
                                                <c:if test="${cnl.fileName!=''&&cnl.fileName!=null}">
                                                    <a onclick="showImage('<c:out value="${cnl.fileName}"/>')"><c:out value="${cnl.fileName}"/></a>                                     
                                                </c:if>
                                            </td>                            
                                            <td align="left" ><c:out value="${cnl.currentHub}"/></td>
                                           
                                            <td align="left" style="display: none" id="show<%=sno%>">
                                                <textarea type="text" name="remark" id="remark<%=sno%>" value="" ></textarea>
                                            </td>
                                            <td align="left" >
                                                <span class="label label-success" style="cursor: pointer;" onclick="approveDeposit('<c:out value="${cnl.depositId}"/>');">
                                                    Approve
                                                </span>
                                                    <br>
                                                    <br>
                                                <span class="label label-danger" style="cursor: pointer;" onclick="rejectDeposit('<c:out value="${cnl.depositId}"/>',<%=sno%>);">
                                                    Reject
                                                </span>
                                            </td>
                                          
                                        </tr>
                                        <%index++;%>
                                        <%sno++;%>

                                    </c:forEach>
                                </tbody>
                            </table>
                            <script>
                                function approveDeposit(depositId) {
                                    document.schedule.action = "/throttle/fkFinanceApproval.do?&param=approve&depositId=" + depositId;
                                    document.schedule.submit();
                                }
                                function rejectDeposit(depositId,sno) {
                                     var remarks = document.getElementById("remark"+sno).value;
                                     
                                    if(remarks === ""){
                                        $("#show"+sno).show();
                                        $("#shows").show();
                                   
                                        return alert("Please Enter remarks")
                                    }
                                    document.schedule.action = "/throttle/fkFinanceApproval.do?&param=reject&depositId=" + depositId+'&remarks='+remarks;
                                    document.schedule.submit();
                                }
                                function showImage(name) {
                                    window.open("/throttle/uploadFiles/pod/" + name, "POD Image", "width=900, height=800");
                                }
                                function showReconcile(depositId, depositCode) {
                                    window.open('/throttle/showReconcile.do?&param=show&depositId=' + depositId + '&depositCode=' + depositCode, 'PopupPage', 'height = 600, width = 1000, scrollbars = yes, resizable = yes');
                                }
                            </script>

                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>


<!--<script async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&callback=initMap"></script>-->
<%@ include file="../common/NewDesign/settings.jsp" %>