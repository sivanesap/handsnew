<%-- 
    Document   : rpStockInward
    Created on : 20 Sep, 2021, 4:40:56 PM
    Author     : alan
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>
<link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>

<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });


    function isNumberKey(e) {
        var key = (e.which) ? e.which : e.keyCode;
        if ((key != 46 && key > 31) && (key < 48 || key > 57)) {
            return false;
        }
        return true;
    }

    function add() {
        document.connect.action = " /throttle/rpWorkOrder.do?param=save";
        document.connect.submit();
    }

</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Stock Inward</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Repose</a></li>
                <li class="">Stock Inward</li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">

                    <form name="uploadSheet" method="post" enctype="multipart/form-data">
                        <div id="sheet1" style="display:none">
                            <table class="table table-info mb30 table-hover">
                                <tr>
                                    <tr>
                                        <td>Storage Location</td>
                                        <td><select id="storageId1" name="storageId1" style="width:250px;height:40px"class="form-control">
                                                <option value="">---Select---</option>
                                                <c:forEach items="${storageDetails}" var="bin">
                                                    <option value="<c:out value="${bin.storageId}"/>"><c:out value="${bin.storageName}"/></option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                        <td>Bin</td>
                                        <td><select id="binId1" name="binId1" style="width:250px;height:40px"class="form-control">
                                                <option value="">---Select---</option>
                                                <c:forEach items="${binDetails}" var="bin">
                                                    <option value="<c:out value="${bin.binId}"/>"><c:out value="${bin.binName}"/></option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>
                                </tr>
                                <tr>
                                    <td>Upload Excel</td>
                                    <td><input type="file" name="file1" id="file1"></td>
                                </tr>
                            </table>
                            <center>
                                <input type="button" class="btn btn-success" name="excelSheet" id="excelSheet" onclick="uploadPage()" value="Upload">&emsp;
                                <input type="button" class="btn btn-success" name="scanNo" id="scanNo" value="Scan" onclick="hideUploadPage()">
                            </center>
                        </div>
                    </form>
                    <form name="connect"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <br>
                        <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                            <input type="hidden" name="indentId" id="indentId" value=""  />
                            <tr>
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="8" >Stock Inward</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="text1"><font color="red">*</font>Product Name</td>
                                    <td class="text1" >
                                        <select name="itemId" id="itemId" class="form-control" style="width:240px;height:40px;" >
                                            <option value="">-----Select One-----</option>
                                            <c:forEach items="${productList}" var="pl">
                                                <option value="<c:out value="${pl.itemId}"/>"><c:out value="${pl.itemName}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                <script>
                                    $('#itemId').select2({placeholder: 'Fill Product Name'});
                                    $('#itemId').val('');
                                </script>
                                <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Input Quantity</td>
                                <td class="text1"><input type="text" name="qty" id="qty" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>

                                <table class="table table-info mb30 table-hover" id="newTable">
                                    <div align="center" style="height:20px;" id="StatusMsg">&nbsp;&nbsp;
                                    </div>
                                    <tr>
                                        <td>Storage Location</td>
                                        <td><select id="storageId" name="storageId" style="width:250px;height:40px"class="form-control">
                                                <option value="">---Select---</option>
                                                <c:forEach items="${storageDetails}" var="bin">
                                                    <option value="<c:out value="${bin.storageId}"/>"><c:out value="${bin.storageName}"/></option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                        <td>Bin</td>
                                        <td><select id="binId" name="binId" style="width:250px;height:40px"class="form-control">
                                                <option value="">---Select---</option>
                                                <c:forEach items="${binDetails}" var="bin">
                                                    <option value="<c:out value="${bin.binId}"/>"><c:out value="${bin.binName}"/></option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Product Condition
                                        </td>
                                        <td><select id="proCon" name="proCon" style="width:250px;height:40px">
                                                <option value="1">Good</option>
                                                <option value="2">Damaged</option>
                                                <option value="3">Excess</option>
                                            </select></td>
                                        <td>
                                            UOM
                                        </td>
                                        <td><select id="Uom" name="Uom" style="width:250px;height:40px">
                                                <option value="1">Pack</option>
                                                <option value="2" selected>Piece</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            Scan Serial No : 
                                        </td>
                                        <td>
                                            <input type="hidden" name="grnId" id="grnId" value="<c:out value="${grnId}"/>"/>
                                            <input type="hidden" name="gtnId" id="gtnId" value="<c:out value="${gtnId}"/>"/>

                                            <input type="text" id="serialNo" name="serialNo" value="" class="form-control" style="width:250px" onkeypress="return RestrictSpace()" onchange="checkExists(this.value);"/> 
                                            <br>
                                            <b>OR</b>&emsp;&emsp;
                                            <input type="button" id="upload" name="upload" onclick="showUploadPage()" class="btn btn-darkblue" value="Upload Excel">
                                        </td>
                                    </tr>
                                </table>                        

                                <br> 
                                <div id="tableContent"></div>
                                <script>
                                    var serial = [];
                                    var ipqt = [];
                                    var uomarr = [];
                                    var sno = [];
                                    var procon = [];
                                    var arr = [];
                                    var binIds = [];
                                    var storageIds = [];
                                    var i = 0;


                                    function go1() {
                                        if (httpRequest.readyState == 4) {
                                            if (httpRequest.status == 200) {
                                                var response = httpRequest.responseText;
                                                if (response != "") {
                                                    alert(response);
                                                    resetTheDetails();

                                                } else
                                                {
                                                    saveTempSerialNos();
                                                }
                                            }
                                        }
                                    }
                                    function checkExists(serialNo) {
                                        var url = "";
                                        if (serialNo != "") {
                                            url = "./checkRpExistSerialNo.do?serialNo=" + serialNo;

                                            if (window.ActiveXObject)
                                            {
                                                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                            } else if (window.XMLHttpRequest)
                                            {
                                                httpRequest = new XMLHttpRequest();
                                            }
                                            httpRequest.open("POST", url, true);
                                            httpRequest.onreadystatechange = function() {
                                                go1();
                                            };
                                            httpRequest.send(null);
                                        }

                                    }


                                    function saveTempSerialNos() {
                                        if (serial.indexOf(serialNo.value) != -1) {
                                            alert("Already Scanned");
                                            serialNo.value="";
                                        }else if (document.getElementById("binId").value=="") {
                                            alert("Choose Bin");
                                        }else if (document.getElementById("itemId").value=="") {
                                            alert("Choose Product Name");
                                        }else if (document.getElementById("qty").value=="") {
                                            alert("Enter Quantity");
                                        } else {
                                            var mk = document.getElementById("qty").value;
                                            if (i <  mk) {
                                                serial[i] = serialNo.value;
                                                ipqt[i] = 1;
                                                binIds[i] = binId.value;
                                                uomarr[i] = Uom.value;
                                                procon[i] = proCon.value;
                                                storageIds[i] = storageId.value;
                                                i++;
                                                table();
                                            }
                                            else {
                                                alert("Quantity Exceeded");
                                            serialNo.value="";
                                            }
                                        }
                                    }
                                function table(){
                                   if(serial.length>0){
                                   for(x=1;x<=serial.length;x++){
                                   sno.push(x);
                                   }}
                                   const tmpl = (sno,serial,uomarr,procon,ipqt,binIds,storageIds) => `
                                   <table class="table table-info mb30 table-bordered" id="table"><thead>
                                   <tr><th>S.No<th>Serial No<th>UOM<th>Product Condition<th>Qty<th>Delete<tbody>
                                   ${sno.map( (cell, index) => `
                                   <tr><td><input type="hidden" id="sNo" name="sNo" value="${cell}"/>${cell}<td><input type="hidden" id="serialNos"+sno name="serialNos" value="${serial[index]}" />${serial[index]}<td><input type="hidden" id="uom" name="uom" value="${uomarr[index]}" />${uomarr[index]}<td><input type="hidden" id="proCond" name="proCond" value="${procon[index]}" />${procon[index]}<td><input type="hidden" id="ipQty" name="ipQty" value="${ipqt[index]}"/>${ipqt[index]}<td><input type="hidden" id="bin" name="bins" value="${binIds[index]}"/><input type="hidden" id="storages" name="storages" value="${storageIds[index]}"/><input type="button" class="btn btn-success" name="${sno[index]}" value="Delete" onclick="deleteRow(this.name,sno, serial, uomarr, ipqt, procon,binIds,storageIds);" style="width:100px;height:35px;"></tr>
                                   `).join('')}
                                   </table><p align = "center" style="font-size:16px"><b>Scanned Qty:</b><input type="text" id="qty" name="qty" value="${serial.length}" class="form-control" style="width:100px;height=30px" readonly/><br><br></p>`;
                                    tableContent.innerHTML=tmpl(sno,serial,uomarr,procon,ipqt,binIds,storageIds);
                                    sno.splice(0,sno.length);
                                    document.getElementById("serialNo").value='';
                               }
                                    function deleteRow(name, sno, serial, uomarr, ipqt, procon, binIds, storageIds) {

                                        var x = name - 1;
                                        serial.splice(x, 1);
                                        uomarr.splice(x, 1);
                                        ipqt.splice(x, 1);
                                        procon.splice(x, 1);
                                        binIds.splice(x, 1);
                                        sno.splice(0, sno.length);
                                        name = 0;
                                        i--;
                                        table();


                                    }
                                    function setVal(value) {
                                        var x = value.split('-');
                                        document.getElementById("itemId").value = x[0];
                                        document.getElementById("asnId").value = x[1];

                                    }
                                    function showTables(i, j, k) {
                                        document.getElementById('proDet').value = j + '-' + k;
                                        document.getElementById("itemId").value = j;
                                        document.getElementById("asnId").value = k;
                                        if (j != "") {
                                            $('#table1').show();
                                        }
                                        if (i != 0) {
                                            $('#table2').show();
                                        }
                                    }
                                    function deleteSerial(x) {
                                        var gtnId = document.getElementById("gtnId").value;
                                        var grnId = document.getElementById("grnId").value;
                                        var itemId = document.getElementById("itemId1").value;
                                        document.gtn.action = '/throttle/editSerialDetail.do?&gtnId=' + gtnId + '&itemId=' + itemId + '&grnId=' + grnId + '&tempId=' + x;
                                        document.gtn.submit();
                                        this.disabled = true;
                                        this.value = 'Searching';
                                    }
                                    
                                function RestrictSpace() {
                                    if (event.keyCode == 32) {
                                    return false;
                                    }
                                }
                                function savePage(){
                                   if(parseInt(i)==parseInt(document.getElementById("qty").value)){
                                       document.connect.action='/throttle/rpStockInward.do?param=save';
                                       document.connect.submit();
                                   }else{
                                       alert("Scan All the Quantity");
                                   }
                                }
                                
                                function uploadPage(){
                                    var storageId = document.getElementById("storageId1").value;
                                    var binId = document.getElementById("binId1").value;
                                    var productId = document.getElementById("itemId").value;
                                    var qty = document.getElementById("qty").value;
                                    if(productId==""){
                                        alert("Please Select Product Name");
                                    }else if(qty==""){
                                        alert("Please Enter Input Qty");
                                    }else{
                                    document.uploadSheet.action= "/throttle/rpStockInward.do?param=upload&productId="+productId+"&qty="+qty+"&storageId1="+storageId+"&binId1="+binId;
                                    document.uploadSheet.submit();
                                }
                                }
                                function showUploadPage(){
                                    $("#sheet1").show();
                                    $("#file1").focus();
                                    $("#newTable").hide();
                                    $("#save").hide();
                                }
                                function hideUploadPage(){
                                    $("#sheet1").hide();
                                    $("#serialNo").focus();
                                    $("#newTable").show();
                                    $("#save").show();
                                }
                                </script>
                                        <br>
                                    <center>
                                    <input type="button" class="btn btn-success" value="Submit" id="save" name="save" onclick="savePage();">
                                    </center>
                                <br>
                                <br>
                                <script language="javascript" type="text/javascript">
                                    setFilterGrid("table");
                                </script>
                                    </form>
                                    </body>
                                 </div>
                              </div>
                             </div>
                                <%@ include file="/content/common/NewDesign/settings.jsp" %>