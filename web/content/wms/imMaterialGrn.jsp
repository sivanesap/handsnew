
<%-- 
    Document   : imMaterialGrn
    Created on : 20 Oct, 2021, 4:37:02 PM
    Author     : Roger
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">


    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    if (day.toString().length == 1) {
        day = "0" + day;
    }
    if (month.toString().length == 1) {
        month = "0" + month;
    }
    if (year.toString().length == 1) {
        year = "0" + year;
    }
    finalDate = day + "-" + month + "-" + year;
    $(document).ready(function() {
        $('#grnDate').val(finalDate);
//        $('#inDate').datepicker('setDate', 'today');
//        $("#datepicker").datepicker({
//            showOn: "button",
//            buttonImage: "calendar.gif",
//            buttonImageOnly: true
//
//        });
    });





    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<style>

    .tdn{
        background-color: #f2efe6;
        border: none;
        border-radius: 2%;
        padding: 2px;
        width: 130px

    }
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Material GRN </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">InstaMart</a></li>
            <li class="active">Material GRN</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="">
                <form name="saveGtnDetails" method="post">
                    <table class="table table-info mb30 table-hover" id="serial">
                        <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                        </div>
                        <tr>
                            <td align="center"><text style="color:red">*</text>GRN Date</td>
                            <td height="30"><input readonly name="grnDate" id="grnDate" type="text"  class="form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="" autocomplete="off"></td>
                            <td align="center"><text style="color:red">*</text>GRN Time</td>
                            <td height="30"><input name="grnTime" id="grnTime" type="time"  class="form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="" autocomplete="off"></td>

                        </tr>
                        <tr>
                            <td align="center"><text style="color:red">*</text>Invoice No</td>
                            <td height="30"><input name="invoiceNo" id="invoiceNo" type="text"  class="form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="" autocomplete="off"></td>
                            <td align="center"><text style="color:red">*</text>Invoice Date</td>
                            <td height="30"><input name="invoiceDate" id="invoiceDate" type="text"  class="datepicker , form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="" autocomplete="off"></td>

                        </tr>
                        <tr>
                            <td align="center">Total Quantity</td>
                            <td><input type="text" readonly name="totQty" id="totQty" class="form-control" value="0" style="width:240px;height:40px"></td>
                            <td align="center">
                                Remarks
                            </td>
                            <td>
                                <textarea type="text" id="remarks" name="remarks" value="" class="form-control" style="width:240px; height:60px"/></textarea> 
                            </td>
                        </tr>



                        <tr>
                            <td align="center"><text style="color:red">*</text>
                                Po No 
                            </td>
                            <td>
                                <!--<select id="indent" name="indent"  class="form-control" style="width:240px;height:40px" onclick="testOn(this.value);">-->
                                <select id="indent" name="indent"  class="form-control" style="width:240px;height:40px" onchange="onDoit(this.value);">
                                    <option value="">-------Select Indent------</option>
                                    <c:forEach items="${imMaterialPo}" var="cm">
                                        <option value="<c:out value="${cm.poId}"/>"><c:out value="${cm.poNo}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>

                    </table>                 
                    <table class="table table-info imMaterialGrn.jspmb30 table-hover sortable" id="addIndent" align="center" width="90%">
                        <thead >
                            <tr>
                                <th>S.No</th>
                                <th>PO No</th>
                                <th>Material Name</th>  
                                <th>PO Status</th>
                                <th>PO Qty</th>
                                <th>PO Pending</th>
                                <th>Received Qty</th>
                                <th>Good Qty</th>
                                <th>Balance Qty</th>
                                <th>Shortage Qty</th>
                                <th>Damage Qty</th>
                                <th>Delete</th>

                            </tr>
                        </thead>
                        <tbody id = "body">


                        </tbody>

                    </table>

                    <script>
                        var rowCount = 0;
                        var sno = 0;
                        var httpRequest;
                        var httpReq;
                        var rowIndex = 0;
                        var httpRequest;


                        function go1() {
                            if (httpRequest.readyState == 4) {
                                if (httpRequest.status == 200) {
                                    var response = httpRequest.responseText;
                                    if (response != "") {
                                        alert(response);
                                        resetTheDetails();

                                    } else
                                    {

                                    }
                                }
                            }
                        }
                        function checkInvoice(invoiceNo) {
                            var url = "";
                            if (invoiceNo != "") {
                                url = "./checkInvoiceExist.do?invoiceNo=" + invoiceNo;

                                if (window.ActiveXObject)
                                {
                                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                } else if (window.XMLHttpRequest)
                                {
                                    httpRequest = new XMLHttpRequest();
                                }
                                httpRequest.open("POST", url, true);
                                httpRequest.onreadystatechange = function() {
                                    go1();
                                };
                                httpRequest.send(null);
                            }

                        }

                        function addRow()
                        {


                            var tab = document.getElementById("addIndent");
                            var iRowCount = tab.getElementsByTagName('tr').length;

                            rowCount = iRowCount;
                            var newrow = tab.insertRow(rowCount);
                            //        alert(iRowCount);
                            sno = rowCount;
                            rowIndex = rowCount;
                            //        alert("rowIndex:"+rowIndex);
                            var cell = newrow.insertCell(0);
                            var cell0 = "<td class='text1' height='25' > <input type='hidden'  name='Sno' value='' > " + sno + "</td>";
                            cell.innerHTML = cell0;

                            cell = newrow.insertCell(1);
                            var cell2 = "<td class='text1' height='30'><select class='form-control' name='poNo' style='width:200px;height:40px' onchange='checkExistPO(this.value," + sno + ")' id='poNo" + sno + "'  value=''><option value=''>-----Select PO No-----</option><c:if test='${imMaterialPO!=null}'><c:forEach items='${imMaterialPO}' var='po'><option value='<c:out value='${po.poId}'/>'><c:out value='${po.poNo}'/></option></c:forEach></c:if></select></td>"
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(2);
                            var cell2 = "<td class='text1' height='25'><select class='form-control' onchange='checkExist(this.value," + sno + ");setPoQty(" + sno + ")' name='materialId' id='materialId" + sno + "' style='width:200px;height:40px'><option value='0'>----Select Material----</option><c:forEach items='${materialMaster}' var='pro'><option value='<c:out value='${pro.materialId}'/>'><c:out value='${pro.material}'/></option></c:forEach></select><input type='hidden' name='materialIdTemp' id='materialIdTemp" + sno + "'><input type='hidden' name='materialName' id='materialName" + sno + "'></td>"
                            cell.innerHTML = cell2;


                            cell = newrow.insertCell(3);
                            var cell2 = "<td class='text1' height='30'><input type='text' class='form-control' name='poQty' maxlength='8' style='width:130px;height:40px' id='poQty" + sno + "'  value='0' readonly></td>"
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(4);
                            cell2 = "<div id='test1'><td class='text1' height='25'><select class='form-control' name='poStatus' disabled style='width:130px;height:40px' id='poStatus" + sno + "' ><option value='1'>Open</option value='2'><option>Close</option> </td></div>";
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(5);
                            cell2 = "<div id='test1'><td class='text1' height='25'><input type='text'class='form-control' name='receivedQty' maxlength='8' style='width:130px;height:40px' id='receivedQty" + sno + "' onchange='checkqty();'  value='0'> </td></div>";
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(6);
                            cell2 = "<div id='test1'><td class='text1' height='25'><input type='text'class='form-control' name='good' maxlength='8' style='width:130px;height:40px' id='good" + sno + "' onchange='goodQty(i);'  value='0'> </td></div>";
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(7);
                            cell2 = "<div id='test1'><td class='text1' height='25'><input type='text'class='form-control' name='shortage' maxlength='8' style='width:130px;height:40px' id='shortage" + sno + "' onchange='shortage(i);'  value='0'> </td></div>";
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(8);
                            var cell2 = "<td class='text1' height='25' ><input type='button' class='btn btn-info'   name='delete'  id='delete'   value='Delete Row' onclick='deleteRow(this," + sno + ");' ></td>";
                            cell.innerHTML = cell2;


                            rowIndex++;
                            rowCount++;
                            sno++;

                        }

                        function deleteRow(src) {
                            sno--;
                            var oRow = src.parentNode.parentNode;
                            var dRow = document.getElementById('addIndent');
                            dRow.deleteRow(oRow.rowIndex);
                            document.getElementById("selectedRowCount").value--;
                        }
                        function submitPage(value) {

                            if (document.getElementById("grnDate").value == "") {
                                alert("Enter GRN Date");
                                document.getElementById("grnDate").focus();
                            } else if (document.getElementById("grnTime").value == "") {
                                alert("Enter GRN Time");
                                document.getElementById("grnTime").focus();
                            } else if (document.getElementById("invoiceNo").value == "") {
                                alert("Enter Invoice Number");
                                document.getElementById("invoiceNo").focus();
                            } else if (document.getElementById("invoiceDate").value == "") {
                                alert("Enter Invoice Date");
                                document.getElementById("invoiceDate").focus();
                            } else {
                                document.saveGtnDetails.action = '/throttle/imMaterialGrn.do?param=save';
                                document.saveGtnDetails.submit();
                            }
                        }
                        function checkqty() {
                            var qty = document.getElementsByName("receivedQty");
                            var totQty = 0;
                            for (var i = 0; i < qty.length; i++) {
                                if (qty[i].value != "") {
                                    totQty = parseInt(qty[i].value) + totQty;
                                }
                            }
                            document.getElementById("totQty").value = totQty;
                        }
                        function goodQty(value) {
                            var qty = document.getElementsByName("receivedQty");

                        }



                        function setPoQty(sno) {
                            var mat = document.getElementById("materialId" + sno).value;

                        }


                        function checkExist(value, sno) {
//                            var arr = value.split("~");
//                            var materialId = arr[0];
//                            $.ajax({
//                                url: "/throttle/getImPOList.do",
//                                dataType: 'json',
//                                data: {
//                                    materialId: materialId
//                                },
//                                success: function(data) {
//                                    if (data != '') {
//                                        $('#poNo' + sno).empty();
//                                        $('#poNo' + sno).append(
//                                                $('<option></option>').val('').html('----select----'));
//                                        $.each(data, function(i, data) {
//                                            $('#poNo' + sno).append(
//                                                    $('<option style="width:90px"></option>').attr("value", (data.poId + '-' + data.poQty)).text(data.poNo)
//                                                    );
//
//                                        })
//                                    }
//                                },
//                            })
                        }
                        function checkExistPO(value, sno) {
                            var poId = value;
                            $.ajax({
                                url: "/throttle/getImPOMaterialList.do",
                                dataType: 'json',
                                data: {
                                    poId: poId
                                },
                                success: function(data) {
                                    if (data != '') {
                                        $('#materialId' + sno).empty();
                                        $('#materialId' + sno).append(
                                                $('<option></option>').val('').html('----select----'));
                                        $.each(data, function(i, data) {
                                            $('#materialId' + sno).append(
                                                    $('<option style="width:90px"></option>').attr("value", (data.materialId + '-' + data.poQty)).text(data.materialName)
                                                    );
                                        })
                                    }
                                },
                            })
                        }

                        function deSelect(value) {

                            var sno = document.getElementById("sno" + value).style.display = "none";
                            var poNo = document.getElementById("poNo" + value).style.display = "none";
                            var materialId = document.getElementById("materialId" + value).style.display = "none";
                            var poStatus = document.getElementById("poStatus" + value).style.display = "none";
                            var poQty = document.getElementById("poQty" + value).style.display = "none";
                            var receivedQty = document.getElementById("receivedQty" + value).style.display = "none";
                            var good = document.getElementById("good" + value).style.display = "none";
                            var balance = document.getElementById("balance" + value).style.display = "none";
                            var shortage = document.getElementById("shortage" + value).style.display = "none";
                            var damage = document.getElementById("damage" + value).style.display = "none";
                            var del = document.getElementById("del" + value).style.display = "none";
                            var poQtyx = document.getElementById("poQtyx" + value).style.display = "none";
                            var detailsid = document.getElementById("detailsid" + value).style.display = "none";

                            var sno = document.getElementById("sno" + value).name = "none";
                            var poNo = document.getElementById("poNo" + value).name = "none";
                            var materialId = document.getElementById("materialId" + value).name = "none";
                            var poStatus = document.getElementById("poStatus" + value).name = "none";
                            var poQty = document.getElementById("poQty" + value).name = "none";
                            var receivedQty = document.getElementById("receivedQty" + value).name = "none";
                            var good = document.getElementById("good" + value).name = "none";
                            var balance = document.getElementById("balance" + value).name = "none";
                            var shortage = document.getElementById("shortage" + value).name = "none";
                            var damage = document.getElementById("damage" + value).name = "none";
                            var del = document.getElementById("del" + value).name = "none";
                            var poQtyx = document.getElementById("poQtyx" + value).name = "none";
                            var detailsid = document.getElementById("detailsid" + value).name = "none";

//
//                            var qty = document.getElementById("qty" + value).id = "none";
//                            var poQty = document.getElementById("poQty" + value).id = "none";
//                            var pending = document.getElementById("pending" + value).id = "none";
//                            var materialCode = document.getElementById("materialCode" + value).id = "none";
//                            var material = document.getElementById("material" + value).id = "none";
//                            var materialDescription = document.getElementById("materialDescription" + value).id = "none";
//                            var materialId = document.getElementById("materialId" + value).id = "none";
//                            var del = document.getElementById("del" + value).id = "none";
//                            var sno = document.getElementById("sno" + value).id = "none";
//                            var idd = document.getElementById("idd" + value).id = "none";
//                            var stock = document.getElementById("stock" + value).id = "none";
//                            var uom = document.getElementById("uom" + value).id = "none";

                            document.getElementById("tr" + value).style.display = "none";


                        }


                        function setValue(value) {

                            var receivedQty = document.getElementById("receivedQty" + value).value;
                            var poQty = document.getElementById("poQty" + value).value;
                            var poQtyx = document.getElementById("poQtyx" + value).value;


//                            alert(pendingX + " " + poQty)
                            if (parseInt(poQtyx) < parseInt(receivedQty)) {
//                                document.getElementById("pending" + value).value = "0"
                                document.getElementById("receivedQty" + value).value = "";
                                document.getElementById("good" + value).value = "";
                                document.getElementById("balance" + value).value = "";


                            }
                            else {
                                document.getElementById("good" + value).value = parseInt(receivedQty)
                                document.getElementById("balance" + value).value = parseInt(poQtyx) - parseInt(receivedQty)



                            }



                            var qty = document.getElementsByName("good");
                            var totQty = 0;
                            for (var i = 0; i < qty.length; i++) {
                                if (qty[i].value != "") {
                                    totQty = parseInt(qty[i].value) + totQty;
                                }
                            }
                            document.getElementById("totQty").value = totQty;


                        }
                        function setValue1(value) {

//                            var receivedQty = document.getElementById("receivedQty" + value).value;
                            var shortage = document.getElementById("shortage" + value).value;
                            var good = document.getElementById("good" + value).value;


//                            alert(pendingX + " " + poQty)
                            if (parseInt(good) < parseInt(shortage)) {
//                                document.getElementById("pending" + value).value = "0"
                                document.getElementById("shortage" + value).value = "0";
//                                document.getElementById("good" + value).value = "";


                            }
                            else {
                                document.getElementById("good" + value).value = parseInt(good) - parseInt(shortage)


                            }



                            var qty = document.getElementsByName("good");
                            var totQty = 0;
                            for (var i = 0; i < qty.length; i++) {
                                if (qty[i].value != "") {
                                    totQty = parseInt(qty[i].value) + totQty;
                                }
                            }
                            document.getElementById("totQty").value = totQty;


                        }

                        function setValue2(value) {

//                            var receivedQty = document.getElementById("receivedQty" + value).value;
                            var damage = document.getElementById("damage" + value).value;
                            var good = document.getElementById("good" + value).value;


//                            alert(pendingX + " " + poQty)
                            if (parseInt(good) < parseInt(damage)) {
//                                document.getElementById("pending" + value).value = "0"
                                document.getElementById("damage" + value).value = "0";


                            }
                            else {
                                document.getElementById("good" + value).value = parseInt(good) - parseInt(damage)


                            }



                            var qty = document.getElementsByName("good");
                            var totQty = 0;
                            for (var i = 0; i < qty.length; i++) {
                                if (qty[i].value != "") {
                                    totQty = parseInt(qty[i].value) + totQty;
                                }
                            }
                            document.getElementById("totQty").value = totQty;


                        }


                        var listOfId = [];





                        function onDoit(value) {

                            if (listOfId.length > 0 && listOfId.indexOf(value) > -1) {
                                alert("Already Selected");
                            } else {

                                listOfId.push(value)


                                // var indentId = value
                                // var maintablebody = document.getElementById("body");

                                // $.ajax({
                                //     url: "/throttle/imInSetGet.do",
                                //     dataType: "json",
                                //     data: {
                                //         indentId: indentId,
                                //     },
                                //     success: function (data) {
                                //         $.each(data, function (i, data) {

                                var poId = value
                                var maintablebody = document.getElementById("body");
                                //                           alert("testing the work flow" + value);

                                $.ajax({
                                    url: "/throttle/imGrnSetGet.do",
                                    dataType: "json",
                                    data: {
                                        poId: poId,
                                    },
                                    success: function(data) {
                                        $.each(data, function(i, data) {

                                            //                                        alert(data)
                                            var tab = document.getElementById("addIndent");
                                            var iRowCount = tab.getElementsByTagName('tr').length;

                                            rowCount = iRowCount;
                                            var newrow = tab.insertRow(rowCount);
                                            //        alert(iRowCount);
                                            sno = rowCount;
                                            rowIndex = rowCount;
                                            //        alert("rowIndex:"+rowIndex);
                                            var cell = newrow.insertCell(0);
                                            var cell0 = "<td> <input type='hidden'  name='Sno' value='' > " + sno + "</td>";
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(1);
                                            var cell2 = "<td><select readonly id='poNo" + sno + "' name = 'poNo'  class = 'tdn'><option value='" + data.poId + "'>" + data.poNo + "</option></select></td>"
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(2);
                                            var cell2 = "<td><select readonly id='materialId" + sno + "' name = 'materialId'  class = 'tdn'><option value='" + data.materialId + "'>" + data.material + "</option></select></td>"
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(3);
//                      cell2 = "<div id='test1'><td class='text1' height='25'></td></div>";

                                            var cell2 = "<td><select name='poStatus' readyonly selected='" + data.status + "' id='poStatus" + sno + "' ><option value='0'>Open</option value='0'><option value='1'>Pending</option> </td>"
//                    var cell2 = "<td><input type='text'value = '" + data.status + "' readonly id='poStatus" + sno + "' name = 'poStatus' class = 'tdn' ></td>"
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(4);
                                            var cell2 = "<td><input type='text' value = '" + data.poQty + "' readonly id='poQty" + sno + "' name = 'poQty' class = 'tdn'></td>"
                                            cell.innerHTML = cell2;
                                            
                                            cell = newrow.insertCell(5);
                                            var cell2 = "<td><input type='text' value = '" + data.pendingQty1 + "' readonly id='poQty1" + sno + "' name = 'poQty1' class = 'tdn'></td>"
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(6);
                                            cell2 = "<td><input type='text' value = '" + "" + "' style = 'background:white; border:2px solid #c1cad9; border-radius:5px;' id='receivedQty" + sno + "' name = 'receivedQty' class = 'tdn' onchange ='setValue(" + sno + ");'></td>";
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(7);
                                            cell2 = "<td><input type='text' value = '" + "" + "' readonly  id='good" + sno + "' name = 'good' class = 'tdn' ></td>";
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(8);
                                            cell2 = "<td><input type='text' value = '" + "0" + "' readonly id='balance" + sno + "' name = 'balance' class = 'tdn' /></td>";
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(9);
                                            cell2 = "<td><input type='text' value = '" + "0" + "' style = 'background:white; border:2px solid #c1cad9; border-radius:5px;' id='shortage" + sno + "' name = 'shortage' class = 'tdn' onchange ='setValue1(" + sno + ");'></td>";
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(10);
                                            var cell2 = "<td><input type='text' value = '" + "0" + "' style = 'background:white; border:2px solid #c1cad9; border-radius:5px;' id='damage" + sno + "' name = 'damage' class = 'tdn' onchange ='setValue2(" + sno + ");'></td>";
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(11);
                                            var cell2 = "<td><input type='button' class='btn btn-info' name='delete'  id='delete'   value='Delete Row' onclick='deleteRow(this," + sno + ");' ></td>";
                                            cell.innerHTML = cell2;


                                            cell = newrow.insertCell(12);
                                            var cell2 = "<td><input type='hidden' value = '" + data.pendingQty1 + "' readonly id='poQtyx" + sno + "' name = 'poQtyx' class = 'tdn'></td>";
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(13);
                                            var cell2 = "<td><input type='hidden' value = '" + data.id + "' readonly id='detailsid" + sno + "' name = 'detailsid' class = 'tdn'></td>";
                                            cell.innerHTML = cell2;


                                            rowIndex++;
                                            rowCount++;
                                            sno++;


                                        })

                                    }
                                }
                                )
                            }
                        }

                        function deleteRow(src) {
                            sno--;
                            var oRow = src.parentNode.parentNode;
                            var dRow = document.getElementById('addIndent');
                            dRow.deleteRow(oRow.rowIndex);
                            document.getElementById("selectedRowCount").value--;
                        }







                        </script>

                        <table>
                            <center>
                                <!--<input value="Add Row" class="btn btn-success" id="countRow" type="button" onClick="addRow();" >-->
                                &emsp;<input type="button" value="Save" id="insert" class="btn btn-success" onClick="submitPage(this.value);">
                                <!--&emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">-->
                            </center>

                        </table>
                    </form>
                </body>
            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>

    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>
