<%-- 
    Document   : viewFkReconcile
    Created on : 27 May, 2021, 11:34:17 AM
    Author     : Roger
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script>
        function submitPage() {
            document.reconcile.action = "viewCashDeposit.do?&param=save&statusId=14";
            document.reconcile.submit();
        }

        function showshipment(reconcileId, reconcileCode) {
            document.reconcile.action = '/throttle/showShipment.do?&param=show&reconcileId=' + reconcileId + '&reconcileCode=' + reconcileCode;
            document.reconcile.submit()
        }

        function setValues(sno, depositAmount, reconcileId) {
            var count = parseInt(document.getElementById("count").value);
            for (i = 1; i <= count; i++) {
                if (i != sno) {
                    document.getElementById("edit" + i).checked = false;
                } else {
                    document.getElementById("edit" + i).checked = true;
                }
            }
            document.getElementById("depositAmount").value = depositAmount;
            document.getElementById("reconcileId").value = reconcileId;
            if (depositAmount == 1) {
                document.getElementById("depositAmount").checked = true;
            } else {
                document.getElementById("depositAmount").checked = false;
            }
            if (reconcileId == 1) {
                document.getElementById("reconcileId").checked = true;
            } else {
                document.getElementById("reconcileId").checked = false;
            }
        }
    </script>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.RunSheet" text="Flipkart Cash Deposit"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Flipkart" text="Flipkart"/></a></li>
                <li class=""><spring:message code="hrms.label.Reconcile" text="Flipkart Cash Deposit"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">
                    <form name="reconcile" method="post">
                        <%@include file="/content/common/message.jsp" %>

                        <table class="table table-info mb30 table-bordered" id="table"  style="width:98%;" >
                            <thead>
                                <tr >
                                    <th >Sno</th>                                        
                                    <th >Reconcile Code</th>                                        
                                    <th >Runsheet Id</th>                                        
                                    <th >DE Name</th>
                                    <th >Total Hub Amount</th>
                                    <th >Total DE COD</th>
                                    <th >Total Prexo Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int sno = 0;%>
                                <c:if test = "${podDetails != null}">
                                    <c:forEach items="${podDetails}" var="re">
                                        <%
                                             sno++;
                                             String className = "text1";
                                             if ((sno % 1) == 0) {
                                                 className = "text1";
                                             } else {
                                                 className = "text2";
                                             }
                                        %>

                                        <tr height="30">
                                            <td><%= sno%> </td>
                                            <td>
                                                <a style="font-size:15px" onclick="showshipment('<c:out value="${re.reconcileId}"/>', '<c:out value="${re.reconcileCode}"/>');">
                                                    <c:out value="${re.reconcileCode}"/>
                                                </a>
                                            </td>
                                            <td><c:out value="${re.runsheetId}"/></td>                                      
                                            <td><c:out value="${re.empName}"/></td>                                                    
                                            <td><c:out value="${re.totalCod}"/></td>                                      
                                            <td><c:out value="${re.totalDeCod}"/></td>                                      
                                            <td><c:out value="${re.totalPrexoAmount}"/></td>  
                                        </tr>
                                    </c:forEach>
                                </tbody>
                                <input type="hidden" name="count" id="count" value="<%= sno%>" />
                            </c:if>
                        </table>

                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>