<%-- 
    Document   : connectBillMappingView
    Created on : 9 Sep, 2021, 2:13:28 PM
    Author     : Roger
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
    function submitPage1(billId, billNo) {
        document.connect.action = '/throttle/connectBillMapping.do?param=upload&billId=' + billId + '&billNo=' + billNo;
        document.connect.submit();
    }
    function viewPage(billId) {
        document.connect.action = '/throttle/connectBillMapping.do?param=view&billId=' + billId;
    }
    function savePage() {
        var billId = document.getElementById("billId").value;
        document.connect.action = '/throttle/connectBillMapping.do?param=save&billId=' + billId;
        document.connect.submit();
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Connect Bill Mapping </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">HS Connect</a></li>
                <li class="">Connect Bill Mapping</li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="connect" method="post" enctype="multipart/form-data">
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30">
                                    <th>S.No</th>
                                    <th>Reference No</th>
                                    <th>Bill No</th>
                                    <th>Bill Date</th>
                                    <th>Base Freight</th>
                                    <th>Unloading Charges</th>
                                    <th>Halting Charges</th>
                                    <th>Other Charges</th>
                                    <th>GST Charges</th>
                                    <th>Bill Amount</th>
                                    <th>Vendor Name</th>
                                    <th>Period From</th>
                                    <th>Period To</th>
                                    <th>Date of Received</th>
                                    <th>Date Of Entry</th>
                                    <th>Received By</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                                <c:if test = "${connectGetBillReceiving != null}">
                                    <c:forEach items="${connectGetBillReceiving}" var="pc">
                                        <%
                                            sno++;
                                            String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.referenceNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.billNo}" /></td>
                                            <c:set var="billNo" value="${pc.billNo}"/>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.billDate}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.freightAmount}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.unloadingAmt}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.haltingAmt}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.otherAmt}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.gstAmt}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.billAmt}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.vendorName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.periodFrom}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.periodTo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.dateOfReceived}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.dateOfEntry}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.receivedBy}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.status}" /></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>
                        <br><br>
                        <table class="table table-info table-hover">
                            <thead>
                            <th colspan="4">Upload Bill Excel</th>
                            </thead>
                            <tr>
                                <td align="center">Bill No</td>
                                <td align="center"><input type="text" style="width:240px;height:40px" readonly class="form-control" id="billNo" name="billNo" value="<c:out value="${billNo}"/>"/></td>
                                <td align="center">
                                    Select Excel
                                </td>
                                <td align="center">
                                    <input type="file" name="excel" id="excel" value="uploadExcel">
                                    <input type="hidden" name="billId" id="billId" value="<c:out value="${billId}"/>"/>
                                </td>
                            </tr>
                        </table>
                        <center> 
                            <input type="button" class="btn btn-success" id="submit1" name="submit1" value="Submit" onclick="submitPage1('<c:out value="${billId}"/>', '<c:out value="${billNo}"/>');">
                        </center>
                        <br>
                        <br>
                        <input type="hidden" name="count" id="count" value="<%=sno%>" />
                        <c:if test="${size>0}">
                            <table class="table table-info table-bordered" id="table2">
                                <thead>
                                    <tr>
                                        <th>S. No</th>
                                        <th>Bill No</th>
                                        <th>Lr No</th>
                                        <th>Created On</th>
                                        <th>Freight Amount</th>
                                        <th>Unloading Charges</th>
                                        <th>Halting Charges</th>
                                        <th>Other Charges</th>
                                        <th>Gst Amount</th>
                                        <th>Bill Amount</th>
                                        <th>Status</th>


                                    </tr>
                                </thead>

                                <%
                                    int sno1 = 1;
                                %>
                                <tbody>

                                    <c:set var="freightTot" value="0"/>
                                    <c:set var="unloadingTot" value="0"/>
                                    <c:set var="haltingTot" value="0"/>
                                    <c:set var="otherTot" value="0"/>
                                    <c:set var="gstTot" value="0"/>
                                    <c:set var="billTot" value="0"/>
                                    <c:forEach items="${billTemp}" var="pc">
                                        <tr>
                                            <td align="left"> <%= sno1%> </td>                                    
                                            <td align="left"> <c:out value="${pc.billNo}" /></td>
                                            <td align="left"> <c:out value="${pc.lrNo}" /></td>
                                            <td align="left"> <c:out value="${pc.createdOn}" /></td>
                                            <td align="left"> <c:out value="${pc.freightAmount}" /></td>
                                            <td align="left"> <c:out value="${pc.unloadingAmt}" /></td>
                                            <td align="left"> <c:out value="${pc.haltingAmt}" /></td>
                                            <td align="left"> <c:out value="${pc.otherAmt}" /></td>
                                            <td align="left"> <c:out value="${pc.gstAmt}" /></td>
                                            <td align="left"> <c:out value="${pc.billAmt}" /></td>
                                            <td align="left"> 
                                                <c:if test="${pc.status=='0'}" ><font color="green">Ready</font></c:if>
                                                <c:if test="${pc.status!='0'}" ><font color="red">Lr No Not Exists</font></c:if>
                                                </td>
                                            <%sno1 ++;%>
                                        </tr>
                                        <c:if test="${pc.status=='0'}" >
                                            <c:set var="freightTot" value="${freightTot+pc.freightAmount}"/>
                                            <c:set var="unloadingTot" value="${unloadingTot+pc.unloadingAmt}"/>
                                            <c:set var="haltingTot" value="${haltingTot+pc.haltingAmt}"/>
                                            <c:set var="otherTot" value="${otherTot+pc.otherAmt}"/>
                                            <c:set var="gstTot" value="${gstTot+pc.gstAmt}"/>
                                            <c:set var="billTot" value="${billTot+pc.billAmt}"/>
                                        </c:if>
                                    </c:forEach>
                                </tbody>
                            </table>
                            <br><br>
                            <table class="table table-info mb30 table-hover" id="table" >	
                                <thead>
                                    <tr><th colspan="8">Expense Review between Excel and Updated</th></tr>
                                    <tr height="30">
                                        <th>Check</th>
                                        <th>Bill No</th>
                                        <th>Base Freight</th>
                                        <th>Unloading Charges</th>
                                        <th>Halting Charges</th>
                                        <th>Other Charges</th>
                                        <th>GST Charges</th>
                                        <th>Bill Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% int sno2 = 1;%>
                                    <c:if test = "${connectGetBillReceiving != null}">
                                        <c:forEach items="${connectGetBillReceiving}" var="pc">

                                            <tr>
                                                <td align="left"><b> System Updated </b></td>
                                                <td align="left"> <c:out value="${pc.billNo}" /></td>
                                                <td align="left"> <c:out value="${pc.freightAmount}" /></td>
                                                <td align="left"> <c:out value="${pc.unloadingAmt}" /></td>
                                                <td align="left"> <c:out value="${pc.haltingAmt}" /></td>
                                                <td align="left"> <c:out value="${pc.otherAmt}" /></td>
                                                <td align="left"> <c:out value="${pc.gstAmt}" /></td>
                                                <td align="left"> <c:out value="${pc.billAmt}" /></td>
                                            </tr>
                                            <%sno2++;%>
                                        </c:forEach>
                                        <tr>
                                            <td align="left"><b>Excel Upload</b></td>
                                            <td align="left"><c:out value="${billNo}"/></td>
                                            <td align="left"><c:out value="${freightTot}"/></td>
                                            <td align="left"><c:out value="${unloadingTot}" /></td>
                                            <td align="left"><c:out value="${haltingTot}" /></td>
                                            <td align="left"><c:out value="${otherTot}" /></td>
                                            <td align="left"><c:out value="${gstTot}" /></td>
                                            <td align="left"><c:out value="${billTot}" /></td>
                                        </tr>
                                    </tbody>
                                </c:if>
                            </table>
                            <br>
                            <br>
                            <center>
                                <input type="button" class="btn btn-success" name="final" id="final" value="Save" onclick="savePage();">
                            </center>
                        </c:if>
                        <br>
                        <br>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>