<%-- 
    Document   : chakiat
    Created on : 10 Feb, 2021, 3:05:32 PM
    Author     : entitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <style>
#table-example-1 tr,th,table{ 
  width:100%;
  border: solid thin; 
  border-collapse: collapse;
}
#table-example-1 th, 
#table-example-1 td { 
  border: solid thin;
  padding: 0.5rem 1.5rem;
}
#table-example-1 td {
  white-space: nowrap;
}
#table-example-1 th { 
  font-weight: normal; 
}
#table-example-1 td { 
  border-style: solid thin; 
  vertical-align: top; 
}
#table-example-1 th { 
  padding: 0.7em; 
  vertical-align: middle; 
  text-align: center; 
}

#table-example-1 tbody td:first-child::after { 
  content: leader(". "); 
}

body {
  padding: 0.7rem;
}

</style>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Consignment Note</title>
    </head>
    <body>
        <br><br><br>
        <h1 style="text-align: center; font-size: 25px">CHAKIAT AGENCIES - TRANSPORT DIVISION</h1>
        <h1 style="text-align: center; font-size: 25px">SUBRAMANIAN ROAD,</h1>
        <h1 style="text-align: center; font-size: 25px">WILLINGDON ISLAND, COCHIN - 682 003</h1>
        <h1 style="text-align: center; font-size: 25px">PH: 0484-2667111,2667459</h1>
        <h1 style="text-align: center; font-size: 25px">GST NO : 32AABFC5853E2ZU</h1>
<table id="table-example-1">
    <thead><tr><th colspan="9"><b><u>Consignment Note</u></b></th></tr></thead><tbody><tr>
            <td colspan="1">S.No:</td>
            <td colspan="5"> &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</td>
     <td colspan="3">Date:</td>
     </tr>
     <tr><td colspan="1">Name of the Consignor</td><td colspan="8"></td></tr>
     <tr><td colspan="1">Name of the Consignee</td><td colspan="8"></td></tr>
     <tr><td colspan="1">Registration Number of the goods<br> Carriage in which the goods are<br> transported:</td><td colspan="8"></td></tr>
     <tr><td colspan="1"><br>Owner of the Vehicle:<br><br></td><td colspan="8"></td></tr>
     <tr><td colspan="9"><br><br><br>Details of the Goods Transported<br></td></tr>
     <tr><td colspan="1"><br><br><br>Invoice No<br></td></td>
     <td colspan="1"><br><br><br>Invoice Date/ Bill<br> of Entry Date</td>
     <td colspan="1"><br><br><br><br>HSN Code</td>
     <td colspan="1"><br><br>Description of <br>Goods/<br>Commodity</td>
     <td colspan="1"><br><br><br><br>Quantity</td>
     <td colspan="1"><br><br><br><br>Units</td>
     <td colspan="1"><br><br>Basic <br>Price<br>(Rs.)</td>
     <td colspan="1">Other<br>Charges <br>Incl.Custom<br> Duty (if <br>any)(Rs.)</td>
     <td colspan="1"><br>Total<br> Value of<br> Goods<br>(Rs.)</td></tr>
     <tr><td colspan="1"><br><br><br><br></td>
     <td colspan="1"></td>
     <td colspan="1"></td>
     <td colspan="1"></td>
     <td colspan="1"></td>
     <td colspan="1"></td>
     <td colspan="1"></td>
     <td colspan="1"></td>
     <td colspan="1"></td>
     </tr>
     <tr>
         <td colspan="1">Place of Origin:</td>
         <td colspan="8"></td>
     </tr>
     <tr>
         <td colspan="1">Place of Destination</td>
         <td colspan="8"></td>
     </tr>
     <tr>
         <td colspan="1"><b>Person Liable for paying GST whether <br>Consignor, Consignee or the Goods<br>Transport Agency:</b></td>
         <td colspan="8"></td>
     </tr>
    </tbody></table>
        <h3 style="text-align: right;">FOR CHAKIAT AGENCIES-TRANSPORT DIVISION</h3>
        <h4 style="text-align: right;"><br><br><br><br>Authorised Signatory</h4>
    <br><br><br><br><br></body>
</html>