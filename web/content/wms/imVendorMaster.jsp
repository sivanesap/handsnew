<%-- 
    Document   : imVendorMaster
    Created on : 1 Dec, 2021, 12:37:38 PM
    Author     : alan
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    function submitPage() {
        if (document.getElementById("vendorName").value == "") {
            alert("Enter Vendor Name");
            document.getElementById("vendorName").focus();
        } else if (document.getElementById("vendorCode").value == "") {
            alert("Enter Vendor Code");
            document.getElementById("vendorCode").focus();
        } else if (document.getElementById("city").value == "") {
            alert("Enter city");
            document.getElementById("city").focus();
        } else if (document.getElementById("state").value == "") {
            alert("Enter state");
            document.getElementById("state").focus();
        } else if (document.getElementById("address").value == "") {
            alert("Enter address");
            document.getElementById("address").focus();
        } else if (document.getElementById("phoneNumber").value == "") {
            alert("Enter phoneNo");
            document.getElementById("phoneNumber").focus();
        } 
         
         else if (document.getElementById("emailId").value == "") {
            alert("Enter EmailId");
            document.getElementById("emailId").focus();
        }
         else if (document.getElementById("contactPerson").value == "") {
            alert("Enter contactPerson");
            document.getElementById("contactPerson").focus();
        }
         else if (document.getElementById("gstNo").value == "") {
            alert("Enter GST NO");
            document.getElementById("gstNo").focus();
        }else {
            document.vendor.action = " /throttle/imVendorMaster.do?param=save";
            document.vendor.method = "post";
            document.vendor.submit();
        }
    }
    function setValues(sno, vendorId,vendorName, vendorCode, city, state, address, phoneNumber, emailId,contactPerson,gstNo) {
        var count = parseInt(document.getElementById("count").value);
        for (var i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        
         document.getElementById("vendorId").value = vendorId;
//        alert(document.getElementById("vendorId").value);
        document.getElementById("vendorName").value = vendorName;
        document.getElementById("vendorCode").value = vendorCode;
        document.getElementById("city").value = city;
        document.getElementById("state").value = state;
        document.getElementById("address").value = address;
        document.getElementById("phoneNumber").value = phoneNumber;
      
        document.getElementById("emailId").value = emailId;
        document.getElementById("contactPerson").value=contactPerson;
        document.getElementById("gstNo").value=gstNo;
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Vendor Master </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html">InstaMart</a></li>
                <li class="">Vendor Master</li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="document.vendor.vendorName.focus();">


                    <form name="vendor"  method="post" >
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <br>
                       <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                            <input type="hidden" name="vendorId" id="vendorId" value=""  />
                            <tr>
                                <!--<table  border="0" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">-->
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="8" >Vendor Master</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;Vendor Name</td>
                                    <td><input type="text" id="vendorName" name="vendorName" class="form-control" style="width:240px;height:40px" autocomplete="off"></td>
                                    <td class="text1">&nbsp;&nbsp;Vendor Code</td>
                                    <td class="text1"><input type="text" name="vendorCode" id="vendorCode" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;City</td>
                                    <td class="text1">
                                        <select name="city" id="city" class="form-control" style="width:240px;height:40px" autocomplete="off">
                                            <option value="">------Select One------</option>
                                            <c:forEach items="${city}" var="vendor">
                                                <option value="<c:out value="${vendor.id}"/>"><c:out value="${vendor.city}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                    <td class="text1">&nbsp;&nbsp;State</td>
                              
                                    <td class="text1">
                                        <select name="state" id="state" class="form-control" style="width:240px;height:40px" autocomplete="off">
                                            <option value="">------Select One------</option>
                                            <c:forEach items="${state}" var="vendor">
                                                <option value="<c:out value="${vendor.stateId}"/>"><c:out value="${vendor.stateName}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;Address</td>
                                    <td class="text1"><input type="text" name="address" id="address" class="form-control" style="width:240px;height:40px"/></td>
                                    <td class="text1">&nbsp;&nbsp;Phone No.</td>
                                    <td class="text1"><input type="text" name="phoneNumber" id="phoneNumber" value="" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>
                                    
                                      <td class="text1">&nbsp;&nbsp;Email Id</td>
                                     <td class="text1"><input type="text" name="emailId" id="emailId" class="form-control" style="width:240px;height:40px"/></td>  
                                    
                                </tr>
                                <td class="text1">&nbsp;&nbsp;Contact Person</td>
                                     <td class="text1"><input type="text" name="contactPerson" id="contactPerson" class="form-control" style="width:240px;height:40px"/></td>
                                      <td class="text1">&nbsp;&nbsp;GST No.</td>
                                     <td class="text1"><input type="text" name="gstNo" id="gstNo" class="form-control" style="width:240px;height:40px"/></td>  
                                <tr>
                                    
                                </tr>

                            </table>
                            </tr>
                            <tr>
                                <td>
                                    <br>
                                    <center>
                                        <input type="button" class="btn btn-success" value="Save" name="Submit" onClick="submitPage()">
                                    </center>
                                </td>
                            </tr>
                        </table>
                        <br>

                        
                        <input type = "hidden" id = "cityName" name = "cityName">
                        <input type = "hidden" id = "stateName" name = "stateName">
                       <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30" style="color: white">
                                    <th>S.No</th>
                                    <th>Vendor Name</th>
                                    <th>Vendor Code</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Address</th>
                                    <th>Phone No.</th>
                                    <th>Email Id</th>
                                    <th>Contact Person</th>
                                    <th>GST No</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                                <c:if test = "${vendorMaster != null}">
                                    <c:forEach items="${vendorMaster}" var="pc">
                                        <%
                                                    sno++;
                                                    String className = "text1";
                                                    if ((sno % 1) == 0) {
                                                        className = "text1";
                                                    } else {
                                                        className = "text2";
                                                    }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.vendorName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.vendorCode}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.cityName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.state}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.address}" /></td>
                                         
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.phoneNumber}" /></td>
                                           
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.emailId}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.contactPerson}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.gstNo}" /></td>
                                            <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${pc.vendorId}" />','<c:out value="${pc.vendorName}" />', '<c:out value="${pc.vendorCode}" />', '<c:out value="${pc.city}" />', '<c:out value="${pc.stateId}" />', '<c:out value="${pc.address}" />','<c:out value="${pc.phoneNumber}" />','<c:out value="${pc.emailId}" />','<c:out value="${pc.contactPerson}" />','<c:out value="${pc.gstNo}" />');" /></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>


                        <input type="hidden" name="count" id="count" value="<%=sno%>" />

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                          setFilterGrid("table");
                        </script>

                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>

