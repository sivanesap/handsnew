
<%-- 
    Document   : imMaterialPO
    Created on : 19 Oct, 2021, 12:45:52 PM
    Author     : Roger
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    if (day.toString().length == 1) {
        day = "0" + day;
    }
    if (month.toString().length == 1) {
        month = "0" + month;
    }
    if (year.toString().length == 1) {
        year = "0" + year;
    }
    finalDate = day + "-" + month + "-" + year;
    $(document).ready(function() {
        $('#poDate').val(finalDate);
//        $('#inDate').datepicker('setDate', 'today');
//        $("#datepicker").datepicker({
//            showOn: "button",
//            buttonImage: "calendar.gif",
//            buttonImageOnly: true
//
//        });
    });




    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<style>

    .tdn{
        background-color: #f2efe6;
        border: none;
        border-radius: 2%;
        padding: 2px;
        width: 130px

    }
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Material PO </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">InstaMart</a></li>
            <li class="active">Material PO</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="">
                <form name="saveGtnDetails" method="post">
                    <table class="table table-info mb30 table-hover" id="serial">
                        <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                        </div>
                        <tr>
                            <td align="center"><text style="color:red">*</text>
                                Warehouse 
                            </td>
                            <td>
                                <select id="whId" name="whId"  class="form-control" style="width:240px;height:40px" onchange="sendWhId(this);">
                                    <option value="">-------Select Warehouse------</option>
                                    <c:forEach items="${whList}" var="cm">
                                        <option value="<c:out value="${cm.whId}"/>"><c:out value="${cm.whName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td align="center"><text style="color:red">*</text>
                                Customer Name
                            </td>
                            <td>
                                <select id="custId" name="custId" value="" class="form-control" style="width:240px;height:40px" onchange="checkInvoice(this.value);">
                                    <option value="">-------Select Customer------</option>
                                    <c:forEach items="${customerMaster}" var="cm">
                                        <option value="<c:out value="${cm.custId}"/>"><c:out value="${cm.custName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <script>
                            document.getElementById("whId").value = '<c:out value="${whId}"/>'
                        </script>
                        <tr>
                            <td align="center"><text style="color:red">*</text>PO Date</td>
                            <td height="30">
                                <input readonly name="poDate" id="poDate"  class="form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="" autocomplete="off">
                                <!--<input readonly name="poDate" id="poDate"  style="width:240px;height:40px"  value="" autocomplete="off"></td>-->

                            <td align="center"><text style="color:red">*</text>Expected Delivery Date</td>
                            <td height="30"><input name="deliveryDate" id="deliveryDate" type="text"  class="datepicker , form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${gtnDate}"/>" autocomplete="off"></td>                            
                        </tr>
                        <script>
                            document.getElementById("whId").value = '<c:out value="${whId}"/>';
                        </script>
                        <tr>
                            <td align="center">Total Quantity</td>
                            <td><input type="text" readonly name="totQty" id="totQty" class="form-control" value="0" style="width:240px;height:40px"></td>
                            <td align="center"><text style="color:red">*</text>
                                Indent No
                            </td>
                            <td>
                                <!--<select id="indent" name="indent"  class="form-control" style="width:240px;height:40px" onclick="testOn(this.value);">-->
                                <select id="indent" name="indent"  class="form-control" style="width:240px;height:40px" onchange="onDoit(this.value);">
                                    <option value="">-------Select Indent------</option>
                                    <c:forEach items="${imMaterialIndent}" var="cm">
                                        <option value="<c:out value="${cm.inId}"/>"><c:out value="${cm.inNo}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                Remarks
                            </td>
                            <td>
                                <textarea type="text" id="remarks" name="remarks" value="" class="form-control" style="width:240px; height:60px"/></textarea> 
                            </td>
                            <td align="center"><text style="color:red">*</text>
                                Tax
                            </td>
                            <td>
                               <select id="tax" name="tax"  class="form-control" style="width:240px;height:40px">
                                    <option value="18">Tax 18%</option>
                                    <option value="0">Tax 0%</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="center"><text style="color:red">*</text>
                                With Tax
                            </td>
                            <td>
                                <input id="priceTotQty" name="priceTotQty" class="form-control" style="width:240px;height:40px" readonly/>
                            </td>
                        </tr>

                    </table>                 
                    <table class="table table-info mb30 table-hover sortable" id="addIndent" align="center" width="90%">
                        <thead >
                            <tr>
                                <th>S.No</th>
                                <th>Material Name</th>  
                                <th>UOM</th>
                                <th>Unit Price</th>
                                <th>Stock</th>
                                <th>In Qty</th>
                                <th>Pending Qty</th>
                                <th>PO Qty</th>
                                <th>Price</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody id = "body">


                        </tbody>
                        <tbody id="addIndent">

                        </tbody>

                    </table>

                    <script>
                        function sendWhId(value) {
                            var whId = value.value;
                            document.saveGtnDetails.action = '/throttle/imMaterialPO.do?param=get&whId=' + whId;
                            document.saveGtnDetails.submit();
                        }

                        function submitPage(value) {

                            if (document.getElementById("custId").value == "") {
                                alert("Enter Customer Name");
                                document.getElementById("custId").focus();
                            } else if (document.getElementById("poDate").value == "") {
                                alert("Enter PO Date");
                                document.getElementById("poDate").focus();
                            } else if (document.getElementById("deliveryDate").value == "") {
                                alert("Enter Expected Delivery Date");
                                document.getElementById("deliveryDate").focus();

                            } else {

                                document.saveGtnDetails.action = '/throttle/imMaterialPO.do?param=save';
                                document.saveGtnDetails.submit();
                            }
                        }
                        function checkqty() {
                            var qty = document.getElementsByName("poQty");
                            var totQty = 0;
                            for (var i = 0; i < qty.length; i++) {
                                if (qty[i].value != "") {
                                    totQty = parseInt(qty[i].value) + totQty;
                                }
                            }
                            document.getElementById("totQty").value = totQty;
                        }
                        function checkExist(value, sno) {
                            var arr = value.split("~");
                            var materialId = arr[0];
                            var materialName = arr[1];
                            var materialCode = arr[2];
                            var materialDescription = arr[3];
                            var count = 0;
                            var matTemp = document.getElementsByName("materialIdTemp");
                            for (var i = 0; i < matTemp.length; i++) {
                                if (matTemp[i].value == materialId) {
                                    count++;
                                }
                            }
                            if (count == 0) {
                                document.getElementById("materialIdTemp" + sno).value = materialId;
                                document.getElementById("materialName" + sno).value = materialName;
                                document.getElementById("materialCode" + sno).value = materialCode;
                                document.getElementById("materialDescription" + sno).value = materialDescription;
                            } else {
                                alert("Already Choosed Product");
                                document.getElementById("materialIdTemp" + sno).value = "";
                                document.getElementById("materialId" + sno).value = "";
                            }
                        }



                        function setValue(value) {

                            var unitPrice = document.getElementById("unitPrice" + value).value;
                            var price = document.getElementById("price" + value).value;
                            var pending = document.getElementById("pending" + value).value;
                            var poQty = document.getElementById("poQty" + value).value;
                            var pendingX = document.getElementById("pendingx" + value).value;
                            var tax = document.getElementById("tax").value;


//                            alert(pendingX + " " + poQty)
                            if (parseInt(pendingX) < parseInt(poQty)) {
                                alert("PO Qty is Greater than Indent Qty")
                                document.getElementById("poQty" + value).value = "";
                                document.getElementById("price" + value).value = "";


                            }
                            else {
                                if (unitPrice != "0") {
                                    document.getElementById("pending" + value).value = parseInt(pendingX) - parseInt(poQty)
                                    document.getElementById("price" + value).value = parseInt(poQty) * parseInt(unitPrice)


                                } else {
                                    alert("select Unit Price")
                                    document.getElementById("poQty" + value).value = "";

                                }



                            }



                            var qty = document.getElementsByName("poQty");
                            var totQty = 0;
                            for (var i = 0; i < qty.length; i++) {
                                if (qty[i].value != "") {
                                    totQty = parseInt(qty[i].value) + totQty;
                                }
                            }
                            document.getElementById("totQty").value = totQty;


                            var price = document.getElementsByName("price");
                            var priceTotQty = 0;
                            for (var i = 0; i < price.length; i++) {
                                if (price[i].value != "") {
                                    priceTotQty = parseInt(price[i].value) + priceTotQty;
                                }
                            }


                            if (tax == "18") {
                                var taxed = priceTotQty * 18 / 100;
                                document.getElementById("priceTotQty").value = taxed + priceTotQty;

                            } else {

                                document.getElementById("priceTotQty").value = priceTotQty;
                            }
                        }

                        var listOfId = [];





                        function onDoit(value) {
                            if (listOfId.length > 0 && listOfId.indexOf(value) > -1) {
                                alert("Already Selected");
                            } else {

                                listOfId.push(value)
                                var indentId = value
                                var maintablebody = document.getElementById("body");

                                $.ajax({
                                    url: "/throttle/imInSetGet.do",
                                    dataType: "json",
                                    data: {
                                        indentId: indentId,
                                    },
                                    success: function(data) {
                                        $.each(data, function(i, data) {

//                                        alert(data)
                                            var tab = document.getElementById("addIndent");
                                            var iRowCount = tab.getElementsByTagName('tr').length;

                                            rowCount = iRowCount;
                                            var newrow = tab.insertRow(rowCount);
                                            //        alert(iRowCount);
                                            sno = rowCount;
                                            rowIndex = rowCount;
                                            //        alert("rowIndex:"+rowIndex);
                                            var cell = newrow.insertCell(0);
                                            var cell0 = "<td > <input type='hidden'  name='Sno' value='' > " + sno + "</td>";
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(1);
                                            var cell2 = "<td ><input type='text'value = '" + data.material + "' readonly id='material" + sno + "' name = 'materialName' style='width:400px;height:40px' ></td>"
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(2);
                                            var cell2 = "<td ><input type='text'value = '" + data.uom + "' readonly id='uom" + sno + "' name = 'uom' style='width:75px;height:40px' ></td>"
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(3);
                                            var cell2 = "<td ><input type='text'value = '0'   id='unitPrice" + sno + "' name = 'unitPrice' style='width:75px;height:40px' ></td>"
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(4);
                                            cell2 = "<td ><input type='text'value = '" + data.stock + "' readonly id='stock" + sno + "' name = 'stock' style='width:75px;height:40px' ></td>";
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(5);
                                            cell2 = "<td ><input type='text'value = '" + data.qty + "' readonly id='qty" + sno + "' name = 'qty' style='width:75px;height:40px'  ></td>";
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(6);
                                            cell2 = "<td ><input type='text'value = '" + data.pendingQty + "' readonly id='pending" + sno + "' name = 'pending' style='width:75px;height:40px' ></td>";
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(7);
                                            cell2 = "<td ><input type='text'value = '" + "" + "' style='width:75px;height:40px' id='poQty" + sno + "' name = 'poQty'  onchange = 'setValue(" + sno + ");'></td>";
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(8);
                                            cell2 = "<td ><input type='text'value = '" + "" + "' readonly id='price" + sno + "' name = 'price' style='width:75px;height:40px' ></td>";
                                            cell.innerHTML = cell2;



                                            cell = newrow.insertCell(9);
                                            var cell2 = "<td  ><input type='button' class='btn btn-info' name='delete'  id='delete'   value='Delete Row' onclick='deleteRow(this," + sno + ");' ></td>";
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(10);
                                            var cell2 = "<td  ><input type='hidden' value = '" + data.materialId + "' readonly id='materialId" + sno + "' name = 'materialId' class = 'tdn'></td>";
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(11);
                                            var cell2 = "<td  ><input type='hidden'value = '" + data.pendingQty + "' readonly id='pendingx" + sno + "' name = 'pendingx' class = 'tdn' ></td>";
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(12);
                                            var cell2 = "<td  ><input type='hidden' value = '" + data.id + "' readonly id='idd" + sno + "' name = 'idd' class = 'tdn' ></td>";
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(13);
                                            var cell2 = "<td  ><input type='hidden'value = '" + data.materialCode + "' readonly id='materialCode" + sno + "' name = 'materialCode' class = 'tdn' ></td>";
                                            cell.innerHTML = cell2;

                                            cell = newrow.insertCell(14);
                                            var cell2 = "<td  ><input type='hidden' value = '" + data.materialDescription + "' readonly id='materialDescription" + sno + "' name = 'materialDescription' class = 'tdn' ></td>";
                                            cell.innerHTML = cell2;


                                            rowIndex++;
                                            rowCount++;
                                            sno++;


                                        })

                                    }
                                }
                                )
                            }
                        }


                        function deleteRow(src) {
                            sno--;
                            var oRow = src.parentNode.parentNode;
                            var dRow = document.getElementById('addIndent');
                            dRow.deleteRow(oRow.rowIndex);
                            document.getElementById("selectedRowCount").value--;
                        }





                    </script>

                    <table>
                        <center>
                            &emsp;<input type="button" value="Submit" id="insert" class="btn btn-success" onClick="submitPage(this.value);">
                        </center>

                    </table>
                </form>
            </body>
        </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>

    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>
