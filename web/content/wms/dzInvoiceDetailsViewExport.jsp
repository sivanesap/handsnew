<%-- 
    Document   : dzInvoiceDetailsViewExport
    Created on : 1 Feb, 2022, 7:42:54 PM
    Author     : alan
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Excel";
        request.setAttribute("menuPath", menuPath);
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "dzInvoiceDetailsExportExcel-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <table class="table table-info mb30 table-bordered" id="table" class="sortable" width="100%">


       
                            <c:if test = "${getdzInvoiceUpload != null}" >


                                    <thead style="font-size:12px">
                                        <tr height="40">
                                            <th>Sno</th>
                                            <th>Product Id</th>
                                            <th>Category</th>
                                            <th>Transfer Order No</th>
                                            <th>Product Code</th>
                                            <th>Product Name</th>
                                            <th>Send Qty</th>
                                            <th>Received Qty</th>
                                            <th>Difference Qty</th>
                                            <th>Send Amount</th>
                                            <th>Received Amount</th>
                                            <th>Price</th>
                                            <th>From Location Name</th>
                                            <th>To Location Name</th>
                                            <th>TO Send Qty</th>
                                            <th>TO Received Qty</th>
                                            <th>TO Send Amount</th>
                                            <th>TO Received Amount</th>
                                  
                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                     int count=0;
                                    %> 
                                    <tbody>

                                        <c:forEach items="${getdzInvoiceUpload}" var="pc">


                                            <tr>
                                                <td align="left"> <%=sno%> </td>
                                                <td align="left"> <c:out value="${pc.productId}"/></td>
                                                <td align="left"> <c:out value="${pc.categoryName}"/> </td>
                                                <td align="left"> <c:out value="${pc.invoiceNo}"/> </td>
                                                <td align="left"> <c:out value="${pc.productCode}"/></td>
                                                <td align="left"> <c:out value="${pc.productName}"/> </td>
                                                <td align="left"> <c:out value="${pc.orderedQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.receivedQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.diffQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.sendAmt}"/></td>
                                                <td align="left"> <c:out value="${pc.receivedAmt}"/></td>
                                                <td align="left"> <c:out value="${pc.price}"/> </td>
                                                <td align="left"> <c:out value="${pc.fromWhName}"/> </td>
                                                <td align="left"> <c:out value="${pc.toWhName}"/> </td>
                                                <td align="left"> <c:out value="${pc.totalOrderedQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.totalReceivedQty}"/></td>
                                                <td align="left"> <c:out value="${pc.totalSendAmt}"/> </td>
                                                <td align="left"> <c:out value="${pc.totalReceivedAmt}"/></td>
                                
                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                    </tbody>
                          

                              
                            </c:if>


            </table>


            <br>
            <br>

        </form>
    </body>
</html>