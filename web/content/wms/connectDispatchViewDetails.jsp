<%-- 
    Document   : connectDispatchViewDetails
    Created on : 7 Sep, 2021, 7:01:28 PM
    Author     : alan
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
//alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script type="text/javascript">
    function uploadContract() {
        document.upload.action = "/throttle/uploadconnectInvoice.do";
        document.upload.method = "post";
        document.upload.submit();
    }

    function uploadPage() {
        if (document.getElementById("countStatus").value > 0) {
            var result = confirm("Some errors are not Updated. Do you want to continue?");
            if (result == true) {
                document.upload.action = "/throttle/connectUploadInvoice.do?&param=save";
                document.upload.method = "post";
                document.upload.submit();
            }
        } else {
            document.upload.action = "/throttle/connectUploadInvoice.do?&param=save";
            document.upload.method = "post";
            document.upload.submit();
        }
    }
    
    function searchPage(param){
        document.upload.action="/throttle/connectPicklist.do?&param="+param;
        document.upload.submit();
    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="Connect Dispatch View Details"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Connect" text="HS Connect"/></a></li>
            <li class="">Connect Dispatch View Details</li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form name="upload"  method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>



                    <div id="ptp">
                        <div class="inpad" style=" overflow-x: visible;">
                            
                            <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                <thead style="font-size:12px">
                                    <tr height="40">
                                        <th  height="30" >S No</th>
                                        <th  height="30" >Invoice No</th>
                                        <th  height="30" >Qty</th>
                                        <th  height="30" >Pre Tax Value</th>
                                        <th  height="30" >Invoice Amount</th>
                                        <th  height="30" >Customer Name</th>
                                        <th  height="30" >Product Name</th>
                                        <th height="30">Delete Details</th>
                                    </tr>
                                </thead>
                                <% int index = 0; 
                                 int sno = 1;
                                 int count=0;
                                %> 
                                <tbody>
                                        <c:forEach items= "${connectDispatchViewDetails}" var="list">
                                                <tr height="30">
                                                    <td align="left"><%=sno%></td>
                                                    <td align="left"   ><c:out value="${list.invoiceNo}"/></td>
                                                    <td align="left"   ><c:out value="${list.qty}"/></td>
                                                    <td align="left"   ><c:out value="${list.pretaxValue}"/></td>
                                                    <td align="left"   ><c:out value="${list.invoiceAmount}"/></td>
                                                    <td align="left"   ><c:out value="${list.customerName}"/></td>
                                                    <td align="left"   ><c:out value="${list.productName}"/></td>
                                                    <td height="25"><a style="font-size:16px;" href="/throttle/connectDispatchView.do?param=delete&dispatchId=<c:out value="${list.dispatchId}"/>&invoiceNo=<c:out value="${list.invoiceNo}"/>">Delete</a></td>
                                                </tr> 
                                                <%
                                                sno++;
                                                index++;
                                                %>
                                        </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                </tbody>
                            </table>

                        </div>
                    </div>     

                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>


