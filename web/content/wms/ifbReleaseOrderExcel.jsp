<%-- 
    Document   : ifbReleaseOrderExcel
    Created on : 3 Jun, 2021, 10:01:18 PM
    Author     : Roger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Excel";
        request.setAttribute("menuPath", menuPath);
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "UndeliveredDetails-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                <thead>
                    <tr height="40">
                        <th  height="30" >S.No</th>
                        <th  height="30" >Invoice</th>
                        <th  height="30" >Item Name</th>
                        <th  height="30" >Serial No</th>
                        <th  height="30" >Customer Name</th>
                        <th  height="30" >City</th>
                        <th  height="30" >Pincode</th>
                        <th  height="30" >Quantity</th>
                        <th  height="30" >Reason for Rejection</th>
                        <th  height="30" >Amount</th>
                    </tr>
                </thead>
                <% int index = 0;
                    int sno = 1;
                %>
                <tbody>
                    <c:forEach items="${ifbOrderRelease}" var="ifb">
                    <td align="left"><%=sno%></td>
                    <td align="left"   ><c:out value="${ifb.invoiceNo}"/></td>
                    <td align="left"   ><c:out value="${ifb.itemName}"/></td>
                    <td align="left"   ><c:out value="${ifb.serialNo}"/></td>
                    <td align="left"   ><c:out value="${ifb.customerName}"/></td>
                    <td align="left"   ><c:out value="${ifb.city}"/></td>
                    <td align="left"   ><c:out value="${ifb.pincode}"/></td>
                    <td align="left"   ><c:out value="${ifb.qty}"/></td>
                    <td align="left"   ><c:out value="${ifb.reason}"/></td>
                    <td align="left"   ><c:out value="${ifb.grossValue}"/></td>                     

                    </tr>
                    <%index++;%>
                    <%sno++;%>
                </c:forEach>
                </tbody>
            </table>

            <br>
            <br>

        </form>
    </body>
</html>