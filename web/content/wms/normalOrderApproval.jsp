<%-- 
    Document   : orderApproval.jsp
    Created on : Mar 9, 2021, 8:02:14 PM
    Author     : DELL
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript">
    function uploadContract() {
        document.upload.action = "/throttle/saveNormalOrderUpload.do";
        document.upload.method = "post";
        document.upload.submit();
    }
    
      function uploadPage() {
        document.upload.action = "/throttle/saveCustomerContractUpload.do?&param=save";
        document.upload.method = "post";
        document.upload.submit();
    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="Normal Order Upload"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="Normal Order Upload"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >
                <form name="upload"  method="post" enctype="multipart/form-data">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    <table class="table table-info mb30 table-hover" id="uploadTable"  >
                        <thead> 
                            <tr>
                                <th  colspan="3">Normal Order Upload Details</th>
                            </tr>
                        </thead>
                        <tr>
                            <td >Select Excel</td>
                             
                            <td ><input type="file" name="importCnote" id="importCnote"  class="importCnote"><font size="1"><u><a href="uploadedxls/Normal order Data.xls" download>sample file</a></u></font></td> 
                            <td colspan="0"><input type="button" class="btn btn-success" value="Upload" name="UploadContract" id="UploadContract" onclick="uploadContract();"></td>
                        </tr>
                        <tr>
                        </tr>
                    </table>
                    <div id="ptp" style="overflow: auto">
                        <div class="inpad">

                            <c:if test = "${getContractList != null}" >

                                <table class="table table-info mb30 table-hover" id="table" >

                                    <thead>
                                        <tr height="40">
                                            <th  height="30" >S No</th>
                                            <th  height="30" >Shipment Id</th>
                                            <th  height="30" >Customer Name</th>
                                            <th  height="30" >City</th>
                                            <th  height="30" >Pin Code</th>
                                            <th  height="30" >Delivery Hub</th>
                                            <th  height="30" >Assigned Executive</th>
                                            <th  height="30" >Origin</th>
                                            <th  height="30" >Current Hub</th>
                                            <th  height="30" >Status</th>                                                    
                                            <th  height="30" >Note</th>
                                            <th  height="30" >Amount</th>
                                            <th  height="30" >Weight</th>
                                            <th  height="30" >Last Modified</th>
                                            <th  height="30" >Last Received</th>
                                            <th  height="30" >Shipment Type</th>
                                            <th  height="30" >Result Status</th>
                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                    %> 
                                    <tbody>

                                        <c:forEach items= "${getContractList}" var="contractList">
                                            <tr height="30">
                                                <td align="left"><%=sno%></td>
                                                <td align="left"   ><c:out value="${contractList.shipmentId}"/></td>
                                                <td align="left"   ><c:out value="${contractList.customerName}"/></td>
                                                <td align="left"   ><c:out value="${contractList.city}"/></td>
                                                <td align="left"   ><c:out value="${contractList.pinCode}"/></td>

                                                <td align="left"   ><c:out value="${contractList.deliveryHub}"/></td>
                                                <td align="left"   ><c:out value="${contractList.assignedExecutive}"/></td>
                                                <td align="left"   ><c:out value="${contractList.origin}"/></td>
                                                <td align="left"   ><c:out value="${contractList.currentHub}"/></td>
                                                <td align="left"   ><c:out value="${contractList.status}"/></td>
                                                <td align="left"   ><c:out value="${contractList.note}"/></td>

                                                <td align="left"   ><c:out value="${contractList.amount}"/></td>
                                                <td align="left"   ><c:out value="${contractList.weight}"/></td>
                                                <td align="left"   ><c:out value="${contractList.lastModified}"/></td>
                                                <td align="left"   ><c:out value="${contractList.lastReceived}"/></td>
                                                <td align="left"   ><c:out value="${contractList.shipmentType}"/></td>

                                                 <c:if test="${contractList.error==0}">
                                                <td style="color:green">
                                                        Ready To Submit
                                                </td>
                                                    </c:if>
                                                    <c:if test="${contractList.error==1}">
                                                <td style="color:red">
                                                        Invalid Pincode
                                                </td>
                                                    </c:if>
                                                    <c:if test="${contractList.error==2}">
                                                <td style="color:red">
                                                        Shipment Id Already Exists
                                                </td>
                                                    </c:if>
                                                    <c:if test="${contractList.error==3}">
                                                <td style="color:red">
                                                        Delivery Hub not Exists in Master
                                                </td>
                                                    </c:if>


                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    </tbody>
                                </table>

                                <center>

                                    <td colspan="0"><input type="button" class="btn btn-success" value="Submit" name="submitPage" id="submitPage" onclick="uploadPage();"></td>
                                </center>
                            </c:if>
                        </div>
                    </div>     

                </form>
            </body>
        </div>
    </div>
</div>


    <%@ include file="/content/common/NewDesign/settings.jsp" %>


