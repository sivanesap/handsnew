<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datep    icker.js"></script>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

    </head>
    
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Ifb Order Details </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.WMS Report" text="WMS Report"/></a></li>
                <li class="">Order Details</li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body>
                    <form name="ifb" id="ifb" method="post">
                        <%@ include file="/content/common/message.jsp" %>
                        <table class="table table-info mb10 table-hover" id="bg" >
                            <thead>
                                <tr>
                                    <th colspan="2" height="30" >Order Details</th>
                                </tr>
                            </thead>
                        </table>
                            

                            <table class="table table-info mb30 table-hover" id="table" >
                                <thead>
                                    <tr>
                                        <th align="center">S.No</th>
                                        <th align="center">Customer Name</th>
                                        <th align="center">Invoice No</th>
                                        <th align="center">Item Description</th>
                                        <th align="center">Material Group</th>
                                        <th align="center">Serial No</th>
                                        <th align="center">Qty</th>
                                        <th align="center">Gross Value</th>
                                        <c:if test="${qte==0}"><th align="center">Action</th></c:if>
                                            <input type="hidden" name="lrId" id="lrId" value="<c:out value="${lrId}"/>">
                                        
                                    </tr> 
                                </thead>
                                <tbody>
                                <% int index = 1;%>
                                    <c:forEach items="${ifbDetails}" var="ifb">
                                        <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                          %>
                                        <tr>
                                            <td><%=index%></td>
                                            <td align="center" id="cust1<%=index%>"><c:out value="${ifb.customerName}"/></td>
                                            <td align="center" id="cust2<%=index%>" style="display:none"><input type="text" name="customerName" id="customerName" style="width:150px;height:30px;" value="<c:out value="${ifb.customerName}"/>"/></td>
                                            <td align="center"><c:out value="${ifb.invoiceNo}"/></td>
                                            <td align="center"><c:out value="${ifb.materialDescription}"/></td>
                                            <td align="center"><c:out value="${ifb.matlGroup}"/></td>
                                            <td align="center"><c:out value="${ifb.material}"/></td>
                                            <td align="center" id="qty1<%=index%>"><c:out value="${ifb.qty}"/></td>
                                            <td align="center" id="qty2<%=index%>" style="display:none"><input type="text" value="<c:out value="${ifb.qty}"/>" name="qty" id="qty<%=index%>" style="width:100px;height:30px;"></td>
                                            <td align="center"><c:out value="${ifb.grossValue}"/></td>
                                            <td align="left"><c:if test="${qte==0}"><input type="checkbox" name="select" id="select<%=index%>" onchange="showQty(this,'<c:out value="${ifb.orderId}"/>','<%=index%>')"></c:if>
                                            <input type="hidden" name="index" id="index<%=index%>" value="0">
                                            <input type="hidden" name="orderId" id="orderId<%=index%>" value="<c:out value="${ifb.orderId}"/>">
                                            </td>
                                            <%
                                            index++;
                                            %>
                                           
                                        </tr>

                                    </c:forEach>
                                </tbody>
                            </table>
                            <center>
                                <input type="button" class="btn btn-success" name="Update" style="display:none" id="update"  value="Update" onclick="submitPage();">
                            </center>
                                    <script>
                                        function showQty(element,orderId,sno){
                                            if(element.checked==true){
                                            $('#qty1'+sno).hide();
                                            $('#qty2'+sno).show();
                                            $('#cust1'+sno).hide();
                                            $('#cust2'+sno).show();
                                            $('#update').show();
                                            $('#index'+sno).val(1);
                                            }else{
                                            $('#qty1'+sno).show();
                                            $('#qty2'+sno).hide();
                                            $('#cust1'+sno).show();
                                            $('#cust2'+sno).hide();
                                            $('#index'+sno).val(0);
                                            }
                                        }
                                        function submitPage(){
                                            document.ifb.action="/throttle/ifbHubOut.do?param=serial&type=update";
                                            document.ifb.submit();
                                        }
                                    </script>
                    </form>
                </body>    
            </div>
        </div>
    </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>