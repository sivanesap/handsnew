<%-- 
    Document   : connectPicklistExcel
    Created on : 28 Aug, 2021, 12:51:07 PM
    Author     : Roger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Excel";
        request.setAttribute("menuPath", menuPath);
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "Docket No Exel-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test = "${docketNoUpdateDetails != null}" >
                <table class="table table-info mb30 table-bordered" id="table" class="sortable" width="100%">
                        <thead>
                            <tr >
                                <th>SNo</th>
                                <th>LRN No</th>
                                <th>Date</th>
                                <th>To Location</th>
                                <th>Docket No.</th>
                                <th>Consignee Name</th>
                                <th>Transporter Name</th>
                                <th>Vehicle Type</th>
                                <th>Vehicle No</th>
                                <th>Transfer Type</th>
                                <th>Invoice No</th>
                                <th>Pre Tax Value</th>
                                <th>Delivery Date</th>
                            </tr>

                        </thead>
                        <tbody>
                            <% int index = 1;%>

                            <c:forEach items="${docketNoUpdateDetails}" var="dc">
                                <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                                %>



                                <tr >
                                    <td ><%=index%></td>


                                    <td ><input type="hidden" id="subLr<%=index%>" name="subLr" value="<c:out value="${dc.lrnNo}"/> " style="width:100px;height:30px;"><c:out value="${dc.lrnNo}"/></td>
                                    <td ><c:out value="${dc.lrDate}"/></td>
                                    <td ><c:out value="${dc.city}"/></td>
                                    <td ><c:out value="${dc.docketNo}"/></td>
                                    <td ><c:out value="${dc.consigneeName}"/></td>
                                    <td ><c:out value="${dc.transporterName}"/></td>
                                    <td ><c:out value="${dc.vehicleType}"/></td>
                                    <td ><c:out value="${dc.vehicleNo}"/></td>
                                    <td ><c:out value="${dc.transferType}"/></td>
                                    <td ><c:out value="${dc.invoiceNo}"/></td>
                                    <td ><c:out value="${dc.preTaxValue}"/></td>
                                    <td ><c:out value="${dc.deliveryDate}"/></td>
                                    
                                    <%index++;%>

                                </c:forEach>

                        </tbody>
                    </table>
            </c:if>

            <br>
            <br>

        </form>
    </body>
</html>