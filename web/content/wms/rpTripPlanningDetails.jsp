<%-- 
    Document   : rpTripPlanningDetails
    Created on : 29 Sep, 2021, 11:32:18 AM
    Author     : Roger
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%@page import="java.text.SimpleDateFormat"%>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>
    <%@ page import="java.text.DecimalFormat" %>
    <%@ page import="java.text.NumberFormat" %>

    <script language="javascript" src="/throttle/js/validate.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->
    <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>

    <link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

    <link rel="stylesheet" type="text/css" href="/throttle/css/select2-bootstrap.css"/>

    <script type="text/javascript">

        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            $(".datepicker").datepicker({
                changeMonth: true, changeYear: true
            });
        });

        function setButtons() {
            var temp = document.trip.actionName.value;
            if (temp != '0') {
                if (temp == '1') {
                    $("#actionDiv").show();
                    // $("#tripDiv").show();
                    //                        $("#preStartDiv").show();
                }
            } else {
                //                    $("#preStartDiv").hide();
                var vehicleId = document.trip.vehicleId.value;
                if (vehicleId != '' && vehicleId != '0') {
                    $("#actionDiv").show();
                    //  $("#tripDiv").show();
                } else {
                    $("#actionDiv").show();
                    //  $("#tripDiv").show();
                }


            }
        }
    </script>

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#tabs").tabs();
        });
    </script>
    <script language="">


        //start ajax for vehicle Nos
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#vehicleNo').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getVehicleRegNos.do",
                        dataType: "json",
                        data: {
                            vehicleNo: (request.term).trim(),
                            textBox: 1
                        },
                        success: function(data, textStatus, jqXHR) {
//                                alert(data);
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            //console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    //alert(value);
                    var tmp = value.split('-');

                    $('#vehicleId').val(tmp[0]);
                    //alert("1");
                    $('#vehicleNo').val(tmp[1]);
                    $('#actionName').empty();
                    var actionOpt = document.trip.actionName;
                    var optionVar = new Option("-select-", '0');
                    actionOpt.options[0] = optionVar;
                    optionVar = new Option("Freeze", '1');
                    actionOpt.options[1] = optionVar;
//                        $('#actionName').append(
//                            $('<option></option>').val(0).html('-select-')
//                            )
//                        $('#actionName').append(
//                            $('<option></option>').val(1).html('Freeze')
//                            )
                    $("#actionDiv").hide();

                    return false;
                }
                // Format the list menu output of the autocomplete
            }).data("autocomplete")._renderItem = function(ul, item) {
                //alert(item);
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });

        function submitPage() {
            var x = document.getElementsByName("selectedStatus");
            var n = 0;
            for (var m = 0; m < x.length; m++) {
                if (x[m].value != 1) {
                    n++;
                }
            }
            var trans = document.getElementById('vendor').value;
            //       alert("fffff"+trans)

            var startKm = document.getElementById('startKm').value

            if (document.getElementById('startKm').value == '') {
                alert("please Enter Start KM")
                $("#startKm").focus();
                return;
            }
            var statusCheck = true;
            var temp = document.trip.actionName.value;

            if (temp == '1') { //freeze selected

            } else {
                alert("please select action");
                return;
            }
            if (statusCheck) {
                if (document.getElementById("deliveryExecutiveId").value != 0 && document.getElementById("deliveryExecutiveId").value != "") {
                    if (n == 0) {
                        $("#save").hide();
                        document.trip.action = "/throttle/saveRpTripSheet.do?&param=rp";
                        document.trip.submit();
                    } else {
                        alert("Choose all Orders");
                    }
                } else {
                    alert("Please choose Delivery Executive");
                    document.getElementById("deliveryExecutiveId").focus();
                }
            }
        }

    </script>
</head>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Trip Planning</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html">Repose</a></li>
            <li class="">Trip Planning</li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="" >
                <form name="trip" method="post">
                    <%
                    Date today = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String startDate = sdf.format(today);
                     Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.DATE, 7);
                       String nextDate = sdf.format(cal.getTime());
                    %>

                    <% int i=1; %>

                    <br>
                    <div id="tabs">
                        <ul class="nav nav-tabs">
                            <li class="active" data-toggle="tab"><a href="#tripDetail"><span>RunSheet Details</span></a></li>
                            <li data-toggle="tab"><a href="#orderDetail"><span>Order Details</span></a></li>
                        </ul>

                        <script>
                            function setVehicleType(val) {
                                if (val != '0~0') {
                                    document.getElementById("hireVehicleNo").value = "";
//                                    document.trip.elements['hireVehicleNo'].style.display = 'none';
                                    document.getElementById("vehicleNo").style.display = "block";
//                                document.getElementById("veh2").style.display = "block";
//                                   document.getElementById("veh3").style.display = "none";
                                    var ownership = $("#ownership").val();
                                    $('#vehicleNo').empty();
                                    $('#vehicleNo').append($('<option></option>').val(0).html('--select--'))
                                    var vendorTemp = document.getElementById("vendor").value;
                                    var temp = vendorTemp.split("~");
                                    if (temp[1] == '1' || temp[1] == '0') {
                                    } else {
                                        document.getElementById("vehicleId").value = "0";
                                    }

                                    $.ajax({
                                        url: "/throttle/getVendorVehicleType.do",
                                        dataType: "json",
                                        data: {
                                            vendorId: temp[0],
                                            ownership: ownership
                                        },
                                        success: function(data) {
                                            //  alert(data);
                                            if (data != '') {
                                                $('#vehicleTypeName').empty();
                                                $('#vehicleTypeName').append(
                                                        $('<option></option>').val(0).html('--select--'))
                                                //                                            $('#vehicleTypeName').append(
                                                //                                                    $('<option></option>').val(1058).html('20\''))
                                                //                                            $('#vehicleTypeName').append(
                                                //                                                    $('<option></option>').val(1059).html('40\''))
                                                $.each(data, function(i, data) {
                                                    var vehicleTypeId1 = data.Id;
                                                    //alert(vehicleTypeId1);
                                                    var temp1 = vehicleTypeId1.split("~");
                                                    $('#vehicleTypeName').append(
                                                            $('<option style="width:90px"></option>').attr("value", temp1[0]).text(data.Name)
                                                            )
                                                });
                                            } else {
                                                $('#vehicleTypeName').empty();
                                                $('#vehicleTypeName').append(
                                                        $('<option></option>').val(0).html('--select--'))
                                            }
                                        }
                                    });
                                } else {
                                    resetAll();
                                    $('#vehicleTypeName').empty();
                                    $('#vehicleTypeName').append(
                                            $('<option></option>').val(0).html('--select--'))
                                    $('#lhcNo').empty();
                                    $('#lhcNo').append(
                                            $('<option></option>').val(0).html('--select--'))
                                    $('#agreedRate').val(0);
                                }
                            }


                            function setVehicle() {
                                // alert("hiii");
                                if ($("#ownership").val() == '1') {
                                    $("#veh").show();
                                    $("#veh1").show();
                                    $("#hireVeh1").hide();
//                                    $("#lhcSpan").hide();
                                } else if ($("#ownership").val() == '2') {
                                    $("#veh").show();
                                    $("#hireVeh1").show();
//                                    $("#lhcSpan").show();
                                    $("#veh1").hide();
                                } else {
                                    $("#veh").hide();
                                    $("#veh1").hide();
                                    $("#hireVeh1").hide();
//                                    $("#lhcSpan").hide();
                                }
                                var statusId = $("#tripStatus").val();
                                var vendorTemp = document.getElementById("vendor").value;
                                var temp = vendorTemp.split("~");
                                var vehicletypename = $("#vehicleTypeName option:selected").text(); //alert("hiii"+vehicletypename);
                                document.getElementById("vehicleType").value = vehicletypename;
                                var vtid = $("#vehicleTypeName option:selected").val();
                                var vtid = vtid.split("-")[0];
                                document.getElementById("vehicleTypeIdSelected").value = vtid;
                                //var statusId = arr[0];
                                $.ajax({
                                    url: "/throttle/getVendorVehicle.do",
                                    dataType: "json",
                                    data: {
                                        vehicleTypeId: $("#vehicleTypeName").val(),
                                        vendorId: temp[0],
                                        statusId: statusId
                                    },
                                    success: function(data) {
                                        // alert(data);
                                        if (data != '') {
                                            $('#vehicleNo').empty();
                                            $('#vehicleNo').append(
                                                    $('<option style="width:240px"> </option>').val(0).html('--select--'))
                                            $.each(data, function(i, data) {
                                                $('#vehicleNo').append(
                                                        $('<option style="width:240px"></option>').attr("value", data.Id).text(data.Name)
                                                        )
                                            });
                                        } else {
                                            $('#vehicleNo').empty();
                                            $('#vehicleNo').append(
                                                    $('<option style="width:240px"></option>').val(0).html('--select--'))
                                        }
                                    }
                                });
                                $('#actionName').empty();
                                var actionOpt = document.trip.actionName;
                                var optionVar = new Option("-select-", '0');
                                actionOpt.options[0] = optionVar;
                                optionVar = new Option("Freeze", '1');
                                actionOpt.options[1] = optionVar;
                                $("#actionDiv").hide();

                                setVehicleTonnage();
                            }
                            function  setVehicleTonnage() {
                                var temp = "";

                                var vehicleTypeTemp = document.getElementById("vehicleTypeName").value;

                            }


                            function resetAll() {
                                // alert("reset ...");
                                //document.getElementById("vehicleTypeName").value = "0~0";
                                document.getElementById("tripStatus").value = "20";
                                document.getElementById("vehicleNo").value = "0";

                                var selectedIndex = document.getElementsByName("selectedIndex");
                                for (var i = 0; i < selectedIndex.length; i++) {
                                    //    document.getElementById("selectedIndex" + (i + 1)).checked = false;
                                }


                                var actionOpt = document.trip.actionName;
                                var optionVar = new Option("-select-", '0');
                                actionOpt.options[0] = optionVar;
                                optionVar = new Option("Cancel Order", 'Cancel');
                                actionOpt.options[1] = optionVar;
                                optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                                actionOpt.options[2] = optionVar;
                                optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                                actionOpt.options[3] = optionVar;
                                $("#actionDiv").show();
                            }
                        </script>
                        <div id="tripDetail">
                            <!--<table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">-->
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="6" >RunSheet Details</th>
                                    </tr>
                                <thead>
                                    <tr >
                                        <td >Ownership</td>
                                        <td >
                                            <select name="ownership" id="ownership"  class="form-control" style="width:240px;height:40px" >
                                                <option value='0' >--select--</option>
                                                <option value='1' > Dedicated </option>
                                                <option value='2' > Hire </option>
                                            </select>
                                        </td>

                                        <td ><font color="red">*</font>Transporter</td>
                                        <td >
                                            <select name="vendor" id="vendor" onchange="setVehicleType(this.value);
                                                    resetAll();
                                                    " class="form-control" style="width:240px;height:35px;">

                                                <c:if test="${vendorList != null}">
                                                    <option value="0" selected>--select--</option>
                                                    <!--<option value="1~0" >Own</option>-->
                                                    <c:forEach items="${vendorList}" var="veh">
                                                        <option value='<c:out value="${veh.vendorId}"/>'><c:out value="${veh.vendorName}"/></option>
                                                    </c:forEach>
                                                </c:if>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr>

                                <input type="hidden" name="totalWeight" Id="totalWeight"  class="form-control" style="width:240px;height:40px"  value='<c:out value="${totalWeight}" />' readonly >
                                <input type="hidden" name="vehicleType" Id="vehicleType"  class="form-control" style="width:240px;height:40px"  value='<c:out value="${vehicleType}" />'>



                                <td ><font color="red">*</font>Vehicle Type</td>
                                <td >
                                    <select name="vehicleTypeName" id="vehicleTypeName" onchange="resetAll();
                                            setVehicle();
                                            "
                                            class="form-control" style="width:240px;height:35px;">

                                    </select>
                                    <input type="hidden" name="vehicleTypeIdSelected" Id="vehicleTypeIdSelected" class="form-control" value=''/>
                                </td>
                                <SCRIPT language=Javascript>

                                    function isNumberKey(evt)
                                    {
                                        var charCode = (evt.which) ? evt.which : evt.keyCode;
                                        if (charCode != 46 && charCode > 31
                                                && (charCode < 48 || charCode > 57))
                                            return false;

                                        return true;
                                    }

                                </SCRIPT>
                                <!--<input type="hidden" name="ownership" id="ownership" class="form-control" value="0">-->
                                <td style="display:none" id="veh">Vehicle No</td>
                                <td style="display:none" id="veh1">
                                    <select  name="vehicleNo" id="vehicleNo"  class="form-control" style="width:240px;height:40px;display:none"  >

                                    </select>
                                    <script>
                                        $('#vehicleNo').select2({placeholder: 'Fill vehicleNo'});
                                    </script>
                                </td>
                                <td id="hireVeh1" style="display:none">
                                    <input type="text" id="hireVehicleNo" name="hireVehicleNo" class="form-control" style="width:240px;height:40px;" value="Hired" onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9]/g, '')" maxlength="10"/>
                                </td>
                                </tr>   
                                <input type="hidden" name="vehicleId" Id="vehicleId"  class="form-control"   value="0">

                                <tr>
                                    <td >
                                        <font color="red">*</font>Delivery Executive
                                    </td>
                                    <td >
                                        <select name="deliveryExecutiveId" id="deliveryExecutiveId" class=" form-control" style="width:240px;height:35px;"  >
                                            <c:if test="${deliveryExecutiveList != null}">
                                                <option value="0">--Select--</option>
                                                <c:forEach items="${deliveryExecutiveList}" var="del">
                                                    <option value='<c:out value="${del.empId}"/>'><c:out value="${del.empName}"/></option>
                                                </c:forEach>
                                            </c:if>

                                        </select>
                                    </td>
                                    <td >
                                    </td>
                                    <td >

                                        <input type="hidden" name="deliveryHelperId" Id="deliveryHelperId" class="form-control" style="width:240px;height:35px;"  value="0">
                                    </td>
                                </tr>
                                <tr>
                                    <td > Driver Name </td>
                                    <td >
                                        <input type="text" name="drivName" Id="drivName" class="form-control" style="width:240px;height:35px;"  value="">
                                        <input type="hidden" name="drivId" Id="drivId" class="form-control" style="width:240px;height:35px;"  value="0">
                                    </td>                                   
                                    <td > Driver Mobile </td>
                                    <td ><input type="text" name="drivMobile" Id="drivMobile" onKeyPress="return onKeyPressBlockCharacters(event);" class="form-control" style="width:240px;height:35px;"  value="">
                                    </td>                                   
                                </tr>
                                <tr>
                                    <td>Special Instruction</td>
                                    <td><textarea name="tripRemarks" cols="20" rows="2"  class="form-control" style="width:240px;height:40px" > <c:out value="${consginmentRemarks}" /></textarea></td>
                                    <td></td>
                                    <td><input type="hidden" name="agreedRate" Id="agreedRate" readonly  class="form-control" style="width:240px;height:35px;"  value="0"/>
                                        <input type="hidden" name="lhcDate" Id="lhcDate" readonly  class="form-control " style="width:240px;height:40px"  value=""/>
                                    </td>                                   
                                </tr>


                                <tr>
                                    <td>
                                        Start KM
                                    </td>
                                    <td >
                                        <input type="text" name="startKm" Id="startKm" onKeyPress="return onKeyPressBlockCharacters(event);"  class="form-control" style="width:240px;height:35px;"  value="">

                                </tr>
                                <tr>
                                <script>
                                    function setDriverIdName(val) {
                                        if (val == 2) {
                                            document.getElementById("driver1Name").value = "";
                                            document.getElementById("driver1Id").value = "0";
                                            $("#vehOwn").hide();
                                            $("#vehHire").show();
                                        } else {
                                            setVehicleValues();
                                        }

                                    }
                                </script>
                                </tr>
                                <tr style="display:none">

                                    <td style="display:none">Trip Status</td>
                                    <td style="display:none">
                                        <select name="tripStatus" id="tripStatus" onchange="setVehicle();"  class="form-control" style="width:240px;height:40px"  disabled>
                                            <c:if test="${statusList != null}">
                                                <c:forEach items="${statusList}" var="status">
                                                    <option value='<c:out value="${status.statusId}"/>' ><c:out value="${status.statusName}"/></option>
                                                </c:forEach>
                                                <!--                                            <option value="0" selected >All Vehicle</option>-->
                                            </c:if>
                                        </select>

                                    </td>
                                </tr>

                            </table>
                            <br/>
                            <br/>

                            <center>
                                <font size="2" color="green"><b> 
                                </b></font>
                                <br/>
                            </center>
                        </div>

                        <div id="orderDetail">
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr height="40">
                                        <th class="contenthead" colspan="12">Order Details</th>
                                    </tr>
                                <thead>
                                    <c:if test = "${planningList != null}" >

                                    <thead>
                                        <tr height="40">
                                            <th>S.No</th>                                    
                                            <th>Customer</th>                                    
                                            <th>Order No</th>                                                                 
                                            <th>Product Name</th>                                    
                                            <th>Pincode</th>                                    
                                            <th>City</th>                                    
                                            <th>Unit Price</th>
                                            <th>Value</th>
                                            <th>Qty</th>                                    
                                            <th>Filled Qty</th>
                                            <th>Scan Serial No</th>
                                            <th>Select All <input type="checkbox" class="" name="all" id="all" onclick="checkAll(this)"></th>
                                        </tr>
                                    </thead>
                                    <% int index = 0;
                                       int sno = 1;
                                    %>
                                    <tbody>
                                        <c:forEach items="${planningList}" var="cnl">
                                            <tr height="30">
                                                <td align="left" ><%=sno%></td>
                                        <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                        <input type="hidden" style="width: 184px;" name="orderId" id="orderId<%=index%>" class="form-control" style="width:250px;height:40px"   value="<c:out value="${cnl.orderId}"/>"  readOnly maxlength="50">
                                        <td align="left" ><c:out value="${cnl.customerName}"/></td>
                                        <td align="left" ><c:out value="${cnl.orderNo}"/></td>                                           
                                        <td align="left" ><c:out value="${cnl.productName}"/></td>                                        
                                        <td align="left" ><c:out value="${cnl.pincode}"/></td>                                        
                                        <td align="left" ><c:out value="${cnl.city}"/></td>                                        
                                        <td align="left" ><c:out value="${cnl.price}"/></td>                                        
                                        <td align="left" ><c:out value="${cnl.totalAmt}"/>
                                        <td align="left" ><c:out value="${cnl.qty}"/>
                                        <input type="hidden" readonly name="qty" id="qty<%=sno%>" value="<c:out value="${cnl.qty}"/>"/>
                                        <input type="hidden" readonly name="itemId" id="itemId<%=sno%>" value="<c:out value="${cnl.productId}"/>"/>
                                        <input type="hidden" readonly name="orderDetailId" id="orderDetailId<%=sno%>" value="<c:out value="${cnl.orderDetailId}"/>"/>
                                        </td>             
                                        <td align="left" ><input type="text" style="width:130px;height:40px"  class="form-control" readonly name="filledQty" id="filledQty<%=sno%>" value="0"/></td>             
                                        <td align="left" ><input type="text" style="width:180px;height:40px" class="form-control" name="serialNo" id="serialNo<%=sno%>" value="" onchange="checkSerialNo(this.value, '<%=sno%>', '<c:out value="${cnl.orderDetailId}"/>', '<c:out value="${cnl.orderId}"/>', '<c:out value="${cnl.productId}"/>')"/>            
                                        </td>         
                                        <td align="left" ><input type="checkbox" disabled name="selectedIndex" id="selectedIndex<%=sno%>" value="<%=sno%>"  onclick="checkSelectStatus(<%=sno%>, this);" />
                                            <input type="hidden" name="selectedStatus" id="selectedStatus<%=sno%>" value="0" />
                                        </td>
                                        </tr>
                                        <%index++;%>
                                        <%sno++;%>
                                    </c:forEach>
                                    <input type="hidden" name="ser" id="ser" value="<%=sno%>">
                                    </tbody>

                                </table>
                                <script>

                                    var arr1 = [];
                                    function checkSelectStatus(sno, obj) {
                                        //                            alert(sno)
                                        var val = document.getElementsByName("selectedStatus");
                                        if (obj.checked == true) {
                                            document.getElementById("selectedStatus" + sno).value = 1;
                                        } else if (obj.checked == false) {
                                            document.getElementById("selectedStatus" + sno).value = 0;
                                        }
                                        var vals = 0;
                                        for (var i = 0; i <= val.length; i++) {
                                        }
                                    }

                                    function checkAll(element) {
                                        var checkboxes = document.getElementsByName('selectedIndex');
                                        if (element.checked == true) {
                                            for (var x = 0; x < checkboxes.length; x++) {
                                                checkboxes[x].checked = true;
                                                $('#selectedStatus' + (x + 1)).val('1');
                                            }
                                        } else {
                                            for (var x = 0; x < checkboxes.length; x++) {
                                                checkboxes[x].checked = false;
                                                $('#selectedStatus' + (x + 1)).val('0');
                                            }
                                        }
                                    }
                                    function checkSerialNo(serialNo, sno, orderDetailId, orderId, productId) {
                                        var sno1 = $('#tempTable').find('tr').length;
                                        var sno1 = parseInt(sno1) + 1;
                                        var qty = document.getElementById("qty" + sno).value;
                                        var filledQty = document.getElementById("filledQty" + sno).value;
                                        var serialId = 1;
                                        $.ajax({
                                            url: "/throttle/rpSelectSerialId.do",
                                            dataType: "json",
                                            data: {
                                                serialNo: serialNo,
                                                productId: productId
                                            },
                                            success: function(data) {
                                                serialId = data;
                                                if (arr1.indexOf(serialNo) > -1) {
                                                    alert("Already Scanned");
                                                    document.getElementById("serialNo" + sno).value = "";
                                                } else if (serialId == 0) {
                                                    alert("Serial Number Not Exists");
                                                    document.getElementById("serialNo" + sno).value = "";
                                                } else if (parseInt(filledQty) < parseInt(qty)) {
                                                    arr1.push(serialNo);
                                                    $("#tempTable").append("<tr><td>" + sno1 + "</td><td><input type='text' class='form-control' id='scannedSerial+" + sno + "' name='scannedSerial' value='" + serialNo 
                                                            + "'/></td><td><input type='text' class='form-control' name='productId' id='productId" + sno + "' value='" + productId 
                                                            + "'><input type='hidden' name='orderDetailIds' id='orderDetailIds" + sno + "' value='" + orderDetailId 
                                                            + "'><input type='hidden' name='orderIds' id='orderIds" + sno + "' value='" + orderId 
                                                            + "'></td><input type='hidden' name='serialId' id='serialId" + sno + "' value='" + serialId 
                                                            + "'></tr>");
                                                    document.getElementById("serialNo" + sno).value = "";
                                                    document.getElementById("filledQty" + sno).value = parseInt(document.getElementById("filledQty" + sno).value) + 1;
                                                    document.getElementById("selectedIndex" + sno).checked = true;
                                                } else {
                                                    alert("Completely Filled");
                                                    document.getElementById("serialNo" + sno).value = "";
                                                }
                                            }

                                        })

                                    }
                                </script>

                            </c:if>
                            <div id="tempDiv" style="display:none">
                                <table class="table table-info mb30 table-hover">
                                    <thead>
                                        <tr>
                                            <th>S. No</th>
                                            <th>Serial No</th>
                                            <th>Product Id</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tempTable">

                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <table class="table table-info mb30 table-hover"  >
                                <tr>
                                    <td >Action: </td>
                                    <td  >
                                        <select name="actionName" id="actionName" onChange="setButtons();" class="form-control" style="width:150px;height:40px">
                                            <option value="0">-select-</option>
                                            <option value="Cancel">Cancel Order</option>
                                            <option value="Suggest Schedule Change">Suggest Schedule Change</option>
                                            <option value="Hold Order for further Processing">Hold Order for further Processing</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td  >Remarks</td>
                                    <td  ><textarea name="actionRemarks" rows="3" cols="100" class="form-control" style="width:240px;height:40px"></textarea> </td>
                                </tr>
                            </table>
                            <br>
                            <center>
                                <input type="button" class="btn btn-success" value="Generate Runsheet" style="width:200px;" id="save" name="Save" onclick="submitPage();" />
                            </center>
                        </div>                


                        <script>
                            $('.btnNext').click(function() {
                                $('.nav-tabs > .active').next('li').find('a').trigger('click');
                            });
                            $('.btnPrevious').click(function() {
                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                            });
                        </script>

                        <!--sivanesa-->
                    </div>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>