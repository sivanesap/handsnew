<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datep    icker.js"></script>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

        <script type="text/javascript">
            function submitPage(itemId,whId,grnId) {                
                document.accountReceivable.action = '/throttle/viewStockQtyDetails.do?itemId=' + itemId + '&whId=' + whId + '&grnId=' + grnId + '&param=ExportExcel';
                document.accountReceivable.submit();
            }
        </script>
    </head>
    
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Serial Number Details </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.WMS Report" text="WMS Report"/></a></li>
                <li class="">Serial Number Details</li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body>
                    <form name="accountReceivable"   method="post">
                        <%@ include file="/content/common/message.jsp" %>
                        <table class="table table-info mb10 table-hover" id="bg" >
                            <thead>
                                <tr>
                                    <th colspan="2" height="30" >Serial Number Details</th>
                                </tr>
                            </thead>
                        </table>
                            

                            <table class="table table-info mb30 table-hover" id="table" >
                                <thead>
                                    <tr>
                                        <th align="center">S.No</th>
                                        <th align="center">Item Name</th>
                                        <th align="center">Item Code</th>
                                        <th align="center">Serial No</th>
                                        
                                    </tr> 
                                </thead>
                                <tbody>
                                <% int index = 1;%>
                                    <c:forEach items="${viewSerialNumberDetails}" var="stock">
                                        <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                          %>
                                        <tr>
                                            <td><%=index++%></td>
                                            <td align="ceneter"><c:out value="${stock.itemName}"/></td>
                                            <td align="ceneter"><c:out value="${stock.skuCode}"/></td>
                                            <td align="ceneter"><c:out value="${stock.serialNo}"/>
                                            </td>
                                            
                                           
                                        </tr>

                                    </c:forEach>
                                </tbody>
                            </table>
<!--                            <center>
                                <input type="button" class="btn btn-success" name="ExportExcel"   value="Export Excel" onclick="submitPage('<c:out value='${itemId}'/>','<c:out value='${whId}'/>','<c:out value='${grnId}'/>');">
                            </center>-->
                    </form>
                </body>    
            </div>
        </div>
    </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>