
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>
<script type="text/javascript">
    function searchPage() {
        document.ifb.action = '/throttle/ifbOrderRelease.do';
        document.ifb.submit();
    }
    function excelPage() {
        document.ifb.action = '/throttle/ifbOrderRelease.do?&param=excel';
        document.ifb.submit();
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="IFB Order Release"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="wms"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="IFB Order Release"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >
                <form name="ifb"  method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <div id="ptp" style="overflow: auto">
                        <div class="inpad">
                            <table class="table table-info mb30 table-hover" style="width:100%">
                                <thead><tr><th colspan="7">IFB Order Release </th></tr></thead>
                                <tr>
                                    <td>From Date</td>
                                    <td><input type="text" class="datepicker form-control" autocomplete="off" name="fromDate" id="fromDate" style="width:200px;height:40px"/></td>
                                    <td>To Date</td>
                                    <td><input type="text" class="datepicker form-control" autocomplete="off" name="toDate" id="toDate" style="width:200px;height:40px"/></td>
                                </tr>
                                <tr>
                                    <td  ><font color="red">*</font>Rejection Reason</td>
                                    <td>
                                        <select class="form-control" id="rejectionId" name="rejectionId" style="width:200px;height: 40px">
                                            <option value="">------Select Reason------</option>
                                            <c:forEach items="${rejectionMaster}" var="rm">
                                                <option value="<c:out value="${rm.rejectionId}"/>"><c:out value="${rm.reason}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <script>
                                document.getElementById('rejectionId').value = '<c:out value="${rejectionId}"/>'
                            </script>
                            <center>
                                <input type="button" class="btn btn-success"   value="Search" onclick="searchPage()">&emsp;&emsp;
                                <input type="button" class="btn btn-success"   value="Export Excel" onclick="excelPage()">
                            </center>

                            <div id="ptp" style="overflow: auto">
                                <div class="inpad">
                                    <table class="table table-info mb30 table-hover" id="table" >

                                        <thead>
                                            <tr height="40">
                                                <th  height="30" >S.No</th>
                                                <th  height="30" >Invoice</th>
                                                <th  height="30" >Item Name</th>
                                                <th  height="30" >Serial No</th>
                                                <th  height="30" >Customer Name</th>
                                                <th  height="30" >City</th>
                                                <th  height="30" >Pincode</th>
                                                <th  height="30" >Quantity</th>
                                                <th  height="30" >Reason for Rejection</th>
                                                <th  height="30" >Amount</th>
                                                <th  height="30" >Action</th>
                                            </tr>
                                        </thead>
                                        <% int index = 0;
                                            int sno = 1;
                                        %>
                                        <tbody>

                                            <c:if test = "${ifbOrderRelease != null}" >
                                                <c:forEach items= "${ifbOrderRelease}" var="ifb">
                                                    <tr height="30">
                                                        <td align="left"><%=sno%></td>
                                                        <td align="left"   ><c:out value="${ifb.invoiceNo}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.itemName}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.serialNo}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.customerName}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.city}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.pincode}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.qty}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.reason}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.grossValue}"/></td>
                                                        <td><a href="ifbOrderRelease.do?orderId=<c:out value="${ifb.orderId}"/>&param=update">
                                                                <br>    <span class="label label-Primary">Release Order</span>
                                                            </a></td> 
                                                    </tr>
                                                    <%
                                                        sno++;
                                                        index++;
                                                    %>
                                                </c:forEach>
                                            <input type="hidden" name="sno" id="sno" value="<%=sno%>">
                                        </c:if>
                                        </tbody>
                                    </table>
                                    <script language="javascript" type="text/javascript">
                                        setFilterGrid("table");
                                    </script>
                                </div>
                            </div>    

                            </form>
                            </body>
                        </div>
                    </div>
        </div>


        <%@ include file="/content/common/NewDesign/settings.jsp" %>