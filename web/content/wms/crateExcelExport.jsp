<%-- 
    Document   : crateExcelExport
    Created on : 24 Jan, 2022, 9:09:11 AM
    Author     : alan
--%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Excel";
        request.setAttribute("menuPath", menuPath);
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "crateInventoryExportExcel-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <table class="table table-info mb30 table-bordered" id="table" class="sortable" width="100%">
              
                <thead>
                    <tr>
                        <th>SNo</th>
                        <th>Bar Code</th>
                        <th>Crate Code</th>
                        <th>Start Time</th>
                        <th>Date</th>
                        <th>Status</th>
                    </tr>
                </thead>
                
                <tbody>
                    
                    <% int index = 1;%>
                    <c:forEach items="${getTempCurrentDataCrate}" var="dc">
                        <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                        %>
                        <tr >
                            <td class="<%=classText%>"  align="center"><%=index%></td>
                            <td class="<%=classText%>"  align="center"><c:out value="${dc.barCode}"/></td>
                            <td class="<%=classText%>"  align="center"><c:out value="${dc.crateCode}"/></td>
                       

                            <td class="<%=classText%>"  align="center"><c:out value="${dc.startTime}"/></td>
                            <td class="<%=classText%>"  align="center"><c:out value="${dc.date}"/></td>
                            <c:if test="${dc.status=='1'}">
                                <td class="<%=classText%>"  align="center">Outward</td>
                            </c:if>
                            <c:if test="${dc.status=='2'}">
                                <td class="<%=classText%>"  align="center">inward</td>
                            </c:if>
                            <c:if test="${dc.status=='3'}">
                                <td class="<%=classText%>"  align="center">Available</td>
                            </c:if>
                            <c:if test="${dc.status=='0'}">
                                <td class="<%=classText%>"  align="center">Not Map</td>
                            </c:if>

                          

                            <%index++;%>

                        </c:forEach>

                </tbody>
            </table>


            <br>
            <br>

        </form>
    </body>
</html>