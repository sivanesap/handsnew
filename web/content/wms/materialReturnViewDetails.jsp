<%-- 
    Document   : materialReturnViewDetails
    Created on : 19 Mar, 2022, 10:12:16 AM
    Author     : mahendiran
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Material Return View</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html">InstaMart</a></li>
                <li class="">Material Return View</li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="material"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>

                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30" style="color: white">
                                    <th>S.No</th>
                                    <th>Material Name</th>
                                    <th>Material Code</th>
                                    <th>Material Description</th>
                                    <th>Returned Qty</th>
                                    <th>Floor Qty</th>
                                    <th>Used Qty</th>
                                    <th>Excess</th>
                                    <th>Shortage</th>
                                    <th>UOM</th>
                                    <th>Bin Location</th>
                                    <th>Remarks</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                            <c:if test = "${issueDetails != null}">
                                <c:forEach items="${issueDetails}" var="pc">
                                    <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>

                                    <tr>
                                        <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                        <td class="<%=className%>"  align="left"> <c:out value="${pc.material}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.materialCode}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.materialDescription}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.qty}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.floorQty}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.usedQty}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.excess}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.shortage}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.uom2}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.binLocation}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.remarks}" /></td>
                                    <td class="<%=className%>"  align="left"> 
                                        <c:if test="${pc.status=='0'}" >
                                            Pending
                                        </c:if>
                                        <c:if test="${pc.status=='1'}" >
                                            Closed
                                        </c:if>
                                    </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </c:if>
                        </table>


                        <input type="hidden" name="count" id="count" value="<%=sno%>" />

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                                        setFilterGrid("table");
                        </script>

                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
