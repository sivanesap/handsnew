
<%-- 
    Document   : dzProductUploadView
    Created on : 10 Dec, 2021, 4:38:08 PM
    Author     : alan
--%>




<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML
    
    4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript"></script>

    <script>
        function uploadPage() {
            document.upload.action = "/throttle/dzProductUpload.do?param=upload";
            document.upload.submit();

        }

        function insertTo() {


            var result = confirm("Some errors are not Updated. Do you want to continue?");
            if (result == true) {
                document.upload.action = "/throttle/dzInsertIntoProductMaster.do?";
                document.upload.method = "post";
                document.upload.submit();
            }
            else {

                document.upload.action = "/throttle/dzInsertIntoProductMaster.do?";
                document.upload.method = "post";
                document.upload.submit();
            }
        }


//         function puvView(productVariantId){
//             
//                        window.open('/throttle/dzProductUploadViewEdit.do?param=view&productVariantId=' + productVariantId, 'PopupPage', 'height = 400, width = 800, scrollbars = yes, resizable = yes');
//                    }
//        

        function editPage() {
            
            document.getElementById("productView").style.display = "block";
            
            var productVariantId = document.getElementById("productVariantId").value;
            var productName = document.getElementById("productName").value;
            var optionName = document.getElementById("optionName").value;
            var optionValue = document.getElementById("optionValue").value;
            var barCode = document.getElementById("barCode").value;
            var hsnCode = document.getElementById("hsnCode").value;
            var categoryName = document.getElementById("categoryName").value;
            var subCategoryName = document.getElementById("subCategoryName").value;
           
            console.log(productVariantId, productName, optionName, optionValue, barCode, hsnCode, categoryName, subCategoryName);
                
                document.puvUpload.action = "/throttle/dzProductUploadView.do?param=save";
                document.puvUpload.submit();
        }



        function puvView(sno, productVariantId, productName, optionName, optionValue, barCode, hsnCode, categoryName, subCategoryName,productId1) {
                    console.log("Product Id ==> " ,productId1)
                    
            document.getElementById("productView").style.display = "block";
             document.getElementById("productVariantId").focus();
            document.getElementById("productVariantId").value = productVariantId;
            document.getElementById("productName").value = productName;
            document.getElementById("optionName").value = optionName;
            document.getElementById("optionValue").value = optionValue;
            document.getElementById("barCode").value = barCode;
            document.getElementById("hsnCode").value = hsnCode;
            document.getElementById("categoryName").value = categoryName;
            document.getElementById("subCategoryName").value = subCategoryName;
            document.getElementById("productId").value = productId1;
            
            var count = parseInt(document.getElementById("count").value);
            for (i = 1; i <= parseInt(count); i++) {
                if (i != sno) {
                    document.getElementById("edit" + i).checked = false;
                } else {
                    document.getElementById("edit" + i).checked = true;
                }
            }
        }

//        function editPage() {
//        
//        if(productVariantId == 1){
//            document.puvUpload.action = "/throttle/dzProductUploadViewEdit.do?param=save";
//            document.puvUpload.submit();

    </script>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Product Upload View</h2>

        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Shop Tree</a></li>
                <li class="">Product Upload View</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
               
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <form name="puvUpload" method="post">
                        <%@include file = "/content/common/message.jsp"%>
                        <div id="productView" style="display:none;">
                        <table class="table table-info mb30 table-hover">
                            <input type="hidden" name="productId" id="productId" value=""/>
                            <tr>

                                <td>Product Variant Id   &emsp;</td>
                                <td><input type="productVariantId" name="productVariantId" id="productVariantId" class="form-control" style="width:240px;height:40px" value='<c:out value="${productVariantId}" />'></td>

                                <td> Product Name   &emsp;</td>
                                <td><input type="productName" name="productName" id="productName" class="form-control" style="width:240px;height:40px" value=""></td>

                            </tr>

                            <tr>

                                <td> Option Name   &emsp;</td>
                                <td><input type="optionName" name="optionName" class="form-control" id="optionName" style="width:240px;height:40px" value=""></td>

                                <td>Option Value   &emsp;</td>
                                <td><input type="optionValue" name="optionValue" class="form-control" id="optionValue" style="width:240px;height:40px" value=""></td>

                            </tr>

                            <tr>

                                <td> Bar Code   &emsp;</td>
                                <td><input type="barCode" name="barCode" id="barCode" class="form-control" style="width:240px;height:40px" value=""></td>

                                <td> HSN Code  &emsp;</td>
                                <td><input type="hsnCode" name="hsnCode" id="hsnCode" class="form-control" style="width:240px;height:40px" value=""></td>

                            </tr>

                            <tr>
                                <td> Category Name  &emsp;</td>
                                <td><input type="categoryName" name="categoryName" id="categoryName" class="form-control" style="width:240px;height:40px" value=""></td>

                                <td> Sub Category Name  &emsp;</td>
                                <td><input type="subCategoryName" name="subCategoryName" id="subCategoryName" class="form-control" style="width:240px;height:40px" value=""></td>
                            </tr>

                        </table>
                        <center>
                            <input type="button" class="btn btn-success"  name="save" id="save"  value="Save" onclick="editPage();">
                        </center>
                                </div>
                    </form>





                    <table class="table table-info mb30 table-hover" id="uploadTable">



                    </table>
                    <div id="ptp">
                        <div class="inpad" style=" overflow-x: visible;">

                            <c:if test = "${getdzProductUpload != null}" >

                                <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                    <thead style="font-size:12px">
                                        <tr height="40">
                                            <th>Sno</th>
                                            <th>Product Variant Id</th>
                                            <th>Product Name</th>
                                            <th>Option Name</th>
                                            <th>Option Value</th>
                                            <th>Bar code</th>
                                            <th>Hsn Code</th>
                                            <th>Category Name</th>
                                            <th>Sub Category Name</th>
                                            <th>Action</th>




                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                     int count=0;
                                    %> 
                                    <tbody>

                                        <c:forEach items="${getdzProductUpload}" var="pc">


                                            <tr>
                                                <td align="left"> <%=sno%> </td>
                                                <td align="left"> <c:out value="${pc.productVariantId}"/></td>
                                                <td align="left"> <c:out value="${pc.productName}"/> </td>
                                                <td align="left"> <c:out value="${pc.optionName}"/></td>
                                                <td align="left"> <c:out value="${pc.optionValue}"/> </td>
                                                <td align="left"> <c:out value="${pc.barCode}"/> </td>
                                                <td align="left"> <c:out value="${pc.hsnCode}"/> </td>
                                                <td align="left"> <c:out value="${pc.categoryName}"/> </td>
                                                <td align="left"> <c:out value="${pc.subCategoryName}"/> </td>
                                                <td> 
                                                    <input type="button" Value="Edit" style="height: 3%;background-color: skyblue;border-style: hidden;" onclick="puvView(<%=sno%>, '<c:out value="${pc.productVariantId}"/>', '<c:out value="${pc.productName}"/>', '<c:out value="${pc.optionName}"/>', '<c:out value="${pc.optionValue}"/>', '<c:out value="${pc.barCode}"/>', '<c:out value="${pc.hsnCode}"/>', '<c:out value="${pc.categoryName}"/>', '<c:out value="${pc.subCategoryName}"/>', '<c:out value="${pc.productId1}"/>');">
                                                </td>






                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                    </tbody>
                                </table>


                            </c:if>
                        </div>
                    </div>    
                    <script>
                        setFilterGrid('table');
                    </script>

                </form>







                <br>


            </div>
        </div>
    </div>
    <%@include file="/content/common/NewDesign/settings.jsp"%>
</html>