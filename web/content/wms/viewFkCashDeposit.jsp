<%-- 
    Document   : viewFkReconcile
    Created on : 27 May, 2021, 11:34:17 AM
    Author     : Roger
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script>
        function submitPage() {
            document.reconcile.action = "viewCashDeposit.do?&param=save&statusId=14";
            document.reconcile.submit();
        }
        function checkSelected(element, sno) {
            if (element.checked == true) {
                document.getElementById("selectedStatus" + sno).value = 1;
            } else {
                document.getElementById("selectedStatus" + sno).value = 0;
            }
        }
        function checkAll(element) {
            var checkboxes = document.getElementsByName('selectedIndex');
            if (element.checked == true) {
                for (var x = 0; x < checkboxes.length; x++) {
                    checkboxes[x].checked = true;
                    $('#selectedStatus' + (x + 1)).val('1');
                }
            } else {
                for (var x = 0; x < checkboxes.length; x++) {
                    checkboxes[x].checked = false;
                    $('#selectedStatus' + (x + 1)).val('0');
                }
            }
        }

            function setValues() {
               var selected= document.getElementsByName("selectedIndex");
               var deCod = document.getElementsByName("deCod");
               var prexo = document.getElementsByName("prexo");
               var depositAmount=0;
               for(var i=0;i<selected.length;i++){
                   if(selected[i].checked==true){
                       depositAmount=parseFloat(depositAmount)+parseFloat(deCod[i].value)+parseFloat(prexo[i].value);
                   }
               }
               document.getElementById("depositAmount").value=depositAmount.toFixed(2);
            }
    </script>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.RunSheet" text="Flipkart Cash Deposit"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Flipkart" text="Flipkart"/></a></li>
                <li class=""><spring:message code="hrms.label.Reconcile" text="Flipkart Cash Deposit"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">
                    <form name="reconcile" method="post">
                        <%@include file="/content/common/message.jsp" %>
                        <table  class="table table-info mb30 table-hover"  align="left" style="width:100%;">
                            <tr>
                                <td>Total Deposit Amount</td>
                                <td>
                                    <input name="depositAmount" id="depositAmount" type="text" style="width:250px;height:40px" class="form-control" value="">
                                </td>

                                <td>Deposit Type</td>
                                <td>
                                    <select name="depositType" id="depositType" style="width:250px;height:40px" class="form-control" value="">
                                        <option value="0">------Select One------</option>
                                        <option value="1">Cash Deposit</option>
                                        <option value="2">Writer's Pickup</option>
                                    </select>
                                </td>
                                <td colspan="4" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td><font color="red">*</font>From Date </td>
                                <td height="30"><input name="fromDate" id="fromDate" autocomplete="off" type="text"  class="datepicker , form-control" style="width:250px;height:40px"  value=""></td>
                                <td><font color="red">*</font>To Date </td>
                                <td height="30">
                                    <input name="toDate" id="toDate" type="text" autocomplete="off" class="datepicker , form-control" style="width:250px;height:40px" class="datepicker" value="">
                                </td>
                                <td colspan="4" >&nbsp;</td>
                            </tr>
                        </table>
                        <center><br><br><input type="button" class="btn btn-success" value="Update" onclick="submitPage()"><br><br></center>
                        <table class="table table-info mb30 table-bordered" id="table"  style="width:98%;" >
                            <thead>
                                <tr >
                                    <th >Sno</th>                                        
                                    <th >Reconcile Code</th>                                        
                                    <th >Runsheet Id</th>                                        
                                    <th >DE Name</th>
                                    <th >Total Hub Amount</th>
                                    <th >Total DE COD</th>
                                    <th >Total Prexo Amount</th>
                                    <th >Select All <input type="checkbox" class="" name="all" id="all" onclick="checkAll(this);setValues()"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int sno = 0;%>
                                <c:if test = "${reconcileMaster != null}">
                                    <c:forEach items="${reconcileMaster}" var="re">
                                        <%
                                             sno++;
                                             String className = "text1";
                                             if ((sno % 1) == 0) {
                                                 className = "text1";
                                             } else {
                                                 className = "text2";
                                             }
                                        %>

                                        <tr height="30">
                                            <td><%=sno%> </td>                                      
                                            <td><c:out value="${re.reconcileCode}"/></td>                                      
                                            <td><c:out value="${re.runsheetId}"/></td>                                      
                                            <td><c:out value="${re.empName}"/></td>                                                    
                                            <td><c:out value="${re.totalCod}"/></td>                                      
                                            <td><c:out value="${re.totalDeCod}"/></td>                                      
                                            <td><c:out value="${re.totalPrexoAmount}"/></td>  
                                            <td  align="left">
                                                <input type="hidden" name="reconcileId" id="reconcileId<%=sno%>" value="<c:out value="${re.reconcileId}"/>">
                                                <input type="hidden" name="deCod" id="deCod<%=sno%>" value="<c:out value="${re.totalDeCod}"/>">
                                                <input type="hidden" name="prexo" id="prexo<%=sno%>" value="<c:out value="${re.totalPrexoAmount}"/>">
                                                <input type="hidden" name="selectedStatus" id="selectedStatus<%=sno%>" value="0">
                                                <input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno%>" onclick="setValues();checkSelected(this, '<%= sno%>');" />
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                                <input type="hidden" name="count" id="count" value="<%= sno%>" />
                            </c:if>
                        </table>

                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>