<%-- 
    Document   : cycleCountAssign
    Created on : 7 Jul, 2021, 12:51:42 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>
<script>
    function searchPage() {
        document.upload.action = "/throttle/cycleCountAssign.do?param=search";
        document.upload.submit();
    }
    function submitPage(val) {
        document.upload.action = "/throttle/cycleCountAssign.do?param=" + val;
        document.upload.submit();
    }
    function setEmpList(val) {
        if (val != "") {
            var empName = document.getElementsByName("empNameTemp");
            var empid = document.getElementsByName("empIdTemp");
            var whId = document.getElementsByName("whIdTemp");
            var elem = document.getElementById("empId");
            while (elem.length > 0) {
                elem.remove(0);
            }
            var ko = document.createElement('option');
            ko.value = "";
            ko.innerHTML = "------Select One------";
            elem.appendChild(ko);
            for (var i = 0; i < empName.length; i++) {
                if (whId[i].value == val) {
                    var opt = document.createElement('option');
                    opt.value = empid[i].value;
                    opt.innerHTML = empName[i].value;
                    elem.appendChild(opt);
                }
            }
        } else {
            var empName = document.getElementsByName("empNameTemp");
            var empid = document.getElementsByName("empIdTemp");
            var whId = document.getElementsByName("whIdTemp");
            var elem = document.getElementById("empId");
            while (elem.length > 0) {
                elem.remove(0);
            }
            var ko = document.createElement('option');
            ko.value = "";
            ko.innerHTML = "------Select One------";
            elem.appendChild(ko);
            for (var i = 0; i < empName.length; i++) {
                var opt = document.createElement('option');
                opt.value = empid[i].value;
                opt.innerHTML = empName[i].value;
                elem.appendChild(opt);
            }
        }
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="Cycle Count Assign"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Wms Master" text="Wms Master"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="Cycle Count Assign"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >
                <form name="upload" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb30">
                        <tbody>
                            <tr>
                                <td>Select Customer</td>
                                <td>
                                    <select id="custId" name="custId" class="form-control" style="width:200px;height:40px">
                                        <option value="">------Select One------</option>
                                        <c:forEach items="${custList}" var="cust">
                                            <option value="<c:out value="${cust.custId}"/>"><c:out value="${cust.custName}"/></option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <td>Select Warehouse</td>
                                <td>
                                    <select id="whId" name="whId" class="form-control" onchange="setEmpList(this.value);" style="width:200px;height:40px;">
                                        <option value="">------Select Name------</option>
                                        <c:forEach items="${whList}" var="wh">
                                            <option value="<c:out value="${wh.whId}"/>"><c:out value="${wh.whName}"/></option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <%
                               int cnt=0;
                            %>
                            <c:forEach items="${empList}" var="em">
                            <input type="hidden" name="empNameTemp" id="empNameTemp<%=cnt%>" value="<c:out value="${em.empName}"/>"/>
                            <input type="hidden" name="empIdTemp" id="empIdTemp<%=cnt%>" value="<c:out value="${em.empId}"/>"/>
                            <input type="hidden" name="whIdTemp" id="whIdTemp<%=cnt%>" value="<c:out value="${em.whId}"/>"/>
                            <%cnt++;%>
                        </c:forEach>
                        <tr>
                            <td>Select Employee</td>
                            <td>
                                <select id="empId" name="empId" class="form-control" style="width:200px;height:40px;">
                                    <option value="">------Select Name------</option>
                                    <c:forEach items="${empList}" var="cust">
                                        <option value="<c:out value="${cust.empId}"/>"><c:out value="${cust.empName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>Select Day</td>
                            <td>
                                <select id="weekDay" name="weekDay" class="form-control" style="width:200px;height:40px">
                                    <option value="">------Select a Day------</option>
                                    <option value="0">Monday</option>
                                    <option value="1">Tuesday</option>
                                    <option value="2">Wednesday</option>
                                    <option value="3">Thursday</option>
                                    <option value="4">Friday</option>
                                    <option value="5">Saturday</option>
                                    <option value="6">Sunday</option>
                                </select>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>

                    <script>
                        document.getElementById("custId").value = '<c:out value="${custId}"/>'
                        document.getElementById("empId").value = '<c:out value="${empId}"/>'
                        document.getElementById("weekDay").value = '<c:out value="${weekDay}"/>'
                    </script>
                    <center><input type="button" class="btn btn-success" name="search" id="search" value="Search" onclick="searchPage();"></center>
                    <br>
                    <br>
                    <c:if test = "${productTypeList != null}">
                        <table class="table table-info mb30 table-hover" id="table" style="width:600px">	
                            <thead>

                                <tr>
                                    <th>S.No</th>
                                    <th>Product Type</th>
                                    <th>Select All <input type="checkbox" name="selectAll" id="selectAll" style="width:15px;height:12px;" onclick="checkAll();"/></th>
                                </tr>
                            </thead>
                            <tbody>

                                <% int sno = 0;%>
                                <c:forEach items="${productTypeList}" var="pc">
                                    <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>

                                    <tr>
                                        <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                        <td class="<%=className%>"  align="left"> <c:out value="${pc.productTypeName}"/> </td>
                                        <td class="<%=className%>"  align="left"> 
                                            <input type="checkbox" name="checkBox" id="selectedIndex<%=sno%>" name="selectedIndex" onchange="checkSelectStatus('<%=sno%>', this)" value="<c:out value="${pc.productTypeId}"/>"/>
                                            <input type="hidden" name="productTypeId" value="<c:out value="${pc.productTypeId}"/>"/>
                                            <input type="hidden" name="productType" value="<c:out value="${pc.productTypeName}"/>"/>
                                            <input type="hidden" value="0" name="selectedStatus" id="selectedStatus<%=sno%>"/>
                                        </td>
                                    </tr>
                                </c:forEach>

                                <tr><br><td colspan="3" align="center"><br><input type="button" class="btn btn-success" name="save" value="Save" onclick="submitPage('save')"></td></tr>
                            </tbody>
                        </table>
                    </c:if>
                    <c:if test = "${selectedProductType != null}">
                        <table class="table table-info mb30 table-hover" id="table" style="width:600px">	
                            <thead>
                                <tr><th colspan="3">Previously Selected Product Type</th></tr>
                                <tr>
                                    <th>S.No</th>
                                    <th>Product Type</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>

                                <% int sn = 0;%>
                                <c:forEach items="${selectedProductType}" var="pc1">
                                    <%
                                        sn++;
                                    %>

                                    <tr>
                                        <td align="left"> <%= sn%> </td>
                                        <td align="left"> <c:out value="${pc1.productTypeName}"/> </td>
                                        <td align="left">
                                            <select id="activeInds<%=sn%>" name="activeInds" value="" style="width:180px;height:40px" >
                                                <option value="Y">Active</option>
                                                <option value="N">In-Active</option>
                                            </select>
                                            <script>document.getElementById("activeInds<%=sn%>").value = '<c:out value="${pc1.activeType}"/>'</script>
                                            <input type="hidden" name="cycleIds" id="cycleIds<%=sn%>" value="<c:out value="${pc1.cycleId}"/>">
                                        </td>
                                    </tr>
                                </c:forEach>

                                <tr><br><td colspan="3" align="center"><br><input type="button" class="btn btn-success" name="save" value="Save" onclick="submitPage('save')"></td></tr>
                            </tbody>
                        </table>
                    </c:if>
                    <script>

                        function checkSelectStatus(sno, obj) {
//                            alert(sno)
                            var val = document.getElementsByName("selectedStatus");
                            if (obj.checked == true) {
                                document.getElementById("selectedStatus" + sno).value = 1;
                            } else if (obj.checked == false) {
                                document.getElementById("selectedStatus" + sno).value = 0;
                            }
                            var vals = 0;
                            for (var i = 0; i <= val.length; i++) {
                            }
                        }
                        function checkAll() {
                            var inputs = document.getElementsByName("selectedIndex");
                            for (var i = 0; i < inputs.length; i++) {
                                if (inputs[i].type == "checkbox") {
                                    if (inputs[i].checked == true) {
                                        inputs[i].checked = false;
                                        document.getElementById("selectedStatus" + (i + 1)).value = 0;
                                    } else if (inputs[i].checked == false) {
                                        inputs[i].checked = true;
                                        document.getElementById("selectedStatus" + (i + 1)).value = 1;
                                    }
                                }
                            }
                        }
                    </script>
                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>


