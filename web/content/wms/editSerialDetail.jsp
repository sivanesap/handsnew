<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.DockIn Update"  text="DockIn Update"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.DockIn Update"  text="DockIn Update"/></a></li>
            <li class="active">DockIn Update</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="showTables(<c:out value="${size}"/>,<c:out value="${itemId}"/>,<c:out value="${asnId}"/>);">
                <form name="gtn" id="gtn" method="post">
                    <table class="table table-info mb30 table-bordered" style="display:none" id="table1" >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>Customer Name</th>
                                <th>Product </th>
                                <th>Product Code</th>
                                <th>GateIn Qty</th>
                                <th>HSN Code</th>
                            </tr>
                        </thead>
                            <tbody>
                                <%
                                int s=1;
                                %>
                                <c:if test="${proListDet!=null}">
                                    <c:forEach items="${proListDet}" var="pd">
                                    <tr >
                                        
                                        <td id="sn"><%=s%></td>
                                        <td id="custName1"><c:out value="${pd.custName}"/></td>
                                        <td id="prodName"><c:out value="${pd.itemName}"/></td>
                                        <td id="prodCode"><c:out value="${pd.skuCode}"/></td>
                                        <td id="Qty"><c:out value="${pd.poQuantity}"/></td>
                                        <td id="hsnCode"><c:out value="${pd.hsnCode}"/></td>
                                    <input type="hidden" value="<c:out value="${pd.poQuantity}"/>" id="poQuantity" name="poQuantity">
                                    <input type="hidden" value="<c:out value="${pd.poQuantity}"/>" id="reqQuantity" name="reqQuantity">
                                    <input type="hidden" value="<c:out value="${pd.asnId}"/>" id="asnId" name="asnId">
                                    <input type="hidden" value="<c:out value="${pd.itemId}"/>" id="itemId1" name="itemId1">
                                    <input type="hidden" value="<c:out value="${pd.gtnDetId}"/>" id="gtnDetId" name="gtnDetId">
                                    <input type="hidden" value="<c:out value="${pd.custId}"/>" id="custId" name="custId">
                                    <input type="hidden" value="<c:out value="${pd.grnId1}"/>" id="grnId1" name="grnId1">
                                        <%s++;%>
                                    </tr>
                                    </c:forEach>
                                    </c:if>
                            </tbody>
            
                    </table>  
                    <table class="table table-info mb30 table-hover" id="serial">
                            <div align="center" style="height:20px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>
                        <c:if test="${proList != null}">
                            
                                <tr>
                                    <td align="center">Select Product</td>
                                <td><select id="proDet" name="proDet" onchange="setVal(this.value);" style="height:40px;width:250px">
                                  <option selected value="">---Select One ---</option>    <c:forEach items="${proList}" var="gtnDet">
                                      <option value="<c:out value="${gtnDet.itemId}"/>-<c:out value="${gtnDet.asnId}"/>"><c:out value="${gtnDet.itemName}"/></option>
                            </c:forEach>
                                </select></td>
                                <input type="hidden" name="itemId" id="itemId" value=""/>
                                <input type="hidden" name="asnId" id="asnId" value=""/>
                                <input type="hidden" name="gtnId" id="gtnId" value="<c:out value="${gtnId}"/>">
                                <td><input type="button" class="btn btn-success" name="Search" value="Search" onclick="getSerialDet();" style="width:100px;height:35px;"></td>
                                </tr>     
                        </c:if>
                         </tr>
                    </table> 
                    <table class="table table-info mb30 table-bordered" id="table2" style="display:none" >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>Serial No</th>
                                <th>Product Condition</th>
                                <th>UOM</th>
                                <th>Bin</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                    int snoo = 1;
                            %>
                            <c:forEach items="${serialDet}" var="sdet">
                                    <tr >
                                        <td id="sn"><%=snoo%>
                                        </td>
                                        <td id="serialNos"><c:out value="${sdet.serials}"/></td>
                                        <c:if test="${sdet.proCons==1}"><td id="proCons">Good</td></c:if>
                                        <c:if test="${sdet.proCons==2}"><td id="proCons">Bad</td></c:if>
                                        <c:if test="${sdet.proCons==3}"><td id="proCons">Excess</td></c:if>
                                        <c:if test="${sdet.uoms==1}"><td id="uoms">Pack</td></c:if>
                                        <c:if test="${sdet.uoms==2}"><td id="uoms">Piece</td></c:if>
                                        <td id="binids"><c:out value="${sdet.bins}"/></td>
                                        <td id="edit"><input type="button" class="btn btn-success" value="Delete"  name="<c:out value="${sdet.tempIds}"/>" onclick="deleteSerial(this.name);"></td>
                                    </tr>
                                    <%
                                    snoo++;
                                    %>
                            </c:forEach>
                            </tbody>
            
                    </table>
                    <table class="table table-info mb30 table-hover">
                            <div align="center" style="height:20px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>
                        <tr>
                           <td align="center">
                                User
                             </td>
                             <td>
                                 
                             <input type="hidden" name="serialSize" id="serialSize" value="<c:out value="${size}"/>"/>
                                 <select id="empUserId" name="empUserId" style="width:250px;height:40px">
                             <c:forEach items="${userDetails}" var="emps">
                                     <option value="<c:out value="${emps.empUserId}"/>"><c:out value="${emps.empName}"/></option>
                             </c:forEach>
                                     </select>
                             </td>
                       <td>Bin</td>
                       <td><select id="binId" name="binId" style="width:250px;height:40px"class="form-control">
                             <option value="0">---Select---</option>
                             <c:forEach items="${binDetails}" var="bin">
                                 <option value="<c:out value="${bin.binId}"/>"><c:out value="${bin.binName}"/></option>
                             </c:forEach>
                                     </select>
                             </td>
                        </tr>
                        <tr>
                            <td>
                                Product Condition
                            </td>
                            <td><select id="proCon" name="proCon" style="width:250px;height:40px">
                                <option value="Good">Good</option>
                                <option value="Damaged">Damaged</option>
                                <option value="Excess">Excess</option>
                            </select></td>
                            
                            <td>
                                UOM
                            </td>
                            <td><select id="Uom" name="Uom" style="width:250px;height:40px">
                                <option value="Pack">Pack</option>
                                <option value="Piece" selected>Piece</option>
                            </select></td>
                        </tr>
                        <tr>
                            <td align="center">
                                SERIAL NO : 
                            </td>
                            <td>
                             <input type="hidden" name="grnId" id="grnId" value="<c:out value="${grnId}"/>"/>
                             <input type="hidden" name="gtnId" id="gtnId" value="<c:out value="${gtnId}"/>"/>
                             
                               <input type="text" id="serialNo" name="serialNo" value="" class="form-control" style="width:250px" onkeypress="return RestrictSpace()" onchange="checkExists(this.value);"/> 
                            </td>
                           
                                       
                        </tr>
                    </table>                        
                    
                    <br> 
                <div id="tableContent"></div>
                        <script>
                            var serial = [];
                            var ipqt=[];
                            var uomarr=[];
                            var sno=[];
                            var procon=[];
                            var arr=[];
                            var binIds=[];
                            var i=0;
    

                     function go1() {
                        if (httpRequest.readyState == 4) {
                        if (httpRequest.status == 200) {
                         var response = httpRequest.responseText;
                            if (response != "") {
                                alert(response);
                                resetTheDetails();
                    
                            } else
                            {
                                saveTempSerialNos();
                            }
                                 }
                               }
                            }
                      function checkExists(serialNo){
                                   var url = "";
                                   if (serialNo != "") {
                                       url = "./saveSerialTempDetails.do?serialNo="+serialNo;
                                       
                             if (window.ActiveXObject)
                            {
                            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                            } else if (window.XMLHttpRequest)
                             {           
                             httpRequest = new XMLHttpRequest();
                             }
                              httpRequest.open("POST", url, true);
                            httpRequest.onreadystatechange = function() {
                            go1();
                             };
                            httpRequest.send(null);
                                  }

                           }
                                
                            
                           function saveTempSerialNos() {
                                if(serial.indexOf(serialNo.value)!=-1){
                                    alert("Already Scanned");
                                }else{
                                    var mk=document.getElementById("serialSize").value;
                                    var u=document.getElementById("poQuantity").value;
                                    if(i<(u-mk)){
                                   serial[i]=serialNo.value;
                                   ipqt[i]=1;
                                   binIds[i]=binId.value;
                                   uomarr[i]=Uom.value;
                                   procon[i]=proCon.value;
                                   i++;
                                   table();
                               }
                               else{
                                   alert("Quantity Exceeded");
                               }
                            }
                        }
                               function table(){
                                   if(serial.length>0){
                                   for(x=1;x<=serial.length;x++){
                                   sno.push(x);
                                   }}
                                   const tmpl = (sno,serial,uomarr,procon,ipqt,binIds) => `
                                   <table class="table table-info mb30 table-bordered" id="table"><thead>
                                   <tr><th>S.No<th>Serial No<th>UOM<th>Product Condition<th>Qty<th>Delete<tbody>
                                   ${sno.map( (cell, index) => `
                                   <tr><td><input type="hidden" id="sNo" name="sNo" value="${cell}"/>${cell}<td><input type="hidden" id="serialNos"+sno name="serialNos" value="${serial[index]}" />${serial[index]}<td><input type="hidden" id="uom" name="uom" value="${uomarr[index]}" />${uomarr[index]}<td><input type="hidden" id="proCond" name="proCond" value="${procon[index]}" />${procon[index]}<td><input type="hidden" id="ipQty" name="ipQty" value="${ipqt[index]}"/>${ipqt[index]}<td><input type="hidden" id="bin" name="bins" value="${binIds[index]}"/><input type="button" class="btn btn-success" name="${sno[index]}" value="Delete" onclick="deleteRow(this.name,sno, serial, uomarr, ipqt, procon,binIds);" style="width:100px;height:35px;"></tr>
                                   `).join('')}
                                   </table><p align = "center" style="font-size:16px"><b>Scanned Qty:</b><input type="text" id="qty" name="qty" value="${serial.length}" class="form-control" style="width:100px;height=30px" readonly/><br><br></p>`;
                                    tableContent.innerHTML=tmpl(sno,serial,uomarr,procon,ipqt,binIds);
                                    sno.splice(0,sno.length);
                                    document.getElementById("serialNo").value='';
                               }
                              function deleteRow(name, sno, serial, uomarr, ipqt, procon,binIds){
                                
                                  var x=name-1;
                                  serial.splice(x,1);
                                  uomarr.splice(x,1);
                                  ipqt.splice(x,1);
                                  procon.splice(x,1);
                                  binIds.splice(x,1);
                                  sno.splice(0,sno.length);
                                  name=0;
                                  i--;
                                  table();
                                  
                                  
                              }
                              function setVal(value){
                                  var x=value.split('-');
                                  document.getElementById("itemId").value=x[0];
                                  document.getElementById("asnId").value=x[1];
                                  
                              }
                               function showTables(i,j,k){
                                  document.getElementById('proDet').value=j+'-'+k;
                                   document.getElementById("itemId").value=j;
                                  document.getElementById("asnId").value=k;
                                  if(j!=""){
                                 $('#table1').show();
                                  }
                                if(i!=0){
                                 $('#table2').show();
                             }
                         }
                         function deleteSerial(x){
                             var gtnId=document.getElementById("gtnId").value;
                             var grnId=document.getElementById("grnId").value;
                             var itemId=document.getElementById("itemId1").value;
                                document.gtn.action = '/throttle/editSerialDetail.do?&gtnId='+gtnId+'&itemId='+itemId+'&grnId='+grnId+'&tempId='+x;
                                document.gtn.submit();
                                this.disabled=true;
                                this.value='Searching';
                            }
                        </script>
                        
                         <script>
                            function resetTheDetails() {
                                document.getElementById("serialNo").value='';
                            }
                            function getSerialDet(){
                                var gtnId=document.getElementById("gtnId").value;
                                var itemId=document.getElementById("itemId").value;
                                var asnId=document.getElementById("asnId").value;
                                document.gtn.action = '/throttle/editSerialDetail.do?&gtnId='+gtnId+'&itemId='+itemId+'&asnId='+asnId;
                                document.gtn.submit();
                                this.disabled=true;
                                this.value='Searching';
                              }
                            function editGrnSerialNos() {
                                var y=document.getElementById("grnId").value;
                                var sizeSerial= document.getElementById("serialSize").value;
                                if(sizeSerial==0){
                                document.gtn.action = '/throttle/saveGrnSerialDetails.do';
                                document.gtn.submit();
                                this.disabled=true;
                                this.value='updating';
                                }else{
                                document.gtn.action = '/throttle/editGrnSerialDetails.do?&grnId='+y;
                                document.gtn.submit();
                                this.disabled=true;
                                this.value='updating';
                                }
                                
                            }
                            function RestrictSpace() {
                                if (event.keyCode == 32) {
                                    return false;
                                }
                            }
                         </script>
                         
                        <center>
                            <input type="button" class="btn btn-success" name="Update" value="Update" onclick="editGrnSerialNos(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;
                        </center>
                         </form>
                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

