<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.DockIn Update"  text="DockIn Update"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.DockIn Update"  text="DockIn Update"/></a></li>
            <li class="active">DockIn Update</li>
        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="">
                <form name="gtn" id="gtn" method="post">
                     <table class="table table-info mb30 table-bordered" >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>Customer Name</th>
                                <th>Product </th>
                                <th>Product Code</th>
                                <th>GateIn Qty</th>
                                <th>HSN Code</th>
                            </tr>
                        </thead>
                            <tbody>
                                
                                    <tr >
                                        <td id="sn">
                                        </td>
                                        <td id="custName1"></td>
                                        <td id="prodName"></td>
                                        <td id="prodCode"></td>
                                        <td id="Qty"></td>
                                        <td id="hsnCode"></td>
                                    </tr>
                            </tbody>
            
                    </table>  
                    <table class="table table-info mb30 table-hover" id="serial">
                            <div align="center" style="height:20px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>
                        <c:if test="${gtnDetails != null}">
                            
                                <tr>
                                    <td align="center">Select Product</td>
                                <td><select id="proDet" name="proDet" onchange="setVal(this.value);" style="height:40px;width:250px">
                                  <option  value="">---Select One ---</option>    <c:forEach items="${gtnDetails}" var="gtnDet">
                                      <option value="<c:out value="${gtnDet.asnId}"/>-<c:out value="${gtnDet.itemId}"/>-<c:out value="${gtnDet.gtnDetId}"/>-<c:out value="${gtnDet.poQuantity}"/>-<c:out value="${gtnDet.itemName}"/>-<c:out value="${gtnDet.custName}"/>-<c:out value="${gtnDet.skuCode}"/>-<c:out value="${gtnDet.hsnCode}"/>-<c:out value="${gtnDet.custId}"/>"><c:out value="${gtnDet.itemName}"/></option>
                            </c:forEach>
                                </select></td>
                                <input type="hidden" name="asnId" id="asnId" value=""/>
                                <input type="hidden" name="itemId" id="itemId" value=""/>
                                <input type="hidden" name="gtnDetId" id="gtnDetId" value=""/>
                                <input type="hidden" name="poQuantity" id="poQuantity" value=""/>
                                <input type="hidden" name="reqQuantity" id="reqQuantity" value=""/>
                                <input type="hidden" name="custId" id="custId" value=""/>
                                        
                                </tr>     
                        </c:if>
                        <tr>
                           <td align="center">
                                User
                             </td>
                             <td>
                                 
                                 <select id="empUserId" name="empUserId" style="width:250px;height:40px">
                             <c:forEach items="${userDetails}" var="emps">
                                     <option value="<c:out value="${emps.empUserId}"/>"><c:out value="${emps.empName}"/></option>
                             </c:forEach>
                                     </select>
                             </td>
                       <td>Bin</td>
                       <td><select id="binId" name="binId" style="width:250px;height:40px"class="form-control">
                             
                                     </select>
                             </td>
                        </tr>
                        <tr>
                            <td>
                                Product Condition
                            </td>
                            <td><select id="proCon" name="proCon" style="width:250px;height:40px">
                                <option value="Good">Good</option>
                                <option value="Damaged">Damaged</option>
                                <option value="Excess">Excess</option>
                            </select></td>
                            
                            <td>
                                UOM
                            </td>
                            <td><select id="Uom" name="Uom" style="width:250px;height:40px">
                                <option value="Pack">Pack</option>
                                <option value="Piece" selected>Piece</option>
                            </select></td>
                        </tr>
                        <tr>
                            <td align="center">
                                SERIAL NO : 
                            </td>
                            <td>
                             <input type="hidden" name="gtnId" id="gtnId" value="<c:out value="${gtnId}"/>"/>

                                <input type="text" id="serialNo" name="serialNo" value="" class="form-control" style="width:250px" onchange="checkExists(this.value);"/> 
                            </td>
                         
                                       
                        </tr>
                    </table>                        
                    
                    <br> 
                <div id="tableContent"></div>
                        <script>
                            var serial = [];
                            var ipqt=[];
                            var uomarr=[];
                            var sno=[];
                            var procon=[];
                            var arr=[];
                            var binIds=[];
                            var i=0;
                           
                                var httpRequest;
    

                     function go1() {
                        if (httpRequest.readyState == 4) {
                        if (httpRequest.status == 200) {
                         var response = httpRequest.responseText;
                            if (response != "") {
                                alert(response);
                                resetTheDetails();
                    
                            } else
                            {
                                saveTempSerialNos();
                            }
                                 }
                               }
                            }
                      function checkExists(serialNo){
                                   var url = "";
                                   if (serialNo != "") {
                                       url = "./saveSerialTempDetails.do?serialNo="+serialNo;
                                       
                             if (window.ActiveXObject)
                            {
                            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                            } else if (window.XMLHttpRequest)
                             {           
                             httpRequest = new XMLHttpRequest();
                             }
                              httpRequest.open("POST", url, true);
                            httpRequest.onreadystatechange = function() {
                            go1();
                             };
                            httpRequest.send(null);
                                  }

                               }
                                
                            
                            function saveTempSerialNos() {
                                if(serial.indexOf(serialNo.value)!=-1){
                                    alert("Already Scanned");
                                }else{
                                    var u=document.getElementById("poQuantity").value;
                                    if(i<u){
                                   serial[i]=serialNo.value;
                                   ipqt[i]=1;
                                   binIds[i]=binId.value;
                                   uomarr[i]=Uom.value;
                                   procon[i]=proCon.value;
                                   i++;
                                   table();
                               }
                               else{
                                   alert("Quantity Exceeded");
                               }
                            }
                        }
                               function table(){
                                   if(serial.length>0){
                                   for(x=1;x<=serial.length;x++){
                                   sno.push(x);
                                   }}
                                   const tmpl = (sno,serial,uomarr,procon,ipqt,binIds) => `
                                   <table class="table table-info mb30 table-bordered" id="table"><thead>
                                   <tr><th>S.No<th>Serial No<th>UOM<th>Product Condition<th>Qty<th>Delete<tbody>
                                   ${sno.map( (cell, index) => `
                                   <tr><td><input type="hidden" id="sNo" name="sNo" value="${cell}"/>${cell}<td><input type="hidden" id="serialNos"+sno name="serialNos" value="${serial[index]}" />${serial[index]}<td><input type="hidden" id="uom" name="uom" value="${uomarr[index]}" />${uomarr[index]}<td><input type="hidden" id="proCond" name="proCond" value="${procon[index]}" />${procon[index]}<td><input type="hidden" id="ipQty" name="ipQty" value="${ipqt[index]}"/>${ipqt[index]}<td><input type="hidden" id="bins" name="bins" value="${binIds[index]}"/><input type="button" class="btn btn-success" name="${sno[index]}" value="Delete" onclick="deleteRow(this.name,sno, serial, uomarr, ipqt, procon,binIds);" style="width:100px;height:35px;"></tr>
                                   `).join('')}
                                   </table><p align = "center" style="font-size:16px"><b>Scanned Qty:</b><input type="text" id="qty" name="qty" value="${serial.length}" class="form-control" style="width:100px;height=30px" readonly/><br><br></p>`;
                                    tableContent.innerHTML=tmpl(sno,serial,uomarr,procon,ipqt,binIds);
                                    sno.splice(0,sno.length);
                                    document.getElementById("serialNo").value='';
                               }
                              function deleteRow(name, sno, serial, uomarr, ipqt, procon,binIds){
                                
                                  var x=name-1;
                                  serial.splice(x,1);
                                  uomarr.splice(x,1);
                                  ipqt.splice(x,1);
                                  procon.splice(x,1);
                                  binIds.splice(x,1);
                                  sno.splice(0,sno.length);
                                  name=0;
                                  i--;
                                  table();
                                  
                                  
                              }
                              function setVal(value){
                                  var val=value.split('-');
                                  document.getElementById("itemId").value=val[1];
                                  document.getElementById("asnId").value=val[0];
                                  document.getElementById("gtnDetId").value=val[2];
                                  document.getElementById("poQuantity").value=val[3];
                                  document.getElementById("reqQuantity").value=val[3];
                                  document.getElementById("custId").value=val[8];
                                  document.getElementById("prodName").innerHTML=val[4];
                                  document.getElementById("Qty").innerHTML=val[3];
                                  document.getElementById("custName1").innerHTML=val[5];
                                  document.getElementById("prodCode").innerHTML=val[6];
                                  document.getElementById("hsnCode").innerHTML=val[7];
                                  document.getElementById("sn").innerHTML='1';
                                  var itemId1=val[1];
                               $.ajax({
                                        url: "/throttle/getBinDetails.do",
                                        dataType: "json",
                                        data: {
                                            item: itemId1
                                        },
                                        success: function(data) {
                                            if (data != '') {
                                                $('#binId').empty();
                                                $('#binId').append(
                                                        $('<option></option>').val(0).html('--select--'))
                                                $.each(data, function(i, data) {
                                                    $('#binId').append(
                                                            $('<option style="width:90px"></option>').attr("value", data.Id).text(data.Name)
                                                            )
                                                });
                                            } else {
                                                $('#binId').empty();
                                                $('#binId').append(
                                                        $('<option></option>').val(0).html('--select--'))
                                            }
                                        }
                                    });
                              }
                        </script>
                        
                         <script>
                            function resetTheDetails() {
                                document.getElementById("serialNo").value='';
                            }
                            function saveGrnSerialNos() {
                                document.gtn.action = '/throttle/saveGrnSerialDetails.do';
                                document.gtn.submit();
                                this.disabled=true;
                                this.value='updating';
                            }
                         </script>
                         
                        <center>
                            <input type="button" class="btn btn-success" name="Update" value="Update" onclick="saveGrnSerialNos(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;
                        </center>
                         </form>
                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

