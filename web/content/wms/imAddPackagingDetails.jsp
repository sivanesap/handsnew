<%-- 
    Document   : imAddPackagingDetails
    Created on : 3 Dec, 2021, 10:30:10 PM
    Author     : Roger
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
    var dateTemp = new Date();
       var day = dateTemp.getDate();
       var month = dateTemp.getMonth()+1;
       var year = dateTemp.getFullYear();
       var hours = dateTemp.getHours();
       var minutes = dateTemp.getMinutes();
       if(day.toString().length==1){
          day = '0'+day;
       }
       if(month.toString().length==1){
          month = '0'+month;
       }
       if(hours.toString().length==1){
          hours = '0'+hours;
       }
       if(minutes.toString().length==1){
          minutes = '0'+minutes;
       }
       var date = day+'-'+month+'-'+year;
       
       var time = hours+':'+minutes;
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Add Packaging Data - Machine Wise</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">InstaMart</a></li>
            <li class="active">Add Packaging Data - Machine Wise</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body>
                <form name="grn" method="post">
                    <table class="table table-info mb30 table-hover" id="serial">
                        <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                        </div>
                        <tr>
                            <td align="center"><text style="color:red">*</text>Machine</td>
                            <td height="30">
                                <select name="machineId" id="machineId" class="form-control" style="width:240px;height:40px"  onclick="" >
                                    <option value="">------Select Machine------</option>
                                    <c:forEach items="${machineList}" var="vn">
                                        <option value="<c:out value="${vn.machineId}"/>"><c:out value="${vn.machineName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="center"><text style="color:red">*</text>Date</td>
                            <td height="30"><input name="packDate" id="packDate" type="text"  class="datepicker form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" readonly autocomplete="off"></td>
                            <td align="center"><text style="color:red">*</text>Time</td>
                            <td height="30"><input name="packTime" id="packTime" type="time"  class="form-control" style="width:240px;height:40px" autocomplete="off"></td>

                        </tr>
                        <tr>
                            <td align="center"><text style="color:red">*</text>Vendor Name</td>
                            <td height="30">
                                <select name="empVendor" id="empVendor" type="text"  class="form-control" style="width:240px;height:40px"  autocomplete="off">
                                    <option value="">------Select Vendor------</option>
                                    <option value="SSE">SSE</option>
                                    <option value="SKANDA">SKANDA</option>
                                </select>
                            </td>
                            <td align="center"><text style="color:red">*</text>Employee Name</td>
                            <td height="30"><input name="empName" id="empName" type="text"  class="form-control" style="width:240px;height:40px" value="" autocomplete="off"></td>
                        </tr>
                        <tr>
                            <td align="center">From Time</td>
                            <td><input type="time" name="startTime" id="startTime" class="form-control" style="width:240px;height:40px"></td>
                            <td align="center">To Time</td>
                            <td><input type="time" name="endTime" id="endTime" class="form-control" style="width:240px;height:40px"></td>
                        </tr>
                    </table> 
                    <script>
                        $('#packDate').val(date);
                        $('#packTime').val(time);
                        document.getElementById('packTime').readOnly=true;
                    </script>
                    <table class="table table-info mb30 table-hover sortable" id="addIndent" align="center" width="90%">
                        <thead >
                            <tr>
                                <th>S.No</th>
                                <th>Sku Code</th>
                                <th>Sku Description</th>  
                                <th>UOM</th>
                                <th>Pack Qty</th>
                                <th>Packing Qty</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody id="addIndent2">
                            
                        </tbody>
                    </table>

                    <script>
                        function addRow() {
                            var tab = document.getElementById("addIndent");
                            var sno = tab.getElementsByTagName('tr').length;
                            $('#addIndent2').append(
                                    '<tr><td>'+sno+'</td>'+
                                    '<td>'+'<select id="skuCode'+sno+'" name="skuCode" onchange="setValues(this.value,'+sno+')" class="form-control" style="width:200px;height:40px"><option value="">-----Select Sku-----</option><c:forEach items="${imSkuDetails}" var="sku"><option value="<c:out value="${sku.productId}"/>~<c:out value="${sku.productName}"/>~<c:out value="${sku.qty}"/>~<c:out value="${sku.uomName}"/>~<c:out value="${sku.productDescription}"/>"><c:out value="${sku.skuCode}"/> - <c:out value="${sku.productDescription}"/></option></c:forEach></select>'+'</td>'+
                                    '<td>'+'<input type="text" id="skuDescription'+sno+'" name="skuDescription" class="form-control" style="width:200px;height:40px" readonly>'+'</td>'+
                                    '<td>'+'<input type="text" id="uom'+sno+'" name="uom" class="form-control" style="width:130px;height:40px" readonly>'+'</td>'+
                                    '<td>'+'<input type="text" id="packQty'+sno+'" name="packQty" class="form-control" style="width:130px;height:40px" readonly>'+'</td>'+
                                    '<td>'+'<input type="text" id="packedQty'+sno+'" name="packedQty" value="0.00" class="form-control" style="width:130px;height:40px">'+
                                    '<input type="hidden" id="skuId'+sno+'" name="skuId" >'+'</td>'+
                                    '<td>'+'<input type="button" class="btn btn-info"   name="delete"  id="delete"   value="Delete Row" onclick="deleteRow(this,' + sno + ');" >'+'</td></tr>'
                                );
                        }

                        function deleteRow(src,sno) {
                            var oRow = src.parentNode.parentNode;
                            var dRow = document.getElementById('addIndent');
                            dRow.deleteRow(oRow.rowIndex);
                        }
                        function submitPage(value) {
                             if (document.getElementById("machineId").value == "") {
                                alert("Enter Machine");
                                document.getElementById("machineId").focus();
                            } else if (document.getElementById("packDate").value == "") {
                                alert("Enter Date");
                                document.getElementById("packDate").focus();
                            } else if (document.getElementById("packTime").value == "") {
                                alert("Enter Time");
                                document.getElementById("packTime").focus();
                            } else if (document.getElementById("empVendor").value == "") {
                                alert("Enter Vendor Name");
                                document.getElementById("empVendor").focus();
                            } else if (document.getElementById("empName").value == "") {
                                alert("Enter Employee Name");
                                document.getElementById("empName").focus();
                            }else{
                            document.grn.action = '/throttle/imAddPackagingDetails.do?param=save';
                            document.grn.submit();
                        }
                    }
                        function checkqty() {
                            var qty = document.getElementsByName("qty");
                            var totQty = 0;
                            for (var i = 0; i < qty.length; i++) {
                                if (qty[i].value != "") {
                                    totQty = parseInt(qty[i].value) + totQty;
                                }
                            }
                            document.getElementById("totQty").value = totQty;
                        }
                        function setValues(value, sno) {
                            if (value != '') {
                                var skuId = value.split('~')[0];
                                var skuName = value.split('~')[1];
                                var skuDescription = value.split('~')[4];
                                var packQty = value.split('~')[2];
                                var uom = value.split('~')[3];
                                document.getElementById("skuId" + sno).value = skuId;
                                document.getElementById("skuDescription" + sno).value = skuDescription;
                                document.getElementById("uom" + sno).value = uom;
                                document.getElementById("packQty" + sno).value = packQty;
                            }else{
                                document.getElementById("skuId" + sno).value = "";
                                document.getElementById("skuDescription" + sno).value = "";
                                document.getElementById("uom" + sno).value = "";
                                document.getElementById("packQty" + sno).value = "";
                            }
                        }
                        </script>

                        <table>
                            <center>
                                <input value="Add Row" class="btn btn-success" id="countRow" type="button" onClick="addRow();" >
                                &emsp;<input type="button" value="Save" id="insert" class="btn btn-success" onClick="submitPage(this.value);">
                                &emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">
                            </center>

                        </table>
                    </form>
                </body>
            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>

    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>