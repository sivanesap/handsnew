<%-- 
    Document   : imCustomerMasterPackMat
    Created on : 19 Oct, 2021, 12:46:09 PM
    Author     : Roger
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    function submitPage() {
            if (document.getElementById("customerName").value == "") {
                alert("Enter Customer Name");
                document.getElementById("customerName").focus();
            } else if (document.getElementById("customerCode").value == "") {
                alert("Enter Customer Code");
                document.getElementById("customerCode").focus();
            } else if (document.getElementById("address").value == "") {
                alert("Enter Address");
                document.getElementById("address").focus();
            } 
//            else if (document.getElementById("address1").value == "") {
//                alert("Enter Address2");
//                document.getElementById("address1").focus();
//            } 
            else if (document.getElementById("city").value == "") {
                alert("Enter City");
                document.getElementById("city").focus();
            } else if (document.getElementById("state").value == "") {
                alert("Enter State");
                document.getElementById("state").focus();
            } else if (document.getElementById("pincode").value == "") {
                alert("Enter Postal Code");
                document.getElementById("pincode").focus();
            } else if (document.getElementById("mobileNo").value == "") {
                alert("Enter PhoneNo");
                document.getElementById("mobileNo").focus();
            } 
//            else if (document.getElementById("mobileNo2").value == "") {
//                alert("Enter AlterPhoneNo");
//                document.getElementById("mobileNo2").focus();
//            }
            else if (document.getElementById("gstNo").value == "") {
                alert("Enter GSTNo");
                document.getElementById("gstNo").focus();
            } else if (document.getElementById("activeInd").value == "") {
                alert("Enter Status");
                document.getElementById("activeInd").focus();
            } else {

            document.customer.action = " /throttle/imCustomerMasterPackMat.do?param=save";
            document.customer.method = "post";
            document.customer.submit();
        }
    }
    function setValues(sno, custId, custName, custCode, address, address1, city,state, pincode, phone1, phone2, gstNumber, activeType) {
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (var i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("customerId").value = custId;
        document.getElementById("customerCode").value = custCode;
        document.getElementById("customerName").value = custName;
        document.getElementById("address").value = address;
        document.getElementById("address1").value = address1;
        document.getElementById("city").value = city;
        document.getElementById("state").value = state;
        document.getElementById("pincode").value = pincode;
        document.getElementById("mobileNo").value = phone1;
        document.getElementById("gstNo").value = gstNumber;
        document.getElementById("mobileNo2").value = phone2;
        document.getElementById("activeInd").value = activeType;
    }
    
    
    
    function checkGst(){
        
        var gstNo = document.getElementById("gstNo").value;
        
        
        if (gstNo.length == 15){
            
            
        }else{
            alert("Gst No Count is Not Correct")
            
            document.getElementById("gstNo").value = ""
        }
        
//        alert("checking")
    }
    
    
    
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Customer Master (Material)</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html">InstaMart</a></li>
                <li class="">Customer Master (Material)</li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="customer"  method="post" >
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <br>
                        <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                            <input type="hidden" name="customerId" id="customerId" value=""  />
                            <tr>
                                <!--<table  border="0" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">-->
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="8" >Customer Master (Material)</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Customer Name</td>
                                    <td><input type="text" id="customerName" name="customerName" class="form-control" style="width:240px;height:40px"></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Customer Code</td>
                                    <td class="text1"><input type="text" name="customerCode" id="customerCode" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>
                                
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Address</td>
                                    <td class="text1"><input type="text" name="address" id="address" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checkproductCategoryName();" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Address 2</td>
                                    <td class="text1"><input type="text" name="address1" id="address1" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>City</td>
                                    <td class="text1"><input type="text" name="city" id="city" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>State</td>
                                    <td class="text1"><input type="text" name="state" id="state" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Postal Code</td>
                                    <td class="text1"><input type="text" name="pincode" id="pincode" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                    
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Phone No</td>
                                    <td class="text1"><input type="text" name="mobileNo" id="mobileNo" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Alternate Phone No</td>
                                    <td class="text1"><input type="text" name="mobileNo2" id="mobileNo2" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>GST No</td>
                                    <td class="text1"><input type="text" name="gstNo" id="gstNo" class="form-control" style="width:240px;height:40px" autocomplete="off"  onchange="checkGst()"/></td>

                                </tr>
                                <tr>
                                     <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Status</td>
                                    <td class="text1">
                                        <select  align="center" class="form-control" style="width:240px;height:40px" name="activeInd" id="activeInd" >
                                            <option value='Y'>Active</option>
                                            <option value='N' id="inActive" style="display: none">In-Active</option>
                                        </select>
                                    </td>
                                </tr>
                                
                            </table>
                            </tr>
                            <tr>
                                <td>
                                    <br>
                                    <center>
                                        <input type="button" class="btn btn-success" value="Save" name="Submit" onClick="submitPage()">
                                    </center>
                                </td>
                            </tr>
                        </table>
                        <br>

                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30" style="color: white">
                                    <th>S.No</th>
                                    <th>Customer Name</th>
                                    <th>Customer Code</th>
                                    <th>Address</th>
                                    <th>Address 2</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Pincode</th>
                                    <th>Phone No</th>
                                    <th>Alternate Phone No</th>
                                    <th>GST Number</th>
                                    <th>Active Status</th>
                                    <th>Select</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                                <c:if test = "${customerMaster != null}">
                                    <c:forEach items="${customerMaster}" var="pc">
                                        <%
                                                    sno++;
                                                    String className = "text1";
                                                    if ((sno % 1) == 0) {
                                                        className = "text1";
                                                    } else {
                                                        className = "text2";
                                                    }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.custName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.custCode}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.address}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.address1}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.city}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.stateId}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.pincode}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.mobileNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.phoneNumber}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.gstinNumber}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.activeType}" /></td>
                                            <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>,'<c:out value="${pc.custId}" />', '<c:out value="${pc.custName}" />', '<c:out value="${pc.custCode}" />', '<c:out value="${pc.address}" />', '<c:out value="${pc.address1}" />', '<c:out value="${pc.city}" />', '<c:out value="${pc.stateId}" />', '<c:out value="${pc.pincode}" />', '<c:out value="${pc.mobileNo}" />','<c:out value="${pc.phoneNumber}" />','<c:out value="${pc.gstinNumber}" />','<c:out value="${pc.activeType}" />');" /></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>


                        <input type="hidden" name="count" id="count" value="<%=sno%>" />

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
