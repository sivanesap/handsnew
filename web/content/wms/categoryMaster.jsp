<%-- 
    Document   : getcategoryMaster
    Created on : 15 Jun, 2021, 12:02:05 AM
    Author     : MAHENDIRAN R
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    function submitPage() {
        document.CategoryMaster.action = " /throttle/saveCategoryMasters.do";
        document.CategoryMaster.method = "post";
        document.CategoryMaster.submit();
    }
    function setValues(sno, supplierId, categoryName, batchCode, categoryId) {
//        alert("111111111")
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }

        document.getElementById("categoryId").value = categoryId;
//        document.getElementById("supplierId").value = supplierId;
        document.getElementById("categoryName").value = categoryName;
        document.getElementById("batchCode").value = batchCode;
    }

</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Category Master" text="Category Master"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Master"/></a></li>
                <li class=""><spring:message code="hrms.label.Category Master" text="Category Master"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">
                    <form name="CategoryMaster"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <br>
                        <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="8" >Category Master</th>
                                    </tr>
                                </thead>
                                <tr>
                                <input type="hidden" id="categoryId" name="categoryId" value=""/>
                                <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Category Name</td>
                                <td class="text1"><input type="text" name="categoryName" id="categoryName" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checksupplierCategoryName();" autocomplete="off"/></td>
                                <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Batch Code</td>
                                <td class="text1"><input type="text" name="batchCode" id="batchCode" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checksupplierCategoryName();" autocomplete="off"/></td>
                                </tr>

                                <tr>
<!--                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Supplier Name</td>
                                    <td class="text1">
                                        <select  align="center" class="form-control" style="width:240px;height:40px" name="supplierId" id="supplierId" >
                                            <option value=''>----Select Supplier----</option>
                                            <c:forEach var="sup" items="${supplierList}">
                                                <option value='<c:out value="${sup.supplierId}"/>'><c:out value="${sup.supplierName}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>-->
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Status</td>
                                    <td class="text1">
                                        <select  align="center" class="form-control" style="width:240px;height:40px" name="ActiveInd" id="ActiveInd" >
                                            <option value='Y'>Active</option>
                                            <option value='N' id="inActive" style="display: none">In-Active</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                <center><a>
                                        <input  type="button" class="btn btn-success" align="center" value="Save" name="Submit" onClick="submitPage()">
                                        &emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">
                                    </a>
                                </center>
                                </tr>
                            </table>
                            <br>
                            <table class="table table-info mb30 table-hover" id="table" >	
                                <thead>

                                    <tr height="30" style="color: white">
                                        <th>S No</th>
                                        <!--<th>Supplier Name</th>-->
                                        <th>Category Name</th>
                                        <th>Batch Code</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>


                                    <% int sno = 0;%>
                                    <c:if test = "${categoryMaster != null}">
                                        <c:forEach items="${categoryMaster}" var="cat">
                                            <%
                                                sno++;
                                                String className = "text1";
                                                if ((sno % 1) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                            %>

                                            <tr>
                                                <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                                <!--<td class="<%=className%>"  align="left"> <c:out value="${cat.supplierName}" /></td>-->
                                                <td class="<%=className%>"  align="left"> <c:out value="${cat.categoryName}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${cat.batchCode}" /></td>
                                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${cat.supplierId}" />', '<c:out value="${cat.categoryName}" />', '<c:out value="${cat.batchCode}" />','<c:out value="${cat.categoryId}" />');" /></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </c:if>
                            </table>


                            <input type="hidden" name="count" id="count" value="<%=sno%>" />

                            <br>
                            <br>
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" selected="selected">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span>Entries Per Page</span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("table", 1);
                            </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>


