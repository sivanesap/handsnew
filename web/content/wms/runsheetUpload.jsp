<%-- 
    Document   : shipmentupload
    Created on : Mar 12, 2021, 6:48:57 PM
    Author     : DELL
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript">
    function uploadContract() {
        document.upload.action = "/throttle/saveRunsheetUpload.do";
        document.upload.method = "post";
        document.upload.submit();
    }

    function uploadPage() {
        var runsheetId = document.getElementById("runsheetId1").value;
        document.upload.action = "/throttle/shipmentupload.do?&param=save&runsheetId="+runsheetId;
        document.upload.method = "post";
        document.upload.submit();
    }
    function saveRunsheet() {
        document.addNew.action = "/throttle/shipmentupload.do?&param=add";
        document.addNew.method = "post";
        document.addNew.submit();
    }
    var cnt = 0;
    var sno1 = 1;
    function checkTrackingId(val) {
        var tab = document.getElementById("rvpAdd");
        $.ajax({
            url: '/throttle/getFkRunsheetDetails.do',
            dataType: 'json',
            data: {id: val},
            success: function(data, textStatus, jqXHR) {
                $.each(data, function(key, value) {
                    if (value.stat == 0) {
                        if (value.count != 0) {
                            alert("Already Runsheet Added / Picked Up");
                        } else {

                            var row = tab.insertRow(cnt);
                            row.innerHTML= '<td>' + sno1 + '</td><td>' + value.shipmentId + '</td><td>' + value.name + '</td><td>' + value.address + '</td><td>'
                                    + 'RVP' + '</td><td>' + '0' + '</td><td>' + value.status + '</td><td>' + value.date + '</td><td>' + value.itemName + '</td><td>'
                                    + '<font color="green">Ready to Submit</font>' + '</td>'
                                    + '<input type="hidden" name="shipmentId" id="shipmentId' + sno1 + '" value="' + value.shipmentId + '">'
                                    + '<input type="hidden" name="name" id="name' + sno1 + '" value="' + value.name + '">'
                                    + '<input type="hidden" name="address" id="address' + sno1 + '" value="' + value.address + '">'
                                    + '<input type="hidden" name="codAmount" id="codAmount' + sno1 + '" value="0">'
                                    + '<input type="hidden" name="status" id="status' + sno1 + '" value="' + value.status + '">'
                                    + '<input type="hidden" name="date" id="date' + sno1 + '" value="' + value.date + '">'
                                    + '<input type="hidden" name="itemName" id="itemName' + sno1 + '" value="' + value.itemName + '">';
                            cnt++;
                            sno1++;
                        }
                    } else if (value.stat == 1) {
                        alert("Shipment Id Not belongs to RVP");
                    } else {
                        alert("Data Not Found")
                    }
                });
            }
        })
    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="Runsheet Upload"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Uploads" text="Uploads"/></a></li>
            <li class=""><spring:message code="hrms.label.runsheetUpload" text="Runsheet Upload "/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >
                <form name="upload"  method="post" enctype="multipart/form-data">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    <table class="table table-info mb30 table-hover" id="uploadTable"  >
                        <thead> 
                            <tr>
                                <th  colspan="4">Runsheet Upload Details</th>
                            </tr>
                        </thead>
                        <tr>
                            <td >Select Excel</td>
                            <td ><input type="file" name="importCnote" id="importCnote"  class="importCnote"><font size="1"><u><a href="uploadedxls/Runsheet Upload.xls" download>sample file</a></u></font></td>                              

                            <c:if test = "${getContractList == null}">
                                <td >RVP Scan Shipment Id</td>
                                <td ><input type="text" name="trackingId" id="trackingId" style="width:200px;height:40px" onchange="checkTrackingId(this.value);"/></td>   
                                </c:if>
                                <c:if test = "${getContractList != null}">
                                <td></td>
                                <td></td>
                            </c:if>
                        </tr>
                        <tr>
                            <td colspan="4" align="center"><input type="button" class="btn btn-success" value="Upload" name="UploadContract" id="UploadContract" onclick="uploadContract();"></td>
                        </tr>
                        <tr>
                        </tr>
                    </table>
                    <div id="ptp" style="overflow: auto">
                        <div class="inpad">

                            <c:if test = "${getContractList != null}" >

                                <table class="table table-info mb30 table-hover" id="table" >

                                    <thead>
                                        <tr height="40">
                                            <th  height="30" >S No</th>
                                            <th  height="30" >Run Sheet Id</th>
                                            <th  height="30" >Tracking Id</th>
                                            <th  height="30" >Mobile No</th>
                                            <th  height="30" >Name</th>
                                            <th  height="30" >Address</th>
                                            <th  height="30" >Shipment Type</th>
                                            <th  height="30" >Cod Amount</th>
                                            <th  height="30" >Assigned User</th>
                                            <th  height="30" >Status</th>                                                    
                                            <th  height="30" >Date</th>
                                            <th  height="30" >Amount Paid</th>
                                            <th  height="30" >Receiver Name</th>
                                            <th  height="30" >Relation</th>
                                            <th  height="30" >Receiver</th>
                                            <th  height="30" >Mobile</th>
                                            <th  height="30" >Signature</th>
                                            <th  height="30" >Vertical</th>
                                            <th  height="30" >Order No</th>
                                            <th  height="30" >Product Details</th>
                                            <th  height="30" >Result Status</th>
                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                    %> 
                                    <tbody>

                                        <c:forEach items= "${getContractList}" var="contractList">
                                            <tr height="30">
                                                <td align="left"><%=sno%></td>
                                                <td align="left"   ><c:out value="${contractList.runSheetId}"/><input type="hidden" name="runsheetId" id="runsheetId<%=sno%>" value="<c:out value="${contractList.runSheetId}"/>"/></td>
                                                <td align="left"   ><c:out value="${contractList.trackingId}"/></td>
                                                <td align="left"   ><c:out value="${contractList.mobileNo}"/></td>
                                                <td align="left"   ><c:out value="${contractList.name}"/></td>

                                                <td align="left"   ><c:out value="${contractList.address}"/></td>
                                                <td align="left"   ><c:out value="${contractList.shipmentType}"/></td>
                                                <td align="left"   ><c:out value="${contractList.codAmount}"/></td>
                                                <td align="left"   ><c:out value="${contractList.assignedUser}"/></td>
                                                <td align="left"   ><c:out value="${contractList.status}"/></td>
                                                <td align="left"   ><c:out value="${contractList.date}"/></td>

                                                <td align="left"   ><c:out value="${contractList.amountPaid}"/></td>
                                                <td align="left"   ><c:out value="${contractList.receiverName}"/></td>
                                                <td align="left"   ><c:out value="${contractList.relation}"/></td>
                                                <td align="left"   ><c:out value="${contractList.receiver}"/></td>
                                                <td align="left"   ><c:out value="${contractList.mobile}"/></td>
                                                <td align="left"   ><c:out value="${contractList.signature}"/></td>
                                                <td align="left"   ><c:out value="${contractList.vertical}"/></td>
                                                <td align="left"   ><c:out value="${contractList.orderNo}"/></td>
                                                <td align="left"   ><c:out value="${contractList.productDetails}"/></td>

                                                <c:if test="${contractList.error==1}">
                                                    <td style="color:red"> 
                                                        Invalid tracking Id
                                                    </td>
                                                </c:if>
                                                <c:if test="${contractList.error==2}">
                                                    <td style="color:red"> 
                                                        Duplicate tracking Id
                                                    </td>
                                                </c:if>
                                                <c:if test="${contractList.error==3}">
                                                    <td style="color:red"> 
                                                        Existing Tracking Id in Runsheet not released
                                                    </td>
                                                </c:if>
                                                <c:if test="${contractList.error==0}">
                                                    <td style="color:green"> 
                                                        Ready To Submit
                                                    </td>
                                                </c:if>

                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    </tbody>
                                </table>

                                <center>

                                    <td colspan="0"><input type="button" class="btn btn-success" value="Submit" name="submitPage" id="submitPage" onclick="uploadPage();"></td>
                                </center>
                            </c:if>
                            <c:if test = "${getContractList == null}">
                            </div></div></form>
                    <form name="addNew"  method="post">
                        <div id="ptp" style="overflow: auto">
                            <div class="inpad">
                                <table class="table table-info mb30 table-hover" id="table" >

                                    <thead>
                                        <tr height="40">
                                            <th  height="30" >S No</th>
                                            <th  height="30" >Tracking Id</th>
                                            <th  height="30" >Name</th>
                                            <th  height="30" >Address</th>
                                            <th  height="30" >Shipment Type</th>
                                            <th  height="30" >Cod Amount</th>
                                            <th  height="30" >Status</th>                                                    
                                            <th  height="30" >Date</th>
                                            <th  height="30" >Product Details</th>
                                            <th  height="30" >Result Status</th>
                                        </tr>
                                    </thead>
                                    <tbody id="rvpAdd">

                                    </tbody>
                                </table>
                                <center>
                                    <input type="button" class="btn btn-success" value="Submit" name="submitPage" id="submitPage" onclick="saveRunsheet();">
                                </center>
                            </c:if>
                        </div>
                    </div>     

                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>

