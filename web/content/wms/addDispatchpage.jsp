
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script> 
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>
<link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>

<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
//alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();
    });
</script>
<script language="javascript">


    function getInvoiceNo() {

        var customerId = document.getElementById('customerId').value;

        document.addLect.action = '/throttle/connectDispatchPlanning.do?customerId=' + customerId;
        document.addLect.submit();


    }


    function searchDispatchDetails(value) {
        var count = $('#tables').find('tr').length;
        if (document.getElementById("vendorId").value == "") {
            alert("Please Select Vendor Id");
        } else if (document.getElementById("plannedDate").value == "") {
            alert("Please Enter Planned Date")
        } else if (document.getElementById("vehicleTypeId").value == "") {
            alert("Please Select Vehicle Type")
        } else if (document.getElementById("transferType").value == "") {
            alert("Please Select Transfer Type")
//        } else if (parseInt(count) < 2) {
//            alert("Please Choose Atleast One Invoice");
        } else {
            document.addLect.action = '/throttle/connectDispatchPlanning.do?param=save';
            document.addLect.submit();
        }
    }
    function searchDetails(value) {
        document.addLect.action = '/throttle/connectDispatchPlanning.do?';
        document.addLect.submit();
    }

    function tansShipMent(val) {
        if (value == 7) {
            document.getElementsByClassName("sender")[0].style.display = "block";
            document.getElementsByClassName("sender")[1].style.display = "block";
            document.getElementById("TranshipmentWhId").value = "";
        }
        else {
            document.getElementsByClassName("sender")[0].style.display = "none";
            document.getElementsByClassName("sender")[1].style.display = "none";
            document.getElementById("TranshipmentWhId").value = "";
        }




    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.Dispatch"  text="Dispatch"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="trucks.label.connect"  text="HS Connect"/></a></li>
            <li class="active"><spring:message code="stores.label.Dispatch"  text="Dispatch"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

            <body>
                <form name="addLect" method="post">   


                    <%@ include file="/content/common/message.jsp" %>

                    <div id="tabs" >
                        <ul class="nav nav-tabs">
                            <li id="active" data-toggle="tab"><a href="#deD"><span>Dispatch Planning</span></a></li>

                        </ul>
                        <div id="deD">



                            <table class="table table-info mb30 table-hover">
                                <thead>
                                    <tr>
                                        <th colspan="4"  height="30"><div ><spring:message code="trucks.label.TransportDetails"  text="TransportDetails"/></div></th>
                                </tr>
                                </thead>
                                <td><font color=red>*</font>Supplier</td>
                                <td>
                                    <select class="form-control" style="width:260px;height:40px;" id="suppId" name="suppId"  >
                                        <option value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                        <c:if test = "${getSupplierList != null}" >
                                            <c:forEach items="${getSupplierList}" var="Dept"> 
                                                <option value='<c:out value="${Dept.supplierId}" />'><c:out value="${Dept.supplierName}" /></option>
                                            </c:forEach >
                                        </c:if>  	
                                    </select>
                                    <input type="hidden" name="supplierIdNew" id="supplierIdNew" value="<c:out value="${suppId}"/>"/>
                                </td>
                                <td>
                                    <script>
                                        document.getElementById("suppId").value = '<c:out value="${suppId}"/>'
                                    </script>

                                    <input type="button" class="btn btn-success" value="Search" name="Search"  onclick="searchDetails()">

                                </td>
                                <tr>
                                    <td   ><font color=red>*</font><spring:message code="trucks.label.Vendor"  text="Vendor"/></td>
                                    <td   ><select class="form-control" style="width:260px;height:40px;" id="vendorId" name="vendorId"  >
                                            <option value="">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                            <c:if test = "${vendorList != null}" >
                                                <c:forEach items="${vendorList}" var="Type"> 
                                                    <option value='<c:out value="${Type.vendorId}" />'><c:out value="${Type.vendorName}" /></option>
                                                </c:forEach >
                                            </c:if>  	
                                        </select></td>

                                <script>
                                    $('#vendorId').select2({placeholder: 'Fill Vendor Name'});
                                </script>
                                <td ><font color="red">*</font>Planned Date</td>
                                <td ><input name="plannedDate" id="plannedDate" type="text" class="datepicker" style="width:260px;height:40px;" value="<c:out value="${plannedDate}"/>"  onclick="ressetDate(this);"></td>

                                </tr>
                                <tr>
                                    <td   ><font color=red>*</font><spring:message code="trucks.label.VehicleType"  text="VehicleType"/></td>
                                    <td   ><select class="form-control" style="width:260px;height:40px;" id="vehicleTypeId" name="vehicleTypeId"  >
                                            <option value="">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                            <c:if test = "${getVehicleTypeList != null}" >
                                                <c:forEach items="${getVehicleTypeList}" var="Type"> 
                                                    <option value='<c:out value="${Type.typeId}" />'><c:out value="${Type.typeName}" /></option>
                                                </c:forEach >
                                            </c:if>  	
                                        </select></td>
                                    <td   ><font color=red>*</font>Transfer Type</td>
                                    <td   ><select class="form-control" style="width:260px;height:40px;" id="transferType" name="transferType" onchange="tansShipMent(this.value)"  >
                                            <option value="">---Select One---</option>
                                            <c:if test = "${transferTypeList != null}" >
                                                <c:forEach items="${transferTypeList}" var="tt"> 
                                                    <option value='<c:out value="${tt.typeId}" />'><c:out value="${tt.transferType}" /></option>
                                                </c:forEach >
                                            </c:if>  	
                                        </select></td>

                                </tr>
                                <tr>


                                    <td class = "sender"style="width:260px;height:40px;display:none;" ><font color=red>*</font>Warehouse Name</td>
                                    <td  ><select class="form-control sender" style="width:260px;height:40px;display:none;"  id="TranshipmentWhId" name = "TranshipmentWhId" >
                                            <option value="">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                            <c:if test = "${whList != null}" >
                                                <c:forEach items="${whList}" var="Type"> 
                                                    <option value='<c:out value="${Type.whId}" />'><c:out value="${Type.whName}" /></option>
                                                </c:forEach >
                                            </c:if>  	
                                        </select></td>

                                </tr>

                            </table>


                            <c:if test="${getCustInvoiceDetails != null}">
                                <table align="center" border="0" id="table" class="table table-info mb30 table-hover" style="width:100%" >
                                    <thead>
                                        <tr style="height: 40px;" id="tableDesingTH">
                                            <th align="center">Customer</th>
                                            <th align="center">Address</th>
                                            <th align="center">City</th>
                                            <th align="center">Pincode</th>
                                            <th align="center">Total Qty</th>
                                            <th align="center">Invoice No</th>
                                            <th align="center">Total Tax Value</th>
                                            <th align="center">Invoice Amount</th>
                                            <th align="center">Pre-tax value</th>
                                            <th align="center">Base price</th>
                                            <th align="center">Choose</th>

                                        </tr></thead>
                                    <tbody>
                                        <% int index = 1;%>
                                        <% int cnt = 1;%>
                                        <c:forEach items="${getCustInvoiceDetails}" var="route">
                                            <%
                                               String className = "text1";
                                               if ((index % 2) == 0) {
                                                   className = "text1";
                                               } else {
                                                   className = "text2";
                                               }
                                            %>
                                            <tr class="item-row">
                                                <td><c:out value="${route.dealerName}"/></td>
                                                <td><c:out value="${route.address}"/></td>
                                                <td><c:out value="${route.city}"/></td>
                                                <td><c:out value="${route.pincode}"/></td>
                                                <td>
                                                    <c:out value="${route.totQty}"/>
                                                </td>
                                                <td>
                                                    <c:out value="${route.invoiceNumber}"/>
                                                </td>
                                                <td>
                                                    <c:out value="${route.totTaxVol}"/>
                                                    <input type="hidden" id="invoiceNumber<%=cnt%>" value="<c:out value="${route.invoiceNumber}"/>">
                                                    <input type="hidden" id="quantity<%=cnt%>" value=" <c:out value="${route.quantity}"/>">
                                                    <input type="hidden" id="itemid<%=cnt%>" value=" <c:out value="${route.itemId}"/>">
                                                    <input type="hidden" id="tempitemid<%=cnt%>" name="tempitemid" value=" ">

                                                    <input type="hidden" id="orderId<%=cnt%>" value=" <c:out value="${route.invoiceNos}"/>">
                                                    <input type="hidden" id="customerId<%=cnt%>" name ="customerId" value=" <c:out value="${route.dealerId}"/>">
                                                    <!--<input type="hidden" id="taxvalue<%=cnt%>" name ="taxvalue" value=" <c:out value="${route.taxValue}"/>">-->
                                                    <input type="hidden" id="pretaxValue<%=cnt%>" name ="pretaxValue" value=" <c:out value="${route.pretaxValue}"/>">
                                                    <input type="hidden" id="invoiceAmount<%=cnt%>" name ="invoiceAmount" value=" <c:out value="${route.invoiceAmount}"/>">
                                                    <input type="hidden" id="supplierId<%=cnt%>" name ="supplierId" value=" <c:out value="${route.supplierId}"/>">
                                                    <input type="hidden" id="checked<%=cnt%>" value="">
                                                    <input type="hidden" id="tempIn<%=cnt%>" name="tempIn" value="">
                                                    <input type="hidden" id="tempQ<%=cnt%>" name="tempQ" value="">
                                                    <input type="hidden" id="tempsn<%=cnt%>" name="tempsn" value="">
                                                    <input type="hidden" id="temporderid<%=cnt%>" name="temporderid" value="">
                                                    <input type="hidden" id="tempcustomerId<%=cnt%>" name="tempcustomerId" value="">
                                                    <input type="hidden" id="tempInvoiceAmt<%=cnt%>" name="tempInvoiceAmt" value="">
                                                    <input type="hidden" id="tempPerTax<%=cnt%>" name="tempPerTax" value="">
                                                    <input type="hidden" id="tempTax<%=cnt%>" name="tempTax" value="">
                                                    <input type="hidden" id="tempsuplierId<%=cnt%>" name="tempsuplierId" value="">

                                                </td>
                                                <td>
                                                    <c:out value="${route.invoiceAmounts}"/>
                                                </td>
                                                <td>
                                                    <c:out value="${route.pretaxValues}"/>
                                                </td>
                                                <td>
                                                    <c:out value="${route.basePrice}"/>
                                                </td>

                                                <td >
                                                    <span  data-toggle="modal" data-target="#myModal"><input type="checkbox"  name="selectedIndex" id="selectedIndex<%=index%>"  value="<%=cnt%>"  onclick="add(<%=cnt%>);"/></span></td>
                                                </td>

                                            </tr>
                                            <%index++;%>
                                            <%cnt++;%>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </c:if>


                            <script>
                                function invoiceSelectAll(element) {
                                    var selectedIndex = document.getElementsByName("selectedIndexs");
                                    if (element.checked == true) {
                                        for (var i = 0; i < selectedIndex.length; i++) {
                                            selectedIndex[i].checked = true;
                                        }
                                    } else {
                                        for (var i = 0; i < selectedIndex.length; i++) {
                                            selectedIndex[i].checked = false;
                                        }
                                    }
                                    selected();
                                }
                                function add(val) {
                                    var selectedIndex = document.getElementsByName("selectedIndex");
                                    var selectedIndextem = document.getElementById("selectedIndex" + val).value;
                                    var suppId = document.getElementById("suppId").value;
                                    if (suppId == "") {
                                        alert("Please Choose Supplier Name")
                                    } else {
                                        document.getElementById("suppId").disabled = true;
                                        for (var i = 0; i < selectedIndex.length; i++) {

                                            if (selectedIndex[i].value != selectedIndextem) {
                                                selectedIndex[i].checked = false;
                                            }
                                        }


                                        var invoiceNo = document.getElementById("invoiceNumber" + val).value;
                                        var itemid = document.getElementById("itemid" + val).value;
                                        var quantity = document.getElementById("quantity" + val).value;
                                        var orderId = document.getElementById("orderId" + val).value;
                                        var customerId = document.getElementById("customerId" + val).value;
                                        var supplierId = document.getElementById("supplierId" + val).value;
                                        var checked = document.getElementById("checked" + val).value;
                                        var pretaxValue = document.getElementById("pretaxValue" + val).value;
                                        var invoiceAmount = document.getElementById("invoiceAmount" + val).value;



                                        var tables = document.getElementById("tbody");
                                        tables.innerHTML = '';
                                        var qty = quantity.split("~");
                                        var invoice = invoiceNo.split("~");
                                        var orderIds = orderId.split("~");
                                        var checkeds = checked.split("~");
                                        var itemids = itemid.split("~");
                                        var pretaxValues = pretaxValue.split("~");
//                                    var taxvalues = taxvalue.split("~");
                                        var invoiceAmounts = invoiceAmount.split("~");


                                        var j = 0;
                                        var podSno1 = 1;

                                        for (var k = 0; k < qty.length; k++) {
                                            var newrow = tables.insertRow(k);
                                            var cell1 = newrow.insertCell(0);
                                            var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
                                            cell1.innerHTML = cell0;

                                            cell2 = newrow.insertCell(1);
                                            cell0 = "<td class='text1' height='25' ><input type='text' style='width:200px' class='form-control'  name='invoiceRow'  id='invoiceRow" + podSno1 + "'   value=" + invoice[k] + " onclick='make();' ><input type='hidden'     name='customerId'  id='customerId" + podSno1 + "'   value=" + customerId + " onclick='make();' readonly ></td>";
                                            cell2.innerHTML = cell0;


                                            cell3 = newrow.insertCell(2);
                                            cell0 = "<td class='text1' height='25'  id='deletes" + podSno1 + "'><input type='text' class='form-control' style='width:200px' name='qtyRow'  id='qtyRow" + podSno1 + "'   value=" + qty[k] + " onclick='make();' readonly> \n\
                                                        \n\
                                                                      <input type='hidden'     name='orderIdRow'  id='orderIdRow" + podSno1 + "'   value=" + orderIds[k] + " onclick='make();' >\n\
                                                                      <input type='hidden'     name='customerIdRow'  id='customerIdRow" + podSno1 + "'   value=" + customerId + " onclick='make();' >\n\
                                                                      <input type='hidden'     name='supplierIdRow'  id='supplierIdRow" + podSno1 + "'   value=" + supplierId + " onclick='make();' >\n\
                                                                      <input type='hidden'     name='itemIdRow'  id='itemIdRow" + podSno1 + "'   value=" + itemids[k] + " onclick='make();' >\n\
                                                                      <input type='hidden'     name='pretaxRow'  id='pretaxRow" + podSno1 + "'   value=" + pretaxValues[k] + " onclick='make();' >\n\
                                                                      <input type='hidden'     name='invoiceAmtRow'  id='invoiceAmtRow" + podSno1 + "'   value=" + invoiceAmounts[k] + " onclick='make();' >\n\
                                                             </td>";
                                            cell3.innerHTML = cell0;

                                            var cell4 = newrow.insertCell(3);
                                            cell0 = "<td class='text1' height='25' align='left'><input type='checkbox' class='button'    name='selectedIndexs'  id='selectedIndexs" + podSno1 + "'value=" + podSno1 + " onclick='selected();' >\n\
                                                                      \n\
                                   <input type='hidden'     name='val'  id='val" + podSno1 + "'   value=" + val + "  ></td>";
                                            cell4.innerHTML = cell0;

                                            podSno1++;
                                        }


                                        var selectedIndexs = document.getElementsByName("selectedIndexs");
                                        for (var m = 0; m < selectedIndexs.length; m++) {

                                            if (parseInt(selectedIndexs[m].value) === parseInt(checkeds[m])) {


                                                selectedIndexs[m].checked = true;
                                            }

                                        }

                                    }

                                }


                                function selected() {
                                    var index = document.getElementsByName("selectedIndexs");
                                    var cntr = 0;
                                    var temp1 = "";
                                    var temp2 = "";
                                    var temp3 = "";
                                    var temp4 = "";
                                    var temp5 = "";
                                    var temp6 = "";
                                    var temp7 = "";
                                    var temp8 = "";
                                    var temp9 = "";
                                    var temp10 = "";
                                    var j = 1;

                                    var k = 1;
                                    var count = 0;
                                    for (var i = 0; i < index.length; ++i) {


                                        var indes = document.getElementById("selectedIndexs" + j).checked;
                                        var val = document.getElementById("val" + j).value;


                                        if (indes) {
                                            count++;

                                            var qtyRow = document.getElementsByName("qtyRow");
                                            var invoiceRow = document.getElementsByName("invoiceRow");
                                            var orderId = document.getElementsByName("orderIdRow");
                                            var customerIdRow = document.getElementsByName("customerIdRow");
                                            var supplierIdRow = document.getElementsByName("supplierIdRow");
                                            var tempitemrow = document.getElementsByName("itemIdRow");
//                                            var temptaxrow = document.getElementsByName("taxValRow");
                                            var tempInvoicerow = document.getElementsByName("invoiceAmtRow");
                                            var tempPerTaxrow = document.getElementsByName("pretaxRow");
                                            console.log(tempPerTaxrow[i].value);

                                            if (cntr == 0) {
                                                temp1 = qtyRow[i].value;
                                                temp2 = invoiceRow[i].value;
                                                temp3 = j;
                                                temp4 = orderId[i].value;
                                                temp5 = customerIdRow[i].value;
                                                temp5 = customerIdRow[i].value;
                                                temp6 = tempitemrow[i].value;
                                                temp7 = tempInvoicerow[i].value;
                                                temp8 = tempPerTaxrow[i].value;
                                                temp10 = supplierIdRow[i].value;

                                                document.getElementById("checked" + val).value = j;
                                                document.getElementById("tempQ" + val).value = temp1;
                                                document.getElementById("tempsn" + val).value = temp3;
                                                document.getElementById("tempIn" + val).value = temp2;
                                                document.getElementById("temporderid" + val).value = temp4;
                                                document.getElementById("tempcustomerId" + val).value = temp5;
                                                document.getElementById("tempitemid" + val).value = temp6;
                                                document.getElementById("tempInvoiceAmt" + val).value = temp7;
                                                document.getElementById("tempPerTax" + val).value = temp8;
                                                document.getElementById("tempsuplierId" + val).value = temp10;
                                            } else {
                                                temp1 = (temp1 + "~" + (qtyRow[i].value).toString());
                                                temp2 = temp2 + "~" + (invoiceRow[i].value.toString());
                                                temp3 = temp3 + "~" + (j.toString());
                                                temp4 = temp4 + "~" + (orderId[i].value.toString());
                                                temp5 = temp5 + "~" + (customerIdRow[i].value.toString());
                                                temp6 = temp6 + "~" + (tempitemrow[i].value.toString());
                                                temp7 = temp7 + "~" + (tempInvoicerow[i].value.toString());
                                                temp8 = temp8 + "~" + (tempPerTaxrow[i].value.toString());
//                                                temp9 = temp9 + "~" + (temptaxrow[i].value.toString());
                                                temp10 = temp10 + "~" + (supplierIdRow[i].value.toString());

                                                document.getElementById("checked" + val).value = temp3;
                                                document.getElementById("tempQ" + val).value = temp1;
                                                document.getElementById("tempsn" + val).value = temp3;
                                                document.getElementById("tempIn" + val).value = temp2;
                                                document.getElementById("temporderid" + val).value = temp4;
                                                document.getElementById("tempcustomerId" + val).value = temp5;
                                                document.getElementById("tempitemid" + val).value = temp6;
                                                document.getElementById("tempInvoiceAmt" + val).value = temp7;
                                                document.getElementById("tempPerTax" + val).value = temp8;
//                                                document.getElementById("tempTax" + val).value = temp9;
                                                document.getElementById("tempsuplierId" + val).value = temp10;
                                            }
                                            cntr++;
                                        }
                                        else if (count === 0) {

                                            document.getElementById("tempQ" + val).value = "";
                                            document.getElementById("tempsn" + val).value = "";
                                            document.getElementById("tempIn" + val).value = "";
                                            document.getElementById("temporderid" + val).value = "";
                                            document.getElementById("tempcustomerId" + val).value = "";
                                            document.getElementById("tempitemid" + val).value = "";
                                            document.getElementById("tempInvoiceAmt" + val).value = "";
                                            document.getElementById("tempPerTax" + val).value = "";
//                                            document.getElementById("tempTax" + val).value = "";
                                            document.getElementById("checked" + val).value = "";
                                            document.getElementById("tempsuplierId" + val).value = "";
                                        }
                                        j++;
                                    }
                                }



                                function sub() {
                                    var invoice = document.getElementsByName("tempIn");
                                    var qty = document.getElementsByName("tempQ");
                                    var sno = document.getElementsByName("tempsn");
                                    var temporderid = document.getElementsByName("temporderid");
                                    var customerIds = document.getElementsByName("tempcustomerId");
                                    var tempitemids = document.getElementsByName("tempitemid");
                                    var tempInvoiceAmt = document.getElementsByName("tempInvoiceAmt");
                                    var tempPerTax = document.getElementsByName("tempPerTax");
//                                    var tempTax = document.getElementsByName("tempTax");
                                    var suplierIds = document.getElementsByName("tempsuplierId");
                                    var tables = document.getElementById("tbodys");
                                    tables.innerHTML = '';

                                    var podSno1 = 1;
                                    var podSno2 = 0;

                                    for (var k = 0; k < sno.length; k++) {
                                        if (invoice[k].value != "") {
                                            var splitInvoice = invoice[k].value;
                                            var splitQty = qty[k].value;
                                            var splitorderid = temporderid[k].value;
                                            var splitcustomerid = customerIds[k].value;
                                            var splitsuplierid = suplierIds[k].value;
                                            var splititemid = tempitemids[k].value;
                                            var splitInvoiceAtm = tempInvoiceAmt[k].value;
                                            var splitPerTax = tempPerTax[k].value;
//                                            var splitTax = tempTax[k].value;

                                            var invoiceSplits = splitInvoice.split("~");
                                            var qtys = splitQty.split("~");
                                            var order = splitorderid.split("~");
                                            var customer = splitcustomerid.split("~");
                                            var suplier = splitsuplierid.split("~");
                                            var itemid = splititemid.split("~");
                                            var invoices = splitInvoiceAtm.split("~");
                                            var perTax = splitPerTax.split("~");
//                                            var tax = splitTax.split("~");

                                            var totalQty = 0;
                                            var perTaxTotal = 0;
                                            var invoiceTotal = 0;


                                            for (var N = 0; N < invoiceSplits.length; N++) {
                                                if (invoiceSplits.length >= 1) {
                                                    var newrow = tables.insertRow(podSno2);
                                                    podSno2++;
                                                    var cell1 = newrow.insertCell(0);
                                                    var cell0 = "<td class='form-control' height='25' id='podSno' >" + podSno1 + "</td>";
                                                    cell1.innerHTML = cell0;
                                                    cell2 = newrow.insertCell(1);
                                                    cell0 = "<td class='form-control' height='25' > <input type='text' style='width:200px' class='form-control'    name='invoiceNumbers'  id='invoiceNumbers" + podSno1 + "'   value=" + invoiceSplits[N] + "  readonly ></td>";
                                                    cell2.innerHTML = cell0;

                                                    cell3 = newrow.insertCell(2);
                                                    cell0 = "<td class='form-control' height='25' > <input type='text' style='width:200px' class='form-control' name='invoiceValue'  id='invoiceValue" + podSno1 + "'   value=" + invoices[N] + "  readonly ></td>";
                                                    cell3.innerHTML = cell0;
                                                    cell4 = newrow.insertCell(3);
                                                    cell0 = "<td class='form-control' height='25' > <input type='text' class='form-control' style='width:200px' name='perTaxValue'  id='perTaxValue" + podSno1 + "'   value=" + perTax[N] + "  readonly ></td>";
                                                    cell4.innerHTML = cell0;

                                                    cell5 = newrow.insertCell(4);
                                                    cell0 = "<td class='form-control' height='25'  id='deletes" + podSno1 + "'><input type='text' class='form-control' style='width:200px' name='qtys'  id='qtys" + podSno1 + "'   value=" + qtys[N] + "  readonly> \n\
                                                                           <input type='hidden'     name='orderids'  id='orderids" + podSno1 + "'   value=" + order[N] + " >\n\
                                                                           <input type='hidden'     name='customerids'  id='customerids" + podSno1 + "'   value=" + customer[N] + " >\n\
                                                                           <input type='hidden'     name='itemids'  id='itemids" + podSno1 + "'   value=" + itemid[N] + " >\n\
                                                                           <input type='hidden'     name='suplierIds'  id='suplierIds" + podSno1 + "'   value=" + suplier[N] + " >\n\
                                                                        </td>";
                                                    cell5.innerHTML = cell0;

                                                    podSno1++;

                                                }
                                            }
                                            var preTaxTot = document.getElementsByName("perTaxValue");
                                            var invoiceValTot = document.getElementsByName("invoiceValue");
                                            var qtyTot = document.getElementsByName("qtys");
                                            for (var i = 0; i < preTaxTot.length; i++) {
                                                perTaxTotal = parseInt(perTaxTotal) + parseInt(preTaxTot[i].value);
                                                invoiceTotal = parseInt(invoiceTotal) + parseInt(invoiceValTot[i].value);
                                                totalQty = parseFloat(parseFloat(totalQty) + parseFloat(qtyTot[i].value));
                                                ;
                                            }

//                                                    TaxTotal += parseInt(tax[N].replace(/\,/g, ''));

                                            document.getElementById("totalInvoice").value = invoiceTotal;
//                                                    document.getElementById("totalTax").value = TaxTotal;
                                            document.getElementById("totalPerTax").value = perTaxTotal;
                                            document.getElementById("totalQty").value = totalQty;

                                        }
                                    }
                                    document.getElementById("select3").checked = false;
                                }

                            </script>

                            <!--                                              pop-up-->



                            <div class="container">

                                <!-- Trigger the modal with a button -->


                                <!-- Modal -->
                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog modal-lg">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Select Invoice Numbers</h4>
                                            </div>
                                            <div class="modal-body">

                                                <table align="center" border="0" id="tables" class="table table-info mb30 table-hover" style="width:100%" >
                                                    <thead>
                                                        <tr style="height: 40px;" id="tableDesingTH">
                                                            <th align="center">Sno</th>
                                                            <th align="center">InvoiceNo</th>
                                                            <th align="center">Qty</th>
                                                            <th align="center">select<input type="checkbox" id="select3" name="select3" onclick="invoiceSelectAll(this);"></th>

                                                        </tr></thead>
                                                    <tbody id="tbody">

                                                    </tbody>


                                                </table>

                                                <center>
                                                    <input type="button" class="btn btn-success" value="Submit" name="Submit" data-dismiss="modal" onclick="sub();" >
                                                </center>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>


                            <!--popo-->
                            <br>
                            <br>
                            <center>
                                <input type="button" class="btn btn-success" value="Submit" name="Submit"  onclick="searchDispatchDetails()">
                            </center>

                            </body>
                        </div>

                        <!--<input type="text" id="tempQty" >
                        <input type="text" id="tempsno" >-->

                        <table align="center" border="0" id="tables" class="table table-info mb30 table-hover" style="width:100%" >
                            <thead>
                                <tr style="height: 40px;" id="tableDesingTH">
                                    <th align="center">Sno</th>
                                    <th align="center">InvoiceNo</th>
                                    <th align="center">Invoice Amount</th>
                                    <th align="center">Pre-Tax</th>
                                    <th align="center">Qty</th>


                                </tr></thead>
                            <tbody id="tbodys">




                            </tbody>
                        </table>
                        <table class="table table-info mb30 table-hover">
                            <br>
                            <br>
                            <br><br>
                            <tr>
                                <td>
                                    Pre-Tax Value
                                </td>
                                <td>
                                    <input class="form-control" style="width:260px;height:40px;" type="text" id="totalPerTax" name="totalPerTax" readonly>
                                </td>
                                <td>
                                    Invoice Amount
                                </td>
                                <td>
                                    <input class="form-control" style="width:260px;height:40px;" type="text" id="totalInvoice" name="totalInvoice" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Qty
                                </td>
                                <td>
                                    <input class="form-control" style="width:260px;height:40px;" type="text" id="totalQty" name="totalQty" readonly>
                                </td>
                            </tr>

                        </table>

                        <br>
                        <br>


                        </body>



                        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>


                    </div>
        </div>
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>

    </div>
</div>






<%@ include file="/content/common/NewDesign/settings.jsp" %>