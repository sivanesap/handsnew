<%--
    Document   : viewTripSheet
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Throttle
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
        <form name="tripSheet" method="post">
            <%
                            Date dNow = new Date();
                            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                            //System.out.println("Current Date: " + ft.format(dNow));
                            String curDate = ft.format(dNow);
                            String expFile = "Completed CycleCountReport-"+curDate+".xls";

                            String fileName = "attachment;filename=" + expFile;
                            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                            response.setHeader("Content-disposition", fileName);
            %>

         
       
            <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                <thead>
                    <tr height="70">
                        <th>S. No</th>
                        <th>Crate No.</th>
                        <th>Crate Code</th>
                        <th>Warehouse Name</th>
                        <th>Barcode</th>
                        <th>Status</th>
                        <th>Created By</th>
                        <th>Created Date</th>
                        <th>Created Time</th>
                    </tr>
                </thead>
                <tbody>
                    <% int indexx = 1;%>
                    <c:forEach items="${getCrateDetailsReportPer}" var="in">
                        <%
                                    String className = "text1";
                                    if ((indexx % 2) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                        %>
                        <tr height="30">
                            <td><%=indexx%></td>
                            <td><c:out value="${in.crateNo}"/></td>
                            <td><c:out value="${in.crateCode}"/></td>
                            <td><c:out value="${in.whName}"/></td>
                            <td><c:out value="${in.barCode}"/></td>
                            <c:if test="${in.status == 0}">
                                <td> Not Mapped</td>
                            </c:if>
                            <c:if test="${in.status == 1}">
                                <td> Inward Pending</td>
                            </c:if>
                            <c:if test="${in.status == 2}">
                                <td> Outward Pending</td>
                            </c:if>
                            <c:if test="${in.status == 3}">
                                <td> Accepted</td>
                            </c:if>
                            <td><c:out value="${in.name}"/></td>
                            <td><c:out value="${in.createdDate}"/></td>
                            <td><c:out value="${in.createdTime}"/></td>
                        </tr>

                        <%indexx++;%>
                    </c:forEach>
                </tbody>
            </table>


        </form>
    </body>
</html>
