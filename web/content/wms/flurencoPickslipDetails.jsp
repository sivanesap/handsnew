<%-- 
    Document   : flurencoPickslipDetails
    Created on : 17 Aug, 2021, 9:18:37 AM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>
<script type="text/javascript">
    function generatePage() {
        document.upload.action = "/throttle/flurencoPickslip.do?&param=generate";
        document.upload.submit();
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Flurenco" text="Update PickSlip"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Flurenco" text="Flurenco"/></a></li>
            <li class=""><spring:message code="hrms.label.Flurenco" text="PickSlip Update"/></li>

        </ol>
    </div>
</div>
<script>
    function checkAll(element) {
        var checkboxes = document.getElementsByName('selectedIndex');
        if (element.checked == true) {
            for (var x = 0; x < checkboxes.length; x++) {
                checkboxes[x].checked = true;
                $('#selectedStatus' + (x + 1)).val('1');
            }
        } else {
            for (var x = 0; x < checkboxes.length; x++) {
                checkboxes[x].checked = false;
                $('#selectedStatus' + (x + 1)).val('0');
            }
        }
    }
    function searchPage() {
        document.upload.action = "/throttle/flurencoPickslip.do";
        document.upload.submit();
    }
    function checkSelectStatus(sno, obj) {
        var val = document.getElementsByName("selectedStatus");
        if (obj.checked == true) {
            document.getElementById("selectedStatus" + sno).value = 1;
        } else if (obj.checked == false) {
            document.getElementById("selectedStatus" + sno).value = 0;
        }
        var vals = 0;
        for (var i = 0; i <= val.length; i++) {
        }
    }

</script>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >
                <form name="upload"  method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    <div id="ptp" style="overflow: auto">
                        <div class="inpad">
                            <table class="table table-info mb30 table-hover" id="report" >
                                <thead>
                                    <tr>
                                        <th colspan="4" height="30" >Fill Details</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td><font color="red">*</font>Vehicle No</td>
                                    <td height="30"><input name="vehicleNo" id="vehicleNo" type="text" class="form-control" style="width:250px;height:40px" autocomplete="off" value=""></td>
                                    <td><font color="red">*</font>Driver Name</td>
                                    <td height="30"><input name="driverName" id="driverName" type="text" class="form-control" style="width:250px;height:40px" autocomplete="off" value=""></td>
                                </tr>
                                <tr>
                                    <td><font color="red">*</font>Driver Mobile</td>
                                    <td height="30"><input name="driverMobile" id="driverMobile" type="text" class="form-control" style="width:250px;height:40px" autocomplete="off" value=""></td>
                                    <td><font color="red">*</font>Delivery Executive</td>
                                    <td height="30">
                                        <select id="deliveryExe" name="deliveryExe" class="form-control" >
                                            <option value="">------Select One------</option>
                                            <c:forEach items="${deliveryExecutiveList}" var="del">
                                                <option value="<c:out value="${del.empId}"/>"><c:out value="${del.empName}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>PickUp Date</td>
                                    <td height="30"><input name="pickupDate" id="pickupDate" type="text" class="datepicker form-control" style="width:250px;height:40px" autocomplete="off" value=""></td>
                                    <td>PickUp Time</td>
                                    <td height="30"><input name="pickupTime" id="pickupTime" type="time" class="form-control" style="width:250px;height:40px" autocomplete="off" value=""></td>
                                </tr>
                                <script>

                                    $(document).ready(function() {
                                        $("#deliveryExe").CreateMultiCheckBox({width: '230px',
                                            defaultText: 'Select Below', height: '250px'});
                                    });
                                </script>

                            </table>
                            <table class="table table-info mb30 table-hover" id="table" >

                                <thead>
                                    <tr height="40">
                                        <th  height="30" >S.No</th>
                                        <th  height="30" >Order Reference No</th>
                                        <th  height="30" >Customer Name</th>
                                        <th  height="30" >Address</th>
                                        <th  height="30" >City</th>
                                        <th  height="30" >Pincode</th>
                                        <th  height="30" >No of Products</th>
                                        <th  height="30" >Quantity</th>
                                        <th  height="30" >Delivery Hub</th>
                                        <th  height="30" >Current Hub</th>
                                        <th  height="30" >Eway Bill No</th>
                                        <th  height="30" >Shipment Type</th>
                                        <th  height="30" >Order Type</th>
                                        <th  height="30" >Select<input type="checkbox" name="all" id="all" onclick="checkAll(this)"></th>

                                    </tr>
                                </thead>
                                <% int index = 0;
                                    int sno = 1;
                                %>
                                <tbody>

                                    <c:if test = "${flurencoPickslip != null}" >
                                        <c:forEach items= "${flurencoPickslip}" var="fc">
                                            <tr height="30">
                                                <td align="left"><%=sno%></td>
                                                <td align="left"   ><c:out value="${fc.orderRefNo}"/></td>
                                                <td align="left"   ><c:out value="${fc.customerName}"/></td>
                                                <td align="left"   ><c:out value="${fc.address}"/></td>
                                                <td align="left"   ><c:out value="${fc.city}"/></td>
                                                <td align="left"   ><c:out value="${fc.pincode}"/></td>
                                                <td align="left"   ><c:out value="${fc.inpQty}"/></td>
                                                <td align="left"   ><c:out value="${fc.qty}"/></td>
                                                <td align="left"   ><c:out value="${fc.deliveryHub}"/></td>
                                                <td align="left"   ><c:out value="${fc.currentHub}"/></td>
                                                <td align="left"   ><c:out value="${fc.ewayBillNo}"/></td>
                                                <td align="left"   >
                                                    <c:if test="${fc.shipmentType == '1'}">Local</c:if>
                                                    <c:if test="${fc.shipmentType == '2'}">Long Distance</c:if>
                                                    <c:if test="${fc.shipmentType == '3'}">Satellite</c:if>
                                                    </td>
                                                    <td align="left"   ><c:out value="${fc.type}"/></td>
                                                <td align="left"   ><input type="checkbox" id="selectedIndex<%=sno%>" name="selectedIndex" value="" onclick="checkSelectStatus('<%=sno%>', this)"/></td>
                                        <input type="hidden" name="orderId" id="orderId<%=sno%>" value="<c:out value="${fc.orderId}"/>" />
                                        <input type="hidden" name="selectedStatus" id="selectedStatus<%=sno%>" value="0" />
                                        </tr>
                                        <%
                                            sno++;
                                            index++;
                                        %>
                                    </c:forEach>
                                    <input type="hidden" name="sno" id="sno" value="<%=sno%>"/>
                                </c:if>
                                </tbody>
                            </table>
                            <center>
                                <input type="button" class="btn btn-success"   value="Generate" onclick="generatePage()">
                            </center>

                        </div>
                    </div>    

                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>

	
	
	
