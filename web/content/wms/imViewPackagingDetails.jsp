<%-- 
    Document   : imViewPackagingDetails
    Created on : 4 Dec, 2021, 12:11:53 AM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Packaging Details View- Machine Wise</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html">InstaMart</a></li>
                <li class="">Packaging Details View- Machine Wise</li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="material"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>

                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30" style="color: white">
                                    <th>S.No</th>
                                    <th>Machine</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Vendor</th>
                                    <th>Employee Name</th>
                                    <th>From Time</th>
                                    <th>To Time</th>
                                    <th>Total Qty</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                                <c:if test = "${packMaster != null}">
                                    <c:forEach items="${packMaster}" var="pc">
                                        <%
                                            sno++;
                                            String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.machineName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.date}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.time}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.vendorName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.empName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.loadingStartTime}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.loadingEndTime}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.qty}" /></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>
                        <br>
                        <br>
                        <table class="table table-info mb30 table-hover" id="table2" >	
                            <thead>

                                <tr height="30" style="color: white">
                                    <th>S.No</th>
                                    <th>SKU Code</th>
                                    <th>SKU Description</th>
                                    <th>UOM</th>
                                    <th>Per Pack Qty</th>
                                    <th>Packed Qty</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno1 = 0;%>
                                <c:if test = "${packDetails != null}">
                                    <c:forEach items="${packDetails}" var="pc">
                                        <%
                                            sno1++;
                                            String className1 = "text1";
                                            if ((sno1 % 1) == 0) {
                                                className1 = "text1";
                                            } else {
                                                className1 = "text2";
                                            }
                                        %>

                                        <tr>
                                            <td class="<%=className1%>"  align="left"> <%=sno1%> </td>
                                            <td class="<%=className1%>"  align="left"> <c:out value="${pc.skuCode}" /></td>
                                            <td class="<%=className1%>"  align="left"> <c:out value="${pc.productDescription}" /></td>
                                            <td class="<%=className1%>"  align="left"> <c:out value="${pc.uomName}" /></td>
                                            <td class="<%=className1%>"  align="left"> <c:out value="${pc.qty}" /></td>
                                            <td class="<%=className1%>"  align="left"> <c:out value="${pc.packQty}" /></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>


                        <input type="hidden" name="count" id="count" value="<%=sno1%>" />

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>

                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>