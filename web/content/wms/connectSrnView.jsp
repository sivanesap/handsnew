<%-- 
    Document   : connectSrnView
    Created on : 31 Aug, 2021, 9:21:29 AM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
function submitPage(){
    document.upload.action= '/throttle/connectSrnView.do';
    document.upload.submit();
}
</script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<style type="text/css">

</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="SRN View"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.HS Connect" text="HS Connect"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="SRN View"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form name="upload"  method="post">
                    <%@ include file="/content/common/message.jsp" %>


                    <div id="ptp">
                        <div class="inpad" style=" overflow-x: visible;">
                            <table class="table table-info mb30 table-hover">
                                <tr>
                                    <td>From Date</td>
                                    <td><input type="text" class="form-control datepicker"  autocomplete="off" id="fromDate" name="fromDate" value="<c:out value="${fromDate}"/>" style="width:240px;height:40px"/></td>
                                    <td>To Date</td>
                                    <td><input type="text" class="form-control datepicker" autocomplete="off" id="toDate" name="toDate" value="<c:out value="${toDate}"/>" style="width:240px;height:40px"/></td>
                                </tr>
                            </table>
                                <center><input type="button" class="btn btn-success" value="Submit" onclick="submitPage()"></center>
                                <br>
                                <br>
                            <c:if test = "${connectSrn != null}" >

                                <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                    <thead style="font-size:12px">
                                        <tr height="40">
                                            <th  height="30" >S No</th>
                                            <th  height="30" >Party Name</th>
                                            <th  height="30" >Sold To Party Code</th>
                                            <th  height="30" >Ship To Party Code</th>
                                            <th  height="30" >Party's DC Nos</th>
                                            <th  height="30" >DC Date</th>
                                            <th  height="30" >Material Code</th>
                                            <th  height="30" >Material Description</th>
                                            <th  height="30" >Quantity</th>                                                    
                                            <th  height="30" >Received On</th>
                                            <th  height="30" >Transport Name</th>
                                            <th  height="30" >Internal Order No</th>
                                            <th  height="30" >Internal Order Date</th>
                                            <th  height="30" >Remarks</th>
                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                     int count=0;
                                    %> 
                                    <tbody>

                                        <c:forEach items= "${connectSrn}" var="srn">
                                            <tr height="30">
                                                <td align="left"><%=sno%></td>
                                                <td align="left"   ><c:out value="${srn.partyName}"/></td>
                                                <td align="left"   ><c:out value="${srn.soldTo}"/></td>
                                                <td align="left"   ><c:out value="${srn.shipTo}"/></td>
                                                <td align="left"   ><c:out value="${srn.partyDcNos}"/></td>
                                                <td align="left"   ><c:out value="${srn.dcDate}"/></td>
                                                <td align="left"   ><c:out value="${srn.materialCode}"/></td>
                                                <td align="left"   ><c:out value="${srn.materialDescription}"/></td>
                                                <td align="left"   ><c:out value="${srn.qty}"/></td>
                                                <td align="left"   ><c:out value="${srn.date}"/></td>
                                                <td align="left"   ><c:out value="${srn.vendorName}"/></td>
                                                <td align="left"   ><c:out value="${srn.orderNo}"/></td>
                                                <td align="left"   ><c:out value="${srn.orderDate}"/></td>
                                                <td align="left"   ><c:out value="${srn.remarks}"/></td>
                                                
                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                    </tbody>
                                </table>

                            </c:if>
                        </div>
                    </div>     

                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>


