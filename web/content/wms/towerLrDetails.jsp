<%-- 
    Document   : towerLrDetails
    Created on : 14 Feb, 2022, 2:51:11 AM
    Author     : alan
--%>

<!DOCTYPE html>
<html>
    <%@page language="java" contentType="text/html; charset=UTF-8"%>
    <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
    <%@page import="java.util.Date"%>
    <%@page import="java.text.SimpleDateFormat"%>
    <%@page import="java.text.DateFormat"%>
    <head>
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
        <script language="javascript" src="/throttle/js/validate.js"></script>  
        <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
        <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
        <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
        <script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
        <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

        <style type="text/css" title="currentStyle">
            /*@import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";*/
        </style>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            body {font-family: Arial;}

            /* Style the tab */
            .tab {
                overflow: hidden;
                border: 1px solid #ccc;
                background-color: red;
                box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            }

            /* Style the buttons inside the tab */
            .tab button {
                background-color: inherit;
                float: left;
                border: none;
                outline: none;
                cursor: pointer;
                padding: 14px 16px;
                transition: 0.3s;
                font-size: 17px;
                color: white;
                width: 200px;
                font-weight: bolder;
                cursor: default;
            }

            /* Change background color of buttons on hover */
            .tab button:hover {
                background-color: #bd3324;
                padding: 6px 2px;
                 cursor: default;
                margin: 0px 2px;
                border-radius: 15px 15px 0px 0px;
            }

            /* Create an active/current tablink class */
            .tab button.active {
                background-color: #168cfa;
                /*padding: 0px 3px;*/
                 cursor: default;
                border-radius: 15px 15px 0px 0px;
                margin: 0px 2px;
            }

            /* Style the tab content */
            .tabcontent {
                display: none;
                padding: 6px 12px;
                border: 1px solid #ccc;
                border-top: none;
            }
        </style>

        <style>

            * {
                box-sizing: border-box;
            }
            .ss{
                margin-top: 15px;
            }
            .cmd{
                margin: 20px 5px;
                padding: 5px;
                box-shadow: rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px;            }
            .row::after {
                content: "";
                clear: both;
                display: block;
            }

            [class*="col-"] {
                float: left;
                padding: 15px;
            }

            html {
                font-family: "Lucida Sans", sans-serif;
            }

            .header {
                background-color: white;
                color: red;
                padding: 15px;
            }

            .menu ul {
                list-style-type: none;
                margin: 0;
                padding: 0;
            }

            .menu li {
                padding: 8px;
                margin-bottom: 7px;
                background-color: red;
                color: #ffffff;
                box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            }

            .menu li:hover {
                background-color: #fa3250;
            }

            .aside {
                background-color: red;
                padding: 15px;
                color: #ffffff;
                text-align: center;
                font-size: 14px;
                box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            }

            .footer {
                background-color: red;
                color: #ffffff;
                text-align: center;
                font-size: 12px;
                padding: 15px;
            }
            .hu{
                font-size:200%; 
            }

            /* For desktop: */
            .col-1 {width: 8.33%;}
            .col-2 {width: 16.66%;}
            .col-3 {width: 25%;}
            .col-4 {width: 33.33%;}
            .col-5 {width: 41.66%;}
            .col-6 {width: 50%;}
            .col-7 {width: 58.33%;}
            .col-8 {width: 66.66%;}
            .col-9 {width: 75%;}
            .col-10 {width: 83.33%;}
            .col-11 {width: 91.66%;}
            .col-12 {width: 100%;}



            #uu{
                background-color: rgb(97, 218, 17);
            }

            .header1{
                text-align: center;
            }


            #main {
                text-align: center;


            }

            .selectit{
                padding: 15px;
                display: inline-block;
            }

            .table {
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 5%;
            }

            .th, .td {
                text-align: center;
                padding: 8px;

            }

            .btnx{
                /* border: rgb(37, 2, 2) 2px solid; */
                background-color: #1937af;
                color: white;
                border-radius: 10%;
                padding: 10px;
                border: none;
                width: 100px;
                height: 50px;
                /* display: block;
                text-align: center; */
            }
            .tr:nth-child(even) {background-color: #f2f2f2;}

            .tdx{
                background-color: rgb(9, 57, 160);
                text-align: left;
                padding: 8px;
                color: white;
            }

            .btnm{
                /* border: rgb(37, 2, 2) 2px solid; */
                background-color: white;
                color: rgb(83, 11, 167);
                border-radius: 50%;
                padding: 10px;
                border: #1937afd2 2px solid;
                width: 100px;
                height: 100px;
            }
            /*                td{
                                font-size: 150%;
                            }*/

            th{
                background-color:  red;
                color: white;
            }
            .cs{
                border: none;
            }


        </style>

        <style>

            .al{
                text-align: center;
            }

        </style>

    </head>
    
    <body onload="openCity(event, '<c:out value="${d}"/>')">

        <script>










            $(document).ready(function () {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function () {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });



            function searchPage() {
                document.DayX.action = "/throttle/towerLrDetails.do?param=search";
                document.DayX.submit();
            }


            function searchPage1() {
                document.RangeX.action = "/throttle/towerLrDetails.do?param=search1";
                document.RangeX.submit();
            }




            function testing(value) {


                var date = new Date();
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();

                if (value == 1) {

                    day = day - 1
                }


                if (day.toString().length == 1) {
                    day = "0" + day;
                }
                if (month.toString().length == 1) {
                    month = "0" + month;
                }
                if (year.toString().length == 1) {
                    year = "0" + year;
                }
                finalDate = day + "-" + month + "-" + year;

                if (value == "") {

                }else{
                    document.getElementById("dayData").value = finalDate;
                    
                }
//                                
//                                const yesterday = new Date(new Date().setDate(new Date().getDate() - 1))
//                                alert(yesterday)
            }



            function choiceDay() {

                var date = document.getElementById("Date").value;
                date.toString();
                var day = date[3] + date[4]
                var month = date[0] + date[1]
                var year = date[6] + date[7] + date[8] + date[9]
//    alert(day+"-"+month+"-"+year);
//    document.getElementById("Date").value = day+"-"+month+"-"+year;
                document.getElementById("dayData").value = day + "-" + month + "-" + year;

            }
            function fromDates() {

                var date = document.getElementById("fromDate").value;
                date.toString();
                var day = date[3] + date[4]
                var month = date[0] + date[1]
                var year = date[6] + date[7] + date[8] + date[9]
//    alert(day+"-"+month+"-"+year);
//    document.getElementById("Date").value = day+"-"+month+"-"+year;
                document.getElementById("fromThisDay").value = day + "-" + month + "-" + year;

            }
            function toDates() {

                var date = document.getElementById("toDate").value;
                date.toString();
                var day = date[3] + date[4]
                var month = date[0] + date[1]
                var year = date[6] + date[7] + date[8] + date[9]
//    alert(day+"-"+month+"-"+year);
//    document.getElementById("Date").value = day+"-"+month+"-"+year;
                document.getElementById("toThisDay").value = day + "-" + month + "-" + year;

            }



        </script>


        <div class="tab">
            <button class="tablinks" onclick="openCity(event, 'Day')">Day</button>
            <button class="tablinks" onclick="openCity(event, 'Range')">Range</button>
            <button class="tablinks" onclick="dash()">Dashboard</button>

        </div>

        <div id="Day" class="tabcontent">




            <form name="DayX" method="post">





                <input id = "dayData" name = "dayData">
              


                <div style="text-align: center;">      
                    <div class="header">
                        <img  src="/throttle/images/HSSupplyLogo.png" alt="" class="img-fluid"/>
                        <img width ="50%" src="/throttle/images/wheelocity.png" alt="" class="img-fluid"/>

                        <span class = "hu" style=" padding-left: 20px">LR Report</span>
                    </div>    
                </div>       




                <div class="container">
                    <!--                                            <div class="row">
                                                                    <div class="col">
                                                                        LR Generated
                                                                    </div>
                                                                    <div class="col">
                                                                        GPS Placed 
                                                                    </div>
                                                                    <div class="col">
                                                                        GPS Not Placed
                                                                    </div>
                                                                       <div class="col">
                                                                        Freight Update
                                                                    </div>
                                                                        <div class="col">
                                                                        Freight Pending
                                                                    </div>
                                                                    
                                                                </div>-->
                </div>
                <div class="row">
                    <div class="col" style="border-top:10px solid red ;padding-top: 10px">
                        <select id="whId" name="whId" style="width:240px;height:40px;border:3px solid red;" class="form-control" >
                            <option value="">------Select Warehouse------</option>
                            <c:forEach items="${getWareHouseList}" var="wh">
                                <option value="<c:out value="${wh.warehouseId}"/>"><c:out value="${wh.whName}"/></option>
                            </c:forEach>
                        </select>
                        <p style="text-align:center;background: red;color: white; display: inline-block ;width: 240px">: Warehouse :
                        </p>
                    </div>
                    <div class="col" style="border-top:10px solid red ;padding-top: 10px">
                        <select id="current" name="current" style="width:240px;height:40px;border:3px solid red;" class="form-control" onchange="testing(this.value)">
                            <option value="">All</option>
                            <option value="0">Today</option>
                            <option value="1">Yesterday</option>
                        </select>
                        <p style="text-align:center;background: red;color: white; display: inline-block;width: 240px">: Day :</p>
                    </div>
                    <div class="col" style="border-top:10px solid red ;padding-top: 10px ">
                        <input type="text" autocomplete="off" class="form-control datepicker" style="width:240px;height:40px;border:3px solid red;" id="Date" name="Date" value="" onchange="choiceDay()"/>
                        <p style="text-align:center;background: red;color: white; display: inline-block;width: 240px">: Date :</p>
                    </div>
                    <div class="col" style="text-align:center;border-top:10px solid red ; padding-top: 10px">
                        <p style="text-align:center;background: red;color: white; display: inline-block;width: 240px"></p>
                        <input type="button" class="btn btn-primary" name="Search"  id="Search"  value="Search" onclick="searchPage()">&nbsp;&nbsp;
                    </div>


                </div>

                <div class="row" >
                    <div class="col al" style=" background:red; color: white ; font-size:150% ; font-weight: bold; padding:10px 0px; box-shadow: rgba(0, 0, 0, 0.45) 0px 25px 20px -20px; ">
                        LR Report
                    </div>
                </div>


                <div class="col">

                    <table class='table'>


                        <tr class='tr'>
                            <th style="background:red" class ='td'>Sno</th>
                            <th style="background:red" class ='td'>LR Date</th>
                            <th style="background:red" class ='td'>LR ID</th>
                            <th style="background:red" class ='td'>No of Drops</th>
                            <th style="background:red" class ='td'>GPS ID</th>
                            <th style="background:red" class ='td'>Crate Count</th>
                            <th style="background:red" class ='td'>Contacted By</th>
                            <th style="background:red" class ='td'>Remarks</th>
                            <th style="background:red" class ='td'>Action</th>


                        </tr>

                        <% int index = 0;
                            int sno = 1;
                            int count = 0;
                        %> 



                        <c:forEach items="${towerLRDetails}" var="pc">




                            <tr class ='tr'>
                                <td class ='td'> <%=sno%> </td>
                                <td class ='td'><c:out value="${pc.date}"/></td>
                                <td class ='td'><c:out value="${pc.lrId}"/></td>
                                <td class ='td'><c:out value="${pc.noOfDrops}"/></td>
                                <td class ='td'><c:out value="${pc.noOfDrops}"/></td>
                                <td class ='td'><c:out value="${pc.noOfCrates}"/></td>
                                <td class ='td'><select value="<c:out value="${pc.contactPerson}"/>"><option value="None">None</option><option value="Email">Email</option><option value="Phone">Phone</option></select></td>
                                <td class ='td'><input type="text" class = "cs" value="<c:out value="${pc.remark}"/>"></td>
                                <td class ='td'><button class="btn btn-primary">Update</button></td>


                            </tr>

                            <%
                                sno++;
                                index++;
                            %>
                        </c:forEach>





                    </table>
                </div>



                <script>
                    var currentDate = new Date()
                    var day = currentDate.getDate();
                    var month = currentDate.getMonth() + 1;
                    var year = currentDate.getFullYear();
                            currentDate = `${day} -${month} -${year}`
                            var startTime = new Date();
                    startTime = startTime.getHours() + ":" + startTime.getMinutes() + ":" + startTime.getSeconds();

                    document.getElementById("dateTo").innerHTML = currentDate;
                    document.getElementById("timeNow").innerHTML = startTime;



                    var input = document.getElementById("myInput");
                    input.addEventListener("keyup", function (event) {
                        event.preventDefault();
                        if (event.keyCode === 13) {
                            document.getElementById("myBtn").click();
                            //                                event.preventDefault();
                            //                                alert(document.getElementById("myInput").value);

                        }
                    });


                    function onDoit() {
                        var barCode = document.getElementById("myInput").value;
                        var currentDate = new Date()
                        var day = currentDate.getDate();
                        var month = currentDate.getMonth() + 1;
                        var year = currentDate.getFullYear();
                                currentDate = `${day} -${month} -${year}`
                                var startTime = new Date();
                        startTime = startTime.getHours() + ":" + startTime.getMinutes() + ":" + startTime.getSeconds();
                        var endTime = new Date();
                        endTime = endTime.getHours() + ":" + endTime.getMinutes() + ":" + endTime.getSeconds();

                        $.ajax({
                            url: "/throttle/crateInventorySetGet.do",
                            dataType: "json",
                            data: {
                                barCode: barCode,
                                currentDate: currentDate,
                                startTime: startTime,
                                endTime: endTime
                            },
                            success: function (data) {
                                $.each(data, function (i, data) {



                                    console.log(data.hr);
                                    console.log(data.totalQty);
                                    console.log(data.available);
                                    console.log(data.notMap);
                                    console.log(data.inwardPending);
                                    console.log(data.outwardPending);

                                    document.getElementById("stock").innerHTML = data.totalQty;
                                    var scannedQty = document.getElementById("scanned").innerHTML = parseInt(data.available) + parseInt(data.notMap) + parseInt(data.inwardPending) + parseInt(data.outwardPending);
                                    document.getElementById("scanned").innerHTML = parseInt(data.available) + parseInt(data.notMap) + parseInt(data.inwardPending) + parseInt(data.outwardPending);
                                    document.getElementById("totalto").innerHTML = parseInt(data.available) + parseInt(data.notMap) + parseInt(data.inwardPending) + parseInt(data.outwardPending);
                                    document.getElementById("pending").innerHTML = parseInt(data.totalQty) - parseInt(scannedQty);
                                    document.getElementById("accepted").innerHTML = data.available;
                                    document.getElementById("notMapped").innerHTML = data.notMap;
                                    document.getElementById("inWard").innerHTML = data.inwardPending;
                                    document.getElementById("outWard").innerHTML = data.outwardPending;

                                    //                                        pending scanned stock
                                    //                                          notMapped outWard inWard accepted

                                    //                                        pending scanned stock
                                    if (data.hr === "1") {

                                        alert("Already Exits");
                                        document.getElementById("myInput").value = "";
                                    }
                                    else {
                                        //                                              alert("Added Successfully")
                                        var snd = new Audio('/throttle/alert/notsend.mp3');
                                        snd.play();
                                        document.getElementById("myInput").value = "";
                                        document.getElementById("myInput").focus();


                                    }


                                    //                                          

                                });
                            }






                        });
                    }



                </script>

            </form>

        </div>

        <div id="Range" class="tabcontent">


            <form name="RangeX" method="post">
                <input type="hidden" id = "fromThisDay" name = "fromThisDay">
                <input type="hidden" id = "toThisDay" name = "toThisDay">

                <div style="text-align: center;">      
                    <div class="header">
                        <img  src="/throttle/images/HSSupplyLogo.png" alt="" class="img-fluid"/>
                        <img width ="50%" src="/throttle/images/wheelocity.png" alt="" class="img-fluid"/>

                        <span class = "hu" style=" padding-left: 20px"> LR Report</span>
                    </div>    
                </div>       




                <div class="row">
                    <div class="col" style="border-top:10px solid red ;padding-top: 10px">
                        <select id="whId1" name="whId1" style="width:240px;height:40px;border:3px solid red;" class="form-control">
                            <option value="">------Select Warehouse------</option>
                            <c:forEach items="${getWareHouseList}" var="wh">
                                <option value="<c:out value="${wh.warehouseId}"/>"><c:out value="${wh.whName}"/></option>
                            </c:forEach>
                        </select>
                        <p style="text-align:center;background: red;color: white; display: inline-block ;width: 240px">: Warehouse :
                        </p>
                    </div>
                    <div class="col" style="border-top:10px solid red ;padding-top: 10px ">
                        <input type="text" autocomplete="off" class="form-control datepicker" style="width:240px;height:40px;border:3px solid red;" id="fromDate" name="fromDate" value="<c:out value="${fromDate}"/>" onchange="fromDates()"/>
                        <p style="text-align:center;background: red;color: white; display: inline-block;width: 240px">: From Date :</p>
                    </div>
                    <div class="col" style="border-top:10px solid red ;padding-top: 10px ">
                        <input type="text" autocomplete="off" class="form-control datepicker" style="width:240px;height:40px;border:3px solid red;" id="toDate" name="toDate" value="<c:out value="${toDate}"/>" onchange="toDates()"/>
                        <p style="text-align:center;background: red;color: white; display: inline-block;width: 240px">: To Date :</p>
                    </div>
                    <div class="col" style="text-align:center;border-top:10px solid red ; padding-top: 10px">
                        <p style="text-align:center;background: red;color: white; display: inline-block;width: 240px"></p>
                        <input type="button" class="btn btn-primary" name="Search"  id="Search"  value="Search" onclick="searchPage1()">&nbsp;&nbsp;
                    </div>


                </div>

                <div class="row" >
                    <div class="col al" style=" background:red; color: white ; font-size:150% ; font-weight: bold; padding:10px 0px; box-shadow: rgba(0, 0, 0, 0.45) 0px 25px 20px -20px; ">
                        LR Report
                    </div>
                </div>






                <div class="col">

                    <table class='table'>


                        <tr class='tr'>
                            <th style="background:red" class ='td'>Sno</th>
                            <th style="background:red" class ='td'>LR Date</th>
                            <th style="background:red" class ='td'>LR ID</th>
                            <th style="background:red" class ='td'>No of Drops</th>
                            <th style="background:red" class ='td'>GPS ID</th>
                            <th style="background:red" class ='td'>Crate Count</th>
                            <th style="background:red" class ='td'>Contacted By</th>
                            <th style="background:red" class ='td'>Remarks</th>
                            <th style="background:red" class ='td'>Action</th>


                        </tr>

                        <% int index1 = 0;
                            int sno1 = 1;
                            int count1 = 0;
                        %> 



                        <c:forEach items="${towerLRDetails2}" var="pc">




                            <tr class ='tr'>
                                <td class ='td'> <%=sno1%> </td>
                                <td class ='td'><c:out value="${pc.date}"/></td>
                                <td class ='td'><c:out value="${pc.lrId}"/></td>
                                <td class ='td'><c:out value="${pc.noOfDrops}"/></td>
                                <td class ='td'><c:out value="${pc.noOfDrops}"/></td>
                                <td class ='td'><c:out value="${pc.noOfCrates}"/></td>
                                <td class ='td'><select value="<c:out value="${pc.contactPerson}"/>"><option value="None">None</option><option value="Email">Email</option><option value="Phone">Phone</option></select></td>
                                <td class ='td'><input type="text" class = "cs" value="<c:out value="${pc.remark}"/>"></td>
                                <td class ='td'><button class="btn btn-primary">Update</button></td>


                            </tr>

                            <%
                                sno1++;
                                index1++;
                            %>
                        </c:forEach>





                    </table>
                </div>


            </form>
    </body>

</div>

<script>

    function dash() {
//                       window.location("localhost:8080throttle/login.do?")
        location.replace("/throttle/dashboardPage.do?");

    }



    function openCity(day, range) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(range).style.display = "block";
        day.currentTarget.className += " active";
    }
</script>

</body>
</html> 

