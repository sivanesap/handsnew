<%-- 
    Document   : vehicleUtilization
    Created on : 13 Nov, 2021, 6:02:58 PM
    Author     : alan
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<!--<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>-->
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<style>
    .form-control:focus{border-color: #5cb85c;  box-shadow: none; -webkit-box-shadow: none;} 
    .has-error .form-control:focus{box-shadow: none; -webkit-box-shadow: none;}
</style>
<meta http-equiv="ConConsignment Notent-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">


    function searchPage(val) {
        document.schedule.action = "/throttle/vehicleUtilization.do?&param=search";
        document.schedule.submit();
    }


</script>
<style>
    #rcorners1 {
        border-radius: 35px;
        background: url(paper.gif);
        background-position: left top;
        background-repeat: repeat;
        padding: 5px; 
        width: 400px;
        height: 150px;
    }
</style>

<script>
    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>



</script>

</head>


<script type="text/javascript">

</script>

<body >




    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Vehicle Utilization" text="Vehicle Utilization"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Vehicles"/></a></li>
                <li class=""><spring:message code="hrms.label.Vehicle Utilization" text="Vehicle Utilizatione"/></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">

        <div class="panel panel-default">
            <div class="panel-body">
                <%@ include file="/content/common/message.jsp" %>

                <form name="schedule" method="post" >
                    <table class="table table-info mb30 table-hover" >
                        <thead><tr><th colspan="10">Delivery Schedule</th></tr></thead>
                        <tr>
                            <td><font color="red">*</font>From Date </td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text"  class="datepicker , form-control" style="width:250px;height:40px" ></td>
                            <td></td>
                            <td><font color="red">*</font>To Date </td>
                            <td height="30">
                                <input name="toDate" id="toDate" type="text" class="datepicker , form-control" style="width:250px;height:40px" class="datepicker">
                            </td>
               
                        </tr>
                        <tr>
                            <td>Customer</td>
                            <td><select id="customer" name="customer" style="width:250px;height:40px"class="form-control">
                                    <option value="">---Select---</option>
                                    <c:forEach items="${vehicleUtilizationCust}" var="cust">
                                        <option value="<c:out value="${cust.custId}"/>"><c:out value="${cust.custName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                             <td></td>
                                                        <td>Warehouse</td>
                            <td><select id="Warehouse" name="Warehouse" style="width:250px;height:40px"class="form-control">
                                    <option value="">---Select---</option>
                                    <c:forEach items="${getWareHouseList}" var="c">
                                        <option value="<c:out value="${c.warehouseId}"/>"><c:out value="${c.whName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>City</td>
                            <td><select id="city" name="city" style="width:250px;height:40px"class="form-control">
                                    <option value="">---Select---</option>
                                    <c:forEach items="${getCityList}" var="citys">
                                        <option value="<c:out value="${citys.id}"/>"><c:out value="${citys.city}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td></td>
                                    <td>State</td>
                            <td><select id="state" name="state" style="width:250px;height:40px"class="form-control">
                                    <option value="">---Select---</option>
                                    <c:forEach items="${stateList}" var="states">
                                        <option value="<c:out value="${states.stateId}"/>"><c:out value="${states.stateName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                            
                        </tr>
                        <td></td>
                          <td></td>
                         <td height="30" >
                                                     <center> 
                                <input type="button" class="btn btn-success"   value="Search" onclick="searchPage('Search')">
                                            </center>

                                   </td>
                                </table>
                    <br>
                    <c:if test = "${getTripDetailsReport != null}" >

                        <table class="table table-info mb30 table-hover" id="table" >
                            <thead>
                                <tr height="40">
                                    <th>S.No</th>    
                                    <th>Vehicle No</th>

                                    <th>Trip Count</th>                                    

                                </tr>
                            </thead>
                            <% int index = 0;
                                int sno = 1;
                              
                            %>
                            <tbody>
                                <c:forEach items="${getTripDetailsReport}" var="cnl">

                                    <tr height="30">
                                        <td align="left" ><%=sno%>
                                        </td>
                                        <td align="left" ><c:out value="${cnl.vehicleNo}"/></td>

                                        <td align="left" ><c:out value="${cnl.tripCount}"/></td>

                                    </tr>
                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>

                            </tbody>
                        </table>
                    </c:if>



                </form>
                </body>
            </div>
        </div>

        <%@ include file="../common/NewDesign/settings.jsp" %>

        <!--        
                
                
                    if (wms.getFromDate() != null && wms.getFromDate() != "") {
                        if (wms.getFromDate().split("-")[2].length() != 2) {
                            map.put("fromDate", wms.getFromDate().split("-")[2] + "-" + wms.getFromDate().split("-")[1] + "-" + wms.getFromDate().split("-")[0]);
                            map.put("toDate", wms.getToDate().split("-")[2] + "-" + wms.getToDate().split("-")[1] + "-" + wms.getToDate().split("-")[0]);
                        } else {
                            map.put("fromDate", wms.getFromDate());
                            map.put("toDate", wms.getToDate());
                        }
                    }-->