<%-- 
    Document   : dzStockDetails
    Created on : 16 Dec, 2021, 12:14:11 PM
    Author     : alan
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Stock Details</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html">dunzo</a></li>
                <li class="">Stock Details</li>

            </ol>
        </div>
    </div>
              
                <script>
                    function setWarehouse(){
                        var warehouseId = document.getElementById("whId").value;
                        document.getElementById("id").value = warehouseId ;
//                        alert(warehouseId)
                    }
                    
                    
                    
                    function submitPage(){
                        var id = document.getElementById("id").value;
                        document.material.action = '/throttle/dzStockDetails.do?param=save&warehouseId=' + id ;
                        document.material.submit();
                     }
                     
                     
                           
                    function exportPage(){
                        var id = document.getElementById("id").value;
                        document.material.action = '/throttle/dzStockDetails.do?param=export&warehouseId=' + id ;
                        document.material.submit();
                     }
                    
                </script>      
                
    <script>
      $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
  
</script>                
                
              
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="material"  method="post" >
                        <br>
                        
                        
                        
                        
                        
                        <%@ include file="/content/common/message.jsp" %>
                        
                        
                        <table class="table table-info mb30 table-hover" >
                            
                               <thead> 
                            <tr>
                                <th  colspan="10">Stock Details</th>
                            </tr>

                        </thead>
                        <tr>
                            <td>From Date</td>
                            <td>
                                <input type="text" autocomplete="off" class="form-control datepicker" style="width:260px;height:40px" id="fromDate" name="fromDate" value="">
                            </td>
                            <td>To Date</td>
                            <td>
                                <input type="text" autocomplete="off" class="form-control datepicker" style="width:260px;height:40px" id="toDate" name="toDate" value="">
                            </td>
                        </tr>
                        <tr>

                            <td align="center"><font color="red">*</font>warehouse</td>
                            <td height="30"> <select name="whId" id="whId" class="form-control" style="width:260px;height:40px;" onchange="setWarehouse()">
                                    <c:if test="${getWareHouseList != null}">
                                        <option value="" selected>----All Warehouse----</option>
                                        <c:forEach items="${getWareHouseList}" var="wh">
                                            <option value='<c:out value="${wh.warehouseId}"/>'><c:out value="${wh.whName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select></td>
                  
                        </tr>
                        <input type="hidden" class="btn btn-success" name="id"  id="id"  />
                   
                    </table>
                    <center>

                        <input type="button" class="btn btn-success" name="Search"  id="Search"  value="Search" onclick="submitPage()">&nbsp;&nbsp;
                        <input type="button" class="btn btn-success" name="Export"  id="Export"  value="Export" onclick="exportPage()">&nbsp;&nbsp;

                    </center>
                        <br>
                        
                        <br>

                   
                            <c:if test = "${getStockDetails != null}" >

                                <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                    <thead style="font-size:12px">
                                        <tr height="40">
                                            <th>Sno</th>
                                            <th>product_id</th>
                                            <th>Product Code</th>
                                            <th>Product Name</th>
                                            <th>product_variant_id</th>
                                            <th>uom</th>
                                            <th>qty</th>
                                            <th>used_qty</th>
                                            

                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                     int count=0;
                                    %> 
                                    <tbody>

                                        <c:forEach items="${getStockDetails}" var="pc">


                                            <tr>
                                                <td align="left"> <%=sno%> </td>
                                                <td align="left"> <c:out value="${pc.productId}"/></td>
                                                <td align="left"> <c:out value="${pc.productCode}"/></td>
                                                <td align="left"> <c:out value="${pc.productName}"/> </td>
                                                <td align="left"> <c:out value="${pc.productVariantId}"/></td>
                                                <td align="left"> <c:out value="${pc.uom2}"/> </td>
                                                <td align="left"> <c:out value="${pc.qty}"/> </td>
                                                <td align="left"> <c:out value="${pc.usedQty}"/> </td>
                                             
                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                    </tbody>
                                </table>

                              
                            </c:if>



                        <br>
                        <br>
                   

                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
