<%-- 
    Document   : shipmentViewReportExcel
    Created on : 29 Oct, 2021, 6:52:59 PM
    Author     : Roger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Excel";
        request.setAttribute("menuPath", menuPath);
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "shipmentViewReportExcel-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
      <c:if test = "${ordersList != null}" >
        <table class="table table-info mb30 table-hover" id="table" >
            <thead>
                <tr height="40">
                    <th>Sno</th>
                    <th width="20%">LoanProposalID</th>
                    <th>Customer</th>                    
                    <th>PinCode</th>
                    <th>Product Name</th>
                    <th>Status</th>
                    <th width="10%">Delivered Date & Time</th>
                    <th>Invoice Number</th>
                    <th>Invoice View</th>
                    <th>Order View</th>
                </tr>
            </thead>
            <% int index = 0;
                        int sno = 1;
                        int deliveredCount=0;
            %>
            <tbody>
                <c:forEach items="${ordersList}" var="order">

                    <tr height="30">
                        <td align="left" ><%=sno%></td>
                        <td align="left" ><c:out value="${order.orderNo}"/></td>
                        <td align="left" ><c:out value="${order.clientName}"/></td>                        
                        <td align="left" ><c:out value="${order.pinCode}"/></td>             
                        <td align="left" ><c:out value="${order.model}"/></td>               
                        <td align="left" >                           
                            <c:if test="${order.cancel == 1}">
                                Order Canceled
                            </c:if>
                            <c:if test="${order.cancel != 1}">
                                <c:if test="${order.invoiceStatus == 0}">
                                    Order Created
                                </c:if>
                                <c:if test="${order.invoiceStatus == 1}">
                                    Scheduled
                                </c:if>
                                <c:if test="${order.invoiceStatus == 2}">
                                    Invoice Created
                                </c:if>
                                <c:if test="${order.invoiceStatus == 3}">
                                    OFD
                                </c:if>
                                <c:if test="${order.invoiceStatus == 4}">
                                    Delivered
                                    <%
                                    deliveredCount++;
                                    %>
                                </c:if>
                                <c:if test="${order.invoiceStatus == 5}">
                                    Returned
                                </c:if>
                                <c:if test="${order.invoiceStatus == 8}">
                                    Cancelled
                                </c:if>
                                <c:if test="${order.invoiceStatus == 9}">
                                    OFD
                                </c:if>
                            </c:if>
                                    </a>
                        </td>  
                        <c:if test="${order.invoiceStatus==4}">
                            <td><c:out value="${order.modifiedDate}"/></td>
                        </c:if>
                         <c:if test="${order.invoiceStatus!=4}">
                            <td>-</td>
                        </c:if>
                        <td align="left" ><c:out value="${order.invoiceNumber}"/></td>
                        <c:if test="${order.path  !='NA'}">
                            
                        <td align="left" ><a href="<c:out value="${order.path}"/>" target="_blank" download>
                                                    <span class="label label-Primary"><font size="2">PDF</font></span> </a>
                                            </td>
                        </c:if>
                        <c:if test="${order.path == 'NA'}">
                            
                        <td align="left" > NA </td>
                        </c:if>
                                            
                        
                         <td align="left" >
                             <a href="#" style="color:#ff6699;"  onclick="orderStatus('<c:out value="${order.orderId}"/>')" ><span >View</span> </a></td> 
                    </tr>
                    <%index++;%>
                    <%sno++;%>
                </c:forEach>
            <input type="hidden" value="<%=deliveredCount%>" id="deliveredCount" name="deliveredCount">
            </tbody>
        </table>
    </c:if>
  

            <br>
            <br>

        </form>
    </body>
</html>