<%-- 
    Document   : imPodUploadExcel
    Created on : 6 Jan, 2022, 10:15:07 AM
    Author     : alan
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Excel";
        request.setAttribute("menuPath", menuPath);
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "imPod-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test = "${imPodUploadDetails != null}" >
                <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                    <thead>
                        <tr height="40">
                            <th>SNo</th>
                            <th style="width:200px;height:30px;">LRN No</th>
                            <th>Date</th>
                            <th>Consignee Name</th>
                            <th>To Location</th>
                            <th>Vehicle Type</th>
                            <th>Invoice No</th>
                            <th>Invoice Amount</th>
                        </tr>
                    </thead>
                    <% int index = 0;
                        int sno = 1;
                    %>
                    <tbody>
                        <c:forEach items="${imPodUploadDetails}" var="dc">

                        <td align="left" ><%=sno%></td>


                        <td  style="width:200px;height:30px;">
                            <c:out value="${dc.lrnNo}"/>
                        </td>
                        <td align="center"><c:out value="${dc.lrDate}"/></td>
                        <td align="center"><c:out value="${dc.lrnNo}"/></td>
                        <td align="center"><c:out value="${dc.lrDate}"/></td>
                        <td align="center"><c:out value="${dc.consigneeName}"/></td>
                        <td align="center"><c:out value="${dc.city}"/></td>
                        <td align="center"><c:out value="${dc.vehicleType}"/></td>
                        <td align="center"><c:out value="${dc.invoiceNo}"/></td>
                        <td align="center"><c:out value="${dc.invoiceAmount}"/></td>

<!--                        <td align="center">
                            <c:if test="${dc.deliveryDate!=''}">
                                <c:out value="${dc.deliveryDate}"/>
                            </c:if>
                        </td>
                        <td><c:out value="${dc.remarks}"/></td>-->
                        </tr>
                        <%index++;%>
                        <%sno++;%>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>

            <br>
            <br>
        </form>
    </body>
</html>