<%-- 
    Document   : ifbRunsheetReportExcel
    Created on : 11 Jun, 2021, 12:26:03 PM
    Author     : Roger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Excel";
        request.setAttribute("menuPath", menuPath);
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "RunsheetReport-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                <thead>
                    <tr height="40">
                        <th  height="30" >S.No</th>
                        <th  height="30" >Runsheet No</th>
                        <th  height="30" >Assigned Date</th>
                        <th  height="30" >DE Name</th>
                        <th  height="30" >Hub Name</th>
                        <th  height="30" >Vehicle No</th>
                        <th  height="30" >Total Quantity</th>
                        <th  height="30" >User Name</th>
                        <th  height="30" >Status</th>
                    </tr>
                </thead>
                <% int index = 0;
                    int sno = 1;
                %>
                <tbody>
                    <c:forEach items= "${ifbRunsheetReport}" var="ifb">
                        <tr>
                            <td align="left"><%=sno%></td>
                            <td align="left"   ><c:out value="${ifb.tripCode}"/></td>
                            <td align="left"   ><c:out value="${ifb.date}"/></td>
                            <td align="left"   ><c:out value="${ifb.driverName}"/></td>
                            <td align="left"   ><c:out value="${ifb.whName}"/></td>
                            <td align="left"   ><c:out value="${ifb.vehicleNo}"/></td>
                            <td align="left"   ><c:out value="${ifb.qty}"/></td>
                            <td align="left"   ><c:out value="${ifb.empName}"/></td>
                            <td align="left"   >
                                <c:if test="${ifb.tripStatusId==8}">
                                    <c:if test="${ifb.status==1}">
                                        Completed Delivery
                                    </c:if>
                                    <c:if test="${ifb.status==2}">
                                        Delivery Pending
                                    </c:if>
                                </c:if>
                                    <c:if test="${ifb.tripStatusId>=12}">
                                    Runsheet Closed
                                </c:if>
                            </td>
                        </tr>
                        <%index++;%>
                        <%sno++;%>
                    </c:forEach>
                </tbody>
            </table>

            <br>
            <br>

        </form>
    </body>
</html>