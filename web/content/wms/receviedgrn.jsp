<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%> 

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>

      

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Dock In"  text="Received GRN"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Received GRN"  text="Received GRN"/></a></li>
            <li class="active">Received GRN</li>
        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="sorter.size(5);">
                <form name="repair" method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <table  class="table table-info mb30 table-hover"  align="left" style="width:50%;">
                        <tr style="display:none;" >
                            <td>From Date</td>
                            <td>
                                <input name="fromDate" id="fromDate" type="text" class="form-control datepicker" onclick="ressetDate(this);" value="">
                            </td>

                            <td>To Date</td>
                            <td>
                                <input name="toDate" id="toDate" type="text" class="form-control datepicker"  onClick="ressetDate(this);" value="">
                            </td>
                            <td colspan="4" >&nbsp;</td>
                        </tr>
                    </table>
                    <c:if test="${getReceivedgrn == null }" >
                        <center><font color="red" size="2"><spring:message code="trucks.label.NoRecordsFound"  text="default text"/>  </font></center>
                        </c:if>
                    <table class="table table-info mb30 table-bordered" id="table" >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>GTN No</th>
                                <th>GTN DATE</th>
                                <th>Invoice No</th>
                                <th>Invoice Date</th>
                                <th>GRN Number</th>
                                <th>VehicleNumber</th>
                                <th>Qty</th>
                                <th>Action</th>
                            </tr>

                        </thead>
                        <c:if test="${getReceivedgrn != null}">
                            <% int index = 0;
                                    int sno = 1;
                            %>
                            <tbody>

                                <c:forEach items="${getReceivedgrn}" var="gtn">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td>
                                            <%=sno%>
                                        </td>
                                        <td ><c:out value="${gtn.gtnNo}"/></td>
                                        <td ><c:out value="${gtn.gateInwardDate}"/></td>
                                        <td><c:out value="${gtn.invoiceNo}"/></td>
                                        <td><c:out value="${gtn.invoiceDate}"/></td>
                                        <td><c:out value="${gtn.grnNo}"/></td>
                                        <td><c:out value="${gtn.vehicleNumber}"/></td>
                                        <td><c:out value="${gtn.qty}"/></td>
                                       
                                        <td>
                                            <input type="hidden" name="gtnId" id="gtnId<%=sno - 1%>" value="<c:out value="${gtn.gtnId}"/>"/>
                                            <input type="hidden" name="selectedIndexs" id="selectedIndexs<%=sno - 1%>" value="0"/>
                                            <c:if test="${gtn.grnId == '0'}" >
                                                <a href='viewReceivedGTN.do?gtnId=<c:out value="${gtn.gtnId}"/>'>
                                                    <span class="label label-success">GRN</span>
                                                </a>
                                            </c:if>    
                                            <c:if test="${gtn.grnId != '0'}" >
                                                <a href='editConnectSerialNumber.do?grnId=<c:out value="${gtn.grnId}"/>&gtnId=<c:out value="${gtn.gtnId}"/>'>
                                                    <span class="label label-success">view</span>
                                                </a>
                                            </c:if>    
                                            
                                        </td>
                                    </tr>

                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>     
                            </tbody>

                        </c:if>


                    </table>
                    <div>
                        <center>

                            <!--<input type="button" class="btn btn-success" name="Update" value="Update" onclick="searchPage(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;-->
                        </center>
                    </div>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                       
                    </script>
                    <div id="loader"></div>

                    <div id="controls"  >
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5"  selected="selected" >5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>

                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>


                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

