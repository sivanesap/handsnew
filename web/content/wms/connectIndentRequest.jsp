<%-- 
    Document   : connectIndentRequest
    Created on : 16 Sep, 2021, 12:50:40 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
    function submitPage() {
        if (document.getElementById("indentDate").value == "") {
            alert("Please enter Indent Date.\n");
            document.getElementById("indentDate").focus();
        } else if (document.getElementById("deliveryDate").value == "") {
            alert("Please Enter Delivery Date.\n");
            document.getElementById("deliveryDate").focus();
        } else if (document.getElementById("appointmentDate").value == "") {
            alert("Please Enter Appointment Date.\n");
            document.getElementById("appointmentDate").focus();
        } else if (document.getElementById("deliveryType").value == "") {
            alert("Please Enter Delivery Type.\n");
            document.getElementById("deliveryType").focus();
        } else if (document.getElementById("fromLocation").value == "") {
            alert("Please Enter From Location.\n");
            document.getElementById("fromLocation").focus();
        } else if (document.getElementById("toLocation").value == "") {
            alert("Please Enter To Location.\n");
            document.getElementById("toLocation").focus();
        } else if (document.getElementById("requiredQty").value == "") {
            alert("Please Enter Required Quantity.\n");
            document.getElementById("requiredQty").focus();
        } else if (document.getElementById("vehicleType").value == "") {
            alert("Please Enter Vehicle Type.\n");
            document.getElementById("vehicleType").focus();
        } else if (document.getElementById("partyName").value == "") {
            alert("Please Enter Party Name.\n");
            document.getElementById("partyName").focus();
        } else {
            document.connect.action = " /throttle/connectIndentRequest.do?&param=save";
            document.connect.method = "post";
            document.connect.submit();
        }
    }
    function setValues(sno, indentId, fromLocation, toLocation, vehicleType, partyName, deliveryType, deliveryDate, appointmentDate, qty, indentDate, boxQty) {
        var count = parseInt(document.getElementById("count").value);
        for (var i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("indentId").value = indentId;
        document.getElementById("indentDate").value = indentDate;
        document.getElementById("fromLocation").value = fromLocation;
        document.getElementById("toLocation").value = toLocation;
        document.getElementById("vehicleType").value = vehicleType;
        document.getElementById("partyName").value = partyName;
        document.getElementById("deliveryType").value = deliveryType;
        document.getElementById("deliveryDate").value = deliveryDate;
        document.getElementById("appointmentDate").value = appointmentDate;
        document.getElementById("requiredQty").value = qty;
        document.getElementById("boxQty").value = boxQty;
    }



    function isNumberKey(e){
        var key = (e.which) ? e.which : e.keyCode;
        if ((key != 46 && key > 31) && (key < 48 || key > 57)) {
            return false;
        }
        return true;
    }

</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Connect Indent Request</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">HS Connect</a></li>
                <li class="">Connect Indent Request</li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="connect"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <br>
                        <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                            <input type="hidden" name="indentId" id="indentId" value=""  />
                            <tr>
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="8" >Indent Request Details</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="text1" ><font color="red">*</font>From Location</td>
                                    <td class="text1" >
                                        <select name="fromLocation" id="fromLocation" class="form-control" style="width:240px;height:40px;" >
                                            <c:if test="${whList != null}">
                                                <option value="" selected>------Select Location------</option>
                                                <c:forEach items="${whList}" var="wh">
                                                    <option value='<c:out value="${wh.whId}"/>'><c:out value="${wh.whName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                    </td>
                                    <td class="text1"><font color="red">*</font>To Location</td>
                                    <td class="text1"><input type="text" name="toLocation" id="toLocation" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Indent Date</td>
                                    <td class="text1"><input type="text" name="indentDate" id="indentDate" class="form-control datepicker" style="width:240px;height:40px" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Vehicle Type</td>
                                    <td class="text1">
                                        <select class="form-control" style="width:240px;height:40px;" id="vehicleType" name="vehicleType"  >
                                            <option value="">------Select Vehicle Type------</option>
                                            <c:if test = "${getVehicleTypeList != null}" >
                                                <c:forEach items="${getVehicleTypeList}" var="Type"> 
                                                    <option value='<c:out value="${Type.typeId}" />'><c:out value="${Type.typeName}" /></option>
                                                </c:forEach >
                                            </c:if>  	
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Party Name</td>
                                    <td class="text1"><input type="text" name="partyName" id="partyName" class="form-control" style="width:240px;height:40px"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Delivery Type</td>
                                    <td class="text1"><input type="text" name="deliveryType" id="deliveryType" class="form-control" style="width:240px;height:40px"/></td>
                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Appointment Date</td>
                                    <td class="text1"><input type="text" name="appointmentDate" id="appointmentDate" class="form-control datepicker" style="width:240px;height:40px"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Delivery Date</td>
                                    <td class="text1"><input type="text" name="deliveryDate" id="deliveryDate" class="form-control datepicker" style="width:240px;height:40px"/></td>
                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Required Quantity</td>
                                    <td class="text1"><input type="text" name="requiredQty" id="requiredQty" class="form-control" style="width:240px;height:40px"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>No. of Boxes</td>
                                    <td class="text1"><input type="text" name="boxQty" id="boxQty" class="form-control" style="width:240px;height:40px"/></td>
                                </tr>
                            </table>
                            </tr>
                            <tr>
                                <td>
                                    <br>
                                    <center>
                                        <input type="button" class="btn btn-success" value="Save" name="Submit" onClick="submitPage()">
                                    </center>
                                </td>
                            </tr>
                        </table>
                        <br>

                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30">
                                    <th>S.No</th>
                                    <th>Indent Date</th>
                                    <th>From Location</th>
                                    <th>To Location</th>
                                    <th>Vehicle Type</th>
                                    <th>Party Name</th>
                                    <th>Delivery Type</th>
                                    <th>Delivery Date</th>
                                    <th>Appointment Date</th>
                                    <th>Required Quantity</th>
                                    <th>No of Boxes</th>
                                    <th>Select</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                                <c:if test = "${connectIndentRequest != null}">
                                    <c:forEach items="${connectIndentRequest}" var="pc">
                                        <%
                                            sno++;
                                            String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.indentDate}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.fromLocation}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.toLocation}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.vehicleType}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.partyName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.deliveryType}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.deliveryDate}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.appointmentDate}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.qty}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.boxQty}" /></td>
                                            <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${pc.indentId}" />', '<c:out value="${pc.whId}" />', '<c:out value="${pc.toLocation}" />', '<c:out value="${pc.vehicleTypeId}" />', '<c:out value="${pc.partyName}" />', '<c:out value="${pc.deliveryType}" />', '<c:out value="${pc.deliveryDate}" />', '<c:out value="${pc.appointmentDate}" />', '<c:out value="${pc.qty}" />','<c:out value="${pc.indentDate}" />','<c:out value="${pc.boxQty}" />');" /></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>


                        <input type="hidden" name="count" id="count" value="<%=sno%>" />

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>