<%-- 
    Document   : imGrnIssueView
    Created on : 1 Nov, 2021, 6:26:03 PM
    Author     : alan
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Material Issue View</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html">InstaMart</a></li>
                <li class="">Material Issue View</li>

            </ol>
        </div>
    </div>
                <script>
                    function openView(issueId){
                        window.open('/throttle/instaMartGrnIssueView.do?param=view&issueId=' + issueId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                    }
                    function openPrint(issueId){
                        window.open('/throttle/instaMartGrnIssueView.do?param=print&issueId=' + issueId, 'PopupPage', 'height = 1000, width = 1000, scrollbars = yes, resizable = yes');
                    }
                    function openUpdate(issueId){
                        window.open('/throttle/instaMartGrnIssueView.do?param=update&issueId=' + issueId, 'PopupPage', 'height = 1000, width = 1000, scrollbars = yes, resizable = yes');
                    }
                </script>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="material"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>

                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30" style="color: white">
                                    <th>S.No</th>
                                    <th>Issue No</th>
                                    <th>Issue Date</th>
                                    <th>Issue Time</th>
                                    <th>Total Qty</th>
                                    <th>Issued By</th>
                                    <th>Received By</th>
                                    <th>Remarks</th>
                                    <!--<th>Status</th>-->
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                            <c:if test = "${issueMaster != null}">
                                <c:forEach items="${issueMaster}" var="pc">
                                    <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>

                                    <tr>
                                        <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                        <td class="<%=className%>"  align="left"> <c:out value="${pc.issueNo}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.date}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.time}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.totalQty}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.empName}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.receivedBy}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.remarks}" /></td>
                                    
                                    <td class="<%=className%>"> 
                                        <a class="label label-Primary" style="color:white" onclick="openView('<c:out value="${pc.issueId}"/>')">View</a>
                                        <br>
                                        <br>
                                        <a class="label label-info" style="color:white" onclick="openPrint('<c:out value="${pc.issueId}"/>')">Print</a>
                                        <br>
                                        <br>
                                        <a class="label label-success" style="color:white" onclick="openUpdate('<c:out value="${pc.issueId}"/>')">Update</a>
                                    </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </c:if>
                        </table>


                        <input type="hidden" name="count" id="count" value="<%=sno%>" />

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                                        setFilterGrid("table");
                        </script>

                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
