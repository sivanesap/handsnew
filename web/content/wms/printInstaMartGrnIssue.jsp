<%-- 
    Document   : printInstaMartGrnIssue
    Created on : 1 Nov, 2021, 2:06:26 PM
    Author     : silambarasan a
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <style type="text/css">

            .main{
                border: 1px solid black;

                margin-top: 2%;
            }


            .td1{
                border: 1px solid black;
                /* margin: 0%;
                        padding: 0%;*/
            }

            .tab{

                margin-top: 2%;
            }
            .tabdata{
                border: 1px solid black;
                height: 30%;

            }
            .table-bordered{
                border-color: black;
            }

            .eventdwidth{
                width: 100px;
                border: 1px solid black;
            }
        </style>




        <script>

            function printPage(val) {
                document.getElementById("ok").style.display = "none";
                window.print('body')
            }
        </script>
    </head>
    <body>




        <div style="border:1px solid black;margin:4%;padding:2%">


            <div class="row">
                <c:forEach items="${issueMaster}" var="is">

                    <table style="width:100%;" align="center">
                        <tr >
                            <td style="width:20%"></td>
                            <td style="width:20%;text-align: right">
                                <img width = "90px" src = "images/HSSupplyLogo.png">&emsp;&emsp;
                            </td>
                            <td style="width:40%">
                                <b>
                                    H&S Supply Chain Services Private Limited
                                </b><br>
                                <c:out value="${is.whAddress}"/>
                                <br>
                            </td>
                            <td style="width:20%"></td>
                        </tr>
                    </table>

                    <div style = "text-align:center;margin-top:2%;"><u><h5>Material Issue Slip</h5></u></div>
                    <div class="col" >
                        <div class = "tab">
                            <table >
                                <tr>
                                    <td>Issue No : <c:out value="${is.issueNo}"/></td><td></td>
                                </tr>
                                <tr>
                                    <td >Location : <c:out value="${is.whName}"/></td><td></td>
                                </tr>
                                <tr>
                                    <td >Date : <c:out value="${is.date}"/>&nbsp;<c:out value="${is.time}"/></td><td ></td>
                                </tr>
                            </table>
                        </div>
                    </div>





                    <div class="col">
                        <div class = "tab" style="float: right;padding-right:20%">
                            <table >
                                <tr>
                                    <td>To Store :  </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Store Person :  <c:out value="${is.empName}"/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Received By :  <c:out value="${is.receivedBy}"/></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </c:forEach>

            </div>

            <table class="table table-bordered" style="margin-top: 20px;">
                <thead>
                    <tr>
                        <th>SI.No         </th>
                        <th>Material</th>
                        <th>Material Code </th>
                        <th>Material Description   </th>
                        <th>UOM           </th>
                        <th>Qty           </th>
                        <th>Bin Location  </th>
                        <th>Remarks       </th>
                    </tr>
                </thead>
                <tbody style="height:400px">
                    <% int sno1 = 0;%>
                    <c:if test = "${issueDetails != null}">
                        <c:forEach items="${issueDetails}" var="pc">
                            <%
                                sno1++;
                                String className1 = "text1";
                                if ((sno1 % 1) == 0) {
                                    className1 = "text1";
                                } else {
                                    className1 = "text2";
                                }
                            %>

                            <tr >
                                <td class="<%=className1%>"><%= sno1%> </td>
                                <td class="<%=className1%>"><c:out value="${pc.material}" /> </td>
                                <td class="<%=className1%>"><c:out value="${pc.materialCode}" /> </td>
                                <td class="<%=className1%>"><c:out value="${pc.materialDescription}" /> </td>
                                <td class="<%=className1%>"><c:out value="${pc.uom2}" /> </td>
                                <td class="<%=className1%>"><c:out value="${pc.qty}" /> </td>
                                <td class="<%=className1%>"><c:out value="${pc.binLocation}" /> </td>
                                <td class="<%=className1%>"><c:out value="${pc.remarks}" /> </td>
                            </tr>




                        </c:forEach>

                    </c:if>
                </tbody>



            </table>
            <br>
            <table style="width: 100%;text-align: center">
                <tr>
                    <td colspan="2">StoreKeeper </td>
                    <td colspan="3">Authorised By </td>
                    <td colspan="3"> Received By </td>
                </tr>
            </table>
        </div>
    <center>This is system generated document. No signature required</center>
    <br>
    <center>
        <input type="button" class="button" name="ok" id="ok" value="Print" onClick="printPage('printDiv');" > &nbsp;&nbsp;&nbsp;
    </center>
</body>
</html>




