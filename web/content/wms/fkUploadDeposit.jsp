<%-- 
    Document   : ifbUploadPod
    Created on : 28 May, 2021, 5:58:35 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="IFB POD Upload"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.IFB" text="IFB"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="IFB POD Upload"/></li>

        </ol>
    </div>
</div>

<script type="text/javascript">
    function uploadPage() {
//        alert('here');
        document.upload.action = "/throttle/saveDeliveryPodUpload.do?param=update";
        document.upload.submit();
    }
</script>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form name="upload"  method="post" enctype="multipart/form-data">

                    <%@ include file="/content/common/message.jsp" %>


                    <table class="table table-info mb30 table-hover" id="uploadTable"  >
                        <thead> 
                            <tr>
                                <th  colspan="5">IFB Deposit Upload</th>
                            </tr>
                        </thead>
                        <tr>
                            <td >Invoice No</td>
                            <td><input type="text" disabled class="form-control" name="depositCode" id="depositCode" style="width:200px;height:40px" value="<c:out value="${depositCode}"/>">
                                <input type="hidden" name="depositId" id="depositId" value="<c:out value="${depositId}"/>"></td>
                            <td >Pod File</td>
                            <td ><input type="file" name="podFile" id="podFile"  class="importCnote"></td>                             
                        </tr>
                        <tr>
                            <td >POD Remarks</td>
                            <td><input type="text" class="form-control" name="podRemarks" style="width:200px;height:40px" id="podRemarks" value=""/>
                            </td>
                            <td ></td>
                            <td ></td>                             
                        </tr>
                        <tr>
                        </tr>
                    </table>
                    <center><input type="button" class="btn btn-success" value="Upload" name="uploadPod" id="uploadPod" onclick="uploadPage();"></center>
                </form>
            </body>
        </div>
    </div>     

</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>


