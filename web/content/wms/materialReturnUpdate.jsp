<%-- 
    Document   : materialReturnUpdate
    Created on : 19 Mar, 2022, 11:57:04 AM
    Author     : mahendiran
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script>
    function checkClose(sno) {
        var qty = document.getElementById("qty" + sno).value;
        var packQty = document.getElementById("packQty" + sno).value;
        var totQty = parseFloat(qty) * parseFloat(packQty);
        var floor = document.getElementById("floorQty" + sno).value;
        var used = document.getElementById("usedQty" + sno).value;
        var excess = document.getElementById("excess" + sno).value;
        var shortage = document.getElementById("shortage" + sno).value;
        var bal = parseFloat(floor) - parseFloat(used) + parseFloat(excess) - parseFloat(shortage);
        var status = document.getElementById("status" + sno).value;
    }
    function checkQty(sno) {
        var qty = document.getElementById("qty" + sno).value;
        var packQty = document.getElementById("packQty" + sno).value;
        var totQty = parseFloat(qty) * parseFloat(packQty);
        var floor = document.getElementById("floorQty" + sno).value;
        var used = document.getElementById("usedQty" + sno).value;
        var excess = document.getElementById("excess" + sno).value;
        var shortage = document.getElementById("shortage" + sno).value;
        var bal = 0.00;
        bal = parseFloat(floor) - parseFloat(used) - parseFloat(excess) + parseFloat(shortage);
        if (parseFloat(totQty) < parseFloat(bal)) {
            alert("Quantity Exceeded");
            document.getElementById("floorQty" + sno).value = document.getElementById("floorQtyTemp" + sno).value;
            document.getElementById("usedQty" + sno).value = document.getElementById("usedQtyTemp" + sno).value;
            document.getElementById("excess" + sno).value = document.getElementById("excessTemp" + sno).value;
            document.getElementById("shortage" + sno).value = document.getElementById("shortageTemp" + sno).value;
        }
    }
    function submitPage() {
        document.material.action = "/throttle/materialReturnView.do?param=save";
        document.material.submit();
    }
    function checkUsedQty(sno, qty) {

        var usedQty = document.getElementById("usedQty" + sno).value;
        var floorQtyTemp = document.getElementById("floorQtyTemp" + sno).value;
        var usedQtyTemp = document.getElementById("usedQtyTemp" + sno).value;
        var shortage = document.getElementById("shortage" + sno).value;
        var floorQty = parseFloat(floorQtyTemp) - parseFloat(shortage) - (parseFloat(usedQty) - parseFloat(usedQtyTemp));
        var packQty1 = document.getElementById("packQty" + sno).value;
        var totQty1 = parseFloat(floorQty) / parseFloat(packQty1);
        document.getElementById("floorPackQty"+sno).value = totQty1;

        document.getElementById("floorQty" + sno).value = floorQty;
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Material Return View</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html">InstaMart</a></li>
                <li class="">Material Return View</li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="material"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>

                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30" style="color: white">
                                    <th>S.No</th>
                                    <th>Material Name</th>
                                    <th>Material Code</th>
                                    <th>Material Description</th>
                                    <th>Returned Qty</th>
                                    <th>Piece Qty</th>
                                    <th>Floor Qty</th>
                                    <th>Used Qty</th>
                                    <th>Excess</th>
                                    <th>Shortage</th>
                                    <th>UOM</th>
                                    <th>Bin Location</th>
                                    <th>Remarks</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                                <c:if test = "${issueDetails != null}">
                                    <c:forEach items="${issueDetails}" var="pc">
                                        <%
                                            sno++;
                                            String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.material}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.materialCode}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.materialDescription}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.qty}" /></td>
                                            <td class="<%=className%>"  align="left"><input type="text" class="form-control" style="width:150px;height:35px" name="totQty" id="totQty<%=sno%>" readonly value=""/></td>
                                            <td class="<%=className%>"  align="left"><input type="text" class="form-control" style="width:150px;height:35px" onchange="checkQty('<%=sno%>');
                                                    checkFloorQty('<%=sno%>', '<c:out value="${pc.qty}" />');" name="floorQty" id="floorQty<%=sno%>" value="<c:out value="${pc.floorQty}" />"/></td>
                                            <td class="<%=className%>"  align="left"> <input type="text" class="form-control" style="width:150px;height:35px" onchange="checkQty('<%=sno%>');
                                                    checkUsedQty('<%=sno%>', '<c:out value="${pc.qty}" />');" name="usedQty" id="usedQty<%=sno%>" value="<c:out value="${pc.usedQty}" />"/></td>
                                            <td class="<%=className%>"  align="left"> <input type="text" class="form-control" style="width:150px;height:35px" onchange="checkQty('<%=sno%>');" name="excess" id="excess<%=sno%>" value="<c:out value="${pc.excess}" />"/></td>
                                            <td class="<%=className%>"  align="left"> <input type="text" class="form-control" style="width:150px;height:35px" onchange="checkQty('<%=sno%>');
                                                    checkUsedQty('<%=sno%>', '<c:out value="${pc.qty}" />');" name="shortage" id="shortage<%=sno%>" value="<c:out value="${pc.shortage}" />"/></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.uom2}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.binLocation}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.remarks}" /></td>
                                            <td class="<%=className%>"  align="left"> 
                                                <select id="status<%=sno%>" name="status" class="form-control" style="width:150px;height:40px" onchange="checkClose('<%=sno%>')">
                                                    <option value="0">Pending</option>
                                                    <option value="1">Closed</option>
                                                </select>
                                                <input type="hidden" name="detailId" id="detailId<%=sno%>" value="<c:out value="${pc.returnDetailId}"/>"/>
                                                <input type="hidden" name="returnId" id="issueId<%=sno%>" value="<c:out value="${pc.returnId}"/>"/>
                                                <input type="hidden" name="packQty" id="packQty<%=sno%>" value="<c:out value="${pc.packQty}"/>"/>
                                                <input type="hidden" name="qty" id="qty<%=sno%>" value="<c:out value="${pc.qty}"/>"/>
                                                <input type="hidden" name="floorQtyTemp" id="floorQtyTemp<%=sno%>" value="<c:out value="${pc.floorQty}" />"/>
                                                <input type="hidden" name="usedQtyTemp" id="usedQtyTemp<%=sno%>" value="<c:out value="${pc.usedQty}" />"/>
                                                <input type="hidden" name="excessTemp" id="excessTemp<%=sno%>" value="<c:out value="${pc.excess}" />"/>
                                                <input type="hidden" name="shortageTemp" id="shortageTemp<%=sno%>" value="<c:out value="${pc.shortage}" />"/>
                                                <input type="hidden" name="materialId" id="materialId<%=sno%>" value="<c:out value="${pc.materialId}" />"/>
                                                <input type="hidden" name="floorPackQty" id="floorPackQty<%=sno%>" value="<c:out value='${pc.floorQty/pc.packQty}'/>"/>
                                            </td>
                                        </tr>
                                    <script>
                                        document.getElementById("status<%=sno%>").value = "<c:out value="${pc.status}"/>";
                                        if (document.getElementById("status<%=sno%>").value == "1") {
                                            document.getElementById("status<%=sno%>").disabled = true;
                                            document.getElementById("floorQty<%=sno%>").readOnly = true;
                                            document.getElementById("usedQty<%=sno%>").readOnly = true;
                                            document.getElementById("excess<%=sno%>").readOnly = true;
                                            document.getElementById("shortage<%=sno%>").readOnly = true;
                                        }
                                        var qty1 = document.getElementById("qty<%=sno%>").value;
                                        var packQty1 = document.getElementById("packQty<%=sno%>").value;
                                        var totQty1 = parseFloat(qty1) * parseFloat(packQty1);
                                        document.getElementById("totQty<%=sno%>").value = totQty1;
                                    </script>
                                </c:forEach>
                                </tbody>
                            </c:if>
                        </table>
                        <center>
                            <input type="button" class="btn btn-success" name="save" id="save" value="Submit" onclick="submitPage()">
                        </center>
                        <input type="hidden" name="count" id="count" value="<%=sno%>" />

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>

                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>