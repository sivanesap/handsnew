<%-- 
    Document   : rpTripPlanning
    Created on : 28 Sep, 2021, 2:09:12 PM
    Author     : Roger
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
    function viewPage(orderId) {
        window.open('/throttle/rpOrderDetails.do?param=view&orderId=' + orderId, 'PopupPage', 'height = 600, width = 800, scrollbars = yes, resizable = yes');
    }
</script>
<script>
    function checkSelectedStatus(elem, sno) {
        if (elem.checked == true) {
            document.getElementById("selectedStatus" + sno).value = 1;
        } else {
            document.getElementById("selectedStatus" + sno).value = 0;
        }
    }
    function submitPage() {
        document.connect.action = "/throttle/rpTripPlanningDetails.do";
        document.connect.submit();
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Repose Trip Planning</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Repose</a></li>
                <li class="">Trip Planning</li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="connect"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30">
                                    <th>S.No</th>
                                    <th>Order No</th>
                                    <th>Order Date</th>
                                    <th>Customer Name</th>
                                    <th>Pincode</th>
                                    <th>Qty</th>
                                    <th>Amount</th>
                                    <th>Expected Delivery Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>


                            <% int sno = 0;%>
                            <c:if test = "${rpOrderMaster != null}">
                                <tbody>
                                    <c:forEach items="${rpOrderMaster}" var="rp">
                                        <%
                                            sno++;
                                            String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                            <td><a style="cursor: pointer" onclick="viewPage('<c:out value="${rp.orderId}"/>')"><c:out value="${rp.orderNo}"/></a></td>
                                            <td><c:out value="${rp.orderDate}"/></td>
                                            <td><c:out value="${rp.customerName}"/></td>
                                            <td><c:out value="${rp.pincode}"/></td>
                                            <td><c:out value="${rp.qty}"/></td>
                                            <td><c:out value="${rp.price}"/></td>
                                            <td><c:out value="${rp.deliveryDate}"/></td>
                                            <td>
                                                <input type="checkbox" id="selectedIndex<%=sno%>" name="selectedIndex" onclick="checkSelectedStatus(this, '<%= sno%>')">
                                                <input type="hidden" id="selectedStatus<%=sno%>" name="selectedStatus" value="0"/>
                                                <input type="hidden" id="orderId<%=sno%>" name="orderId" value="<c:out value="${rp.orderId}"/>"/>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>
                        <center>
                            <input type="button" class="btn btn-success" name="commit" id="commit" value="Plan Trip" onclick="submitPage()"/>
                        </center>

                        <input type="hidden" name="count" id="count" value="<%=sno%>" />

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
