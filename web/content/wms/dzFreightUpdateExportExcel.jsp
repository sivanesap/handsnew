<%-- 
    Document   : dzFreightUpdateExportExcel
    Created on : 5 Jan, 2022, 5:20:30 PM
    Author     : alan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Excel";
        request.setAttribute("menuPath", menuPath);
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "imFreightUpdateExportExcel-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
 <table class="table table-info mb30 table-bordered" id="table" class="sortable" width="100%">
                    <thead>
                        <tr >
                            <th>SNo</th>
                            <th>Vendor Name</th>
                            <th>LRN No</th>
                            <th>Sub LRN No</th>
                            <th>Customer Name</th>
                            <th>Dispatch date</th>
                            <th>Vehicle No</th>
                            <th>Vehicle Type</th>
                            <th>Billed Quantity</th>
                            <th>Freight Charges</th>
                            <th>Other Charges</th>
                            <th>Total Charges</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        <c:forEach items="${imFreightDetails}" var="dc">
                            <%
                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "text2";
                                } else {
                                    classText = "text1";
                                }
                            %>
                            <tr >
                                <td class="<%=classText%>"  align="center"><%=index%></td>
                                <td class="<%=classText%>"  align="center"><c:out value="${dc.vendorName}"/></td>
                                <td class="<%=classText%>"  align="center"><c:out value="${dc.lrNo}"/></td>
                                <td class="<%=classText%>"  align="center"><input type="hidden" id="subLr<%=index%>" name="subLr" value="<c:out value="${dc.subLrid}"/> " style="width:100px;height:30px;"><c:out value="${dc.lrnNo}"/></td>
                                <td class="<%=classText%>"  align="center"><c:out value="${dc.customerName}"/></td>
                                <td class="<%=classText%>"  align="center"><c:out value="${dc.date}"/></td>
                                <td class="<%=classText%>"  align="center"><c:out value="${dc.vehicleNo}"/></td>
                                <td class="<%=classText%>"  align="center"><c:out value="${dc.vehicleType}"/></td>
                                <td class="<%=classText%>"  align="center"><c:out value="${dc.qty}"/></td>
                                <td class="<%=classText%>"  align="center"><input type="text" name="freightAmt" id="freightAmt<%=index%>" value="<c:out value="${dc.freightAmount}"/>" onkeyup="totalCharges(<%=index%>);" style="width:100px;height:30px;"></td>
                                <td class="<%=classText%>"  align="center"><input type="text" name="otherAmt" id="otherAmt<%=index%>" value="<c:out value="${dc.otherAmt}"/>" onkeyup="totalCharges(<%=index%>);" style="width:100px;height:30px;"></td>
                                <td class="<%=classText%>"  align="center"><input type="text" name="totalAmt" id="totalAmt<%=index%>" value="<c:out value="${dc.totalAmt}"/>" style="width:100px;height:30px;"></td>

                                <td class="<%=classText%>"  align="center"><input type="checkbox" id="selectedIndex<%=index%>" name="selectedIndex" value="" onclick="checkSelectStatus('<%=index%>', this)">
                                    <input type="hidden" name="selectedStatus" id="selectedStatus<%=index%>" value="0" />
                                </td>
                                <c:if test="${dc.status=='1'}">
                            <script>
                                document.getElementById("selectedIndex<%=index%>").style.display = "none";
                                document.getElementById("freightAmt<%=index%>").readOnly = true;
                                document.getElementById("otherAmt<%=index%>").readOnly = true;
                                document.getElementById("totalAmt<%=index%>").readOnly = true;
                            </script>
                        </c:if>
                        <%index++;%>

                    </c:forEach>

                    </tbody>
                </table>
  

            <br>
            <br>

        </form>
    </body>
</html>