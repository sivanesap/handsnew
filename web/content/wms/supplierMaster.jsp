<%-- 
    Document   : supplierMaster
    Created on : 7 Jun, 2021, 7:03:31 AM
    Author     : MAHENDIRAN R
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    function submitPage() {
        document.supplierCategory.action = " /throttle/saveSupplierMaster.do";
        document.supplierCategory.method = "post";
        document.supplierCategory.submit();
    }
    function setValues(sno, supplierId, supplierName, supplierCode, address, address1, city, state, pincode, contactNo, contactPerson, emailId, gstNo) {
        var count = parseInt(document.getElementById("count").value);

        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("supplierId").value = supplierId;
        document.getElementById("supplierName").value = supplierName;
        document.getElementById("supplierCode").value = supplierCode;
        document.getElementById("address").value = address;
        document.getElementById("address1").value = address1;
        document.getElementById("city").value = city;
        document.getElementById("state").value = state;
        document.getElementById("pincode").value = pincode;
        document.getElementById("contactNo").value = contactNo;
        document.getElementById("contactPerson").value = contactPerson;
        document.getElementById("emailId").value = emailId;
        document.getElementById("gstNo").value = gstNo;
    }

</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Supplier Master" text="Supplier Master"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Master"/></a></li>
                <li class=""><spring:message code="hrms.label.Supplier Master" text="Supplier Master"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">
                    <form name="supplierCategory"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <br>
                        <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                            <input type="hidden" name="supplierId" id="supplierId" value=""  />
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="8" >Supplier Master</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Supplier Name</td>
                                    <td><input type="text" id="supplierName" name="supplierName" class="form-control" style="width:240px;height:40px"></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Address 1</td>
                                    <td class="text1"><input type="text" name="address" id="address" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Address 2</td>
                                    <td class="text1"><input type="text" name="address1" id="address1" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checksupplierCategoryName();" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Postal Code</td>
                                    <td class="text1"><input type="text" name="pincode" id="pincode" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>City</td>
                                    <td class="text1"><input type='text' align="center" class="form-control" style="width:240px;height:40px" name="city" id="city" autocomplete="off"/></td>
                                    
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>State</td>
                                    <td class="text1">
                                        <select  align="center" class="form-control" style="width:240px;height:40px" name="state" id="state" >
                                            <option value=''>--select--</option>                            
                                            <c:if test = "${getStateList != null}" >
                                                <c:forEach items="${getStateList}" var="Type"> 
                                                    <option value='<c:out value="${Type.stateId}" />'><c:out value="${Type.stateName}" /></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Contact Person</td>
                                    <td class="text1"><input type="text" name="contactPerson" id="contactPerson" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Contact No</td>
                                    <td class="text1"><input type="text" name="contactNo" id="contactNo" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>E-mail id</td>
                                    <td class="text1"><input type="text" name="emailId" id="emailId" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>GST Number</td>
                                    <td class="text1"><input type="text" name="gstNo" id="gstNo" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Supplier Code</td>
                                    <td class="text1"><input type="text" name="supplierCode" id="supplierCode" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Status</td>
                                    <td class="text1">
                                        <select  align="center" class="form-control" style="width:240px;height:40px" name="ActiveInd" id="ActiveInd" >
                                            <option value='Y'>Active</option>
                                            <option value='N' id="inActive" style="display: none">In-Active</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                <center><a>
                                        <input  type="button" class="btn btn-success" align="center" value="Save" name="Submit" onClick="submitPage()">
                                    </a>
                                </center>
                                </tr>
                            </table>
                            <br>
                            <table class="table table-info mb30 table-hover" id="table" >	
                                <thead>

                                    <tr height="30" style="color: white">
                                        <th>S No</th>
                                        <th>Supplier Id</th>
                                        <th>Supplier Name</th>
                                        <th>Supplier Code</th>
                                        <th>Address 1</th>
                                        <th>Address 2</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th>Postal Code</th>
                                        <th>Contact No</th>
                                        <th>Contact Person</th>
                                        <th>E-mail id</th>
                                        <th>GST Number</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>


                                    <% int sno = 0;%>
                                    <c:if test = "${supplierMaster != null}">
                                        <c:forEach items="${supplierMaster}" var="supplier">
                                            <%
                                                sno++;
                                                String className = "text1";
                                                if ((sno % 1) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                            %>

                                            <tr>
                                                <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${supplier.supplierId}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${supplier.supplierName}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${supplier.supplierCode}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${supplier.address}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${supplier.address1}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${supplier.city}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${supplier.state}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${supplier.pincode}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${supplier.contactNo}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${supplier.contactPerson}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${supplier.emailId}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${supplier.gstNo}" /></td>
                                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${supplier.supplierId}" />', '<c:out value="${supplier.supplierName}" />', '<c:out value="${supplier.supplierCode}" />', '<c:out value="${supplier.address}" />', '<c:out value="${supplier.address1}" />', '<c:out value="${supplier.city}" />', '<c:out value="${supplier.state}" />', '<c:out value="${supplier.pincode}" />', '<c:out value="${supplier.contactNo}" />', '<c:out value="${supplier.contactPerson}" />', '<c:out value="${supplier.emailId}" />', '<c:out value="${supplier.gstNo}" />');" /></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </c:if>
                            </table>


                            <input type="hidden" name="count" id="count" value="<%=sno%>" />

                            <br>
                            <br>
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" selected="selected">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span>Entries Per Page</span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("table", 1);
                            </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
