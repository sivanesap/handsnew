<%-- 
    Document   : orderApproval.jsp
    Created on : Mar 9, 2021, 8:02:14 PM
    Author     : DELL
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<style type="text/css">

</style>
<script type="text/javascript">
    function uploadContract() {
                                       
        var supplyId = document.getElementById("supplyId").value;
        document.getElementById("UploadContract").disabled = true;
        
//        alert(supplyId);
        document.upload.action = "/throttle/uploadconnectInvoice.do?&supplyId=" + supplyId;
        document.upload.method = "post";
        document.upload.submit();
        
    }

    function uploadPage() {
        document.getElementById("submitPage").disabled = true;
        var supplyId = document.getElementById("supplyId").value;
//        alert(supplyId);
        if (document.getElementById("countStatus").value > 0) {
            alert("Please Remove Rows with Error and Upload Again");
        } else {
            document.upload.action = "/throttle/connectUploadInvoice.do?&param=save&supplyId=" + supplyId;
            document.upload.method = "post";
            document.upload.submit();
        }
    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="Invoice Upload"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="Invoice Upload"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form name="upload"  method="post" enctype="multipart/form-data">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    <table class="table table-info mb30 table-hover" id="uploadTable"  >
                        <thead> 
                            <tr>
                                <th  colspan="3"> Invoice Upload Details</th>
                            </tr>
                        </thead>
                        <tr>
                            <td> 
                                <select name="supplyId" id="supplyId"   class="form-control" style="width:250px;height:40px" >
                                    <option value="0">--Select--</option>
                                    <c:forEach items="${getSupplierList}" var="supplier">
                                        <option value="<c:out value="${supplier.supplierId}"/>"><c:out value="${supplier.supplierName}"/></option>
                                    </c:forEach>
                                </select>

                            </td>
                            <td ><input type="file" name="importCnote" id="importCnote"  class="importCnote"><font size="1"><u><a href="uploadedxls/ifbFormat.xls" id="template" download>sample file</a></u></font></td>                             
                            <td colspan="0"><input type="button" class="btn btn-success" value="Upload" name="UploadContract" id="UploadContract" onclick="uploadContract();"></td>
                        </tr>
                    </table>
                    <script>
                        document.getElementById("supplyId").value = '<c:out value="${supplyId}"/>';
                        if ('<c:out value="${supplyId}"/>' == "" || '<c:out value="${supplyId}"/>' == null) {
                            document.getElementById("supplyId").value = 2;
                            document.getElementById("template").href='uploadedxls/hindwareTemplate.xls';
                        }
                    </script>
                    <div id="ptp">
                        <div class="inpad" style=" overflow-x: visible;">

                            <c:if test = "${getContractList != null}" >
                                
                                <script>
                                            document.getElementById("UploadContract").disabled = true;

                                </script>

                                <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                    <thead style="font-size:12px">
                                        <tr height="40">
                                            <th  height="30" >S No</th>
                                            <th  height="30" >Plant Code</th>
                                            <th  height="30" >Ship-To-Party code</th>
                                            <th  height="30" >Ship-To-Party Name</th>
                                            <th  height="30" >Invoice Date</th>
                                            <th  height="30" >Invoice Time</th>
                                            <th  height="30" >Invoice No.</th>
                                            <th  height="30" >Material Code</th>
                                            <th  height="30" >Billed Quantity</th>
                                            <th  height="30" >Basic Price</th>
                                            <th  height="30" >Pre-tax Value</th>
                                            <th  height="30" >Tax Value</th>
                                            <th  height="30" >Invoice Amount</th>
                                            <th  height="30" >Purchase Order No</th>
                                            <th  height="30" >Purchase Order Date</th>
                                            <th  height="30" >Sale Order No</th>
                                            <th  height="30" >Sale Order Date</th>
                                            <th height="30">Status</th>
                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                     int count=0;
                                    %> 
                                    <tbody>

                                        <c:forEach items= "${getContractList}" var="contractList">
                                            <tr height="30">
                                                <td align="left"><%=sno%></td>
                                                <td align="left"   ><c:out value="${contractList.plantCode}"/></td>
                                                <td align="left"   ><c:out value="${contractList.shipToPartycode}"/></td>
                                                <td align="left"   ><c:out value="${contractList.shipToPartyName}"/></td>
                                                <td align="left"   ><c:out value="${contractList.invoiceDate}"/></td>
                                                <td align="left"   ><c:out value="${contractList.invoiceTime}"/></td>
                                                <td align="left"   ><c:out value="${contractList.invoiceNo}"/></td>
                                                <td align="left"   ><c:out value="${contractList.materialCode}"/></td>
                                                <td align="left"   ><c:out value="${contractList.billedQuantity}"/></td>
                                                <td align="left"   ><c:out value="${contractList.basicPrice}"/></td>
                                                <td align="left"   ><c:out value="${contractList.pretaxValue}"/></td>
                                                <td align="left"   ><c:out value="${contractList.taxValue}"/></td>
                                                <td align="left"   ><c:out value="${contractList.invoiceAmount}"/></td>
                                                <td align="left"   ><c:out value="${contractList.purchaseOrderNo}"/></td>
                                                <td align="left"   ><c:out value="${contractList.purchaseOrderDate}"/></td>
                                                <td align="left"   ><c:out value="${contractList.saleOrderNo}"/></td>
                                                <td align="left"   ><c:out value="${contractList.saleOrderDate}"/>
                                                    <input type="hidden" name="status" id="status<%=sno%>" value="<c:out value="${contractList.status}"/>"></td>
                                                    <c:if test="${contractList.status==0}">
                                                    <td align="left"><font color="green">Ready To Submit</font></td>
                                                        </c:if>
                                                        <c:if test="${contractList.status==1}">
                                                    <td align="left"><font color="red">Material does not Exist</font></td>
                                                            <%count++;%>
                                                        </c:if>
                                                        <c:if test="${contractList.status==2}">
                                                    <td align="left"><font color="red">Invoice No Already Exists</font></td>
                                                            <%count++;%>
                                                        </c:if>
                                                        <c:if test="${contractList.status==3}">
                                                    <td align="left"><font color="red">Party Code Not exists in Dealer Master</font></td>
                                                            <%count++;%>
                                                        </c:if>
                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                    </tbody>
                                </table>

                                <center>

                                    <td colspan="0"><input type="button" class="btn btn-success" value="Submit" name="submitPage" id="submitPage" onclick="uploadPage();"></td>
                                </center>
                            </c:if>
                        </div>
                    </div>     

                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>


