
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    function submitPage() {
        document.productMaster.action = " /throttle/savegrnData.do";
        document.productMaster.method = "post";
        document.productMaster.submit();
    }

    function setValues(sno,gateInwardNo,vehicleNO,materialCode,materialDes,giQuantity,supPlantCode,supPlantDes,recplantCode,recplantDes,storageLocation,purchaseOrderNo,deliveryNo,cancelledNumber,grnID) {
       
        var count = parseInt(document.getElementById("count").value);
    

//        document.getElementById('inActive').style.display = 'block';
        
        for (i = 1; i <= count; i++) {
          
            if (i != sno) {
               
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("gateInwardNo").value = gateInwardNo;
        document.getElementById("vehicleNO").value = vehicleNO;
        document.getElementById("materialCode").value = materialCode;
        document.getElementById("materialDes").value = materialDes;
        document.getElementById("giQuantity").value = giQuantity;
        document.getElementById("supPlantCode").value = supPlantCode;
        document.getElementById("supPlantDes").value = supPlantDes;
        document.getElementById("recplantCode").value = recplantCode;
        document.getElementById("recplantDes").value = recplantDes;
        document.getElementById("storageLocation").value = storageLocation;
        document.getElementById("purchaseOrderNo").value = purchaseOrderNo;
        document.getElementById("grnID").value = grnID;
    }

</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Product Master" text="GRN VIEW "/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="INBOUND"/></a></li>
                <li class=""><spring:message code="hrms.label.GRN VIEW" text="GRN VIEW"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">
                    <form name="productMaster"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <br>
                        <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                            <input type="hidden" name="supplierId" id="supplierId" value=""  />
                            <c:if test="${param=='edit'}">
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="8" >GRN VIEW</th>
                                    </tr>
                                </thead>
                                <tr>
       
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Gate Inward No</td>
                                    <td class="text1"><input type="text" name="gateInwardNo" id="gateInwardNo" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checksupplierCategoryName();" autocomplete="off"/></td>
                                    
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>vehicle NO</td>
                                    <td class="text1"><input type="text" name="vehicleNO" id="vehicleNO" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checksupplierCategoryName();" autocomplete="off"/></td>
                                    <input type="hidden" name="grnID" id="grnID" class="form-control" style="width:240px;height:40px" maxlength="50"/>
                              
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Material Code</td>
                                    <td class="text1"><input type="text" name="materialCode" id="materialCode" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checksupplierCategoryName();" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Material Des</td>
                                    <td class="text1"><input type="text" name="materialDes" id="materialDes" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checksupplierCategoryName();" autocomplete="off"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Gr Quantity</td>
                                    <td class="text1"><input type='text' align="center" class="form-control" style="width:240px;height:40px" name="giQuantity" id="giQuantity" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Sup Plant Code</td>
                                    <td class="text1"><input type="text" name="supPlantCode" id="supPlantCode" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Sup Plant Des</td>
                                    <td class="text1"><input type="text" name="supPlantDes" id="supPlantDes" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Rec plant Code</td>
                                    <td class="text1"><input type="text" name="recplantCode" id="recplantCode" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Rec plant Des</td>
                                    <td class="text1"><input type="text" name="recplantDes" id="recplantDes" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Storage Location</td>
                                    <td class="text1"><input type="text" name="storageLocation" id="storageLocation" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Purchase Order No</td>
                                    <td class="text1"><input type="text" name="purchaseOrderNo" id="purchaseOrderNo" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                <center><a>
                                        <input  type="button" class="btn btn-success" align="center" value="Save" name="Submit" onClick="submitPage()">      
                                        &emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">
                                    </a>
                                </center>
                                </tr>
                            </table>
                            </c:if>
                            <br>
                            <table class="table table-info mb30 table-hover" id="table" >	
                                <thead>

                                    <tr height="30" style="color: white">
                                        <th>S No</th>
                                        
                                        <th>Gate Inward No</th>
                                        <th>vehicle NO</th>
                                        <th>Material Code</th>
                                        <th>Material Des</th>
                                        <th>Gr Quantity</th>
                                        <th>Sup Plant Code</th>
                                        <th>Sup Plant Des</th>
                                        <th>Rec plant Code</th>
                                        <th>Rec plant Des</th>
                                        <th>Storage Location</th>
                                        <th>Purchase Order No</th>
                                        <c:if test="${param=='edit'}">
                                        <th>Edit</th>
                                        </c:if>
                                    </tr>
                                </thead>
                                <tbody>


                                    <% int sno = 0;%>
                                    <c:if test = "${ProductMasters != null}">
                                        <c:forEach items="${ProductMasters}" var="Pro">
                                            <%
                                                sno++;
                                                String className = "text1";
                                                if ((sno % 1) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                            %>

                                            <tr>
                                                <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.gateInwardNo}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.vehicleNO}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.materialCode}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.materialDes}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.giQuantity}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.supPlantCode}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.supPlantDes}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.recplantCode}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.recplantDes}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.storageLocation}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${Pro.purchaseOrderNo}" /></td>
                                                <c:if test="${param=='edit'}">
                                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>,  '<c:out value="${Pro.gateInwardNo}" />', '<c:out value="${Pro.vehicleNO}" />', '<c:out value="${Pro.materialCode}" />', '<c:out value="${Pro.materialDes}" />', '<c:out value="${Pro.giQuantity}" />', '<c:out value="${Pro.supPlantCode}" />', '<c:out value="${Pro.supPlantDes}" />','<c:out value="${Pro.recplantCode}" />','<c:out value="${Pro.recplantDes}" />', '<c:out value="${Pro.storageLocation}" />', '<c:out value="${Pro.purchaseOrderNo}" />', '<c:out value="${Pro.deliveryNo}" />', '<c:out value="${Pro.cancelledNumber}" />', '<c:out value="${Pro.grnID}" />');" /></td>
                                                </c:if>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </c:if>
                            </table>


                            <input type="hidden" name="count" id="count" value="<%=sno%>" />

                            <br>
                            <br>
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>
                            
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>

