<%-- 
    Document   : connectIndentConfirmDetails
    Created on : 16 Sep, 2021, 12:51:15 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
    function isNumberKey(e)
    {
        var key = (e.which) ? e.which : e.keyCode;
        if ((key != 46 && key > 31) && (key < 48 || key > 57)) {
            return false;
        }
        return true;
    }

</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Connect Indent Request</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">HS Connect</a></li>
                <li class="">Connect Indent Request</li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="connect"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>

                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30">
                                    <th>S.No</th>
                                    <th>Indent Date</th>
                                    <th>From Location</th>
                                    <th>To Location</th>
                                    <th>Vehicle Type</th>
                                    <th>Party Name</th>
                                    <th>Delivery Type</th>
                                    <th>Delivery Date</th>
                                    <th>Appointment Date</th>
                                    <th>Required Quantity</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                                <c:if test = "${connectIndentRequest != null}">
                                    <c:forEach items="${connectIndentRequest}" var="pc">
                                        <%
                                            sno++;
                                            String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.indentDate}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.fromLocation}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.toLocation}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.vehicleType}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.partyName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.deliveryType}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.deliveryDate}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.appointmentDate}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.qty}" /></td>
                                    <input type="hidden" name="indentId" id="indentId" value="<c:out value="${pc.indentId}"/>"/>
                                    <input type="hidden" name="filledQty" id="filledQty" value="<c:out value="${pc.inpQty}"/>"/>
                                    <input type="hidden" name="requiredQty" id="requiredQty" value="<c:out value="${pc.qty}"/>"/>
                                    <input type="hidden" name="plant" id="plant" value="<c:out value="${pc.plantCode}"/>"/>
                                    <input type="hidden" name="fromLocation" id="fromLocation" value="<c:out value="${pc.whId}"/>"/>
                                    <input type="hidden" name="vehicleType" id="vehicleType" value="<c:out value="${pc.vehicleTypeId}"/>"/>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </c:if>
                        </table>
                        <br>
                        <br>
                        <table class="table table-info mb30 table-hover sortable" id="addIndent" align="center" width="90%">
                            <thead >
                                <tr>
                                    <th>S.No</th>
                                    <th>Vehicle No</th>  
                                    <th>Driver Name</th>
                                    <th>Driver Mobile</th>
                                    <th>Expected Delivery Date</th>
                                    <th>Delete</th>

                                </tr>
                            </thead>
                        </table>
                        <table>
                            <center>
                                <input value="Add Row" class="btn btn-success" id="countRow" type="button" onClick="addRow();" >
                                &emsp;<input type="button" value="Save" id="insert" class="btn btn-success" onClick="submitPage(this.value);">
                                &emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">
                            </center>

                        </table>
                        <input type="hidden" name="count" id="count" value="<%=sno%>" />
                        <script>
                            var rowCount = 0;
                            var sno = 0;
                            var rowIndex = 0;
                            var filled = document.getElementById("filledQty").value;
                            var requiredQty = document.getElementById("requiredQty").value;

                            function addRow() {
                                var tab = document.getElementById("addIndent");
                                var iRowCount = tab.getElementsByTagName('tr').length;
                                if ((parseInt(filled) + parseInt(sno)) <= parseInt(requiredQty)) {
                                    rowCount = iRowCount;
                                    var newrow = tab.insertRow(rowCount);
                                    sno = rowCount;
                                    rowIndex = rowCount;
                                    var cell = newrow.insertCell(0);
                                    var cell0 = "<td class='text1' height='25' > <input type='hidden'  name='Sno' value='' > " + sno + "</td>";
                                    cell.innerHTML = cell0;

                                    cell = newrow.insertCell(1);
                                    var cell2 = "<td class='text1' height='25'><input class='form-control' name='vehicleNo' id='vehicleNo" + sno + "' style='width:180px;height:35px'/></td>"
                                    cell.innerHTML = cell2;

                                    cell = newrow.insertCell(2);
                                    var cell2 = "<td class='text1' height='25'><input class='form-control' name='driverName' id='driverName" + sno + "' style='width:180px;height:35px'/></td>"
                                    cell.innerHTML = cell2;

                                    cell = newrow.insertCell(3);
                                    var cell2 = "<td class='text1' height='25'><input class='form-control' name='driverMobile' id='driverMobile" + sno + "' style='width:180px;height:35px'/></td>"
                                    cell.innerHTML = cell2;

                                    cell = newrow.insertCell(4);
                                    var cell2 = "<td class='text1' height='25'><input type='text' class='form-control datepicker' name='deliveryDate' id='deliveryDate" + sno + "' value='' style='width:180px;height:35px'/>\n\
                                                <input type='hidden' class='form-control' name='placedQty' id='placedQty" + sno + "' value='1' readonly style='width:180px;height:35px'/></td>"
                                    cell.innerHTML = cell2;


                                    cell = newrow.insertCell(5);
                                    var cell2 = "<td class='text1' height='25' ><input type='button' class='btn btn-info'   name='delete'  id='delete'   value='Delete Row' onclick='deleteRow(this," + sno + ");' ></td>";
                                    cell.innerHTML = cell2;

                                    rowIndex++;
                                    rowCount++;
                                    sno++;
                                }else{
                                    alert("Quantity Exceeded");
                                }
                                $(".datepicker").datepicker();

                            }
                            function submitPage(){
                                document.connect.action = '/throttle/connectIndentConfirm.do?param=save';
                                document.connect.submit();
                            }
                            function deleteRow(src) {
                                sno--;
                                var oRow = src.parentNode.parentNode;
                                var dRow = document.getElementById('addIndent');
                                dRow.deleteRow(oRow.rowIndex);
                                document.getElementById("selectedRowCount").value--;
                            }
                        </script>
                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>