

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>
<link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>
<script type="text/javascript">
    function submitpage() {
        document.dept.action = "/throttle/handleUpdateSku.do";
        document.dept.submit();
    }




    function setValues(elem, sno, serialNumber, itemId) {
        if (elem.checked) {
            document.getElementById("selectedStatus"+sno).value = "1";
        } else {
            document.getElementById("selectedStatus"+sno).value = "0";
        }
        document.getElementById("oldItemId").value = itemId;
    }
</script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.SKU Update"  text="SKU Update"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="trucks.label.Inbound"  text="Inbound"/></a></li>
            <li class="active"><spring:message code="stores.label.SKU Update"  text="SKU Update"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">
            <body>
                <form name="dept"  method="post">

                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover">
                        <thead>
                            <tr>
                                <th colspan="2"  ><div ><spring:message code="trucks.label.update"  text="Update SKU"/></div></th>
                            </tr>
                        </thead>
                        <input type="hidden" id="oldItemId" name="oldItemId" value="">
                        <tr>

                            <td  ><font color=red>*</font>Item Name </td>
                            <td>
                                <select  style="width:200px;height:40px" type="text"  class="select2-search form-control" id="itemId" name="itemId">
                                    <option value="">---Select ItemName---</option>
                                    <c:forEach items="${getItemList}" var="item">
                                        <option value="<c:out value="${item.itemId}"/>">
                                            <c:out value="${item.itemName}"/> ~ <c:out value="${item.skuCode}"/></option>
                                        </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <script>
                            $('#itemId').select2({placeholder: 'Select Item Name'});
                            $('#itemId').val('');
                        </script>
                        <tr>
                        </tr>

                    </table>
                    <center>
                        <input type="button" value="Save" class="btn btn-success" onclick="submitpage();">
                        &emsp;<input type="reset" class="btn btn-success" value="Clear">
                    </center>
                    <br>

                    <table class="table table-info mb30 table-hover " id="table">

                        <thead>

                            <tr >
                                <th>S.No</th>
                                <th>Serial No</th>
                                <th>SKU Code</th>
                                <th align="right">Edit</th>
                            </tr>
                        </thead>
                        <tbody>


                            <% int sno = 0;%>
                            <c:if test = "${picklistMaster != null}">
                                <c:forEach items="${picklistMaster}" var="cml">
                                    <%
                                                sno++;
                                                String className = "text1";
                                                if ((sno % 1) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>

                                    <tr>
                                        <td align="left"> <%= sno%> </td>
                                        <td align="left"><c:out value="${cml.serialNumber}"/></td>
                                        <td align="left"><c:out value="${cml.itemName}"/></td>
                                        <td align="left"> 
                                            <input type="checkbox" id="edit<%=sno%>" onclick="setValues(this,<%= sno%>, '<c:out value="${cml.serialNumber}" />', '<c:out value="${cml.itemId}" />');" />
                                            <input type="hidden" id="selectedStatus<%=sno%>" name="selectedStatus" value="0"/>
                                        </td>
                                    </tr>
                                <input type="hidden" name="serialNumber" id="serialNumber<%=sno%>" value="<c:out value="${cml.serialNumber}"/>" />
                            </c:forEach>
                            </tbody>
                            <input type="hidden" name="count" id="count" value="<%=sno%>" />
                        </c:if>
                        <input type="hidden" name="mfrId" id="mfrId" > 
                    </table>
                    <br>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>

                    <br>
                    <!--<div id="myMap" style="width: 1000px; height: 400px; margin-top:20px;"></div>-->

                    </body>
                    </div>


                    </div>
                    </div>





                    <%@ include file="/content/common/NewDesign/settings.jsp" %>