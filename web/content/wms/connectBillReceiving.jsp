<%-- 
    Document   : connectBillReceiving
    Created on : 6 Sep, 2021, 7:05:07 PM
    Author     : Roger
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
    function submitPage() {
        if (document.getElementById("billNo").value == "") {
            alert("Please enter Bill No.\n");
            document.getElementById("billNo").focus();
        } else if (document.getElementById("billDate").value == "") {
            alert("Please Enter Bill Date.\n");
            document.getElementById("billDate").focus();
        } else if (document.getElementById("baseFreight").value == "") {
            alert("Please Enter Base Freight.\n");
            document.getElementById("baseFreight").focus();
        } else if (document.getElementById("unloadingCharge").value == "") {
            alert("Please Enter Unloading Charge.\n");
            document.getElementById("unloadingCharge").focus();
        } else if (document.getElementById("haltingCharge").value == "") {
            alert("Please Enter Halting Charge.\n");
            document.getElementById("haltingCharge").focus();
        } else if (document.getElementById("otherCharges").value == "") {
            alert("Please Enter Other Charges.\n");
            document.getElementById("otherCharges").focus();
        } else if (document.getElementById("gstCharge").value == "") {
            alert("Please Enter GST Charge.\n");
            document.getElementById("gstCharge").focus();
        } else if (document.getElementById("billAmount").value == "") {
            alert("Please Enter Bill Amount.\n");
            document.getElementById("billAmount").focus();
        } else if (document.getElementById("vendorId").value == "") {
            alert("Please Enter Transporter.\n");
            document.getElementById("vendorId").focus();
        } else if (document.getElementById("periodFrom").value == "") {
            alert("Please Enter Period From.\n");
            document.getElementById("periodFrom").focus();
        } else if (document.getElementById("periodTo").value == "") {
            alert("Please Enter Period To.\n");
            document.getElementById("periodTo").focus();
        } else if (document.getElementById("dateOfReceived").value == "") {
            alert("Please Enter Date of Received.\n");
            document.getElementById("dateOfReceived").focus();
        } else if (document.getElementById("dateOfEntry").value == "") {
            alert("Please Enter Date of Entry.\n");
            document.getElementById("dateOfEntry").focus();
        } else if (document.getElementById("receivedBy").value == "") {
            alert("Please Enter Received By.\n");
            document.getElementById("receivedBy").focus();
        } else {
            document.connect.action = " /throttle/connectBillReceiving.do?&param=save";
            document.connect.method = "post";
            document.connect.submit();
        }
    }
    function setValues(sno,billId, refNo, billNo, billDate, freight, unloading, halting, others, gst, billAmount, vendorId, periodFrom, periodTo, dateOfReceived, dateOfEntry, receivedBy) {
        var count = parseInt(document.getElementById("count").value);
        for (var i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("billId").value = billId;
        document.getElementById("referenceNo").value = refNo;
        document.getElementById("billNo").value = billNo;
        document.getElementById("billDate").value = billDate;
        document.getElementById("baseFreight").value = freight;
        document.getElementById("unloadingCharge").value = unloading;
        document.getElementById("haltingCharge").value = halting;
        document.getElementById("otherCharges").value = others;
        document.getElementById("gstCharge").value = gst;
        document.getElementById("billAmount").value = billAmount;
        document.getElementById("vendorId").value = vendorId;
        document.getElementById("periodFrom").value = periodFrom;
        document.getElementById("periodTo").value = periodTo;
        document.getElementById("dateOfReceived").value = dateOfReceived;
        document.getElementById("dateOfEntry").value = dateOfEntry;
        document.getElementById("receivedBy").value = receivedBy;
    }



    function isNumberKey(e)
    {
        var key = (e.which) ? e.which : e.keyCode;
        if((key!=46 && key>31)&&(key<48 || key > 57)){
            return false;
        }
        return true;
    }
    
    function addAmount(){
        var freight = $('#baseFreight').val();
        var halting = $('#haltingCharge').val();
        var unloading = $('#unloadingCharge').val();
        var other = $('#otherCharges').val();
        var gst = $('#gstCharge').val();
        if(freight == ""){
            freight = 0;
        } 
        if(halting==""){
            halting =0;
        } 
        if(unloading==""){
            unloading=0;
        } 
        if(other==""){
           other = 0;
        }
        if(gst==""){
           gst = 0;
        }
        var total = parseInt(freight)+parseInt(halting)+parseInt(unloading)+parseInt(other)+parseInt(gst);
             $('#billAmount').val(total);
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Connect Bill Receiving </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">HS Connect</a></li>
                <li class="">Connect Bill Receiving</li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="connect"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <br>
                        <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                            <input type="hidden" name="billId" id="billId" value=""  />
                            <tr>
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="8" >Bill Details</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="text1" style="display:none">&nbsp;&nbsp;<font color="red">*</font>Reference No</td>
                                    <td class="text1" style="display:none"><input type="text" name="referenceNo" id="referenceNo" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checkproductCategoryName();" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Bill No</td>
                                    <td class="text1"><input type="text" name="billNo" id="billNo" class="form-control" style="width:240px;height:40px" autocomplete="off" onkeypress="return isNumberKey(event)"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Bill Date</td>
                                    <td class="text1"><input type="text" name="billDate" id="billDate" class="form-control datepicker" style="width:240px;height:40px" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Base Freight</td>
                                    <td class="text1"><input type="text" name="baseFreight" id="baseFreight" onkeyup="addAmount()" onkeypress="return isNumberKey(event)" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Unloading Charges</td>
                                    <td class="text1"><input type="text" name="unloadingCharge" id="unloadingCharge" onkeyup="addAmount()" onkeypress="return isNumberKey(event)" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Halting Charges</td>
                                    <td class="text1"><input type="text" name="haltingCharge" id="haltingCharge" onkeyup="addAmount()"  onkeypress="return isNumberKey(event)"class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Other Charges</td>
                                    <td class="text1"><input type="text" name="otherCharges" id="otherCharges" onkeyup="addAmount()" onkeypress="return isNumberKey(event)" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>GST Charges</td>
                                    <td class="text1"><input type="text" name="gstCharge" id="gstCharge" onkeyup="addAmount()" onkeypress="return isNumberKey(event)"class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Bill Amount</td>
                                    <td class="text1"><input type="text" readonly name="billAmount" id="billAmount" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Transporter</td>
                                    <td class="text1">
                                        <select name="vendorId" id="vendorId" class="form-control" style="width:240px;height:40px">
                                            <option value="">------Select One------</option>
                                            <c:forEach items="${vendorList}" var="ven">
                                                <option value="<c:out value="${ven.vendorId}"/>"><c:out value="${ven.vendorName}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Period From</td>
                                    <td class="text1"><input type="text" name="periodFrom" id="periodFrom" class="form-control datepicker" style="width:240px;height:40px" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Period To</td>
                                    <td class="text1"><input type="text" name="periodTo" id="periodTo" class="form-control datepicker" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Date Of Received</td>
                                    <td class="text1"><input type="text" name="dateOfReceived" id="dateOfReceived" class="form-control datepicker" style="width:240px;height:40px" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Date Of Entry</td>
                                    <td class="text1"><input type="text" name="dateOfEntry" id="dateOfEntry" class="form-control datepicker" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Received By</td>
                                    <td class="text1"><input type="text" name="receivedBy" id="receivedBy" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>
                            </table>
                            </tr>
                            <tr>
                                <td>
                                    <br>
                                    <center>
                                        <input type="button" class="btn btn-success" value="Save" name="Submit" onClick="submitPage()">
                                    </center>
                                </td>
                            </tr>
                        </table>
                        <br>

                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30">
                                    <th>S.No</th>
                                    <th>Reference No</th>
                                    <th>Bill No</th>
                                    <th>Bill Date</th>
                                    <th>Base Freight</th>
                                    <th>Unloading Charges</th>
                                    <th>Halting Charges</th>
                                    <th>Other Charges</th>
                                    <th>GST Charges</th>
                                    <th>Bill Amount</th>
                                    <th>Vendor Name</th>
                                    <th>Period From</th>
                                    <th>Period To</th>
                                    <th>Date of Received</th>
                                    <th>Date Of Entry</th>
                                    <th>Received By</th>
                                    <th>Status</th>
                                    <th>Select</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                                <c:if test = "${connectGetBillReceiving != null}">
                                    <c:forEach items="${connectGetBillReceiving}" var="pc">
                                        <%
                                            sno++;
                                            String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.referenceNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.billNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.billDate}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.freightAmount}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.unloadingAmt}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.haltingAmt}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.otherAmt}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.gstAmt}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.billAmt}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.vendorName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.periodFrom}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.periodTo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.dateOfReceived}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.dateOfEntry}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.receivedBy}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.status}" /></td>
                                            <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${pc.billId}" />','<c:out value="${pc.referenceNo}" />', '<c:out value="${pc.billNo}" />', '<c:out value="${pc.billDate}" />', '<c:out value="${pc.freightAmount}" />', '<c:out value="${pc.unloadingAmt}" />', '<c:out value="${pc.haltingAmt}" />', '<c:out value="${pc.otherAmt}" />', '<c:out value="${pc.gstAmt}" />', '<c:out value="${pc.billAmt}" />', '<c:out value="${pc.vendorId}" />','<c:out value="${pc.periodFrom}" />','<c:out value="${pc.periodTo}" />','<c:out value="${pc.dateOfReceived}" />','<c:out value="${pc.dateOfEntry}" />','<c:out value="${pc.receivedBy}" />');" /></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>


                        <input type="hidden" name="count" id="count" value="<%=sno%>" />

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>