<%-- 
    Document   : fkRunsheetReport
    Created on : 12 Jun, 2021, 8:35:09 AM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>
<script type="text/javascript">
    function searchPage() {
        document.fk.action = '/throttle/fkRunsheetReport.do';
        document.fk.submit();
    }
    function excelPage() {
        document.fk.action = '/throttle/fkRunsheetReport.do?&param=ExportExcel';
        document.fk.submit();
    }
    function tripSheetDetails(val){
         window.open('/throttle/viewFkTripDetails.do?&param=view&tripId=' + val, 'PopupPage', 'height = 600, width = 1000, scrollbars = yes, resizable = yes');
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="Runsheet Report"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.IFB" text="Flipkart"/></a></li>
            <li class=""><spring:message code="hrms.label.ifb" text="Flipkart Runsheet Report"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >
                <form name="fk"  method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <div id="ptp" style="overflow: auto">
                        <div class="inpad">
                            <table class="table table-info mb30 table-hover" style="width:100%">
                                <thead><tr><th colspan="7">Fliparkt Runsheet Report</th></tr></thead>
                                <tr>
                                    <td>From Date</td>
                                    <td><input type="text" class="datepicker form-control" autocomplete="off" name="fromDate" id="fromDate" style="width:200px;height:40px"/></td>
                                    <td>To Date</td>
                                    <td><input type="text" class="datepicker form-control" autocomplete="off" name="toDate" id="toDate" style="width:200px;height:40px"/></td>
                                </tr>
                                <tr>
                                    <td  ><font color="red">*</font>Shipment Type</td>
                                    <td>
                                        <select class="form-control" id="orderType" name="orderType" style="width:200px;height: 40px">
                                            <option value="">------Select One------</option>
                                            <option value="1">Normal Forward</option>
                                            <option value="2">PREXO</option>
                                            <option value="3">RVP</option>
                                        </select>
                                    </td>
                                    <td><font color="red">*</font>DE Name</td>
                                    <td>
                                        <select class="form-control" id="empId" name="empId" style="width:200px;height: 40px">
                                            <option value="">------Select One------</option>
                                            <c:forEach items="${deliveryExecutiveList}" var="de">
                                                <option value="<c:out value="${de.empId}"/>"><c:out value="${de.empName}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <c:if test="${RoleId == 1023 || RoleId == 1016 }">
                                        <td><font color="red">*</font>WareHouse</td>
                                     <td>
                                        <select class="form-control" id="whId" name="whId" style="width:200px;height: 40px">
                                            <option value="">------Select One------</option>
                                            <c:forEach items="${getWareHouseList}" var="de">
                                                <option value="<c:out value="${de.warehouseId}"/>"><c:out value="${de.whName}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                    </c:if>
                                </tr>
                            </table>
                            <script>
                                document.getElementById('orderType').value = '<c:out value="${orderType}"/>'
                                document.getElementById('fromDate').value = '<c:out value="${fromDate}"/>'
                                document.getElementById('toDate').value = '<c:out value="${toDate}"/>'
                                document.getElementById('empId').value = '<c:out value="${empId}"/>'
                            </script>
                            <center>
                                <input type="button" class="btn btn-success"   value="Search" onclick="searchPage()">&emsp;&emsp;
                                <input type="button" class="btn btn-success"   value="Export Excel" onclick="excelPage()">
                            </center>

                            <div id="ptp" style="overflow: auto">
                                <div class="inpad">
                                    <table class="table table-info mb30 table-hover" id="table" >

                                        <thead>
                                            <tr height="40">
                                                <th  height="30" >S.No</th>
                                                <th  height="30" >Runsheet No</th>
                                                <th  height="30" >Assigned Date</th>
                                                <th  height="30" >DE Name</th>
                                                <th  height="30" >Hub Name</th>
                                                <th  height="30" >Vehicle No</th>
                                                <th  height="30" >Delivered</th>
                                                <th  height="30" >Undelivered</th>
                                                <th  height="30" >Forward</th>
                                                <th  height="30" >PREXO</th>
                                                <th  height="30" >RVP</th>
                                                <th  height="30" >Total Quantity</th>
                                                <th  height="30" >User Name</th>
                                                <th  height="30" >Status</th>
                                            </tr>
                                        </thead>
                                        <% int index = 0;
                                            int sno = 1;
                                        %>
                                        <tbody>

                                            <c:if test = "${fkRunsheetReport != null}" >
                                                <c:forEach items= "${fkRunsheetReport}" var="fk">
                                                    <tr height="30">
                                                        <td align="left"><%=sno%></td>
                                                        <td align="left"   ><a style="font-size:15;cursor: pointer" onclick="tripSheetDetails(<c:out value='${fk.tripId}'/>)"><c:out value="${fk.tripCode}"/></a></td>
                                                        <td align="left"   ><c:out value="${fk.date}"/></td>
                                                        <td align="left"   ><c:out value="${fk.driverName}"/></td>
                                                        <td align="left"   ><c:out value="${fk.whName}"/></td>
                                                        <td align="left"   ><c:out value="${fk.vehicleNo}"/></td>
                                                        <td align="left"   ><c:out value="${fk.delivered}"/></td>
                                                        <td align="left"   ><c:out value="${fk.returned}"/></td>
                                                        <td align="left"   ><c:out value="${fk.inbound}"/></td>
                                                        <td align="left"   ><c:out value="${fk.prexo}"/></td>
                                                        <td align="left"   ><c:out value="${fk.rvp}"/></td>
                                                        <td align="left"   ><c:out value="${fk.qty}"/></td>
                                                        <td align="left"   ><c:out value="${fk.empName}"/></td>
                                                        <td align="left"   >
                                                            <c:if test="${fk.tripStatusId==8}">
                                                                <c:if test="${fk.status==1}">
                                                                    Completed Delivery
                                                                </c:if>
                                                                <c:if test="${fk.status==2}">
                                                                    Delivery Pending
                                                                </c:if>
                                                                <c:if test="${fk.status==3}">
                                                                    Runsheet Assigned
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${fk.tripStatusId==13}">
                                                                Runsheet Closed(In Reconciliation)
                                                            </c:if>
                                                            <c:if test="${fk.tripStatusId==14||fk.tripStatusId==15}">
                                                                In Cash Deposit
                                                            </c:if>
                                                            <c:if test="${fk.tripStatusId==16}">
                                                                In Cash Deposit Proof Upload
                                                            </c:if>
                                                            <c:if test="${fk.tripStatusId==17}">
                                                                Waiting to submit Deposit Proof
                                                            </c:if>
                                                            <c:if test="${fk.tripStatusId==18}">
                                                                In Finance Approval
                                                            </c:if>
                                                            <c:if test="${fk.tripStatusId==19}">
                                                                In Manager Approval
                                                            </c:if>
                                                            <c:if test="${fk.tripStatusId==19}">
                                                                Approved
                                                            </c:if>
                                                            
                                                        </td>
                                                    </tr>
                                                    <%
                                                        sno++;
                                                        index++;
                                                    %>
                                                </c:forEach>
                                            <input type="hidden" name="sno" id="sno" value="<%=sno%>">
                                        </c:if>
                                        <script language="javascript" type="text/javascript">
                                            setFilterGrid("table");
                                        </script>
                                        </tbody>
                                    </table>
                                </div>
                            </div>    

                        </div>
                    </div>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
