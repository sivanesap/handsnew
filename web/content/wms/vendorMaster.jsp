<%-- 
    Document   : vendorMaster
    Created on : 16 Jun, 2021, 8:20:18 PM
    Author     : MAHENDIRAN R
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    function submitPage() {
        document.VendorMaster.action = " /throttle/saveVendorMaster.do";
        document.VendorMaster.method = "post";
        document.VendorMaster.submit();
    }
    function setValues(sno, transportCode, vendorName, city, state, pincode, address, address1, contactNo, contactPerson, emailId, refNo, gstNo,
            gstPercentage, tdsPercentage) {
        var count = parseInt(document.getElementById("count").value);

        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("transportCode").value = transportCode;
        document.getElementById("vendorName").value = vendorName;
        document.getElementById("city").value = city;
        document.getElementById("state").value = state;
        document.getElementById("pincode").value = pincode;
        document.getElementById("address").value = address;
        document.getElementById("address1").value = address1;
        document.getElementById("contactNo").value = contactNo;
        document.getElementById("contactPerson").value = contactPerson;
        document.getElementById("emailId").value = emailId;
        document.getElementById("refNo").value = refNo;
        document.getElementById("gstNo").value = gstNo;
        document.getElementById("gstPercentage").value = gstPercentage;
        document.getElementById("tdsPercentage").value = tdsPercentage;
    }

</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Supplier Master" text="Vendor Master"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Master"/></a></li>
                <li class=""><spring:message code="hrms.label.Vendor Master" text="Vendor Master"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">
                    <form name="VendorMaster"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <br>
                        <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                            <input type="hidden" name="transportCode" id="transportCode" value=""  />
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="8" >Vendor Master</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Vendor Name</td>
                                    <td><input type="text" id="vendorName" name="vendorName" class="form-control" style="width:240px;height:40px"></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>City</td>
                                    <td class="text1"><input type='text' align="center" class="form-control" style="width:240px;height:40px" name="city" id="city" autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>State</td>
                                    <td class="text1">
                                        <select  align="center" class="form-control" style="width:240px;height:40px" name="state" id="state" >
                                            <option value=''>--select--</option>                            
                                            <c:if test = "${getStateList != null}" >
                                                <c:forEach items="${getStateList}" var="Type"> 
                                                    <option value='<c:out value="${Type.stateId}" />'><c:out value="${Type.stateName}" /></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>  
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Postal Code</td>
                                    <td class="text1"><input type="text" name="pincode" id="pincode" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Address 1</td>
                                    <td class="text1"><input type="text" name="address" id="address" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Address 2</td>
                                    <td class="text1"><input type="text" name="address1" id="address1" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checksupplierCategoryName();" autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Contact No</td>
                                    <td class="text1"><input type="text" name="contactNo" id="contactNo" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Contact Person</td>
                                    <td class="text1"><input type="text" name="contactPerson" id="contactPerson" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>E-mail id</td>
                                    <td class="text1"><input type="text" name="emailId" id="emailId" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Ref No</td>
                                    <td class="text1"><input type="text" name="refNo" id="refNo" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>GST Number</td>
                                    <td class="text1"><input type="text" name="gstNo" id="gstNo" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>GST Percentage</td>
                                    <td class="text1"><input type="text" name="gstPercentage" id="gstPercentage" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>TDS Percentage</td>
                                    <td class="text1"><input type="text" name="tdsPercentage" id="tdsPercentage" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Status</td>
                                    <td class="text1">
                                        <select  align="center" class="form-control" style="width:240px;height:40px" name="ActiveInd" id="ActiveInd" >
                                            <option value='Y'>Active</option>
                                            <option value='N' id="inActive" style="display: none">In-Active</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                <center><a>
                                        <input  type="button" class="btn btn-success" align="center" value="Save" name="Submit" onClick="submitPage()">
                                        &emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">
                                    </a>
                                </center>
                                </tr>
                            </table>
                            <br>
                            <table class="table table-info mb30 table-hover" id="table" >	
                                <thead>

                                    <tr height="30" style="color: white">
                                        <th>S No</th>
                                        <th>Transport Code</th>
                                        <th>Vendor Name</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th>Postal Code</th>
                                        <th>Address 1</th>
                                        <th>Address 2</th>
                                        <th>Contact No</th>
                                        <th>Contact Person</th>
                                        <th>Email Id</th>
                                        <th>Ref No</th>
                                        <th>GST Number</th>
                                        <th>GST Percentage</th>
                                        <th>TDS Percentage</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>


                                    <% int sno = 0;%>
                                    <c:if test = "${vendorMaster != null}">
                                        <c:forEach items="${vendorMaster}" var="ven">
                                            <%
                                                sno++;
                                                String className = "text1";
                                                if ((sno % 1) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                            %>

                                            <tr>
                                                <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${ven.transportCode}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${ven.vendorName}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${ven.city}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${ven.state}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${ven.pincode}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${ven.address}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${ven.address1}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${ven.contactNo}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${ven.contactPerson}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${ven.emailId}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${ven.refNo}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${ven.gstNo}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${ven.gstPercentage}" /></td>
                                                <td class="<%=className%>"  align="left"> <c:out value="${ven.tdsPercentage}" /></td>
                                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${ven.transportCode}" />', '<c:out value="${ven.vendorName}" />', '<c:out value="${ven.city}" />', '<c:out value="${ven.state}" />', '<c:out value="${ven.pincode}" />', '<c:out value="${ven.address}" />', '<c:out value="${ven.address1}" />', '<c:out value="${ven.contactNo}" />', '<c:out value="${ven.contactPerson}" />', '<c:out value="${ven.emailId}" />', '<c:out value="${ven.refNo}" />', '<c:out value="${ven.gstNo}" />','<c:out value="${ven.gstPercentage}" />','<c:out value="${ven.tdsPercentage}" />');" /></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </c:if>
                            </table>


                            <input type="hidden" name="count" id="count" value="<%=sno%>" />

                            <br>
                            <br>
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" selected="selected">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span>Entries Per Page</span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("table", 1);
                            </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>

