<%-- 
    Document   : connectBillMapping
    Created on : 9 Sep, 2021, 12:04:27 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
    function viewPage(billId){
        document.connect.action = '/throttle/connectBillMapping.do?param=view&billId='+billId;
        document.connect.submit();
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Connect Bill Mapping </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">HS Connect</a></li>
                <li class="">Connect Bill Mapping</li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">


                    <form name="connect"  method="post" >
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <table class="table table-info mb30 table-hover" id="table" >	
                            <thead>

                                <tr height="30">
                                    <th>S.No</th>
                                    <th>Reference No</th>
                                    <th>Bill No</th>
                                    <th>Bill Date</th>
                                    <th>Base Freight</th>
                                    <th>Unloading Charges</th>
                                    <th>Halting Charges</th>
                                    <th>Other Charges</th>
                                    <th>GST Charges</th>
                                    <th>Bill Amount</th>
                                    <th>Vendor Name</th>
                                    <th>Period From</th>
                                    <th>Period To</th>
                                    <th>Date of Received</th>
                                    <th>Date Of Entry</th>
                                    <th>Received By</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                                <c:if test = "${connectGetBillReceiving != null}">
                                    <c:forEach items="${connectGetBillReceiving}" var="pc">
                                        <%
                                            sno++;
                                            String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.referenceNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.billNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.billDate}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.freightAmount}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.unloadingAmt}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.haltingAmt}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.otherAmt}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.gstAmt}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.billAmt}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.vendorName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.periodFrom}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.periodTo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.dateOfReceived}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.dateOfEntry}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.receivedBy}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.status}" /></td>
                                            <td class="<%=className%>"><span class="label label-success" style="cursor: pointer" onclick="viewPage(<c:out value="${pc.billId}"/>);">Process</span></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>


                        <input type="hidden" name="count" id="count" value="<%=sno%>" />

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>