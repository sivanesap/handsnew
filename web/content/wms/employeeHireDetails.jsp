<%-- 
    Document   : employeeHireDetails
    Created on : 28 Feb, 2022, 3:11:28 AM
    Author     : alan
--%>

<!DOCTYPE html>
<html lang="en">
    <%@page language="java" contentType="text/html; charset=UTF-8"%>
    <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
    <%@page import="java.util.Date"%>
    <%@page import="java.text.SimpleDateFormat"%>
    <%@page import="java.text.DateFormat"%>

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
        <script language="javascript" src="/throttle/js/validate.js"></script>  
        <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
        <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
        <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
        <script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
        <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

        <style type="text/css" title="currentStyle">
            /*@import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";*/
        </style>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <style>
            .main{

                margin: 2%; 
                box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
                border-radius: 13px;
            } 


            body {font-family: Arial;}


            .tab {
                overflow: hidden;
                border: 1px solid #ccc;
                padding: 2px 15px;
                background: linear-gradient(to right,#0c70d4 ,#96c5fc,#0c70d4);
                border-radius: 10px 10px 0px 0px;
                box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;

            }


            .tab button {
                background-color: inherit;
                float: left;
                border: none;
                outline: none;
                cursor: pointer;
                padding: 14px 16px;
                transition: 0.3s;
                font-size: 17px;
                color: white;
                width: 200px;
                margin: 0px 3px;
            }


            .tab button:hover {
                background-color: rgb(66, 178, 252);
                border-radius: 10px 10px 0px 0px;
                height: 50px;
            }

            .tab button.active {
                background-color: rgb(105, 194, 253);
                border-radius: 10px 10px 0px 0px;
            }

            .tabcontent {
                display: none;
                padding: 6px 12px;
                border: 1px solid #ccc;
                border-top: 100px;
                height: 100%;

            }
            .box{
                background-color: rgb(255, 255, 255); 
                margin: 20px  ;
                color: #fff; 
                border-radius: 5px; 


                font-size: 150%; 

                box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;

            }
            img{
                margin: 30px;
            }

            #uploader{
                display: inline-block;
                width: 300px;
                height: 200px;
                background-image: url("/throttle/images/www.png");
                background-repeat: no-repeat ;
                margin: 2%;


            }

            input[type="file"] {
                display: none;
            }
            .custom-file-upload {
                display: inline-block;
                width: 300px; 
                height: 200px;
                background-image: url("/throttle/images/www.png");
                background-repeat: no-repeat ;
                margin: 2%;
            }
            button{
                display: inline-block;

                text-align: center;
                width: 10%;


            }
            .temp{
                background: linear-gradient(to right,#0456a8 ,#0456a8,#0456a8);

                color: aliceblue;
                /* border-radius: 13px; 
                border-radius: 10px ; */


            }


            .table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
                margin-top: 40px;
                /* background: linear-gradient(to right,#768681,#f7f3f4); */
                border-radius: 10px ;
                color: white;




            }

            .td, .th {

                /* text-align: left; */
                padding: 8px;
                text-align: center;




            }

            .tr:nth-child(odd) {
                background-color: rgb(79, 151, 245);

            }    

            .tr:nth-child(even) {

                background-color: #96c5fc;
            }    




            .kk{
                width: 100px;
                display: inline-block;
                padding: 1px 2px;
                margin-left: 100%;
                margin-top: 10%;
                background-color: #0c70d4;
                color: white;
                border: none;
                border-radius: 5px;
            }



            .mango{
                margin-left: 20%;
            }
            .cont{
                background-color: #0c70d4;
                text-align: center;
                color: white;
                border: none;
                border-radius: 5px;
                margin-left: 45%;
                margin-top: 20px;

            }
            .upt{
                width: 100px;
                padding: 2px 3px;
                text-align: center;
                border-radius: 5px;

            }
            .upt:hover{
                background-color: blue;
                color: white;
            }
        </style>
        <title>HS</title>
    </head>

    <body onload="openCity(event, 'Entry')">
        <div class="main">

            <div class="tab">
                <button class="tablinks" onclick="openCity(event, 'Entry')">Add Employee</button>
                <button class="tablinks" onclick="openCity(event, 'Upload')">Update DOL</button>
                <button class="tablinks" onclick="openCity(event, 'View')">Employee Details</button>
                <button class="tablinks" onclick="dash()">Dashboard</button>
            </div>

            <div id="Entry" class="tabcontent">
                <form name="upload"  method="post" enctype="multipart/form-data">

                    <c:if test="${msg ==1}">
                        <h1 style='color:green;text-align: center'>Uploaded Successfully</h1>
                    </c:if>
                    <c:if test="${msg ==2}">
                        <h1 style='color:red;text-align: center'>Failed Successfully</h1>
                    </c:if>

                    <div class="box">

                        <table class="mango">
                            <tr>
                                <td> <label class="custom-file-upload">
                                        <input type="file" name="importCnote" id="importCnote"  onchange="showUpload(this.value)" />

                                    </label></td>
                            <input readonly style="border:none;" id="shower">

                            <td width="200px"></td>
                            <td>
                                <input class="kk" readonly value= ' Update' onclick="uploadPage();"/>


                            </td>
                            </tr>
                        </table>




                    </div>
                    <c:if test = "${employeeHireDetailsTemps > 0}" >
                        <div>
                            <table class="table">

                                <tr class="temp tr">
                                    <th class="th">Sno</th>
                                    <th class="th">Employee Name</th>
                                    <th class="th">Emp ID</th>
                                    <th class="th">CTC</th>
                                    <th class="th">Designation</th>
                                    <th class="th">client</th>
                                    <th class="th">Location</th>
                                    <th class="th">DOJ</th>
                                    <th class="th">DOL</th>
                                    <th class="th">Type</th>
                                    <th class="th">Category</th>
                                    <th class="th">Status</th>
                                </tr>

                                <% int index = 0;
                                    int sno = 1;
                                    int count = 0;
                                %> 

                                <c:forEach items="${employeeHireDetailsTemp}" var="pc">

                                    <tr class="tr">
                                        <td class="td"><%=sno%></td>
                                        <td class="td"><c:out value="${pc.name}"/></td>
                                        <td class="td"><c:out value="${pc.id}"/></td>
                                        <td class="td"><c:out value="${pc.ctc}"/></td>
                                        <td class="td"><c:out value="${pc.designation}"/></td>
                                        <td class="td"><c:out value="${pc.client}"/></td>
                                        <td class="td"><c:out value="${pc.location}"/></td>
                                        <td class="td"><c:out value="${pc.date}"/></td>
                                        <td class="td"><c:out value="${pc.leavingDate}"/></td>
                                        <td class="td"><c:out value="${pc.type}"/></td>
                                        <td class="td"><c:out value="${pc.category}"/></td>
                                        <c:if test="${pc.status ==1}">
                                            <td class="td"style="background:red">Employee Id Already Exists</td>
                                        </c:if>
                                        <c:if test="${pc.status ==0}">
                                            <td class="td" style="background:green"> Ready To Submit </td>
                                        </c:if>

                                    </tr>

                                    <%
                                        sno++;
                                        index++;
                                    %>
                                </c:forEach>
                            </table>
                        </div>
                        <div>
                            <button class="cont" name="submitPage" id="submitPage" onclick="insertTo();">
                                <h3>Submit</h3>
                            </button>
                        </div>
                    </c:if>  


                </form>


            </div>

            <div id="Upload" class="tabcontent">
                <form action="">

                    <div class="box">



                    </div>


                    <div>
                        <table class="table">

                            <tr class="temp tr">
                                <th class="th">Employee Name</th>
                                <th class="th">Emp ID</th>
                                <th class="th">CTC</th>
                                <th class="th">Designation</th>
                                <th class="th">client</th>
                                <th class="th">Location</th>
                                <th class="th">DOJ</th>
                                <th class="th">DOL</th>
                                <th class="th">Type</th>
                                <th class="th">Category</th>
                                <th class="th">Update</th>
                            </tr>

                            <tr class="tr">
                                <td class="td">shakshi</td>
                                <td class="td">hs12</td>
                                <td class="td">20000</td>
                                <td class="td">superviser</td>
                                <td class="td">null</td>
                                <td class="td">chennai</td>
                                <td class="td">12-03-2021</td>
                                <td class="td"></td>
                                <td class="td">full time</td>
                                <td class="td"></td>
                                <td class="td"><input class="upt" value= 'Update' onclick="uploadPage();"/></td>
                            </tr>

                            <tr class="tr">
                                <td class="td">abarna</td>
                                <td class="td">hs56</td>
                                <td class="td">30000</td>
                                <td class="td">manager</td>
                                <td class="td">null</td>
                                <td class="td">bangalore</td>
                                <td class="td">21-04-2020</td>
                                <td class="td"></td>
                                <td class="td">full time</td>
                                <td class="td"></td>
                                <td class="td"><button class="upt">Update</button></td>
                            </tr>

                            <tr class="tr">
                                <td class="td">alan</td>
                                <td class="td">hs78</td>
                                <td class="td">50000</td>
                                <td class="td">manager</td>
                                <td class="td">null</td>
                                <td class="td">chennai</td>
                                <td class="td">12-02-2018</td>
                                <td class="td"></td>
                                <td class="td">part time</td>
                                <td class="td"></td>
                                <td class="td"><button class="upt">Update</button></td>
                            </tr>

                            <tr class="tr">
                                <td class="td">sara</td>
                                <td class="td">hs34</td>
                                <td class="td">20000</td>
                                <td class="td">superviser</td>
                                <td class="td">null</td>
                                <td class="td">mumbai </td>
                                <td class="td">04-03-2020</td>
                                <td class="td"></td>
                                <td class="td">part time</td>
                                <td class="td"></td>
                                <td class="td"><button class="upt">Update</button></td>
                            </tr>

                            <tr class="tr">
                                <td class="td">akash</td>
                                <td class="td">hs90</td>
                                <td class="td">25000</td>
                                <td class="td">superviser</td>
                                <td class="td">null</td>
                                <td class="td">chennai</td>
                                <td class="td">08-02-2019</td>
                                <td class="td"></td>
                                <td class="td">full time</td>
                                <td class="td"></td>
                                <td class="td"><button class="upt">Update</button></td>
                            </tr>

                            <tr class="tr">
                                <td class="td">priya</td>
                                <td class="td">hs71</td>
                                <td class="td">50000</td>
                                <td class="td">manager</td>
                                <td class="td">null</td>
                                <td class="td">mumbai</td>
                                <td class="td">13-01-2017</td>
                                <td class="td"></td>
                                <td class="td">full time</td>
                                <td class="td"></td>
                                <td class="td"><button class="upt">Update</button></td>
                            </tr>

                            <tr class="tr">
                                <td class="td">dinesh</td>
                                <td class="td">hs11</td>
                                <td class="td">23000</td>
                                <td class="td">superviser</td>
                                <td class="td">null</td>
                                <td class="td">chennai</td>
                                <td class="td">13-01-2016</td>
                                <td class="td"></td>
                                <td class="td">part time</td>
                                <td class="td"></td>
                                <td class="td"><button class="upt">Update</button></td>
                            </tr>

                            <tr class="tr">
                                <td class="td">deepa</td>
                                <td class="td">hs89</td>
                                <td class="td">30000</td>
                                <td class="td">manager</td>
                                <td class="td">null</td>
                                <td class="td">mumbai</td>
                                <td class="td">30-1-2021</td>
                                <td class="td"></td>
                                <td class="td">full time</td>
                                <td class="td"></td>
                                <td class="td"><button class="upt">Update</button></td>
                            </tr>


                        </table>
                    </div>


                </form>


            </div>

            <div id="View" class="tabcontent">

                <form action="">

                    <div class="box">



                    </div>


                    <div>
                        <table class="table">

                            <tr class="temp tr">
                                <th class="th">Employee Name</th>
                                <th class="th">Emp ID</th>
                                <th class="th">CTC</th>
                                <th class="th">Designation</th>
                                <th class="th">client</th>
                                <th class="th">Location</th>
                                <th class="th">DOJ</th>
                                <th class="th">DOL</th>
                                <th class="th">Type</th>
                                <th class="th">Category</th>
                                <th class="th">View</th>
                            </tr>

                            <tr class="tr">
                                <td class="td">shakshi</td>
                                <td class="td">hs12</td>
                                <td class="td">20000</td>
                                <td class="td">superviser</td>
                                <td class="td">null</td>
                                <td class="td">chennai</td>
                                <td class="td">12-03-2021</td>
                                <td class="td"></td>
                                <td class="td">full time</td>
                                <td class="td"></td>
                                <td class="td"><button class="upt">View</button></td>
                            </tr>

                            <tr class="tr">
                                <td class="td">abarna</td>
                                <td class="td">hs56</td>
                                <td class="td">30000</td>
                                <td class="td">manager</td>
                                <td class="td">null</td>
                                <td class="td">bangalore</td>
                                <td class="td">21-04-2020</td>
                                <td class="td"></td>
                                <td class="td">full time</td>
                                <td class="td"></td>
                                <td class="td"><button class="upt">View</button></td>
                            </tr>

                            <tr class="tr">
                                <td class="td">alan</td>
                                <td class="td">hs78</td>
                                <td class="td">50000</td>
                                <td class="td">manager</td>
                                <td class="td">null</td>
                                <td class="td">chennai</td>
                                <td class="td">12-02-2018</td>
                                <td class="td"></td>
                                <td class="td">part time</td>
                                <td class="td"></td>
                                <td class="td"><button class="upt">View</button></td>
                            </tr>

                            <tr class="tr">
                                <td class="td">sara</td>
                                <td class="td">hs34</td>
                                <td class="td">20000</td>
                                <td class="td">superviser</td>
                                <td class="td">null</td>
                                <td class="td">mumbai </td>
                                <td class="td">04-03-2020</td>
                                <td class="td"></td>
                                <td class="td">part time</td>
                                <td class="td"></td>
                                <td class="td"><button class="upt">View</button></td>
                            </tr>

                            <tr class="tr">
                                <td class="td">akash</td>
                                <td class="td">hs90</td>
                                <td class="td">25000</td>
                                <td class="td">superviser</td>
                                <td class="td">null</td>
                                <td class="td">chennai</td>
                                <td class="td">08-02-2019</td>
                                <td class="td"></td>
                                <td class="td">full time</td>
                                <td class="td"></td>
                                <td class="td"><button class="upt">View</button></td>
                            </tr>

                            <tr class="tr">
                                <td class="td">priya</td>
                                <td class="td">hs71</td>
                                <td class="td">50000</td>
                                <td class="td">manager</td>
                                <td class="td">null</td>
                                <td class="td">mumbai</td>
                                <td class="td">13-01-2017</td>
                                <td class="td"></td>
                                <td class="td">full time</td>
                                <td class="td"></td>
                                <td class="td"><button class="upt">View</button></td>
                            </tr>

                            <tr class="tr">
                                <td class="td">dinesh</td>
                                <td class="td">hs11</td>
                                <td class="td">23000</td>
                                <td class="td">superviser</td>
                                <td class="td">null</td>
                                <td class="td">chennai</td>
                                <td class="td">13-01-2016</td>
                                <td class="td"></td>
                                <td class="td">part time</td>
                                <td class="td"></td>
                                <td class="td"><button class="upt">View</button></td>
                            </tr>

                            <tr class="tr">
                                <td class="td">deepa</td>
                                <td class="td">hs89</td>
                                <td class="td">30000</td>
                                <td class="td">manager</td>
                                <td class="td">null</td>
                                <td class="td">mumbai</td>
                                <td class="td">30-1-2021</td>
                                <td class="td"></td>
                                <td class="td">full time</td>
                                <td class="td"></td>
                                <td class="td"><button class="upt">View</button></td>
                            </tr>


                        </table>
                    </div>


                </form>
            </div>

            <script>

                function insertTo() {
                    var result = confirm("Do you want to continue?");
                    if (result == true) {
                        document.upload.action = "/throttle/employeeHiringDetails.do?param=save";
                        document.upload.method = "post";
                        document.upload.submit();
                    }
                }
                function uploadPage() {
//                    alert("sss")
                    document.upload.action = "/throttle/empHiringDetailsUpload.do?param=upload";
                    document.upload.submit();

                }

                function dash() {
                    //                       window.location("localhost:8080throttle/login.do?")
                    location.replace("/throttle/dashboardPage.do?");

                }

                function showUpload(value) {
                    document.getElementById("shower").value = value;
                }



                function openCity(evt, cityName) {
                    var i, tabcontent, tablinks;
                    tabcontent = document.getElementsByClassName("tabcontent");
                    for (i = 0; i < tabcontent.length; i++) {
                        tabcontent[i].style.display = "none";
                    }
                    tablinks = document.getElementsByClassName("tablinks");
                    for (i = 0; i < tablinks.length; i++) {
                        tablinks[i].className = tablinks[i].className.replace(" active", "");
                    }
                    document.getElementById(cityName).style.display = "block";
                    evt.currentTarget.className += " active";
                }
            </script>



        </div>

    </body>

</html>