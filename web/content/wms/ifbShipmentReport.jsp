<%-- 
    Document   : ifbShipmentReport
    Created on : 11 Jun, 2021, 12:26:20 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<!--<script language="javascript" src="/throttle/js/validate.js"></script>-->
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>



<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>
<script type="text/javascript">
    function searchPage() {
        document.ifb.action = '/throttle/ifbShipmentReport.do';
        document.ifb.submit();
    }
    function excelPage() {
        document.ifb.action = '/throttle/ifbShipmentReport.do?&param=ExportExcel';
        document.ifb.submit();
    }
    function showImage(name) {
        window.open("/throttle/uploadFiles/pod/" + name, "POD Image", "width=900, height=800");
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="IFB Shipment Report"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.IFB" text="ifb"/></a></li>
            <li class=""><spring:message code="hrms.label.ifb" text="IFB Shipment Report"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >
                <form name="ifb"  method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <div id="ptp">
                        <div class="inpad">
                            <table class="table table-info mb30 table-hover" style="width:100%">
                                <thead><tr><th colspan="7">IFB Shipment Report</th></tr></thead>
                                <tr>
                                    <td>From Date</td>
                                    <td><input type="text" class="datepicker form-control" value="<c:out value="${fromDate}"/>" autocomplete="off" name="fromDate" id="fromDate" style="width:200px;height:40px"/></td>
                                    <td>To Date</td>
                                    <td><input type="text" class="datepicker form-control" autocomplete="off" name="toDate" id="toDate" style="width:200px;height:40px" value="<c:out value="${toDate}"/>"/></td>
                                </tr>
                                <tr>
                                    <td><font color="red">*</font>Shipment Type</td>
                                    <td>
                                        <select class="form-control" id="orderType" name="orderType" style="width:200px;height: 40px" value="<c:out value="${orderType}"/>">
                                            <option value="">------Select One------</option>
                                            <option value="1">Delivery</option>
                                            <option value="2">Exchange</option>
                                            <option value="3">PickUp</option>
                                        </select>
                                    </td>
                                    <td><font color="red">*</font>Status</td>
                                    <td>
                                        <select class="form-control" id="invoiceStatus" name="invoiceStatus" style="width:200px;height: 40px">
                                            <option value="">------Select One------</option>
                                            <option value="1">Pending</option>
                                            <option value="2">Assigned</option>
                                            <option value="4">Delivered</option>
                                            <option value="3">Rejected</option>
                                        </select>
                                    </td>
                                </tr>
                                <c:if test="${roleId==1023||roleId==1016}">
                                <tr>
                                    <td><font color="red">*</font>Warehouse</td>
                                    <td height="30">
                                        <select name="whId" id="whId" class="form-control" style="width:250px;height:40px" autocomplete="off" value="<c:out value="${whId}"/>">
                                            <option value="">------Select Warehouse------</option>
                                            <c:forEach items="${whList}" var="wh">
                                                <option value="<c:out value="${wh.whId}"/>"><c:out value="${wh.whName}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                </c:if>
                            </table>
                            <script>
                                document.getElementById('orderType').value = '<c:out value="${orderType}"/>'
                                document.getElementById('fromDate').value = '<c:out value="${fromDate}"/>'
                                document.getElementById('toDate').value = '<c:out value="${toDate}"/>'
                                document.getElementById('invoiceStatus').value = '<c:out value="${invoiceStatus}"/>'
                            </script>
                            <center>
                                <input type="button" class="btn btn-success"   value="Search" onclick="searchPage()">&emsp;&emsp;
                                <input type="button" class="btn btn-success"   value="Export Excel" onclick="excelPage()">
                            </center>

                            <div id="ptp" style="overflow: auto">
                                <div class="inpad">
                                    <table class="table table-info mb30 table-hover" id="table" >

                                        <thead>
                                            <tr height="40">
                                                <th  height="30" >S.No</th>
                                                <th  height="30" >LR No</th>
                                                <th  height="30" >LR Date</th>
                                                <th  height="30" >Hub Name</th>
                                                <th  height="30" >Shipment No</th>
                                                <th  height="30" >Shipment Date</th>
                                                <th  height="30" >Shipment Type</th>
                                                <th  height="30" >Pay Type</th>
                                                <th  height="30" >Customer Name</th>
                                                <th  height="30" >City</th>
                                                <th  height="30" >Pincode</th>
                                                <th  height="30" >Amount</th>
                                                <th  height="30" >Material Code</th>
                                                <th  height="30" >Quantity</th>
                                                <th  height="30" >Shipment Status</th>
                                                <th  height="30" >POD</th>
                                            </tr>
                                        </thead>
                                        <%
                                            int index = 0;
                                            int sno = 1;
                                        %>
                                        <tbody>
                                            <c:forEach items= "${ifbShipmentReport}" var="ifb">
                                                <tr height="30">
                                                    <td align="left"><%=sno%></td>
                                                    <td align="left"><c:out value="${ifb.lrNo}"/></td>
                                                    <td align="left"><c:out value="${ifb.lrDate}"/></td>
                                                    <td align="left"><c:out value="${ifb.whName}"/></td>
                                                    <td align="left"><c:out value="${ifb.invoiceNo}"/></td>
                                                    <td align="left"><c:out value="${ifb.billDate}"/></td>
                                                    <td align="left">
                                                        <c:if test="${ifb.orderType==1}">Delivery</c:if>
                                                        <c:if test="${ifb.orderType==2}">Exchange</c:if>
                                                        <c:if test="${ifb.orderType==3}">PickUp</c:if>
                                                        </td>
                                                        <td align="left">PP</td>
                                                        <td align="left"><c:out value="${ifb.partyName}"/></td>
                                                    <td align="left"><c:out value="${ifb.city}"/></td>
                                                    <td align="left"><c:out value="${ifb.pincode}"/></td>
                                                    <td align="left"><c:out value="${ifb.grossValue}"/></td>
                                                    <td align="left"><c:out value="${ifb.matlGroup}"/></td>
                                                    <td align="left"><c:out value="${ifb.qty}"/></td>
                                                    <td align="left">
                                                        <c:if test="${ifb.invoiceStatus==1}">Pending</c:if>
                                                        <c:if test="${ifb.invoiceStatus==2}">Assigned</c:if>
                                                        <c:if test="${ifb.invoiceStatus==3}">Rejected</c:if>
                                                        <c:if test="${ifb.invoiceStatus==4}">Delivered</c:if>
                                                        <c:if test="${ifb.invoiceStatus==5}">Shipment Transfer Pending</c:if>
                                                        </td>
                                                        <td align="left"   >
                                                            <a onclick="showImage('<c:out value="${ifb.fileName}"/>')"><c:out value="${ifb.fileName}"/></a>
                                                    </td>
                                                </tr>
                                                <%
                                                    sno++;
                                                    index++;
                                                %>
                                            </c:forEach>
                                        <input type="hidden" name="sno" id="sno" value="<%=sno%>">
                                        <script language="javascript" type="text/javascript">
                                            setFilterGrid("table");
                                        </script>
                                        </tbody>
                                    </table>
                                </div>
                            </div>    

                        </div>
                    </div>
                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>