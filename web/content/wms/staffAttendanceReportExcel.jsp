<%--
    Document   : viewTripSheet
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Throttle
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
    <form name="tripSheet" method="post">
        <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "StaffAttendanceReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

            <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                <thead>
                    <tr height="70">
                       <th>S.No.</th>
                       <th>Warehouse Name</th>
                                <th>Attendance Date</th>
                                <th>Support Staff</th>
                                <th>House Keeping</th>
                                <th>Security</th>
                                <th>Delivery Executive</th>
                                <th>Date Entry Operator</th>
                                <th>Executive</th>
                                <th>Other</th>
                                <th>Total</th>
                                <th>Updated By</th>
                                <th>Updated On</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <% int index = 1;%>
                    <c:forEach items="${staffAttendanceReport}" var="wor">
                        <%
                                    String className = "text1";
                                    if ((index % 2) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                        %>
                        <tr height="30">
                            <td class="" width="40" align="left"><%=index%></td>
                             <c:set var="staff" value="${staff+wor.staff}"/>
                                         <c:set var="houseKeeping" value="${houseKeeping+wor.houseKeeping}"/>
                                         <c:set var="security" value="${security+wor.security}"/>
                                         <c:set var="deliveryExecutive" value="${deliveryExecutive+wor.deliveryExecutive}"/>
                                         <c:set var="dataEntryOperator" value="${dataEntryOperator+wor.dataEntryOperator}"/>
                                         <c:set var="executive" value="${executive+wor.executive}"/>
                                         <c:set var="others" value="${others+wor.others}"/>
                                         <c:set var="totalStaff" value="${totalStaff+wor.others+wor.executive+wor.dataEntryOperator+wor.deliveryExecutive+wor.security+wor.houseKeeping+wor.staff}"/>
                                         
                                        <td ><c:out value="${wor.whName}"/></td>
                                        <td><c:out value="${wor.attendanceDate}"/></td>
                                        <td><c:out value="${wor.staff}"/></td>
                                        <td><c:out value="${wor.houseKeeping}"/></td>
                                        <td><c:out value="${wor.security}"/></td>
                                        <td><c:out value="${wor.deliveryExecutive}"/></td>
                                        <td><c:out value="${wor.dataEntryOperator}"/></td>
                                        <td><c:out value="${wor.executive}"/></td>
                                        <td><c:out value="${wor.others}"/></td>
                                        <td><c:out value="${wor.others+wor.executive+wor.dataEntryOperator+wor.deliveryExecutive+wor.security+wor.houseKeeping+wor.staff}"/></td>
                                        <td ><c:out value="${wor.name}"/></td>
                                        <td><c:out value="${wor.createdDate}"/>: <c:out value="${wor.createdTime}"/></td>
                        </tr>

                        <%index++;%>
                    </c:forEach>
                           <tr>
                                    <td><b>Total</b></td>
                                    <td><b></b></td>
                                    <td><b></b></td>
                                    <td><b><c:out value="${staff}"/></b></td>
                                    <td><b><c:out value="${houseKeeping}"/></b></td>
                                    <td><b><c:out value="${security}"/></b></td>
                                    <td><b><c:out value="${deliveryExecutive}"/></b></td>
                                    <td><b><c:out value="${dataEntryOperator}"/></b></td>
                                    <td><b><c:out value="${executive}"/></b></td>
                                    <td><b><c:out value="${others}"/></b></td>
                                    <td><b><c:out value="${totalStaff}"/></b></td>
                                    <td><b></b></td>
                                    <td><b></b></td>
                                    <td><b></b></td>
                                   
                                    </tr>
                </tbody>
            </table>
    </form>
</body>
</html>
