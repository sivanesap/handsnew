<%-- 
    Document   : fkReconcileView
    Created on : 14 Jun, 2021, 5:12:02 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<!--<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>-->
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<style>
    .form-control:focus{border-color: #5cb85c;  box-shadow: none; -webkit-box-shadow: none;} 
    .has-error .form-control:focus{box-shadow: none; -webkit-box-shadow: none;}
</style>

<style>
    .opci{
        background:rgba(101, 237, 221,0.5);
        min-height:100px;
        max-width:180px;
        padding:20px;
        border-radius: 12px;
        font-size: 300%;
        alignment-adjust: 60%;

    }

    /*            table {
                    float:left;
                    background:yellow;
                    margin-left:10px;
                }*/
</style>

<meta http-equiv="ConConsignment Notent-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">

    function searchPage(val) {
//        alert("here");
        document.schedule.action = "/throttle/fkReconcileView.do?param=" + val;
        document.schedule.submit();
    }
</script>
<script>
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<style>
    #rcorners1 {
        border-radius: 35px;
        background: url(paper.gif);
        background-position: left top;
        background-repeat: repeat;
        padding: 5px; 
        width: 400px;
        height: 150px;
    }
</style>


</head>

<body>
    <form name="schedule" method="post" >
        <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Flipkart Reconcile View" text="Flipkart Reconcile View"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operation" text="Flipkart"/></a></li>
                    <li class=""><spring:message code="hrms.label.Flipkart RunSheet Planning" text="Flipkart Reconcile View"/></li>
                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <%@include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover" style="width:100%" >
                        <tr>
                            <td>
                                <table class="table table-info mb30 table-hover" style="width:100%" >
                                    <thead><tr><th colspan="10">View Reconcile</th></tr></thead>
                                    <tr>
                                        <td><font color="red">*</font>From Date </td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text"  class="datepicker , form-control" style="width:250px;height:40px"  value="<c:out value="${fromDate}"/>"></td>
                                        <td></td>
                                        <td><font color="red">*</font>To Date </td>
                                        <td height="30">
                                            <input name="toDate" id="toDate" type="text" class="datepicker , form-control" style="width:250px;height:40px" class="datepicker" value="<c:out value="${toDate}"/>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" height="30" align="center">
                                            <center> 
                                                <input type="button" class="btn btn-success"   value="Search" onclick="searchPage('Search');">&emsp;&emsp;
                                                <input type="button" class="btn btn-success"   value="Export Excel" onclick="searchPage('ExportExcel');">
                                            </center>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                    <br>

                    <table class="table table-info mb30 table-hover" id="table" >
                        <thead>
                            <tr height="40">
                                <th>S.No</th>                                    
                                <th>Reconcile Code</th>  
                                <th>Runsheet Id</th>
                                <th>Runsheet Date</th>                                                                                                         
                                <th>DE Name</th>                                                                                                         
                                <th>Hub Name</th>
                                <th>Total Orders</th>
                                <th>Delivered</th>
                                <th>Rejected</th>
                                <th>Ship COD</th>
                                <th>Prexo COD</th>
                                <th>DE COD</th>
                                <th>Status</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <% int index = 0;
                           int sno = 1;
                        %>
                        <tbody>
                            <c:forEach items="${fkReconcileView}" var="cnl">

                                <tr height="30">
                                    <td align="left"><%=sno%></td>
                                    <td align="left" ><c:out value="${cnl.reconcileCode}"/></td>
                                    <td align="left" ><c:out value="${cnl.tripCode}"/></td>
                                    <td align="left" ><c:out value="${cnl.date}"/></td>
                                    <td align="left" ><c:out value="${cnl.empName}"/></td>
                                    <td align="left" ><c:out value="${cnl.whName}"/></td>                            
                                    <td align="left" ><c:out value="${cnl.qty}"/></td> 
                                    <td align="left" ><c:out value="${cnl.delivered}"/></td> 
                                    <td align="left" ><c:out value="${cnl.returned}"/></td> 
                                    <td align="left" ><c:out value="${cnl.totalCod}"/></td> 
                                    <td align="left" ><c:out value="${cnl.totalPrexoAmount}"/></td> 
                                    <td align="left" ><c:out value="${cnl.totalDeCod}"/></td> 
                                    <td align="left" ><c:if test="${cnl.status==1}">Deposit Pending</c:if><c:if test="${cnl.status==2}">Deposited</c:if></td> 

                                            <td align="left" ><a href="viewFkReconcile.do?tripId=<c:out value="${cnl.tripId}"/>&statusId=14&param=view">View</a></td> 

                                </tr>
                                <%index++;%>
                                <%sno++;%>

                            </c:forEach>
                        </tbody>
                    </table>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">

                        <script>
                            function checkAll() {
                                var inputs = document.getElementsByName("selectedIndex");
                                for (var i = 0; i < inputs.length; i++) {
                                    if (inputs[i].type == "checkbox") {
                                        if (inputs[i].checked == true) {
                                            inputs[i].checked = false;
                                            document.getElementById("selectedStatus" + (i + 1)).value = 0;
                                        } else if (inputs[i].checked == false) {
                                            inputs[i].checked = true;
                                            document.getElementById("selectedStatus" + (i + 1)).value = 1;
                                        }
                                    }
                                }
                            }
                        </script>

                        </form>
                        </body>
                    </div>      

                </div>
            </div>


            <!--<script async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&callback=initMap"></script>-->
            <%@ include file="../common/NewDesign/settings.jsp" %>