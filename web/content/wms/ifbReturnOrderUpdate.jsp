
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>
<link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="Ifb Return Order Update"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="wms"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="Return Order Update"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form name="return" method="post">

                    <%@ include file="/content/common/message.jsp" %>
                    <div id="ptp">
                        <div class="inpad">
                            <table class="table table-info mb30 table-hover" style="width:100%">
                                <thead><tr><th colspan="7">Return Order Update</th></tr></thead>
                                <tr>
                                    <td  ><font color="red">*</font>Invoice No</td>
                                    <td>
                                        <input type="text" id="invoiceNo1"  class="form-control" name="invoiceNo1" style="width:200px;height:40px"/>
                                    </td>
                                </tr>
                            </table>
                            <center>
                                <input type="button" class="btn btn-success"  id="search" value="Search" onclick="searchDiv();">&emsp;
                                <input type="button" class="btn btn-success"   value="Add" onclick="addDiv();">
                            </center>
                            <div id="add1" style="display:none">
                                <table class="table table-info mb30 table-hover" id="serial">
                                    <thead><tr><th colspan="7">Add Return Order </th></tr></thead>
                                    <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                                    </div>
                                    <tr>
                                        <td align="center"><text style="color:red">*</text>
                                    Invoice No
                                    </td>
                                    <td>
                                        <input type="text" id="invoiceNo" name="invoiceNo" value="" class="form-control" style="width:200px;height:30px" onchange="checkInvoice(this.value);"/> 
                                    </td>
                                    <td align="center">Invoice Date</td>
                                    <td height="30"><input name="invoiceDate" id="invoiceDate" type="text"  class="datepicker , form-control" style="width:200px;height:30px" autocomplete="off"></td>

                                    </tr>
                                    <tr>
                                        <td align="center"><text style="color:red">*</text>Item Description</td>
                                    <td height="30"><input name="itemDescription" id="itemDescription" type="text"  class="form-control" style="width:200px;height:30px"  autocomplete="off"></td>
                                    <td align="center"><text style="color:red">*</text>Serial No</td>
                                    <td height="30"><input name="serialNo" id="serialNo" type="number" class="form-control" style="width:200px;height:30px" autocomplete="off"></td>
                                    </tr>
                                    <tr>
                                        <td align="center">Material Group</td>
                                    <td height="30"><input name="matlGroup" id="matlGroup" type="text" class="form-control" style="width:200px;height:30px" autocomplete="off"></td>
                                    <td align="center">Customer Name</td>
                                    <td height="30"><input name="purchaseNo" id="purchaseNo" type="text" class="form-control" style="width:200px;height:30px"   autocomplete="off"></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><text style="color:red">*</text>Dealer Name</td>
                                    <td height="30">
                                        <select name="dealerName1" id="dealerName1" type="text" style="width:200px;height:30px" class="select2-search" onchange="setValues(this.value)">
                                            <option val="">-----Select One-----</option>
                                            <c:forEach items="${dealerMaster}" var="dm">
                                                <option value="<c:out value="${dm.dealerId}"/>"><c:out value="${dm.dealerName}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                    <td align="center"><text style="color:red">*</text>Quantity</td>
                                    <td height="30"><input name="qty" id="qty" class="form-control" type="text" style="width:200px;height:30px"  value="1" autocomplete="off"></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><text style="color:red">*</text>Pincode</td>
                                        <td height="30"><input type="number" name="pincode" id="pincode" class="form-control" maxlength="6" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" style="width:200px;height:30px" autocomplete="off"></td>
                                        <td align="center"><text style="color:red">*</text>City</td>
                                        <td height="30"><input type="text" name="city" id="city" class="form-control" style="width:200px;height:30px" autocomplete="off"></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><text style="color:red">*</text>Gross Value</td>
                                        <td height="30"><input name="grossValue" id="grossValue" class="form-control" type="number" style="width:200px;height:30px" autocomplete="off"></td>
                                        <td align="center">Customer Gstin </td>
                                        <td height="30"><input type="text" name="gst" id="gst" class="form-control" style="width:200px;height:30px" autocomplete="off"></td>
                                    </tr>
                                    <input type='hidden' name='dealerName' id='dealerName' value="">
                                    <input type='hidden' name='dealerCode' id='dealerCode' value="">
                                </table> 
                                <center>
                                    <input type="button" class="btn btn-success" value="Submit" id="submit1" onclick="submitPage()">
                                </center>
                            </div>
                            <script>
                                $('#dealerName1').select2({ placeholder : 'Fill Customer Name' });
                                $('#dealerName1').val('');
                            </script>
                            <script>
                                function setValues(val){
                                    var x=val.split('~');
                                    $('#dealerName').val(x[0]);
                                    $('#dealerCode').val(x[1]);
                                    $('#city').val(x[2]);
                                    $('#pincode').val(x[3]);
                                    $('#gst').val(x[4]);
                                    $('#dealerName').prop('disabled', true);
                                    $('#dealerCode').prop('disabled', true);
                                    $('#city').prop('disabled', true);
                                    $('#pincode').prop('disabled', true);
                                    $('#gst').prop('disabled', true);
                                }
                            </script>
                            <div id="ptp">
                                <div class="inpad">
                                    <c:if test = "${ifbReturnOrders != null}" >
                                        <table class="table table-info mb30 table-hover" id="table" >

                                            <thead>
                                                <tr height="40">
                                                    <th  height="30" >S.No</th>
                                                    <th  height="30" >Invoice</th>
                                                    <th  height="30" >Item Name</th>
                                                    <th  height="30" >Serial No</th>
                                                    <th  height="30" >Customer Name</th>
                                                    <th  height="30" >City</th>
                                                    <th  height="30" >Pincode</th>
                                                    <th  height="30" >Quantity</th>
                                                    <th  height="30" >Amount</th>
                                                    <th  height="30" >Action</th>
                                                </tr>
                                            </thead>
                                            <% int index = 0;
                                                int sno = 1;
                                            %>
                                            <tbody>

                                                <c:forEach items= "${ifbReturnOrders}" var="ifb">
                                                    <tr height="30">
                                                        <td align="left"><%=sno%></td>
                                                        <td align="left"   ><c:out value="${ifb.invoiceNo}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.itemName}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.serialNo}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.customerName}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.city}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.pincode}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.qty}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.grossValue}"/></td>
                                                        <td><input type="checkbox" id="selected<%=sno%>" name="selected" value="<c:out value="${ifb.orderId}"/>" onclick="checkSelectStatus('<%=sno%>', this)"></td>
                                                <input type="hidden" name="index" id="index<%=sno%>" value="0" /> 
                                                <input type="hidden" name="orderId" id="orderId<%=sno%>" value="<c:out value="${pc.orderId}" />" />
                                                </tr>
                                                <%
                                                    sno++;
                                                    index++;
                                                %>
                                            </c:forEach>
                                            <input type="hidden" name="sno" id="sno" value="<%=sno%>">
                                            </tbody>
                                        </table>
                                        <center>
                                            <input type="button" class="btn btn-success" value="Submit" id="update" onclick="updatePage()">
                                        </center>
                                    </c:if>

                                </div>
                            </div>    
                            <script type="text/javascript">
                                function searchDiv() {
                                    document.return.action = '/throttle/ifbReturnOrderUpdate.do?param=search';
                                    document.return.submit();
                                }
                                function updatePage() {
                                    document.return.action = '/throttle/ifbReturnOrderUpdate.do?param=update';
                                    document.return.submit();
                                }
                                function submitPage() {
                                    $('#dealerName').prop('disabled', false);
                                    $('#dealerCode').prop('disabled', false);
                                    $('#city').prop('disabled', false);
                                    $('#pincode').prop('disabled', false);
                                    $('#gst').prop('disabled', false);
                                    $('#update').hide();
                                    document.return.action = '/throttle/ifbReturnOrderUpdate.do?param=save';
                                    document.return.submit();
                                }
                            </script>
                            <script>
                                function addDiv() {
                                    $("#add1").show();
                                }
                            </script>
                        </div>
                    </div>
                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>