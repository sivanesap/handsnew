<%-- 
    Document   : viewConnectSerialDetails
    Created on : 24 Aug, 2021, 2:24:13 PM
    Author     : Roger
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script>
    function showSerialNumbers(stockId) {
        document.connect.action = "/throttle/viewConnectStockDetails.do?param=view&stockId=" + stockId;
        document.connect.submit();
    }
</script>
</head>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.View Order List" text="View Stock Details"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Connect Inbound" text="Connect Inbound"/></a></li>
            <li class=""><spring:message code="hrms.label.View Stock Details" text="View Stock Details"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="connect" method="post">

                    <table align="center" border="0" id="table5" class="table table-info mb30 table-bordered" style="width:100%" >
                        <thead>
                        <th>S. No</th>
                        <th>Serial No</th>
                        <th>Product Name</th>
                        <th>Storage Name</th>
                        <th>Rack Name</th>
                        <th>Bin Name</th>
                        <th>Qty</th>
                        <th>UOM</th>
                        <th>Product Condition</th>
                        </thead>
                         <% int index = 0;
                                    int sno = 1;
                        %>
                        <tbody id="table6">
                          <c:forEach items="${serialNumberDetails}" var="ser">
                            <tr>    
                                <td><%=sno%></td>
                                <td><c:out value="${ser.serialNo}"/></td>
                                <td><c:out value="${ser.productName}"/></td>
                                <td><c:out value="${ser.storageName}"/></td>
                                <td><c:out value="${ser.rackName}"/></td>
                                <td><c:out value="${ser.binName}"/></td>
                                <td><c:out value="${ser.qty}"/></td>
                                <td>
                                    <c:if test="${ser.uoms==1}">Pack</c:if>
                                    <c:if test="${ser.uoms==2}">Piece</c:if>
                                </td>
                                <td>
                                    <c:if test="${ser.proCons==1}">Good</c:if>
                                    <c:if test="${ser.proCons==2}">Damaged</c:if>
                                    <c:if test="${ser.proCons==3}">Excess</c:if>
                                </td>
                            </tr>
                            <%sno++;%>
                          </c:forEach>
                        </tbody>
                    </table>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
