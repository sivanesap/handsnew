<%-- 
    Document   : connectPickListSerial
    Created on : 5 Aug, 2021, 1:59:34 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<style type="text/css">

</style>
<script type="text/javascript">
    function uploadContract() {
        document.upload.action = "/throttle/uploadconnectInvoice.do";
        document.upload.method = "post";
        document.upload.submit();
    }

    function uploadPage() {
        if (document.getElementById("countStatus").value > 0) {
            var result = confirm("Some errors are not Updated. Do you want to continue?");
            if (result == true) {
                document.upload.action = "/throttle/connectUploadInvoice.do?&param=save";
                document.upload.method = "post";
                document.upload.submit();
            }
        } else {
            document.upload.action = "/throttle/connectUploadInvoice.do?&param=save";
            document.upload.method = "post";
            document.upload.submit();
        }
    }

    var arr = [];
    var arr2 = [];
    var arr3 = [];

    function add(){
        
         document.upload.action = "/throttle/connectPicklist.do?param=save";
            document.upload.method = "post";
            document.upload.submit();
    }


</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="PickList Serial"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Connect" text="HS Connect"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="PickList Serial"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form name="upload"  method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>



                    <div id="ptp">
                        <div class="inpad" style=" overflow-x: visible;">
                            <table class="table table-info mb30 table-info" >
                                <tr>
                                    <td style="text-align:center">Add Serial no</td>
                                    <td><input type="text" id="scanSerial" name="scanSerial" style="width:200px;height:40px" class="form-control"></td>
                                </tr>
                            </table>

                            <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">
                                <thead style="font-size:12px">
                                    <tr colspan="5">
                                        <th colspan="5">Auto Picked</th>
                                    </tr>
                                    <tr height="40">
                                        <th  height="30" >S No</th>
                                        <th  height="30" >Serial No</th>

                                        <th  height="30" >Product Name</th>
                                        <th height="30" >Select</th>
                                    </tr>
                                </thead>
                                <input type="hidden" name="dispatchId" id="dispatchId" value="<c:out value="${dispatchId}"/>"/>
                                <c:set var="qty" value="${qty}"/>
                                <% int index = 0; 
                                 int sno = 1;
                                 int count=0;
                                 int qty = Integer.parseInt((String) pageContext.getAttribute("qty"));
                                %> 
                                <tbody id="addSerial">
                                    <c:forEach items= "${serialList}" var="pick">
                                        <%if(count<qty){%>
                                        <tr height="30">
                                            <td align="left"><%=sno%></td>

                                            <td align="left"   ><c:out value="${pick.serialNumber}"/></td>
                                            <td align="left"   ><c:out value="${pick.productName}"/></td>
                                            <td>
                                                <input type="hidden" name="serialNo" id="serialNo<%=sno%>" value="<c:out value="${pick.serialNumber}"/>">
                                                <input type="hidden" name="stockId" id="stockId<%=sno%>" value="<c:out value="${pick.stockId}"/>">
                                                <input type="hidden" name="productId" id="productId<%=sno%>" value="<c:out value="${pick.productId}"/>">
                                                <input type="hidden" name="productName" id="productName<%=sno%>" value="<c:out value="${pick.productName}"/>">
                                                <input type="checkbox" id="selectedIndex<%=sno%>" name="selectedIndex" checked />
                                                <input type="hidden" id="selectedStatus<%=sno%>" name="selectedStatus" value="1"/>
                                            </td>

                                        </tr> 
                                        <%
                                        sno++;
                                        index++;
                                        %>
                                        <%
                                        count++;
                                            }else{
                                        %>
                                    <script>
                                        arr.push('<c:out value="${pick.serialNo}"/>');
                                        arr2.push('<c:out value="${pick.dispatchNo}"/>');
                                        arr3.push('<c:out value="${pick.productName}"/>');
                                    </script>
                                    <%}%>
                                </c:forEach>
                                <input type="hidden" name="countStatus" id="countStatus" value="<c:out value="${qty}"/>"/>
                                </tbody>
                            </table>

                            <center>

                                <td colspan="0"><input type="button" class="btn btn-success" value="Submit" name="submitPage" id="submitPage" onclick="add();"></td>

                            </center>
                            <c:if test="${serialSize>0}">
                                <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">
                                    <thead style="font-size:12px">
                                        <tr colspan="5">
                                            <th colspan="5">Picked Items</th>
                                        </tr>
                                        <tr height="40">
                                            <th  height="30" >S No</th>
                                            <th  height="30" >Serial No</th>
                                            <th  height="30" >Dispatch No</th>
                                            <th  height="30" >Product Name</th>
                                            <th height="30" >Select</th>
                                        </tr>
                                    </thead>

                                   

                                    <tbody>
                                        <c:forEach items= "${serialList}" var="pick">
                                            <%if(count<qty){%>
                                            <tr height="30">
                                                <td align="left"><%=sno%></td>

                                                
                                                <td align="left"   ><c:out value="${pick.serialNumber}"/></td>
<!--                                                <td align="left"   ><c:out value="${pick.dispatchNo}"/></td>-->
                                                <td align="left"   ><c:out value="${pick.productName}"/></td>
                                                <td>
                                                    <input type="text" name="serialNo" id="serialNo<%=sno%>" value="<c:out value="${pick.serialNumber}"/>">
<!--                                                    <input type="hidden" name="dispatchNo" id="dispatchNo<%=sno%>" value="<c:out value="${pick.dispatchNo}"/>">-->

                                                    <input type="hidden" name="stockId" id="stockId<%=sno%>" value="<c:out value="${pick.stockId}"/>">
                                                    <input type="hidden" name="productId" id="productId<%=sno%>" value="<c:out value="${pick.productId}"/>">
                                                    <input type="hidden" name="productName" id="productName<%=sno%>" value="<c:out value="${pick.productName}"/>">
                                                    <input type="checkbox" id="selectedIndex<%=sno%>" name="selectedIndex" checked />
                                                    <input type="hidden" id="selectedStatus<%=sno%>" name="selectedStatus" value="1"/>
                                                </td>


                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                            <%
                                            count++;
                                                }else{
                                            %>
                                        <script>
                                            arr.push('<c:out value="${pick.serialNo}"/>');
                                            arr2.push('<c:out value="${pick.dispatchNo}"/>');
                                            arr3.push('<c:out value="${pick.productName}"/>');

                                        </script>
                                        <%}%>
                                    </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<c:out value="${qty}"/>"/>
                                    </tbody>
                                </table>
                            </c:if>

                        </div>
                    </div>     

                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>

