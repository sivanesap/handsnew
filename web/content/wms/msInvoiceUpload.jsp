<%-- 
    Document   : meeshoInvoiceUpload
    Created on : 7 Dec, 2021, 3:09:40 PM
    Author     : alan
--%>




<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript"></script>

    <script>
        function insertInstaMart() {
            var skuCode = document.getElementById("skuCode").value;
            var productName = document.getElementById("productName").value;
            var productDescription = document.getElementById("productDescription").value;
            var minimumStockQty = document.getElementById("minimumStockQty").value;
            var uom = document.getElementById("uom").value;
            var temperature = document.getElementById("temperature").value;
            var storage = document.getElementById("storage").value;
            var activeInd = document.getElementById("activeInd").value;
            if (activeInd != "0") {

                console.log(skuCode, productName, productDescription, minimumStockQty, uom, temperature, storage, activeInd);
                if (skuCode != "" && productName != "" && productDescription != "" && minimumStockQty != "" && uom != "" && temperature != "" && storage != "" && activeInd != "")
                {
                    document.instaMart.action = "/throttle/instaMartProductMaster.do?param=save";
                    document.instaMart.submit();
                }

                else {
                    alert("Enter The Value");
                }
            } else {
                alert("Please Select Active Not Active ");
            }
        }

    function uploadPage() {
            document.upload.action = "/throttle/imProductMasterExcelUpload.do?param=upload";
            document.upload.submit();
    
    }

        function updateInstaMart(sno, productName, skuCode, productDescription, minimumStockQty, uom, temperature, storage, activeInd, productId) {

            document.getElementById("productName").value = productName;
            document.getElementById("skuCode").value = skuCode;
            document.getElementById("productDescription").value = productDescription;
            document.getElementById("minimumStockQty").value = minimumStockQty;
            document.getElementById("uom").value = uom;
            document.getElementById("temperature").value = temperature;
            document.getElementById("storage").value = storage;
            document.getElementById("activeInd").value = activeInd;
            document.getElementById("productId").value = productId;


            var count = parseInt(document.getElementById("count").value);
            for (i = 1; i <= parseInt(count); i++) {
                if (i != sno) {
                    document.getElementById("edit" + i).checked = false;
                } else {
                    document.getElementById("edit" + i).checked = true;
                }
            }
        }
        
        
 function insertTo(){
    
            
            var result = confirm("Some errors are not Updated. Do you want to continue?");
            if (result == true) {
                document.upload.action = "/throttle/imProductMasterToMainTable.do?";
                document.upload.method = "post";
                document.upload.submit();
            }
            else {
            
            document.upload.action = "/throttle/imProductMasterToMainTable.do?";
            document.upload.method = "post";
            document.upload.submit();
        }
    }
       
        
        

    </script>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Invoice Upload</h2>

        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Meesho</a></li>
                <li class="">Invoice Upload</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <form name="upload"  method="post" enctype="multipart/form-data">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    <table class="table table-info mb30 table-hover" id="uploadTable"  >
                        <thead> 
                            <tr>
                                <th  colspan="10">Invoice Upload</th>
                            </tr>

                        </thead>


                        <tr>
                            <td style="width: 200px;">Select Excel</td>
                            <td style="width: 200px;"><input type="file" name="importCnote" id="importCnote"  class="importCnote"><font size="1"><u><a href="uploadedxls/ifbFormat.xls" download>sample file</a></u></font></td>                             
                            <td><input type="button" class="btn btn-success" value="Upload" name="UploadPage" id="UploadPage" onclick="uploadPage();"></td>
                        </tr>

                    </table>
                    <div id="ptp">
                        <div class="inpad" style=" overflow-x: visible;">

                            <c:if test = "${getImProductMasterTemp != null}" >

                                <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                    <thead style="font-size:12px">
                                        <tr height="40">
                                            <th>Sno</th>
                                            <th>Invoice No</th>
                                            <th>Order Date</th>
                                            <th>Estimated Date</th>
                                            <th>Customer Order No</th>
                                            <th>Customer Order Id</th>
                                            <th>Customer_Suborder Id</th>
                                            <th>Product Id</th>
                                            <th>Product Name</th>
                                            <th>Quantity</th>
                                            <th>Category</th>
                                            <th>SKU</th>
                                            <th>Weight</th>
                                            <th>Dealer Id</th>
                                            <th>Dealer Name</th>
                                            <th>Dealer Address</th>
                                            <th>Dealer Address2</th>
                                            <th>Dealer City</th>
                                            <th>Dealer State</th>
                                            <th>Dealer Pincode</th>
                                            <th>Dealer Phone</th>
                                            <th>Dealer Phone2</th>
                                            <th>Customer Name</th>
                                            <th>Customer Phone</th>
                                            <th>Dealer Payment Mode</th>
                                            <th>Cod Amount</th>
                                            <th>Product Price</th>
                                            <th>Discount</th>
                                            <th>Dealer Latitude</th>
                                            <th>Dealer Longitude</th>
                                            <th>Listing Facility Id</th>
                                            <th>Listing Facility Type</th>
                                            <th>Listing Facility Name</th>
                                            <th>Dealer CW Id</th>
                                            <th>Dealer CW Name</th>
                                            <th>Champ Fdc Id</th>
                                            <th>Champ Fdc Name</th>
                                            <th>Dealer Token</th>
                                            <th>Status</th>
                                         
                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                     int count=0;
                                    %> 
                                    <tbody>

                                        <c:forEach items="${getImProductMasterTemp}" var="pc">


                                            <tr>
                                                <td align="left"> <%=sno%> </td>
                                                <td align="left"> <c:out value="${pc.productName}"/></td>
                                                <td align="left"> <c:out value="${pc.skuCode}"/> </td>
                                                <td align="left"> <c:out value="${pc.productDescription}"/></td>
                                                <td align="left"> <c:out value="${pc.minimumStockQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.uom2}"/> </td>
                                                <td align="left"> <c:out value="${pc.temperature}"/> </td>
                                                <td align="left"> <c:out value="${pc.storage}"/> </td>
                                                <td align="left">
                                                    <c:if test="${pc.activeInd=='Y'}">
                                                        Active
                                                    </c:if>
                                                    <c:if test="${pc.activeInd=='N'}">
                                                        In-Active
                                                    </c:if>
                                                </td>



                                                <c:if test="${pc.status==0}">
                                                    <td align="left"><font color="green">Ready To Submit</font></td>
                                                        </c:if>
                                                        <c:if test="${pc.status==1}">
                                                    <td align="left"><font color="red">Dealer Code Already Exits Temp</font></td>
                                                            <%count++;%>
                                                        </c:if>
                                                    <c:if test="${pc.status==2}">
                                                    <td align="left"><font color="red">Dealer Code Already Exits Main Table</font></td>
                                                            <%count++;%>
                                                        </c:if>


                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                    </tbody>
                                </table>

                                <center>

                                    <td colspan="0"><input type="button" class="btn btn-success" value="Submit" name="submitPage" id="submitPage" onclick="insertTo();"></td>
                                </center>
                            </c:if>
                        </div>
                    </div>     

                </form>






                
                <br>


                      </div>
        </div>
    </div>
    <%@include file="/content/common/NewDesign/settings.jsp"%>
</html>