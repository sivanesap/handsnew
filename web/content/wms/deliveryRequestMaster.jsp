<%-- 
    Document   : deliveryRequestMaster
    Created on : Dec 11, 2020, 5:36:22 PM
    Author     : ADMIN
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%> 

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>


<script type="text/javascript">

    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Delivery Request"  text="Delivery Request"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.OUTBOUND"  text="OUTBOUND"/></a></li>
            <li class="active">Delivery Request</li>
        </ol>
    </div>
</div>

<script>
    function searchPage() {
        document.request.action = "/throttle/viewDeliveryRequest.do?";
        document.request.submit();
    }
</script>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="sorter.size(5);">
                <form name="request" method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <table class="table table-info mb30 table-hover" id="report" >
                        <thead>
                            <tr>
                                <th colspan="4" height="30" >Delivery Request</th>
                            </tr>
                        </thead>
                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:250px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>
                        </tr>
                        <tr>             
                            <td></td>
                            <td></td>
                            <td  colspan="2"><input  class="btn btn-success"   value="Search" onclick="searchPage()"></td>
                        </tr>
                    </table>
                    <c:if test="${deliveryRequestList == null }" >

                        <center><font color="red" size="2"><spring:message code="trucks.label.NoRecordsFound"  text="default text"/>  </font></center>
                        </c:if>
                    <table class="table table-info mb30 table-bordered" id="table" >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>LoanProposalId</th>
                                <th>Customer</th>
                                <th>Product</th>
                                <th>Model</th>
                                <th>Order Date</th>
                                <th>Branch</th>
                                <th>Created On</th>
                                <th>View</th>
                            </tr>

                        </thead>
                        <c:if test="${deliveryRequestList != null}">
                            <% int index = 0;
                                int sno = 1;
                            %>
                            <tbody>

                                <c:forEach items="${deliveryRequestList}" var="asn">

                                    <%
                                        String className = "text1";
                                        if ((index % 2) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>
                                    <tr >
                                        <td>
                                            <%=sno%>
                                        </td>
                                        <td><c:out value="${asn.loanProposalId}"/></td>
                                        <td><c:out value="${asn.custName}"/></td>
                                        <td><c:out value="${asn.productID}"/></td>
                                        <td><c:out value="${asn.model}"/></td>
                                        <td><c:out value="${asn.batchDate}"/></td>
                                        <td><c:out value="${asn.branchName}"/></td>
                                        <td><c:out value="${asn.createdDate}"/></td>
                                        <td>
                                            <input type="button" class="btn btn-success" name="delRequestId" id="delRequestId" value="View" onclick="generateRequest('<c:out value="${asn.delRequestId}"/>')" />
                                        </td>
                                    </tr>

                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>     
                            </tbody>

                        </c:if>


                    </table>

                    <script>
                        function generateRequest(delRequestId) {
                            var param = "0";
                            document.request.action = "/throttle/getDeliveryRequestUpdate.do?orderId=" + delRequestId + "&param=" + param;
                            document.request.submit();
                        }
                    </script>
                    <div>
                        <center>

                            <!--<input type="button" class="btn btn-success" name="Update" value="Update" onclick="searchPage(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;-->
                        </center>
                    </div>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="loader"></div>

                    <div id="controls"  >
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5"  selected="selected" >5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>

                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>


                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>


