<%-- 
    Document   : imPodApprovalView
    Created on : 27 Jan, 2022, 1:00:11 PM
    Author     : alan
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>


<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script type="text/javascript">
    function submitPage(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {

            document.podUpload.action = '/throttle/imPodApproval.do?param=Search';
            document.podUpload.submit();
        }
    }
    function checkSelectStatus(sno, obj) {
        var val = document.getElementsByName("selectedStatus");
        if (obj.checked == true) {
            document.getElementById("selectedStatus" + sno).value = 1;
        } else if (obj.checked == false) {
            document.getElementById("selectedStatus" + sno).value = 0;
        }

    }
</script>
<script>
    function showImage(podLink) {
        var link = document.getElementById("podLink" + podLink).value;
        open("uploadFiles/POD/" + link, "PopUpPage", "width=1000,height=1000");
    }
    function approvePod(val1, val2) {
        document.podUpload.action = '/throttle/imPodApproval.do?param=update&podId=' + val1 + '&status=' + val2;
        document.podUpload.submit();
    }

</script>



<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>POD Approval</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="general-forms.html">Instamart</a></li>
            <li class="active">POD Approval</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="podUpload" method="post" >

                    <table class="table table-info mb30 table-hover" >
                        <tr>

                            <td align="center"><font color="red">*</font>Plant</td>
                            <td height="30"> <select name="whId" id="whId" class="form-control" style="width:260px;height:40px;" >
                                    <c:if test="${whList != null}">
                                        <option value="0" selected>----All Warehouse----</option>
                                        <c:forEach items="${whList}" var="wh">
                                            <option value='<c:out value="${wh.whId}"/>'><c:out value="${wh.whName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select></td>
                        <script>
                            document.getElementById("whId").value = '<c:out value="${whId}"/>';
                        </script>
                          <td align="center"><font color="red">*</font>Status</td>
                       <td height="30"> <select name="podstatus" id="podstatus" class="form-control" style="width:260px;height:40px;" >
                               <option value="" selected>----Select----</option>
                               <option value="1" >Approval Pending</option>
                               <option value="2" >Completed</option>
                               <!--<option value="3" >Rejected</option>-->
                           </select></td> 
                           <script>
                            document.getElementById("podstatus").value = '<c:out value="${podstatus}"/>';
                        </script>
                        
                        </tr>
                        <tr>
                            <td align="center"><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                            <td height="30"><input name="fromDate" id="fromDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                            <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                            <td height="30"><input name="toDate" id="toDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                        </tr>
                    </table>
                    <center>

                        <input type="button" class="btn btn-success" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);">&nbsp;&nbsp;

                    </center>
                    <br>
                    <br>

                    <table class="table table-info mb30 table-bordered" id="table" class="sortable" width="100%">
                        <thead>
                            <tr >
                                <th>SNo</th>
                                <th style="width:200px;height:30px;">LRN No</th>
                                <th>Date</th>
                                <th>To Location</th>
                                <th>Consignee Name</th>
                                <th>Invoice No</th>
                                <th>Pre Tax Value</th>
                                <th>Delivery Date</th>
                                <th>View POD</th>
                                <th>Action</th>
                            </tr>

                        </thead>
                        <tbody>
                            <% int index = 1;%>

                            <c:forEach items="${getPodList}" var="dc">
                                <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                                %>



                                <tr >
                                    <td class="<%=classText%>"  align="center"><%=index%></td>


                                    <td  style="width:200px;height:30px;"><input type="hidden" id="subLr<%=index%>" name="subLr" value="<c:out value="${dc.lrnNo}"/> " style="width:100px;height:30px;">
                                        <input type="hidden" id="subLrId<%=index%>" name="subLrId" value="<c:out value="${dc.subLrId}"/> " style="width:100px;height:30px;">
                                        <input type="hidden" id="dispatchid<%=index%>" name="dispatchid" value="<c:out value="${dc.dispatchid}"/> " style="width:100px;height:30px;">
                                        <input type="hidden" id="lrnNo<%=index%>" name="lrnNo" value="<c:out value="${dc.lrnNo}"/> " style="width:100px;height:30px;">
                                        <input type="hidden" id="dispatchDetailId<%=index%>" name="dispatchDetailId" value="<c:out value="${dc.dispatchDetailId}"/> " style="width:100px;height:30px;">
                                        <input type="hidden" id="CustomerName<%=index%>" name="CustomerName" value="<c:out value="${dc.custName}"/> " style="width:100px;height:30px;">
                                        <c:out value="${dc.lrnNo}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.lrDate}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.city}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.consigneeName}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.invoiceNo}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.preTaxValue}"/></td>
                                    <td class="<%=classText%>"  align="center"><c:out value="${dc.deliveryDate}"/></td>
                                    <td style="width:70px">
                                        <c:if test='${dc.podLink1!=null&&dc.podLink1!=""}'><span class="label label-success" style="cursor:pointer" onclick="showImage('1<%=index%>')">
                                                <input type='hidden' name='podLink1' id='podLink1<%=index%>' value="<c:out value="${dc.podLink1}"/>"/>
                                                <c:out value="${dc.podName1}"/>
                                            </span>
                                            <br><br>
                                        </c:if>
                                        <c:if test='${dc.podLink2!=null&&dc.podLink2!=""}'><span class="label label-success" style="cursor:pointer" onclick="showImage('2<%=index%>')">
                                                <input type='hidden' name='podLink2' id='podLink2<%=index%>' value="<c:out value="${dc.podLink2}"/>"/>
                                                <c:out value="${dc.podName2}"/>
                                            </span><br><br></c:if>
                                        <c:if test='${dc.podLink3!=null&&dc.podLink3!=""}'><span class="label label-success" style="cursor:pointer" onclick="showImage('3<%=index%>')">
                                                <input type='hidden' name='podLink3' id='podLink3<%=index%>' value="<c:out value="${dc.podLink3}"/>"/>
                                                <c:out value="${dc.podName3}"/>
                                            </span></c:if>
                                        </td>
                                        <td>
                                        <c:if test="${dc.status==1}">
                                            <span class="label label-success" style="cursor: pointer;" onclick="approvePod('<c:out value="${dc.podId}"/>', 2);">
                                                Approve
                                            </span>
                                            <br>
                                            <br>
                                            <span class="label label-danger" style="cursor: pointer;" onclick="approvePod('<c:out value="${dc.podId}"/>', 3);">
                                                Reject
                                            </span>
                                        </c:if>
                                        <c:if test="${dc.status==2}">
                                            <span class="label label-Primary">Approved</span>
                                        </c:if>
                                        <c:if test="${dc.status==3}">
                                            <span class="label label-warning">Rejected</span>
                                        </c:if>
                                    </td>
                            <input type="hidden" name="selectedStatus" id="selectedStatus<%=index%>" value="0" />
                            </td>

                            <%index++;%>
                        </c:forEach>

                        </tbody>
                    </table>




                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>


                    <br/>
                    <br/>

                    <br/>
                    <br/>
                    <br/>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>