<%-- 
    Document   : inInvoiceViewPrint
    Created on : 1 Dec, 2021, 2:27:51 PM
    Author     : alan
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ page import="ets.domain.util.ThrottleConstants"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>
        <%@ page import="java.lang.Double" %>
        <%@ page language="java" session="true" import="java.sql.*" import="java.lang.*" contentType="application/pdf" %>

        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="style.css" />
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript" src="/throttle/js/code39.js"></script>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <%@page import="java.util.Iterator"%>
        <%@page import="java.util.ArrayList"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script src="/throttle/js/JsBarcode.all.min.js"></script>
    <script src="JsBarcode.all.min.js"></script>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="JsBarcode.codabar.min.js"></script>

    <title>H&S Instamart LR Print</title>
</head>
<script type="text/javascript">
    function printPage(val)
    {
        var printContents = document.getElementById(val).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
    function goBack() {
        document.enter.action = '/throttle/imInvoiceView.do';
        document.enter.submit();

    }
</script>

<%
response.setHeader("Content-Disposition","attachment;filename=sample.pdf");
%>

<body onload="calcExpenses();">
<style type="text/css">
    #barcode {font-weight: normal; font-style: normal; line-height:normal; font-size: pt}
</style>
<form name="enter" method="post">
    <div id="printDiv">
        <c:forEach items="${getImLrDetails}" var="lr">
            <table width="1000" border="1" cellpadding="10px" cellspacing="0" align="center" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; border-bottom:0px">
                <tr>
                    <td rowspan="2" colspan="2"><img src="images/HSSupplyLogo.png" style="width:70px;height:70px"></td>
                    <td rowspan="2" colspan="6"><b><center>H&S Supply Chain Services Private Limited</center>
                            <div style="text-align:center"><b><br><c:out value="${lr.whAddress}"/><br><center>Phone: <c:out value="${lr.mobile}"/></center></b></div></td>
                    <td colspan="2">Transporter Copy</td>
                </tr>
                <tr>
                    <td colspan="2">GST NO.: <c:out value="${lr.gstinNumber}"/><br>CIN NO.</td>
                </tr>
                <tr>
                    <td colspan="10"><center>INVOICE ACKOWLEDGMENT</center></td>
                </tr>
            </table>

            <table width="1000" border="1" cellpadding="10px" cellspacing="0" align="center" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; ">

                <tr>
                    <td colspan="4">LR No: <c:out value="${lr.lrNumber}"/></td>
                    <td colspan="4">Vehicle No:.<c:out value="${lr.vehicleNumber}"/></td>
                    <td rowspan="3" colspan="2"><svg id="barcodes"  style="width:250px;height:50px">
                </svg></td>
                </tr>
                <input type="hidden" id="lrNos"  value="<c:out value="${lr.lrNumber}"/>"/>
                <tr>
                    <td colspan="4">LR Date: <c:out value="${lr.lrdate}"/> <c:out value="${lr.lrtime}"/></td>
                    <td colspan="4">Vehicle Type: <c:out value="${lr.typeName}"/></td>
                </tr>
                <tr>
                    <td colspan="4">Driver: <c:out value="${lr.driverName}"/></td>
                    <td colspan="4">Contact No.: <c:out value="${lr.phoneNumber}"/></td>
                </tr>
            </table>
        </c:forEach>
        <%int sno=1;%>
        <c:forEach items="${getImSubLrDetails}" var="sublr">
            <table width="1000" border="1" cellpadding="10px" cellspacing="0" align="center" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px;border-top:0px ">

                <tr>
                    <td colspan="3" style="border-right:0px">From : <b><c:out value="${sublr.consinorCity}"/></b></td>
                    <td colspan="3" style="border-right:0px;border-left: 0px">To :<b> <c:out value="${sublr.city}"/></b></td>
                    <td colspan="4" style="text-align:right;border-left: 0px">LR No : <b><c:out value="${sublr.sublrNumber}"/></b></td>
                </tr>
                <input type="hidden" id="lrNo<%=sno%>"  value="<c:out value="${sublr.sublrNumber}"/>"/>
                <tr>
                    <td colspan="4" style="width:35%"><u>From: Name and Address of Consigner</u><br><b><c:out value="${sublr.consinorName}"/></b><br><c:out value="${sublr.consinorAddress}"/><br><c:out value="${sublr.consinorCity}"/><br><br><br>Phone No.<c:out value="${sublr.consinorPhone}"/></td>
                    <td colspan="4" style="width:35%"><u>To: Name and Address of Consignee:</u><br><b><c:out value="${sublr.dealerName}"/></b><br><c:out value="${sublr.address}"/><br><c:out value="${sublr.city}"/><br><c:out value="${sublr.pincode}"/><br><br><br>Phone No.<c:out value="${sublr.phoneNumber}"/></td>

                    <td colspan="2" rowspan="1" style="border-bottom:0px">
                <svg id="barcode<%=sno%>"  style="width:250px;height:65px;">
                </svg></td>
                </tr>
                <tr>
                    <td colspan ="4" style="border-right:0px">
                        GST No: <c:out value="${sublr.consinorGst}"/>
                    </td>
                    <td colspan="4" style="text-align:right;border-left: 0px">GST No:<c:out value="${sublr.gstinNumber}"/></td>
                    <td rowspan="4" colspan="2" style="border-top:0px; text-align: center">
                        <font style="font-size:22px;color:lightgrey">SEAL AND SIGN</font><br><br>
                        Any shortage / damage /<br>
                        other discrepancies must be <br>
                        mentioned here
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                        Invoice No: <c:out value="${sublr.invoiceNo}"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">Total Lr Crates</td>
                    <td colspan="1">Dispatch Qty</td>
                    <td colspan="2"></td>
                    <td colspan="2"></td>
                    <td colspan="2">Invoice Amount</td>
                </tr>
                <input type="hidden" id="received<%=sno%>" value="<c:out value="${sublr.packQty}"/>">
                <input type="hidden" id="boxQTy<%=sno%>" value="<c:out value="${sublr.looseQty}"/>">
                <tr>
                    <td colspan="1"><br> <input type="text" style="width:120px" reaonly id="totalbox<%=sno%>"value="<c:out value="${sublr.boxQty}"/>"</td>
                    <td colspan="1"><br><input type="text" style="width:120px" reaonly id="pendingpack<%=sno%>"value="<c:out value="${sublr.qty}"/>"/></td>
                    <td colspan="2"><br></td>
                    <td colspan="2"><br></td>
                    <td colspan="2"><br><c:out value="${sublr.invoiceAmount}"/></td>
                </tr>

            </table>
            <script>
                var num = document.getElementById("lrNo<%=sno%>").value;


                JsBarcode("#barcode<%=sno%>", num, {
                    textAlign: "top",
                    textPosition: "bottom",
                    font: "Times",
                    fontOptions: "bold",
                    fontSize: 25,
                    displayValue: num,
                    padding: 0,
                    margin: 0
                });

            </script>
            <%if(sno%3==0){%>
            <div class="pagebreak"> </div>
            <%}%>
            <%sno++;%>
        </c:forEach> 



        </td>
        <script>

            var num = document.getElementById("lrNos").value;


            JsBarcode("#barcodes", num, {
                textAlign: "top",
                textPosition: "bottom",
                font: "Times",
                fontOptions: "bold",
                fontSize: 25,
                displayValue: num,
                padding: 0,
                margin: 0
            });

        </script>

        </tr>     
    </div>
</form>
<br>
<center>
    <input type="button" class="button" name="ok" value="Print" onClick="printPage('printDiv');" > &nbsp;&nbsp;&nbsp;
    <input type="button" class="button" name="back" value="Back" onClick="goBack();" > &nbsp;
</center>
<br>
</body>
</html>
