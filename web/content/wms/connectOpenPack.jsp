<%-- 
    Document   : connectOpenPack
    Created on : 22 Aug, 2021, 8:26:49 AM
    Author     : Roger
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%> 

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>



<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Connect Open Pack"  text="Open Pack"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Connect Open Pack"  text="Connect"/></a></li>
            <li class="active">Open Pack</li>
        </ol>
    </div>
</div>
<script>
    function searchOpenPack() {
        document.gtn.action = "/throttle/connectOpenPack.do";
        document.gtn.submit();
    }
</script>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="sorter.size(5);">
                <form name="gtn" method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <table  class="table table-info mb30 table-hover"  align="left">
                        <tr style="" >
                            <td>Pack Serial No</td>
                            <td>
                                <input name="packSerialNo" id="packSerialNo" type="text" class="form-control" style="width:240px;height:40px"/>
                            </td>

                            <td colspan="4" >&nbsp;</td>
                        </tr>

                    </table>
                    <center>
                        <input type="button" class="btn btn-success" value="Search" onclick="searchOpenPack()">
                    </center>
                    <br><br>
                    <c:if test="${size >0}">
                        <table class="table table-info mb30 table-bordered" id="table" >
                            <thead >
                                <tr >
                                    <th>S.No</th>
                                    <th>Product Name</th>
                                    <th>GRN No</th>
                                    <th>Serial No</th>
                                    <th>Storage Name</th>
                                    <th>Rack Name</th>
                                    <th>Bin Name</th>
                                    <th>Pack Qty</th>
                                    <th>Product Condition</th>
                                </tr>

                            </thead>
                            <% int index = 0;
                                int sno = 1;
                            %>
                            <tbody>

                                <c:forEach items="${connectSerialNoDetails}" var="grn">

                                    <%
                                        String className = "text1";
                                        if ((index % 2) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>
                                    <tr >
                                        <td>
                                            <%=sno%>
                                        </td>
                                        <td ><c:out value="${grn.productName}"/></td>
                                        <td><c:out value="${grn.grnNo}"/></td>
                                        <td><c:out value="${grn.serialNo}"/></td>
                                        <td><c:out value="${grn.storageName}"/></td>
                                        <td><c:out value="${grn.rackName}"/></td>
                                        <td><c:out value="${grn.binName}"/></td>
                                        <td><c:out value="${grn.qty}"/>
                                            <input type="hidden" value="<c:out value="${grn.serialId}"/>" id="serialId" name="serialId">
                                            <input type="hidden" value="<c:out value="${grn.productId}"/>" id="productId" name="productId">
                                            <input type="hidden" value="<c:out value="${grn.stockId}"/>" id="stockId" name="stockId">
                                            <input type="hidden" value="<c:out value="${grn.grnId}"/>" id="grnId" name="grnId">
                                            <input type="hidden" value="<c:out value="${grn.qty}"/>" id="poQuantity" name="poQuantity">
                                        </td>
                                        <td>
                                            <c:if test="${grn.proCons=='1'}">Good</c:if>
                                            <c:if test="${grn.proCons=='2'}">Damaged</c:if>
                                            <c:if test="${grn.proCons=='3'}">Excess</c:if>
                                            </td>

                                        </tr>

                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>  
                            </tbody>
                        </table>

                        <table class="table table-info mb30 table-hover" id="table4">
                            <div align="center" style="height:20px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>

                            <td align="center">
                                Storage Location
                            </td>
                            <td>


                                <select id="storageId" name="storageId" style="width:250px;height:40px" onchange="setrack(this.value, 0);">
                                    <option value="0">----select---</option>
                                    <c:forEach items="${getStorageDetails}" var="loc">
                                        <option value="<c:out value="${loc.storageId}"/>"><c:out value="${loc.storageName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>Rack</td>
                            <td><select id="rackId" name="rackId" style="width:250px;height:40px"class="form-control" onchange="setbin(this.value, 0);"></select></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    Bin
                                </td>
                                <td>
                                    <select id="binId" name="binId" style="width:250px;height:40px"> </select>
                                </td>
                                <td align="center">
                                    Qty
                                </td>
                                <td>
                                    <input id="quantity" name="quantity" type="text" value="1" style="width:250px;height:40px"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Product Condition
                                </td>
                                <td><select id="proCon" name="proCon" style="width:250px;height:40px">
                                        <option value="Good">Good</option>
                                        <option value="Damaged">Damaged</option>
                                        <option value="Excess">Excess</option>
                                    </select></td>

                                <td>
                                    UOM
                                </td>
                                <td><select id="Uom" name="Uom" style="width:250px;height:40px" onchange="changePieceQty(this.value)">

                                        <option value="Pack">Pack</option>
                                        <option selected="" value="Piece">Piece</option>
                                    </select></td>
                            </tr>
                            <tr id="tb4">
                                <td align="center">
                                    SERIAL NO : 
                                </td>
                                <td>
                                    <input type="text" id="serialNo" name="serialNo" value="" class="form-control" style="width:250px" onchange="checkExists(this.value);"/> 
                                    <input type="hidden" id="serialId" name="serialId" value="" class="form-control" style="width:250px" /> 
                                </td>


                            </tr>

                        </table>
                        <div id="tableContent"></div>

                        <script>

                        var serial = [];
                        var ipqt = [];
                        var uomarr = [];
                        var sno = [];
                        var procon = [];
                        var arr = [];
                        var binIds = [];
                        var rackIds = [];
                        var locationIds = [];
                        var i = 0;





                        function go1() {
                            if (httpRequest.readyState == 4) {
                                if (httpRequest.status == 200) {
                                    var response = httpRequest.responseText;
                                    if (response != "") {
                                        alert(response);
                                        resetTheDetails();

                                    } else
                                    {
                                        saveTempSerialNos();
                                    }
                                }
                            }
                        }


                        function checkExists(serialNo) {
                            var y = document.getElementById("grnId").value;

                            var url = "";
                            if (serialNo != "") {
                                url = "./checkconnectSerialNo.do?serialNo=" + serialNo + "&grnId=" + y;

                                if (window.ActiveXObject)
                                {
                                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                } else if (window.XMLHttpRequest)
                                {
                                    httpRequest = new XMLHttpRequest();
                                }
                                httpRequest.open("POST", url, true);
                                httpRequest.onreadystatechange = function() {
                                    go1();
                                };
                                httpRequest.send(null);
                            }

                        }


                        function saveTempSerialNos() {
                            if (serial.indexOf(serialNo.value) != -1) {
                                alert("Already Scanned");
                            } else {
//                                var mk = document.getElementById("size").value;
                                var u = document.getElementById("poQuantity").value;
                                if (serial.length < u) {
                                    serial[i] = serialNo.value;
                                    ipqt[i] = quantity.value;
                                    binIds[i] = document.getElementById("binId").value;
                                    locationIds[i] =document.getElementById("storageId").value;
                                    rackIds[i] = document.getElementById("rackId").value;
                                    uomarr[i] = Uom.value;
                                    procon[i] = proCon.value;
//                                    sno[i]=i+1;
                                    i++;
//                                    totQty = parseInt(totQty) + parseInt(quantity.value);
                                    table();
                                }
                                else {
                                    alert("quanity Exceed")
                                }

                            }
                        }

                        function table(){
//                            alert(sno.length);
                                   if(serial.length>0){
                                   for(x=1;x<=serial.length;x++){
                            sno.push(x);
                            }}
                                   const tmpl = (sno,serial,uomarr,procon,ipqt,binIds,locationIds,rackIds) => `
                                   <table class="table table-info mb30 table-bordered" id="table"><thead>
                                   <tr><th>S.No<th>Serial No<th>UOM<th>Product Condition<th>Qty<th>Delete<tbody>
                        ${sno.map( (cell, index) => `
                          <tr><td><input type="hidden" id="sNo" name="sNo" value="${cell}"/>${cell}<td><input type="hidden" id="serialNos"+sno name="serialNos" value="${serial[index]}" />${serial[index]}<td><input type="hidden" id="uom" name="uom" value="${uomarr[index]}" />${uomarr[index]}<td><input type="hidden" id="proCond" name="proCond" value="${procon[index]}" />${procon[index]}<td><input type="hidden" id="ipQty" name="ipQty" value="${ipqt[index]}"/>${ipqt[index]}<td><input type="hidden" id="binIds" name="binIds" value="${binIds[index]}"/><input type="hidden" id="storageIds" name="storageIds" value="${locationIds[index]}"/><input type="hidden" id="rackIds" name="rackIds" value="${rackIds[index]}"/><input type="button" class="btn btn-success" name="${sno[index]}" value="Delete" onclick="deleteRow(this.name);" style="width:100px;height:35px;"></tr>
                          `).join('')}
                                   </table><p align = "center" style="font-size:16px"><b>Scanned Qty:</b><input type="text" id="qty" name="qty" value="${serial.length}" class="form-control" style="width:100px;height=30px" readonly/><br><br></p>`;
                                    tableContent.innerHTML=tmpl(sno,serial,uomarr,procon,ipqt,binIds,locationIds,rackIds);
                                    sno.splice(0,sno.length);
                                    document.getElementById("serialNo").value='';
                        }


                        function deleteRow(name) {
                            var x = name - 1;
                            serial.splice(x, 1);
                            uomarr.splice(x, 1);
                            ipqt.splice(x, 1);
                            procon.splice(x, 1);
                            binIds.splice(x, 1);
                            locationIds.splice(x, 1);
                            rackIds.splice(x, 1);
                            sno.splice(0, sno.length);
                            name = 0;
                            i--;
                            table();


                        }

                        </script>

                        <script>
                            function setrack(value, val1) {

                                $.ajax({
                                    url: "/throttle/getrackList.do",
                                    dataType: "json",
                                    data: {
                                        locationId: value,
                                    },
                                    success: function(data) {
                                        if (data != '') {
                                            $('#rackId').empty();
                                            $('#rackId').append(
                                                    $('<option></option>').val(0).html('--select--'))
                                            $.each(data, function(i, data) {
                                                $('#rackId').append(
                                                        $('<option style="width:90px"></option>').attr("value", data.rackId).text(data.rackName)
                                                        )
                                                document.getElementById("rackId").value = val1;


                                            });
                                        } else {

                                        }
                                    }
                                });
                            }


                            function setbin(value, val2) {

                                $.ajax({
                                    url: "/throttle/getbinList.do",
                                    dataType: "json",
                                    data: {
                                        binId: value,
                                    },
                                    success: function(data) {
                                        if (data != '') {
                                            $('#binId').empty();
                                            $('#binId').append(
                                                    $('<option></option>').val(0).html('--select--'))
                                            $.each(data, function(i, data) {
                                                $('#binId').append(
                                                        $('<option style="width:90px"></option>').attr("value", data.binId).text(data.binName)
                                                        )
                                                document.getElementById("binId").value = val2;
                                            });
                                        } else {

                                        }
                                    }
                                });
                            }

                            function saveSerialNos(val) {
                                var pq = document.getElementById("poQuantity").value;
                                var fq = serial.length;
                                if(pq==fq){
                                document.gtn.action = "/throttle/connectOpenPack.do?&param=save";
                                document.gtn.submit();
                                }else{
                                    alert("Scan all quantity");
                                }
                            }
                        </script>

                        <div>
                            <center>

                                <!--<input type="button" class="btn btn-success" name="Update" value="Update" onclick="searchPage(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;-->
                            </center>

                            <center>
                                <input type="button" class="btn btn-success" name="Update" id="update1" value="Update" onclick="saveSerialNos(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;
                            </center>
                        </div>
                    </c:if>


                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>



                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

