<html>
    <head>
        <%@page import="java.util.Iterator"%>
        <%@page import="java.util.ArrayList"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script src="/throttle/js/JsBarcode.all.min.js"></script>
    <script src="JsBarcode.all.min.js"></script>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="JsBarcode.codabar.min.js"></script>
    <style type='text/css'>
        body{
            color:#000000; background-color:#ffffff;
            font-family: "Times New Roman", Times, serif; font-size:10pt;}

        table, th, td {
            border: 1px solid black;
            padding:5px;
            border-collapse: collapse;
        }
    </style>

    <style type="text/css">
        @media print {
            #printbtn {
                display :  none;
            }
        }
    </style>
    <style type="text/css" media="print">
        @media print
        {
            @page {
                margin-top: 0;
                margin-bottom: 0;
            }
        }
    </style>
</head>

<body>
    <%int sno=1;%>
    <c:forEach items="${ifbLRPrintlist}" var="lr">
        <c:if test="${lr.type==0}">
            <c:set var="custDet" value="${lr.custName}"/>
            <c:set var="modified" value="${lr.lastModified}"/>
            <%
                                    String x = ((Object) pageContext.getAttribute("custDet")).toString();
                                    String y = ((Object) pageContext.getAttribute("modified")).toString();
                                    String[] x1= x.split("~");
                                    String[] y1= y.split(" ");
                                    pageContext.setAttribute("pickupDate",y1[0]);
                                    pageContext.setAttribute("pickupTime",y1[1]);
                                    pageContext.setAttribute("custName",x1[0]);
                                    pageContext.setAttribute("custAddress",x1[1]);
                                    pageContext.setAttribute("custCity",x1[2]);
                                    pageContext.setAttribute("custPincode",x1[3]);
                                    pageContext.setAttribute("custPhone",x1[4]);
            %>
            <c:set var="customerName" value="${lr.customerName}"/>
            <c:set var="pincode" value="${lr.pincode}"/>
            <c:set var="gstNo" value="${lr.gstinNumber}"/>
            <c:set var="phoneNo" value="${lr.mobileNo}"/>
            <c:set var="city" value="${lr.city}"/>
            <c:set var="ewayBillNo" value="${lr.ewayBillNo}"/>
            <c:set var="ewayExpiry" value="${lr.ewayExpiry}"/>
            <c:set var="lrNo" value="${lr.lrNo}"/>
            <c:set var="invoiceNo" value="${lr.invoiceNo}"/>
            <c:set var="orderType" value="${lr.orderType}"/>
            <c:set var="grossValue" value="${lr.grossValue}"/>
            <c:set var="itemDescription" value="${lr.materialDescription}"/>
            <c:set var="serialNo" value="${lr.serialNo}"/>
            <c:set var="qty" value="${lr.qty}"/>
        </c:if>
        <c:if test="${lr.type==1}">
            <c:set var="custDet" value="${lr.custName}"/>
            <c:set var="modified" value="${lr.lastModified}"/>
            <%
                                    String x = ((Object) pageContext.getAttribute("custDet")).toString();
                                    String y = ((Object) pageContext.getAttribute("modified")).toString();
                                    String[] x1= x.split("~");
                                    String[] y1= y.split(" ");
                                    pageContext.setAttribute("pickupDate",y1[0]);
                                    pageContext.setAttribute("pickupTime",y1[1]);
                                    pageContext.setAttribute("customerName",x1[0]);
                                    pageContext.setAttribute("customerAddress",x1[1]);
                                    pageContext.setAttribute("city",x1[2]);
                                    pageContext.setAttribute("pincode",x1[3]);
                                    pageContext.setAttribute("phoneNo",x1[4]);
            %>
            <c:set var="custName" value="${lr.customerName}"/>
            <c:set var="custPincode" value="${lr.pincode}"/>
            <c:set var="custGstNo" value="${lr.gstinNumber}"/>
            <c:set var="custPhone" value="${lr.mobileNo}"/>
            <c:set var="custCity" value="${lr.city}"/>
            <c:set var="ewayBillNo" value="${lr.ewayBillNo}"/>
            <c:set var="ewayExpiry" value="${lr.ewayExpiry}"/>
            <c:set var="lrNo" value="${lr.lrNo}"/>
            <c:set var="invoiceNo" value="${lr.invoiceNo}"/>
            <c:set var="serialNo" value="${lr.serialNo}"/>
            <c:set var="orderType" value="${lr.orderType}"/>
            <c:set var="grossValue" value="${lr.grossValue}"/>
            <c:set var="itemDescription" value="${lr.materialDescription}"/>
            <c:set var="qty" value="${lr.qty}"/>
        </c:if>
        <%if(sno==1){%>
        <div style="border:2px #126879 double;width:640px;height:500px;alignment-adjust: center;margin-top:30px;margin-left: 30px">
            <%}%>
        <%if(sno!=1){%>
        <div style="border:2px #126879 double;width:640px;height:500px;alignment-adjust: center;margin-top:15px;margin-left: 30px">
            <%}%>
            <div style="width:59%;text-align: center;height: 50px;padding-top: 10px;font-family:Times;float: left ">
                <font size="4.5"><b>DOMESTIC CONSIGNMENT NOTE</b></font><br>
                <font size="3.5"><c:out value="${companyName}"/><br>GST No: 24AADCH5462M1ZD</font><br>
                <div style="height:5px"></div>
                <div style="width:90%;text-align: left;height:100px;margin-left: 30px;border:1.5px black solid;padding-left: 5px;font-size: 13px">
                    From (Sender/Consignor)<br> <b>Company : <c:out value="${custName}"/></b><br>
                    <div style="width:19%;float:left"><b>Address&nbsp;&nbsp;&nbsp;:</b></div>
                    <div style="width:81%;float:right"><c:out value="${custAddress}"/></div>
                    <br><br>
                    <div style="float:left"><b>City : </b><c:out value="${custCity}"/></div>
                    <div style="float:right;padding-right: 10px"><b>Pincode : </b><c:out value="${custPincode}"/></div><br>
                    <b>Phone : </b><c:out value="${custPhone}"/>
                </div><br>
                <div style="width:90%;text-align: left;height:100px;margin-left: 30px;border:1.5px black solid;padding-left: 5px">
                    To (Receiver/Consignee) <br> <b>Company : <c:out value="${customerName}"/></b><br>
                    <div style="width:19%;float:left"><b>Address&nbsp;&nbsp;&nbsp;:</b></div>
                    <div style="width:81%;float:right"> <c:out value="${customerAddress}"/></div>
                    <br><br>
                    <div style="float:left"><b>City : </b><c:out value="${city}"/></div>
                    <div style="float:right;padding-right: 10px"><b>Pincode : </b><c:out value="${pincode}"/></div><br>
                    <b>Phone : </b><c:out value="${phoneNo}"/>
                </div>
                <br>
                <div style="width:90%;height:55px;text-align:left;border:1px solid black;margin-left: 30px;padding-left: 5px;font-size: 12"><b><u>Description</u> : </b><c:out value="${itemDescription}"/></div>
                
                <div style="width:90%;height:55px;text-align:left;border:1px solid black;margin-left: 30px;padding-left: 5px;font-size: 12;border-top: none"><b><u>Serial No</u> : </b><c:out value="${serialNo}"/></div>
                <div style="width:45%;margin-left: 30px;float:left">
                    <br><span style="float:left;">Pickup Date</span>
                    <span style="float:right;padding-right: 10px;text-align: center">
                        <div style="width:80px;height:20px;border:1px solid black"><c:out value="${pickupDate}"/></div></span>

                </div> 
                <div style="width:45%;float:right">

                    <br><span style="float:left;">Pickup Time</span>
                    <span style="float:right;padding-right: 10px;text-align: center">
                        <div style="width:80px;height:20px;border:1px solid black"><c:out value="${pickupTime}"/></div></span>

                </div>
                <div style="width:45%;margin-left: 30px;float:left">
                <div style="height:5px"></div><span style="float:left;">Pickup By</span>
                    <span style="float:right;padding-right: 10px;text-align: center">
                        <div style="width:80px;height:20px;border:1px solid black"></div></span>

                </div> 
                <div style="width:45%;float:right">
                <div style="height:5px"></div><span style="float:left;">Spcl. Pickup</span>
                    <span style="float:right;padding-right: 10px;text-align: right">
                        <div style="width:80px;height:20px;border:1px solid black"></div></span>

                </div> 

            </div>
            <div style="width:41%;text-align: center;height: 300px;padding-top: 10px;font-family: Times;float:right;">
                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<img src="images/HSSupplyLogo.png" style="width:56px;height:60px">
                <br>
                <input type="hidden" id="lrNo<%=sno%>"  value="<c:out value="${lrNo}"/>"/>

                <svg id="barcode<%=sno%>"  style="width:250px;height:65px">
                </svg>
                <br>
                <br>
                <div style="line-height:25px">
                    <span style="float:left;padding-left: 15px;"><font size="1.5">Scan all the boxes with this Consignment</font></span>
                    <br><span style="float:left;padding-left: 15px">Invoice No.</span>
                    <span style="float:right;padding-right: 10px;text-align: right">
                        <div style="width:140px;height:20px;border:1px solid black;font-weight: bold;padding-right: 2px"><c:out value="${invoiceNo}"/></div></span>

                    <br><span style="float:left;padding-left: 15px">Eway Bill No.</span>
                    <span style="float:right;padding-right: 10px;text-align: right">
                        <div style="width:140px;height:20px;border:1px solid black;font-weight: bold;padding-right: 2px"><c:out value="${ewayBillNo}"/></div></span>

                    <br><span style="float:left;padding-left: 15px">Valid Till</span>
                    <span style="float:right;padding-right: 10px;text-align: right">
                        <div style="width:140px;height:20px;border:1px solid black;font-weight: bold;padding-right: 2px"><c:out value="${ewayExpiry}"/></div></span>

                    <br><span style="float:left;padding-left: 15px">Invoice Qty</span>
                    <span style="float:right;padding-right: 10px;text-align: right">
                        <div style="width:140px;height:20px;border:1px solid black;font-weight: bold;padding-right: 2px"><c:out value="${qty}"/></div></span>

                    <br><span style="float:left;padding-left: 15px">Invoice Value</span>
                    <span style="float:right;padding-right: 10px;text-align: right">
                        <div style="width:140px;height:20px;border:1px solid black;font-weight: bold;padding-right: 2px"><c:out value="${grossValue}"/></div></span>

                    <br><span style="float:left;padding-left: 15px">Shipment Type.</span>
                    <span style="float:right;padding-right: 10px">
                        <div style="width:140px;height:20px;border:1px solid black;font-weight: bold;padding-right: 2px"><c:out value="${orderType}"/></div></span>

                    <br><span style="float:left;padding-left: 15px">Deliver Date</span>
                    <span style="float:right;padding-right: 40px">Deliver Time</span>

                    <br><span style="float:left;padding-left: 15px;text-align: left"><div style="width:110px;height:20px;border:1px solid black">Consignee Name</div></span>
                    <span style="float:right;padding-right: 10px">
                        <div style="width:110px;height:20px;border:1px solid black;text-align: left">Consignee Mobile</div></span>
                    <br>
                    <br><span style="float:left;padding-left: 15px;text-align: left"><div style="width:110px;height:19px;border:1px solid black"></div></span>
                    <span style="float:right;padding-right: 10px">
                        <div style="width:110px;height:19px;border:1px solid black;text-align: left"></div></span><br>

                    <span style="float:left;padding-left: 15px;text-align: left"><div style="width:235px;height:30px;border:1px solid black"></div></span>
                    <br><b>Receiver Sign and Stamp</b>

                </div>
            </div>

        </div>
        <div style="border:2px #126879 double;width:640px;height:500px;alignment-adjust: center;margin-top:50px;margin-left: 30px">
            <div style="width:59%;text-align: center;height: 50px;padding-top: 10px;font-family:Times;float: left ">
                <font size="4.5"><b>DOMESTIC CONSIGNMENT NOTE</b></font><br>
                <font size="3.5"><c:out value="${companyName}"/><br>GST No: 24AADCH5462M1ZD</font><br>
                <div style="height:5px"></div>
                <div style="width:90%;text-align: left;height:100px;margin-left: 30px;border:1.5px black solid;padding-left: 5px;font-size: 13px">
                    From (Sender/Consignor)<br> <b>Company : <c:out value="${custName}"/></b><br>
                    <div style="width:19%;float:left"><b>Address&nbsp;&nbsp;&nbsp;:</b></div>
                    <div style="width:81%;float:right"><c:out value="${custAddress}"/></div>
                    <br><br>
                    <div style="float:left"><b>City : </b><c:out value="${custCity}"/></div>
                    <div style="float:right;padding-right: 10px"><b>Pincode : </b><c:out value="${custPincode}"/></div><br>
                    <b>Phone : </b><c:out value="${custPhone}"/>
                </div><br>
                <div style="width:90%;text-align: left;height:100px;margin-left: 30px;border:1.5px black solid;padding-left: 5px">
                    To (Receiver/Consignee) <br> <b>Company : <c:out value="${customerName}"/></b><br>
                    <div style="width:19%;float:left"><b>Address&nbsp;&nbsp;&nbsp;:</b></div>
                    <div style="width:81%;float:right"> <c:out value="${customerAddress}"/></div>
                    <br><br>
                    <div style="float:left"><b>City : </b><c:out value="${city}"/></div>
                    <div style="float:right;padding-right: 10px"><b>Pincode : </b><c:out value="${pincode}"/></div><br>
                    <b>Phone : </b><c:out value="${phoneNo}"/>
                </div>
                <br>
                <div style="width:90%;height:55px;text-align:left;border:1px solid black;margin-left: 30px;padding-left: 5px;font-size: 12"><b><u>Description</u> : </b><c:out value="${itemDescription}"/></div>
                
                <div style="width:90%;height:55px;text-align:left;border:1px solid black;margin-left: 30px;padding-left: 5px;font-size: 12;border-top: none"><b><u>Serial No</u> : </b><c:out value="${serialNo}"/></div>
                <div style="width:45%;margin-left: 30px;float:left">
                    <br><span style="float:left;">Pickup Date</span>
                    <span style="float:right;padding-right: 10px;text-align: center">
                        <div style="width:80px;height:20px;border:1px solid black"><c:out value="${pickupDate}"/></div></span>

                </div> 
                <div style="width:45%;float:right">

                    <br><span style="float:left;">Pickup Time</span>
                    <span style="float:right;padding-right: 10px;text-align: center">
                        <div style="width:80px;height:20px;border:1px solid black"><c:out value="${pickupTime}"/></div></span>

                </div>
                <div style="width:45%;margin-left: 30px;float:left">
                <div style="height:5px"></div><span style="float:left;">Pickup By</span>
                    <span style="float:right;padding-right: 10px;text-align: center">
                        <div style="width:80px;height:20px;border:1px solid black"></div></span>

                </div> 
                <div style="width:45%;float:right">
                <div style="height:5px"></div><span style="float:left;">Spcl. Pickup</span>
                    <span style="float:right;padding-right: 10px;text-align: right">
                        <div style="width:80px;height:20px;border:1px solid black"></div></span>

                </div> 

            </div>
            <div style="width:41%;text-align: center;height: 300px;padding-top: 10px;font-family: Times;float:right;">
                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<img src="images/HSSupplyLogo.png" style="width:56px;height:60px">
                <br>
                <input type="hidden" id="lrNo<%=sno%>"  value="<c:out value="${lrNo}"/>"/>

                <svg id="barcode<%=sno%>"  style="width:250px;height:65px">
                </svg>
                <br>
                <br>
                <div style="line-height:25px">
                    <span style="float:left;padding-left: 15px;"><font size="1.5">Scan all the boxes with this Consignment</font></span>
                    <br><span style="float:left;padding-left: 15px">Invoice No.</span>
                    <span style="float:right;padding-right: 10px;text-align: right">
                        <div style="width:140px;height:20px;border:1px solid black;font-weight: bold;padding-right: 2px"><c:out value="${invoiceNo}"/></div></span>

                    <br><span style="float:left;padding-left: 15px">Eway Bill No.</span>
                    <span style="float:right;padding-right: 10px;text-align: right">
                        <div style="width:140px;height:20px;border:1px solid black;font-weight: bold;padding-right: 2px"><c:out value="${ewayBillNo}"/></div></span>

                    <br><span style="float:left;padding-left: 15px">Valid Till</span>
                    <span style="float:right;padding-right: 10px;text-align: right">
                        <div style="width:140px;height:20px;border:1px solid black;font-weight: bold;padding-right: 2px"><c:out value="${ewayExpiry}"/></div></span>

                    <br><span style="float:left;padding-left: 15px">Invoice Qty</span>
                    <span style="float:right;padding-right: 10px;text-align: right">
                        <div style="width:140px;height:20px;border:1px solid black;font-weight: bold;padding-right: 2px"><c:out value="${qty}"/></div></span>

                    <br><span style="float:left;padding-left: 15px">Invoice Value</span>
                    <span style="float:right;padding-right: 10px;text-align: right">
                        <div style="width:140px;height:20px;border:1px solid black;font-weight: bold;padding-right: 2px"><c:out value="${grossValue}"/></div></span>

                    <br><span style="float:left;padding-left: 15px">Shipment Type.</span>
                    <span style="float:right;padding-right: 10px">
                        <div style="width:140px;height:20px;border:1px solid black;font-weight: bold;padding-right: 2px"><c:out value="${orderType}"/></div></span>

                    <br><span style="float:left;padding-left: 15px">Deliver Date</span>
                    <span style="float:right;padding-right: 40px">Deliver Time</span>

                    <br><span style="float:left;padding-left: 15px;text-align: left"><div style="width:110px;height:20px;border:1px solid black">Consignee Name</div></span>
                    <span style="float:right;padding-right: 10px">
                        <div style="width:110px;height:20px;border:1px solid black;text-align: left">Consignee Mobile</div></span>
                    <br>
                    <br><span style="float:left;padding-left: 15px;text-align: left"><div style="width:110px;height:19px;border:1px solid black"></div></span>
                    <span style="float:right;padding-right: 10px">
                        <div style="width:110px;height:19px;border:1px solid black;text-align: left"></div></span><br>

                    <span style="float:left;padding-left: 15px;text-align: left"><div style="width:235px;height:30px;border:1px solid black"></div></span>
                    <br><b>Receiver Sign and Stamp</b>
                    <script>
                        var num = document.getElementById("lrNo<%=sno%>").value;

                        JsBarcode("#barcode<%=sno%>", num, {
                            textAlign: "top",
                            textPosition: "bottom",
                            font: "Times",
                            fontOptions: "bold",
                            fontSize: 25,
                            displayValue: num,
                            padding: 0,
                            margin: 0
                        });

                    </script>
                    <script>
                        JsBarcode("#barcodes<%=sno%>", num, {
                            textAlign: "top",
                            textPosition: "bottom",
                            font: "Times",
                            fontOptions: "bold",
                            fontSize: 25,
                            displayValue: num,
                            padding: 0,
                            margin: 0
                        });
                    </script>
                </div>
            </div>

        </div>
        <br>
        <br>
        <br>
        <%sno++;%>
    </c:forEach>
</body>