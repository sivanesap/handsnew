<%--
    Document   : grnDetails
    Created on : Jan 21, 2021, 12:47:26 PM
    Author     : throttle
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.PickList Master"  text="PickList Master"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.PickList Master"  text="Master"/></a></li>
            <li class="active">PickList Master</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %><br><br>
            <body onload="">
                <form name="picklist" method="post">
                    
                   
                           

                       
                    <c:if test="${picklistMaster!=null}">
                 <table class="table table-info mb30 table-bordered" id="table1" >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>Picklist No</th>
                                <th>Employee Name</th>
                                <th>Planned Date</th>
                                <th>Action</th>
                            </tr>

                        </thead>
                            <% int index = 0;
                                    int sno = 1;
                            %>
                            <tbody>

                                <c:forEach items="${picklistMaster}" var="pin">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td>
                                            <%=sno%>
                                        </td>
                                        <td ><c:out value="${pin.pickNo}"/></td>
                                        <td><c:out value="${pin.empName}"/></td>
                        <td><c:out value="${pin.planDate}"/></td>
                                        <td><input type="button" id="view" name="view" style="border:none;background-color: #DC143C; color: white ; height:30px;width: 60px; font-size: 15px;border-radius: 4px; "  value="View" onclick="viewTable(<c:out value="${pin.pickId}"/>)"></td>
                                        
                                    </tr>

                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>
                            </tbody>
                            </table>
                    </c:if>
                     <div id="table2">    
                    </div>
 <script>
  
     function viewTable(pickId){
              debugger;
              window.open("/throttle/picklistView.do?&param=view&pickId="+pickId,'PopupPage', 'height = 500, width = 800, scrollbars = yes');
                               
     }
     
    
 </script>
                           
                       
               </form>
        </body>
                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
                   
        </div>
    </div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>s