
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script> 
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script type="text/javascript">
    var lrDate = '<c:out value="${lrdate}"/>';
    lrDate = lrDate.split('-')[1] + '/' + lrDate.split('-')[0] + '/' + lrDate.split('-')[2];
    var date1 = new Date();
    var date = new Date(lrDate);
    var lrDate = Math.floor(Math.abs(date1.getTime() - date.getTime()) / (1000 * 3600 * 24));
    $(document).ready(function () {

        $("#datepicker").datepicker({minDate: -lrDate, maxDate: 0,
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });

    });

    $(function () {
        $(".datepicker").datepicker({minDate: -lrDate, maxDate: 0,
            changeMonth: true, changeYear: true,
            dateFormat: 'dd-mm-yy'
        });

    });

</script>

<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();
    });
</script>
<script language="javascript">

    function setdate() {
        var lrdate = document.getElementById("lrdate").value;
        var newdate1 = lrdate.split("-").reverse().join("-");
        var plannedDate = document.getElementById("plannedDate").value;
        var plannedDate = plannedDate.split("-").reverse().join("-");
        if (lrdate < plannedDate) {
            alert('Please check the Date')
        }
        if (lrdate == "" || lrdate == null) {
            alert('Please Enter the Date')
        }
    }


    function savePod() {
        var podName1 = document.getElementById('podName1').value;
        var podName2 = document.getElementById('podName2').value;
        var podName3 = document.getElementById('podName3').value;
        var remarks1 = document.getElementById('remarks1').value;
        var remarks2 = document.getElementById('remarks2').value;
        var remarks3 = document.getElementById('remarks3').value;
        var deliveryDate = document.getElementById('deliveryDate').value;
        if (deliveryDate == '00-00-0000' || deliveryDate.split('-').length != 3 || deliveryDate == "") {
            alert("Please Enter Delivery Date Properly");
        } else if (parseInt(deliveryDate.split('-')[0]) > 0 && parseInt(deliveryDate.split('-')[1]) > 0 && parseInt(deliveryDate.split('-')[2]) > 0) {
            document.addLect.action = '/throttle/handlePODupload.do?podName1=' + podName1 + '&podName2=' + podName2 + '&podName3=' + podName3 + '&remarks1=' + remarks1 + '&remarks2=' + remarks2 + '&remarks3=' + remarks3 + '&fromDate=<c:out value="${fromDate}"/>&toDate=<c:out value="${toDate}"/>';
            document.addLect.submit();
        } else {
            alert("Please Enter Delivery Date Properly");
        }
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.POD UPLOAD"  text="POD UPLOAD"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="trucks.label.connect"  text="HS Connect"/></a></li>
            <li class="active"><spring:message code="stores.label.POD UPLOAD"  text="POD UPLOAD"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

            <body onLoad="setGSTHead();">
                <form name="addLect" method="post" enctype="multipart/form-data">


                    <%@ include file="/content/common/message.jsp" %>

                    <div  >
                        <div id="deD">
                            <table class="table table-info mb30 table-hover">
                                <thead>
                                    <tr>
                                        <th colspan="4"  height="30">POD Upload</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td ><font color="red"></font>CUSTOMER NAME</td>
                                    <td><c:out value="${CustomerName}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td ><font color="red"></font>LR NO</td>
                                    <td >
                                        <input type="hidden" id="lrdate" name="lrdate" value="<c:out value="${lrdate}"/>" style="width:100px;height:30px;">
                                        <input type="hidden" id="dispatchDetailId" name="dispatchDetailId" value="<c:out value="${dispatchDetailId}"/>" style="width:100px;height:30px;">
                                        <input type="hidden" id="subLrId" name="subLrId" value="<c:out value="${subLrId}"/>" style="width:100px;height:30px;">
                                        <input type="hidden" id="dispatchid" name="dispatchid" value="<c:out value="${dispatchid}"/>" style="width:100px;height:30px;">
                                        <input type="hidden" id="dealerId" name="dealerId" value="<c:out value="${dealerId}"/>" style="width:100px;height:30px;">
                                        <c:out value="${lrnNo}"/><input type="hidden" id="lrnNo" name="lrnNo" value="<c:out value="${lrnNo}"/>" style="width:100px;height:30px;">
                                    </td>

                                </tr>
                                <tr>
                                    <td ><font color="red"></font>LR Date</td>
                                    <td >
                                        <c:out value="${lrdate}"/>                       
                                    </td>

                                </tr>
                                     <tr>
                                    <td ><font color="red"></font>Invoice No</td>
                                    <td >
                                        <c:out value="${invoiceNo}"/>                       
                                    </td>

                                </tr>
                                
                                </tr>
                                     <tr>
                                    <td ><font color="red"></font>Invoice Date </td>
                                    <td >
                                        <c:out value="${invoiceDate}"/>                       
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <input name="plannedTime" id="plannedTime" type="hidden" class="form-control" style="width:200px;height:40px;"  onclick="ressetDate(this);">      
                                    </td>

                                </tr>
                                <tr>
                                    <td ><font color="red"></font>DELIVERY DATE </td>
                                    <td>
                                        <input name="deliveryDate" id="deliveryDate" type="text" class="datepicker" style="width:200px;height:40px;" value="<c:out value="${deliveryDate}"/>" onChange="setdate(this);">      
                                    </td>

                                </tr>

                            </table>
                            <table class="table table-info mb30 table-hover" id="POD1">
                                <tr  >
                                <thead>
                                <th   height="30">Name</th>
                                <th   height="30">Attachment</th>
                                <th   height="30">Remarks</th>
                                </thead>
                                </tr>
                                <% int index = 1;%>
                                <tr>
                                    <td >
                                        <select name="podName" id="podName1" style="width:260px;height:40px" class="form-control" value="">
                                            <option selected value="1">POD</option>
                                            <option value="2">DEBIT NOTE</option>
                                            <option value="3">SRN</option>
                                        </select>
                                    </td>
                                    <td  ><input type='file'   id='podFile1' name='podFile' class='form-control' value='' ><br/><font size='2' color='blue'> Allowed file type:pdf & image</td>
                                    <td  ><input type='text'   id='remarks1' name='remarks' class='form-control' value=''></td>
                                </tr>
                                <tr>
                                    <td >
                                        <select name="podName" id="podName2" style="width:260px;height:40px" class="form-control" value="">
                                            <option selected value="1">POD</option>
                                            <option value="2">DEBIT NOTE</option>
                                            <option value="3">SRN</option>
                                        </select>
                                    </td>
                                    <td  ><input type='file'   id='podFile2' name='podFile' class='form-control' value='' ><br/><font size='2' color='blue'> Allowed file type:pdf & image</td>
                                    <td  ><input type='text'   id='remarks2' name='remarks' class='form-control' value=''></td>
                                </tr>
                                <tr>
                                    <td >
                                        <select name="podName" id="podName3" style="width:260px;height:40px" class="form-control" value="">
                                            <option selected value="1">POD</option>
                                            <option value="2">DEBIT NOTE</option>
                                            <option value="3">SRN</option>
                                        </select>
                                    </td>
                                    <td  ><input type='file'   id='podFile3' name='podFile' class='form-control' value='' ><br/><font size='2' color='blue'> Allowed file type:pdf & image</td>
                                    <td  ><input type='text'   id='remarks3' name='remarks' class='form-control' value=''></td>
                                </tr>

                                <%index++;%>
                            </table>

                            <br>
                            <br>
                            <center>
                                <input type="button" class="btn btn-success" value="Submit" name="Submit"  onclick="savePod()">
                            </center>

                            </body>
                        </div>

                        <!--<input type="text" id="tempQty" >
                        <input type="text" id="tempsno" >-->


                        <br>
                        <br>


                        </body>



                        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>


                    </div>
        </div>
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>

    </div>
</div>






<%@ include file="/content/common/NewDesign/settings.jsp" %>