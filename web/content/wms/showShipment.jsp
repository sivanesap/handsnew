<%-- 
    Document   : viewFkReconcile
    Created on : 27 May, 2021, 11:34:17 AM
    Author     : Roger
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script>
        
        function ChangeDropdowns(sno) {
        var shipmentType = document.getElementById('shipmentType'+sno).value;
        if (shipmentType == 'PP') {
            document.getElementById("deCod"+sno).value = '0';
            document.getElementById("deCod"+sno).style.display = 'block';
            document.getElementById("cardAmount"+sno).style.display = 'block';
            document.getElementById("prexoAmount"+sno).style.display = 'block';
            
            document.getElementById("cardAmount"+sno).value = '0';
            document.getElementById("prexoAmount"+sno).value = '0';
            
            
            var deCod = document.getElementsByName("deCod");
                                 var deCodTot = 0;
                                 for (var i = 0; i < deCod.length; i++) {
                                       deCodTot = parseFloat(deCodTot) + parseInt(deCod[i].value); 
                                     }
                                     $('#totalDeCod').val(deCodTot);
            
            var cardAmount = document.getElementsByName("cardAmount");
                                  var cardAmountTot = 0;
                                 for (var i = 0; i < deCod.length; i++) {
                                      var cardAmountTot = parseFloat(cardAmountTot) + parseInt(cardAmount[i].value); 
                                     }
                                     $('#totalCard').val(cardAmountTot);
            var prexoAmount = document.getElementsByName("prexoAmount");
                                  var prexoAmountTot = 0;
                                 for (var i = 0; i < prexoAmount.length; i++) {
                                      var prexoAmountTot = parseFloat(prexoAmountTot) + parseInt(prexoAmount[i].value); 
                                     }
//                                     alert(deCodTot);
                                     $('#totalPrexo').val(prexoAmountTot);
        } 
        if (shipmentType == 'COD') {
            
            
            var deCod = document.getElementsByName("deCod");
                                 var deCodTot = 0;
                                 for (var i = 0; i < deCod.length; i++) {
                                       deCodTot = parseFloat(deCodTot) + parseInt(deCod[i].value); 
                                     }
                                     $('#totalDeCod').val(deCodTot);
            
            var cardAmount = document.getElementsByName("cardAmount");
                                  var cardAmountTot = 0;
                                 for (var i = 0; i < deCod.length; i++) {
                                      var cardAmountTot = parseFloat(cardAmountTot) + parseInt(cardAmount[i].value); 
                                     }
                                     $('#totalCard').val(cardAmountTot);
            var prexoAmount = document.getElementsByName("prexoAmount");
                                  var prexoAmountTot = 0;
                                 for (var i = 0; i < prexoAmount.length; i++) {
                                      var prexoAmountTot = parseFloat(prexoAmountTot) + parseInt(prexoAmount[i].value); 
                                     }
//                                     alert(deCodTot);
                                     $('#totalPrexo').val(prexoAmountTot);
        } 
            
        } 
    
    
        function submitPage() {
            document.reconcile.action = "viewFkReconcile.do?&param=save&statusId=14";
            document.reconcile.submit();
        }
    </script>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.RunSheet" text="Trip Reconciliation Details"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Flipkart" text="Flipkart"/></a></li>
                <li class=""><spring:message code="hrms.label.Reconcile" text="Reconciliation"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">
                    <form name="reconcile" method="post">
                        <%@include file="/content/common/message.jsp" %>
                        <table class="table table-info mb30 table-bordered" id="table"  style="width:98%;" >
                            <thead>
                                <tr >
                                    <th >Sno</th>                                        
                                    <th >Shipment Id</th>                                        
                                    <th >Shipment Type</th>                                        
                                    <th >DE Name</th>
                                    <th >Product Detail</th>
                                    <th >Pickup/Prexo Remarks</th>
                                    <th >Pincode</th>
                                    <th >status</th>
                                    <th >Rejection Remarks</th>
                                    <th >Product Value</th>
                                    <th >COD Amount</th>                        
                                    <th >DE COD</th>    
                                    <th >Card Amount</th>
                                    <th >Prexo Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 1;%>
                                <c:forEach items="${reconcileMaster}" var="re">
                                    <%
                                        String className = "text1";
                                        if ((index % 2) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>
                                    
                                    <tr height="30">
                                        <td><%=index%></td>                                      
                                        <td><c:out value="${re.shipmentId}"/></td>
                                        <c:if test="${re.shipmentType=='COD'}"><td>COD</td>   </c:if>
                                        <c:if test="${re.shipmentType=='PP'}"><td>PP</td>   </c:if>
                                        <td><c:out value="${re.empName}"/></td>                                                    
                                        <td><c:out value="${re.materialDescription}"/></td>                                      
                                        <td><c:out value="${re.remarks}"/></td>                                      
                                        <td><c:out value="${re.pincode}"/></td>  
                                        <c:if test="${re.status==4}">
                                            <td>Delivered</td>
                                        </c:if>
                                        <c:if test="${re.status==3}">
                                            <td>Rejected</td>  
                                        </c:if>
                                        <c:if test="${re.status==2}">
                                            <td>Not Attempted</td>  
                                        </c:if>
                                        <td><c:out value="${re.reason}"/></td>  
                                        <td align="right" ><c:out value="${re.price}"/></td>                                      
                                        <td align="right" ><c:out value="${re.codAmount}"/></td>                                      
                                        <td align="right" ><c:out value="${re.deCod}"/></td>                                      
                                        <td align="right" ><c:out value="${re.cardAmount}"/></td>                                      
                                        <td align="right" ><c:out value="${re.prexoAmount}"/></td>                                      
                                    </tr>
                                <input type="hidden" name="orderId" id="orderId<%=index%>" value="<c:out value="${re.orderId}"/>">
                                <c:set var="totalPrice" value="${totalPrice+re.price}"/>
                                <c:set var="totalCod" value="${totalCod+re.codAmount}"/>
                                <c:set var="totalDeCod" value="${totalDeCod+re.deCod}"/>
                                <c:set var="totalCard" value="${totalCard+re.cardAmount}"/>
                                <c:set var="totalPrexo" value="${totalPrexo+re.prexoAmount}"/>
                                <%index++;%>
                            </c:forEach>
                            
                            </tbody>
                        </table>
                        
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>