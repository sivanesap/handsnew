<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>



</head>
<script language="javascript" type="text/javascript">
            setFilterGrid("table");
             function orderStatus(orderId) {
                 debugger;
            window.open('/throttle/handleViewOrderStatus.do?orderId=' + orderId +'&value=1', 'PopupPage', 'height = 400, width = 800, scrollbars = yes, resizable = yes');
        }
        </script>
<script>
    function searchPage() {
        document.invoiceSearch.action = "/throttle/shipmentReport.do";
        document.invoiceSearch.submit();
    }
    function getWarehouseId(value){
        if(value!=""&&value!=null){
        document.getElementById("fleetCenterId").value=value;
        }
    }
    
     function submitPage1() {
            document.invoiceSearch.action = '/throttle/shipmentReport.do?param=exportExcel';
            document.invoiceSearch.submit();
        }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Shipment Report </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html">Wms report</a></li>
            <li class="">Shipment Report</li>

        </ol>
    </div>
</div>
    
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="contentpanel" style="overflow:auto">
    <div class="row" align="center" style="margin:5px;">
        
        <div class="col-sm-1 col-md-3">
            <div class="panel panel-success panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">

                                <i class="ion ion-android-bus"></i>
                            </div>
                            <div class="col-xs-8">

                                <h4> Total Delivered</h4>

                                <h1><span id="deliveredList"></span></h1>
                            </div></div></div></div></div></div></div></div>    
<body onload="getWarehouseId(<c:out value="${whId}"/>)">
    <%
                String menuPath = "Order List >> View / Edit";
                request.setAttribute("menuPath", menuPath);
    %>
    <form name="invoiceSearch" method="post">
     
        <table class="table table-info mb30 table-hover" id="report" >
            <thead>
		<tr>
		    <th colspan="6" height="30" >Shipment Report</th>
		</tr>
            </thead>
            <tr>
                <td><font color="red">*</font>From Date</td>
                <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:250px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                <td><font color="red">*</font>To Date</td>
                <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>
                <td>Warehouse Name</td>
                                          <td height="30"> <select name="fleetCenterId" id="fleetCenterId"  class="form-control" style="width:250px;height:40px" style="height:20px; width:122px;">
                                               <c:if test="${getWareHouseList != null}">
                                                    <option value="" selected>--Select--</option>
                                                    <c:forEach items="${getWareHouseList}" var="companyList">
                                                        <option value='<c:out value="${companyList.warehouseId}"/>'><c:out value="${companyList.whName}"/></option>
                                                    </c:forEach>
                                                </c:if>
                                            </select></td>
            </tr>
            <tr>             
                
               <td  colspan="6"align="center">
                   <input type="button" class="btn btn-success"   value="Search" onclick="searchPage()">
                   <input type="button" class="btn btn-success" name="ExportExcel" id="ExportExcel" onclick="submitPage1(this.name);" value="ExportExcel">
               </td>
           </tr>
        </table>
        </div>
    </div>
         
    <c:if test = "${ordersList != null}" >
        <table class="table table-info mb30 table-hover" id="table" >
            <thead>
                <tr height="40">
                    <th>Sno</th>
                    <th width="20%">LoanProposalID</th>
                    <th>Customer</th>                    
                    <th>PinCode</th>
                    <th>Product Name</th>
                    <th>Status</th>
                    <th width="10%">Delivered Date & Time</th>
                    <th>Invoice Number</th>
                    <th>Invoice View</th>
                    <th>Order View</th>
                </tr>
            </thead>
            <% int index = 0;
                        int sno = 1;
                        int deliveredCount=0;
            %>
            <tbody>
                <c:forEach items="${ordersList}" var="order">

                    <tr height="30">
                        <td align="left" ><%=sno%></td>
                        <td align="left" ><c:out value="${order.orderNo}"/></td>
                        <td align="left" ><c:out value="${order.clientName}"/></td>                        
                        <td align="left" ><c:out value="${order.pinCode}"/></td>             
                        <td align="left" ><c:out value="${order.model}"/></td>               
                        <td align="left" >                           
                            <c:if test="${order.cancel == 1}">
                                Order Canceled
                            </c:if>
                            <c:if test="${order.cancel != 1}">
                                <c:if test="${order.invoiceStatus == 0}">
                                    Order Created
                                </c:if>
                                <c:if test="${order.invoiceStatus == 1}">
                                    Scheduled
                                </c:if>
                                <c:if test="${order.invoiceStatus == 2}">
                                    Invoice Created
                                </c:if>
                                <c:if test="${order.invoiceStatus == 3}">
                                    OFD
                                </c:if>
                                <c:if test="${order.invoiceStatus == 4}">
                                    Delivered
                                    <%
                                    deliveredCount++;
                                    %>
                                </c:if>
                                <c:if test="${order.invoiceStatus == 5}">
                                    Returned
                                </c:if>
                                <c:if test="${order.invoiceStatus == 8}">
                                    Cancelled
                                </c:if>
                                <c:if test="${order.invoiceStatus == 9}">
                                    OFD
                                </c:if>
                            </c:if>
                                    </a>
                        </td>  
                        <c:if test="${order.invoiceStatus==4}">
                            <td><c:out value="${order.modifiedDate}"/></td>
                        </c:if>
                         <c:if test="${order.invoiceStatus!=4}">
                            <td>-</td>
                        </c:if>
                        <td align="left" ><c:out value="${order.invoiceNumber}"/></td>
                        <c:if test="${order.path  !='NA'}">
                            
                        <td align="left" ><a href="<c:out value="${order.path}"/>" target="_blank" download>
                                                    <span class="label label-Primary"><font size="2">PDF</font></span> </a>
                                            </td>
                        </c:if>
                        <c:if test="${order.path == 'NA'}">
                            
                        <td align="left" > NA </td>
                        </c:if>
                                            
                        
                         <td align="left" >
                             <a href="#" style="color:#ff6699;"  onclick="orderStatus('<c:out value="${order.orderId}"/>')" ><span >View</span> </a></td> 
                    </tr>
                    <%index++;%>
                    <%sno++;%>
                </c:forEach>
            <input type="hidden" value="<%=deliveredCount%>" id="deliveredCount" name="deliveredCount">
            </tbody>
        </table>
    </c:if>
      
        <script language="javascript" type="text/javascript">
    document.getElementById("deliveredList").innerHTML = document.getElementById("deliveredCount").value;       
    setFilterGrid("table");
        </script>
    </form>
</body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>