<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%> 
<%@page import="java.text.SimpleDateFormat" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
   

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.LHC Update"  text="Daily Update"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Approval"  text="Approval"/></a></li>
            <li class="active">DC Bill Approval</li>
        </ol>
    </div>
</div>
<%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat sdf1 = new SimpleDateFormat("01-MM-yyyy");
            String todayDate = sdf.format(today);
            String monthNow = sdf1.format(today);
%>
<script>
    
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });
    
    function approvePage(){
        
    }
    function searchPage(){
        document.
    }
    
    function setValue() {

        var selectedTrip = document.getElementsByName('selectedIndex');
        var temp = "";
        var cntr = 0;
        for (var i = 0; i < selectedTrip.length; i++) {
            if (selectedTrip[i].checked == true) {
                document.getElementById('selectedIndexs' + i).value = 1;
            } else {
                document.getElementById('selectedIndexs' + i).value = 0;
            }
        }
    }

    function searchPage(val) {
        var selectedLhc = document.getElementsByName('selectedIndex');
        var lhcId = document.getElementsByName('lhcId');
        var approvStatus = document.getElementsByName('approvStatus');
        var temp = "";
        var cntr = 0;
        for (var i = 0; i < selectedLhc.length; i++) {
//            alert("i="+i);
            if (selectedLhc[i].checked == true) {
                document.getElementById('selectedIndexs' + i).value = 1;
//                alert("selected :"+selectedTrip[i].value);
//                alert("trip expense id :"+tripExpenseId[i].value);
//                alert("approvStatus"+approvStatus[i].value);
                if (cntr == 0) {
                    temp = lhcId[i].value;
                } else {
                    temp = temp + "," + lhcId[i].value;
                }
                cntr++;
            } else {
                document.getElementById('selectedIndexs' + i).value = 0;
            }
        }

        if (temp != "") {
//            alert("Hai:"+temp); 
            document.repair.action = "/throttle/lhcApproval.do?param=" + val + "&lhcIds=" + temp;
            document.repair.submit();

        } else {
            alert("Please select any one and proceed....");
        }

    }

</script>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="">
                <form name="repair" method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <table  class="table table-info mb30 table-hover"  align="left" style="width:100%;">
                        <tr>
                            <td >From Date</td>
                            <td >
                                <input name="fromDate" id="fromDate" style="width: 250px;height:40px" autocomplete="off" type="text" class="form-control datepicker" onclick="ressetDate(this);" value="<c:out value="${todayDate}"/>"/>
                            </td>

                            <td >To Date</td>
                            <td >
                                <input name="toDate" id="toDate" autocomplete="off" style="width: 250px;height:40px" type="text" class="form-control datepicker"  onClick="ressetDate(this);"  value="<c:out value="${monthNow}"/>"/>
                            </td>
                            <td colspan="2"></td>
                        </tr>  
                        <tr>
                            <td >Vendor Name</td>
                            <td >
                                <select name="vendorId" id="vendorId" style="width: 250px;height:40px" class="form-control" >
                                    <option value="">-----Select Vendor-----</option>
                                    <c:forEach items="${vendorList}" var="vl">
                                        <option value="<c:out value="${vl.vendorId}"/>"><c:out value="${vl.vendorName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                    </table>
                            <center>
                                <input type="button" class="btn btn-success" style="width:120px" value="Search" onclick="approvePage();">
                            </center>
                            <br>
                    <c:if test="${vendorBillApproval == null }" >
                        <center><font color="red" size="2">No Records Found</font></center>
                        </c:if>
                        <c:if test="${vendorBillApproval != null}">
                        <table class="table table-info mb30 table-bordered" id="table" >
                            <thead >
                                <tr >
                                    <th>S.No</th>
                                    <th>Date</th>
                                    <th>Runsheet ID</th>
                                    <th>Transporter</th>
                                    <th>Vehicle Type</th>	
                                    <th>Total qty</th>	
                                    <th>Start Date & Time</th>
                                    <th>End Date & Time</th>	
                                    <th>Start KM</th>
                                    <th>End KM</th>	
                                    <th>Total KM</th>	
                                    <th>Fixed Km</th>	
                                    <th>Fixed Hr</th>	
                                    <th>Billing Type</th> 	
                                    <th>Freight Charges</th>
                                    <th>Extra Km Charges</th>
                                    <th>Extra Hr Charges</th>
                                    <th>Other Expenses</th>
                                    <th>Total Amount to be billed</th>
                                </tr>

                            </thead>
                            <% int index = 0;
                                int sno = 1;
                            %>
                            <tbody>

                                <c:forEach items="${vendorBillApproval}" var="rm">

                                    <%
                                        String className = "text1";
                                        if ((index % 2) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>
                                    <tr >
                                        <td>
                                            <%=sno%>
                                        </td>
                                        <td><c:out value="${rm.scheduleDate }"/></td>
                                        <td><c:out value="${rm.tripId }"/></td>
                                        <td><c:out value="${rm.vendorName }"/></td>
                                        <td><c:out value="${rm.vehicleTypeName}"/></td>
                                        <td><c:out value="${rm.qty }"/></td>
                                        <td><c:out value="${rm.tripactualstartDate }"/>&nbsp;
                                             <c:out value="${rm.tripactualstartTime }"/></td>
                                        <td><c:out value="${rm.tripactualendDate }"/>&nbsp;
                                             <c:out value="${rm.tripactualendTime }"/></td>
                                        <td><input type="text" name="startKm" id="startKm<%=sno%>" value="<c:out value="${rm.tripstartKm }"/>"/></td>
                                        <td><input type="text" name="endKm" id="endKm<%=sno%>" value="<c:out value="${rm.tripendKm }"/>"/></td>
                                        <td><input type="text" name="runKm" id="runKm<%=sno%>" readonly="" value="<c:out value="${rm.tripendKm-rm.tripstartKm }"/>"/></td>
                                        <td><c:out value="${rm.daymaxKm }"/></td>
                                        <td><c:out value="${rm.daymaxHr}"/></td>
                                        <td>
                                            <input type="hidden" id="billingType<%=sno%>" name="billingType" value="<c:out value="${rm.billingType}">"/>
                                            <input type="hidden" id="tripId<%=sno%>" name="tripId" value="<c:out value="${rm.tripId}">"/>
                                            <c:if test="${rm.billingType==1}">
                                                Monthly
                                            </c:if>
                                            <c:if test="${rm.billingType==2}">
                                                Daily
                                            </c:if>
                                            <c:if test="${rm.billingType==''||rm.billingType==null}">
                                                Not Updated
                                            </c:if>
                                        </td>
                                        <td><c:out value="${rm.freightCharge}"/></td>
                                        <td id="extraKmChg"><c:if test="${rm.billingType==1}">
                                                0
                                            </c:if>
                                            <c:if test="${rm.billingType==2}">
                                                <c:if test="${rm.tripstartKm+tm.tripendKm > rm.daymaxKm}">
                                                    <c:out value="${(rm.tripstartKm+tm.tripendKm-rm.daymaxKm)*rm.kmexceedRate}"/> 
                                                    <c:set var="extraKmCharge" value="${(rm.tripstartKm+tm.tripendKm-rm.daymaxKm)*rm.kmexceedRate}"/> 
                                                </c:if>
                                                <c:if test="${rm.tripstartKm+tm.tripendKm <= rm.daymaxKm}">
                                                    0 
                                                    <c:set var="extraKmCharge" value="0"/> 
                                                </c:if>
                                            </c:if>
                                        </td>
                                        <td id="extraHrChg">
                                            <c:if test="${rm.billingType==1}">
                                                0
                                            </c:if>
                                            <c:if test="${rm.billingType==2}">
                                                <c:if test="${rm.timeDiff > rm.daymaxHr}">
                                                    <c:out value="${(rm.timeDiff-rm.daymaxHr)*rm.hrexceedRate}"/> 
                                                    <c:set var="extraHrCharge" value="${(rm.timeDiff-rm.daymaxHr)*rm.hrexceedRate}"/> 
                                                </c:if>
                                                <c:if test="${rm.tripstartKm+tm.tripendKm <= rm.daymaxKm}">
                                                    <c:set var="extraHrCharge" value="0"/> 
                                                    0 
                                                </c:if>
                                            </c:if>
                                        </td>
                                        <td>
                                            <c:out value="${rm.tollExpense+rm.otherExpense}"/>
                                            <c:set var="totExp" value="${rm.tollExpense+rm.otherExpense}"/>
                                        </td>
                                        <td>
                                            <c:out value="${rm.freightCharge+extraKmCharge+extraHrCharge}"/>
                                        </td>

                                    </tr>

                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>     
                            </tbody>



                        </table>
                    </c:if>
                    <div>
                        <center>

                            <!--<input type="button" class="btn btn-success" name="Update" value="Update" onclick="searchPage(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;-->
                        </center>
                    </div>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
<!--                    <div id="loader"></div>

                    <div id="controls"  >
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5"  selected="selected" >5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>-->

                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>


                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

