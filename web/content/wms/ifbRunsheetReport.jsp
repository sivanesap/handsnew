<%-- 
    Document   : ifbRunsheetReport
    Created on : 11 Jun, 2021, 12:25:53 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>
<script type="text/javascript">
    function searchPage() {
        document.ifb.action = '/throttle/ifbRunsheetReport.do';
        document.ifb.submit();
    }
    function excelPage() {
        document.ifb.action = '/throttle/ifbRunsheetReport.do?&param=ExportExcel';
        document.ifb.submit();
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="IFB Runsheet Report"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.IFB" text="ifb"/></a></li>
            <li class=""><spring:message code="hrms.label.ifb" text="IFB Runsheet Report"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >
                <form name="ifb"  method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <div id="ptp" style="overflow: auto">
                        <div class="inpad">
                            <table class="table table-info mb30 table-hover" style="width:100%">
                                <thead><tr><th colspan="7">IFB Runsheet Report</th></tr></thead>
                                <tr>
                                    <td>From Date</td>
                                    <td><input type="text" class="datepicker form-control" autocomplete="off" name="fromDate" id="fromDate" style="width:200px;height:40px"/></td>
                                    <td>To Date</td>
                                    <td><input type="text" class="datepicker form-control" autocomplete="off" name="toDate" id="toDate" style="width:200px;height:40px"/></td>
                                </tr>
                                <tr>
                                    <td  ><font color="red">*</font>Shipment Type</td>
                                    <td>
                                        <select class="form-control" id="orderType" name="orderType" style="width:200px;height: 40px">
                                            <option value="">------Select One------</option>
                                            <option value="1">Delivery</option>
                                            <option value="2">Exchange</option>
                                            <option value="3">PickUp</option>
                                        </select>
                                    </td>
                                    <td>Vehicle No</td>
                                    <td><input type="text" class="form-control" name="vehicleNo" id="vehicleNo" style="width:200px;height:40px"/></td>
                                </tr>
                                <tr>
                                    <td><font color="red">*</font>DE Name</td>
                                    <td>
                                        <select class="form-control" id="empId" name="empId" style="width:200px;height: 40px">
                                            <option value="">------Select One------</option>
                                            <c:forEach items="${deliveryExecutiveList}" var="de">
                                                <option value="<c:out value="${de.empId}"/>"><c:out value="${de.empName}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <script>
                                document.getElementById('orderType').value = '<c:out value="${orderType}"/>'
                                document.getElementById('fromDate').value = '<c:out value="${fromDate}"/>'
                                document.getElementById('toDate').value = '<c:out value="${toDate}"/>'
                                document.getElementById('vehicleNo').value = '<c:out value="${vehicleNo}"/>'
                                document.getElementById('empId').value = '<c:out value="${empId}"/>'
                            </script>
                            <center>
                                <input type="button" class="btn btn-success"   value="Search" onclick="searchPage()">&emsp;&emsp;
                                <input type="button" class="btn btn-success"   value="Export Excel" onclick="excelPage()">
                            </center>

                            <div id="ptp" style="overflow: auto">
                                <div class="inpad">
                                    <table class="table table-info mb30 table-hover" id="table" >

                                        <thead>
                                            <tr height="40">
                                                <th  height="30" >S.No</th>
                                                <th  height="30" >Runsheet No</th>
                                                <th  height="30" >Assigned Date</th>
                                                <th  height="30" >DE Name</th>
                                                <th  height="30" >Hub Name</th>
                                                <th  height="30" >Vehicle No</th>
                                                <th  height="30" >Total Quantity</th>
                                                <th  height="30" >User Name</th>
                                                <th  height="30" >Status</th>
                                            </tr>
                                        </thead>
                                        <% int index = 0;
                                            int sno = 1;
                                        %>
                                        <tbody>

                                            <c:if test = "${ifbRunsheetReport != null}" >
                                                <c:forEach items= "${ifbRunsheetReport}" var="ifb">
                                                    <tr height="30">
                                                        <td align="left"><%=sno%></td>
                                                        <td align="left"   ><c:out value="${ifb.tripCode}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.date}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.driverName}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.whName}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.vehicleNo}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.qty}"/></td>
                                                        <td align="left"   ><c:out value="${ifb.empName}"/></td>
                                                        <td align="left"   >
                                                            <c:if test="${ifb.tripStatusId==8}">
                                                                <c:if test="${ifb.status==1}">
                                                                    Completed Delivery
                                                                </c:if>
                                                                <c:if test="${ifb.status==2}">
                                                                    Delivery Pending
                                                                </c:if>
                                                                <c:if test="${ifb.status==3}">
                                                                    Runsheet Assigned
                                                                </c:if>
                                                            </c:if>
                                                                <c:if test="${ifb.tripStatusId>=12}">
                                                                Runsheet Closed
                                                            </c:if>
                                                        </td>
                                                    </tr>
                                                    <%
                                                        sno++;
                                                        index++;
                                                    %>
                                                </c:forEach>
                                            <input type="hidden" name="sno" id="sno" value="<%=sno%>">
                                        </c:if>
                                        <script language="javascript" type="text/javascript">
                                            setFilterGrid("table");
                                        </script>
                                        </tbody>
                                    </table>
                                </div>
                            </div>    

                        </div>
                    </div>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>