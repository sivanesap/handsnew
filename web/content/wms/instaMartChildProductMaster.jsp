<%-- 
    Document   : instaMartChildProductMaster
    Created on : 15 Nov, 2021, 3:18:15 PM
    Author     : alan
--%>



<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript"></script>

    <script>
        function insertInstaMart() {
            var skuCode = document.getElementById("skuCode").value;
            var productName = document.getElementById("productName").value;
            var productDescription = document.getElementById("productDescription").value;
            var minimumStockQty = document.getElementById("minimumStockQty").value;
            var uom = document.getElementById("uom").value;
            var temperature = document.getElementById("temperature").value;
            var storage = document.getElementById("storage").value;
            var activeInd = document.getElementById("activeInd").value;
            if (activeInd != "0") {

                console.log(skuCode, productName, productDescription, minimumStockQty, uom, temperature, storage, activeInd);
                if (skuCode != "" && productName != "" && productDescription != "" && minimumStockQty != "" && uom != "" && temperature != "" && storage != "" && activeInd != "")
                {
                    document.instaMart.action = "/throttle/instaMartProductMaster.do?param=save";
                    document.instaMart.submit();
                }

                else {
                    alert("Enter The Value");
                }
            } else {
                alert("Please Select Active Not Active ");
            }
        }

    function uploadPage() {
            document.upload.action = "/throttle/imProductMasterExcelUpload.do?param=upload";
            document.upload.submit();
    
    }

        function updateInstaMart(sno, productName, skuCode, productDescription, minimumStockQty, uom, temperature, storage, activeInd, productId) {

            document.getElementById("productName").value = productName;
            document.getElementById("skuCode").value = skuCode;
            document.getElementById("productDescription").value = productDescription;
            document.getElementById("minimumStockQty").value = minimumStockQty;
            document.getElementById("uom").value = uom;
            document.getElementById("temperature").value = temperature;
            document.getElementById("storage").value = storage;
            document.getElementById("activeInd").value = activeInd;
            document.getElementById("productId").value = productId;


            var count = parseInt(document.getElementById("count").value);
            for (i = 1; i <= parseInt(count); i++) {
                if (i != sno) {
                    document.getElementById("edit" + i).checked = false;
                } else {
                    document.getElementById("edit" + i).checked = true;
                }
            }
        }
        
        
 function insertTo(){
    
            
            var result = confirm("Some errors are not Updated. Do you want to continue?");
            if (result == true) {
                document.upload.action = "/throttle/imProductMasterToMainTable.do?";
                document.upload.method = "post";
                document.upload.submit();
            }
            else {
            
            document.upload.action = "/throttle/imProductMasterToMainTable.do?";
            document.upload.method = "post";
            document.upload.submit();
        }
    }
       
        
        

    </script>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Product Master</h2>

        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Instamart</a></li>
                <li class="">Product Master</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <form name="upload"  method="post" enctype="multipart/form-data">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    <table class="table table-info mb30 table-hover" id="uploadTable"  >
                        <thead> 
                            <tr>
                                <th  colspan="3">Dealer Master Upload</th>
                            </tr>

                        </thead>


                        <tr>
                            <td >Select Excel</td>
                            <td ><input type="file" name="importCnote" id="importCnote"  class="importCnote"><font size="1"><u><a href="uploadedxls/ifbFormat.xls" download>sample file</a></u></font></td>                             
                            <td colspan="0"><input type="button" class="btn btn-success" value="Upload" name="UploadPage" id="UploadPage" onclick="uploadPage();"></td>
                        </tr>

                    </table>
                    <div id="ptp">
                        <div class="inpad" style=" overflow-x: visible;">

                            <c:if test = "${getImProductMasterTemp != null}" >

                                <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                    <thead style="font-size:12px">
                                        <tr height="40">
                                            <th>Sno</th>
                                            <th>Product Name</th>
                                            <th>SKU Code</th>
                                            <th>Product Description</th>
                                            <th>Minimum Stock Qty</th>
                                            <th>UOM</th>
                                            <th>Temperature</th>
                                            <th>Storage</th>
                                            <th>Active</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                     int count=0;
                                    %> 
                                    <tbody>

                                        <c:forEach items="${getImProductMasterTemp}" var="pc">


                                            <tr>
                                                <td align="left"> <%=sno%> </td>
                                                <td align="left"> <c:out value="${pc.productName}"/></td>
                                                <td align="left"> <c:out value="${pc.skuCode}"/> </td>
                                                <td align="left"> <c:out value="${pc.productDescription}"/></td>
                                                <td align="left"> <c:out value="${pc.minimumStockQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.uom2}"/> </td>
                                                <td align="left"> <c:out value="${pc.temperature}"/> </td>
                                                <td align="left"> <c:out value="${pc.storage}"/> </td>
                                                <td align="left">
                                                    <c:if test="${pc.activeInd=='Y'}">
                                                        Active
                                                    </c:if>
                                                    <c:if test="${pc.activeInd=='N'}">
                                                        In-Active
                                                    </c:if>
                                                </td>



                                                <c:if test="${pc.status==0}">
                                                    <td align="left"><font color="green">Ready To Submit</font></td>
                                                        </c:if>
                                                        <c:if test="${pc.status==1}">
                                                    <td align="left"><font color="red">Dealer Code Already Exits Temp</font></td>
                                                            <%count++;%>
                                                        </c:if>
                                                    <c:if test="${pc.status==2}">
                                                    <td align="left"><font color="red">Dealer Code Already Exits Main Table</font></td>
                                                            <%count++;%>
                                                        </c:if>


                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                    </tbody>
                                </table>

                                <center>

                                    <td colspan="0"><input type="button" class="btn btn-success" value="Submit" name="submitPage" id="submitPage" onclick="insertTo();"></td>
                                </center>
                            </c:if>
                        </div>
                    </div>     

                </form>






                <form name="instaMart" method="post">
                    <%@include file = "/content/common/message.jsp"%>
                    <table class="table table-info mb30 table-hover">
                        <input type="hidden" name="productId" id="productId" value=""/>
                        <tr>

                            <td> Product Name   &emsp;</td>
                            <td><input type="productName" name="productName" id="productName" class="form-control" style="width:240px;height:40px" value=""></td>

                            <td> SKU Code   &emsp;</td>
                            <td><input type="skuCode" name="skuCode" id="skuCode" class="form-control" style="width:240px;height:40px" value=""></td>

                        </tr>

                        <tr>

                            <td> Product Description   &emsp;</td>
                            <td><input type="productDescription" name="productDescription" class="form-control" id="productDescription" style="width:240px;height:40px" value=""></td>

                            <td> Minimum Stock Qty   &emsp;</td>
                            <td><input type="minimumStockQty" name="minimumStockQty" class="form-control" id="minimumStockQty" style="width:240px;height:40px" value=""></td>

                        </tr>

                        <tr>

                            <td> UOM   &emsp;</td>
                            <td><input type="uom" name="uom" id="uom" class="form-control" style="width:240px;height:40px" value=""></td>

                            <td> Temperature  &emsp;</td>
                            <td><input type="temperature" name="temperature" id="temperature" class="form-control" style="width:240px;height:40px" value=""></td>

                        </tr>

                        <tr>

                            <td> Storage  &emsp;</td>
                            <td><input type="storage" name="storage" id="storage" class="form-control" style="width:240px;height:40px" value=""></td>

                            <td> Active&emsp;</td>
                            <td><select type="activeInd" name="activeInd" id="activeInd" class="form-control" style="width:240px;height:40px" value="">
                                    <option value ="0">--------Select--------</option>
                                    <option value ="Y">Active</option>
                                    <option value ="N">In-Active</option>
                                </select>

                        </tr>
                    </table>
                    <center><input type="button" class="btn btn-success"  name="save" id="save"  value="Save" onclick="insertInstaMart();"></center>

                    </table>

                </form>

                <br>


                <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                        <tr height="30" style="colour:blue">
                            <th>Sno</th>
                            <th>Product Name</th>
                            <th>SKU Code</th>
                            <th>Product Description</th>
                            <th>Minimum Stock Qty</th>
                            <th>UOM</th>
                            <th>Temperature</th>
                            <th>Storage</th>
                            <th>Active</th>
                            <th>Action</th>


                    </thead>

                    <%int sno1 = 1;%>
                    <tbody>
                        <c:if test="${instaMartProduct !=null}">
                            <c:forEach items="${instaMartProduct}" var="pc">


                                <tr>
                                    <td align="left"> <%=sno1%> </td>
                                    <td align="left"> <c:out value="${pc.productName}"/></td>
                                    <td align="left"> <c:out value="${pc.skuCode}"/> </td>
                                    <td align="left"> <c:out value="${pc.productDescription}"/></td>
                                    <td align="left"> <c:out value="${pc.minimumStockQty}"/> </td>
                                    <td align="left"> <c:out value="${pc.uom2}"/> </td>
                                    <td align="left"> <c:out value="${pc.temperature}"/> </td>
                                    <td align="left"> <c:out value="${pc.storage}"/> </td>
                                    <td align="left">
                                        <c:if test="${pc.activeInd=='Y'}">
                                            Active
                                        </c:if>
                                        <c:if test="${pc.activeInd=='N'}">
                                            In-Active
                                        </c:if>
                                    </td>
                                    <td> <input type="checkbox"  value="save" id="edit<%=sno1%>" onclick="updateInstaMart(<%=sno1%>, '<c:out value="${pc.productName}"/>', '<c:out value="${pc.skuCode}"/>', '<c:out value="${pc.productDescription}"/>', '<c:out value="${pc.minimumStockQty}"/>', '<c:out value="${pc.uom2}"/>', '<c:out value="${pc.temperature}"/>', '<c:out value="${pc.storage}"/>', '<c:out value="${pc.activeInd}"/>', '<c:out value="${pc.productId}"/>');"/></td>
                                </tr>
                                <%sno1++;%>
                            </c:forEach>
                        </tbody>
                    </c:if>
                </table>
            </div>
        </div>
    </div>
    <input type = "hidden" name="count" id="count" value="<%=sno1%>"/>
    <%@include file="/content/common/NewDesign/settings.jsp"%>
</html>