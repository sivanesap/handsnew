<%-- 
    Document   : dzCratesReport
    Created on : 4 Jan, 2022, 5:03:16 PM
    Author     : alan
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>


<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<script>
      $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
    function viewCrates(sublrId) {
        window.open("/throttle/dzCratesReport.do?param=view&sublrId=" + sublrId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function searchPage(){
        document.connect.action="/throttle/dzCratesReport.do?param=search";
        document.connect.submit();
    }
</script>


<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Crates report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="general-forms.html">Shop Tree</a></li>
            <li class="active">Crates reports</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="connect" method="post">
                    <br>
                    <br>
                    <table class="table table-info mb30 table-hover" id="table" style="font-size:12px">
                        <thead><tr><th colspan="4">Crates Report</th></tr></thead>
                    <tbody>
                        <tr>
                            <td>From Date</td>
                            <td>
                                <input type="text" autocomplete="off" class="form-control datepicker" style="width:240px;height:40px" id="fromDate" name="fromDate" value="<c:out value="${fromDate}"/>">
                            </td>
                            <td>To Date</td>
                            <td>
                                <input type="text" autocomplete="off" class="form-control datepicker" style="width:240px;height:40px" id="toDate" name="toDate" value="<c:out value="${toDate}"/>">
                            </td>
                        </tr>
                        <tr>
                            <td>Select Warehouse</td>
                            <td>
                                <select id="whId" name="whId" style="width:240px;height:40px" class="form-control">
                                    <option value="">------Select Warehouse------</option>
                                    <c:forEach items="${whList}" var="wh">
                                        <option value="<c:out value="${wh.whId}"/>"><c:out value="${wh.whName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td><input type="button" value="Search" class="btn btn-success" id="search" onclick="searchPage()"></td>
                        </tr>
                    </tbody>
                    </table>
                    <script>
                        document.getElementById("whId").value='<c:out value="${whId}"/>'
                    </script>
                    <table class="table table-info mb30 table-bordered" id="table1" style="font-size:12px">

                        <thead style="font-size:12px">
                            <tr height="40">
                                <th  height="30" >S No</th>
                                <th  height="30" >Warehouse Name</th>
                                <th  height="30" >Lr No</th>
                                <th  height="30" >Lr Date</th>
                                <th  height="30" >SubLr No</th>
                                <th  height="30" >Dealer Name</th>
                                <th  height="30" >Outward Crates</th>
                                <th  height="30" >Inward Crates</th>
                                <th  height="30" >Pending Crates</th>
                                <th  height="30" >Action</th>

                            </tr>
                        </thead>
                        <% int index = 0;
                            int sno = 1;
                            int count = 0;
                        %> 
                        <tbody>

                            <c:forEach items= "${cratesReport}" var="list">
                                <tr height="30">
                                    <td align="left"><%=sno%></td>
                                    <td align="left"   ><c:out value="${list.whName}"/></td>
                                    <td align="left"   ><c:out value="${list.lrNo}"/></td>
                                    <td align="left"   ><c:out value="${list.lrDate}"/></td>
                                    <td align="left"   ><c:out value="${list.sublrNumber}"/></td>
                                    <td align="left"   ><c:out value="${list.dealerName}"/></td>
                                    <td align="left"   ><c:out value="${list.outwardQty}"/></td>
                                    <td align="left"   ><c:out value="${list.inwardQty}"/></td>
                                    <td align="left"   ><c:out value="${list.balanceQty}"/></td>
                                    <td>
                                        <span class="label label-success" style="cursor: pointer" onclick="viewCrates('<c:out value="${list.subLrId}"/>')">View</span>
                                        <br>
                                    </td>
                                </tr> 
                                <%
                                    sno++;
                                    index++;
                                %>
                            </c:forEach>
                        <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                        </tbody>
                    </table>




                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table1");
                    </script>


                    <br/>
                    <br/>

                    <br/>
                    <br/>
                    <br/>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>