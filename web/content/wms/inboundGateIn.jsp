<%-- 
    Document   : grnDetails
    Created on : Jan 21, 2021, 12:47:26 PM
    Author     : throttle
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Gate In Details"  text="GateIn Details"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.GateIn Details"  text="GateIn Details"/></a></li>
            <li class="active">Receive GateIn</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="">
                <form name="saveGtnDetails" method="post">
                <table class="table table-info mb30 table-hover" id="serial">
                            <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>
                        <tr>
                            <td align="center"><text style="color:red">*</text>
                                Invoice No
                            </td>
                            <td>
                                <input type="text" id="invoiceNo" name="invoiceNo" value="" class="form-control" style="width:200px;height:30px" onchange="checkInvoice(this.value);"/> 
                            </td>
                            <td align="center"><text style="color:red">*</text>Invoice Date</td>
                            <td height="30"><input name="invoiceDate" id="invoiceDate" type="text"  class="datepicker , form-control" style="width:200px;height:30px"  onclick="ressetDate(this);" value="" autocomplete="off"></td>
                            
                        </tr>
                        <tr>
                            <td align="center"><text style="color:red">*</text>GateIn Date</td>
                            <td height="30"><input name="gtnDate" id="gtnDate" type="text"  class="datepicker , form-control" style="width:200px;height:30px"  onclick="ressetDate(this);" value="<c:out value="${gtnDate}"/>" autocomplete="off"></td>
                            <td align="center"><text style="color:red">*</text>GateIn Time</td>
                            <td height="30"><input name="gtnTime" id="gtnTime" type="time" style="width:200px;height:30px"  onclick="ressetDate(this);" value="<c:out value="${gtnDate}"/>" autocomplete="off"></td>
                        </tr>
                        <tr>
                        
                            
                            <td align="center"><text style="color:red">*</text>
                                Supplier Plant
                            </td>
                            <td><select id="custId" name="custId" style="width:200px;height:30px">
                              <option value="">---Select One ---</option>    <c:forEach items="${custList}" var="cust">
                                      <option selected value="<c:out value="${cust.custId}"/>"><c:out value="${cust.custName}"/></option>
                            </c:forEach>
                              </select></td>
                               <td align="center"><text style="color:red">*</text>
                                Dock Location
                            </td>
                            <td><select id="dock" name="dock" style="width:200px;height:30px">
                              <option value="">---Select One ---</option>    
                              <option value="1">Dock 1</option> 
                              <option value="2">Dock 2</option>
                              <option value="3">Dock 3</option>
                              <option value="4">Dock 4</option>
                              </select></td>
                              </tr>
                        <tr>
                           
                            <td align="center"><text style="color:red">*</text>
                                Vehicle Type
                            </td>
                            <td>
                                <input type="text" id="vehicleType" name="vehicleType" value="" class="form-control" style="width:200px; height:30px"/> 
                            </td>
                            <td align="center"><text style="color:red">*</text>
                                Vehicle No
                            </td>
                            <td>
                                <input type="text" id="vehicleNo" name="vehicleNo" value="" class="form-control" style="width:200px; height:30px"/> 
                            </td>
                            </tr>
                            <tr>
                                <td align="center">
                                Driver Name
                            </td>
                            <td>
                                <input type="text" id="driverName" name="driverName" value="" class="form-control" style="width:200px; height:30px"/> 
                            </td>
                             <td align="center">
                                Remarks
                            </td>
                            <td>
                                <textarea type="text" id="remarks" name="remarks" value="" class="form-control" style="width:200px; height:40px"/></textarea> 
                            </td>
                        </tr>
                        
                    </table>                 
                                    <table class="table table-info mb30 table-hover sortable" id="addIndent" align="center" width="90%">
                                        <thead >
                                            <tr>
                                                <th>S.No</th>
                                                <th>PO No.</th>  
                                                <th>PO Qty</th>
                                                <th>Pending GRN Qty</th>
                                                <th>GRN Quantity</th>
                                                <th>Delete</th>
                                                
                                            </tr>
                                        </thead>
                                       
                                    </table>

 <script>
    var rowCount = 0;
    var sno = 0;
    var httpRequest;
    var httpReq;
    var rowIndex = 0;
var httpRequest;
    

                     function go1() {
                        if (httpRequest.readyState == 4) {
                        if (httpRequest.status == 200) {
                         var response = httpRequest.responseText;
                            if (response != "") {
                                alert(response);
                                resetTheDetails();
                    
                            } else
                            {
                                
                            }
                                 }
                               }
                            }
                      function checkInvoice(invoiceNo){
                                   var url = "";
                                   if (invoiceNo != "") {
                                       url = "./checkInvoiceExist.do?invoiceNo="+invoiceNo;
                                       
                             if (window.ActiveXObject)
                            {
                            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                            } else if (window.XMLHttpRequest)
                             {           
                             httpRequest = new XMLHttpRequest();
                             }
                              httpRequest.open("POST", url, true);
                            httpRequest.onreadystatechange = function() {
                            go1();
                             };
                            httpRequest.send(null);
                                  }

                               }
                                
    function addRow()
    {


        var tab = document.getElementById("addIndent");
        var iRowCount = tab.getElementsByTagName('tr').length;

        rowCount = iRowCount;
        var newrow = tab.insertRow(rowCount);
//        alert(iRowCount);
        sno = rowCount;
        rowIndex = rowCount;
//        alert("rowIndex:"+rowIndex);
        var cell = newrow.insertCell(0);
        var cell0 = "<td class='text1' height='25' > <input type='hidden'  name='Sno' value='' > " + sno + "</td>";
        cell.innerHTML = cell0;


        cell = newrow.insertCell(1);
        var cell2 = "<td class='text1' height='25'><select class='form-control' maxlength='4' onchange='asnQuantity(this.value,"+ sno +");' name='poNo' id='poNo"+sno+"' style='width:200px;height:40px'><option value='0'>--Select--</option><c:forEach items='${poCode}' var='pro'><option value='<c:out value='${pro.asnId}'/>-<c:out value='${pro.itemId}'/>-<c:out value='${pro.asnQty}'/>-<c:out value='${pro.prevPoQuantity}'/>'><c:out value='${pro.poName}'/></option></c:forEach></select></td>"
        cell.innerHTML = cell2;
        
        cell = newrow.insertCell(2);
        var cell2 = "<td class='text1' height='30'><input type='text'class='form-control' name='asngty' maxlength='4' style='width:130px;height:40px' id='asngty"+ sno +"'  value='' readonly></td>"
        cell.innerHTML = cell2;
        
        cell = newrow.insertCell(3);
        var cell2 = "<td class='text1' height='30'><input type='text'class='form-control' name='prevPoQty' maxlength='4' style='width:130px;height:40px' id='prevPoQty"+ sno +"'  value='' readonly></td>"
        cell.innerHTML = cell2;
        
        cell = newrow.insertCell(4);
        cell2 = "<div id='test1'><td class='text1' height='25'><input type='text'class='form-control' name='poQty' maxlength='4' style='width:130px;height:40px' id='poQty' onchange='checkqty();'  value='0'> </td></div>";
        cell.innerHTML = cell2;
        
        cell = newrow.insertCell(5);
        var cell2 = "<td class='text1' height='25' ><input type='button' class='btn btn-info'   name='delete'  id='delete'   value='Delete Row' onclick='deleteRow(this," + sno + ");' ></td>";
        cell.innerHTML = cell2;

        
        rowIndex++;
        rowCount++;
        sno++;

    }
    var y;
    function asnQuantity(value,sno){
        var c;
        var d;
        var m=0,mn,mx;
        c=document.getElementById("poNo"+sno);
        for(m=1;m<(sno);m++){
            d=document.getElementById("poNo"+m);
            mx=(c.options[c.selectedIndex].text);
            mn=(d.options[d.selectedIndex].text);
           if(mx==mn){
            alert("Already Selected Product");
            document.getElementById("poNo"+sno).value='0';
            }
        }
        var x= value.split('-');
        var y=x[2];
        var z=x[3];
       document.getElementById("asngty"+sno).value=y;
       document.getElementById("prevPoQty"+sno).value=(y-x[3]);
    }
    function checkqty(){
        var z=document.getElementById("poQty").value;
       var m = (document.getElementById("poNo").value).split('-');
        var y=parseInt(m[2]);
        var u=parseInt(m[3]);
     if(z>(y-u)){
        alert("Exceeded Limit");
        document.getElementById("poQty").value='';
    }
}
    function deleteRow(src) {
        sno--;
        var oRow = src.parentNode.parentNode;
        var dRow = document.getElementById('addIndent');
        dRow.deleteRow(oRow.rowIndex);
        document.getElementById("selectedRowCount").value--;
    }
     function submitPage(value) {
         if(document.getElementById("invoiceNo").value==""){
             alert("Enter Invoice No");
             document.getElementById("invoiceNo").focus();
         }else if(document.getElementById("invoiceDate").value==""){
             alert("Enter Invoice Date");
             document.getElementById("invoiceDate").focus();
         }else if(document.getElementById("gtnDate").value==""){
             alert("Enter GateIn Date");
             document.getElementById("gtnDate").focus();
         }else if(document.getElementById("gtnTime").value==""){
             alert("Enter GateIn Time");
             document.getElementById("gtnTime").focus();
         }else if(document.getElementById("custId").value==""){
             alert("Select Supplier Plant");
             document.getElementById("custId").focus();
         }else if(document.getElementById("dock").value==""){
             alert("Enter Dock");
             document.getElementById("dock").focus();
         }else if(document.getElementById("vehicleType").value==""){
             alert("Enter Vehicle Type");
             document.getElementById("vehicleType").focus();
         }else if(document.getElementById("vehicleNo").value==""){
             alert("Enter VehicleNo");
             document.getElementById("vehicleNo").focus();
         }else{
            document.saveGtnDetails.action = '/throttle/inboundGateIn.do?param=Save';
            document.saveGtnDetails.submit();
        }
    }
                </script>
                            
                       <table>
                            <center>
                                <input value="Add Row" class="btn btn-success" id="countRow" type="button" onClick="addRow();" >
                                &emsp;<input type="button" value="Save" id="insert" class="btn btn-success" onClick="submitPage(this.value);">
                                &emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">
                            </center>

                        </table>
               </form>
        </body>
                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
                   
        </div>
    </div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

