<%-- 
    Document   : connectPicklistExcel
    Created on : 28 Aug, 2021, 12:51:07 PM
    Author     : Roger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Excel";
        request.setAttribute("menuPath", menuPath);
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "ConnectPickList-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test = "${getDispatchDetails != null}" >
                <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                    <thead>
                        <tr height="40">
                            <th  height="30" >S No</th>
                            <th  height="30" >Vendor Name</th>
                            <th  height="30" >vehicle Type</th>
                            <th  height="30" >Dispatch Number</th>
                            <th  height="30" >Planned Date</th>
                            <th  height="30" >Total Quantity</th>
                            <th height="30">Action</th>
                        </tr>
                    </thead>
                    <% int index = 0;
                        int sno = 1;
                    %>
                    <tbody>
                        <tr height="30">
                        <c:forEach items="${getDispatchDetails}" var="list">
                        <td align="left"><%=sno%></td>
                        <td align="left"   ><c:out value="${list.vendorName}"/></td>
                        <td align="left"   ><c:out value="${list.typeName}"/></td>
                        <td align="left"   ><c:out value="${list.dispatchNo}"/></td>
                        <td align="left"   ><c:out value="${list.plannedDate}"/></td>
                        <td align="left"   ><c:out value="${list.qty}"/></td>

                        </tr>
                        <%index++;%>
                        <%sno++;%>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>

            <br>
            <br>

        </form>
    </body>
</html>