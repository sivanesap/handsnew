<%-- 
    Document   : dzGrnUploadView
    Created on : 13 Dec, 2021, 1:14:34 PM
    Author     : alan
--%>




<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript"></script>

    <script>
        function insertInstaMart() {
            var skuCode = document.getElementById("skuCode").value;
            var productName = document.getElementById("productName").value;
            var productDescription = document.getElementById("productDescription").value;
            var minimumStockQty = document.getElementById("minimumStockQty").value;
            var uom = document.getElementById("uom").value;
            var temperature = document.getElementById("temperature").value;
            var storage = document.getElementById("storage").value;
            var activeInd = document.getElementById("activeInd").value;
            if (activeInd != "0") {

                console.log(skuCode, productName, productDescription, minimumStockQty, uom, temperature, storage, activeInd);
                if (skuCode != "" && productName != "" && productDescription != "" && minimumStockQty != "" && uom != "" && temperature != "" && storage != "" && activeInd != "")
                {
                    document.instaMart.action = "/throttle/instaMartProductMaster.do?param=save";
                    document.instaMart.submit();
                }

                else {
                    alert("Enter The Value");
                }
            } else {
                alert("Please Select Active Not Active ");
            }
        }


        function exportInward() {
          
                window.open('/throttle/dzGrnUploadView.do?param=export');
         
        }

        function printInward() {
          
                window.open('/throttle/dzGrnUploadView.do?param=print');
         
        }


//        function uploadPage() {
//            document.upload.action = "/throttle/dzProductUpload.do?param=upload";
//            document.upload.submit();
//
//        }

        function updateInstaMart(sno, productName, skuCode, productDescription, minimumStockQty, uom, temperature, storage, activeInd, productId) {

            document.getElementById("productName").value = productName;
            document.getElementById("skuCode").value = skuCode;
            document.getElementById("productDescription").value = productDescription;
            document.getElementById("minimumStockQty").value = minimumStockQty;
            document.getElementById("uom").value = uom;
            document.getElementById("temperature").value = temperature;
            document.getElementById("storage").value = storage;
            document.getElementById("activeInd").value = activeInd;
            document.getElementById("productId").value = productId;


            var count = parseInt(document.getElementById("count").value);
            for (i = 1; i <= parseInt(count); i++) {
                if (i != sno) {
                    document.getElementById("edit" + i).checked = false;
                } else {
                    document.getElementById("edit" + i).checked = true;
                }
            }
        }


        function insertTo() {


            var result = confirm("Some errors are not Updated. Do you want to continue?");
            if (result == true) {
                document.upload.action = "/throttle/dzInsertIntoProductMaster.do?";
                document.upload.method = "post";
                document.upload.submit();
            }
            else {

                document.upload.action = "/throttle/dzInsertIntoProductMaster.do?";
                document.upload.method = "post";
                document.upload.submit();
            }
        }




    </script>
                
    <script>
      $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
  
</script>
    <script>
        function openView(grnId) {
            window.open('/throttle/dzGrnUploadView.do?param=view&grnId=' + grnId, 'PopupPage', 'height = 400, width = 800, scrollbars = yes, resizable = yes');
        }
    </script>


                <script>
                    function setWarehouse(){
                        var warehouseId = document.getElementById("whId").value;
                        document.getElementById("id").value = warehouseId ;
                    }
                    
                    
                    
                    function submitPage(){
                        var id = document.getElementById("whId").value;
                        document.material.action = '/throttle/dzGrnUploadView.do?param=save' ;
                        document.material.submit();
                     }
                     
                    function searchPage(){
                        var id = document.getElementById("whId").value;
                        document.material.action = '/throttle/dzGrnUploadView.do'
                        document.material.submit();
                     }
                     
                     
                           
                    function exportPage(){
                        var id = document.getElementById("whId").value;
                        document.material.action = '/throttle/dzGrnUploadView.do?param=export' ;
                        document.material.submit();
                     }
                    
                </script>   


    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Grn Upload View</h2>

        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Shop Tree</a></li>
                <li class="">Grn Upload View</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <form name="material"  method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    <table class="table table-info mb30 table-hover" id="table" style="font-size:12px">
                        <thead><tr><th colspan="4">Invoice Details</th></tr></thead>
                    <tbody>
                        <tr>
                            <td>From Date</td>
                            <td>
                                <input type="text" autocomplete="off" class="form-control datepicker" style="width:240px;height:40px" id="fromDate" name="fromDate" value="<c:out value="${fromDate}"/>">
                            </td>
                            <td>To Date</td>
                            <td>
                                <input type="text" autocomplete="off" class="form-control datepicker" style="width:240px;height:40px" id="toDate" name="toDate" value="<c:out value="${toDate}"/>">
                            </td>
                        </tr>
                        <tr style="display:none">
                            <td>Select Warehouse</td>
                            <td>
                                <select id="whId" name="whId" style="width:240px;height:40px" class="form-control">
                                    <option value="">------Select Warehouse------</option>
                                    <c:forEach items="${getWareHouseList}" var="wh">
                                        <option value="<c:out value="${wh.warehouseId}"/>"><c:out value="${wh.whName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
<!--                            <td><input type="button" value="Search" class="btn btn-success" id="search" onclick="searchPage()"></td>-->
                        </tr>
                    </tbody>
                    </table>
                    <script>
                        document.getElementById("whId").value='<c:out value="${whId}"/>'
                    </script>
             
                    <center>

                        <input type="button" class="btn btn-success" name="Search"  id="Search"  value="Search" onclick="searchPage()">&nbsp;&nbsp;
                        <input type="button" class="btn btn-success" name="Export"  id="Export"  value="Export" onclick="exportPage()">&nbsp;&nbsp;

                    </center>
                    <br>
                    <br>
                    <div id="ptp">
                        <div class="inpad" style=" overflow-x: visible;">

                            <c:if test = "${getdzGrnUpload != null}" >

                                <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                    <thead style="font-size:12px">
                                        <tr height="40">
                                            <th>Sno</th>
                                            <th>Supplier Name</th>
                                            <th>Grn No</th>
                                            <th>Grn Date</th>
                                            <th>Grn Completed Date</th>
                                            <th>Po No</th>
                                            <th>Order Qty</th>
                                            <th>Received Qty</th>
                                            <th>Diff  Qty</th>
                                            <th>Total Price</th>
                                            <th>Total Tax</th>
                                            <th>Total Ordered</th>
                                            <th>Total Received</th>
                                            <th>Total Diff</th>
                                            <th>Status</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                     int count=0;
                                    %> 
                                    <tbody>

                                        <c:forEach items="${getdzGrnUpload}" var="pc">


                                            <tr>       
                                                <td align="left"> <%=sno%> </td>
                                                <td align="left"> <c:out value="${pc.supplierName}"/></td>
                                                 <td align="left"> <c:out value="${pc.grnNo}"/></td>
                                                <td align="left"> <c:out value="${pc.date}"/></td>
                                                <td align="left"> <c:out value=""/></td>
                                                <td align="left"> <c:out value="${pc.poNo}"/></td>
                                                <td align="left"> <c:out value="${pc.orderedQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.receivedQty}"/></td>
                                                <td align="left"> <c:out value="${pc.diffQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.price}"/> </td>
                                                <td align="left"> <c:out value="${pc.taxValue}"/> </td>
                                                <td align="left"> <c:out value="${pc.totalOrderedQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.totalReceivedQty}"/> </td>
                                                <td align="left"> <c:out value="${pc.totalDiffQty}"/> </td>
                                                  <c:if test="${pc.status == 0}">
                                                    <td align="left" style="color: blue"> Pending </td> 
                                                </c:if>
                                                <c:if test="${pc.status == 1}">
                                                    <td align="left" style="color: yellow" > On Progress </td> 
                                                </c:if>
                                                <c:if test="${pc.status == 2}">
                                                    <td align="left" style="color: green"> Completed </td> 
                                                </c:if>
                                                
                                                <td align="left"> <a onclick="openView('<c:out value="${pc.grnId}"/>')">View</a></td>






                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                    </tbody>
                                </table>

                                <center>
                                    <td colspan="0"><input type="button" class="btn btn-success" value="Print" name="print" id="print" onclick="printInward();"></td>
                                    <td colspan="0"><input type="button" class="btn btn-success" value="Export" name="export" id="export" onclick="exportInward();"></td>
                                </center>
                                    
                            </c:if>
                        </div>
                    </div>     

                </form>







                <br>


            </div>
        </div>
    </div>
    <%@include file="/content/common/NewDesign/settings.jsp"%>
</html>
