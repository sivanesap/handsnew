<%-- 
    Document   : dzOtherStockUpload
    Created on : 17 Jan, 2022, 2:25:25 PM
    Author     : alan
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript"></script>

    <script>
        
        function uploadPage() {
            document.upload.action = "/throttle/dzGrnUpload.do?param=upload";
            document.upload.submit();

        }

        function insertTo() {
            var result = confirm("Do you want to continue?");
            if (result == true) {
                document.upload.action = "/throttle/dzInsertGrnMaster.do?param=save";
                document.upload.method = "post";
                document.upload.submit();
            }
        }




    </script>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Other Stock Upload</h2>

        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Dunzo</a></li>
                <li class="">Other Stock Upload</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <form name="upload"  method="post" enctype="multipart/form-data">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    <table class="table table-info mb30 table-hover" id="uploadTable"  >
                        <thead> 
                            <tr>
                                <th  colspan="10">Other Stock Upload</th>
                            </tr>

                        </thead>


                        <tr>
                            <td>Select Excel</td>
                            <td><input type="file" name="importCnote" id="importCnote"  class="importCnote"><font size="1"><u><a href="uploadedxls/dzGrn.xlsx" download>sample file</a></u></font></td>                             
                            <td olspan="0"><input type="button" class="btn btn-success" value="Upload" name="UploadPage" id="UploadPage" onclick="uploadPage();"></td>
                        </tr>

                    </table>
                    <div id="ptp">
                        <div class="inpad" style=" overflow-x: visible;">

                            <c:if test = "${getdzGrnDetailsTemp != null}" >

                                <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                    <thead style="font-size:12px">
                                        <tr height="40">
                                            <th>Sno</th>
                                            <th>Category</th>
                                            <th>Product ID</th>
                                            <th>Product Variant ID</th>
                                            <th>Product Full Name</th>
                                            <th>Qty</th>
                                            


                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                     int count=0;
                                    %> 
                                    <tbody>

                                        <c:forEach items="${getdzGrnDetailsTemp}" var="pc">


                                            <tr>
                                                <td align="left"> <%=sno%> </td>
                                                <td align="left"> <c:out value="${pc.productLineEntryId}"/></td>
                                                <td align="left"> <c:out value="${pc.categoryName}"/> </td>
                                                <td align="left"> <c:out value="${pc.productId}"/></td>
                                                <td align="left"> <c:out value="${pc.name}"/> </td>
                                                <td align="left"> <c:out value="${pc.orderedQty}"/> </td>
                                            

                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                    </tbody>
                                </table>

                                <center>

                                    <td colspan="0"><input type="button" class="btn btn-success" value="Submit" name="submitPage" id="submitPage" onclick="insertTo();"></td>
                                </center>
                            </c:if>
                        </div>
                    </div>     

                </form>

                <br>
            </div>
        </div>
    </div>
    <%@include file="/content/common/NewDesign/settings.jsp"%>
</html>