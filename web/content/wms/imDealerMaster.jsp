
<%-- 
    Document   : imDealerMaster
    Created on : 11 Dec, 2021, 12:35:53 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


    <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
    <script language="javascript" src="/throttle/js/validate.js"></script>  
    <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
    <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
    <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
    <script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
    <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="ajaxrequest.js"></script>
    <script type="text/javascript"></script>


    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <script>
        function insertDealerMaster() {
            if (document.getElementById("dealerName").value == "") {
                alert("Enter Dealer Name");
                document.getElementById("dealerName").focus();
            } else if (document.getElementById("dealerCode").value == "") {
                alert("Enter Dealer Code");
                document.getElementById("dealerCode").focus();
            } else if (document.getElementById("industries").value == "") {
                alert("Enter Industries");
                document.getElementById("industries").focus();
//            } else if (document.getElementById("district").value == "") {
//                alert("Enter District");
//                document.getElementById("district").focus();
//            } else if (document.getElementById("street").value == "") {
//                alert("Enter Street");
//                document.getElementById("street").focus();
            } else if (document.getElementById("address").value == "") {
                alert("Enter Address");
                document.getElementById("address").focus();
//            } else if (document.getElementById("address2").value == "") {
//                alert("Enter Address2");
//                document.getElementById("address2").focus();
            } else if (document.getElementById("city").value == "") {
                alert("Enter City");
                document.getElementById("city").focus();
            } else if (document.getElementById("pincode").value == "") {
                alert("Enter Pincode");
                document.getElementById("pincode").focus();
            } else if (document.getElementById("phone1").value == "") {
                alert("Enter Phone1");
                document.getElementById("phone1").focus();
//            } else if (document.getElementById("phone2").value == "") {
//                alert("Enter Phone2");
//                document.getElementById("phone2").focus();
            } else if (document.getElementById("gstNo").value == "") {
                alert("Enter GST No");
                document.getElementById("gstNo").focus();
            } else if (document.getElementById("customerName").value == "") {
                alert("Enter Customer Name");
                document.getElementById("customerName").focus();
            } else if (document.getElementById("customerPhone").value == "") {
                alert("Enter Customer Phone");
                document.getElementById("customerPhone").focus();
//            } else if (document.getElementById("activeInd").value == "") {
//                alert("Enter ActiveInd  ");
//                document.getElementById("activeInd").focus();
            } else {

                document.dealer.action = "/throttle/imDealerMaster.do?param=save";
                document.dealer.submit();
            }
        }


        function updateDealerMaster(sno, elem, dealerName, dealerCode, industries, district, street, address, address2, city, pincode, phone1, phone2, gstNo, customerName, customerPhone, activeInd, dealerId) {
            if (elem.checked) {
                document.getElementById("dealerName").value = dealerName;
                document.getElementById("dealerCode").value = dealerCode;
                document.getElementById("industries").value = industries;
                document.getElementById("district").value = district;
                document.getElementById("street").value = street;
                document.getElementById("address").value = address;
                document.getElementById("address2").value = address2;
                document.getElementById("city").value = city;
                document.getElementById("pincode").value = pincode;
                document.getElementById("phone1").value = phone1;
                document.getElementById("phone2").value = phone2;
                document.getElementById("gstNo").value = gstNo;
                document.getElementById("customerName").value = customerName;
                document.getElementById("customerPhone").value = customerPhone;
                document.getElementById("activeInd").value = activeInd;
                document.getElementById("dealerId").value = dealerId;
                var count = parseInt(document.getElementById("count").value);
                for (var i = 1; i <= parseInt(count); i++) {
                    if (i != sno) {
                        document.getElementById("edit" + i).checked = false;
                    } else {
                        document.getElementById("edit" + i).checked = true;
                    }
                }
            } else {
                document.getElementById("dealerId").value = "";
            }
        }




    </script>


    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Dealer Master</h2>

        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Instamart</a></li>
                <li class="">Dealer Master</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <form name="dealer" method="post">
                    <%@include file = "/content/common/message.jsp"%>
                    <table class="table table-info mb30 table-hover" style="width:70%">
                        <input type="hidden" name="dealerId" id="dealerId" value=""/>
                        <tr>
                            <td><font color="red">*</font>Dealer Name : &emsp;</td>
                            <td>
                                <input type="text" name="dealerName" class="form-control" style="width:240px;height:40px" id="dealerName" value="">
                            </td>
                            <td><font color="red">*</font>Dealer Code : &emsp;</td>
                            <td>
                                <input type="text" name="dealerCode" class="form-control"  style="width:240px;height:40px" id="dealerCode" value="">
                            </td>
                        </tr>

                        <tr>
                            <td> <font color="red">*</font>Industries : &emsp;</td>
                            <td>
                                <input type="text" name="industries" class="form-control" style="width:240px;height:40px" id="industries" value="">
                            </td>
                            <td>District : &emsp;</td>
                            <td>
                                <input type="text" name="district" class="form-control" style="width:240px;height:40px" id="district" value="">
                            </td>
                        </tr>
                        <tr>
                            <td>Street : &emsp;</td>
                            <td>
                                <input type="text" name="street" class="form-control" style="width:240px;height:40px" id="street" value="">
                            </td>
                            <td><font color="red">*</font>Address : &emsp;</td>
                            <td>
                                <input type="text" name="address" id="address"  style="width:240px;height:40px;"  class="form-control" value="">
                            </td>
                        </tr>

                        <tr>
                            <td>Address 2:&emsp;</td>
                            <td>
                                <input type="text" id="address2" name="address2" style="width:240px;height:40px;" class="form-control"  value="">
                            </td>
                            <td> City :  &emsp;</td>
                            <td><input type="text" name="city" id="city" style="width:240px;height:40px;" class="form-control"  value=""></td>

                        </tr>

                        <tr>
                            <td><font color="red">*</font> Pincode : &emsp;</td>
                            <td><input type="text" name="pincode" id="pincode" style="width:240px;height:40px;"  class="form-control" value="" ></td>
                            <td><font color="red">*</font>Phone No : &emsp;</td>
                            <td><input type="text" name="phone1" id="phone1" style="width:240px;height:40px;"  class="form-control" value="" ></td>
                        </tr>
                        <tr>
                            <td>Alternate Phone : &emsp;</td>
                            <td><input type="text" name="phone2" id="phone2" style="width:240px;height:40px;"  class="form-control" value="" ></td>
                            <td><font color="red">*</font>GST No: &emsp;</td>
                            <td><input type="text" name="gstNo" id="gstNo" style="width:240px;height:40px;"  class="form-control" value="" ></td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Customer Name: &emsp;</td>
                            <td>
                                <input type="text" name="customerName" id="customerName" style="width:240px;height:40px;"  class="form-control" value="" >
                            </td>
                            <td><font color="red">*</font>Customer Phone: &emsp;</td>
                            <td>
                                <input type="text" name="customerPhone" id="customerPhone" style="width:240px;height:40px;"  class="form-control" value="" >
                            </td>
                        </tr>
                        <tr>
                            <td>Active Ind:    &emsp;</td>
                            <td>
                                <select type="text" name="activeInd" id="activeInd" style="width:240px;height:40px;"  class="form-control">
                                    <option value="Y">Active</option>
                                    <option value="N">In-Active</option>
                                </select>
                            </td>
                        </tr>

                    </table>
                    <center>
                        <input type="button" class="btn btn-success" value="Save" name="Submit" onClick="insertDealerMaster()">
                    </center>

                    <br>
                    <table class="table table-info mb30 table-hover" id="table" style="width:100%" > 
                        <thead>

                            <tr height="30" style="color: white">
                                <th>Sno</th>
                                <th>Dealer Name</th>
                                <th>Dealer Code</th>
                                <th>Industries</th>
                                <th>District</th>
                                <th>Street</th>
                                <th>Address</th>
                                <th>Address2</th>
                                <th>City</th>
                                <th>Pincode</th>
                                <th>Phone</th>
                                <th>Alternate Phone</th>
                                <th>GST No</th>
                                <th>Customer Name</th>
                                <th>Customer Phone</th>
                                <th>Status</th>
                                <th>Select</th>
                            </tr>
                        </thead>
                        <% int sno1 = 1;%>
                        <tbody>
                            <c:if test = "${instaMartDealerMaster != null}">
                                <c:forEach items="${instaMartDealerMaster}" var="pc">


                                    <tr>
                                        <td  align="left"> <%=sno1%> </td>
                                        <td  align="left"> <c:out value="${pc.dealerName}" /></td>
                                        <td align="left"> <c:out value="${pc.dealerCode}" /></td>
                                        <td align="left"> <c:out value="${pc.industries}" /></td>
                                        <td align="left"> <c:out value="${pc.district}" /></td>
                                        <td align="left"> <c:out value="${pc.street}" /></td>
                                        <td align="left"> <c:out value="${pc.address}" /></td>
                                        <td align="left"> <c:out value="${pc.address1}" /></td>
                                        <td align="left"> <c:out value="${pc.city}" /></td>
                                        <td align="left"> <c:out value="${pc.pincode}" /></td>
                                        <td align="left"> <c:out value="${pc.mobileNo}" /></td>
                                        <td align="left"> <c:out value="${pc.phoneNumber}" /></td>
                                        <td align="left"> <c:out value="${pc.gstNo}" /></td>
                                        <td align="left"> <c:out value="${pc.customerName}" /></td>
                                        <td align="left"> <c:out value="${pc.mobile}" /></td>
                                        <td align="left">
                                            <c:if test="${pc.activeInd=='Y'}">
                                                Active
                                            </c:if>
                                            <c:if test="${pc.activeInd=='N'}">
                                                In-Active
                                            </c:if>
                                        </td>
                                        <td>
                                            <input type="checkbox" value="save" id="edit<%=sno1%>" onclick="updateDealerMaster(<%= sno1%>, this, '<c:out value="${pc.dealerName}" />', '<c:out value="${pc.dealerCode}" />', '<c:out value="${pc.industries}" />', '<c:out value="${pc.district}" />', '<c:out value="${pc.street}" />', '<c:out value="${pc.address}" />', '<c:out value="${pc.address1}" />', '<c:out value="${pc.city}" />', '<c:out value="${pc.pincode}" />', '<c:out value="${pc.mobileNo}" />', '<c:out value="${pc.phoneNumber}" />', '<c:out value="${pc.gstNo}" />', '<c:out value="${pc.customerName}" />', '<c:out value="${pc.mobile}" />', '<c:out value="${pc.activeInd}"/>', '<c:out value="${pc.dealerId}"/>');" />
                                        </td>
                                    </tr>
                                    <%sno1++;%>
                                </c:forEach>
                            </tbody>
                        </c:if>
                    </table>

                </form>
            </div>
        </div>
    </div>

    <input type = "hidden" name="count" id="count" value="<%=sno1%>"/>
    <%@include file="/content/common/NewDesign/settings.jsp"%>
</html>