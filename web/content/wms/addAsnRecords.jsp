<%-- 
    Document   : addAsnRecords.jsp
    Created on : Mar 10, 2021, 1:14:56 PM
    Author     : throttle
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Gate In Details"  text="GateIn Details"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Inbound</a></li>
            <li class="active">ASN Details</li>
        </ol>
    </div>
</div>
           
            <div class="contentpanel">
                <div class="panel panel-default">
                    <div class="panel-body">
                         <%@ include file="/content/common/message.jsp" %>
                         <body>
                              <form name="saveAsnDetails" method="multipart">
                                  
                                  <table class="table table-info mb30 table-hover" id="serial">
                                      <div align="center" style="height:15px;" id="statusMsg">&nbsp;&nbsp
                                          
                                      </div>
                                      <tr>
                                          <td><text style="color:red">*</text>
                                              PO Number
                                          </td>
                                          <td>
                                              <input style="width:200px;height:30px" type="text" class="form-control" id="poNumber" name="poNumber"/>
                                          </td>
                                     
                                          <td><text style="color:red">*</text>
                                              PO Request Id
                                          </td>
                                          <td>
                                              <input style="width:200px;height:30px" type="text" class="form-control" id="PoRequestId" name="PoRequestId"/>
                                          </td>
                                      </tr>
                                       <tr>
                                          <td><text style="color:red">*</text>
                                              PO Date
                                          </td>
                                          <td>
                                              <input style="width:200px;height:30px" type="text" class="datepicker form-control" name="poDate" id="poDate" onclick="ressetDate(this);"/>
                                          </td>
                                     
                                          <td><text style="color:red">*</text>
                                             Vendor Name
                                          </td>
                                          <td>
                                              
                                              <select  style="width:200px;height:45px" type="text" class="form-control" id="vendorId" name="vendorId">
                                                  <option value="">-----------select-----------</option>
                                                  <c:forEach items="${vendorList}" var="list">
                                                      <option value="<c:out value="${list.vendorId}"/>">
                                                          <c:out value="${list.vendorName}"/></option>
                                                          
                                                      
                                                      
                                                  </c:forEach>
                                                 
                                                      
                                                  
                                              </select>
                                          </td>
                                      </tr>
                                      
                                       <tr>
                                          <td><text style="color:red">*</text>
                                              SKU Code
                                          </td>
                                          <td>
                                              <select  style="width:200px;height:45px" type="text" class="form-control" id="itemId" name="itemId">
                                                  <option value="">-----------select-----------</option>
                                                  <c:forEach items="${getItemList}" var="item">
                                                      <option value="<c:out value="${item.itemId}"/>">
                                                          <c:out value="${item.itemName}"/></option>
                                                          
                                                      
                                                      
                                                  </c:forEach>
                                                 
                                                      
                                                  
                                              </select>
                                          </td>
                                     
                                          <td><text style="color:red">*</text>
                                             Warehouse
                                          </td>
                                          <td>
                                               <select  style="width:200px;height:45px" type="text" class="form-control" id="warehouseId" name="warehouseId">
                                                  <option value="">-----------select-----------</option>
                                                  <c:forEach items="${getWareHouseList}" var="war">
                                                      <option value="<c:out value="${war.warehouseId}"/>">
                                                          <c:out value="${war.whName}"/></option>
                                                          
                                                      
                                                      
                                                  </c:forEach>
                                                 
                                                      
                                                  
                                              </select>
                                          </td>
                                      </tr>
                                       <tr>
                                          <td><text style="color:red">*</text>
                                              Quantity
                                          </td>
                                          <td>
                                              <input style="width:200px;height:30px" type="text" class="form-control" id="Quantity" name="Quantity"/>
                                          </td>
                                   
                                          <td><text style="color:red">*</text>
                                              Remarks
                                          </td>
                                          <td>
                                              <input style="width:200px;height:30px" type="text" class="form-control" id="remark" name="remark"/>
                                          </td>
                                      </tr>
                                      
                                  </table>
                                  
                                  <table>
                                      <center>
                                          <input type="button" value="save" id="insert" class="btn btn-success" onclick="submitPage();"/>
                                      </center>
                                  </table>
                                   </form>
                             
                             <script>
                                 function submitPage(){
                                     
                                   document.saveAsnDetails.action='/throttle/updateAnsRecord.do?param=save';
                                    document.saveAsnDetails.submit();
                                     
                                     
                                 }
                             </script>
                             
                             
                         </body>
                       
                        
                    </div>
                         <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
                </div>
            </div>
                <%@ include file="/content/common/NewDesign/settings.jsp" %>

