<%-- 
    Document   : connectPicklist
    Created on : 3 Aug, 2021, 1:51:24 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
//alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script type="text/javascript">
    function uploadContract() {
        document.upload.action = "/throttle/uploadconnectInvoice.do";
        document.upload.method = "post";
        document.upload.submit();
    }

    function uploadPage() {
        if (document.getElementById("countStatus").value > 0) {
            var result = confirm("Some errors are not Updated. Do you want to continue?");
            if (result == true) {
                document.upload.action = "/throttle/connectUploadInvoice.do?&param=save";
                document.upload.method = "post";
                document.upload.submit();
            }
        } else {
            document.upload.action = "/throttle/connectUploadInvoice.do?&param=save";
            document.upload.method = "post";
            document.upload.submit();
        }
    }
    
    function searchPage(param){
        document.upload.action="/throttle/connectPicklist.do?&param="+param;
        document.upload.submit();
    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="PickList Generate"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Connect" text="HS Connect"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="PickList Generate"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form name="upload"  method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>



                    <div id="ptp">
                        <div class="inpad" style=" overflow-x: visible;">
                            <table class="table table-info table-hover">
                                <tr>
                                    <td>From Date</td>
                                    <td><input type="text" autocomplete="off" class="datepicker form-control" style="width:240px;height:40px" name="fromDate" id="fromDate" value="<c:out value="${fromDate}"/>"/></td>
                                    <td>To Date</td>
                                    <td><input type="text" autocomplete="off" class="datepicker form-control" style="width:240px;height:40px" name="toDate" id="toDate" value="<c:out value="${toDate}"/>"/></td>
                                </tr>
                            </table>
                            <center>
                                <input type="button" class="btn btn-success" name="search" value="Search" onclick="searchPage('search')">&nbsp;&nbsp;
                                <input type="button" class="btn btn-success" name="exportExcel" value="Export Excel" onclick="searchPage('excel')">
                            </center><br><br>
                            <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                <thead style="font-size:12px">
                                    <tr height="40">
                                        <th  height="30" >S No</th>
                                        <th  height="30" >Vendor Name</th>
                                        <th  height="30" >vehicle Type</th>
                                        <th  height="30" >Dispatch Number</th>
                                        <th  height="30" >Planned Date</th>
                                        <th  height="30" >Total Quantity</th>
                                        <th height="30">Action</th>
                                    </tr>
                                </thead>
                                <% int index = 0; 
                                 int sno = 1;
                                 int count=0;
                                %> 
                                <tbody>
                                        <c:forEach items= "${getDispatchDetails}" var="list">
                                                <tr height="30">
                                                    <td align="left"><%=sno%></td>
                                                    <td align="left"   ><c:out value="${list.vendorName}"/></td>
                                                    <td align="left"   ><c:out value="${list.typeName}"/></td>
                                                    <td align="left"   ><c:out value="${list.dispatchNo}"/></td>
                                                    <td align="left"   ><c:out value="${list.plannedDate}"/></td>
                                                    <td align="left"   ><c:out value="${list.qty}"/></td>
                                                    <td>
                                                        <a href="/throttle/connectPicklist.do?param=generate&dispatchId=<c:out value="${list.dispatchId}"/>">View Details</a>
                                                    </td>

                                                </tr> 
                                                <%
                                                sno++;
                                                index++;
                                                %>
                                        </c:forEach>
                                    <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                </tbody>
                            </table>

                        </div>
                    </div>     

                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>


