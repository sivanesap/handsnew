<%-- 
    Document   : dzGrnInvoiceViewExport
    Created on : 1 Feb, 2022, 5:44:34 PM
    Author     : alan
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Excel";
        request.setAttribute("menuPath", menuPath);
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "dzGrnDetailsExportExcel-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <table class="table table-info mb30 table-bordered" id="table" class="sortable" width="100%">



                    

                        <thead style="font-size:12px">
                            <tr height="40">
                                <th>Sno</th>
                                <th>Product Line Entry Id</th>
                                <th>Category</th>
                                <th>Product Id</th>
                                <th>Product Full Name</th>
                                <th>Ordered Qty</th>
                                <th>Received Qty</th>
                                <th>Difference Qty</th>
                                <th>Price</th>
                                <th>Tax</th>
                                <th>PO No</th>
                                <th>Ordered Total</th>
                                <th>Received Total</th>
                                <th>Diff Total</th>
                                <th>Tax Name</th>
                                <th>Bill To Location Id</th>
                                <th>Ship To Location Id</th>
                                <th>Bill To Location Name</th>
                                <th>Ship To Location Name</th>
                                <th>PO Date</th>
                                <th>PO Reference Number</th>
                                <th>PO Supplier Id</th>
                                <th>PO Supplier Name</th>
                                <th>PO Ordered Qty</th>
                                <th>PO Received Qty</th>
                                <th>Bin</th>


                            </tr>
                        </thead>
                        <% int index = 0;
                            int sno = 1;
                            int count = 0;
                        %> 
                        <tbody>

                            <c:forEach items="${getDzGrnDetailsExport}" var="pc">


                                <tr>
                                    <td align="left"> <%=sno%> </td>
                                    <td align="left"> <c:out value="${pc.productLineEntryId}"/></td>
                                    <td align="left"> <c:out value="${pc.categoryName}"/> </td>
                                    <td align="left"> <c:out value="${pc.productId}"/></td>
                                    <td align="left"> <c:out value="${pc.name}"/> </td>
                                    <td align="left"> <c:out value="${pc.orderedQty}"/> </td>
                                    <td align="left"> <c:out value="${pc.receivedQty}"/> </td>
                                    <td align="left"> <c:out value="${pc.diffQty}"/> </td>
                                    <td align="left"> <c:out value="${pc.price}"/> </td>
                                    <td align="left"> <c:out value="${pc.taxValue}"/></td>
                                    <td align="left"> <c:out value="${pc.poNo}"/></td>
                                    <td align="left"> <c:out value="${pc.totalOrderedQty}"/> </td>
                                    <td align="left"> <c:out value="${pc.totalReceivedQty}"/></td>
                                    <td align="left"> <c:out value="${pc.totalDiffQty}"/> </td>
                                    <td align="left"> <c:out value="${pc.taxVol}"/> </td>
                                    <td align="left"> <c:out value="${pc.billToLocationId}"/> </td>
                                    <td align="left"> <c:out value="${pc.shipToLocationId}"/> </td>
                                    <td align="left"> <c:out value="${pc.billToLocationName}"/> </td>
                                    <td align="left"> <c:out value="${pc.shipToLocationName}"/></td>
                                    <td align="left"> <c:out value="${pc.poDate}"/> </td>
                                    <td align="left"> <c:out value="${pc.referenceNo}"/></td>
                                    <td align="left"> <c:out value="${pc.supplierId}"/> </td>
                                    <td align="left"> <c:out value="${pc.supplierName}"/> </td>
                                    <td align="left"> <c:out value="${pc.poOrderedQty}"/> </td>
                                    <td align="left"> <c:out value="${pc.poReceivedQty}"/> </td>
                                    <td align="left"> <c:out value="${pc.binName}"/> </td>

                                </tr> 
                                <%
                                    sno++;
                                    index++;
                                %>
                            </c:forEach>
                                
                        <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                        </tbody>
                 


            </table>


            <br>
            <br>

        </form>
    </body>
</html>