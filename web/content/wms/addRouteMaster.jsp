<%-- 
    Document   : addRouteMaster
    Created on : Feb 16, 2021, 4:46:05 PM
    Author     : hp
--%>


<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

 <script>
                        function checkAll() {
                            var inputs = document.getElementsByName("selectedIndex");
                            for (var i = 0; i < inputs.length; i++) {
                                if (inputs[i].type == "checkbox") {
                                    if (inputs[i].checked == true) {
                                        inputs[i].checked = false;
                                    } else if (inputs[i].checked == false) {
                                        inputs[i].checked = true;
                                    }
                                }
                            }
                        }
                    </script>
                    
                      
                
                <script>
                 function setWare(value){
                                  var val=value;
                                  document.getElementById("stateId").value=val;
                                  var stateId1=val;
                               $.ajax({
                                        url: "/throttle/getWareHouse.do",
                                        dataType: "json",
                                        data: {
                                            item: stateId1
                                        },
                                        success: function(data) {
                                            if (data != '') {
                                                $('#whId').empty();
                                                $('#whId').append(
                                                        $('<option></option>').val(0).html('--select--'))
                                                $.each(data, function(i, data) {
                                                    $('#whId').append(
                                                            $('<option style="width:90px"></option>').attr("value", data.Id).text(data.Name)
                                                            )
                                                });
                                            } else {
                                                $('#whId').empty();
                                                $('#whId').append(
                                                        $('<option></option>').val(0).html('--select--'))
                                            }
                                        }
                                    });
                              }
                              </script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Route Master"  text="Route Master"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Route Master"  text="Master"/></a></li>
            <li class="active">Route Master</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="setValues(<c:out value="${stateId}"/>,<c:out value="${whId}"/>);">
                <form name="pincode" method="post">
                <table class="table table-info mb30 table-hover" id="pincode">
                            <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>
                    <thead><tr>
                    <th class="contenthead" colspan="8">Route Master</th>
                          </tr></thead>  
                        <tr>
                           <td align="center"><br><text style="color:red">*</text>
                                State 
                            </td>
                            <td><br><select id="stateId" name="stateId" onchange="setWare(this.value);"  style="width:200px;height:30px">
                              <option value="">---Select One ---</option>    <c:forEach items="${stateList}" var="st">
                                      <option value="<c:out value="${st.stateId}"/>"><c:out value="${st.stateName}"/></option>
                            </c:forEach>
                                </select></td>
                    <td align="center"><br><font color="red">*</font>Warehouse Name</td>
                    <td ><br>
                        <select style="width:200px;height:30px" name="whId" id="whId" onchange="searchPage();">
                            <option value="">---Select One ---</option>    <c:forEach items="${getWareHouse}" var="war">
                                      <option value="<c:out value="${war.whId}"/>"><c:out value="${war.whName}"/></option>
                            </c:forEach>
                        </select>
                    </td>
                      
                   </tr>
                        <tr>
                           <td align="center"><br><text style="color:red">*</text>
                                Route Code
                            </td>
                            <td><br><input type="text" id="routeCode" name="routeCode" style="width:200px;height:30px" onchange="checkExists(this.value);"></td>
                           <td align="center"><br><text style="color:red">*</text>
                                Route Name
                            </td>
                            <td><br><input type="text" id="routeName" name="routeName" style="width:200px;height:30px"></td>
                            
                          </tr>
               
                          </table>
                    <c:if test="${pincodeTemp!=null}">
                 <table class="table table-info mb30 table-bordered" id="table" >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>Pincode</th>
                                <th>Place</th>
                                <th>Select 
                                        <input type="checkbox" name="selectAll" id="selectAll" style="width:15px;height:12px;" onclick="checkAll();"/>
                                    </th>
                            </tr>

                        </thead>
                            <% int index = 0;
                                    int sno = 1;
                            %>
                            <tbody>

                                <c:forEach items="${pincodeTemp}" var="pin">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td>
                                            <%=sno%>
                                        </td>
                                        <td ><c:out value="${pin.pincode}"/></td>
                                        <td ><c:out value="${pin.place}"/></td>
                                      
                                         
                                        <td> <input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno%>" value="<c:out value="${pin.pincode}"/>" /></td>
                          
                            <input type="hidden" name="pincode" id="pincode" value="<c:out value="${pin.pincode}"/>">
                                    </tr>

                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach> 
                           

                    </table>    
                 </c:if>
                        <table>
                            <center>
                                <input type="button" value="Save" id="insert" class="btn btn-success" onClick="submitPage(this.value);">
                            
                            </center>

                        </table>
 <script>
                        var httpRequest;
    

              function go1() {
                        if (httpRequest.readyState == 4) {
                        if (httpRequest.status == 200) {
                         var response = httpRequest.responseText;
                            if (response != "") {
                                alert(response);
                                $('#routeCode').val('');
                    
                            } else
                            {
                            }
                                 }
                               }
                            }
                      function checkExists(routeCode){
                                   var url = "";
                                   if (routeCode != "") {
                                       url = "./checkRouteCode.do?routeCode="+routeCode;
                                       
                             if (window.ActiveXObject)
                            {
                            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                            } else if (window.XMLHttpRequest)
                             {           
                             httpRequest = new XMLHttpRequest();
                             }
                              httpRequest.open("POST", url, true);
                            httpRequest.onreadystatechange = function() {
                            go1();
                             };
                            httpRequest.send(null);
                                  }

                               }
     function searchPage() {
       
         if(document.getElementById("stateId").value==""){
             alert("Select State");
             document.getElementById("stateId").focus();
         }else{
             var a =document.getElementById("stateId").value;
            document.pincode.action = '/throttle/handleAddRouteMaster.do?&param=search&stateId='+a;
            document.pincode.submit();
        }
    }
    
      function submitPage(value) {
         
       
         if(document.getElementById("stateId").value==""){
             alert("Select State");
             document.getElementById("stateId").focus();
         }else if(document.getElementById("whId").value==""){
             alert("Select Warehose");
             document.getElementById("whId").focus();
         }else if(document.getElementById("routeCode").value==""){
             alert("Select routeCode");
             document.getElementById("routeCode").focus();
         }else if(document.getElementById("routeName").value==""){
             alert("Select routeName");
             document.getElementById("routeName").focus();
         }else{
             
            document.pincode.action = '/throttle/insertRouteMaster.do?&param=add';
            document.pincode.submit();
        }
    }
    
    function setValues(stateId,whId){
        if(stateId!=""){
            $('#search').hide();
        }else{
            $('#search').show();
        }
        document.getElementById("stateId").value=stateId;
        document.getElementById("whId").value=whId;
    }
  
                </script>
                            
                       
               </form>
        </body>
                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
                   
        </div>
    </div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>


