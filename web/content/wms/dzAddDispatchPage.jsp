<%-- 
    Document   : dzAddDispatchPage
    Created on : 29 Dec, 2021, 1:46:24 PM
    Author     : alan
--%>



<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


    <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
    <script language="javascript" src="/throttle/js/validate.js"></script>  
    <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
    <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
    <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
    <script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
    <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <script src="/throttle/js/select2.js" defer></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/select2.css"/>
    <link rel="stylesheet" type="text/css" href="/throttle/css/select2-bootstrap.css"/>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function () {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                dateFormat: 'dd-mm-yy',
                yearRange: '1900:' + new Date().getFullYear(),
                changeMonth: true, changeYear: true
            });
        });

        function setNoOfCrates(val) {
            var noOfCrates1 = document.getElementsByName("noOfCrates1");
            var totalNoOfCrates = 0;
            for (var i = 0; i < noOfCrates1.length; i++) {
                if (noOfCrates1[i].value != "") {
                    totalNoOfCrates = parseInt(noOfCrates1[i].value) + totalNoOfCrates;
                }
            }
            document.getElementById("noOfCrates").value = totalNoOfCrates;
        }




    </script>


    <script>



        function setCrates(val, sno) {
            var custId = val.split('-')[0];
            var no = val.split('-')[1];
            $('#customerId' + sno).val(custId);
            $('#noOfCrates1' + sno).val(no);
            setNoOfCrates('');
        }



        function checkTrackable(val) {
            var deviceId = val.split('-')[1];
            var isTrackable = false;

            $.ajax({"url": "https://sct.intutrack.com/api/test/devices/is_trackable?device_id=" + deviceId,
                "method": "GET",
                "mode": "no-cors",
                "headers": {
                    "Authorization": "Basic ZTNBWmFCT1F2bUtKWU5palh6Tmx2eVJtV2RLdTVhZFkybTB6UXhFVDRZNUFBZ2s0VE5pdUR2bDZ5UGp3VzB0WjpIdHJzRnIyR0FyQUloc05obk9BQXNORHZ5TjBBSVdqaElLSDZMRVJGR3JMTnl1NHVRaTVWU2FzMExBUmVNRnQw"
                },
                success: function (data) {
                    console.log(data);
                    console.log(data.is_trackable);
                    if (!data.is_trackable) {
                        document.getElementById("deviceId").value = "";
                        alert("Device is Offline");
                    }
                },
                error: function (r, e) {
                    console.log(r.responseJSON.message);
                    alert(r.responseJSON.message);
                    document.getElementById("deviceId").value = "";
                }
            });
        }
        function addSelect2() {
            $('#vehicleType').select2();
            $('#deviceId').select2();
            $('#vendorId').select2();
            $('#supplierId').select2();
        }

    </script>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> LR Generate</h2>

        <div class="breadcrumb-wrapper">
            <span class="label">You Are Here</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Shop Tree</a></li>
                <li class="">LR Generate</li>
            </ol>
        </div>
    </div>

    <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">

                <body onload="addSelect2()">

                    <form name="invoice" method="post">
                        <%@include file = "/content/common/message.jsp"%>
                        <table class="table table-info mb30 table-hover">


                            <tr>
                                <td><text style="color:red">*</text>Supplier Name : &emsp;</td>
                            <td>
                                <select  type="text" name="supplierId" id="supplierId" class="form-control" style="width:240px;height:40px">
                                    <option value="16">Dunzo</option>
                                    <!--  <c:if test = "${getSupplierList != null}" >
                                        <c:forEach items="${getSupplierList}" var="Dept"> 
                                            <option value='<c:out value="${Dept.supplierId}" />'><c:out value="${Dept.supplierName}" /></option>
                                        </c:forEach >
                                    </c:if>  -->
                                </select>
                            </td>

                            <td><text style="color:red">*</text>Transporter Name : &emsp;</td>
                            <td><select  type="text" name="vendorId" id="vendorId" class="form-control" style="width:240px;height:40px">
                                    <option value="">----Select----</option>
                                    <c:forEach items="${getImVendorList}" var="vn">
                                        <option value="<c:out value="${vn.vendorId}"/>"><c:out value="${vn.vendorName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>

                            </tr>

                            </tr>

                            <tr>

                                <td><text style="color:red">*</text>Device No : &emsp;</td>
                            <td>

                                <select id="deviceId" name="deviceId" onchange="checkTrackable(this.value)" class="form-control" style="width:240px;height:35px;">
                                    <option value="">-------Select One------</option>
                                    <c:forEach items="${deviceList}" var="did">
                                        <option value="<c:out value="${did.deviceId}"/>-<c:out value="${did.deviceNo}"/>"><c:out value="${did.deviceNo}"/></option>
                                    </c:forEach>
                                </select>
                                <!--<input type="hidden" name="deviceNo" class="form-control" style="width:240px;height:40px" id="deviceNo" value="">-->
                            </td>
                            <td style><text style="color:red">*</text>Vehicle No : &emsp;</td>
                            <td>
                                <input type="text" name="vehicleNo" class="form-control" style="width:240px;height:40px" id="vehicleNo" value="">
                            </td>
                            </tr>

                            <tr>

                                <td style>No of Crates : &emsp;</td>
                                <td>
                                    <input type="text" name="noOfCrates" class="form-control" style="width:240px;height:40px" disabled ="true" id="noOfCrates" value="0">
                                </td>

                                <td style><text style="color:red">*</text>Vehicle Type : &emsp;</td>
                            <td>
                                <select name="vehicleType" class="form-control" style="width:240px;height:40px" id="vehicleType" >
                                    <option value="">-----Select Vehicle Type-----</option>
                                    <c:forEach items="${getVehicleTypeList}" var="vt">
                                        <option value="<c:out value="${vt.typeId}"/>"><c:out value="${vt.typeName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                            </tr>
                            <tr>
                                <td><text style="color:red">*</text>Driver Name : &emsp;</td>
                            <td>
                                <input type="text" name="driverName" class="form-control" style="width:240px;height:40px" id="driverName" value="">
                            </td>


                            <td><text style="color:red">*</text> Driver Mobile : &emsp;</td>
                            <td>
                                <input type="text" id="driverMobile" name="driverMobile" style="width:240px;height:40px;"  class="form-control" value="">
                            </td>
                            </tr>

                            <tr>
                                <td><text style="color:red">*</text> Dispatch Date :&emsp;</td>
                            <td>
                                <input type="text" id="dispatchDate" name="dispatchDate" style="width:240px;height:40px;" class="datepicker form-control" >
                            </td>

                            <td><text style="color:red">*</text>Trip Start Time :  &emsp;</td>
                            <td><input type="time" name="tripStartTime" id="tripStartTime" style="width:240px;height:40px;" class="form-control"></td>

                            </tr>
                            <tr>
                                <td>Remarks : &emsp;</td>
                                <td><input type="text" name="remarks" id="reamrks" style="width:240px;height:40px;"  class="form-control" ></td>
                                <td>Start Km.: &emsp;</td>
                                <td><input type="text" name="startKm" id="startKm" style="width:240px;height:40px;"  class="form-control" ></td>
                            </tr>

                        </table>
                        <script>
                            var invoiceIds = [];

                            $(document).ready(function () {
                                $('#btnMultiple').click(function () {
                                    var testInvoices = [];
                                    $("input:checkbox[name='selectedIndexs']:checked").each(function () {
                                        if ($.inArray($(this).val(), invoiceIds) >= 0) {
                                            var del = invoiceIds.indexOf($(this).val()) ;
                                            invoiceIds.splice(del, 1);
                                        } else {
                                            testInvoices.push($(this).val());
                                            invoiceIds.push($(this).val());
                                        }

                                    });
                                    console.log(invoiceIds);
                                });
                            });


                            function submitPage(value) {
                                if (document.getElementById("supplierId").value == "") {
                                    alert("Enter Supplier Name");
                                    document.getElementById("supplierId").focus();
                                }
                                else if (document.getElementById("vendorId").value == "") {
                                    alert("Enter Transporter Name");
                                    document.getElementById("vendorId").focus();
                                }
                                else if (document.getElementById("startKm").value == "") {
                                    alert("Enter start km.");
                                    document.getElementById("startKm").focus();
                                }
                                else if (document.getElementById("vehicleNo").value == "") {
                                    alert("Enter Vehicle No");
                                    document.getElementById("vehicleNo").focus();
                                } else if (document.getElementById("vehicleType").value == "") {
                                    alert("Enter Vehicle Type");
                                    document.getElementById("vehicleType").focus();
                                } else if (document.getElementById("driverName").value == "") {
                                    alert("Enter Driver Name");
                                    document.getElementById("driverName").focus();
                                } else if (document.getElementById("driverMobile").value == "") {
                                    alert("Enter Driver Mobile");
                                    document.getElementById("driverMobile").focus();
                                } else if (document.getElementById("dispatchDate").value == "") {
                                    alert("Enter Dispatch Date");
                                    document.getElementById("dispatchDate").focus();
                                } else if (document.getElementById("tripStartTime").value == "") {
                                    alert("Enter Trip Start Time");
                                    document.getElementById("tripStartTime").focus();
                                } else {
                                    document.getElementById("invoiceIdList").value = invoiceIds;

                                    document.invoice.action = "/throttle/dzDispatchPlanning.do?param=lr";
                                    document.invoice.submit();

                                }
                            }

                        </script>




                        <c:if test="${invoiceMaster != null}">
                            <table align="center" border="0" id="table" class="table table-info mb30 table-hover" style="width:100%" >
                                <thead>
                                    <tr style="height: 40px;" id="tableDesingTH">

                                        <th>Invoice No</th>
                                        <th>Transfer Order No</th>
                                        <th>Party Code</th>
                                        <th>Party Name</th>
                                        <th align="center">Choose</th>

                                    </tr></thead>
                                <tbody>
                                    <% int index = 1;%>
                                    <% int cnt = 1;%>
                                    <c:forEach items="${invoiceMaster}" var="route">
                                        <%
                                           String className = "text1";
                                           if ((index % 2) == 0) {
                                               className = "text1";
                                           } else {
                                               className = "text2";
                                           }
                                        %>
                                        <tr class="item-row">

                                            <td align="left"><%=cnt%></td>
                                            <td align="left"><c:out value="${route.toNo}"/></td>
                                            <td align="left"><c:out value="${route.partyCode}"/></td>
                                            <td align="left"><c:out value="${route.partyName}"/>
                                            </td>
                                            <td >
                                                <span  data-toggle="modal" data-target="#myModal"><input type="checkbox"  name="selectedIndex" id="selectedIndex<%=index%>"  value="<%=cnt%>"  onclick="add(<%=cnt%>, '<c:out value="${route.invoiceId}"/>');"/></span></td>
                                            </td>

                                        </tr>
                                        <%index++;%>
                                        <%cnt++;%>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </c:if>


                        <script>
                            function invoiceSelectAll(element) {
                                var selectedIndex = document.getElementsByName("selectedIndexs");
                                if (element.checked == true) {
                                    for (var i = 0; i < selectedIndex.length; i++) {
                                        selectedIndex[i].checked = true;
                                    }
                                } else {
                                    for (var i = 0; i < selectedIndex.length; i++) {
                                        selectedIndex[i].checked = false;
                                    }
                                }
                                selected();
                            }


                            function add(val, invoiceId) {
//                                alert(invoiceId)
                                $.ajax({
                                    url: "/throttle/dzInvoiceDetailsPicked.do",
                                    dataType: "json",
                                    data: {
                                        invoiceId: invoiceId,
                                    },
                                    success: function (data) {
                                        var Sno1 = 1
                                        var tables = document.getElementById("tbody");
                                        tables.innerHTML = '';
                                        $.each(data, function (i, data) {

//                                            alert(data.prouductName);
//                                                console.log(data);
//                                                alert(data);
                                            $('#tbody').append(
                                                    "<tr>\n\
                                            <td class='text1' height='25' >" + Sno1 + "</td>\n\
                                            <td class='text1' height='25' >" + data.partyName + "</td>\n\
                                            <td class='text1' height='25' >" + data.prouductName + "</td>\n\
                                            <td class='text1' height='25' >" + data.qty + "</td>\n\
                                            <td class='text1' height='25' >" + data.amount + "</td>\n\
                                            <td class='text1' height='25' align='left'><input type='checkbox' class='button myclass'    name='selectedIndexs'  id='selectedIndexs" + Sno1 + "'value=" + data.toNo + "~" + data.id + " onclick='selected();' >\n\
</tr>\n\
                                               ");
                                               if($.inArray(data.toNo+"~"+data.id, invoiceIds)>-1){
                                                   document.getElementById('selectedIndexs'+Sno1).checked=true;
                                               }
                                                   console.log(invoiceIds);
                                            Sno1++;

                                        });
                                        var selectedIndex = document.getElementsByName("selectedIndex");
                                        var selectedIndextem = document.getElementById("selectedIndex" + val).value;
                                        for (var i = 0; i < selectedIndex.length; i++) {

                                            if (selectedIndex[i].value != selectedIndextem) {
                                                selectedIndex[i].checked = false;
                                            }
                                        }

                                        var checked = document.getElementById("checked" + val).value;
                                        var checkeds = checked.split("~");
                                        var invoiceNo = invoiceId
                                        var invoice = invoiceNo.split("~");
                                        var checkeds = checked.split("~");


                                        var selectedIndexs = document.getElementsByName("selectedIndexs");
                                        for (var m = 0; m < selectedIndexs.length; m++) {

                                            if (parseInt(selectedIndexs[m].value) === parseInt(checkeds[m])) {


                                                selectedIndexs[m].checked = true;
                                            }

                                        }

                                    }
                                });
                            }


                            function selected() {
                                var index = document.getElementsByName("selectedIndexs");
                                var cntr = 0;
                                var temp1 = "";
                                var temp2 = "";
                                var temp3 = "";
                                var temp4 = "";
                                var temp5 = "";
                                var temp6 = "";
                                var temp7 = "";
                                var temp8 = "";
                                var temp9 = "";
                                var temp10 = "";
                                var j = 1;

                                var k = 1;
                                var count = 0;
                                for (var i = 0; i < index.length; ++i) {


                                    var indes = document.getElementById("selectedIndexs" + j).checked;
                                    var val = document.getElementById("val" + j).value;


                                    if (indes) {
                                        count++;

                                        var qtyRow = document.getElementsByName("qtyRow");
                                        var invoiceRow = document.getElementsByName("invoiceRow");
                                        var orderId = document.getElementsByName("orderIdRow");
                                        var customerIdRow = document.getElementsByName("customerIdRow");
                                        var supplierIdRow = document.getElementsByName("supplierIdRow");
                                        var tempitemrow = document.getElementsByName("itemIdRow");
//                                            var temptaxrow = document.getElementsByName("taxValRow");
                                        var tempInvoicerow = document.getElementsByName("invoiceAmtRow");
                                        var tempPerTaxrow = document.getElementsByName("pretaxRow");
                                        console.log(tempPerTaxrow[i].value);

                                        if (cntr == 0) {
                                            temp1 = qtyRow[i].value;
                                            temp2 = invoiceRow[i].value;
                                            temp3 = j;
                                            temp4 = orderId[i].value;
                                            temp5 = customerIdRow[i].value;
                                            temp5 = customerIdRow[i].value;
                                            temp6 = tempitemrow[i].value;
                                            temp7 = tempInvoicerow[i].value;
                                            temp8 = tempPerTaxrow[i].value;
                                            temp10 = supplierIdRow[i].value;

                                            document.getElementById("checked" + val).value = j;
                                            document.getElementById("tempQ" + val).value = temp1;
                                            document.getElementById("tempsn" + val).value = temp3;
                                            document.getElementById("tempIn" + val).value = temp2;
                                            document.getElementById("temporderid" + val).value = temp4;
                                            document.getElementById("tempcustomerId" + val).value = temp5;
                                            document.getElementById("tempitemid" + val).value = temp6;
                                            document.getElementById("tempInvoiceAmt" + val).value = temp7;
                                            document.getElementById("tempPerTax" + val).value = temp8;
                                            document.getElementById("tempsuplierId" + val).value = temp10;
                                        } else {
                                            temp1 = (temp1 + "~" + (qtyRow[i].value).toString());
                                            temp2 = temp2 + "~" + (invoiceRow[i].value.toString());
                                            temp3 = temp3 + "~" + (j.toString());
                                            temp4 = temp4 + "~" + (orderId[i].value.toString());
                                            temp5 = temp5 + "~" + (customerIdRow[i].value.toString());
                                            temp6 = temp6 + "~" + (tempitemrow[i].value.toString());
                                            temp7 = temp7 + "~" + (tempInvoicerow[i].value.toString());
                                            temp8 = temp8 + "~" + (tempPerTaxrow[i].value.toString());
//                                                temp9 = temp9 + "~" + (temptaxrow[i].value.toString());
                                            temp10 = temp10 + "~" + (supplierIdRow[i].value.toString());

                                            document.getElementById("checked" + val).value = temp3;
                                            document.getElementById("tempQ" + val).value = temp1;
                                            document.getElementById("tempsn" + val).value = temp3;
                                            document.getElementById("tempIn" + val).value = temp2;
                                            document.getElementById("temporderid" + val).value = temp4;
                                            document.getElementById("tempcustomerId" + val).value = temp5;
                                            document.getElementById("tempitemid" + val).value = temp6;
                                            document.getElementById("tempInvoiceAmt" + val).value = temp7;
                                            document.getElementById("tempPerTax" + val).value = temp8;
//                                                document.getElementById("tempTax" + val).value = temp9;
                                            document.getElementById("tempsuplierId" + val).value = temp10;
                                        }
                                        cntr++;
                                    }
                                    else if (count === 0) {

                                        document.getElementById("tempQ" + val).value = "";
                                        document.getElementById("tempsn" + val).value = "";
                                        document.getElementById("tempIn" + val).value = "";
                                        document.getElementById("temporderid" + val).value = "";
                                        document.getElementById("tempcustomerId" + val).value = "";
                                        document.getElementById("tempitemid" + val).value = "";
                                        document.getElementById("tempInvoiceAmt" + val).value = "";
                                        document.getElementById("tempPerTax" + val).value = "";
                                        document.getElementById("checked" + val).value = "";
                                        document.getElementById("tempsuplierId" + val).value = "";
                                    }
                                    j++;
                                }
                            }





                        </script>




                        <div class="container">

                            <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog modal-lg">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Select Invoice Numbers</h4>
                                        </div>
                                        <div class="modal-body">

                                            <table align="center" border="0" id="tables" class="table table-info mb30 table-hover" style="width:100%" >
                                                <thead>
                                                    <tr style="height: 40px;" id="tableDesingTH">

                                                        <th align="center">Sno</th>
                                                        <th align="center">Party Name</th>
                                                        <th align="center">Product Name</th>
                                                        <th align="center">Qty</th>
                                                        <th align="center">Amount</th>
                                                        <th align="center">select <input type="checkbox" id="select3" name="select3" onclick="invoiceSelectAll(this);"></th>

                                                    </tr>
                                                </thead>
                                                <tbody id="tbody">


                                                </tbody>
                                            </table>

                                            <center>
                                                <input type="button" class="btn btn-success" value="Submit" name="Submit"  id="btnMultiple" data-dismiss="modal" >
                                            </center>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <br>
                        <br>
                        <center>
                            <input type="button" class="btn btn-success" value="Submit" name="Submit"  onclick="submitPage()">
                        </center>
                        <input type="hidden" id = "invoiceIdList" name = "invoiceIdList">

                    </form>
                </body>
            </div>
            </div>
            </div>


            </body>
            <%@include file="/content/common/NewDesign/settings.jsp"%>
            </html>
