


<%-- 
    Document   : dzLrGenerate
    Created on : 29 Dec, 2021, 10:10:19 AM
    Author     : alan
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script> 
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
//alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });</script>

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.Dispatch"  text="DC Generate"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="trucks.label.FleetName"  text="Fleet"/></a></li>
            <li class="active"><spring:message code="stores.label.Dispatch"  text="DC Generate"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

            <body>
                <form name="addLect" method="post">   


                    <%@ include file="/content/common/message.jsp" %>

                    <div id="tabs" >
                        <ul class="nav nav-tabs">
                            <li id="active" data-toggle="tab"><a href="#deD"><span>DC generate</span></a></li>

                        </ul>
                        <div id="deD">


                            <table class="table table-info mb30 table-hover">
                                <thead>

                                    <tr>
                                        <th colspan="4"  height="30"><div ><spring:message code="trucks.label.Dispatch"  text="Dispatch"/></div></th>
                                </tr>
                                </thead>
                                <c:forEach items="${getdispatch}" var="dispatch">
                                    <tr>
                                        <td   ><font color=red>*</font>Dispatch Order No</td>
                                        <td   >
                                            <input name="dispatchNo" id="dispatchNo" readonly type="text" class="form-control" style="width:260px;height:40px;" value="<c:out value="${dispatch.dispatchNo}"/>" >
                                            <input  id="dispatchid" name="dispatchid" type="hidden" class="form-control" style="width:260px;height:40px;" value="<c:out value="${dispatch.dispatchId}"/>" >
                                            <input  id="supplierId" name="supplierId" type="hidden" class="form-control" style="width:260px;height:40px;" value="<c:out value="${dispatch.supplierId}"/>" >
                                            <input  id="stockType" name="stockType" type="hidden" class="form-control" style="width:260px;height:40px;" value="<c:out value="${dispatch.stockType}"/>" >
                                            <c:set value="${dispatch.stockType}" var="stockType"/>
                                        </td>

                                        <td   ><font color=red>*</font>Transport</td>
                                        <td>
                                            <input name="vendorName" id="vendorName" readonly type="text" class="form-control" style="width:260px;height:40px;" value="<c:out value="${dispatch.vendorName}"/>" >
                                            <input name="vendorId" id="vendorId" type="hidden" class="form-control" style="width:260px;height:40px;" value="<c:out value="${dispatch.vendorId}"/>" >

                                        </td>
                                    </tr>

                                    <tr>
                                        <td><font color="red">*</font>Lorry No</td>
                                        <td ><select style="width:260px;height:40px;" id="vehicleNumber" name="vehicleNumber" class="form-control" onchange="setValue();">
                                                <option value="0">--Select--</option>
                                                <c:forEach items="${getVechicleDetails}" var="veh">
                                                    <option value="<c:out value="${veh.tatId}"/>"><c:out value="${veh.vehicleNumber}"/></option>
                                                </c:forEach>
                                            </select></td>
                                            <input type="hidden" name="vehicleNo" id="vehicleNo" />

                                        <td ><font color="red">*</font>vehicle Type</td>
                                        <td > <select style="width:260px;height:40px;" id="vehicleType" class="form-control" name="vehicleType" readonly >
                                                <option value="0">--Select--</option>
                                                <c:forEach items="${getVechicleDetails}" var="veh">
                                                    <option value="<c:out value="${veh.tatId}"/>"><c:out value="${veh.typeName}"/></option>
                                                </c:forEach></select></td>
                                    </tr>
                                    <tr>
                                        <td ><font color="red">*</font>Driver Name</td>
                                        <td ><select style="width:260px;height:40px;" id="driverName" class="form-control" readonly ><option value="0">--Select--</option>
                                                <c:forEach items="${getVechicleDetails}" var="veh">
                                                    <option value="<c:out value="${veh.tatId}"/>"><c:out value="${veh.driverName}"/></option>
                                                </c:forEach>   </select></td>
                                        <td><font color="red">*</font>Cell No</td>
                                        <td ><select style="width:260px;height:40px;" id="phoneNo" class="form-control" readonly ><option value="0">--Select--</option>
                                                <c:forEach items="${getVechicleDetails}" var="veh">
                                                    <option value="<c:out value="${veh.tatId}"/>"><c:out value="${veh.phoneNumber}"/></option>
                                                </c:forEach></select></td>
                                    </tr>
                                </c:forEach>
                                <tr>
                                    <td ><font color="red">*</font>Loading start Date</td>
                                    <td ><input name="loadingStart" id="loadingStart" type="text" autocomplete="off" class="datepicker form-control" style="width:260px;height:40px;" onclick="ressetDate(this);"></td>
                                    <td ><font color="red">*</font>Loading start Time</td>
                                    <td ><input name="loadingStartTime" id="loadingStartTime" type="time" autocomplete="off" class="form-control" style="width:260px;height:40px;" onclick="ressetDate(this);"></td>
                                </tr>
                                <tr>
                                    <td><font color="red">*</font>Loading end Date</td>
                                    <td >
                                        <input type="hidden" id="dispatchId" name="dispatchId" value="<c:out value="${dispatchId}"/>">
                                        <input name="loadingEnd" id="loadingEnd" type="text" autocomplete="off" class="form-control datepicker" style="width:260px;height:40px;" onClick="ressetDate(this);"></td>
                                    <td><font color="red">*</font>Loading end Time</td>
                                    <td >
                                        <input name="loadingEndTime" id="loadingEndTime" type="time" autocomplete="off" class="form-control" style="width:260px;height:40px;" onClick="ressetDate(this);"></td>
                                </tr>
                                <tr>
                                    <td><font color="red">*</font>Supervisor</td>
                                    <td>
                                        <input name="supervisorName" id="supervisorName" type="text" autocomplete="off" class="form-control" style="width:260px;height:40px;" onClick="ressetDate(this);">
                                    </td>
                                    <td><font color="red">*</font>Remarks</td>
                                    <td>
                                        <input name="remarks" id="remarks" type="text" autocomplete="off" class="form-control" style="width:260px;height:40px;" onClick="ressetDate(this);">
                                    </td>
                                </tr>
                                <tr>
                                    <td><font color="red">*</font>No. of Crates/Boxes</td>
                                    <td>
                                        <input name="noOfBoxes" id="noOfBoxes" type="text" autocomplete="off" class="form-control" style="width:260px;height:40px;" onClick="ressetDate(this);">
                                    </td>
                                    <td>Device Id</td>
                                    <td>
                                        <select id="deviceId" name="deviceId" onchange="checkTrackable(this.value)" class="form-control" style="width:240px;height:35px;">
                                            <option value="">-------Select One------</option>
                                            <c:forEach items="${deviceList}" var="did">
                                                <option value="<c:out value="${did.deviceId}"/>-<c:out value="${did.deviceNo}"/>"><c:out value="${did.deviceNo}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>


                            </table>
                            <br>
                            <br>
                            <br>
                            <script>
                                var picklistIdArr = [];
                                function setValue() {
                                    var vehicleNumber = document.getElementById("vehicleNumber").value;
                                    document.getElementById("driverName").value = vehicleNumber;
                                    document.getElementById("driverName").disabled = true;
                                    document.getElementById("phoneNo").value = vehicleNumber;
                                    document.getElementById("phoneNo").disabled = true;
                                    document.getElementById("vehicleType").value = vehicleNumber;
                                    document.getElementById("vehicleType").disabled = true;
                                    var vehicleNo = $("#vehicleNumber option:selected").text();
                                    document.getElementById("vehicleNo").value=vehicleNo;
                                }

                                function add(val1, val2, val3, val4, productId, hubId, scannable, dealerId, invoiceNo, invoiceDetailId) {
                                    var total = document.getElementById("quantity" + val1).value;
                                    var filledQty = document.getElementById("dcQty" + val1).value;
                                    if (total == filledQty) {
                                        alert("Filled Completely");
                                    } else {
                                        var selectedIndex = document.getElementsByName("selectedIndex");
                                        var selectedIndextem = document.getElementById("selectedIndex" + val1).value;
                                        var invoiceId = document.getElementById("invoiceId" + val1).value;
                                        document.getElementById("tempInvoiceId").value = invoiceId;
                                        document.getElementById("tempInvoiceDetailId").value = invoiceDetailId;
                                        document.getElementById("tempScannable").value = scannable;
                                        document.getElementById("addedInpQty").value = "0";
                                        document.getElementById("tempProductId").value = productId;
                                        document.getElementById("tempHubId").value = hubId;
                                        document.getElementById("tempDealerId").value = dealerId;
                                        document.getElementById("tempInvoiceNo").value = invoiceNo;
                                        document.getElementById("tempInpQty").value = val2;
                                        document.getElementById("tempCount").value = "1";
                                        document.getElementById("checkedRow").value = val1;
                                        document.getElementById("addedRowCount").value = "0";
                                        $('#table6').empty();
                                        if (scannable == "N") {
                                            checkExistSerial("Non Scannable");
                                        }
                                        for (var i = 0; i < selectedIndex.length; i++) {
                                            if (selectedIndex[i].value != selectedIndextem) {
                                                selectedIndex[i].checked = false;
                                            }
                                        }
                                    }
                                }

                                function checkAllShipment(elem) {
                                    var count = document.getElementById("countNo").value;
                                        for (var i = 1; i < count; i++) {
                                            if (elem.checked == true) {
                                                document.getElementById("selectedIndex" + i).checked = true;
                                                var dcQty = document.getElementById("dcQty" + i).value;
                                                var pendingQty = document.getElementById("pendingQty" + i).value;
                                                var quantity = document.getElementById("quantity" + i).value;
                                                document.getElementById("dcQty" + i).value = quantity;
                                                document.getElementById("dcQty" + i).readOnly = false;
                                                document.getElementById("pendingQty" + i).value = 0;
                                                scanQty(i,elem);
                                            } else {
                                                document.getElementById("selectedIndex" + i).checked = false;
                                                var dcQty = document.getElementById("dcQty" + i).value;
                                                var pendingQty = document.getElementById("pendingQty" + i).value;
                                                var quantity = document.getElementById("quantity" + i).value;
                                                document.getElementById("pendingQty" + i).value = quantity;
                                                document.getElementById("dcQty" + i).value = 0;
                                                document.getElementById("dcQty" + i).readOnly = true;
                                                scanQty(i,elem);
                                            }
                                        }
                                }
                                
                                
                                    function checkTrackable(val) {
                                        var deviceId = val.split('-')[1];
                                        var isTrackable = false;

                                        $.ajax({"url": "https://sct.intutrack.com/api/test/devices/is_trackable?device_id=" + deviceId,
                                            "method": "GET",
                                            "mode": "no-cors",
                                            "headers": {
                                                "Authorization": "Basic ZTNBWmFCT1F2bUtKWU5palh6Tmx2eVJtV2RLdTVhZFkybTB6UXhFVDRZNUFBZ2s0VE5pdUR2bDZ5UGp3VzB0WjpIdHJzRnIyR0FyQUloc05obk9BQXNORHZ5TjBBSVdqaElLSDZMRVJGR3JMTnl1NHVRaTVWU2FzMExBUmVNRnQw"
                                            },
                                            success: function(data) {
                                                console.log(data);
                                                console.log(data.is_trackable);
                                                if (!data.is_trackable) {
                                                    document.getElementById("deviceId").value = "";
                                                    alert("Device is Offline");
                                                }
                                            },
                                            error: function(r, e) {
                                                console.log(r.responseJSON.message);
                                                alert(r.responseJSON.message);
                                                document.getElementById("deviceId").value = "";
                                            }
                                        });
                                    }


                            </script>



                            <c:if test="${getdispatchList != null}">
                                <table align="center" border="0" id="table" class="table table-info mb30 table-hover" style="width:100%" >
                                    <thead>
                                        <tr style="height: 40px;" id="tableDesingTH">
                                            <th>Sno</th>
                                            <th align="center">Invoice Number</th>
                                            <th align="center">Plan Date</th>
                                            <th align="center">Customer Name</th>
                                            <th align="center">Product Name</th>
                                            <th align="center">Plan Qty</th>
                                            <th align="center">Filled Qty</th>
                                            <th align="center">Pending Qty</th>
                                            <th align="center">Pending Reason</th>
                                            <th align="center">Action<c:if test="${stockType!='1'}"><input type="checkbox" name="selectAll" id="selectAll" onchange="checkAllShipment(this);"/></c:if></th>

                                            </tr></thead>
                                        <tbody>
                                        <% int index = 1;%>
                                        <% int cnt = 1;%>
                                        <c:forEach items="${getdispatchList}" var="route">
                                            <%
                                               String className = "text1";
                                               if ((index % 2) == 0) {
                                                   className = "text1";
                                               } else {
                                                   className = "text2";
                                               }
                                            %>
                                            <tr class="item-row">
                                                <td><%=index%></td>
                                                <td><c:out value="${route.invoiceNo}"/>
                                                    <input type="hidden" id="invoiceId<%=cnt%>" name="invoiceId" value="<c:out value="${route.invoiceId}"/>">
                                                    <input type="hidden" id="invoiceDetailId<%=cnt%>" name="invoiceDetailId" value="<c:out value="${route.orderDetailId}"/>">
                                                    <input type="hidden" id="itemId<%=cnt%>" name="itemId" value="<c:out value="${route.productId}"/>">
                                                </td>

                                                <td><c:out value="${route.plannedDate}"/><input type="hidden" id="plannedDate" name="plannedDate" value="<c:out value="${route.plannedDate}"/>"></td>

                                                <td><c:out value="${route.customerName}"/><input type="hidden" id="customerId<%=cnt%>" name="customerId" value="<c:out value="${route.customerId}"/>"></td>
                                                <td><c:out value="${route.productName}"/></td>
                                                <td><c:out value="${route.inpQty}"/><input type="hidden" id="quantity<%=cnt%>" name="quantity" value="<c:out value="${route.inpQty}"/>"></td>
                                                <td>
                                                    <input type="text" style="width:180px;height:40px;" id="dcQty<%=cnt%>" readonly class="form-control" name="dcQty" value="0" onchange="pendingQtys(<%=cnt%>);"/>
                                                </td>
                                                <td>
                                                    <input type="text" style="width:180px;height:40px;" readonly class="form-control" id="pendingQty<%=cnt%>" name="pendingQty" value="<c:out value="${route.inpQty}"/>"/>
                                                </td>  

                                                <td>
                                                    <input type="text" style="width:180px;height:40px;"  class="form-control" id="pendingReason" name="pendingReason" value=""/>

                                                </td>
                                                <c:if test="${stockType=='1'}">
                                                    <td><span  data-toggle="modal" data-target="#myModal"><input type="checkbox"  name="selectedIndex" id="selectedIndex<%=index%>"  value="<%=cnt%>"  onclick="add('<%=cnt%>', '<c:out value="${route.inpQty}"/>', '<c:out value="${route.invoiceId}"/>', '<c:out value="${route.invoiceNo}"/>', '<c:out value="${route.productId}"/>', '<c:out value="${hubId}"/>', '<c:out value="${route.scannable}"/>', '<c:out value="${route.customerId}"/>', '<c:out value="${route.invoiceNo}"/>', '<c:out value="${route.orderDetailId}"/>');"/></span></td>
                                                        </c:if>

                                                <c:if test="${stockType!='1'}">
                                                    <td align="center"><input type="checkbox"  name="selectedIndex" id="selectedIndex<%=index%>"  value="<%=cnt%>" onclick="updateFilledQty('<%=cnt%>');
                                                            scanQty('<%=cnt%>', this)"/>
                                                        <br><br><span id="stockScan<%=cnt%>" data-toggle="modal" data-target="#serialDiv" class="label label-success" style="cursor: pointer;display:none" onclick="scanDiv('<%=cnt%>', '<c:out value="${route.productId}"/>', '<c:out value="${route.customerId}"/>', '<c:out value="${route.invoiceNo}"/>', '<c:out value="${route.orderDetailId}"/>', '<c:out value="${route.invoiceId}"/>')">Scan</span></td>
                                                    </c:if>

                                            </tr>
                                            <%index++;%>
                                            <%cnt++;%>
                                        </c:forEach>
                                    <input type="hidden" name="countNo" id="countNo" value="<%=cnt%>">
                                    </tbody>
                                </table>
                            </c:if>

                            <script>

                                function serialNumber(val1, val2, val3, val4) {
                                    document.addLect.action = '/throttle/connectinvoicUpdate.do?invoiceId=' + val1 + '&invoiceNumber=' + val2 + '&custId=' + val3 + '&custName=' + val4;
                                    document.addLect.submit();
                                }
                            </script>

                            <script>
                                var arrofs = [];
                                function checkExistSerial(val) {
                                    $("#submit2").show();
                                    var tempHubId = document.getElementById("tempHubId").value;
                                    var tempProductId = document.getElementById("tempProductId").value;
                                    var tempInvoiceId = document.getElementById("tempInvoiceId").value;
                                    var tempInvoiceDetailId = document.getElementById("tempInvoiceDetailId").value;
                                    var tempInpQty = document.getElementById("tempInpQty").value;
                                    var tempScannable = document.getElementById("tempScannable").value;
                                    var tempDealerId = document.getElementById("tempDealerId").value;
                                    var tempInvoiceNo = document.getElementById("tempInvoiceNo").value;
                                    var addedQty = document.getElementById("addedInpQty").value;
                                    var checkedRow = document.getElementById("checkedRow").value;
                                    var totalQty = document.getElementById("quantity" + checkedRow).value;
                                    var filledQty = document.getElementById("dcQty" + checkedRow).value;
                                    var pendingQty = parseInt(totalQty) - parseInt(filledQty) - parseInt(addedQty);
                                    var scannable = document.getElementById("tempScannable").value;
                                    if (arrofs.indexOf(val) > -1) {
                                        alert("Already Scanned");
                                    } else {
                                        if (pendingQty > 0) {
                                            $.ajax({
                                                url: "/throttle/checkConnectSerialNo.do",
                                                dataType: "json",
                                                data: {
                                                    whId: tempHubId,
                                                    productId: tempProductId,
                                                    invoiceId: tempInvoiceId,
                                                    inpQty: tempInpQty,
                                                    serialNo: val,
                                                    scannable: tempScannable
                                                },
                                                success: function(data) {
                                                    var count = document.getElementById("tempCount").value;
                                                    $.each(data, function(i, data) {
//                                                alert(data.status);
                                                        if (data.status == "Y") {
                                                            var serialNo = data.serialNo;
                                                            var storageName = data.storageName;
                                                            var rackName = data.rackName;
                                                            var binName = data.binName;
                                                            var serialId = data.serialId;
                                                            var stockId = data.stockId;
                                                            var packQty = data.packQty;
                                                            var qty = data.qty;
                                                            if (scannable == "N") {
                                                                var pack = Math.floor(pendingQty / packQty);
                                                                var loose = pendingQty % packQty;
                                                                var serialQty = (parseInt(packQty) * parseInt(pack)) + parseInt(loose);
                                                                var itemName = data.productName;
                                                                $('#table6').append("<tr><td>" + count + "</td><td>" + serialNo + "</td><td>" + itemName +
                                                                        "</td><td>" + storageName + "</td><td>" + rackName + "</td><td>" + binName +
                                                                        "</td><td><input type='text' class='form-control' onchange='changeNonScannablePackQty(" + packQty + "," + count + "," + pack + "," + loose + ",this.value)' name='addedPackQty' id='addedPackQty" + count + "' value=" + pack + ">" +
                                                                        "</td><td><input type='text' class='form-control' onchange='changeNonScannableQty(" + packQty + "," + count + "," + pack + "," + loose + ",this.value)' name='addedLooseQty' id='addedLooseQty" + count + "' value=" + loose + ">" +
                                                                        "</td><input type='hidden' name='addedSerialNo' id='addedSerialNo" + count + "' value=" + serialNo + ">" +
                                                                        "</td><input type='hidden' name='addedSerialId' id='addedSerialid" + count + "' value=" + serialId + ">" +
                                                                        "<input type='hidden' name='addedStockId' id='addedStockId" + count + "' value=" + stockId + ">" +
                                                                        "<input type='hidden' name='addedPackQty' id='addedPackQty" + count + "' value=" + pack + ">" +
                                                                        "<input type='hidden' name='addedLooseQty' id='addedLooseQty" + count + "' value=" + loose + ">" +
                                                                        "<input type='hidden' name='addedProductId' id='addedProductId" + count + "' value=" + tempProductId + ">" +
                                                                        "<input type='hidden' name='addedInvoiceId' id='addedInvoiceId" + count + "' value=" + tempInvoiceId + ">" +
                                                                        "<input type='hidden' name='addedInvoiceDetailId' id='addedInvoiceDetailId" + count + "' value=" + tempInvoiceDetailId + ">" +
                                                                        "<input type='hidden' name='addedInvoiceNo' id='addedInvoiceNo" + count + "' value=" + tempInvoiceNo + ">" +
                                                                        "<input type='hidden' name='addedTotQty' id='addedTotQty" + count + "' value=" + tempInpQty + ">" +
                                                                        "<input type='hidden' name='serialQty' id='serialQty" + count + "' value=" + serialQty + ">" +
                                                                        "</tr>");
                                                                count++;
                                                                document.getElementById("addedInpQty").value = parseInt(document.getElementById("addedInpQty").value) + parseInt(tempInpQty);
                                                                document.getElementById("tempCount").value = count;
                                                            } else {
                                                                arrofs.push(serialNo);
                                                                if (parseInt(qty) > pendingQty) {
                                                                    alert("Quantity Exceeded");
                                                                } else {
                                                                    var pack = 0;
                                                                    var loose = 0;
                                                                    if (parseInt(data.uom) == 1) {
                                                                        pack = parseInt(qty) / parseInt(packQty);
                                                                    } else {
                                                                        loose = parseInt(qty)
                                                                    }
                                                                    var serialQty = qty;
                                                                    var itemName = data.productName;
                                                                    $('#table6').append("<tr><td>" + count + "</td><td>" + serialNo + "</td><td>" + itemName +
                                                                            "</td><td>" + storageName + "</td><td>" + rackName + "</td><td>" + binName + "</td><td>" + pack + "</td><td>" + loose +
                                                                            "</td><input type='hidden' name='addedSerialNo' id='addedSerialNo" + count + "' value=" + serialNo + ">" +
                                                                            "</td><input type='hidden' name='addedSerialId' id='addedSerialId" + count + "' value=" + serialId + ">" +
                                                                            "<input type='hidden' name='addedStockId' id='addedStockId" + count + "' value=" + stockId + ">" +
                                                                            "<input type='hidden' name='addedPackQty' id='addedPackQty" + count + "' value=" + pack + ">" +
                                                                            "<input type='hidden' name='addedLooseQty' id='addedLooseQty" + count + "' value=" + loose + ">" +
                                                                            "<input type='hidden' name='addedProductId' id='addedProductId" + count + "' value=" + tempProductId + ">" +
                                                                            "<input type='hidden' name='addedInvoiceId' id='addedInvoiceId" + count + "' value=" + tempInvoiceId + ">" +
                                                                            "<input type='hidden' name='addedInvoiceDetailId' id='addedInvoiceDetailId" + count + "' value=" + tempInvoiceDetailId + ">" +
                                                                            "<input type='hidden' name='addedInvoiceNo' id='addedInvoiceNo" + count + "' value=" + tempInvoiceNo + ">" +
                                                                            "<input type='hidden' name='addedTotQty' id='addedTotQty" + count + "' value=" + qty + ">" +
                                                                            "<input type='hidden' name='serialQty' id='serialQty" + count + "' value=" + serialQty + ">" +
                                                                            "</tr>");
                                                                    count++;
                                                                    document.getElementById("addedInpQty").value = parseInt(document.getElementById("addedInpQty").value) + parseInt(qty);
                                                                    var temp5 = document.getElementById("addedRowCount").value;
                                                                    document.getElementById("addedRowCount").value = parseInt(temp5) + 1;
                                                                    document.getElementById("tempCount").value = count;
                                                                }
                                                            }
//                                                    alert(storageName);
                                                        } else {
                                                            alert("Stock Not Updated for the selected Product");
                                                        }
                                                    });

                                                }
                                            });
                                        } else {
                                            alert("Already Filled");
                                        }
                                    }
                                    document.getElementById("scannedSno").value = "";
                                }

                                function changeNonScannablePackQty(packQty, sno, pack, loose, inpQty) {
                                    if (parseInt(inpQty) > parseInt(pack)) {
                                        document.getElementById("addedPackQty" + sno).value = pack;
                                    } else {
                                        document.getElementById("serialQty" + sno).value = (parseInt(inpQty) * parseInt(packQty)) + (parseInt(loose));
                                    }
                                }

                                function changeNonScannableQty(packQty, sno, pack, loose, inpQty) {
                                    if (parseInt(inpQty) > parseInt(loose)) {
                                        document.getElementById("addedLooseQty" + sno).value = loose;
                                    } else {
                                        document.getElementById("serialQty" + sno).value = (parseInt(pack) * parseInt(packQty)) + (parseInt(inpQty));
                                    }
                                }

                                function submits() {
                                    document.getElementById("submit2").style.display = "none";
                                    var dispatchId = document.getElementById("dispatchId").value;
                                    var serialIds = document.getElementsByName("addedSerialId");
                                    var serialNos = document.getElementsByName("addedSerialNo");
                                    var stockIds = document.getElementsByName("addedStockId");
                                    var invoiceIds = document.getElementsByName("addedInvoiceId");
                                    var invoiceDetailIds = document.getElementsByName("addedInvoiceDetailId");
                                    var productIds = document.getElementsByName("addedProductId");
                                    var invoiceNos = document.getElementsByName("addedInvoiceNo");
                                    var serialQtys = document.getElementsByName("serialQty");
                                    var inpQty = document.getElementsByName("addedTotQty");
                                    var dealerId = document.getElementById("tempDealerId").value;
                                    var checkedRow = document.getElementById("checkedRow").value;
                                    var qty = 0;
                                    for (var x = 0; x < serialIds.length; x++) {
                                        $.ajax({
                                            url: '/throttle/insertConnectPicklistDetails.do',
                                            dataType: 'json',
                                            data: {
                                                dispatchId: dispatchId,
                                                productId: productIds[x].value,
                                                serialId: serialIds[x].value,
                                                serialNo: serialNos[x].value,
                                                stockId: stockIds[x].value,
                                                invoiceId: invoiceIds[x].value,
                                                invoiceDetailId: invoiceDetailIds[x].value,
                                                invoiceNo: invoiceNos[x].value,
                                                custId: dealerId,
                                                qty: serialQtys[x].value
                                            },
                                            success: function(data) {
                                                console.log("done " + x);
                                                $.each(data, function(i, data) {
                                                    if (data.status == "200") {
                                                        picklistIdArr.push(data.id);
                                                        qty = qty + parseInt(data.qty);
                                                        document.getElementById("dcQty" + checkedRow).value = parseInt(document.getElementById("dcQty" + checkedRow).value) + parseInt(data.qty);
                                                        document.getElementById("pendingQty" + checkedRow).value = parseInt(document.getElementById("pendingQty" + checkedRow).value) - parseInt(data.qty);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                    alert("added");

                                }

                                function searchDispatchDetails(value) {
                                    var pendingQty = document.getElementsByName('pendingQty');
                                    var x1 = "";
                                    for (var j = 0; j < picklistIdArr.length; j++) {
                                        if (j == 0) {
                                            x1 = picklistIdArr[j];
                                        } else {
                                            x1 = x1 + "~" + picklistIdArr[j];
                                        }
                                    }
                                    document.getElementById("picklistIds").value = x1;
                                    var qtyFin = 0;
                                    for (var i = 0; i < pendingQty.length; i++) {
                                        qtyFin = parseInt(qtyFin) + parseInt(pendingQty[i].value);
                                    }
                                    if (document.getElementById("vehicleNumber").value == 0) {
                                        alert("Please select Lorry No");
                                    } else if (document.getElementById("loadingStart").value == "") {
                                        alert("Please select Loading Start Date");
                                    } else if (document.getElementById("loadingEnd").value == "") {
                                        alert("Please Enter Loading End Date");
//                                    } else if (qtyFin > 0) {
//                                        alert("Please Fill All Qty");
                                    } else {
                                        document.addLect.action = '/throttle/saveDcGenerate.do?param=save';
                                        document.addLect.submit();
                                    }
                                }
                                function updateFilledQty(sno) {
                                    if (document.getElementById("selectedIndex" + sno).checked == true) {
                                        var dcQty = document.getElementById("dcQty" + sno).value;
                                        var pendingQty = document.getElementById("pendingQty" + sno).value;
                                        var quantity = document.getElementById("quantity" + sno).value;
                                        document.getElementById("dcQty" + sno).value = quantity;
                                        document.getElementById("dcQty" + sno).readOnly = false;
                                        document.getElementById("pendingQty" + sno).value = 0;
                                    } else {
                                        var dcQty = document.getElementById("dcQty" + sno).value;
                                        var pendingQty = document.getElementById("pendingQty" + sno).value;
                                        var quantity = document.getElementById("quantity" + sno).value;
                                        document.getElementById("pendingQty" + sno).value = quantity;
                                        document.getElementById("dcQty" + sno).value = 0;
                                        document.getElementById("dcQty" + sno).readOnly = true;
                                    }
                                }
                                function pendingQtys(sno) {
                                    var dcQty = document.getElementById("dcQty" + sno).value;
                                    var quantity = document.getElementById("quantity" + sno).value;
                                    if (parseInt(dcQty) > parseInt(quantity)) {
                                        alert("Quantity Exceeded");
                                        document.getElementById("dcQty" + sno).value = quantity;
                                    } else {
                                        document.getElementById("pendingQty" + sno).value = parseInt(quantity) - parseInt(dcQty);
                                    }
                                }
                                function scanQty(sno, elem) {
                                    if (elem.checked == true) {
                                        document.getElementById("stockScan" + sno).style.display = '';
                                    } else {
                                        document.getElementById("stockScan" + sno).style.display = 'none';
                                    }
                                }
                                function scanDiv(sno, productId, custId, invoiceNo, invoiceDetailId, invoiceId) {
                                    var tempElem = document.getElementsByName("xInvoiceDetailId");
                                    var qtyTot = 0;
                                    for (var i = 0; i < tempElem.length; i++) {
                                        if (tempElem[i].value == invoiceDetailId) {
                                            document.getElementById("serialCountNew" + (i + 1)).style.display = "";
                                        } else {
                                            document.getElementById("serialCountNew" + (i + 1)).style.display = "none";
                                        }
                                    }
                                    document.getElementById("productIdNew").value = productId;
                                    document.getElementById("custIdNew").value = custId;
                                    document.getElementById("invoiceNoNew").value = invoiceNo;
                                    document.getElementById("invoiceDetailIdNew").value = invoiceDetailId;
                                    document.getElementById("invoiceIdNew").value = invoiceId;
                                    document.getElementById("dcQtyNew").value = document.getElementById("dcQty" + sno).value;
                                }
                                function scanSerialNew(serial) {
                                    var productId = document.getElementById("productIdNew").value;
                                    var custId = document.getElementById("custIdNew").value;
                                    var invoiceNo = document.getElementById("invoiceNoNew").value;
                                    var invoiceDetailId = document.getElementById("invoiceDetailIdNew").value;
                                    var invoiceId = document.getElementById("invoiceIdNew").value;
                                    var dcQty = document.getElementById("dcQtyNew").value;
                                    var count = ($('#serialScanDiv').find('tr').length) + 1;
                                    var tempElem = document.getElementsByName("xInvoiceDetailId");
                                    var qtyTot = 0;
                                    for (var i = 0; i < tempElem.length; i++) {
                                        if (tempElem[i].value == invoiceDetailId) {
                                            qtyTot = qtyTot + 1;
                                        }
                                    }
                                    if (parseInt(qtyTot) < parseInt(dcQty)) {
                                        $.ajax({
                                            url: "/throttle/checkConnectSerialNoMaster.do",
                                            dataType: "json",
                                            data: {
                                                serialNo: serial
                                            },
                                            success: function(data) {
                                                if (data == "0") {
                                                    $("#serialScanDiv").append('<tr id="serialCountNew' + count + '"><td>' + count
                                                            + '</td><td><input type="text" id="xSerialNo' + count + '" name="xSerialNo" value="' + serial + '" class="form-control" style="width:180px;height:40px"/>'
                                                            + '</td><td><input type="text" id="xQty' + count + '" name="xQty" value="' + 1 + '" class="form-control" style="width:180px;height:40px"/>'
                                                            + '</td><td><select id="xUom' + count + '" name="xUom" class="form-control" style="width:180px;height:40px"><option value="1">Pack</option><option value="2">Piece</option></select>'
                                                            + '<input type="hidden" id="xProductId' + count + '" name="xProductId" value="' + productId + '" class="form-control" style="width:180px;height:40px"/>'
                                                            + '<input type="hidden" id="xCustId' + count + '" name="xCustId" value="' + custId + '" class="form-control" style="width:180px;height:40px"/>'
                                                            + '<input type="hidden" id="xInvoiceNo' + count + '" name="xInvoiceNo" value="' + invoiceNo + '" class="form-control" style="width:180px;height:40px"/>'
                                                            + '<input type="hidden" id="xInvoiceId' + count + '" name="xInvoiceId" value="' + invoiceId + '" class="form-control" style="width:180px;height:40px"/>'
                                                            + '<input type="hidden" id="xInvoiceDetailId' + count + '" name="xInvoiceDetailId" value="' + invoiceDetailId + '" class="form-control" style="width:180px;height:40px"/>'
                                                            + '</td></tr>');
                                                    $('#scannedSerialNew').val('');
                                                } else {
                                                    alert("Already Exist Serial No");
                                                }
                                            }
                                        });
                                    } else {
                                        alert("Quantity Exceeded");
                                    }
                                }
                            </script>
                            <center>
                                <input type="button" class="btn btn-success" value="Submit" name="Submit"  onclick="searchDispatchDetails()">
                            </center>

                            <div class="container">
                                <div class="modal fade" id="serialDiv">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Select Invoice Numbers</h4>
                                            </div>
                                            <div class="modal-body">

                                                <table align="center" border="0" id="tablesScan" class="table table-info mb30 table-hover" style="width:100%" >
                                                    <tr>
                                                        <td>Scan Serial No</td>
                                                        <td>
                                                            <input type="text" id="scannedSerialNew" name="scannedSerialNew" style="width:240px;height:40px" class="form-control" onchange="scanSerialNew(this.value)">
                                                        </td>
                                                    </tr>

                                                </table>
                                                <table class="table table-info mb30 table-hover">
                                                    <thead>
                                                    <th>S. No</th>
                                                    <th>Serial No</th>
                                                    <th>Quantity</th>
                                                    <th>UOM</th>
                                                    </thead>
                                                    <tbody id="serialScanDiv"></tbody>
                                                </table>
                                                <input type="hidden" name="productIdNew" id="productIdNew" value="">
                                                <input type="hidden" name="invoiceDetailIdNew" id="invoiceDetailIdNew" value="">
                                                <input type="hidden" name="invoiceNoNew" id="invoiceNoNew" value="">
                                                <input type="hidden" name="invoiceIdNew" id="invoiceIdNew" value="">
                                                <input type="hidden" name="custIdNew" id="custIdNew" value="">
                                                <input type="hidden" name="dcQtyNew" id="dcQtyNew" value="0">
                                                <center>
                                                    <input type="button" class="btn btn-success" value="submit" name="Submit" id="submit2" data-dismiss="modal" onclick="submits();" >
                                                </center>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" id="cancelBtn" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="container">
                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Scan Serial No</h4>
                                            </div>
                                            <div class="modal-body">

                                                <table align="center" border="0" id="tables" class="table table-info mb30 table-hover" style="width:100%" >
                                                    <tr>
                                                        <td>Scan Serial No</td>
                                                        <td>
                                                            <input type="text" id="scannedSno" name="scannedSno" style="width:240px;height:40px" class="form-control" onchange="checkExistSerial(this.value)">
                                                        </td>
                                                    </tr>

                                                </table>
                                                <table align="center" border="0" id="table5" class="table table-info mb30 table-bordered" style="width:100%" >
                                                    <thead>
                                                    <th>S. No</th>
                                                    <th>Serial No</th>
                                                    <th>Product Name</th>
                                                    <th>Storage Name</th>
                                                    <th>Rack Name</th>
                                                    <th>Bin Name</th>
                                                    <th>Pack Qty</th>
                                                    <th>Loose Qty</th>
                                                    </thead>
                                                    <tbody id="table6">

                                                    </tbody>
                                                </table>
                                                <input type="hidden" name="picklistIds" id="picklistIds" value="">
                                                <input type="hidden" name="tempProductId" id="tempProductId" value="">
                                                <input type="hidden" name="tempHubId" id="tempHubId" value="">
                                                <input type="hidden" name="tempInvoiceId" id="tempInvoiceId" value="">
                                                <input type="hidden" name="tempInvoiceDetailId" id="tempInvoiceDetailId" value="">
                                                <input type="hidden" name="tempDealerId" id="tempDealerId" value="">
                                                <input type="hidden" name="tempInvoiceNo" id="tempInvoiceNo" value="">
                                                <input type="hidden" name="tempInpQty" id="tempInpQty" value="">
                                                <input type="hidden" name="tempScannable" id="tempScannable" value="">
                                                <input type="hidden" name="tempCount" id="tempCount" value="0">
                                                <input type="hidden" name="addedRowCount" id="addedRowCount" value="0">
                                                <input type="hidden" name="addedInpQty" id="addedInpQty" value="0">
                                                <input type="hidden" name="checkedRow" id="checkedRow" value="0">
                                                <center>
                                                    <input type="button" class="btn btn-success" value="submit" name="Submit" id="submit2" data-dismiss="modal" onclick="submits();" >
                                                </center>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" id="cancelBtn" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </body>
                        </div>

                        <br>
                        <br>
                        </body>



                        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>


                    </div>
        </div>


    </div>
</div>






<%@ include file="/content/common/NewDesign/settings.jsp" %>
