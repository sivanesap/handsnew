<%-- 
    Document   : fkOrderUploadView
    Created on : 8 Jul, 2021, 6:28:28 PM
    Author     : Roger
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>



</head>
<script language="javascript" type="text/javascript">
    setFilterGrid("table");
    function orderStatus(orderId) {
        debugger;
        window.open('/throttle/handleViewOrderStatus.do?orderId=' + orderId + '&value=1', 'PopupPage', 'height = 400, width = 800, scrollbars = yes, resizable = yes');
    }
</script>
<script>
    function searchPage() {
        document.fk.action = "/throttle/fkOrderUploadView.do";
        document.fk.submit();
    }

</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.View Order List" text="Flipkart Upload View"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Flipkart"/></a></li>
            <li class=""><spring:message code="hrms.label.View Order List" text="Flipkart Order Details"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="fk"  method="post">
                    <table class="table table-info mb30 table-hover" id="report" >
                        <thead>
                            <tr>
                                <th colspan="4" height="30" >Flipkart Upload View</th>
                            </tr>
                        </thead>
                        <tr>
                            <td><font color="red">*</font>Type</td>
                            <td height="30">
                                <select name="orderType" id="orderType" class="form-control" style="width:250px;height:40px" autocomplete="off" value="<c:out value="${orderType}"/>">
                                    <option value="">------Select Type------</option>
                                    <option value="1">Forward</option>
                                    <option value="2">Prexo</option>
                                    <option value="3">RVP</option>
                                </select>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <script>
                            document.getElementById("orderType").value='<c:out value="${orderType}"/>';
                        </script>

                        <tr>             
                            <td  colspan="4" align="center">
                                <input type="button" class="btn btn-success"   value="Search" onclick="searchPage()"/>&emsp;&emsp;
                            </td>
                        </tr>
                    </table>
                    <table colspan="4" class="table table-info mb30 table-hover" id="table" >
                        <thead>
                            <tr height="40" colspan="6">
                                <th>Sno</th>
                                <th >Shipment Id</th>
                                <th >Uploaded Date & Time</th>
                                <th >Shipment Type</th>
                                <th >Pay Type</th>
                                <th>Customer Name</th>    
                                <th>Delivery Hub</th>
                                <th>Order Type</th>
                                <th>City</th>
                                <th>Pincode</th>
                                <th>Product info</th>
                                <th>Amount</th>

                            </tr>
                        </thead>
                        <% int index = 0;
                                    int sno = 1;
                        %>
                        <tbody>
                            <c:forEach items="${fkOrderUploadView}" var="order">

                                <tr height="30">
                                    <td align="left" ><%=sno%></td>
                                    <td align="left" ><c:out value="${order.shipmentId}"/></td>
                                    <td align="left" ><c:out value="${order.billDate}"/></td>
                                    <td align="left" >
                                        <c:if test="${order.orderType==1}">Forward</c:if>
                                        <c:if test="${order.orderType==2}">PREXO</c:if>
                                        <c:if test="${order.orderType==3}">RVP</c:if>
                                        </td>
                                        <td align="left" ><c:out value="${order.shipmentType}"/></td>
                                    <td align="left" ><c:out value="${order.customerName}"/></td> 
                                    <td align="left" ><c:out value="${order.deliveryHub}"/></td>
                                    <td align="left" ><c:out value="${order.orderType}"/></td>                        
                                    <td align="left" ><c:out value="${order.city}"/></td>                        
                                    <td align="left" ><c:out value="${order.pincode}"/></td>                        
                                    <td align="left" ><c:out value="${order.itemName}"/></td>                        
                                    <td align="left" ><c:out value="${order.amount}"/></td>                        
                                </tr>
                                <%index++;%>
                                <%sno++;%>
                            </c:forEach>
                        </tbody>
                    </table>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
