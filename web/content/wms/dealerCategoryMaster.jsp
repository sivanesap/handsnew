<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    function checkBin() {
        var url = "";
        var Bin = document.getElementById("binName").value
        if (Bin != "") {
            var url = "./checkBinName1.do?binName=" + Bin;

            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("POST", url, true);
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var response = httpRequest.responseText;
                        if (response != "") {
                            alert(response);
                            //console.log(response)
                            document.getElementById("binName").value = ""
                        }
                    }
                }
            };
            httpRequest.send(null);
        }

    }


    function submitPage() {


        document.productCategory.action = " /throttle/saveDealerCategory.do";
        document.productCategory.method = "post";
        document.productCategory.submit();



    }


    function setValues(sno, dealerName, dealerCode, industries, district, street, city, postalCode, telephone1, telephone2, gstNumber, customerName, customerMobileNo) {
        var count = parseInt(document.getElementById("count").value);
        // alert("fhskf=="+binId);


        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("dealerName").value = dealerName;
        document.getElementById("dealerCode").value = dealerCode;
        document.getElementById("industries").value = industries;
        document.getElementById("district").value = district;
        document.getElementById("street").value = street;
        document.getElementById("city").value = city;
        document.getElementById("postalCode").value = postalCode;
        document.getElementById("telephone1").value = telephone1;
        document.getElementById("telephone2").value = telephone2;
        document.getElementById("gstNumber").value = gstNumber;
        document.getElementById("customerName").value = customerName;
        document.getElementById("customerPhone").value = customerMobileNo;
    }

    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }



    var httpRequest;
    function checkproductCategoryName() {

        var productCategoryName = document.getElementById('productCategoryName').value;

        var url = '/throttle/checkProductCategory.do?productCategoryName=' + productCategoryName;
        if (window.ActiveXObject) {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest) {
            httpRequest = new XMLHttpRequest();
        }
        httpRequest.open("GET", url, true);
        httpRequest.onreadystatechange = function() {
            processRequest();
        };
        httpRequest.send(null);

    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#productCategoryNameStatus").text('Please Check Product Category Name: ' + val + ' is Already Exists');
                } else {
                    $("#nameStatus").hide();
                    $("#productCategoryNameStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

    function uploadPage() {

        var customerName = document.getElementById("customerName").value;
        var customerPhone = document.getElementById("customerPhone").value;

        document.upload.action = "/throttle/dealerMasterUpload.do?param=upload&customerName=" + customerName;
        document.upload.submit();
    }



    function insertTo() {
        var result = confirm("Some errors are not Updated. Do you want to continue?");
        if (result == true) {
            document.upload.action = "/throttle/dealerMasterUploads.do";
            document.upload.method = "post";
            document.upload.submit();
        }
        else {

            document.upload.action = "/throttle/dealerMasterUploads.do?";
            document.upload.method = "post";
            document.upload.submit();
        }
    }

</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Product Master" text="Dealer Master"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
                <li class=""><spring:message code="hrms.label.Product Master" text="Dealer Master"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="document.productCategory.productCategoryName.focus();">
                    <form name="upload"  method="post" enctype="multipart/form-data">
                        <%--<%@ include file="/content/common/path.jsp" %>--%>

                        <%@ include file="/content/common/message.jsp" %>


                        <table class="table table-info mb30 table-hover" id="uploadTable"  >
                            <thead> 
                                <tr>
                                    <th  colspan="3">Dealer Master Upload</th>
                                </tr>

                            </thead>

                            <tr> 
                                <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Customer Name</td>
                                <td class="text1">
                                    <select  align="center" class="form-control" style="width:240px;height:40px" name="customerName" id="customerName" >
                                        <option value=''>--select--</option>                            
                                        <c:if test = "${customerList != null}" >
                                            <c:forEach items="${customerList}" var="cust"> 
                                                <option value='<c:out value="${cust.custId}" />'><c:out value="${cust.custName}" /></option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td >Select Excel</td>
                                <td ><input type="file" name="importCnote" id="importCnote"  class="importCnote"><font size="1"><u><a href="uploadedxls/dealerMaster.xls" download>sample file</a></u></font></td>                             
                                <td colspan="0"><input type="button" class="btn btn-success" value="Upload" name="UploadPage" id="UploadPage" onclick="uploadPage();"></td>
                            </tr>

                        </table>
                        <div id="ptp">
                            <div class="inpad" style=" overflow-x: visible;">

                                <c:if test = "${getDealerList != null}" >

                                    <table class="table table-info mb30 table-bordered" id="table" style="font-size:12px">

                                        <thead style="font-size:12px">
                                            <tr height="40">
                                                <th  height="30" >S No</th>
                                                <th  height="30" >Dealer Name</th>
                                                <th  height="30" >Dealer Code</th>
                                                <th  height="30" >Industries</th>
                                                <!--<th  height="30" >Matl Group</th>-->
                                                <th  height="30" >Street</th>
                                                <th  height="30" >District</th>
                                                <th  height="30" >City</th>
                                                <th  height="30" >Pincode</th>
                                                <th  height="30" >phone1</th>                                                    
                                                <th  height="30" >Phone2</th>
                                                <!--<th  height="30" >GSTIN Number</th>-->
                                                <th  height="30" >Gst No</th>
                                                <th  height="30" >Customer Name</th>
                                                <th  height="30" >Customer Mobile</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <% int index = 0; 
                                         int sno = 1;
                                         int count=0;
                                        %> 
                                        <tbody>

                                            <c:forEach items= "${getDealerList}" var="contractList">
                                                <tr height="30">
                                                    <td align="left"><%=sno%></td>
                                                    <td align="left"   ><c:out value="${contractList.dealerName}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.dealerCode}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.industries}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.district}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.street}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.city}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.pincode}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.phone}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.phone1}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.gstNo}"/></td>

                                                    <td align="left"   ><c:out value="${contractList.customerName}"/></td>
                                                    <td align="left"   ><c:out value="${contractList.customerMobile}"/></td>


                                                    <c:if test="${contractList.status==0}">
                                                        <td align="left"><font color="green">Ready To Submit</font></td>
                                                            </c:if>
                                                            <c:if test="${contractList.status==1}">
                                                        <td align="left"><font color="red">Dealer Code Already Exits Temp</font></td>
                                                                <%count++;%>
                                                            </c:if>
                                                            <c:if test="${contractList.status==2}">
                                                        <td align="left"><font color="red">Dealer Code Already Exists in Dealer Master</font></td>
                                                                <%count++;%>
                                                            </c:if>


                                                </tr> 
                                                <%
                                                sno++;
                                                index++;
                                                %>
                                            </c:forEach>
                                        <input type="hidden" name="countStatus" id="countStatus" value="<%=count%>"/>
                                        </tbody>
                                    </table>

                                    <center>

                                        <td colspan="0"><input type="button" class="btn btn-success" value="Submit" name="submitPage" id="submitPage" onclick="insertTo();"></td>
                                    </center>
                                </c:if>
                            </div>
                        </div>     

                    </form>

                    <form name="productCategory"  method="post" >
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <br>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <br>
                        <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                            <input type="hidden" name="productCategoryId" id="productCategoryId" value=""  />
                            <tr>
                                <!--<table  border="0" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">-->
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="8" >Dealer Master</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="productCategoryNameStatus" style="color: red"></label></td>
                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Dealer Name</td>
                                    <td><input type="text" id="dealerName" name="dealerName" class="form-control" style="width:240px;height:40px"></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Dealer Code</td>
                                    <td class="text1"><input type="text" name="dealerCode" id="dealerCode" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Industries</td>
                                    <td class="text1"><input type="text" name="industries" id="industries" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checkproductCategoryName();" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>District</td>
                                    <td class="text1"><input type="text" name="district" id="district" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Street</td>
                                    <td class="text1"><input type="text" name="street" id="street" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>City</td>
                                    <td class="text1"><input type="text" name="city" id="city" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Postal Code</td>
                                    <td class="text1"><input type="text" name="postalCode" id="postalCode" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Phone No</td>
                                    <td class="text1"><input type="text" name="telephone1" id="telephone1" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Alternate Phone No</td>
                                    <td class="text1"><input type="text" name="telephone2" id="telephone2" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>GST Number</td>
                                    <td class="text1"><input type="text" name="gstNumber" id="gstNumber" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>

                                </tr>
                                <tr> 
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Customer Name</td>
                                    <td class="text1">
                                        <select  align="center" class="form-control" style="width:240px;height:40px" name="customerName" id="customerName" >
                                            <option value=''>--select--</option>                            
                                            <c:if test = "${customerList != null}" >
                                                <c:forEach items="${customerList}" var="cust"> 
                                                    <option value='<c:out value="${cust.custId}" />'><c:out value="${cust.custName}" /></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                    </td>

                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Customer Mobile </td>
                                    <td class="text1"><input type="text" name="customerPhone" id="customerPhone" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>


                                </tr>

                                <tr>
                                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Status</td>
                                    <td class="text1">
                                        <select  align="center" class="form-control" style="width:240px;height:40px" name="status" id="ActiveInd" >
                                            <option value='Y'>Active</option>
                                            <option value='N' id="inActive" style="display: none">In-Active</option>
                                        </select>
                                    </td>
                                </tr>

                            </table>
                            </tr>
                            <tr>
                                <td>
                                    <br>
                                    <center>
                                        <input type="button" class="btn btn-success" value="Save" name="Submit" onClick="submitPage()">
                                    </center>
                                </td>
                            </tr>
                        </table>
                        <br>

                        <table class="table table-info mb30 table-hover" id="table" > 
                            <thead>

                                <tr height="30" style="color: white">
                                    <th>S.No</th>
                                    <th>Dealer Name</th>
                                    <th>Dealer Code</th>
                                    <th>Industries</th>
                                    <th>District</th>
                                    <th>Street</th>
                                    <th>City</th>
                                    <th>Postal Code</th>
                                    <th>Phone No</th>
                                    <th>Alternate Phone No</th>
                                    <th>GST Number</th>
                                    <th>Customer Name</th>
                                    <th>Customer Mobile</th>
                                </tr>
                            </thead>
                            <tbody>


                                <% int sno = 0;%>
                                <c:if test = "${dealerCategoryList != null}">
                                    <c:forEach items="${dealerCategoryList}" var="pc">
                                        <%
                                                    sno++;
                                                    String className = "text1";
                                                    if ((sno % 1) == 0) {
                                                        className = "text1";
                                                    } else {
                                                        className = "text2";
                                                    }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.dealerName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.dealerCode}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.industries}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.district}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.street}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.city}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.pincode}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.phone}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.phone1}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.gstNo}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.customerName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${pc.customerMobile}" /></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>
                                           

                        <input type="hidden" name="count" id="count" value="<%=sno%>" />

                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>