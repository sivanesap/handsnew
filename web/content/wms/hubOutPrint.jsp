<html>
    <head>
        <%@page import="java.util.Iterator"%>
        <%@page import="java.util.ArrayList"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <style type='text/css'>
        body{
            color:#000000; background-color:#ffffff;
            font-family:arial, verdana, sans-serif; font-size:10pt;}

        fieldset {
            font-size:10pt;
            padding:5px;
            width:700px;
            line-height:5;}

        label:hover {cursor:hand;}

        .flex-item2 {
            background: white;
            width: 100%;
            height: 20px;
            line-height: 20px;
            color: black;
            font-size: 1em;
            text-align: center;
            marker-attachment: none;
        }
        .flex-item3 {
            background: white;
            margin-left: 0%;
            padding-left: 7px;
            margin-left: 7%;
            width: 40%;
            height: 160px;
            line-height: 19px;
            float: left;
            color: black;
            font-size: 1em;
            text-align: left;
            border: 2px solid black;
            marker-attachment: none;
        }
        .flex-item4 {
            background: white;
            float: right;
            margin-left: 3%;
            margin-right: 7%;
            width: 40%;
            height: 160px;
            padding-left: 4px;
            line-height: 19px;
            color: black;
            font-size: 1em;
            text-align: left;
            border: 2px solid black;
            marker-attachment: none;
        }
        .flex-item5 {
            background: white;
            margin-left: 0%;
            margin-right: 4%;
            width: 40%;
            float: left;
            height: 180px;
            padding-left: 7px;
            line-height: 19px;
            color: black;
            font-size: 1em;
            text-align: center;
            marker-attachment: none;
        }
        .flex-item6 {
            background: white;
            margin-left: 0%;
            margin-right: 4%;
            width: 40%;
            float: right;
            height: 180px;
            padding-left: 7px;
            line-height: 19px;
            color: black;
            font-size: 1em;
            text-align: center;
            marker-attachment: none;
        }
        table, td {
            border:2px solid #999999;
            padding:5px;
            border-collapse: collapse;
        }
        th {
            border: 2px solid white;
            padding:5px;
            border-collapse: collapse;
        }
    </style>

    <style type="text/css">
        @media print {
            #printbtn {
                display :  none;
            }
        }
    </style>
    <style type="text/css" media="print">
        @media print
        {
            @page {
                margin-top: 0;
                margin-bottom: 0;
            }
        }
    </style>
</head>

<body>
    <table>
        <div style="width:100%">
            <div style="width:70%;float: left;text-align: center;padding-left: 100px">
                <font size="4.5"><b><br>DISPATCH DETAILS</b></font><br><font size="4.5"><c:out value="${companyName}"/></font><br>Transporter ID : 24AADCH5462M1ZD<br><br>
            </div>
            <div style="float:right;width:10%;padding-right: 30px">
                <br>
                <img src="images/HSSupplyLogo.png" style="width:58px;height:60px">
                <br>
            </div>
        </div>
        <br>
        <div class='flex-item2'><br>
            <table class="newtable" style="width:86%;margin-left: 7%;margin-right: 7%;font-size: 13px">
                <tr colspan="3">
                    <td style="text-align: left;width:25%;border-right: none">Dispatch Id: <b><c:out value="${dispatchId}"/></b></td>
                    <td style="text-align: left;width:25%;padding-left: 20px;border-left: none;border-right: none">Date: <b><c:out value="${date}"/></b></td>
                    <td style="text-align: right;width:25%;border-left: none"></td>
                </tr>
                <tr colspan="3">
                    <td style="text-align: left;width:25%;border-right: none">Driver: <b><c:out value="${driverName}"/></b></td>
                    <td style="text-align: left;width:25%;padding-left: 20px;border-left: none;border-right: none">Mobile No: <b><c:out value="${driverMobile}"/></b></td>
                    <td style="text-align: right;width:25%;border-left: none">Lorry No: <b><c:out value="${vehicleNo}"/></b></td>
                </tr>
            </table>
        </div>
        <br>
        <div>
            <div class="flex-container2">
                <div class="flex-item3">
                    <u>From (Sending Hub)</u><br>
                    <br><b>Company :</b> <c:out value="${fromWhName}"/>
                    <br><b>Gst No : </b> <c:out value="${fromWhGst}"/>
                    <br><b>Mobile No : </b><c:out value="${fromWhPhone}"/>
                    <br><b>Address : </b><c:out value="${fromWhAddress}"/>
                    <br><br><br>
                </div>
                &nbsp;
                <div class="flex-item4">
                    <u>To (Receiving Hub)</u><br>
                    <br><b>Company : </b><c:out value="${toWhName}"/>
                    <br><b>Gst No : </b><c:out value="${toWhGst}"/>
                    <br><b>Mobile No : </b><c:out value="${toWhPhone}"/>
                    <br><b>Address : </b><c:out value="${toWhAddress}"/>
                    <br><br><br>
                </div>
            </div>
        </div>
        <br>
        <div style="padding-top:150px">
            <table style="width:86%;margin-left: 7%;margin-right: 7%;font-size: 13px">
                <thead style="background-color:#999999;color:white;text-align: center;border: 1.5px solid black">
                <tr colspan="7">
                    <th><b>LR No</b></th>
                    <th><b>Invoice No</b></th>
                    <th><b>Customer Name</b></th>
                    <th><b>City</b></th>
                    <th><b>Pincode</b></th>
                    <th><b>Inv Qty</b></th>
                    <th><b>Inv Amount</b></th>
                </tr>
                </thead>
                    <tbody>
                <c:forEach items="${hubOutList}" var="if">
                    <tr colspan="7">
                        <td><c:out value="${if.lrNo}"/></td>
                        <td><c:out value="${if.invoiceNo}"/></td>
                        <td><c:out value="${if.customerName}"/></td>
                        <td><c:out value="${if.city}"/></td>
                        <td><c:out value="${if.pincode}"/></td>
                        <td><c:out value="${if.qty}"/></td>
                        <td><c:out value="${if.grossValue}"/></td>
                        <c:set var="qty" value="${qty+if.qty}"/>
                        <c:set var="value" value="${value+if.grossValue}"/>
                    </tr>
                </c:forEach>
                    </tbody>
                <tr>
                    <td align="left" colspan="5"><b>&nbsp;Total</b></td>
                    <td colspan="1"><b><c:out value="${qty}"/></b></td>
                    <td colspan="1"><b><c:out value="${value}"/></b></td>
                <tr>
            </table>
        </div>
        <br>
        <br>
        <br>
        <br>
        <div>
            <div>
                <div class="flex-item5">
                    <b>Sender Signature</b><br>
                </div>
                &nbsp;
                <div class="flex-item6">
                    <b>Receiver Signature</b><br>
                </div>
            </div>
        </div>

    </table>
</body>