<%-- 
    Document   : addAsnRecords.jsp
    Created on : Mar 10, 2021, 1:14:56 PM
    Author     : throttle
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Gate In Details"  text="Stock Transfer"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Stock Transfer</a></li>
            <li class="active">Stock Transfer</li>
        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="">

                <form name="stockDetails" method="post">

                    <table class="table table-info mb30 table-hover" id="serial">
                        <div align="center" style="height:15px;" id="statusMsg">&nbsp;&nbsp

                        </div>
                        <tr>
                            <td><text style="color:red">*</text>
                                From  Warehouse
                            </td>
                            <td>
                                <input type="hidden" id="userWhId" name="userWhId" value="<c:out value="${userWhId}"/>" >
                                <c:out value="${userWhName}"/>
                            </td>
                            <td><text style="color:red">*</text>
                                Item 
                            </td>
                            <td>
                                <select  style="width:200px;height:40px" type="text"  id="itemId" name="itemId" onchange="setval(this.value);"  >
                                    <option style="text-align:center" value="">------select one------</option>
                                    <c:forEach items="${getItemDetails}" var="list">

                                        <option value="<c:out value="${list.itemId}"/>-<c:out value="${list.stockId}"/>-<c:out value="${list.qty}"/>"><c:out value="${list.itemName}"/></option>

                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><text style="color:red">*</text>
                                Stock
                            </td>
                            <td>
                                <input style="width:200px;height:40px" readonly type="text" class="form-control" id="Quantity" name="Quantity" value=""/>
                                <input style="width:200px;height:30px" type="hidden" class="form-control" id="stockId" name="stockId"/>
                                <input style="width:200px;height:30px" type="hidden" class="form-control" id="itemid" name="itemid"/>

                            </td>

                            <td><text style="color:red">*</text>
                                Transfer Quantity
                            </td>
                            <td>
                                <input style="width:200px;height:40px" type="text" id="TransferQty" name="TransferQty" onchange="getInput(this.value);"/>
                            </td>
                        </tr>
                        <script>
                                document.getElementById("itemId").value = '<c:out value="${item1}"/>';
                                document.getElementById("Quantity").value = '<c:out value="${Quantity}"/>';
                                document.getElementById("TransferQty").value = '<c:out value="${TransferQty}"/>';
                        </script>
                        <script>
                                function setval(val) {
                                    var value = val.split("-");
                                    var itemId = value[0];
                                    var Qty = value[2];
                                    var stockId = value[1];
                                    document.getElementById("Quantity").value = Qty;
                                    document.getElementById("stockId").value = stockId;
                                    document.getElementById("itemid").value = itemId;
                                }
                        </script>

                        <tr>

                        </tr>
                        <tr>

                        </tr>

                        <tr>
                            <td><text style="color:red">*</text>
                                To Warehouse
                            </td>
                            <td>
                                <select  style="width:200px;height:40px" type="text" id="warehouseToId" name="warehouseToId">
                                    <option value="">------select one------</option>
                                    <c:forEach items="${getWareHouseList}" var="war">
                                        <option value="<c:out value="${war.warehouseId}"/>">
                                            <c:out value="${war.whName}"/></option>



                                    </c:forEach>



                                </select>
                            </td>

                            <td class="classText1">Serial No</td>
                            <td><input name="serial" id="serial" type="text" style="font-size:15px;width:200px;height:40px" class="form-control" style="width:250px;height:40px" value="" onchange="scanningArray(this.value);" size="20"></td>

                        </tr>
                        <tr>

                        </tr>


                    </table>

                    <c:if test="${getSerialNumber == null }" >
                        <center><font color="red" size="2"><spring:message code="trucks.label.NoRecordsFound"  text="default text"/>  </font></center>
                        </c:if>
                    <table class="table table-info mb30 table-bordered" id="table" >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>serial No</th>
                                <th align="center">Scanned</th>

                            </tr>

                        </thead>
                        <c:if test="${getSerialNumber != null}">
                            <% int index = 0;
                                    int sno = 1;
                                      int ser=1;
                                          
                                           int check = 0;
                            %>
                            <tbody>

                                <c:forEach items="${getSerialNumber}" var="asn">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td>
                                            <%=sno%>
                                        </td>
                                        <td ><c:out value="${asn.serialNumber}"/></td>
                                <input type="hidden" id="serNo<%=ser%>" name="serNo" value="<c:out value="${asn.serialNumber}"/>">
                                <% ser++; %>
                                <td><input type="checkbox" style="color:dark" id="selectSerial<%=index%>" name="selectSerial" value="<c:out value="${asn.serialNumber}"/>" disabled=""></td>
                                <input type="hidden" id="selectedIndex<%=index%>" name="selectedIndex" value="0">
                                </tr>

                                <%index++;%>
                                <%sno++;%>

                            </c:forEach>    
                            <input type="hidden" id="ser" name="ser" value="<%=ser%>">
                            <input type="hidden" id="ind" name="ind" value="<%=index%>">

                            </tbody>
                        </c:if>
                    </table>      
                </form>


            <center>
                <input type="button" value="save"  class= btn btn-success onclick="submitPage();">

            </center>
            <script>
                    var arr1 = [];
                    var arr2 = [];
                    var ser = document.getElementById("ser").value;
                    var index = document.getElementById("ind").value;
                    var i = 0;
                    for (var x = 1; x <= ser; x++) {

                        arr1[i] = document.getElementById("serNo" + x).value;
                        i++;
                    }
                    function scanningArray(value) {
                        var transfer = document.getElementById("TransferQty").value;
                        var qty = arr2.length;
                        if (qty < transfer) {
                            if (arr1.indexOf(value) != -1) {
                                if (arr2.indexOf(value) != -1) {
                                    alert("Already Scanned");
                                    document.getElementById("serial").value = "";
                                } else {
                                    arr2.push(value);
                                    for (var k = 0; k < index; k++) {
                                        if (document.getElementById("selectSerial" + k).value == value) {
                                            document.getElementById("selectSerial" + k).checked = true;
                                            document.getElementById("selectedIndex" + k).value = 1;
                                        }
                                    }
                                    document.getElementById("serial").value = "";
                                }
                            } else {
                                alert("Not a Undelivered Product");
                                document.getElementById("serial").value = "";
                            }
                        } else {
                            alert("Transfer Quantity Input Exceeded");
                        }
                    }
                    function checkSelectStatus(sno, obj) {

                        if (obj.checked == true) {
                            document.getElementById("selectedStatus" + sno).value = 1;
                        } else if (obj.checked == false) {
                            document.getElementById("selectedStatus" + sno).value = 0;
                        }
                    }
            </script>


            <script>


                    function getInput(value) {
                        var qty = document.getElementById("Quantity").value;

                        if (parseInt(value) > parseInt(qty)) {
                            alert("please enter the valid")
                        } else {
                            load();
                        }
                    }

                    function load() {

                        document.stockDetails.action = '/throttle/viewStockDetails.do?&param=search';
                        document.stockDetails.submit();
                    }





            </script>

            <script>
                    function submitPage() {
                        var TransferQty = document.getElementById("TransferQty").value;


                        if (arr2.length == TransferQty) {
                            document.stockDetails.action = '/throttle/saveTransferDetails.do';
                            document.stockDetails.submit();
                        } else {
                            alert("please Check Quantity");
                        }
                }
            </script>


            </body>


        </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>


