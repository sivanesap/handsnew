<%-- 
    Document   : orderApproval.jsp
    Created on : Mar 9, 2021, 8:02:14 PM
    Author     : DELL
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript">
    function uploadContract() {
        document.upload.action = "/throttle/uploadReplacementOrder.do";
        document.upload.method = "post";
        document.upload.submit();
    }
    
      function uploadPage() {
        document.upload.action = "/throttle/saveReplacementOrderUpload.do";
        document.upload.method = "post";
        document.upload.submit();
    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="Replacement Order Upload"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="Replacement Order Upload"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >
                <form name="upload"  method="post" enctype="multipart/form-data">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    <table class="table table-info mb30 table-hover" id="uploadTable"  >
                        <thead> 
                            <tr>
                                <th  colspan="3">Replacement Order Upload Details</th>
                            </tr>
                        </thead>
                        <tr>
                            <td >Select Excel</td>
                            <td ><input type="file" name="importCnote" id="importCnote"  class="importCnote"><font size="1"><u><a href="uploadedxls/Replacement Order Upload.xls" download>sample file</a></u></font></td>                             
                            <td colspan="0"><input type="button" class="btn btn-success" value="Upload" name="UploadContract" id="UploadContract" onclick="uploadContract();"></td>
                        </tr>
                        <tr>
                        </tr>
                    </table>
                    <div id="ptp" style="overflow: auto">
                        <div class="inpad">

                            <c:if test = "${getreplacementList != null}" >

                                <table class="table table-info mb30 table-hover" id="table" >

                                    <thead>
                                        <tr height="40">
                                            <th  height="30" >S No</th>
                                            <th  height="30" >Shipment Id</th>
                                            <th  height="30" >Shipment Type</th>
                                            <th  height="30" >Shipment Tier Type</th>
                                            <th  height="30" >Reverse Shipment Id</th>
                                            <th  height="30" >Bag Status</th>
                                            <th  height="30" >Type</th>
                                            <th  height="30" >Price</th>
                                            <th  height="30" >Latest Status</th>
                                            <th  height="30" >Delivery Hub</th>                                                    
                                            <th  height="30" >Current Location</th>
                                            <th  height="30" >Latest Update Date Time</th>
                                            <th  height="30" >First Received Hub</th>
                                            <th  height="30" >First Receive Date Time</th>
                                            <th  height="30" >Hub Notes</th>
                                            <th  height="30" >Cs Notes</th>
                                            <th  height="30" >Number of Attempts</th>
                                            <th  height="30" >Bag Id</th>
                                            <th  height="30" >Consignment Id</th>
                                            <th  height="30" >Customer Promise Date</th>
                                            <th  height="30" >Logistics Promise Date</th>
                                            <th  height="30" >On Hold By Ops Reason</th>
                                            <th  height="30" >On Hold By Ops Date</th>
                                            <th  height="30" >First Assigned Hub Name</th>
                                            <th  height="30" >Delivery Pin Code</th>
                                            <th  height="30" >Last Received Date Time</th>
                                            <th  height="30" >Result Status</th>
                                        </tr>
                                    </thead>
                                    <% int index = 0; 
                                     int sno = 1;
                                    %> 
                                    <tbody>

                                        <c:forEach items= "${getreplacementList}" var="contractList">
                                            <tr height="30">
                                                <td align="left"><%=sno%></td>
                                                <td align="left"   ><c:out value="${contractList.shipmentId}"/></td>
                                                <td align="left"   ><c:out value="${contractList.shipmentType}"/></td>
                                                <td align="left"   ><c:out value="${contractList.shipmentTierType}"/></td>
                                                <td align="left"   ><c:out value="${contractList.reverseShipmentId}"/></td>

                                                <td align="left"   ><c:out value="${contractList.bagStatus}"/></td>
                                                <td align="left"   ><c:out value="${contractList.type}"/></td>
                                                <td align="left"   ><c:out value="${contractList.price}"/></td>
                                                <td align="left"   ><c:out value="${contractList.latestStatus}"/></td>
                                                <td align="left"   ><c:out value="${contractList.deliveryHub}"/></td>
                                                <td align="left"   ><c:out value="${contractList.currentLocation}"/></td>

                                                <td align="left"   ><c:out value="${contractList.latestUpdateDateTime}"/></td>
                                                <td align="left"   ><c:out value="${contractList.firstReceivedHub}"/></td>
                                                <td align="left"   ><c:out value="${contractList.firstReceiveDateTime}"/></td>
                                                <td align="left"   ><c:out value="${contractList.hubNotes}"/></td>
                                                <td align="left"   ><c:out value="${contractList.csNotes}"/></td>
                                                <td align="left"   ><c:out value="${contractList.numberofAttempts}"/></td>
                                                <td align="left"   ><c:out value="${contractList.bagId}"/></td>
                                                <td align="left"   ><c:out value="${contractList.consignmentId}"/></td>
                                                <td align="left"   ><c:out value="${contractList.customerPromiseDate}"/></td>
                                                <td align="left"   ><c:out value="${contractList.logisticsPromiseDate}"/></td>
                                                <td align="left"   ><c:out value="${contractList.onHoldByOpsReason}"/></td>
                                                <td align="left"   ><c:out value="${contractList.onHoldByOpsDate}"/></td>
                                                <td align="left"   ><c:out value="${contractList.firstAssignedHubName}"/></td>
                                                <td align="left"   ><c:out value="${contractList.deliveryPinCode}"/></td>
                                                <td align="left"   ><c:out value="${contractList.lastReceivedDateTime}"/></td>
                                                
                                                
                                                <c:if test="${contractList.error==0}">
                                                    <td style="color:green">
                                                        Ready To Submit
                                                    </td>
                                                 </c:if>
                                                
                                                <c:if test="${contractList.error==1}">
                                                    <td style="color:red">
                                                       Invalid Reverse Shipment Id
                                                    </td>
                                                 </c:if>  
                                                    
                                                 <c:if test="${contractList.error==2}">
                                                    <td style="color:red">
                                                       Shipment Id Already Exists
                                                    </td>
                                                 </c:if> 
                                                    
                                                <c:if test="${contractList.error==3}">
                                                    <td style="color:red">
                                                       Delivery Hub not Exists in Master
                                                    </td>
                                                 </c:if>   
                                        

                                            </tr> 
                                            <%
                                            sno++;
                                            index++;
                                            %>
                                        </c:forEach>
                                    </tbody>
                                </table>

                                <center>

                                    <td colspan="0"><input type="button" class="btn btn-success" value="Submit" name="submitPage" id="submitPage" onclick="uploadPage();"></td>
                                </center>
                            </c:if>
                        </div>
                    </div>     

                </form>
            </body>
        </div>
    </div>
</div>


    <%@ include file="/content/common/NewDesign/settings.jsp" %>


