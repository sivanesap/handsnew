<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">


        function viewTripDetails(tripId) {
            window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function viewVehicleDetails(vehicleId) {
            window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script type="text/javascript">
        function setValues() {
            if ('<%=request.getAttribute("vehicleTypeId")%>' != 'null') {
                document.getElementById('vehicleTypeId').value = '<%=request.getAttribute("vehicleTypeId")%>';
            }
            if ('<%=request.getAttribute("fromDate")%>' != 'null') {
                document.getElementById('startDateFrom').value = '<%=request.getAttribute("fromDate")%>';
            }
            if ('<%=request.getAttribute("toDate")%>' != 'null') {
                document.getElementById('startDateTo').value = '<%=request.getAttribute("toDate")%>';
            }
            if ('<%=request.getAttribute("endDateFrom")%>' != 'null') {
                document.getElementById('endDateFrom').value = '<%=request.getAttribute("endDateFrom")%>';
            }
            if ('<%=request.getAttribute("endDateTo")%>' != 'null') {
                document.getElementById('endDateTo').value = '<%=request.getAttribute("endDateTo")%>';
            }
            if ('<%=request.getAttribute("fleetCenterId")%>' != 'null') {
                document.getElementById('fleetCenterId').value = '<%=request.getAttribute("fleetCenterId")%>';
            }
            if ('<%=request.getAttribute("vehicleId")%>' != 'null') {
                document.getElementById('vehicleId').value = '<%=request.getAttribute("vehicleId")%>';
            }
            if ('<%=request.getAttribute("customerId")%>' != 'null') {
                document.getElementById('customerId').value = '<%=request.getAttribute("customerId")%>';
            }
            if ('<%=request.getAttribute("tripStatusId")%>' != 'null') {
                document.getElementById('tripStatusId').value = '<%=request.getAttribute("tripStatusId")%>';
            }
            if ('<%=request.getAttribute("tripStatusIdTo")%>' != 'null') {
                document.getElementById('tripStatusIdTo').value = '<%=request.getAttribute("tripStatusIdTo")%>';
            }
            if ('<%=request.getAttribute("not")%>' != 'null' && '<%=request.getAttribute("not")%>' != '') {
                document.getElementById('not').checked = true;
            }
            if ('<%=request.getAttribute("page")%>' != 'null') {
                var page = '<%=request.getAttribute("page")%>';
                if (page == 1) {
                    submitPage('search');
                }
            }


        }

        function searchPage(val) {

            if (val == 'ExportExcel') {
                document.tripSheet.action = '/throttle/handleViewTripSheetReport.do?param=ExportExcel';
                document.tripSheet.submit();
            } else {
                document.tripSheet.action = '/throttle/grnView.do?param=search';
                document.tripSheet.submit();
            }
        }

        function serialExportExcel(grnId, grnNo, grnDate, asnNo, asnDate) {
            document.tripSheet.action = '/throttle/viewSerialNumberDetails.do?grnId=' + grnId + '&param=ExportExcel&grnNo=' + grnNo + '&grnDate=' + grnDate + '&asnDate=' + asnDate + '&asnNo=' + asnNo;
            document.tripSheet.submit();
        }
        function viewSerialNumber(grnId) {
            window.open('/throttle/viewSerialNumberDetails.do?grnId=' + grnId + '&param=Search', 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
    </script>

    <style type="text/css">





        .container {width: 960px; margin: 0 auto; overflow: hidden;}
        .content {width:800px; margin:0 auto; padding-top:50px;}
        .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

        /* STOP ANIMATION */



        /* Second Loadin Circle */

        .circle1 {
            background-color: rgba(0,0,0,0);
            border:5px solid rgba(100,183,229,0.9);
            opacity:.9;
            border-left:5px solid rgba(0,0,0,0);
            /*	border-right:5px solid rgba(0,0,0,0);*/
            border-radius:50px;
            /*box-shadow: 0 0 15px #2187e7; */
            /*	box-shadow: 0 0 15px blue;*/
            width:40px;
            height:40px;
            margin:0 auto;
            position:relative;
            top:-50px;
            -moz-animation:spinoffPulse 1s infinite linear;
            -webkit-animation:spinoffPulse 1s infinite linear;
            -ms-animation:spinoffPulse 1s infinite linear;
            -o-animation:spinoffPulse 1s infinite linear;
        }

        @-moz-keyframes spinoffPulse {
            0% { -moz-transform:rotate(0deg); }
            100% { -moz-transform:rotate(360deg);  }
        }
        @-webkit-keyframes spinoffPulse {
            0% { -webkit-transform:rotate(0deg); }
            100% { -webkit-transform:rotate(360deg);  }
        }
        @-ms-keyframes spinoffPulse {
            0% { -ms-transform:rotate(0deg); }
            100% { -ms-transform:rotate(360deg);  }
        }
        @-o-keyframes spinoffPulse {
            0% { -o-transform:rotate(0deg); }
            100% { -o-transform:rotate(360deg);  }
        }

    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Grn View Report </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Reports"/></a></li>
                <li class="">Grn View Report</li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body"><body onload="setValues();
                    sorter.size(20);">
                    <form name="tripSheet" method="post">
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <%@include file="/content/common/message.jsp" %>
                        <table class="table table-info mb30 table-hover">
                            <thead><tr><th colspan="8">Grn View Report</th></tr></thead>

                            <tr>
                                <td><font color="red">*</font>From Date </td>
                                <td height="30"><input name="fromDate" id="fromDate" type="text"  class="datepicker , form-control" style="width:250px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>" autocomplete="off"></td>
                                <td></td>
                                <td><font color="red">*</font>To Date </td>
                                <td height="30">
                                    <input name="toDate" id="toDate" type="text" class="datepicker , form-control" style="width:250px;height:40px" class="datepicker" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>">
                                </td>
                            </tr>
                            <tr>
                                <td>Warehouse Name</td>
                                <td height="30"> <select name="fleetCenterId" id="fleetCenterId"  class="form-control" style="width:250px;height:40px" style="height:20px; width:122px;" >
                                        <c:if test="${getWareHouseList != null}">
                                            <option value="" selected>--Select--</option>
                                            <c:forEach items="${getWareHouseList}" var="companyList">
                                                <option value='<c:out value="${companyList.warehouseId}"/>'><c:out value="${companyList.whName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select></td>
                                <td></td>
                                <td>City Name</td>
                                <td height="30"> <select name="cityList" id="cityList"  class="form-control" style="width:250px;height:40px" style="height:20px; width:122px;" >
                                        <c:if test="${getCityList != null}">
                                            <option value="" selected>--Select--</option>
                                            <c:forEach items="${getCityList}" var="companyList">
                                                <option value='<c:out value="${companyList.id}"/>'><c:out value="${companyList.city}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select></td>
                            </tr>
                            <tr>             

                                <td  colspan="6"align="center">
                                    <input type="button" class="btn btn-success"   value="Search" onclick="searchPage('Search')">

                                </td>
                            </tr>
                        </table>

                        <c:if test = "${grnViewDetails == null}" >
                            <center>
                                <font color="blue">Please Wait Your Request is Processing</font>
                                <div class="container">
                                    <div class="content">
                                        <div class="circle"></div>
                                        <div class="circle1"></div>
                                    </div>
                                </div>
                            </center>
                        </c:if>
                        <c:if test="${grnViewDetails != null}">
                            <table class="table table-info mb30 table-hover" id="table"  >
                                <thead>
                                    <tr height="70">
                                        <th>Sno</th>
                                        <th>GRN No</th>
                                        <th>GRN Date</th>                        
                                        <th>ASN No/PO No</th>                                                
                                        <th>PO Request ID</th>                        
                                        <th>ASN Date</th>
                                        <th>Invoice Number</th>
                                        <th>Invoice Date</th>
                                        <th>PO Qty</th>
                                        <th>Received Qty</th>                        
                                        <th>Accepted Qty</th>
                                        <th>Damaged Qty</th>
                                        <th>Excess Qty</th>
                                        <th>Invoice View</th>
                                        <th>PO View</th>
                                        <th>Serial Number</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <% int index = 1;%>
                                    <c:forEach items="${grnViewDetails}" var="tripDetails">
                                        <%
                                                    String className = "text1";
                                                    if ((index % 2) == 0) {
                                                        className = "text1";
                                                    } else {
                                                        className = "text2";
                                                    }
                                        %>
                                        <tr height="30">
                                            <td class="<%=className%>" width="40" align="left"><%=index%></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.grnNo}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.grnDate}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.asnNo}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.asnReqId}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.asnDate}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.invoiceNo}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.invoiceDate}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.actualQuantity}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.reqQty}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.actQty}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.unusedQty}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.excessQty}"/></td>

                                            <td align="left" >
                                                <c:if test="${tripDetails.path != '' && tripDetails.path != '-'}">
                                                    <a href="<c:out value="${tripDetails.path}"/>" target="_blank" download>
                                                        <span class="label label-Primary"><font size="2"><c:out value="${tripDetails.path}"/></font></span> 
                                                    </a>
                                                </c:if>
                                            </td>
                                            <td align="left" >
                                                <c:if test="${tripDetails.asnPath != ''  && tripDetails.asnPath != '-'}">
                                                    <a href="<c:out value="${tripDetails.asnPath}"/>" target="_blank" download>
                                                        <span class="label label-Primary"><font size="2"><c:out value="${tripDetails.asnPath}"/></font></span> </a>
                                                            </c:if>
                                            </td>

                                            <td align="ceneter">
                                                <a href="#" onclick="viewSerialNumber('<c:out value="${tripDetails.grnId}"/>');">View</a>
                                                <input type="button" class="btn btn-success" name="ExportExcel" id="ExportExcel" onclick="serialExportExcel('<c:out value="${tripDetails.grnId}"/>', '<c:out value="${tripDetails.grnNo}"/>', '<c:out value="${tripDetails.grnDate}"/>', '<c:out value="${tripDetails.asnNo}"/>', '<c:out value="${tripDetails.asnDate}"/>');" value="ExportExcel">
                                            </td>

                                        </tr>

                                        <%index++;%>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </c:if>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>