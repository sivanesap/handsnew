<%--
    Document   : standardChargesMaster
    Created on : Oct 29, 2013, 11:32:08 AM
    Author     : srinivasan
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
  
  $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });
  
  
    //Update Vehicle Planning
    function setValues(expenseId,expenseName,expenseType,sno,activeInd) {
        var count = parseInt(document.getElementById("count").value);
         for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("expenseId").value = expenseId;
        document.getElementById("expenseName").value = expenseName;
        document.getElementById("expenseType").value = expenseType;
        document.getElementById("activeInd").value = activeInd;

    }

    function submitPage() {
            document.expense.action = '/throttle/saveExpenseTypeMaster.do';
            document.expense.submit();
        }
    
    
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body >
        <form name="expense"  method="post" >
            <br>
            <h2>Master >> Expense Master</h2>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>
            <br>

            <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="contenthead" colspan="4" align="center">Expense Type Master</td>
                </tr>
                <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Expense Name</td>
                    <td class="text1"><input type="hidden" name="expenseId" id="expenseId" class="textbox" value=""  >
                        <input name="expenseName" id="expenseName" type="text"  class="textbox" value="">
                    </td>
                     <td class="text1">Expense Type</td>
                    <td class="text1">
                        <select  align="center" class="textbox" name="expenseType" id="expenseType" >
                            <option value='1'>Customs</option>
                            <option value='2'>Handlers</option>
                            <option value='3'>Truck</option>
                        </select>
                    </td>
                </tr>
                     <td class="text2">&nbsp;&nbsp;Active Status</td>
                    <td class="text2">
                        <select  align="center" class="textbox" name="activeInd" id="activeInd" >
                            <option value='Y'>Active</option>
                            <option value='N' id="inActive">In-Active</option>
                        </select>
                    </td>
                    <td colspan="2">&nbsp;</td>
                <tr>
                    
                </tr>
     
                <tr>
                    <td class="text1" colspan="4" align="center"><input type="button" class="button" value="Save" id="save" onClick="submitPage();"   /></td>
                </tr>
                
               
            </table>
            <br>
            <br/>
            <table width="815" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>Expense Name </h3></th>
                        <th><h3>Expense Type </h3></th>
                        <th><h3>Active Status</h3></th>
                       <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <% int sno = 0;%>
                    <c:if test = "${expenseList != null}">
                        <c:forEach items="${expenseList}" var="expense">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno %> </td>
                                <td class="<%=className%>"  align="left">
                                   <c:out value="${expense.expenseName}"/>
                                    </td>
                                <td class="<%=className%>"  align="left">
                                    <c:if test = "${expense.expenseType == 1}">
                                        Customs
                                    </c:if>
                                    <c:if test = "${expense.expenseType == 2}">
                                        Handlers
                                    </c:if>
                                    <c:if test = "${expense.expenseType == 3}">
                                        Truck
                                    </c:if>
                                    </td>
                                <td class="<%=className%>"  align="left">
                                   <c:out value="${expense.activeInd}"/>
                                    </td>
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues('<c:out value="${expense.expenseId}" />', '<c:out value="${expense.expenseName}" />','<c:out value="${expense.expenseType}" />','<%=sno%>','<c:out value="${expense.activeInd}" />');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>
            <input type="hidden" name="count" id="count" value="<%=sno%>" />
            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");</script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        </form>
    </body>
</html>