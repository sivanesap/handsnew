 <%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
//    $(document).ready(function() {
//
//        $("#datepicker").datepicker({
//            showOn: "button",
//            buttonImage: "calendar.gif",
//            buttonImageOnly: true
//
//        });
//
//
//
//    });
//
//    $(function() {
//        //	alert("cv");
//        $(".datepicker").datepicker({
//            /*altField: "#alternate",
//             altFormat: "DD, d MM, yy"*/
//            changeMonth: true, changeYear: true
//        });
//
//    });

    function submitPage()
    {
        var errStr = "";
        var nameCheckStatus = $("#binNameStatus").text();
        if(document.getElementById("binName").value == "") {
            errStr = "Please enter binName.\n";
            alert(errStr);
            document.getElementById("binName").focus();
        }
        else if(document.getElementById("binCapacity").value == "") {
            errStr = "Please enter binCapacity.\n";
            alert(errStr);
            document.getElementById("binCapacity").focus();
        }
        else if(nameCheckStatus != "") {
            errStr ="Product CategoryName Already Exists.\n";
            alert(errStr);
            document.getElementById("binName").focus();
        }
        

        if(errStr == "") {
            document.bin.action=" /throttle/savebinmaster.do";
            document.bin.method="post";
            document.bin.submit();
        }



    }
     function setValues(sno,binName,binId,status,binbreath,binheight,binweight,binCapacity){
 var count = parseInt(document.getElementById("count").value);
         document.getElementById('inActive').style.display = 'block';

        document.getElementById("binId").value = binId;
        document.getElementById("binName").value = binName; 
        document.getElementById("status").value = status;
        document.getElementById("binbreath").value = binbreath;
        document.getElementById("binheight").value = binheight;
        document.getElementById("binweight").value = binweight;
        document.getElementById("binCapacity").value = binCapacity;
        
        for (var i = 1; i <= count; i++) {
            if(i != sno) {
                document.getElementById("edit"+i).checked = false;
            } else {
                document.getElementById("edit"+i).checked = true;
            }
        }
    }







    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }


    function extractNumber(obj, decimalPlaces, allowNegative)
{
	var temp = obj.value;

	// avoid changing things if already formatted correctly
	var reg0Str = '[0-9]*';
	if (decimalPlaces > 0) {
		reg0Str += '\\.?[0-9]{0,' + decimalPlaces + '}';
	} else if (decimalPlaces < 0) {
		reg0Str += '\\.?[0-9]*';
	}
	reg0Str = allowNegative ? '^-?' + reg0Str : '^' + reg0Str;
	reg0Str = reg0Str + '$';
	var reg0 = new RegExp(reg0Str);
	if (reg0.test(temp)) return true;

	// first replace all non numbers
	var reg1Str = '[^0-9' + (decimalPlaces != 0 ? '.' : '') + (allowNegative ? '-' : '') + ']';
	var reg1 = new RegExp(reg1Str, 'g');
	temp = temp.replace(reg1, '');

	if (allowNegative) {
		// replace extra negative
		var hasNegative = temp.length > 0 && temp.charAt(0) == '-';
		var reg2 = /-/g;
		temp = temp.replace(reg2, '');
		if (hasNegative) temp = '-' + temp;
	}

	if (decimalPlaces != 0) {
		var reg3 = /\./g;
		var reg3Array = reg3.exec(temp);
		if (reg3Array != null) {
			// keep only first occurrence of .
			//  and the number of places specified by decimalPlaces or the entire string if decimalPlaces < 0
			var reg3Right = temp.substring(reg3Array.index + reg3Array[0].length);
			reg3Right = reg3Right.replace(reg3, '');
			reg3Right = decimalPlaces > 0 ? reg3Right.substring(0, decimalPlaces) : reg3Right;
			temp = temp.substring(0,reg3Array.index) + '.' + reg3Right;
		}
	}

	obj.value = temp;
}
function blockNonNumbers(obj, e, allowDecimal, allowNegative)
{
	var key;
	var isCtrl = false;
	var keychar;
	var reg;

	if(window.event) {
		key = e.keyCode;
		isCtrl = window.event.ctrlKey
	}
	else if(e.which) {
		key = e.which;
		isCtrl = e.ctrlKey;
	}

	if (isNaN(key)) return true;

	keychar = String.fromCharCode(key);

	// check for backspace or delete, or if Ctrl was pressed
	if (key == 8 || isCtrl)
	{
		return true;
	}

	reg = /\d/;
	var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
	var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;

	return isFirstN || isFirstD || reg.test(keychar);
}

 var httpRequest;
    function checkbinName() {

        var binName = document.getElementById('binName').value;

            var url = '/throttle/checkbinName.do?binName=' + binName ;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);

    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#binNameStatus").text('Please Check Product Category Name: ' + val+' is Already Exists');
                } else {
                    $("#nameStatus").hide();
                    $("#binNameStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Bin Master" text="Bin Master"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Bin Master" text="Bin Master"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">
    <body onload="document.bin.binName.focus();">

        <form name="bin"  method="post" >
            
            <%@ include file="/content/common/message.jsp" %>

            <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                <input type="hidden" name="binId" id="binId" value=""  />
                <tr>
                <!--<table  border="0" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">-->
                    <table class="table table-info mb30 table-hover" id="bg" >
                        <thead>
                <tr>
                    <th class="contenthead" colspan="8" >Bin Master</th>
                </tr>
                        </thead>
                <tr>
                    <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="binNameStatus" style="color: red"></label></td>
                </tr>
                <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Bin Name</td>
                    <td class="text1"><input type="text" name="binName" id="binName" class="form-control" style="width:240px;height:40px" maxlength="50"  onchange="checkbinName();" autocomplete="off"/></td>
                    
                     <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Capacity</td>
                    <td class="text1"><input type="text" name="binCapacity" id="binCapacity" class="form-control" style="width:240px;height:40px" maxlength="50"   autocomplete="off"/></td>
                   
</tr>
 
               
                <tr>
                    <td class="text1">&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                    <td class="text1">
                        <select  align="center" class="form-control" style="width:240px;height:40px" name="status" id="ActiveInd" >
                            <option value='Y'>Active</option>
                            <option value='N' id="inActive" style="display: none">In-Active</option>
                        </select>
                    </td>
                     
                     <td class="text1">&nbsp;&nbsp;<font color="red">*</font>breath</td>
                    <td class="text1"><input type="text" name="binbreath" id="binbreath" class="form-control" style="width:240px;height:40px" maxlength="50"   autocomplete="off"/></td>
                </tr>
                <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>height</td>
                    <td class="text1"><input type="text" name="binheight" id="binheight" class="form-control" style="width:240px;height:40px" maxlength="50"   autocomplete="off"/></td>
               
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>length</td>
                    <td class="text1"><input type="text" name="binweight" id="binweight" class="form-control" style="width:240px;height:40px" maxlength="50"   autocomplete="off"/></td>
                
                     </tr>
                    </table>
                </tr>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button" class="btn btn-success" value="Save" name="Submit" onClick="submitPage();">
                        </center>
                    </td>
                </tr>
            </table>
            <br>
            <br>


            <h2 align="center">List The Bin Category</h2>


            <!--<table width="815" align="center" border="0" id="table" class="sortable">-->
             <c:if test = "${productCategoryList != null}">
                       <table class="table table-info mb30 table-hover" id="table1"  >	
                <thead>
                          <tr height="30">
                        <th>S.No</th>
                        <th>Bin Name</th>  
                        <th>Breadth</th> 
                        <th>Height </th> 
                        <th>Length</th>
                        <th>Capacity</th>
                        <th>Select</th>
                    </tr>
                </thead>
                <%
                                    int sno = 0;
                                %>
                <tbody>
                       <c:forEach items="${productCategoryList}" var="pc">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.binName}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.binbreath}" /></td>  
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.binheight}" /></td>  
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.binweight}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.binCapacity}" /></td> 
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues( <%= sno%>,'<c:out value="${pc.binName}" />','<c:out value="${pc.binId}" />','<c:out value="${pc.status}" />','<c:out value="${pc.binbreath}" />','<c:out value="${pc.binheight}" />','<c:out value="${pc.binweight}" />','<c:out value="${pc.binCapacity}" />');" /></td>
                            </tr>
                           
                        </c:forEach>
                    </tbody>
            </table>


            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            <br>
            <br>
<script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table1", 0);
            </script>
             </c:if>
        </form>
    </body>
</div>
	</div>
        </div>
        <%@ include file="/content/common/NewDesign/settings.jsp" %>