<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script type="text/javascript">

    function citysubmit() {
//        alert("sucess");
        var errStr = "";
        if (document.getElementById("plantId").value == "0") {
            alert("Please enter plant.\n");
            document.getElementById("plantId").focus();
        } else if (document.getElementById("vehicleNo").value == "") {
            alert("Enter Vehicle No");
            document.getElementById("vehicleNo").focus();
        } else if (document.getElementById("driverName").value == "") {
            alert("Enter Driver Name");
            document.getElementById("driverName").focus();
        } else if (document.getElementById("cellNo").value == "") {
            alert("Enter Driver Mobile");
            document.getElementById("cellNo").focus();
        } else if (document.getElementById("vehicleType").value == "") {
            alert("Enter Vehicle Type");
            document.getElementById("vehicleType").focus();
        } else if (document.getElementById("inDate").value == "") {
            alert("Enter In Date");
            document.getElementById("inDate").focus();
        } else {
            document.vehicleTat.action = "/throttle/saveVehicleTat.do";
            document.vehicleTat.method = "post";
            document.vehicleTat.submit();
        }
    }

    function setValues(sno, tatId, tatType, plantId, vehicleType, driverName, vehicleNo, cellNo, inDate, inTime) {
        var count = parseInt(document.getElementById("count").value);
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }

        document.getElementById("tatId").value = tatId;
        document.getElementById("tatType").value = tatType;
        document.getElementById("plantId").value = plantId;
        document.getElementById("vehicleType").value = vehicleType;
        document.getElementById("driverName").value = driverName;
        document.getElementById("vehicleNo").value = vehicleNo;
        document.getElementById("cellNo").value = cellNo;
        document.getElementById("inDate").value = inDate;
        document.getElementById("inTime").value = inTime;
    }

    function setTat(tat, sno) {
        var tatId = tat;
        var counts = parseInt(document.getElementById("count").value);

        for (i = 1; i <= counts; i++) {
            if (i != sno) {
                document.getElementById("hubOut" + i).checked = false;
            } else {
                document.getElementById("hubOut" + i).checked = true;
            }
        }

        document.getElementById("tatId").value = tatId;


    }
    function updateoutTime() {
        document.vehicleTat.action = "/throttle/saveVehicleTat.do?param=update";
        document.vehicleTat.method = "post";
        document.vehicleTat.submit();

    }

    function checkExistingVehicle(value) {
        $.ajax({
            url: '/throttle/checkConnectExistingVehicleTat.do',
            dataType: "json",
            data: {
                vehicleNo: value,
                status: '1'
            },
            success: function(data, textStatus, jqXHR) {
                if(parseInt(data.count)>0){
                    alert("Close Existing Vehicle");
                    document.getElementById("vehicleNo").value="";
                    document.getElementById("vehicleNo").focus();
                }
            }
        })
    }
</script>




<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="Vehicle TAT"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="INBOUND"/></a></li>
            <li class=""><spring:message code="hrms.label.Vehicle TAT" text="Vehicle TAT"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >
                <form name="vehicleTat"  method="POST">

                    <%@ include file="/content/common/message.jsp" %>


                    <table class="table table-info mb30 table-hover" id="bg" >	
                        <input type="hidden" name="tatId" id="tatId" value=""  />
                        <thead>
                        <th  colspan="4" >Vehicle TAT Details</th>
                        <th  colspan="4" ></th>
                        </thead>
                        <tr>
                            <td>Type</td>
                            <td><select  align="center" class="form-control" style="width:250px;height:40px" id="tatType" name="tatType" >
                                    <option value='1'>Dispatch Plan</option>
                                    <option value='2'>Inbound</option>
                                    <!--<option value='N' id="inActive" style="display: none">In-Active</option>-->
                                </select>
                            </td>
                            <td ><font color="red">*</font>Plant</td>
                            <td >
                                <select name="plantId" id="plantId" disabled  class="form-control" style="width:250px;height:40px" >
                                    <option value="0">--Select--</option>
                                    <c:forEach items="${getSupplierWhList}" var="st">
                                        <option value="<c:out value="${st.whId}"/>"><c:out value="${st.whName}"/></option>
                                    </c:forEach>
                                </select>
                                <script>
                                    document.getElementById("plantId").value="<c:out value="${hubId}"/>";
                                </script>

                            <td>Vehicle Type</td>
                            <td><select  align="center" class="form-control" style="width:250px;height:40px" id="vehicleType" name="vehicleType" >
                                    <option value=''>------Select One------</option>
                                    <c:forEach items="${vehicleTypeList}" var="type">
                                        <option value='<c:out value="${type.vehicleTypeId}"/>' ><c:out value="${type.vehicleTypeName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>

                        </tr>
                        <tr>
                            <td>Vehicle No</td>
                            <td ><input type="text" name="vehicleNo" id="vehicleNo" value="" onchange="checkExistingVehicle(this.value);" class="form-control" style="width:250px;height:40px"   autocomplete="off" maxlength="50"></td>
                            <td >&nbsp;&nbsp;<font color="red">*</font>Driver Name</td>
                            <td ><input type="text" name="driverName" id="driverName" value="" class="form-control" style="width:250px;height:40px"  autocomplete="off" maxlength="50"></td>


                            <td>Cell No</td>
                            <td><input type="text" name="cellNo" id="cellNo" value="" class="form-control" style="width:250px;height:40px"/>
                            </td>


                        </tr>
                        <tr>
                            <td><font color="red">*</font>In Date</td>
                            <td height="30"><input name="inDate" id="inDate" type="text" class="datepicker form-control" style="width:250px;height:40px"  onclick="ressetDate(this);"></td>
                            <td><font color="red">*</font>In Time</td>
                            <td height="30"><input name="inTime" id="inTime" type="time" class="form-control" style="width:250px;height:40px"  ></td>
                        </tr>

                    </table>
                    </tr>
                    <tr>
                        <td>

                            <center>
                                <input type="button" class="btn btn-success"  value="Save" name="Submit" onClick="citysubmit()">
                            </center>
                        </td>
                    </tr>
                    </table>

                    <br>

                    <!--<h2 align="center">City List</h2>-->

                    <table class="table table-info mb30 table-hover" id="table" >	


                        <thead>

                            <tr >
                                <th > S.No </th>
                                <th> Update Out Time  </th>
                                <th> Edit  </th>
                                <th> Plant </th>
                                <th> TAT Type </th>
                                <th> Vehicle No</th>
                                <th> Vehicle Type </th>
                                <th> Driver Name </th>
                                <th> Cell No </th>
                                <th> In Date & Time </th>
                                <th> OUT Date & Time </th>
                            </tr>
                        </thead>
                        <tbody>



                            <% int sno = 0;%>
                            <c:if test = "${vehicleTatList != null}">
                                <c:forEach items="${vehicleTatList}" var="cml">
                                    <%
                                                sno++;
                                                String className = "text1";
                                                if ((sno % 1) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>

                                    <tr>
                                        <td align="left"> <%= sno%> </td>
                                        <td  align="left" > <span  data-toggle="modal" data-target="#myModal"><input type="checkbox" id="hubOut<%=sno%>" onclick="setTat(<c:out value="${cml.tatId}" />,<%= sno%>);"/></span></td>
                                        <td  align="left" > <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${cml.tatId}" />', '<c:out value="${cml.tatType}" />', '<c:out value="${cml.plantId}" />', '<c:out value="${cml.vehicleType}" />', '<c:out value="${cml.driverName}" />', '<c:out value="${cml.vehicleNo}" />', '<c:out value="${cml.cellNo}" />', '<c:out value="${cml.inDate}" />', '<c:out value="${cml.inTime}" />');" /></td>
                                        <td  align="left"> <c:out value="${cml.whName}" /></td>
                                        <td  align="left"> <c:out value="${cml.tatType}" /></td>
                                        <td  align="left"><c:out value="${cml.vehicleNo}"/></td>
                                        <td align="left"><c:out value="${cml.vehicleTypeName}"/></td>
                                        <td align="left"><c:out value="${cml.driverName}"/></td>
                                        <td align="left"><c:out value="${cml.cellNo}"/></td>
                                        <td align="left"><c:out value="${cml.inDate}"/> <c:out value="${cml.inTime}"/></td>
                                        <td align="left"><c:out value="${cml.outDate}"/> <c:out value="${cml.outTime}"/></td>
                                    </tr>


                                </c:forEach>
                            </tbody>
                            <input type="hidden" name="count" id="count" value="<%=sno%>" />
                        </c:if>
                    </table>
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog modal-lg">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Update Out Date and Time</h4>
                                </div>
                                <div class="modal-body">

                                    <table align="center" border="0" id="tables" class="table table-info mb30 table-hover" style="width:100%" >
                                        <tr>
                                            <td>Out Date</td>
                                            <td><input type="text" class="datepicker form-control" name="outDate" id="outTime" style="width:150px;height:40px"></td>
                                            <td>Out Time</td>
                                            <td><input type="time" class="form-control" name="outTime" id="outTime" style="width:150px;height:40px"></td>
                                        </tr>
                                    </table>

                                    <center>
                                        <input type="button" class="btn btn-success" value="submit" name="Submit" onclick="updateoutTime();" >
                                    </center>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>

                    <div id="myMap" style="width: 1000px; height: 400px; margin-top:20px;"></div>            
                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>