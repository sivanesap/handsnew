<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    function submitPage() {
        $("#hidebut").hide();
        document.enter.action = '/throttle/saveFuelRateUpload.do';
        document.enter.submit();
    }
</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>  
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Upload Fuel Rate" text="Upload Fuel Rate"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="general-forms.html"> <spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
                <li class=""><spring:message code="hrms.label.Upload Fuel Rate" text="Upload Fuel Rate"/></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body>
                    <form name="enter" method="post" enctype="multipart/form-data">
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <br>
                        <br>
                        <br>

                        <%      if (request.getAttribute("errorMessage") != null) {
                                String errorMessage = (String) request.getAttribute("errorMessage");
                        %>
                        <center><b><font color="red" size="3"><%=errorMessage%></font></b></center>
                                    <%}%>
                        <%      if (request.getAttribute("successMessage") != null) {
                                String successMessage = (String) request.getAttribute("successMessage");
                        %>
                        <center><b><font color="Green" size="3"><%=successMessage%></font></b></center>
                                    <%}%>
                        <br>
                        <table class="table table-info mb30 table-hover">
                            <thead> 
                                <tr>
                                    <th  colspan="2">Upload Fuel Rate</th>
                                </tr>
                            </thead>
                            
                            <tr>
                                <td>Base Price</td>
                                <td>61.36
                                <input type="hidden" id="basePrice" name="basePrice" class="form-control" value="61.36"/>
                                <input type="hidden" id="fuelEsc" name="fuelEsc" value="0.5"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Month </td>
                                <td>   <select name="month" class="form-control" style="width:180px;height:40px">
                                        <option value="0">-Select Any one-</option>
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Year </td>
                                <td><select name="year" class="form-control" style="width:180px;height:40px">
                                        <option value="0">-Select Any one-</option>
                                        <option value="2018">2018</option>
                                        <option value="2019" selected>2019</option>
                                        <option value="2020">2020</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td> Excel </td>
                                <td><input type="file" name="importCnote" id="importCnote"  class="importCnote"></td>                             
                            </tr>
                            <tr>
                                <td >  </td>
                                <td ><a href="uploadedxls/FuelUpload.xls">Template XLS</a></td>                             
                            </tr>
                            <tr>
                                <td colspan="2" align="center" >
                                    <input type="button" id="hidebut" class="btn btn-success" value="Submit" name="Submit" onclick="submitPage()"></td>
                            </tr>
                        </table>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>