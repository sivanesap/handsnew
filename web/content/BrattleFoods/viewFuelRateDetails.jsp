<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="java.text.SimpleDateFormat" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true
        });
    });
</script>

<script type="text/javascript">
    function submitPage() {
        $("#hidebut").hide();
        document.fuleprice.action = '/throttle/confirmFuelRateUpload.do';
        document.fuleprice.submit();
    }
</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.FuelPriceMaster" text="FuelPriceMaster"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Fuel" text="Fuel"/></a></li>
                <li class=""><spring:message code="hrms.label.FuelPriceMaster" text="FuelPriceMaster"/></li>		
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body >
                    <%  
                        String menuPath = "Fuel  >>  Manage Fuel Price";
                        request.setAttribute("menuPath", menuPath);
                    %>

                    <%
                       Date today = new Date();
                       SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                       String startDate = sdf.format(today);                                    
                    %>

                    <form name="fuleprice"  method="post" >
                        <br/>
                        <table class="table table-info mb30 table-hover" id="table" >
                            <thead>
                                <tr height="30">
                                    <th>S.No</th>                
                                    <th>Date </th>
                                    <th>Chennai </th>
                                    <th>Mumbai </th>
                                    <th>Delhi </th>
                                    <th>Kolkata </th>
                                    <th>Fuel Price </th>
                                    <th>Average Price </th>
                                    <th>Fuel Escalation % </th>
                                    <th>Fuel Escalation Price</th>
                                    <th>check</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int sno = 1;  int check = 0; String month=""; String year="";%>
                                <c:if test = "${FuelUplodedLogDetails != null}" >
                                    <%
                                                       String className = "text1";
                                                       if ((sno % 1) == 0) {
                                                           className = "text1";
                                                       } else {
                                                           className = "text2";
                                                       }
                                    %>
                                    <c:forEach items="${FuelUplodedLogDetails}" var="fuelPriceMaster">
                                        <tr height="30">
                                            <td><%=sno++%></td>
                                            <td  align="left" ><c:out value="${fuelPriceMaster.effectiveDate}"/></td>
                                            <td  align="left" ><c:out value="${fuelPriceMaster.chennaiPrice}"/></td>
                                            <td  align="left" ><c:out value="${fuelPriceMaster.mumbaiPrice}"/></td>
                                            <td  align="left" ><c:out value="${fuelPriceMaster.delhiPrice}"/></td>
                                            <td  align="left" ><c:out value="${fuelPriceMaster.kolkataPrice}"/></td>
                                            <td  align="left" ><c:out value="${fuelPriceMaster.fuelPrice}"/></td>
                                            <td  align="left" ><c:out value="${fuelPriceMaster.averagePrice}"/></td>
                                            <td  align="left" ><c:out value="${fuelPriceMaster.escalationPercent}"/></td>
                                            <td  align="left" ><c:out value="${fuelPriceMaster.escalationPrice}"/></td>
                                            <td  align="left" >
                                                <c:if test = "${fuelPriceMaster.statusName == '1'}" >
                                                    Invalid Month
                                                    <%  check = 1;%>
                                                </c:if>
                                                <c:if test = "${fuelPriceMaster.statusName == '2'}" >
                                                    Invalid Year
                                                    <%  check = 2;%>
                                                </c:if>
                                                <c:if test = "${fuelPriceMaster.statusName == '0'}" >
                                                    -
                                                </c:if>
                                            </td>              
                                            <c:set var="month" value="${fuelPriceMaster.fuelMonth}"/>
                                            <c:set var="year" value="${fuelPriceMaster.fuelYear}"/>
                                            <%
                                             month = (String) pageContext.getAttribute("month");
                                             year  = (String) pageContext.getAttribute("year");
                                            %>
                                        </tr>
                                    </c:forEach>
                                </c:if>

                                <tr>
                                    <td colspan="10" align="center" >
                                        <%
                                            if(check==0){
                                        %>
                                        <input type="button"  id="hidebut" class="btn btn-success" value="Confirm" name="Submit" onclick="submitPage()">
                                        <input type="hidden" name="month" value="<%=month%>"/>
                                        <input type="hidden" name="year" value="<%=year%>"/>
                                        <%}%>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>

