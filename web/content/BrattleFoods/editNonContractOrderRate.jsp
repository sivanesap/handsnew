<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" language="javascript">
            $(document).ready(function () {
                $("#tabs1").tabs();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function () {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        </script>
        <script type="text/javascript" language="javascript">
            function submitPage() {
                $("#saveButton").hide();
                var validate = validateTruckInfo();
                if (validate == 1) {
                    $("#saveButton").show();
                    return;
                } else {
                    document.cNote.action = '/throttle/updateOrderRate.do';
                    document.cNote.submit();
                }
            }

            function validateTruckInfo() {
                var regNo = document.getElementsByName("regNo");
                var chargeableWeight = document.getElementsByName("chargeableWeight");
                var freightCharge = document.getElementsByName("freightCharge");
                var ret = 0;
                for (var i = 0; i < chargeableWeight.length; i++) {
                    if (chargeableWeight[i].value == '') {
                        alert("Please enter the weight for truck :" + regNo[i].value);
                        chargeableWeight[i].focus();
                        ret = 1;
                        return ret;
                    } else if (freightCharge[i].value == '') {
                        alert("Please enter the weight for truck :" + regNo[i].value);
                        freightCharge[i].focus();
                        ret = 1;
                        return ret;
                    }
                }
                return ret;
            }
        </script>

        <script>
            function setRoyaltyPercent() {
                var tsrInput = $("#tsrInput").val();
                if (tsrInput == 1) {
                    $("#royaltyPercent").show();
                    $("#originRoyaltyPercent").val('<c:out value="${tsrOriginPercent}"/>');
                    $("#destinationRoyaltyPercent").val('<c:out value="${tsrDestPercent}"/>');
                    document.getElementById("tsrOriginAmount").readOnly = true;
                    document.getElementById("tsrDestinationAmount").readOnly = true;
                } else if (tsrInput == 2) {
                    $("#royaltyPercent").hide();
                    $("#originRoyaltyPercent").val('<c:out value="${tsrOriginPercent}"/>');
                    $("#destinationRoyaltyPercent").val('<c:out value="${tsrDestPercent}"/>');
                    document.getElementById("tsrOriginAmount").readOnly = false;
                    document.getElementById("tsrDestinationAmount").readOnly = false;
                }
                $("#tsrOriginAmount").val(0);
                $("#tsrDestinationAmount").val(0);
                calculateAllRate();
                calculateRoyaltyPercent(1);calculateRoyaltyPercent(2);
            }
            function calculateRoyaltyPercent(val) {
                var royaltyPercent = 0;
                if (val == 1) {
                    royaltyPercent = $("#originRoyaltyPercent").val();
                } else if (val == 2) {
                    royaltyPercent = $("#destinationRoyaltyPercent").val();
                }
                if (royaltyPercent == '') {
                    royaltyPercent = 0;
                }
                var freightPercent = 0;
                var rateMode1 = document.getElementById("rateMode1").checked;
                var freightAmount = 0;
                if (rateMode1 == true) {
                    freightAmount = $("#rateValue").val();
                } else {
                    freightAmount = $("#fullTruckValue").val();
                }
                if (freightAmount == '') {
                    freightAmount = 0;
                }
                freightPercent = (parseFloat(freightAmount) * parseFloat(royaltyPercent)) / 100;
                if (val == 1) {
                    $("#tsrOriginAmount").val(parseFloat(freightPercent).toFixed(2));
                } else if (val == 2) {
                    $("#tsrDestinationAmount").val(parseFloat(freightPercent).toFixed(2));
                }
                calculateAllRate();
            }

        </script>
        <script type="text/javascript">


            function setPayName(typeId) {
                if (typeId == 1) {
                    $("#tspOriginPaidTo").val(0);
                    $("#tspDestinationPaidTo").val(0);
                } else if (typeId == 2) {
                    $("#tsrOriginPaidTo").val(0);
                    $("#tsrDestinationPaidTo").val(0);
                } else if (typeId == 3) {
                    $("#scOriginPaidTo").val(0);
                    $("#scDestinationPaidTo").val(0);
                }
            }
            function checkRateMode(value) {
                if (parseInt(value) == 3) {
                    document.getElementById("tspOriginAmount").value = "0";
                    document.getElementById("tspDestinationAmount").value = "0";
                    document.getElementById("tsrOriginAmount").value = "0";
                    document.getElementById("tsrDestinationAmount").value = "0";
                    document.getElementById("scOriginAmount").value = "0";
                    document.getElementById("scDestinationAmount").value = "0";
                    document.getElementById("fullTruckValue").value = "0";

                    $("#trperKgRate").show();
                    $("#trfullTruckRate").hide();
                    document.getElementById('rateMode2').checked = false;
                }
                if (parseInt(value) == 4) {
                    document.getElementById("tspOriginAmount").value = "0";
                    document.getElementById("tspDestinationAmount").value = "0";
                    document.getElementById("tsrOriginAmount").value = "0";
                    document.getElementById("tsrDestinationAmount").value = "0";
                    document.getElementById("scOriginAmount").value = "0";
                    document.getElementById("scDestinationAmount").value = "0";
                    document.getElementById("ratePerKg").value = "0";
                    document.getElementById("rateValue").value = "0";
                    $("#trperKgRate").hide();
                    $("#trfullTruckRate").show();
                    document.getElementById('rateMode1').checked = false;
                }
                document.getElementById("rateMode").value = value;

                calculateFreightRate();
            }


        </script>
        <script type="text/javascript">
            function checkRateMode1(value) {
                // alert("dhf");
                if (parseInt(value) == 3) {

                    $("#trperKgRate").show();
                    $("#trfullTruckRate").hide();
                }
                if (parseInt(value) == 2) {

                    $("#trperKgRate").hide();
                    $("#trfullTruckRate").show();
                }
            }
            function setValues() {
                //  alert("i");
                if ('<%=request.getAttribute("payBillScreen")%>' == '1' || '<%=request.getAttribute("payBillScreen")%>' == '0') {
                    $("#scPaidTo").hide();
                } else {
                    $("#scPaidTo").show();
                }
                if ('<%=request.getAttribute("payBillTsr")%>' == '1' || '<%=request.getAttribute("payBillTsr")%>' == '0') {
                    $("#tsrPaidTo").hide();
                } else {
                    $("#tsrPaidTo").show();
                }
                if ('<%=request.getAttribute("payBillTsp")%>' == '1' || '<%=request.getAttribute("payBillTsp")%>' == '0') {
                    $("#tspPaidTo").hide();
                } else {
                    $("#tspPaidTo").show();
                }
            }
//                   
            setValues();

            function calculateFreightRate() {
                // alert("bui");
                var ratePerKg = $("#ratePerKg").val();
                if (ratePerKg == '') {
                    ratePerKg = 0;
                }
                var freightCharges = document.getElementsByName("freightCharge");
                var chargeableWeights = document.getElementsByName("chargeableWeight");
                if (document.getElementById('rateMode1').checked == true) {//if rate per kg

                    //*****
                    var totChrgWt = 0;
                    var chgWt = 0;
                    for (var i = 0; i < chargeableWeights.length; i++) {
                        if (chargeableWeights[i].value == '') {
                            chgWt = 0;
                            var amount = parseFloat(chgWt) * parseFloat(ratePerKg);
                            $("#freightCharge" + i).val(parseFloat(amount).toFixed(2));
                        } else {
                            chgWt = parseFloat(chargeableWeights[i].value);
                            var amount = parseFloat(chgWt) * parseFloat(ratePerKg);
                            $("#freightCharge" + i).val(parseFloat(amount).toFixed(2));
                        }
                        totChrgWt += parseFloat(chgWt);
                    }
                    if (totChrgWt != '') {
                        $("#totalChargableWeight").val(totChrgWt.toFixed(2));
                    } else {
                        $("#totalChargableWeight").val(0);
                    }
                    //**
                    var totalChargableWeight = $("#totalChargableWeight").val();
                    if (totalChargableWeight == '') {
                        totalChargableWeight = 0;
                    }
                    var result = parseFloat(totalChargableWeight) * parseFloat(ratePerKg);
                    $("#rateValue").val(parseFloat(result).toFixed(2));
                    $("#fullTruckValue").val(0);



                } else if (document.getElementById('rateMode2').checked == true) {//if full truck rate
                    var totFreightChrge = 0;
                    var freightChrge = 0;
                    for (var i = 0; i < freightCharges.length; i++) {
                        if (freightCharges[i].value == '') {
                            freightChrge = 0;
                        } else {
                            freightChrge = parseFloat(freightCharges[i].value);
                        }
                        totFreightChrge += parseFloat(freightChrge);
                    }
                    document.getElementById("fullTruckValue").value = parseFloat(totFreightChrge).toFixed(2);
                    document.getElementById("rateValue").value = 0;

                }
                calculateAllRate();
            }

            function calculateAllRate() {

                var totalChargableWeight = $("#totalChargableWeight").val();
                if (totalChargableWeight == '') {
                    totalChargableWeight = 0;
                }

                //TSP//

                var tspOriginAmount = $("#tspOriginAmount").val();
                if (tspOriginAmount == '') {
                    tspOriginAmount = 0;
                }
                var tspDestinationAmount = $("#tspDestinationAmount").val();
                if (tspDestinationAmount == '') {
                    tspDestinationAmount = 0;
                }

                var result = 0;
                var payBillTsp = $("#payBillTsp").val();
                var tspInput = $("#tspInput").val();

                var tspRatePerKg = parseFloat(tspOriginAmount) + parseFloat(tspDestinationAmount);
                if (parseInt(tspInput) == 1) {
                    result = totalChargableWeight * tspRatePerKg;
                    $("#totTSPrate").val(parseFloat(result).toFixed(2));
                } else {
                    $("#totTSPrate").val(parseFloat(tspRatePerKg).toFixed(2));
                }
                if (parseInt(payBillTsp) == 1) {
                    $("#tspPaidTo").hide();
                } else {
                    $("#tspPaidTo").show();
                }
                //Tsp End//

                //TSR//
                var tsrOriginAmount = $("#tsrOriginAmount").val();
                if (tsrOriginAmount == '') {
                    tsrOriginAmount = 0;
                }
                var tsrDestinationAmount = $("#tsrDestinationAmount").val();
                if (tsrDestinationAmount == '') {
                    tsrDestinationAmount = 0;
                }
                var result = 0;
                var payBillTsr = $("#payBillTsr").val();
                var tsrInput = $("#tsrInput").val();

                var tsrRatePerKg = parseFloat(tsrOriginAmount) + parseFloat(tsrDestinationAmount);

                if (parseInt(tsrInput) == 1) {
                    result = tsrRatePerKg;
                    $("#totTSRrate").val(parseFloat(result).toFixed(2));
                } else {
                    $("#totTSRrate").val(parseFloat(tsrRatePerKg).toFixed(2));
                }
                if (parseInt(payBillTsr) == 1) {
                    $("#tsrPaidTo").hide();
                } else {
                    $("#tsrPaidTo").show();
                }
                //TSR end//
                //SCR
                var result = 0;
                var payBillScreen = $("#payBillScreen").val();
                var screeningInput = $("#screeningInput").val();

                var scOriginAmount = $("#scOriginAmount").val();
                if (scOriginAmount == '') {
                    scOriginAmount = 0;
                }
                var scDestinationAmount = $("#scDestinationAmount").val();
                if (scDestinationAmount == '') {
                    scDestinationAmount = 0;
                }

                var totalScrAmount = parseFloat(scOriginAmount) + parseFloat(scDestinationAmount);

                if (parseInt(screeningInput) == 1) {
                    result = totalChargableWeight * totalScrAmount;
                    $("#totSCRrate").val(parseFloat(result).toFixed(2));
                } else {
                    $("#totSCRrate").val(parseFloat(totalScrAmount).toFixed(2));
                }
                if (parseInt(payBillScreen) == 1) {
                    $("#scPaidTo").hide();
                } else {
                    $("#scPaidTo").show();
                }
                //SCR end//

                calTotValues();

            }

            function calTotValues() {

                var payBillTsr = $("#payBillTsr").val();
                var payBillTsp = $("#payBillTsp").val();
                var payBillScreen = $("#payBillScreen").val();

                var customerTotalAmount = 0;
                var ttaTotalAmount = 0;

                /*TSP start*/
                if (parseInt(payBillTsp) == 1) {
                    var totTSPrate = $("#totTSPrate").val();
                    if (totTSPrate == '') {
                        totTSPrate = 0;
                    }
                    customerTotalAmount += parseFloat(totTSPrate);
                }
                if (parseInt(payBillTsp) == 2) {
                    var totTSPrate = $("#totTSPrate").val();
                    if (totTSPrate == '') {
                        totTSPrate = 0;
                    }
                    ttaTotalAmount += parseFloat(totTSPrate);
                }
                /*TSP end*/
                /*TSR start*/
                if (parseInt(payBillTsr) == 1) {
                    var totTSRrate = $("#totTSRrate").val();
                    if (totTSRrate == '') {
                        totTSRrate = 0;
                    }
                    customerTotalAmount += parseFloat(totTSRrate);
                }
                if (parseInt(payBillTsr) == 2) {
                    var totTSRrate = $("#totTSRrate").val();
                    if (totTSRrate == '') {
                        totTSRrate = 0;
                    }
                    ttaTotalAmount += parseFloat(totTSRrate);
                }
                /*TSR end*/
                /*SC start*/
                if (parseInt(payBillScreen) == 1) {
                    var totSCRrate = $("#totSCRrate").val();
                    if (totSCRrate == '') {
                        totSCRrate = 0;
                    }
                    customerTotalAmount += parseFloat(totSCRrate);
                }
                if (parseInt(payBillScreen) == 2) {
                    var totSCRrate = $("#totSCRrate").val();
                    if (totSCRrate == '') {
                        totSCRrate = 0;
                    }
                    ttaTotalAmount += parseFloat(totSCRrate);
                }
                /*SC end*/

                $("#customerNetAmount").val(parseFloat(customerTotalAmount).toFixed(2));
                $("#ttaNetAmount").val(parseFloat(ttaTotalAmount).toFixed(2));


            }
            
            $('#ratePerKg').keypress(function(event) {
  
//    if ((event.which != 46 || $(this).val().indexOf('.') != -1)&&(event.which < 48 || event.which > 57)) {
//    alert('hello');
//   		if((event.which != 46 || $(this).val().indexOf('.') != -1)){
//      	alert('Multiple Decimals are not allowed');
//      }
//      event.preventDefault();
//   }
   if(this.value.indexOf(".")>-1 && (this.value.split('.')[1].length > 1) && event.which != 46 && event.which != 8)		{
        alert('Two numbers only allowed after decimal point');
        event.preventDefault();
    }
    });
        </script> 


    </head>
    <body onload="checkRateMode1('<c:out value="${rateMode}"/>');">
        <form name="cNote" method="post">
            <input name="statusId" id="statusId" type="hidden" value="<c:out value="${statusId}"/>">
            <input name="tripType" id="tripType" type="hidden" value="<c:out value="${tripType}"/>">
            <input name="vehicleId" id="vehicleId" type="hidden" value="<c:out value="${vehicleId}"/>">
            <input name="tripSheetId" id="tripSheetId" type="hidden" value="<c:out value="${tripSheetId}"/>">
            <input name="fromDate" id="fromDate" type="hidden" value="<c:out value="${fromDate}"/>">
            <input name="toDate" id="toDate" type="hidden" value="<c:out value="${toDate}"/>">
            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                <tr class="contenthead" style="height:27px;">
                    <td>Trip Code</td>
                    <td>Truck No</td>
                    <td>Route Info</td>
                    <td>Customer Name</td>
                    <td>Gross.Weight</td>
                    <td>Pieces</td>
                    <td>Charg.Weight</td>
                    <td>Freight Charge</td>

                </tr>
                <c:if test="${getOrderTruckList != null}">
                    <% int index =0 ;%>
                    <c:forEach items="${getOrderTruckList}" var="truckList">
                        <tr style="height:28px;">
                            <td><input type="hidden" name="routeCourseId" value="<c:out value="${truckList.routeCourseId}"/>" /><c:out value="${truckList.tripCode}"/></td>
                            <td><input type="hidden" name="regNo" value="<c:out value="${truckList.regNo}"/>" /><c:out value="${truckList.regNo}"/></td>
                            <td><c:out value="${truckList.routeInfo}"/></td>
                            <td><c:out value="${truckList.customerName}"/></td>
                            <td><c:out value="${truckList.grossWeight}"/><input type="hidden" name="grossWeight" value="<c:out value="${truckList.grossWeight}"/>" /></td>
                            <td><c:out value="${truckList.awbReceivedPackages}"/><input type="hidden" name="awbReceivedPackages" value="<c:out value="${truckList.awbReceivedPackages}"/>" /></td>
                            <td><input type="text" id="chargeableWeight<%=index%>" name="chargeableWeight" value="<c:out value="${truckList.chargeableWeight}"/>"  onkeyup="calculateFreightRate();" style="height:20px; width:125px;"/></td>
                            <td><input type="text" id="freightCharge<%=index%>" name="freightCharge" value="<c:out value="${truckList.freightCharge}"/>" onkeyup="calculateFreightRate();" style="height:20px; width:125px;"/></td>
                        <input type="hidden" name="vehicleCost" value="<c:out value="${truckList.vehicleCost}"/>" />
                        </tr>
                        <%  index++ ;%>
                    </c:forEach>
                </c:if>
            </table>
            <br>
            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">    
                <tr class="contenthead" style="height:26px;">
                    <td colspan="2" style="width: 50%;text-align: right;">Edit Consignment</td>
                    <td colspan="2" style="width:50%;">&nbsp; Order Rate</td>
                </tr>
                <tr class="text1" style="height:25px;">
                    <td style="width:20%;">AWB No</td>
                    <td style="width:30%;"><c:out value="${awbNo}"/></td>
                    <td style="width:25%;">Total Chargable Weight</td>
                    <td style="width:25%;">
                        <input type="text" name="totalChargableWeight" id="totalChargableWeight" value="<c:out value="${totalChargableWeight}"/>" onkeyup="calculateFreightRate()" onKeyPress='return onKeyPressBlockCharacters(event);' readonly style="height:20px; width:150px;"/>
                    </td>
                </tr>
                <tr class="text2" style="height:25px;">
                    <td >Rate Mode</td>
                    <td>
                        <c:if test="${rateMode == 3}">
                            <input type="radio" name="rateMode1" id="rateMode1" value="3" checked onclick="checkRateMode(3);"/>Rate Per KG
                        </c:if>
                        <c:if test="${rateMode != 3}">
                            <input type="radio" name="rateMode1" id="rateMode1" value="3"  onclick="checkRateMode(3);" checked/>Rate Per KG
                            <script>checkRateMode(3);</script>
                        </c:if>
                    </td>

                    <td>
                        <c:if test="${rateMode == 4}">
                            <input type="radio" name="rateMode2" id="rateMode2" value="4" checked  onclick="checkRateMode(4);"/>Full Truck Rate
                        </c:if>
                        <c:if test="${rateMode != 4}">
                            <input type="radio" name="rateMode2" id="rateMode2" value="4"   onclick="checkRateMode(4);"/>Full Truck Rate
                        </c:if>

                    </td>
                    <td><input type="hidden" name="rateMode" id="rateMode" value="<c:out value="${rateMode}"/>"/></td>
                </tr>
                <tr class="text1" id="trperKgRate" style="height:25px;">
                    <td>Per Kg Rate</td>
                    <td><input type="text" name="ratePerKg" id="ratePerKg" class="textbox"  onkeyup="calculateFreightRate();calculateRoyaltyPercent(1);calculateRoyaltyPercent(2);" value="<c:out value="${ratePerKg}"/>" onKeyPress='return onKeyPressBlockCharacters(event);' style="height:20px; width:150px;" ></td>
                    <td>Freight Amount</td>
                    <td><input type="text" name="rateValue" id="rateValue" class="textbox" readonly value="<c:out value="${freightCharge}"/>" onKeyPress='return onKeyPressBlockCharacters(event);' style="height:20px; width:150px;"></td>
                </tr>
                <tr class="text1" id="trfullTruckRate" style="display: none;height:26px;">
                    <td colspan="2">&nbsp;</td>
                    <td>Freight Amount</td>
                    <td><input type="text" name="fullTruckValue" id="fullTruckValue" readonly class="textbox" onkeyup="calculateAllRate();calculateRoyaltyPercent(1);calculateRoyaltyPercent(2);" value="<c:out value="${freightCharge}"/>" onKeyPress='return onKeyPressBlockCharacters(event);' style="height:20px; width:150px;"></td>
                </tr>
            </table>
            <br>
            <div id="tabs1" style="height: 220px;">
                <ul>
                    <li><a href="#tspDetails"><span>TSP Details</span></a></li>
                    <li><a href="#tsrDetails"><span>Royalty Details</span></a></li>
                    <li><a href="#scrDetails"><span>Screening Details</span></a></li>
                </ul>
                <br>
                <div id="tspDetails">
                    <table  border="1" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">          
                        <tr class="contenthead" style="height:26px;">
                            <td colspan="4" style="width:50%;">TSP Details</td>
                        </tr>
                        <c:if test="${tspInput != 0}">
                            <script>
                                document.getElementById("tspInput").value = '<c:out value="${tspInput}"/>';
                                setPayName(1);calculateAllRate();calculateRoyaltyPercent(1);calculateRoyaltyPercent(2);
                            </script>
                        </c:if>
                        <tr style="height:25px;" class="text1">
                            <td>TSP</td>
                            <td>
                                <select name="payBillTsp" id="payBillTsp" class="textbox" style="height:20px; width:150px;" onchange="setPayName(1);
                                        calculateAllRate();">
                                    <option value="1" selected>Billed To Customer</option>
                                    <option value="2">Paid By TTA</option>
                                </select>
                                <script>
                                    <c:if test="${payBillTsp != '0'}">
                                    document.getElementById("payBillTsp").value = '<c:out value="${payBillTsp}"/>';
                                    </c:if>
                                </script>
                            </td>
                            <td style="width:25%">TSP Input<font color="red">*</font></td>
                            <td style="width:25%;">
                                <select name="tspInput" id="tspInput" onchange="setPayName(1);calculateAllRate()" class="textbox" style="height:20px; width:150px;">
                                    <option value="1">Auto</option>
                                    <option value="2">Manual</option>
                                </select>
                            </td>
                        </tr>
                        <tr  id="tspPaidTo" style="display: none;height:25px;" class="text2">
                            <td>Origin PaidTo</td>
                            <td>
                                <select name="tspOriginPaidTo" id="tspOriginPaidTo" class="textbox" style="height:20px; width:150px;" >
                                    <c:if test="${royaltyPayList != null}">
                                        <option value="0" selected>--Select--</option>
                                        <c:forEach items="${royaltyPayList}" var="royalty">
                                            <option value='<c:out value="${royalty.paidId}"/>'><c:out value="${royalty.paidBy}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                                <script>
                                    document.getElementById("tspOriginPaidTo").value = '<c:out value="${tspOriginPaidTo}"/>';
                                </script>
                            </td>
                            <td>Destination PaidTo</td>
                            <td>
                                <select name="tspDestinationPaidTo" id="tspDestinationPaidTo" class="textbox" style="height:20px; width:150px;" >
                                    <c:if test="${royaltyPayList != null}">
                                        <option value="0" selected>--Select--</option>
                                        <c:forEach items="${royaltyPayList}" var="royalty">
                                            <option value='<c:out value="${royalty.paidId}"/>'><c:out value="${royalty.paidBy}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                                <script>
                                    document.getElementById("tspDestinationPaidTo").value = '<c:out value="${tspDestinationPaidTo}"/>';
                                </script>
                            </td>
                        </tr>
                        <tr style="height:25px;" class="text2">
                            <td>Origin Amount</td>
                            <td><input type="text" name="tspOriginAmount" id="tspOriginAmount" class="textbox" style="height:20px; width:150px;" onkeyup="calculateAllRate();" value="<c:out value="${tspOriginAmount}"/>" onKeyPress='return onKeyPressBlockCharacters(event);'/></td>
                            <td>Destination Amount</td>
                            <td><input type="text" name="tspDestinationAmount" id="tspDestinationAmount" class="textbox" style="height:20px; width:150px;" onkeyup="calculateAllRate();" value="<c:out value="${tspDestinationAmount}"/>" onKeyPress='return onKeyPressBlockCharacters(event);'/></td>
                        </tr>
                    </table>
                    <br>
                    <center>
                        <a  class="nexttab1" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>
                    <br>
                </div>
                <div id="tsrDetails">
                    <table  border="1" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">    
                        <tr class="contenthead" style="height:26px;">
                            <td colspan="4">Royalty Details</td>
                        </tr>
                        <c:if test="${tsrInput != '0'}">
                            <script>
                                document.getElementById("tsrInput").value = '<c:out value="${tsrInput}"/>';
                                setRoyaltyPercent();setPayName(2);calculateAllRate();
                            </script>
                        </c:if>
                        <tr style="height:25px;" class="text1">
                            <td>Royalty Rate</td>
                            <td>
                                <select name="payBillTsr" id="payBillTsr" class="textbox" style="height:20px; width:150px;" onchange="setPayName(2);
                                        calculateAllRate();">
                                    <option value="1" selected>Billed To Customer</option>
                                    <option value="2">Paid By TTA</option>
                                </select>
                                <script>
                                    <c:if test="${payBillTsr != '0'}">
                                    document.getElementById("payBillTsr").value = '<c:out value="${payBillTsr}"/>';
                                    </c:if>
                                </script>
                            </td>
                            <td>Royalty Input</td>
                            <td>
                                <select name="tsrInput" id="tsrInput" onchange="setRoyaltyPercent();
                                        setPayName(2);
                                        calculateAllRate()" class="textbox" style="height:20px; width:150px;">
                                    <option value="1">Auto</option>
                                    <option value="2">Manual</option>
                                </select>
                            </td>
                        </tr>
                        <tr id="royaltyPercent" style="height:25px;" class="text2">
                            <td>Origin Royalty %</td>
                            <td><input type="text" name="originRoyaltyPercent" id="originRoyaltyPercent" class="textbox" style="height:20px; width:150px;" onkeyup="calculateRoyaltyPercent(1)" value="<c:out value="${tsrOriginPercent}"/>" onKeyPress='return onKeyPressBlockCharacters(event);'/></td>
                            <td colspan="2">&nbsp;</td>
                            <input type="hidden" name="destinationRoyaltyPercent" id="destinationRoyaltyPercent" class="textbox" style="height:20px; width:150px;" onkeyup="calculateRoyaltyPercent(2)" value="<c:out value="${tsrDestPercent}"/>" onKeyPress='return onKeyPressBlockCharacters(event);'/>
                        </tr>
                        <tr id="tsrPaidTo" style="display: none;height:25px;" class="text2">
                            <td>Origin PaidTo</td>
                            <td>
                                <select name="tsrOriginPaidTo" id="tsrOriginPaidTo" class="textbox" style="height:20px; width:150px;" >
                                    <c:if test="${royaltyPayList != null}">
                                        <option value="0" selected>--Select--</option>
                                        <c:forEach items="${royaltyPayList}" var="royalty">
                                            <option value='<c:out value="${royalty.paidId}"/>'><c:out value="${royalty.paidBy}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                                <script>
                                    document.getElementById("tsrOriginPaidTo").value = '<c:out value="${tsrOriginPaidTo}"/>';
                                </script>
                            </td>
                            <td>Destination PaidTo</td>
                            <td>
                                <select name="tsrDestinationPaidTo" id="tsrDestinationPaidTo" class="textbox" style="height:20px; width:150px;" >
                                    <c:if test="${royaltyPayList != null}">
                                        <option value="0" selected>--Select--</option>
                                        <c:forEach items="${royaltyPayList}" var="royalty">
                                            <option value='<c:out value="${royalty.paidId}"/>'><c:out value="${royalty.paidBy}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                                <script>
                                    document.getElementById("tsrDestinationPaidTo").value = '<c:out value="${tsrDestinationPaidTo}"/>';
                                </script>
                            </td>
                        </tr>    
                        <tr style="height:25px;" class="text2">
                            <td>Origin Amount</td>
                            <td><input type="text" name="tsrOriginAmount" id="tsrOriginAmount" class="textbox" style="height:20px; width:150px;" onkeyup="calculateAllRate();" value="<c:out value="${tsrOriginAmount}"/>" onKeyPress='return onKeyPressBlockCharacters(event);'/></td>
                            <td colspan="2">&nbsp;</td>
                            <input type="hidden" name="tsrDestinationAmount" id="tsrDestinationAmount" class="textbox" style="height:20px; width:150px;" onkeyup="calculateAllRate();" value="<c:out value="${tsrDestinationAmount}"/>" onKeyPress='return onKeyPressBlockCharacters(event);'/>
                        </tr>
                    </table>
                    <br>
                    <center>
                        <a  class="nexttab1" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                        <a  class="previoustab1" href="#"><input type="button" class="button" value="Previous" name="Previous" /></a>
                    </center>
                    <br>
                </div>
                <div id="scrDetails">
                    <table  border="1" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">            
                        <tr class="contenthead" style="height:26px;">
                            <td colspan="4">Screening Details</td>
                        </tr>
                        <c:if test="${screeningInput != '0'}">
                            <script>
                                document.getElementById("screeningInput").value = '<c:out value="${screeningInput}"/>';
                                setPayName(3);
                                calculateAllRate();
                            </script>
                        </c:if>
                        <tr style="height:25px;" class="text1"> 
                            <td>Screening Charge</td>
                            <td>
                                <select name="payBillScreen" id="payBillScreen" class="textbox" style="height:20px; width:150px;" onchange="setPayName(3);
                                        calculateAllRate();">
                                    <option value="1" selected>Billed To Customer</option>
                                    <option value="2">Paid By TTA</option>
                                </select>
                                <script>
                                    <c:if test="${payBillScreen != '0'}">
                                    document.getElementById("payBillScreen").value = '<c:out value="${payBillScreen}"/>';
                                    </c:if>
                                </script>
                            </td>
                            <td>Screening Input</td>
                            <td>
                                <select name="screeningInput" id="screeningInput" onchange="setPayName(3);calculateAllRate()" class="textbox" style="height:20px; width:150px;">
                                    <option value="1">Auto</option>
                                    <option value="2">Manual</option>
                                </select>
                            </td>
                        </tr>
                        <tr id="scPaidTo" style="display: none;height:25px;" class="text2">
                            <td>Origin PaidTo</td>
                            <td>
                                <select name="scOriginPaidTo" id="scOriginPaidTo" class="textbox" style="height:20px; width:150px;" >
                                    <c:if test="${royaltyPayList != null}">
                                        <option value="0" selected>--Select--</option>
                                        <c:forEach items="${royaltyPayList}" var="royalty">
                                            <option value='<c:out value="${royalty.paidId}"/>'><c:out value="${royalty.paidBy}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                                <script>
                                    document.getElementById("scOriginPaidTo").value = '<c:out value="${scOriginPaidTo}"/>';
                                </script>
                            </td>
                            <td>Destination PaidTo</td>
                            <td>
                                <select name="scDestinationPaidTo" id="scDestinationPaidTo" class="textbox" style="height:20px; width:150px;" >
                                    <c:if test="${royaltyPayList != null}">
                                        <option value="0" selected>--Select--</option>
                                        <c:forEach items="${royaltyPayList}" var="royalty">
                                            <option value='<c:out value="${royalty.paidId}"/>'><c:out value="${royalty.paidBy}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                                <script>
                                    document.getElementById("scDestinationPaidTo").value = '<c:out value="${scDestinationPaidTo}"/>';
                                </script>
                            </td>

                        </tr>
                        <tr style="height:25px;" class="text2">
                            <td>Origin Amount</td>
                            <td><input type="text" name="scOriginAmount" id="scOriginAmount" class="textbox"  style="height:20px; width:150px;" onkeyup="calculateAllRate();"  value="<c:out value="${scOriginAmount}"/>" onKeyPress='return onKeyPressBlockCharacters(event);' /></td>
                            <td>Destination Amount</td>
                            <td><input type="text" name="scDestinationAmount" id="scDestinationAmount" class="textbox"  style="height:20px; width:150px;" onkeyup="calculateAllRate();"  value="<c:out value="${scDestinationAmount}"/>" onKeyPress='return onKeyPressBlockCharacters(event);' /></td>
                        </tr>
                    </table>
                    <br>
                    <center>
                        <a  class="previoustab1" href="#"><input type="button" class="button" value="Previous" name="Previous" /></a>
                    </center>
                    <br>
                </div>
            </div>
            <br>
            <br>
            <div id="summary">
                <table  border="1" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">    
                    <tr class="contenthead" style="height:26px;">
                        <td colspan="4">Summary Details</td>
                    </tr>
                    <tr style="height:25px;" class="text1">
                        <td>Total TSP</td>
                        <td><input type="text" name="totTSPrate" id="totTSPrate" class="textbox" readonly value="<c:out value="${totTSPrate}"/>" onKeyPress='return onKeyPressBlockCharacters(event);' style="height:20px; width:150px;"></td>
                        <td>Total Royalty</td>
                        <td><input type="text" name="totTSRrate" id="totTSRrate" class="textbox" readonly value="<c:out value="${totTSRrate}"/>" onKeyPress='return onKeyPressBlockCharacters(event);' style="height:20px; width:150px;"></td>
                    </tr>
                    <tr style="height:25px;" class="text2">
                        <td>Total SCR</td>
                        <td><input type="text" name="totSCRrate" id="totSCRrate" class="textbox" readonly value="<c:out value="${totSCRrate}"/>" onKeyPress='return onKeyPressBlockCharacters(event);' style="height:20px; width:150px;"></td>
                        <td>Customer Net Amount(Other Charges)</td>
                        <td><input type="text" name="customerNetAmount" id="customerNetAmount" class="textbox"  value="<c:out value="${totCustAmount}"/>" onKeyPress='return onKeyPressBlockCharacters(event);' style="height:20px; width:150px;"></td>
                    </tr>
                    <tr  class="text1" style="height:25px;">
                        <td>Incentive Charge</td>
                        <td><input type="text" name="totIncentiveAmount" id="totIncentiveAmount" class="textbox"  value="<c:out value="${totIncentiveAmount}"/>" onKeyPress='return onKeyPressBlockCharacters(event);' style="height:20px; width:150px;"></td>
                        <td>Total TTA Amount</td>
                        <td><input type="text" name="ttaNetAmount" id="ttaNetAmount" class="textbox" value="<c:out value="${totTTAamount}"/>" onKeyPress='return onKeyPressBlockCharacters(event);' style="height:20px; width:150px;"></td> 
                    </tr>
                </table>
                <br>
            </div>

            <!--</div>-->
            <script type="text/javascript" language="javascript">
                $(document).ready(function () {
                    $("#tabs1").tabs();
                });
            </script>
            <script>
                $(".nexttab1").click(function () {
                    var selected = $("#tabs1").tabs("option", "selected");
                    $("#tabs1").tabs("option", "selected", selected + 1);
                });
            </script>
            <script>
                $(".previoustab1").click(function () {
                    var selected = $("#tabs1").tabs("option", "selected");
                    $("#tabs1").tabs("option", "selected", selected - 1);
                });
            </script>
            <input type="hidden" name="consignmentOrderId" value="<c:out value="${consignmentOrderId}"/>" />
            <input type="hidden" name="tripSheetId" value="<c:out value="${tripSheetId}"/>" />
            <br>

            <center>
                <input type="button" class="button" name="Save" value="Save" id="saveButton" onclick="submitPage();" />
            </center>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br> 

        </form>
    </body>
</html>
