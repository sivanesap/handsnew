
<%-- 
    Document   : tsaPrintView
    Created on : Mar 5, 2015, 11:38:59 AM
    Author     : srinientitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <style type="text/css" title="currentStyle">
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.7.1.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
        <!-- Our jQuery Script to make everything work -->
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    </head>
    <body onload="checkSpace();">
        <form name="tsaPrint" method="post" style="font-size: 10px;">
            <div id="printDiv">
                <table align="center" width="100%">
                    <tr><td><font size="1"><center> <c:choose>
                                        <c:when test="${shipType ==1}">
                                            Imports  
                                        </c:when>
                                        <c:otherwise>
                                            Exports 
                                        </c:otherwise>
                                    </c:choose><br/>TT AVIATION HANDLING SERVICES PVT LTD, CHENNAI - 600 027.<br>TRANSHIPMENT APPLICATION</center></font></td></tr>
                </table>
                <br>
                <table align="center" width="100%" style="font-size: 10px;">
                    <tr>
                        <td>
                            <table align="center" width="100%">
                                <tr><td><b>From</b></td></tr>
                                <tr><td><b>The Asst.Commissioner Of Customs</b></td></tr>
                                <tr><td><b>Air Cargo Complex </b></td></tr>
                                <tr><td><b><c:out value="${fromAddress}"/>-600 027 </b></td></tr>
                            </table>
                        </td>
                        <td>
                            <table align="center" width="100%">
                                <tr><td><b>To</b></td></tr>
                                <tr><td><b>The Asst.Commissioner Of Customs</b></td></tr>
                                <tr><td><b>Air Cargo Complex </b></td></tr>
                                <tr><td><b><c:out value="${toAddress}"/> </b></td></tr>
                            </table>

                        </td>
                        <td> 
                            <table align="center"  width="100%" style="text-align: center;font-size: 10px;" >
                                <c:if test="${shipType == 1}">
                                    <tr><td align="left"><font size="2"></font>ITSA No: </td>    </tr>
                                            </c:if>
                                <tr><td align="left"><font size="2"><b>FLT NO</b>:<b>&nbsp;&nbsp;<c:out value="${fleetDetails}"/></b></font></td>    </tr>
                                <tr><td align="left"><font size="2">Created Date:<b>&nbsp;&nbsp;<c:out value="${flightDate}"/></b></font></td>    </tr>
                                <tr><td align="left"><font size="2">
                                            <c:choose>
                                                <c:when test="${shipType ==1}">
                                                    IGM No :<b><c:out value="${consignmentEgmNo}"/></b>
                                                </c:when>
                                                <c:otherwise>
                                                    EGM No :<b><c:out value="${consignmentEgmNo}"/></b>
                                                </c:otherwise>
                                            </c:choose></font></td>    </tr>
                                            <c:if test="${shipType == 2}">
                                    <tr><td align="left"><font size="2">TP No:&nbsp;&nbsp;&nbsp;&nbsp;<b><c:out value="${consignmentTpNo}"/></b></font></td>    </tr>
                                            </c:if>
                            </table>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%" style="font-size: 10px;">
                    <tr>
                        <td>
                            Sub:Movement of  <c:choose>
                                <c:when test="${shipType ==1}">
                                    Import 
                                </c:when>
                                <c:otherwise>
                                    Export 
                                </c:otherwise>
                            </c:choose> Cargo By Bonded Truck from Air Cargo Complex <c:out value="${fromAddress}"/> to <c:out value="${toAddress}"/> Air Cargo Complex.</td>
                    </tr>
                    <tr>
                        <td>
                            Dear Sir / Madam,</td> </tr>
                    <tr>
                        <td>
                            Please grant Permission to tranship the following shipments through Bonded Trucking:</td> </tr>
                </table>
                <c:set var="totalPackages" value="0" />
                <c:if test="${tsiViewList != null}">
                    <table align="center" border="1" width="100%" height="20%" style="font-size: 10px;">
                        <thead>
                            <tr height="10">
                                <th align="center">S.No</th>
                                <th align="center">AWB No</th>
                                    <c:if test="${shipType == 2}">
                                    <th align="center">SB No</th>
                                    <th align="center">Exporter</th>
                                    </c:if>
                                <th align="center">Consignee</th>
                                <th align="center">Desp Of Goods</th>
                                <th align="center">Origin</th>
                                <th align="center">Destination</th>
                                <th align="center">No Of Pcs</th>
                                <th align="center">Total Grs.Weight</th>
                                <th align="center">Value Of Goods INR</th>
                            </tr>
                        </thead>
                        <% int index = 1;%>
                        <% int indexp = 1;%>
                        <c:forEach begin="0" end="11" items="${tsiViewList}" var="tsiViewList">
                            <tr height="9">
                                <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                                %>
                                <td class="<%=classText%>"  ><%=index%></td>
                                <td class="<%=classText%>"   ><c:out value="${tsiViewList.consignmentAwbNo}"/></td>
                                <c:if test="${shipType == 2}">
                                    <td class="<%=classText%>"   ><c:out value="${tsiViewList.customsSbNo}"/></td>
                                    <td class="<%=classText%>"   ><c:out value="${tsiViewList.consignorName}"/></td>
                                </c:if>
                                <td class="<%=classText%>"   ><c:out value="${tsiViewList.consigneeName}"/></td>
                                <td class="<%=classText%>"   ><c:out value="${tsiViewList.commodity}"/></td>
                                <td class="<%=classText%>"   ><c:out value="${tsiViewList.awbOriginName}"/></td>
                                <td class="<%=classText%>"   ><c:out value="${tsiViewList.awbDestinationName}"/></td>
                                <td class="<%=classText%>">
                                    <c:if test="${tsiViewList.shipmentType == '1' }">
                                        <c:out value="${tsiViewList.totalPackages}"/>
                                        <c:set var="totalPackages" value="${totalPackages + tsiViewList.totalPackages}"/>
                                    </c:if>
                                    <c:if test="${tsiViewList.shipmentType == '2' }">
                                        <c:out value="${tsiViewList.totalPackages}"/>/<c:out value="${tsiViewList.awbTotalPackages}"/>
                                        <c:set var="totalPackages" value="${totalPackages + tsiViewList.totalPackages}"/>
                                    </c:if>
                                    <input type="hidden" id="totalPackages" value="0"/>
                                </td>
                                <td class="<%=classText%>">
                                    <c:if test="${tsiViewList.shipmentType == '1' }">
                                        <c:out value="${tsiViewList.totalWeight}"/>
                                        <c:set var="totalWeight" value="${totalWeight + tsiViewList.totalWeight}"/>
                                    </c:if>
                                    <c:if test="${tsiViewList.shipmentType == '2' }">
                                        <c:set var="totalWeight" value="${totalWeight + tsiViewList.receivedWeight}"/>
                                        <c:out value="${tsiViewList.receivedWeight}"/>/<c:out value="${tsiViewList.awbTotalGrossWeight}"/>
                                    </c:if>
                                </td>
                                <td class="<%=classText%>"   ><c:out value="${tsiViewList.totalCost}"/></td> 
                                <c:set var="totalCost" value="${totalCost + tsiViewList.totalCost}"/>
                                <%++index;%>
                            </tr>

                        </c:forEach>
                        <!--</table>-->
                        <tr  height="10" style="display:none" id="space1">
                            <c:if test="${shipType == 2}">
                                <td colspan="8" align="right">&nbsp;Total</td>
                            </c:if>
                            <c:if test="${shipType == 1}">
                                <td colspan="6" align="right">&nbsp;Total</td>
                            </c:if>
                            <td ><c:out value="${totalPackages}"/></td>
                            <td ><c:out value="${totalWeight}"/></td>
                            <td ><c:out value="${totalCost}"/></td>
                        </tr>

                        <!--</div>-->
                        <input type="hidden" class="textbox" id="count" name="count" value="<%=index%>">
                    </table>



                    <!--<div >-->
                    <table align="center"  width="100%" height="20%" style="font-size: 10px;border:none;display: none;" id="spaceTable">
                        
                        <tr>
                            <td colspan="1"  id="displayrow11" align="right" style="margin-right: 190px;display: none"><b>Page 01 of 02</b></td>
                            <td colspan="1"  id="displayrow12" align="right" style="margin-right: 190px;display: none"><b>Page 01 of 03</b></td>
                        </tr>
                        <tr style="border:none">
                            <td style="border:none">
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                            </td> 
                        </tr>
                    </table>
                    <!--</div>-->
                    <%if(index >= 13){ %>
                    <table align="center" border="1" width="100%" height="20%" style="font-size: 10px;">
                         <thead>
                            <tr height="10">
                                <th align="center">S.No</th>
                                <th align="center">AWB No</th>
                                    <c:if test="${shipType == 2}">
                                    <th align="center">SB No</th>
                                    <th align="center">Exporter</th>
                                    </c:if>
                                <th align="center">Consignee</th>
                                <th align="center">Desp Of Goods</th>
                                <th align="center">Origin</th>
                                <th align="center">Destination</th>
                                <th align="center">No Of Pcs</th>
                                <th align="center">Total Grs.Weight</th>
                                <th align="center">Value Of Goods INR</th>
                            </tr>
                        </thead>
                        <%}%>
                        <% int index1 = 13;%>
                        <c:forEach begin="12" end="23" items="${tsiViewList}" var="tsiViewList">
                            <tr height="9">
                                <%
                                    String classText1 = "";
                                    int oddEven1 = index1 % 2;
                                    if (oddEven1 > 0) {
                                        classText1 = "text2";
                                    } else {
                                        classText1 = "text1";
                                    }
                                %>
                                <td class="<%=classText1%>"  ><%=index1%></td>
                                <td class="<%=classText1%>"   ><c:out value="${tsiViewList.consignmentAwbNo}"/></td>
                                <c:if test="${shipType == 2}">
                                    <td class="<%=classText1%>"   ><c:out value="${tsiViewList.customsSbNo}"/></td>
                                    <td class="<%=classText1%>"   ><c:out value="${tsiViewList.consignorName}"/></td>
                                </c:if>
                                <td class="<%=classText1%>"   ><c:out value="${tsiViewList.consigneeName}"/></td>
                                <td class="<%=classText1%>"   ><c:out value="${tsiViewList.commodity}"/></td>
                                <td class="<%=classText1%>"   ><c:out value="${tsiViewList.awbOriginName}"/></td>
                                <td class="<%=classText1%>"   ><c:out value="${tsiViewList.awbDestinationName}"/></td>
                                <td class="<%=classText1%>">
                                    <c:if test="${tsiViewList.shipmentType == '1' }">
                                        <c:out value="${tsiViewList.totalPackages}"/>
                                        <c:set var="totalPackages" value="${totalPackages + tsiViewList.totalPackages}"/>
                                    </c:if>
                                    <c:if test="${tsiViewList.shipmentType == '2' }">
                                        <c:out value="${tsiViewList.totalPackages}"/>/<c:out value="${tsiViewList.awbTotalPackages}"/>
                                        <c:set var="totalPackages" value="${totalPackages + tsiViewList.totalPackages}"/>
                                    </c:if>
                                    <input type="hidden" id="totalPackages" value="0"/>
                                </td>
                                <td class="<%=classText1%>">
                                    <c:if test="${tsiViewList.shipmentType == '1' }">
                                        <c:out value="${tsiViewList.totalWeight}"/>
                                        <c:set var="totalWeight" value="${totalWeight + tsiViewList.totalWeight}"/>
                                    </c:if>
                                    <c:if test="${tsiViewList.shipmentType == '2' }">
                                        <c:set var="totalWeight" value="${totalWeight + tsiViewList.receivedWeight}"/>
                                        <c:out value="${tsiViewList.receivedWeight}"/>/<c:out value="${tsiViewList.awbTotalGrossWeight}"/>
                                    </c:if>
                                </td>
                                <td class="<%=classText1%>"   ><c:out value="${tsiViewList.totalCost}"/></td> 
                                <c:set var="totalCost" value="${totalCost + tsiViewList.totalCost}"/>
                                <%++index1;%>
                            </tr>
                        </c:forEach>
                        <%if(index >= 13){ %>    
                        <tr height="9" style="display:none" id="space11">
                            <c:if test="${shipType == 2}">
                                <td colspan="8" align="right">&nbsp;Total</td>
                            </c:if>
                            <c:if test="${shipType == 1}">
                                <td colspan="6" align="right">&nbsp;Total</td>
                            </c:if>
                            <td><c:out value="${totalPackages}"/></td>
                            <td><c:out value="${totalWeight}"/></td>
                            <td><c:out value="${totalCost}"/></td>
                        </tr>
                        <input type="hidden" name="rowNo" id="rowNo" value="<%=index%>"/>
                        <%}%>
                    </table>
                    <!--<div>-->
                    <%if(index >= 13){ %>
                    <table align="center"  width="100%" height="20%" style="font-size: 10px;border:none;display: none" id="spaceTable1">
                        <tr>
                            <td colspan="1"  id="displayrow21" align="right" style="margin-right: 190px;display: none"><b>Page 02 of 03</b></td>
                        </tr>
                        <tr style="border:none">
                            <td style="border:none">
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br> 
                                <br></td> 
                        </tr>
                    </table>
                    <%}%>
                    <table align="center" border="1" width="100%" height="20%" style="font-size: 10px;">
                        <% int index2 = 25;%>
                        <c:forEach begin="24" end="36" items="${tsiViewList}" var="tsiViewList">
                            <tr height="9">
                                <%
                                    String classText2 = "";
                                    int oddEven2 = index2 % 2;
                                    if (oddEven2 > 0) {
                                        classText2 = "text2";
                                    } else {
                                        classText2 = "text1";
                                    }
                                %>
                                <td class="<%=classText2%>"  ><%=index2%></td>
                                <td class="<%=classText2%>"   ><c:out value="${tsiViewList.consignmentAwbNo}"/></td>
                                <c:if test="${shipType == 2}">
                                    <td class="<%=classText2%>"   ><c:out value="${tsiViewList.customsSbNo}"/></td>
                                    <td class="<%=classText2%>"   ><c:out value="${tsiViewList.consignorName}"/></td>
                                </c:if>
                                <td class="<%=classText2%>"   ><c:out value="${tsiViewList.consigneeName}"/></td>
                                <td class="<%=classText2%>"   ><c:out value="${tsiViewList.commodity}"/></td>
                                <td class="<%=classText2%>"   ><c:out value="${tsiViewList.awbOriginName}"/></td>
                                <td class="<%=classText2%>"   ><c:out value="${tsiViewList.awbDestinationName}"/></td>
                                <td class="<%=classText2%>">
                                    <c:if test="${tsiViewList.shipmentType == '1' }">
                                        <c:out value="${tsiViewList.totalPackages}"/>
                                        <c:set var="totalPackages" value="${totalPackages + tsiViewList.totalPackages}"/>
                                    </c:if>
                                    <c:if test="${tsiViewList.shipmentType == '2' }">
                                        <c:out value="${tsiViewList.totalPackages}"/>/<c:out value="${tsiViewList.awbTotalPackages}"/>
                                        <c:set var="totalPackages" value="${totalPackages + tsiViewList.totalPackages}"/>
                                    </c:if>
                                    <input type="hidden" id="totalPackages" value="0"/>
                                </td>
                                <td class="<%=classText2%>">
                                    <c:if test="${tsiViewList.shipmentType == '1' }">
                                        <c:out value="${tsiViewList.totalWeight}"/>
                                        <c:set var="totalWeight" value="${totalWeight + tsiViewList.totalWeight}"/>
                                    </c:if>
                                    <c:if test="${tsiViewList.shipmentType == '2' }">
                                        <c:set var="totalWeight" value="${totalWeight + tsiViewList.receivedWeight}"/>
                                        <c:out value="${tsiViewList.receivedWeight}"/>/<c:out value="${tsiViewList.awbTotalGrossWeight}"/>
                                    </c:if>
                                </td>
                                <td class="<%=classText2%>"   ><c:out value="${tsiViewList.totalCost}"/></td> 
                                <c:set var="totalCost" value="${totalCost + tsiViewList.totalCost}"/>
                                <%++index2;%>
                            </tr>
                        </c:forEach>
                        <tr height="10" style="display:none" id="space3" >
                            <c:if test="${shipType == 2}">
                                <td colspan="8" align="right">&nbsp;Total</td>
                            </c:if>
                            <c:if test="${shipType == 1}">
                                <td colspan="6" align="right">&nbsp;Total</td>
                            </c:if>
                            <td ><c:out value="${totalPackages}"/></td>
                            <td ><c:out value="${totalWeight}"/></td>
                            <td ><c:out value="${totalCost}"/></td>
                        <input type="hidden" name="rowNo" id="rowNo" value="<%=index%>"/>
                        </tr>
                    </table> 
                    <input type="hidden" class="textbox" id="count1" name="count1" value="<%=index1%>">
                    <table align="right" width="100%"   style="margin-left: 100px;font-size: 10px;" >
                        <tr><td>&nbsp;</td>
                            <td align="center">&nbsp;&nbsp;&nbsp;Transhipment Application Submitted For Your Approval.<br>                     
                            </td>
                        </tr>
                        <tr><td>Transhipment may be permitted please.</td>

                            <td align="center">&nbsp;&nbsp;&nbsp;We declare the contents to be as truly stated.<br>                     
                            </td>
                        </tr>
                    </table>
                    <table align="center" width="100%" cellspacing="5" style="font-size: 10px;">
                        <tr>
                            <td>Supervised the loading of <c:out value="${totalPackages}"/> Loose Cargo on 
                                <script>
                                    document.getElementById("noOfPackage").innerHTML = document.getElementById("totalPackages").value;
                                </script>
                            </td>
                        </tr>
                        <c:if test="${shipType == 1}">
                            <tr>
                                <td>Truck/Container No:<c:out value="${fleetCode}"/></td><td>EFO</td><td align="center">Yours faithfully,<br>for TT Aviation Handling Services Pvt Ltd,</td>
                            </tr>
                        </c:if>
                        <c:if test="${shipType == 2}">
                            <tr>
                                <td>Truck No:<c:out value="${fleetCode}"/></td><td>&nbsp;</td><td align="center">Yours faithfully,<br>for TT Aviation Handling Services Pvt Ltd,</td>
                            </tr>
                        </c:if>
                        <tr>
                            <td>Destined to <c:out value="${toAddress}"/> Acc and sealed With Customs</td>
                        </tr>
                        <c:set var="count" value="1" scope="page" />
                        <c:forEach items="${tsiViewList}" var="tsiViewList">
                            <c:if test="${count == 1}">
                                <tr>
                                    <td colspan="2">OTL  Seal No.<label id="otlNoDisplay" style="display: none">0</label>
                                        <input type="text" name="otlNo" id="otlNo"  style="width: 420px;" value="<c:out value="${tsiViewList.otlNo}"/>"  />
                                        <input type="button" name="EditOtlNo" value="EditOtlNo"  id="EditOtlNo" onclick="showOtlEditOption()" /></td>

                                </tr><input type="hidden" name="consignmentOrderId" id="consignmentOrderId" value="<c:out value="${tsiViewList.consignmentOrderId}"/>"/>
                                <c:set var="count" value="${count + 1}" scope="page" />
                            </c:if>
                        </c:forEach>
                        <c:if test="${shipType == 1}">
                            <tr>
                                <td>
                                    Covered by the T. P. Number <c:choose>
                                        <c:when test="${shipType ==1}">
                                            ITSA  
                                        </c:when>
                                        <c:otherwise>
                                            ETSA 
                                        </c:otherwise>
                                    </c:choose> <u><c:out value="${consignmentTpNo}"/></u>________/15__

                                </td>
                                <td align="right">&nbsp;<td align="center"><b>	Authorised signature.</b></td>
                            </tr>
                            <tr>
                                <td>
                                    Checked Custom Seal No :&emsp;&emsp;&emsp;&emsp;&emsp; 
                                </td>
                            </tr>
                        </c:if>
                        <c:if test="${shipType == 2}">
                            <tr>
                                <td>
                                    Checked Custom Seal No :&emsp;&emsp;&emsp;&emsp;&emsp; On the Truck No : <c:out value="${fleetCode}"/> 
                                </td>
                                <td align="right">&nbsp;<td align="center"><b>	Authorised signature.</b></td>
                            </tr>
                        </c:if>
                        <tr>
                            <td colspan="2">
                                Packages covered by this application have been found in good condition
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr style="margin-top: 280px;">
                            <td colspan="1" align="center"><b>Name and Signature of the custom officer at destination</b></td>
                            <td colspan="1"  id="displayrow1" align="right" style="margin-right: 190px;display: none"><b>Page 01 of 01</b></td>
                            <td colspan="1"  id="displayrow2" align="right" style="margin-right: 190px;display: none"><b>Page 02 of 02</b></td>
                            <td colspan="1"  id="displayrow3" align="right" style="margin-right: 190px;display: none"><b>Page 03 of 03</b></td>
                        </tr>
                    </table>
                </div>
                <center>
                    <input type="button" class="button" name="print" id="print" value="Print" onclick="printPage()"/>&nbsp;&nbsp;&nbsp;<input type="button" class="button" id="close" name="Close"  value="Close" onclick="closeWindow()"/>
                </center>
            </c:if>

            <script type="text/javascript">
                function checkSpace() {
                    var count = document.getElementById("count").value;
                    var count1 = 0;
                    if (count <= 12) {
                        $("#spaceTable").hide();
                        $("#space2").hide();
                        $("#space1").show();
                        $("#spaceTable1").hide();
                        $("#space3").hide();
                        $("#displayrow1").show();
                        $("#displayrow2").hide();
                        $("#displayrow3").hide();
                        $("#displayrow21").hide();
                        $("#space3").hide();
                    }
                    else {
                        if (count = 12) {
                            $("#spaceTable").hide();
                            $("#space2").hide();
                            $("#space1").show();
                            $("#spaceTable1").hide();
                            $("#space3").hide();
                            $("#displayrow1").show();
                            $("#displayrow2").hide();
                            $("#displayrow3").hide();
                            $("#displayrow21").hide();
                            $("#space3").hide();
                        }

//                        alert("Hi I am Here");
                        count1 = document.getElementById("count1").value;
                    }
                    if (count1 > 13 && count1 <= 24) {
                        $("#displayrow1").hide();
                        $("#displayrow2").show();
                        $("#displayrow3").hide();
                        $("#displayrow11").show();
                        $("#displayrow12").hide();
                        $("#spaceTable").show();
                        $("#space11").show();
                        $("#spaceTable1").hide();
                        $("#displayrow21").hide();
                        $("#space3").hide();
                        $("#space1").hide();
                    }
                    if (count1 > 25) {
                        $("#displayrow1").hide();
                        $("#displayrow2").hide();
                        $("#displayrow3").show();
                        $("#displayrow11").hide();
                        $("#displayrow12").show();
                        $("#displayrow21").show();
                        $("#spaceTable1").show();
                        $("#space3").show();
                        $("#space1").hide();
                    }
                }
            </script>
            <script type="text/javascript">
                function printPage()
                {
//                    $("#displayrow").show();
//                    $("#displayrow2").show();
//                    $("#displayrow1").hide();
                    document.getElementById("print").style.display = 'none';
                    document.getElementById("close").style.display = 'none';
                    document.getElementById("EditOtlNo").style.display = 'none';
                    document.getElementById('otlNo').style.display = 'none';
                    var DocumentContainer = document.getElementById('printDiv');
                    var WindowObject = window.open('', "TSA Print View",
                            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                    WindowObject.document.writeln(DocumentContainer.innerHTML);
                    WindowObject.document.close();
                    WindowObject.focus();
                    WindowObject.print();
                    //WindowObject.close();   
                }




                function closeWindow() {
                    window.close();
                }
                function showOtlEditOption() {
                    var EditOtlNo = document.getElementById("EditOtlNo").value;
                    if (EditOtlNo == "EditOtlNo") {
                        document.getElementById('otlNo').readOnly = false;
                        document.getElementById("EditOtlNo").value = "InsertOltNo";
                    }
                    if (EditOtlNo == "InsertOltNo") {
                        var consignmentOrderId = document.getElementById("consignmentOrderId").value;
                        var otlNo = document.getElementById("otlNo").value;
                        $.ajax({
                            url: '/throttle/saveOtlSealNo.do',
                            data: {otlNo: otlNo, consignmentOrderId: consignmentOrderId},
                            dataType: 'json',
                            success: function (data) {
                                if (data == '' || data == null) {
                                    alert('');
                                } else {
                                    $.each(data, function (i, data) {
                                        document.getElementById("otlNo").value = data.Name;
                                    });



                                }
                            }
                        });
                        document.getElementById("EditOtlNo").style.display = 'none';
                        document.getElementById('otlNo').style.display = 'none';
                        $("#otlNoDisplay").text(document.getElementById('otlNo').value);
                        $('#otlNoDisplay').show();
                    }



                }

            </script>
            <input type="hidden" name="otlHidden" id="otlHidden" value="0"/>
        </form>
    </body>

</html>

