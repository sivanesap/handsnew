<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
    function checksubmit()
    {var errStr = "";

        
        var nameCheckStatus = $("#checkListNameStatus").text();
//        if(textValidation(document.checkList.checkListName,'checkListName')){
//            return;
//        }
//        if(document.checkList.checkListStage.value==0){
//            alert("checkList Type should not be Empty");
//             document.checkList.checkListStage.focus();
//            return;
//            }
//        if(textValidation(document.checkList.checkListDate,'checkListDate')){
//            return;
//        }
        if(document.getElementById("checkListName").value == "") {
            errStr = "Please enter Check List Name.\n";
            alert(errStr);
            document.getElementById("checkListName").focus();
        }else if(nameCheckStatus != "") {
            errStr ="CheckList Name Already Exists.\n";
            alert(errStr);
            document.getElementById("checkListName").focus();
        }
        else if(document.checkList.checkListStage.value==0){
            alert("checkList Type should not be Empty");
             document.checkList.checkListStage.focus();
            return;
            }
        else if(document.getElementById("checkListStage").value == "") {
            errStr ="Please enter check List Stage.\n";
            alert(errStr);
            document.getElementById("checkListStage").focus();
        }
         else if(document.getElementById("checkListDate").value == "") {
            errStr ="Please enter Check List Date.\n";
            alert(errStr);
            document.getElementById("checkListDate").focus();
        }

        if(errStr == "") {
            document.checkList.action=" /throttle/saveCheckList.do";
            document.checkList.method="post";
            document.checkList.submit();
        }
}


    
    function setValues(sno,checkListName,checkListStage,checkListDate,checkListStatus,checkListId){
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if(i != sno) {
                document.getElementById("edit"+i).checked = false;
            } else {
                document.getElementById("edit"+i).checked = true;
            }
        }
        document.getElementById("checkListId").value = checkListId;
        document.getElementById("checkListName").value = checkListName;
        document.getElementById("checkListStage").value = checkListStage;
        document.getElementById("checkListDate").value = checkListDate;
        document.getElementById("checkListStatus").value = checkListStatus;
    }






    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }

var httpRequest;
    function checkCheckListName() {

        var checkListName = document.getElementById('checkListName').value;
        
            var url = '/throttle/checkCheckList.do?checkListName=' + checkListName ;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);
       
    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#checkListNameStatus").text('Please Check checkList Name ' + val+' is Already Exists');
                } else {
                     $("#nameStatus").hide();
                    $("#checkListNameStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

</script>
<!--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>-->
    
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CheckList" text="CheckList"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.CheckList" text="CheckList"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    
    <body onload="document.standardCharges.stChargeName.focus();">


        <form name="checkList"  method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            
            <%@ include file="/content/common/message.jsp" %>
            
            
           <table class="table table-info mb30 table-hover" id="bg" >	
			<thead>
               
                <tr>
                     <th  colspan="4" >CheckList Details</th>
                </tr>
                        </thead>
                <tr>
                    <td  colspan="4" align="center" style="display: none" id="nameStatus"><label id="checkListNameStatus" style="color: red"></label></td>
                </tr>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font>CheckList Name</td>
                    <td ><input type="text" name="checkListName" id="checkListName" class="form-control" style="width:250px;height:40px" maxlength="50" onkeypress="return onKeyPressBlockNumbers(event);" onchange="checkCheckListName();" autocomplete="off"></td>

                    <td >&nbsp;&nbsp;<font color="red">*</font>Stage</td>
                    <td >
                        <select style="width:250px;height:40px" name="checkListStage" id="checkListStage">
                            <option value="0">--Select--</option>
                            <c:if test="${stageList != null}">
                                <c:forEach items="${stageList}" var="stage">
                                    <option value='<c:out value="${stage.stageId}"/>'><c:out value="${stage.stageName}"/></option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font>Effective Date</td>
                    <td ><input type="textbox" class="datepicker" name="checkListDate" id="checkListDate" value="" style="width:250px;height:40px" /></td>
                    <td >&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                    <td >
                        <select  align="center" class="textbox" name="checkListStatus" id="checkListStatus"  style="width:250px;height:40px">
                            <option value='Y'>Active</option>
                            <option value='N' id="inActive" style="display: none">In-Active</option>
                        </select>
                    </td>
                </tr>
                
             </table>
                </tr>
                <tr>
                    <td>
                        
                        <center>
                            <input type="button" class="btn btn-success"  value="Save" name="Submit" onClick="checksubmit()">
                        </center>
                    </td>
                </tr>
            </table>
            
            


            <h2 align="center">CheckList View</h2>
           <table class="table table-info mb30 table-hover" id="table" >	
			
                <thead>
                    <tr height="30">
                        <th>S.No</th>
                        <th>CheckList Name</th>
                        <th>CheckList Type</th>
                        <th>Effective Date</th>
                        <th>Select</th>
                    </tr>
                </thead>
                <tbody>
<% int sno = 0;%>
                    <c:if test = "${checkList != null}">
                        <c:forEach items="${checkList}" var="checkList">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno + 1%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${checkList.checkListName}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${checkList.checkListStage}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${checkList.checkListDate}"/></td>
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues( <%= sno%>,'<c:out value="${checkList.checkListName}" />','<c:out value="${checkList.checkListStage}" />','<c:out value="${checkList.checkListDate}" />','<c:out value="${checkList.checkListStatus}" />','<c:out value="${checkList.checkListId}" />');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>


            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            
            
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>