<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<!--<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>-->
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<style>
    .form-control:focus{border-color: #5cb85c;  box-shadow: none; -webkit-box-shadow: none;} 
    .has-error .form-control:focus{box-shadow: none; -webkit-box-shadow: none;}
</style>
<meta http-equiv="ConConsignment Notent-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    function viewConsignmentDetails(orderId) {
        window.open('/throttle/getConsignmentDetails.do?consignmentOrderId=' + orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<style>
    #rcorners1 {
        border-radius: 35px;
        background: url(paper.gif);
        background-position: left top;
        background-repeat: repeat;
        padding: 5px; 
        width: 400px;
        height: 150px;
    }
</style>

</script>

</head>

<script>
    function updateOrder(orderId) {
        document.getElementById("confirmBtn").style.display="none";
        var param = "1";
        document.schedule.action = "/throttle/updateDeliverySchedule.do?orderId=" + orderId + "&param=" + param;
        document.schedule.submit();
    }

</script>

<body>
    <%
                String menuPath = "Delivery Schedule>> View / Edit";
                request.setAttribute("menuPath", menuPath);
    %>
    <form name="schedule" method="post" >
        <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Delivery Schedule" text="Delivery Schedule"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
                    <li class=""><spring:message code="hrms.label.Delivery Schedule" text="Delivery Schedule"/></li>
                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <br>
                    <br>
                    <c:if test = "${getViewOrderLists != null}" >
                        <c:forEach items="${getViewOrderLists}" var="cnl">
                            <table class="table table-info mb30 table-hover" style="width:80%;" >
                                <thead><tr><th colspan="4">Delivery Schedule</th></tr></thead>
                                <tr>
                                    <td>Customer</td>
                                    <td height="30">
                                        <c:out value="${cnl.clientName}"/>
                                    </td>
                                    <td>Loan Proposal ID </td>
                                    <td height="30">
                                        <c:out value="${cnl.loanProposalID}"/>
                                        <input type="hidden" name="loanProposalID" id="loanProposalID" value='<c:out value="${cnl.loanProposalID}"/>'/>
                                        <input type="hidden" name="orderId" id="orderId" value='<c:out value="${cnl.orderId}"/>'/>
                                    </td>

                                </tr>
                                <tr>
                                    <td>Batch Number </td>
                                    <td height="30">
                                        <c:out value="${cnl.batchNumber}"/>
                                    </td>
                                    <td>Batch Date </td>
                                    <td height="30">
                                        <c:out value="${cnl.batchDate}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Branch</td>
                                    <td height="30">
                                        <c:out value="${cnl.branchName}"/>
                                    </td>
                                    <td>Model</td>
                                    <td height="30">
                                        <c:out value="${cnl.model}"/>
                                    </td>

                                </tr>
                                <tr>
                                    <td>Product </td>
                                    <td height="30">
                                        <c:out value="${cnl.productID}"/>
                                    </td>
                                    <td><font color="red">*</font>Schedule Date </td>
                                    <td height="30"><input name="schedulerDate" id="schedulerDate" type="text"  class="datepicker , form-control" style="width:200px;height:40px" value="" autocomplete="off">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Remarks</td>
                                    <td><textarea name="remarks" id="remarks" class="form-control" ></textarea></td>
                                    <td><font color="red">*</font>Schedule Time </td>
                                    <td>
                                        HH:<select name="scheduleHour" id="scheduleHour" class="form-control" style="width:70px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                        MI:<select name="scheduleMin" id="scheduleMin" class="form-control" style="width:70px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
                                    </td>
                                </tr>
                            </table>
                            <center>
                                <div>  <input type="button" class="btn btn-success"   value="Confirm" id="confirmBtn" onclick="updateOrder('<c:out value="${cnl.orderId}"/>')"></div>
                            </center>
                            <br>
                        </c:forEach>
                    </c:if>
                    <br>
                    <br>
                    <!--<center>-->
                    <!--<input type="button" class="btn btn-success" name="trip" id="Confirm" value="Proceed" onclick="submitPageForTripGeneration()" />-->
                    <!--</center>-->
                    </form>
                    </body>
                </div>
            </div>
        </div>
        <!--<script async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&callback=initMap"></script>-->
        <%@ include file="../common/NewDesign/settings.jsp" %>