<%-- 
    Document   : tsaPrintView
    Created on : Mar 5, 2015, 11:38:59 AM
    Author     : srinientitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
          <script language="javascript" src="/throttle/js/validate.js"></script>
          <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
          <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
          <%@ page import="java.util.* "%>
          <%@ page import=" javax. servlet. http. HttpServletRequest" %>
          <%@ page import="java.text.DecimalFormat" %>
         <%@ page import="java.text.NumberFormat" %>
         <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
          <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
          <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
          <script language="javascript" src="/throttle/js/validate.js"></script>
          <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
          <style type="text/css" title="currentStyle">
          @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
          </style>
         
          
          <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
          <!-- jQuery libs -->
          <script  type="text/javascript" src="/throttle/js/jquery-1.7.1.js"></script>
          <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
          <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
          <!-- Our jQuery Script to make everything work -->
         <script  type="text/javascript" src="js/jq-ac-script.js"></script>
           <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
          <script src="/throttle/js/jquery.ui.core.js"></script>
          <script src="/throttle/js/jquery.ui.datepicker.js"></script>
          <style >
          .text11 {
	  	height:10px;
	  	
	          font-size:9px;
	  	font-weight:normal;
	  }
	  
	  .text21 {
	  	height:10px;
	  	
	          font-size:9px;
	  	font-weight:normal;
}
          </style>
    </head>
    <body>
        <form name="tsaPrint" method="post">
            <br>
           
            <div id="printDiv">
            <table align="center" width="100%">
                <tr><td><font size="2"><center><br/>TT AVIATION HANDLING SERVICES PVT LTD BANGALORE.<br>
                                <c:choose>
                                            <c:when test="${shipType ==1}">
                                                <b>IMPORT</b>
                                            </c:when>
                                            <c:otherwise>
                                                <b>EXPORT</b>
                                            </c:otherwise>
                                        </c:choose>
                                TRANSHIPMENT </center></font></td></tr>
            </table>
            <br>
            <table align="center" width="100%">
                <tr>
                    <td>
                       <table align="center" width="100%">
                            <tr><td><b>From</b></td></tr>
                            <tr><td>The Asst Commissioner of Customs, <c:out value="${fromAddress}"/></b></td></tr>
                            <tr><td>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<b>Sub:</b>Movement of  <c:choose>
                                            <c:when test="${shipType ==1}">
                                                <b>Import</b>
                                            </c:when>
                                            <c:otherwise>
                                                <b>Export</b>
                                            </c:otherwise>
                                        </c:choose> Cargo By Container's/Truck's from  <c:out value="${fromAddress}"/> to <c:out value="${toAddress}"/> . </b></td></tr>
                            <tr><td><b>To</b></td></tr>
                            <tr><td>The Asst Commissioner of Customs,<c:out value="${toAddress}"/> </b></td></tr>
                        </table> 
                    </td>
                    <td>
                    
                       &nbsp;
                    </td>
                    <td>
                    <table align="center"  width="100%" style="text-align: center" >
                        <tr><td align="left"><font size="2">Flight No:<b>&nbsp;&nbsp;<c:out value="${flightNo}"/></b></font></td>    </tr>
                            <tr><td align="left"><font size="2">Date:<b>&nbsp;&nbsp;<c:out value="${flightDate}"/></b></font></td>    </tr>
                            <tr><td align="left"><font size="2">
                                        <c:choose>
                                            <c:when test="${shipType ==1}">
                                                IGM No :<b><c:out value="${consignmentEgmNo}"/></b>
                                            </c:when>
                                            <c:otherwise>
                                                EGM No :<b><c:out value="${consignmentEgmNo}"/></b>
                                            </c:otherwise>
                                        </c:choose></font></td>    </tr>
                            <tr><td align="left"><font size="2">TP No:<b><c:out value="${consignmentTpNo}"/></b></font></td>    </tr>
                    </table>
                    </td>
                </tr>
            </table>
                           
                            
                            <table align="center" width="100%">
                                <tr>
                                    <td>
                                      Dear Sir / Madam,</td> </tr>
                                <tr>
                                    <td>
                                        Please grant Permission to tranship the following shipments :</td> <td>Country Of Origin:<b>BANGALORE ACC -India</b></td></tr>

                            </table>

            
         <c:set var="totalPackages" value="0" />
        <c:if test="${tsiViewList != null}">
       
         <table align="center" border="1" width="100%" height="50px">
                <thead>
                    <tr height="10">
                        <th align="center">S.No</th>
                        <th align="center">AWB No</th>
                         <c:if test="${shipType ==2 }">
                        <th align="center">SB No</th>
                        </c:if>
                        <th align="center">Consignor</th>
                        <th align="center">Consignee</th>
                        <!--<th align="center">Exporter</th>-->
                        <th align="center">Desp Of Goods</th>
                        <th align="center">Origin</th>
                        <th align="center">Destination</th>
                        <th align="center">No Of Pcs</th>
                        <th align="center">Total Weight(Kgs)</th>
                        <th align="center">Value Of Goods(Rs.)</th>
                        
<!--                        <th align="center">Status</th>-->
                    </tr>
                </thead>
                <% int index = 1;%>
                <c:set var="totalCost" value="0"/>
               
                <c:forEach items="${tsiViewList}" var="tsiViewList">
                    <tr height="10">
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text21";
                                    } else {
                                        classText = "text11";
                                    }
                        %>
                        <td class="<%=classText%>"  ><%=index%></td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.consignmentAwbNo}"/></td>
                        <c:if test="${shipType ==2 }">
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.customsSbNo}"/></td>
                        </c:if>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.consignorName}"/></td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.consigneeName}"/></td>
                        <!--<td class="<%=classText%>"   ><%--<c:out value="${tsiViewList.customerName}"/>--%></td>-->
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.commodity}"/></td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.awbOriginName}"/></td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.awbDestinationName}"/></td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.totalPackages}"/></td>
                            <c:set var="totalPackages" value="${totalPackages + tsiViewList.totalPackages}"/>
                            <c:set var="dest" value="${tsiViewList.awbDestinationName}"/>
                            
                            <input type="hidden" id="totalPackages" value="0"/>
                            
                        </td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.totalWeight}"/>
                          <c:set var="totalWeight" value="${totalWeight + tsiViewList.receivedWeight}"/></td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.totalCost}"/>
                        <c:set var="totalCost" value="${totalCost + tsiViewList.totalCost}"/>
                        </td>                    
                       

                        <%++index;%>
                    </tr>
                </c:forEach>
                                   
		                    <tr height="7">
		                    <c:if test="${shipType ==1 }">
		                        <td align="center" class="text21"  colspan="7" ><b>Total</b></td>
		                        </c:if>
		                         <c:if test="${shipType ==2 }">
					<td align="center" class="text21"  colspan="8" ><b>Total</b></td>
		                        </c:if>
		                        <td  class="text21"   ><c:out value="${totalPackages}"/>
		                        </td><td class="text21"   ><c:out value="${totalWeight}"/></td>
		                        <td  class="text21"   >
                                            <fmt:formatNumber type="number" pattern="##0.00" minFractionDigits="0" value="${totalCost}"/>
                                        </td>
		                      
		                       
		                    </tr>

                    <input type="hidden" name="rowNo" id="rowNo" value="<%=index%>"/>
            </table>
           
            <table align="right" width="100%"   style="margin-left:100px" cellspacing="5" >
                <tr>
                    <td>We declare the contents to be as truly stated.</td>
                </tr>
                <tr>
                <td>Transhipment Application submitted for your Approval.</td>
                </tr>
                <tr>
                    <td>Transhipment may be permitted please.</td>
                </tr>
            </table>
            <table align="center" width="100%" cellspacing="1">
                <tr>
                    <td>Supervised the loading of No of Pcs <b><c:out value="${totalPackages}"/></b> 
                        <script>
                            document.getElementById("noOfPackage").innerHTML=document.getElementById("totalPackages").value;
                        </script>
                    </td>
                </tr>
                <tr>
                    <td>Destined to <c:out value="${toAddress}"/> ACC on Truck/Container No:<b><c:out value="${fleetCode}"/></b></td><td><c:out value="${fromAddress}"/> Customs</td><td align="center">Yours faithfully,<br>TT Aviation Handling Services Pvt Ltd,Bangalore</td>
                </tr>
                <c:set var="count" value="1" scope="page" />
                <c:forEach items="${tsiViewList}" var="tsiViewList">
                    <c:if test="${count == 1}">
                <tr>
                    <td colspan="2">Sealed With Customs seal no's <label id="otlNoDisplay" style="display: none">0</label>
                        <input type="text" name="otlNo" id="otlNo"  style="width: 120px;" value="<c:out value="${tsiViewList.otlNo}"/>"  />
                        <input type="button" name="EditOtlNo" value="EditOtlNo"  id="EditOtlNo" onclick="showOtlEditOption()" /></td>
                        
                </tr><input type="hidden" name="consignmentOrderId" id="consignmentOrderId" value="<c:out value="${tsiViewList.consignmentOrderId}"/>"/>
               <c:set var="count" value="${count + 1}" scope="page" />
                    </c:if>
                </c:forEach>
                <tr>
                    <td>
                        VALID TO ALL CHECK POSTS IN KARNATAKA,TAMILNADU,ANDRAPRADESH,KERELA
                    </td>
                    <td align="right"><b>Inspector / Superintendent of Customs.</b><td align="center"></td>
                </tr>
            </table>
            </div>
            <br>
            <center>
                <input type="button" class="button" name="print" id="print" value="Print" onclick="printPage()"/>&nbsp;&nbsp;&nbsp;<input type="button" class="button" id="close" name="Close"  value="Close" onclick="closeWindow()"/>
        </center>

        </c:if>
       
                        <script type="text/javascript">
                              function printPage()
                        {       
                         document.getElementById("print").style.display='none';
						                                 document.getElementById("close").style.display='none';
			                                 document.getElementById("EditOtlNo").style.display='none';
                            var DocumentContainer = document.getElementById('printDiv');
                            var WindowObject = window.open('', "TSA Print View", 
                                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                            WindowObject.document.writeln(DocumentContainer.innerHTML);
                            WindowObject.document.close();
                            WindowObject.focus();
                            WindowObject.print();
                            //WindowObject.close();   
                        }            
            
            
                            
                            
                            function closeWindow(){
                             window.close();
                            }
                            function showOtlEditOption(){
                                var EditOtlNo=document.getElementById("EditOtlNo").value;
                                if(EditOtlNo=="EditOtlNo"){
                                document.getElementById('otlNo').readOnly = false;
                                document.getElementById("EditOtlNo").value="InsertOltNo";
                                }
                                if(EditOtlNo=="InsertOltNo"){
                                    var consignmentOrderId = document.getElementById("consignmentOrderId").value;
                                    var otlNo = document.getElementById("otlNo").value;
                                     $.ajax({
                                            url: '/throttle/saveOtlSealNo.do',
                                            data: {otlNo: otlNo, consignmentOrderId: consignmentOrderId},
                                            dataType: 'json',
                                            success: function(data) {
                                                if (data == '' || data == null) {
                                                    alert('');
                                                } else {
                                                    $.each(data, function(i, data) {
                                                        document.getElementById("otlNo").value = data.Name;
                                                    });



                                                }
                                            }
                                        });
                                        document.getElementById("EditOtlNo").style.display='none';
                                        document.getElementById('otlNo').style.display='none';
                                        $("#otlNoDisplay").text(document.getElementById('otlNo').value);
                                         $('#otlNoDisplay').show();
                                }
                                


                            }

                        </script>
                                <input type="hidden" name="otlHidden" id="otlHidden" value="0"/>
                                 </form>
    </body>

</html>