<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %>


<div class="pageheader">
    <h2><i class="fa fa-home"></i> Dashboard
        <!--<span>Operations</span>-->
    </h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li>Dashboard</li>
            <!--<li class="active">Operations</li>-->
        </ol>
    </div>
</div>

<div class="contentpanel" style="overflow: auto;height:1600px">

    <div class="row">

        <div class="col-sm-6 col-md-3">
            <div class="panel panel-success panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">

                                <i class="ion ion-android-bus"></i>
                            </div>
                            <div class="col-xs-8">

                                <h4> <spring:message code="dashboard.label.Vendors"  text="Vendors"/></h4>

                                <h1><span id="totalVendors"></span></h1>
                            </div>
                        </div><!-- row -->

                        <div class="mb15"></div>

                    </div><!-- stat -->

                </div><!-- panel-heading -->
            </div><!-- panel -->
        </div><!-- col-sm-6 -->


        <div class="col-sm-6 col-md-3">
            <div class="panel panel-warning panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="ion ion-happy"></i>
                            </div>
                            <div class="col-xs-8">
                                <h4> <spring:message code="dashboard.label.Vehicles"  text="Vehicles"/></h4>
                                <h1><span id="totalCustomers"></span></h1>
                            </div>
                        </div><!-- row -->

                        <div class="mb15"></div>



                    </div><!-- stat -->

                </div><!-- panel-heading -->
            </div><!-- panel -->
        </div><!-- col-sm-6 -->
    </div><!-- row -->

    <table style="width:100%">
        <tr>
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer2">
                                <font color="white"><b> Order Status</b>  </font>
                                <div  id="orderStatus" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                            </div>                        
                        </div>
                    </div>

                </div>
            </td>
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                                               
                        
                        <div class="panel panel-default">

                            <div class="outer3">
                                <font color="white"><b>Today Delivery Status</b>  </font>
                                <div  id="todDeliveryStatus" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                            </div>                        
                        </div>
                        
                    </div>

                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer4">
                                <font color="white"><b> Monthly Delivery Status</b>  </font>
                                <div  id="monDeliveryStatus" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                            </div>                        
                        </div>
                    </div>

                </div>
            </td>
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                                               
                        
                        <div class="panel panel-default">

                            <div class="outer1">
                                <font color="white"><b>Overall Delivery Status</b>  </font>
                                <div  id="deliverystatus" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                            </div>                        
                        </div>
                        
                    </div>

                </div>
            </td>
        </tr>
        
        <tr>

            <td >
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">
                            <div class="outer2">
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div><!-- panel-btns -->
                                <h5  class="panel-title"><font color="white"> <spring:message code="dashboard.label.TripStatus"  text="Trip Status"/></font></h5>
                                <div  id="tripStatus" style="min-width: 550px; height: 400px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
            <td>
                <div class="col-sm-8 col-md-9">
                    <div class="panel panel-default">
                        <div class="outer3">
                            <div class="panel-btns">
                                <a href="" class="panel-close">&times;</a>
                                <a href="" class="minimize">&minus;</a>
                            </div><!-- panel-btns -->
                            <h5  class="panel-title"><font color="white"> <spring:message code="dashboard.label.MonthWiseOrder"  text="Month Wise Order"/></font></h5>
                            <div  id="monthStatus" style="min-width: 550px; height: 400px; margin: 0 auto"></div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>





</div><!-- contentpanel -->

</div><!-- mainpanel -->

<%@ include file="../common/NewDesign/settings.jsp" %>



<script>

    getTodDeliveryStatus();
    function getTodDeliveryStatus() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/getTodTripNos.do',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(key, value) {
                    x_values_sub['name'] = value.Name;
                    x_values_sub['y'] = parseInt(value.Count);
                    x_values.push(x_values_sub);
                    x_values_sub = {};

                });

                $('#todDeliveryStatus').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });

    }
    
    getMonDeliveryStatus();
    function getMonDeliveryStatus() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/getMonTripNos.do',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(key, value) {
                    x_values_sub['name'] = value.Name;
                    x_values_sub['y'] = parseInt(value.Count);
                    x_values.push(x_values_sub);
                    x_values_sub = {};

                });

                $('#monDeliveryStatus').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });

    }
    
    getDeliveryStatus();
    function getDeliveryStatus() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/getOverAllTripNos.do',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(key, value) {
                    x_values_sub['name'] = value.Name;
                    x_values_sub['y'] = parseInt(value.Count);
                    x_values.push(x_values_sub);
                    x_values_sub = {};

                });

                $('#deliverystatus').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });

    }


    orderStatus();
    function orderStatus() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;

        $.ajax({
            url: '/throttle/getRouteWiseTripNos.do',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(key, value) {
                    //alert(value.Name);
                    x_values_sub['name'] = value.Name;
                    x_values_sub['y'] = parseInt(value.Count);
                    x_values.push(x_values_sub);
                    x_values_sub = {};
//                    }
                });

                $('#orderStatus').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });
    }


    function2();
    function function2() {
        var x_values = [];
        var y_values = [];
        $.ajax({
            url: '/throttle/getStatusWiseTripNos.do',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(key, value) {
                    x_values.push(value.Name);
                    y_values.push([parseInt(value.Count)]);
                });
                $('#tripStatus').highcharts({
                    chart: {type: 'column',
                        options2d: {
                            enabled: true,
                            alpha: 15,
                            beta: 15,
                            depth: 50,
                            viewDistance: 25
                        }
                    }, plotOptions: {
                        series: {
                            depth: 25,
                            colorByPoint: true
                        }
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    }, credits: {
                        enabled: false
                    },
                    xAxis: {
                        type: 'category',
                        categories: x_values,
                        labels: {
                            style: {
                                fontSize: '13px',
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: '{series.name} : <b>{point.y:f} Nos</b>'
                    },
                    series: [{
                            name: '',
                            colorByPoint: true,
                            data: y_values,
                            dataLabels: {
                                enabled: true,
                                rotation: 180,
                                color: '#FFFFFF',
                                align: 'right',
                                format: '{point.y:f}',
                                y: 10,
                                style: {
                                    fontSize: '0px',
                                }
                            }
                        }]
                });
            }
        });
    }
    function3();
    function function3() {
        var x_values = [];
        var y_values = [];
        $.ajax({
            url: '/throttle/getCustRevenu.do',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(key, value) {
                    x_values.push(value.Name);
                    y_values.push([parseInt(value.Count)]);
                });
                $('#monthStatus').highcharts({
                    chart: {type: 'column',
                        options2d: {
                            enabled: true,
                            alpha: 15,
                            beta: 15,
                            depth: 50,
                            viewDistance: 25
                        }
                    }, plotOptions: {
                        series: {
                            depth: 25,
                            colorByPoint: true
                        }
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    }, credits: {
                        enabled: false
                    },
                    xAxis: {
                        type: 'category',
                        categories: x_values,
                        labels: {
                            style: {
                                fontSize: '13px',
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: '{series.name} : <b>{point.y:f} Nos</b>'
                    },
                    series: [{
                            name: '',
                            colorByPoint: true,
                            data: y_values,
                            dataLabels: {
                                enabled: true,
                                rotation: 180,
                                color: '#FFFFFF',
                                align: 'right',
                                format: '{point.y:f}',
                                y: 10,
                                style: {
                                    fontSize: '0px',
                                }
                            }
                        }]
                });
            }
        });
    }


</script>

<script>
    loadTrucknos();
    function loadTrucknos() {
        $.ajax({
            url: '/throttle/getDashBoardOperationNos.do',
            dataType: 'json',
            success: function(data) {
                //alert(data);
                $.each(data, function(key, value) {
                    //alert(value.Name);
                    if (value.Name == 'Vendor') {
                        $("#totalVendors").text(value.Count);
                    } else {
                        $("#totalCustomers").text(value.Count);
                    }
                });
            }
        });
    }
</script>


<style>
    .outer1 {
        width: 600px;
        color: navy;            
        background-color: #1caf9a;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer2 {
        width: 600px;
        color: navy;
        background-color: #a94442;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer3 {
        width: 600px;
        color: navy;
        background-color: #f0ad4e;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer4 {
        width: 600px;
        color: navy;
        background-color: #ccad4e;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer5 {
        width: 500px;
        color: navy;            
        background-color: #1caf9a;
        border: 2px solid darkblue;
        padding: 5px;
    }

    .ben{
        width: 600px;
    }



    .highcharts-figure, .highcharts-data-table table {
        min-width: 320px; 
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }


    input[type="number"] {
        min-width: 50px;
    }





    .highcharts-figure, .highcharts-data-table table {
        min-width: 310px; 
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }

    .highcharts-figure, .highcharts-data-table table {
        min-width: 310px; 
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }



</style>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

