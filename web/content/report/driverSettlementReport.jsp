<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        <script type="text/javascript" language="javascript">
            var poItems = 0;
            var rowCount='';
            var sno='';
            var snumber = '';            
            
            function setDefaultValue(){
                //alert("i'm here");
                var sdate='<%=request.getAttribute("fromDate")%>';
                var edate='<%=request.getAttribute("toDate")%>';
                var regno='<%=request.getAttribute("regNo")%>';
                var driName='<%=request.getAttribute("driName")%>';

                if(sdate!='null' && edate!='null'){
                    document.settle.fromDate.value=sdate;
                    document.settle.toDate.value=edate;
                    document.settle.toDate.value=edate;
                    document.getElementById("tabs").style.display='block';
                } if(regno!='null'){
                    document.settle.regno.value=regno;
                }
                if(driName!='null'){
                    document.settle.driName.value=driName;
                }

                document.settle.suDriverName.value = '<%= request.getAttribute("driName")%>';
                document.settle.suVehicleNo.value = '<%= request.getAttribute("regNo")%>';
                document.settle.suFromDate.value = '<%= request.getAttribute("fromDate")%>';
                document.settle.suToDate.value = '<%= request.getAttribute("toDate")%>';
                document.settle.bulkerCrown.value = '<%= request.getAttribute("bulkerCrown")%>';


                document.settle.startKM.value = '<%= request.getAttribute("outKm")%>';
                document.settle.endKM.value = '<%= request.getAttribute("inKm")%>';
                document.settle.totalDiesel.value = '<%= request.getAttribute("totalFuel")%>';
                document.settle.dieselAmt.value = Math.ceil('<%= request.getAttribute("totalFuelAmount")%>');
                document.settle.driverExpense.value = '<%= request.getAttribute("driExpense")%>';
                document.settle.driverSalary.value = '<%= request.getAttribute("driSalary")%>';

                //document.settle.expenses.value = '<%= request.getAttribute("genExpense")%>';

                document.settle.driverAdvance.value = '<%= request.getAttribute("driAdvance")%>';
                document.settle.runningKM.value = (parseInt(document.settle.endKM.value) - parseInt(document.settle.startKM.value));
                document.settle.totalTonAmount.value = '<%= request.getAttribute("totalTonnage")%>';
                document.settle.income.value = '<%= request.getAttribute("totalTonnage")%>';
                document.settle.suTotalTon.value = '<%= request.getAttribute("totalTonnage")%>';
                document.settle.driverBataSalary.value = '<%= request.getAttribute("driBata")%>';
                var cleanerCount = '<%= request.getAttribute("clenaerCount")%>';
                if(cleanerCount=="null"){cleanerCount=0;}
                //alert(cleanerCount);
                if(document.settle.runningKM.value!="NaN"){
                    var tripNos = eval(document.settle.runningKM.value) / 150;
                    //alert("tripNos: "+tripNos);
                    var oneDay = 24*60*60*1000;
                    var d = '<%= request.getAttribute("fromDate")%>';
                    var temp1 = d.split("-");
                    var firstDate = new Date(temp1[2],temp1[1],temp1[0]);

                    var dd = '<%= request.getAttribute("toDate")%>';
                    var temp2 = dd.split("-");
                    var secondDate = new Date(temp2[2],temp2[1],temp2[0]);

                    var days = Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay));
                    if(days==0){
                       days=1;
                    }
                    //alert("days : "+days);
                    var bCrown = '<%= request.getAttribute("bulkerCrown")%>';
                    days = Math.ceil(days+1);
                    document.settle.totalDays.value = days;
                    tripNos = Math.ceil(tripNos);
                    document.settle.totalTrips.value = tripNos;
                    //alert("bCrown - "+bCrown);
                    if(bCrown == "1"){
                        if(tripNos >= days){
                            document.settle.totalTripAmount.value = (parseFloat(tripNos * 10000)).toFixed(2);
                            document.settle.fiveNoOfTrips.value = (parseFloat(tripNos * 5000)).toFixed(2);
                            if(tripNos>3){
                                document.settle.driverSalary.value = (parseInt(document.settle.driverSalary.value) + (parseInt(tripNos)*100)).toFixed(2);
                            }else{
                                document.settle.driverSalary.value = (parseInt(document.settle.driverSalary.value) + parseInt(0));
                            }
                            document.settle.driverBataSalary.value = eval(parseInt(document.settle.driverSalary.value) + parseInt(document.settle.driverBataSalary.value)).toFixed(2);
                            document.settle.cleanerSalary.value = (parseInt(cleanerCount)*250).toFixed(2);
                            document.settle.finalTrips.value = tripNos;
                        } else{
                            document.settle.totalTripAmount.value = parseFloat(days * 10000).toFixed(2);
                            document.settle.fiveNoOfTrips.value = parseFloat(days * 5000).toFixed(2);
                            if(days>30){
                                document.settle.driverSalary.value = (parseInt(document.settle.driverSalary.value) + (parseInt(days)*100)).toFixed(2);
                            }else{
                                document.settle.driverSalary.value = (parseInt(document.settle.driverSalary.value) + parseInt(0));
                            }
                            document.settle.driverBataSalary.value = eval(parseInt(document.settle.driverSalary.value) + parseInt(document.settle.driverBataSalary.value)).toFixed(2);
                            document.settle.cleanerSalary.value = (parseInt(days)*250).toFixed(2);
                            document.settle.finalTrips.value = days;
                        }
                    } else {
                        if(tripNos >= days){
                            document.settle.totalTripAmount.value = (parseFloat(tripNos * 15000)).toFixed(2);
                            document.settle.fiveNoOfTrips.value = (parseFloat(tripNos * 7500)).toFixed(2);
                            if(tripNos>30){
                                document.settle.driverSalary.value = (parseInt(document.settle.driverSalary.value) + (parseInt(tripNos)*100)).toFixed(2);
                            }else{
                                document.settle.driverSalary.value = (parseInt(document.settle.driverSalary.value) + parseInt(0));
                            }
                            document.settle.driverBataSalary.value = eval(parseInt(document.settle.driverSalary.value) + parseInt(document.settle.driverBataSalary.value)).toFixed(2);
                            document.settle.cleanerSalary.value = (parseInt(cleanerCount)*250).toFixed(2);
                            document.settle.finalTrips.value = tripNos;
                        } else{
                            document.settle.totalTripAmount.value = parseFloat(days * 15000).toFixed(2);
                            document.settle.fiveNoOfTrips.value = parseFloat(days * 7500).toFixed(2);
                            if(days>30){
                                document.settle.driverSalary.value = (parseInt(document.settle.driverSalary.value) + (parseInt(days)*100)).toFixed(2);
                            }else{
                                document.settle.driverSalary.value = (parseInt(document.settle.driverSalary.value) + parseInt(0));
                            }
                            document.settle.driverBataSalary.value = eval(parseInt(document.settle.driverSalary.value) + parseInt(document.settle.driverBataSalary.value)).toFixed(2);
                            document.settle.cleanerSalary.value = (parseInt(days)*250).toFixed(2);
                            document.settle.finalTrips.value = days;
                        }
                    }

                    //document.settle.totalExpenses.value = (parseInt(document.settle.dieselAmt.value) + parseInt(document.settle.driverExpense.value) + parseInt(document.settle.driverSalary.value) + parseInt(document.settle.cleanerSalary.value)).toFixed(2);
                    document.settle.shortage.value = parseInt(document.settle.totalTripAmount.value) - parseInt(document.settle.totalTonAmount.value);
                    document.settle.totalExpenses.value = (parseInt(document.settle.dieselAmt.value) + parseInt(document.settle.driverExpense.value) + parseInt(document.settle.driverSalary.value) + parseInt(document.settle.cleanerSalary.value)).toFixed(2);
                    document.settle.expenses.value = document.settle.totalExpenses.value;
                    document.settle.balance.value = (parseFloat(document.settle.income.value) - parseFloat(document.settle.expenses.value));
                    document.settle.shortage1.value = (parseInt(document.settle.fiveNoOfTrips.value) - parseInt(document.settle.balance.value)).toFixed(2);
                    document.settle.perDayIncome.value = (parseInt(document.settle.balance.value) / parseInt(document.settle.finalTrips.value)).toFixed(2);
                    document.settle.lossDay.value = (parseInt(document.settle.shortage1.value) / parseInt(document.settle.finalTrips.value)).toFixed(2);
                    //document.settle.shortage.value = (parseInt(document.settle.totalTripAmount.value) - parseInt(document.settle.totalTonAmount.value)).toFixed(2);

                    document.settle.driverExpenses.value = parseInt(document.settle.driverExpense.value).toFixed(2);
                    document.settle.kmpl.value = (eval(document.settle.runningKM.value) / eval(document.settle.totalDiesel.value)).toFixed(2);
                    document.settle.cleanerBataSalary.value = document.settle.cleanerSalary.value;

                    document.settle.totalSalary.value = (parseInt(document.settle.cleanerBataSalary.value) + parseInt(document.settle.driverBataSalary.value)).toFixed(2);
                    document.settle.dueFromDriver.value = (parseInt(document.settle.driverAdvance.value) - parseInt(document.settle.driverExpenses.value)).toFixed(2);
                    document.settle.dDueFomDriver.value = document.settle.dueFromDriver.value;
                    document.settle.busExpenses.value = 250;
                    document.settle.actualSalary.value = (parseInt(document.settle.totalSalary.value) - parseInt(document.settle.dDueFomDriver.value)).toFixed(2);
                    document.settle.totalAmount.value = (parseInt(document.settle.actualSalary.value) + parseInt(document.settle.busExpenses.value)).toFixed(2);
                    document.settle.cFRs.value = document.settle.totalAmount.value;
                }
            }
            function calc(obj){
                if(obj.value!=""){
                    document.settle.cleanerBataSalary.value = obj.value;
                    document.settle.shortage.value = parseInt(document.settle.totalTripAmount.value) - parseInt(document.settle.totalTonAmount.value);
                    document.settle.totalExpenses.value = (parseInt(document.settle.dieselAmt.value) + parseInt(document.settle.driverExpense.value) + parseInt(document.settle.driverSalary.value) + parseInt(obj.value)).toFixed(2);
                    document.settle.expenses.value = document.settle.totalExpenses.value;
                    document.settle.balance.value = (parseFloat(document.settle.income.value) - parseFloat(document.settle.expenses.value));
                    document.settle.shortage1.value = (parseInt(document.settle.fiveNoOfTrips.value) - parseInt(document.settle.balance.value)).toFixed(2);
                    document.settle.perDayIncome.value = (parseInt(document.settle.balance.value) / parseInt(document.settle.finalTrips.value)).toFixed(2);
                    document.settle.lossDay.value = (parseInt(document.settle.shortage1.value) / parseInt(document.settle.finalTrips.value)).toFixed(2);
                    //document.settle.shortage.value = (parseInt(document.settle.totalTripAmount.value) - parseInt(document.settle.totalTonAmount.value)).toFixed(2);

                    document.settle.driverExpenses.value = parseInt(document.settle.driverExpense.value).toFixed(2);
                    document.settle.kmpl.value = (eval(document.settle.runningKM.value) / eval(document.settle.totalDiesel.value)).toFixed(2);
                    document.settle.cleanerBataSalary.value = document.settle.cleanerSalary.value;

                    document.settle.totalSalary.value = (parseInt(document.settle.cleanerBataSalary.value) + parseInt(document.settle.driverBataSalary.value)).toFixed(2);
                    document.settle.dueFromDriver.value = (parseInt(document.settle.driverAdvance.value) - parseInt(document.settle.driverExpenses.value)).toFixed(2);
                    document.settle.dDueFomDriver.value = document.settle.dueFromDriver.value;
                    document.settle.busExpenses.value = 250;
                    document.settle.actualSalary.value = (parseInt(document.settle.totalSalary.value) - parseInt(document.settle.dDueFomDriver.value)).toFixed(2);
                    document.settle.totalAmount.value = (parseInt(document.settle.actualSalary.value) + parseInt(document.settle.busExpenses.value)).toFixed(2);
                    document.settle.cFRs.value = document.settle.totalAmount.value;
                }
            }
            function balCalc(obj){
                document.settle.cBalance.value = parseInt(document.settle.cFRs.value) - parseInt(obj.value);
            }
            function balCalc1(obj){
                document.settle.dBalance.value = parseInt(document.settle.cBalance.value) - parseInt(obj.value);
            }
            function submitPage(obj){
                if(obj.name=="search"){
                    var fromDate=document.settle.fromDate.value;
                    var toDate=document.settle.toDate.value;
                    //var regno=document.settle.regno.value;
                    var driName=document.settle.driName.value;
                    if (driName=="") {
                        alert("please enter the Driver Name");
                        document.settle.driName.focus();
                    }else if (fromDate=="") {
                        alert("please enter the From Date");
                        document.settle.fromDate.focus();
                    }else if (toDate=="") {
                        alert("please enter the To Date");
                        document.settle.toDate.focus();
                    }else{
                        document.settle.action="/throttle/driverSettlementReport.do";
                        document.settle.submit();
                    }
                }                
            }            

            function getDriverName(){
                var oTextbox = new AutoSuggestControl(document.getElementById("driName"),new ListSuggestions("driName","/throttle/handleDriverSettlement.do?"));

            }
        </script>
        <script language="">
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
    </script>
    </head>

    <body onload="setDefaultValue();">
        <!--<form name="settle" method="post" action="saveTripSheet.do" method="post" onsubmit="return validateSubmit();">-->
        <form name="settle" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>

            <table width="75%" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr>
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <b>Driver Settlement Report</b>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4">
                                    <tr>
                                        <td><font color="red">*</font>Driver Name</td>
                                        <td height="30">
                                            <input name="driName" id="driName" type="text" class="textbox" size="20" value="" onKeyPress="getDriverName();" autocomplete="off">
                                        </td>
                                        <td><!--Vehicle No, Settlement Date--></td>
                                        <td><!--<input name="regno" id="regno" type="text" class="textbox" size="20" value="">
                                            <input type="text" name="settleDate" class="textbox" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.settle.settleDate,'dd-mm-yyyy',this)"/>-->
                                        </td>
                                        <td><!--Bulker Type--></td>
                                        <td>                                            
                                            <!--<select name="bulkerCrown" class="textbox"  id="bulkerCrown">
                                                <option value="1">Double Axle</option>
                                                <option value="2">Triple Axle</option>
                                            </select>-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="30"><font color="red">*</font>From Date</td>
                                        <td height="30"><input type="text" name="fromDate" class="textbox" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.settle.fromDate,'dd-mm-yyyy',this)"/></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td><input type="text" name="toDate" class="textbox" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.settle.toDate,'dd-mm-yyyy',this)"/></td>
                                        <td>&nbsp;</td>
                                        <td><input type="button" class="button" name="search" onclick="submitPage(this);" value="Search"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br/>
            <div id="tabs" style="display: none;">
                <ul>
                    <li><a href="#tripDetail"><span>Trip Details</span></a></li>
                    <li><a href="#expenseDetail"><span>Expenses Entry</span></a></li>
                    <li><a href="#advDetail"><span>Advance Details</span></a></li>
                    <li><a href="#fuelDetail"><span>Fuel Details</span></a></li>
                    <li><a href="#haltDetail"><span>Vehicle In/Out Details</span></a></li>
                    <li><a href="#Remarks"><span>Remarks</span></a></li>
                    <li><a href="#Cleaner"><span>Cleaner Status</span></a></li>
                    <li><a href="#summary"><span>summary</span></a></li>
                </ul>
                <div id="tripDetail">
                        <c:if test = "${searchProDriverSettlement != null}" >
                            <%
                                        int index = 0;
                                        ArrayList searchProDriverSettlement = (ArrayList) request.getAttribute("searchProDriverSettlement");
                                        if (searchProDriverSettlement.size() != 0) {
                            %>
                            <div id="tripPrint">
                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                <!--<tr>
                                    <td height="30" class="contenthead" colspan="11">Proceed to Driver Settlement</td>
                                </tr>-->
                                <tr>
                                    <td class="contentsub" height="30">S.No</td>
                                  <!--  <td class="contentsub" height="30">Trip Id</td>
                                    <td class="contentsub" height="30">Vehicle No</td>
                                    <td class="contentsub" height="30">Card No</td>-->
                                    <td class="contentsub" height="30">Route Name</td>
                                    <td class="contentsub" height="30">OUT KM</td>
                                    <!--<td class="contentsub" height="30">Driver Name</td>-->
                                    <td class="contentsub" height="30">OUT DateTime</td>
                                    <td class="contentsub" height="30">IN KM</td>
                                    <td class="contentsub" height="30">IN DateTime</td>
                                    <td class="contentsub" height="30">Total Tonnage</td>
                                    <td class="contentsub" height="30">Delivered Tonnage</td>
                                    <td class="contentsub" height="30">Shortage</td>
                                    <td class="contentsub" height="30">Total Tonnage Amount</td>
                                    <!--<td class="contentsub" height="30">Trip Status</td>  -->
                                </tr>
                                <c:set var="tripIds" value="${''}" />
                                <c:set var="tripIdComa" value="${','}" />
                                <c:forEach items="${searchProDriverSettlement}" var="pdriversettle">
                                    <c:set var="total" value="${total+1}"></c:set>
                                    <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>
                                    <tr>
                                        <td class="<%=classText%>"  height="30"><%=index + 1%></td>
                                     <!--   <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.tripId}"/></td>
                                        <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.regno}"/>  </td> -->
                                        <c:set var="tripIds" value="${tripIds}${pdriversettle.tripId}${tripIdComa}" />
                                        
                                        <!--<td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.identityNo}"/></td> -->
                                        <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.routeName}"/></td>
                                        <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.outKM}"/></td>
                                        <!--<td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.driverName}"/></td>-->
                                        <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.outDateTime}"/></td>
                                        <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.inKM}"/></td>
                                        <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.inDateTime}"/></td>
                                        <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.totalTonnage}"/></td>
                                        <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.deliveredTonnage}"/></td>
                                        <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.shortage}"/></td>
                                        <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.totalTonAmount}"/></td>
                                       <!-- <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.status}"/></td>  -->
                                    </tr>
                                    <%index++;%>
                                </c:forEach>
                            </table>
                            </div>
                            <%
                                        }
                            %>
                        </c:if>
                        <input name="tripIds" type="hidden" value="<c:out value="${tripIds}"/>" />
                        <input name="check" type="hidden" value='0'>
                    <br/>
                    <br/>
                    <center>
                        <input type="button" class="button" name="tripPrint" value="Print" onClick="print('tripPrint');" >
                    </center>
                </div>
                <div id="expenseDetail" align="center">
                        <div id="exp">
                    <c:if test = "${fixedExpDetails != null}" >
                        <%
                                    int index5 = 0;
                                    ArrayList fixedExpDetails = (ArrayList) request.getAttribute("fixedExpDetails");
                                    if (fixedExpDetails.size() != 0) {
                        %>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                                <td height="30" class="contenthead" colspan="11" align="center"><b>Fixed Expenses</b></td>
                            </tr>
                            <tr>
                                <td class="contentsub"  height="30">S.No</td>
                                <td class="contentsub"  height="30">Trip Id</td>
                                <td class="contentsub"  height="30">Vehicle No</td>
                                <td class="contentsub" height="30">Trip Date</td>
                                <td class="contentsub" height="30">Amount</td>
                                <td class="contentsub" height="30">Remarks</td>
                                <td class="contentsub" height="30">Created Date</td>
                            </tr>
                            <c:forEach items="${fixedExpDetails}" var="fixedExp">
                                <c:set var="total5" value="${total5+1}"></c:set>
                                <%
                                                                    String classText = "";
                                                                    int oddEven = index5 % 2;
                                                                    if (oddEven > 0) {
                                                                        classText = "text2";
                                                                    } else {
                                                                        classText = "text1";
                                                                    }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><%=index5 + 1%></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fixedExp.tripId}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fixedExp.regno}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fixedExp.tripDate}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fixedExp.amount}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fixedExp.remarks}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fixedExp.createdDate}"/></td>
                                </tr>
                                <%index5++;%>
                            </c:forEach>
                        </table>
                            
                        <%
                                    }
                        %>

                    </c:if>
                    <br/>
                    <c:if test = "${driverExpDetails != null}" >
                        <%
                                    int index7 = 0;
                                    ArrayList driverExpDetails = (ArrayList) request.getAttribute("driverExpDetails");
                                    if (driverExpDetails.size() != 0) {
                        %>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                                <td height="30" class="contenthead" colspan="11" align="center"><b>Driver Expenses</b></td>
                            </tr>
                            <tr>
                                <td class="contentsub"  height="30">S.No</td>
                                <td class="contentsub"  height="30">Trip Id</td>
                                <td class="contentsub" height="30">Trip Date</td>
                                <td class="contentsub" height="30">Description</td>
                                <td class="contentsub" height="30">Amount</td>
                                <td class="contentsub" height="30">Remarks</td>
                            </tr>
                            <c:forEach items="${driverExpDetails}" var="driverExp">
                                <c:set var="total7" value="${total7+1}"></c:set>
                                <%
                                                                    String classText = "";
                                                                    int oddEven = index7 % 2;
                                                                    if (oddEven > 0) {
                                                                        classText = "text2";
                                                                    } else {
                                                                        classText = "text1";
                                                                    }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><%=index7 + 1%></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${driverExp.tripId}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${driverExp.tripDate}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${driverExp.expensesDesc}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${driverExp.amount}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${driverExp.remarks}"/></td>
                                </tr>
                                <%index7++;%>
                            </c:forEach>
                        </table>
                        <%
                                    }
                        %>
                    </c:if>
                    </div>
                    <br/>                    
                    <br/>
                    <center>
                        <input type="button" class="button" name="tripExp" value="Print" onClick="print('exp');" >
                    </center>
                </div>
                <div id="advDetail">
                    <c:if test = "${AdvDetails != null}" >
                        <%
                                    int index1 = 0;
                                    ArrayList AdvDetails = (ArrayList) request.getAttribute("AdvDetails");
                                    if (AdvDetails.size() != 0) {
                        %>
                        <div id="adv">
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <!--<tr>
                                <td height="30" class="contenthead" colspan="11">Proceed to Driver Settlement</td>
                            </tr>-->
                            <tr>
                                <td class="contentsub"  height="30">S.No</td>
                              <!--  <td class="contentsub"  height="30">Trip Id</td>
                                <td class="contentsub" height="30">Card No</td> -->
                                <td class="contentsub" height="30">Vehicle No</td>
                                <td class="contentsub" height="30">Issuer Name</td>
                                <!--<td class="contentsub" height="30">Designation</td>
                                <td class="contentsub" height="30">Driver Name</td>-->
                                <td class="contentsub" height="30">Date</td>
                                <td class="contentsub" height="30">Route Name</td>
                                <td class="contentsub" height="30">Advance Amount</td>
                                
                                <!--<td class="contentsub" height="30">Settlement Status</td>-->
                             <!--   <td class="contentsub" height="30">Trip Status</td> -->
                            </tr>
                            <%
                                String advanceAmount = "0";
                                double advanceTotal = 0;
                            %>

                            <c:forEach items="${AdvDetails}" var="advDetail">
                                <c:set var="total1" value="${total1+1}"></c:set>
                                <%
                                                                    String classText = "";
                                                                    int oddEven = index1 % 2;
                                                                    if (oddEven > 0) {
                                                                        classText = "text2";
                                                                    } else {
                                                                        classText = "text1";
                                                                    }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><%=index1 + 1%></td>
                                   <!-- <td class="<%=classText%>"  height="30"><c:out value="${advDetail.tripId}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${advDetail.identityNo}"/></td>-->
                                    <td class="<%=classText%>"  height="30"><c:out value="${advDetail.regno}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${advDetail.issuerName}"/></td>
                                    <!--<td class="<%=classText%>"  height="30"><c:out value="${advDetail.designation}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${advDetail.driverName}"/></td>-->
                                    <td class="<%=classText%>"  height="30"><c:out value="${advDetail.advDatetime}"/></td>
                                    
                                    <c:set var="advanceAmount" value="${advDetail.amount}" />
                                    <td class="<%=classText%>"  height="30"><c:out value="${advDetail.routeName}"/></td>
                                    <td class="<%=classText%>"  align="right"  height="30"><c:out value="${advDetail.amount}"/></td>
                                    <!--<td class="<%=classText%>"  height="30"><c:out value="${advDetail.settlementFlag}"/></td>-->
                                  <!--  <td class="<%=classText%>"  height="30"><c:out value="${advDetail.status}"/></td>  -->

                                     <%
                                        advanceAmount = (String) pageContext.getAttribute("advanceAmount");
                                        advanceTotal +=  Double.parseDouble(advanceAmount);
                                    %>
                                </tr>
                                <%index1++;%>
                            </c:forEach>
                               <tr>
                                   <td class="text2" align="right" style="font-weight:bold;" colspan="5">Total:&nbsp;&nbsp;</td>
                                    <td class="text2" align="right"  style="font-weight:bold;"><%=advanceTotal%></td>
                                    
                                </tr>
                        </table>
                        </div>
                        <%
                                    }
                        %>
                    </c:if>
                    <br/>
                    <br/>
                    <center>
                        <input type="button" class="button" name="tripAdv" value="Print" onClick="print('adv');" >
                    </center>
                </div>
                <div id="fuelDetail">
                    <c:if test = "${fuelDetails != null}" >
                        <%
                                    int index2 = 0;
                                    ArrayList fuelDetails = (ArrayList) request.getAttribute("fuelDetails");
                                    if (fuelDetails.size() != 0) {
                        %>
                        <div id="fuel">
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <!--<tr>
                                <td height="30" class="contenthead" colspan="11">Proceed to Driver Settlement</td>
                            </tr>-->
                             

                            <tr>
                                <td class="contentsub"  height="30">S.No</td>
                               <!-- <td class="contentsub"  height="30">Trip Id</td>-->
                                <td class="contentsub" height="30">Vehicle No</td>
                                <!--<td class="contentsub" height="30">Card No</td>  -->
                                <td class="contentsub" height="30">Fuel Location</td>
<!--                                <td class="contentsub" height="30">Bunk Name</td>-->
                                <!--<td class="contentsub" height="30">Driver Name</td>-->
                                <td class="contentsub" height="30">Date</td>
                                <td class="contentsub" height="30">Route Name</td>
                                <td class="contentsub" height="30">Liters</td>
                                <!--<td class="contentsub" height="30">Settlement Status</td>
                                <td class="contentsub" height="30">Trip Status</td>-->
                            </tr>
                            <%
                            String fuelLiters = "0";
                            double fuelTotal = 0;
                            %>

                            <c:forEach items="${fuelDetails}" var="fuelDetail">
                                <c:set var="total2" value="${total2+1}"></c:set>
                                <%
                                                                    String classText = "";
                                                                    int oddEven = index2 % 2;
                                                                    if (oddEven > 0) {
                                                                        classText = "text2";
                                                                    } else {
                                                                        classText = "text1";
                                                                    }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><%=index2 + 1%></td>
                                  <!--  <td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.tripId}"/></td>  -->
                                    <td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.regno}"/></td>
                                    <!--<td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.identityNo}"/></td>   -->
                                    <td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.fuelName}"/></td>
<!--                                    <td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.bunkName}"/></td>-->
                                    <!--<td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.driverName}"/></td>-->
                                    <td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.fuelDatetime}"/></td>
                                    <c:set var="fuelLiters" value="${fuelDetail.liters}" />
                                    <td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.routeName}"/></td>

                                    <td class="<%=classText%>"  align="right"  height="30"><c:out value="${fuelDetail.liters}"/></td>
                                    <%
                                        fuelLiters = (String) pageContext.getAttribute("fuelLiters");
                                        fuelTotal +=  Double.parseDouble(fuelLiters);
                                    %>
                                </tr>
                                <%index2++;%>
                            </c:forEach>
                                <tr>
                                    <td class="text2" align="right" style="font-weight:bold;" colspan="5">Total:&nbsp;&nbsp;</td>
                                    <td class="text2"  align="right" style="font-weight:bold;"><%=fuelTotal%></td>
                                    
                                </tr>
                        </table>
                            </div>
                        <%
                                    }
                        %>

                    </c:if>
                    <br/>
                    <br/>
                    <center>
                            <input type="button" class="button" name="tripFuel" value="Print" onClick="print('fuel');" >
                    </center>
                </div>
                <div id="haltDetail">
                    <c:if test = "${fuelDetails != null}" >
                        <%
                                    int index3 = 0;
                                    ArrayList haltDetails = (ArrayList) request.getAttribute("haltDetails");
                                    if (haltDetails.size() != 0) {
                        %>
                        <div id="halt">
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <!--<tr>
                                <td height="30" class="contenthead" colspan="11">Proceed to Driver Settlement</td>
                            </tr>-->
                            <tr>
                                <td class="contentsub" height="30">S.No</td>
                                <td class="contentsub" height="30">Trip Id</td>
                                <td class="contentsub" height="30">Vehicle No</td>
                                <td class="contentsub" height="30">Card No</td>
                                <td class="contentsub" height="30">In Out Indication</td>
                                <td class="contentsub" height="30">In Out Date Time</td>
                                <td class="contentsub" height="30">Location Name</td>
                                <!--<td class="contentsub" height="30">Driver Name</td>-->
                                <td class="contentsub" height="30">Route Name</td>
                                <!--<td class="contentsub" height="30">Settlement Status</td>
                                <td class="contentsub" height="30">Trip Status</td>-->
                            </tr>
                            <c:forEach items="${haltDetails}" var="haltDetail">
                                <c:set var="total3" value="${total3+1}"></c:set>
                                <%
                                                                    String classText = "";
                                                                    int oddEven = index3 % 2;
                                                                    if (oddEven > 0) {
                                                                        classText = "text2";
                                                                    } else {
                                                                        classText = "text1";
                                                                    }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><%=index3 + 1%></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${haltDetail.tripId}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${haltDetail.regno}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${haltDetail.identityNo}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${haltDetail.inOutIndication}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${haltDetail.inOutDateTime}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${haltDetail.locationName}"/></td>
                                    <!--<td class="<%=classText%>"  height="30"><c:out value="${haltDetail.driverName}"/></td>-->
                                    <td class="<%=classText%>"  height="30"><c:out value="${haltDetail.routeName}"/></td>
                                </tr>
                                <%index3++;%>
                            </c:forEach>
                        </table>
                            </div>
                        <%
                                    }
                        %>

                    </c:if>
                    <br/>
                    <br/>
                    <center>
                            <input type="button" class="button" name="tripHalt" value="Print" onClick="print('halt');" >
                    </center>
                </div>
                <div id="Remarks">
                    <c:choose>
                      <c:when test = "${remarkDetails != null}">
                            <%
                                    int index6 = 0;
                                    ArrayList remarkDetails = (ArrayList) request.getAttribute("remarkDetails");
                                    if (remarkDetails.size() != 0) {
                        %>
                        <div id="remark">
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <!--<tr>
                                <td height="30" class="contenthead" colspan="11">Proceed to Driver Settlement</td>
                            </tr>-->
                            <tr>
                                <td class="contentsub"  height="30">S.No</td>
                                <td class="contentsub"  height="30">Driver Name</td>
                                <td class="contentsub" height="30">Vehicle No</td>
                                <td class="contentsub" height="30">Embarkment Date</td>
                                <td class="contentsub" height="30">Alighting Date</td>
                                <td class="contentsub" height="30">Alighting Status</td>
                                <td class="contentsub" height="30">Remarks</td>
                                <td class="contentsub" height="30">Remark Date</td>
                                <!--<td class="contentsub" height="30">Settlement Status</td>
                                <td class="contentsub" height="30">Trip Status</td>-->
                            </tr>
                            <c:forEach items="${remarkDetails}" var="remarkDetail">
                                <c:set var="total6" value="${total6+1}"></c:set>
                                <%
                                                                    String classText = "";
                                                                    int oddEven = index6 % 2;
                                                                    if (oddEven > 0) {
                                                                        classText = "text2";
                                                                    } else {
                                                                        classText = "text1";
                                                                    }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><%=index6 + 1%></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${remarkDetail.empName}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${remarkDetail.vehicleNo}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${remarkDetail.embarkDate}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${remarkDetail.alightDate}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${remarkDetail.alightStatus}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${remarkDetail.remarks}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${remarkDetail.createdOn}"/></td>
                                </tr>
                                <%index6++;%>
                            </c:forEach>
                        </table>
                            </div>
                        <%
                                    }
                        %>
                      </c:when>
                      <c:when test = "${remarkDetails == null}">
                          <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0">
                              <tr>
                                  <td>
                                        No Remarks Found...
                                  </td>
                              </tr>
                          </table>
                      </c:when>
                    </c:choose>
                    <br/>
                    <br/>
                    <center>
                            <input type="button" class="button" name="tripRemark" value="Print" onClick="print('remark');" >
                    </center>
                </div>
                <div id="Cleaner">
                    <c:if test = "${cleanerTrip != null}" >
                        <%
                                    int index8 = 0;
                                    ArrayList cleanerTrip = (ArrayList) request.getAttribute("cleanerTrip");
                                    if (cleanerTrip.size() != 0) {
                        %>
                        <div id="cln">
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                                <td class="contentsub" height="30">S.No</td>
                                <td class="contentsub" height="30">Trip Id</td>
                                <td class="contentsub" height="30">Vehicle No</td>
                                <td class="contentsub" height="30">Route Name</td>
                                <td class="contentsub" height="30">OUT KM</td>
                                <td class="contentsub" height="30">IN KM</td>
                                <td class="contentsub" height="30">OUT DateTime</td>
                                <td class="contentsub" height="30">IN DateTime</td>
                                <td class="contentsub" height="30">Trip Status</td>
                            </tr>
                            <c:forEach items="${cleanerTrip}" var="cln">
                                <c:set var="total" value="${total+1}"></c:set>
                                <%
                                    String classText = "";
                                    int oddEven = index8 % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><%=index8 + 1%></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${cln.tripId}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${cln.regno}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${cln.routeName}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${cln.outKM}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${cln.inKM}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${cln.outDateTime}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${cln.inDateTime}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${cln.status}"/></td>
                                </tr>
                                <%index8++;%>
                            </c:forEach>
                        </table>
                            </div>
                        <%
                                    }
                        %>
                    </c:if>
                    <br/>
                    <br/>
                    <center>
                            <input type="button" class="button" name="tripCleaner" value="Print" onClick="print('cln');" >
                    </center>
                </div>
                <div id="summary" align="center">
                    <c:if test = "${summaryDetails != null}" >
                            <%                                        
                                        ArrayList summaryDetails = (ArrayList) request.getAttribute("summaryDetails");
                                        if (summaryDetails.size() != 0) {
                            %>
                            <c:forEach items="${summaryDetails}" var="smry">
                    <div id="print" >
                    <table cellpadding="0" cellspacing="4" border="0" width="80%" class="border">
                        <tr style="width:50px;">
                            <th colspan="8" class="contenthead">Driver Details</th>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Driver Name
                            </td>
                            <td>
                                <input type="hidden" id="suDriverName" name="suDriverName" value="" />
                                <c:out value="${smry.driName}"/>
                            </td>
                            <td class="contenletter">&nbsp;
                                <input type="hidden" id="suTotalTon" name="suTotalTon" value=""  />
                                
                            </td>
                            <td>&nbsp;

                            </td>

                            <td class="contenletter">
                                Cleaner Name
                            </td>
                            <td>
                                <input type="hidden" id="cleanerName" name="cleanerName" value=""  />
                                <c:out value="${smry.cleanerName}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Vehicle No
                            </td>
                            <td>
                                <input type="hidden" id="suVehicleNo" name="suVehicleNo" value=""  />
                                <c:out value="${smry.vehicleNo}"/>
                            </td>
                            <td class="contenletter">
                                From Date
                            </td>
                            <td>
                                <input type="hidden" id="suFromDate" name="suFromDate" value=""  readonly="true" />
                                <c:out value="${smry.fromDate}"/>
                            </td>
                            <td class="contenletter">
                                To Date
                            </td>
                            <td>
                                <input type="hidden" id="suToDate" name="suToDate" value=""  readonly="true" />
                                <c:out value="${smry.toDate}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Total Days
                            </td>
                            <td>
                                <input type="hidden" id="totalDays" name="totalDays" value=""  readonly="true" />
                                <c:out value="${smry.totalDays}"/>
                            </td>
                            <td class="contenletter">
                                Total Trips
                            </td>
                            <td>
                                <input type="hidden" id="totalTrips" name="totalTrips" value=""  readonly="true" />
                                <c:out value="${smry.totalDays}"/>
                            </td>
                            <td class="contenletter">
                                End KM
                            </td>
                            <td>
                                <input type="hidden" id="endKM" name="endKM" value=""  readonly="true" />
                                <c:out value="${smry.endKm}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                10000 * No.of Trips
                            </td>
                            <td>
                                <input type="hidden" id="totalTripAmount" name="totalTripAmount" value=""  readonly="true" />
                                <c:out value="${smry.noOfTrips}"/>
                                <input name="finalTrips" type="hidden" value="" />
                            </td>
                            <td class="contenletter">&nbsp;

                            </td>
                            <td>&nbsp;

                            </td>
                            <td class="contenletter">
                                Start KM
                            </td>
                            <td>
                                <input type="hidden" id="startKM" name="startKM" value=""  readonly="true" />
                                <c:out value="${smry.startKM}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Total Tonnage Amount
                            </td>
                            <td>
                                <input type="hidden" id="totalTonAmount" name="totalTonAmount" value=""  readonly="true" />
                                <c:out value="${smry.totalTonnageAmount}"/>
                            </td>
                            <td class="contenletter">&nbsp;

                            </td>
                            <td>&nbsp;

                            </td>
                            <td class="contenletter">
                                Running KM
                            </td>
                            <td>
                                <input type="hidden" id="runningKM" name="runningKM" value=""  readonly="true" />
                                <c:out value="${smry.runningKm}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Shortage
                            </td>
                            <td>
                                <input type="hidden" id="shortage" name="shortage" value=""  />
                                <c:out value="${smry.shortage}"/>
                            </td>
                            <td class="contenletter">&nbsp;

                            </td>
                            <td>&nbsp;

                            </td>
                            <td class="contenletter">
                                Total Diesel (Lit)
                            </td>
                            <td>
                                <input type="hidden" id="totalDiesel" name="totalDiesel" value=""  />
                                <c:out value="${smry.totalDiesel}"/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table cellpadding="0" cellspacing="4" border="0" width="70%" class="border">
                        <tr>
                            <td class="contenletter">
                                Actual KM
                            </td>
                            <td>
                                <input type="hidden" id="actualKM" name="actualKM" value=""  />
                                <c:out value="${smry.actualKM}"/>
                            </td>
                            <td class="contenletter">
                                Kmpl
                            </td>
                            <td>
                                <input type="hidden" id="kmpl" name="kmpl" value=""  />
                                <c:out value="${smry.kmpl}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Speed 'O' Meter Km
                            </td>
                            <td>
                                <input type="hidden" id="speedOMeterKM" name="speedOMeterKM" value=""  />
                                <c:out value="${smry.speedOMeterKm}"/>
                            </td>
                            <td class="contenletter">
                                sKmpl
                            </td>
                            <td>
                                <input type="hidden" id="sKmpl" name="sKmpl" value=""  />
                                <c:out value="${smry.sKmpl}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Above / Below KM
                            </td>
                            <td>
                                <input type="hidden" id="aboveBelowKM" name="aboveBelowKM" value=""  />
                                <c:out value="${smry.aboveBelowKM}"/>
                            </td>
                            <td class="contenletter">&nbsp;

                            </td>
                            <td>&nbsp;

                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table cellpadding="0" cellspacing="4" border="0" width="80%" class="border">
                        <tr>
                            <td class="contenletter">
                                Diesel Amount
                            </td>
                            <td>
                                <input type="hidden" id="dieselAmt" name="dieselAmt" value=""  />
                                <c:out value="${smry.dieselAmount}"/>
                            </td>
                            <td class="contenletter">
                                Income
                            </td>
                            <td>
                                <input type="hidden" id="income" name="income" value=""  />
                                <c:out value="${smry.income}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Driver Expenses
                            </td>
                            <td>
                                <input type="hidden" id="driverExpense" name="driverExpense" value=""  readonly="true" />
                                <c:out value="${smry.driverExpenses}"/>
                            </td>
                            <td class="contenletter">
                                Expenses
                            </td>
                            <td>
                                <input type="hidden" id="expenses" name="expenses" value=""  />
                                <c:out value="${smry.generalExpenses}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Driver Salary
                            </td>
                            <td>
                                <input type="hidden" id="driverSalary" name="driverSalary" value=""  />
                                <c:out value="${smry.driverSalary}"/>
                            </td>
                            <td class="contenletter">
                                Balance
                            </td>
                            <td>
                                <input type="hidden" id="balance" name="balance" value=""  />
                                <c:out value="${smry.balance}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Cleaner Salary
                            </td>
                            <td>
                                <input type="hidden" id="cleanerSalary" name="cleanerSalary" value="" onkeypress="calc(this);"  />
                                <c:out value="${smry.cleanerSalary}"/>
                            </td>
                            <td class="contenletter">&nbsp;

                            </td>
                            <td>&nbsp;

                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Total Expenses
                            </td>
                            <td>
                                <input type="hidden" id="totalExpenses" name="totalExpenses" value=""  />
                                <c:out value="${smry.totalExpenses}"/>
                            </td>
                            <td class="contenletter">
                                5000* No.of Trips
                            </td>
                            <td>
                                <input type="hidden" id="fiveNoOfTrips" name="fiveNoOfTrips" value=""  />
                                <c:out value="${smry.noOfTrips5k}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Driver Advance
                            </td>
                            <td>
                                <input type="hidden" id="driverAdvance" name="driverAdvance" value=""  />
                                <c:out value="${smry.driverAdvance}"/>
                            </td>
                            <td class="contenletter">
                                Shortage
                            </td>
                            <td>
                                <input type="hidden" id="shortage1" name="shortage1" value=""  />
                                <c:out value="${smry.shortage1}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Driver + Cleaner Expenses
                            </td>
                            <td>
                                <input type="hidden" id="driverExpenses" name="driverExpenses" value=""  />
                                <c:out value="${smry.driverCleanerExpenses}"/>
                            </td>
                            <td class="contenletter">
                                Per Days Income
                            </td>
                            <td>
                                <input type="hidden" id="perDayIncome" name="perDayIncome" value=""  />
                                <c:out value="${smry.perDaysIncome}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Due from Driver
                            </td>
                            <td>
                                <input type="hidden" id="dueFromDriver" name="dueFromDriver" value=""  />
                                <c:out value="${smry.duefromDriver}"/>
                            </td>
                            <td class="contenletter">
                                Loss / Day
                            </td>
                            <td>
                                <input type="hidden" id="lossDay" name="lossDay" value=""  />
                                <c:out value="${smry.lossOrDay}"/>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table cellpadding="0" cellspacing="4" border="0" width="80%" class="border">
                        <tr>
                            <td width="20%" class="contenletter">
                                Driver Bata + Salary
                            </td>
                            <td width="20%">
                                <input type="hidden" id="driverBataSalary" name="driverBataSalary" value=""  />
                                <c:out value="${smry.driverBataSalary}"/>
                            </td>
                            <td width="7%">&nbsp;</td>
                            <td width="16%">&nbsp;</td>
                            <td width="7%">&nbsp;</td>
                            <td width="16%">&nbsp;</td>
                            <td width="10%" class="contenletter">
                                C/F Rs
                            </td>
                            <td width="14%">
                                <input type="hidden" id="cFRs" name="cFRs" value=""  />
                                <c:out value="${smry.cFRs}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Cleaner Bata + Salary
                            </td>
                            <td>
                                <input type="hidden" id="cleanerBataSalary" name="cleanerBataSalary" value=""  />
                                <c:out value="${smry.cleanerBataSalary}"/>
                            </td>
                            <td class="contenletter">
                                Date
                            </td>
                            <td>
                                <input type="hidden" id="cDate" name="cDate" value="" class="datepicker"  />
                                <c:out value="${smry.cDate}"/>
                            </td>
                            <td class="contenletter">
                                V.No
                            </td>
                            <td>
                                <input type="hidden" id="cVNo" name="cVNo" value=""  />
                                <c:out value="${smry.cVNo}"/>
                            </td>
                            <td class="contenletter">
                                Rs
                            </td>
                            <td>
                                <input type="hidden" id="cRs" name="cRs" value="" onblur="balCalc(this);"  />
                                <c:out value="${smry.cRs}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Total Salary
                            </td>
                            <td>
                                <input type="hidden" id="totalSalary" name="totalSalary" value=""  />
                                <c:out value="${smry.totalSalary}"/>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="contenletter">
                                Balance
                            </td>
                            <td >
                                <input type="hidden" id="cBalance" name="cBalance" value=""  />
                                <c:out value="${smry.cBalance}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Due From Driver
                            </td>
                            <td>
                                <input type="hidden" id="dDueFomDriver" name="dDueFomDriver" value=""  />
                                <c:out value="${smry.dDueFromDriver}"/>
                            </td>
                            <td class="contenletter">
                                Date
                            </td>
                            <td>
                                <input type="hidden" id="dDate" name="dDate" value="" class="datepicker"  />
                                <c:out value="${smry.dDate}"/>
                            </td>
                            <td class="contenletter">
                                V.No
                            </td>
                            <td>
                                <input type="hidden" id="dVNo" name="dVNo" value=""  />
                                <c:out value="${smry.dVNo}"/>
                            </td>
                            <td class="contenletter">
                                Rs
                            </td>
                            <td>
                                <input type="hidden" id="dRs" name="dRs" value="" onblur="balCalc1(this);" />
                                <c:out value="${smry.dRs}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Actual Salary
                            </td>
                            <td>
                                <input type="hidden" id="actualSalary" name="actualSalary" value=""  />
                                <c:out value="${smry.actualSalary}"/>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="contenletter">
                                Balance
                            </td>
                            <td>
                                <input type="hidden" id="dBalance" name="dBalance" value=""  />
                                <c:out value="${smry.dBalance}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Bus Expenses
                            </td>
                            <td>
                                <input type="hidden" id="busExpenses" name="busExpenses" value=""  />
                                <c:out value="${smry.busExpenses}"/>
                            </td>
                            <td rowspan="2" colspan="1" class="contenletter">
                                Remarks
                            </td>
                            <td rowspan="2" colspan="5">
                                <input type="hidden" id="settleRemarks" name="settleRemarks" value=""  />
                                <c:out value="${smry.remarks}"/>
                                <!--<textarea cols="80" rows="5" id="settleRemarks" name="settleRremarks" ></textarea>-->
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Total Amount
                            </td>
                            <td>
                                <input type="hidden" id="totalAmount" name="totalAmount" value=""  />
                                <c:out value="${smry.totalAmount}"/>
                            </td>
                        </tr>
                        <tr>&nbsp;</tr>
                        <tr>
                            <td colspan="8" align="center">
                            </td>
                        </tr>
                    </table>
                    </c:forEach>
                    <%
                                        }
                            %>
                    </c:if>
                        <center>
                            <input type="button" class="button" name="Print" value="Print" onClick="print('print');" >
                        </center>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>