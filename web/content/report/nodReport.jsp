<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE html>
<!--<html>-->
<head>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
    <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <title></title>
</head>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script>
    function redirectMethod(statusName, statusId, day) {
        document.nodReport.action = '/throttle/nodReportLevel2.do?statusName=' + statusName + '&statusId=' + statusId + '&day=' + day;
        document.nodReport.submit();
    }

    function openLevel3Report(statusName, statusId, day) {
        document.nodReport.action = '/throttle/nodReportLevel3.do?statusName=' + statusName + '&statusId=' + statusId + '&day=' + day;
        document.nodReport.submit();
    }

</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Aging Report" text="Aging Report"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Report"/></a></li>
            <li class=""><spring:message code="hrms.label.Aging Report" text="Aging Report"/></li>
        </ol>
    </div>
</div>
<c:set var="zeroDayTotal" value="0"/>
<c:set var="oneDayTotal" value="0"/>
<c:set var="twoDayTotal" value="0"/>
<c:set var="threeDayTotal" value="0"/>
<c:set var="fourDayTotal" value="0"/>
<c:set var="fiveDayTotal" value="0"/>
<c:set var="sixDayTotal" value="0"/>
<c:set var="sevenDayTotal" value="0"/>
<c:set var="moreDayTotal" value="0"/>

<c:set var="zeroDayTotals" value="0"/>
<c:set var="oneDayTotals" value="0"/>
<c:set var="twoDayTotals" value="0"/>
<c:set var="threeDayTotals" value="0"/>
<c:set var="fourDayTotals" value="0"/>
<c:set var="fiveDayTotals" value="0"/>
<c:set var="sixDayTotals" value="0"/>
<c:set var="sevenDayTotals" value="0"/>
<c:set var="moreDayTotals" value="0"/>

<c:set var="orderCreatedTotal" value="0"/>
<c:set var="tripCreatedTotal" value="0"/>
<c:set var="tripFreezedTotal" value="0"/>
<c:set var="tripStartTotal" value="0"/>
<c:set var="tripEndTotal" value="0"/>
<c:set var="tripClosureTotal" value="0"/>
<c:set var="tripSettledTotal" value="0"/>
<c:set var="billCreatedTotal" value="0"/>
<c:set var="billSubmittedTotal" value="0"/>
<c:set var="summaryTotal" value="0"/>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <br>
            <table class="table table-info mb30 table-bordered"  style="width:85%" align="center">
                <thead >
                <th >&emsp; &emsp;&emsp;&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;Status </th>
                <th>1 to 7</th>
                <th>8 to 14</th>
                <th>15 to 20</th>
                <th>21 to 30</th>
                <th>>30</th>
                <th>Total</th>
                </thead>
                <c:if test = "${monthNodList != null}" >
                    <c:forEach items="${monthNodList}" var="mnl">

                        <tr>
                            <td><b>
                                    <c:out value="${mnl.statusName}"/>
                                </b>
                            </td>
                            <td align="center"><b><c:if test="${mnl.week1==0}">
                                        <font size='1'>-</font> 
                                    </c:if>
                                    <c:if test="${mnl.week1!=0}">
                                        <font size='3' color="green">
                                        <c:out value="${mnl.week1}"/>
                                        <c:set var="week1total" value="${week1total+mnl.week1}"/> 
                                        </font>
                                    </c:if>

                                </b>
                            </td>
                            <td align="center"><b><c:if test="${mnl.week2==0}">
                                        <font size='1'>-</font> 
                                    </c:if>
                                    <c:if test="${mnl.week2!=0}">
                                        <font size='3' color="green">
                                        <c:out value="${mnl.week2}"/>
                                        <c:set var="week2total" value="${week2total+mnl.week2}"/> 
                                        </font>
                                    </c:if>

                                </b>
                            </td>
                            <td align="center"><b><c:if test="${mnl.week3==0}">
                                        <font size='1'>-</font> 
                                    </c:if>
                                    <c:if test="${mnl.week3!=0}">
                                        <font size='3' color="green">
                                        <c:out value="${mnl.week3}"/>
                                        <c:set var="week3total" value="${week3total+mnl.week3}"/> 
                                        </font>
                                    </c:if>

                                </b>
                            </td>
                            <td align="center"><b><c:if test="${mnl.week4==0}">
                                        <font size='1'>-</font> 
                                    </c:if>
                                    <c:if test="${mnl.week4!=0}">
                                        <font size='3' color="green">
                                        <c:out value="${mnl.week4}"/>
                                        <c:set var="week4total" value="${week4total+mnl.week4}"/> 
                                        </font>
                                    </c:if>

                                </b>
                            </td>
                            <td align="center"><b><c:if test="${mnl.week5==0}">
                                        <font size='1'>-</font> 
                                    </c:if>
                                    <c:if test="${mnl.week5!=0}">
                                        <font size='3' color="green">
                                        <c:out value="${mnl.week5}"/>
                                        <c:set var="week5total" value="${week5total+mnl.week5}"/> 
                                        </font>
                                    </c:if>

                                </b>
                            </td>
                        
                            <td align="center"><b>
                                    <font color="#FF3D03" size='3'>
                                    <c:out value="${mnl.week1+mnl.week2+mnl.week3+mnl.week4+mnl.week5}"/>
                                    <c:set var="totalTotal" value="${totalTotal+mnl.week1+mnl.week2+mnl.week3+mnl.week4+mnl.week5}"/>
                                    </font></b>
                            </td>
                        </tr>
                    </c:forEach>
                </c:if>
                <tr>
                    <td style="text-align: center"><font size='3'><b>Total</b></font></td>
                    <td style="text-align: center"><font color="green" size='3'><b><c:out value="${week1total}"/></b></font></td>
                    <td style="text-align: center"><font color="green" size='3'><b><c:out value="${week2total}"/></b></font></td>
                    <td style="text-align: center"><font color="green" size='3'><b><c:out value="${week3total}"/></b></font></td>
                    <td style="text-align: center"><font color="green" size='3'><b><c:out value="${week4total}"/></b></font></td>
                    <td style="text-align: center"><font color="green" size='3'><b><c:out value="${week5total}"/></b></font></td>
                    <td style="text-align: center"><font color="#FF3D03" size='3'><b><c:out value="${totalTotal}"/></b></font></td>
                </tr>
            </table>
            <div class="contentpanel" style="overflow: auto;height:1600px;padding-left: 50px">

                <table style="width:100%">

                    <tr>
                        <td>
                            <div class="row" >
                                <div class="col-sm-8 col-md-9">
                                    <div class="panel panel-default">

                                        <div class="outer4">
                                            <font color="white"><b>Monthly Delivery Orders</b>  </font>
                                            <div  id="monDeliveryStatus" style="width: 530px; height: 350px; margin: 0 auto"></div>
                                        </div>                        
                                    </div>
                                </div>

                            </div>
                        </td>
                        <td style="padding-left:10px">
                            <div class="row" >
                                <div class="col-sm-8 col-md-9">
                                    <div class="panel panel-default">

                                        <div class="outer5">
                                            <font color="white"><b>Pending Orders</b>  </font>
                                            <div  id="monthlyPendingOrders" style="width: 530px; height: 350px; margin: 0 auto"></div>
                                        </div>                        
                                    </div>
                                </div>

                            </div>
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <div class="row" >
                                <div class="col-sm-8 col-md-9">
                                    <div class="panel panel-default">

                                        <div class="outer3">
                                            <font color="white"><b>Product Wise Delivered</b>  </font>
                                            <div  id="productWiseDelivery" style="width: 530px; height: 350px; margin: 0 auto"></div>
                                        </div>                        
                                    </div>
                                </div>

                            </div>
                        </td>
                        <td style="padding-left:10px">
                            <div class="row" >
                                <div class="col-sm-8 col-md-9">
                                    <div class="panel panel-default">

                                        <div class="outer1">
                                            <font color="white"><b>Product Wise Pending</b>  </font>
                                            <div  id="productWisePending" style="width: 530px; height: 350px; margin: 0 auto"></div>
                                        </div>                        
                                    </div>
                                </div>

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row" >
                            <div class="col-sm-8 col-md-9">
                                <div class="panel panel-default">
                                    <div class="outer5">
                                        <font color="white">Month Wise Order FY: 2021-2022</font>
                                        <div  id="monthStatus" style="width: 530px; height: 400px; margin: 0 auto"></div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                </table>





            </div><!-- contentpanel -->

        </div><!-- mainpanel -->

    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>



<script>

    getMonDeliveryStatus();
    function getMonDeliveryStatus() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/getMonTripNos.do',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(key, value) {
                    x_values_sub['name'] = value.Name;
                    x_values_sub['y'] = parseInt(value.Count);
                    x_values.push(x_values_sub);
                    x_values_sub = {};

                });

                $('#monDeliveryStatus').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });

    }

    getMonthlyPending();
    function getMonthlyPending() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/monthlyPendingOrders.do',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(key, value) {
                    x_values_sub['name'] = value.Name;
                    x_values_sub['y'] = parseInt(value.Count);
                    x_values.push(x_values_sub);
                    x_values_sub = {};

                });

                $('#monthlyPendingOrders').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });

    }

    productWiseDelivery();
    function productWiseDelivery() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/productWiseDelivered.do',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(key, value) {
                    x_values_sub['name'] = value.Name;
                    x_values_sub['y'] = parseInt(value.Count);
                    x_values.push(x_values_sub);
                    x_values_sub = {};

                });

                $('#productWiseDelivery').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });

    }

    productWisePending();
    function productWisePending() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/productWisePending.do',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(key, value) {
                    x_values_sub['name'] = value.Name;
                    x_values_sub['y'] = parseInt(value.Count);
                    x_values.push(x_values_sub);
                    x_values_sub = {};

                });

                $('#productWisePending').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });

    }

    function3();
    function function3() {
        var x_values = [];
        var y_values = [];
        $.ajax({
            url: '/throttle/getCustRevenu.do',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(key, value) {
                    x_values.push(value.Name);
                    y_values.push([parseInt(value.Count)]);
                });
                $('#monthStatus').highcharts({
                    chart: {type: 'column',
                        options2d: {
                            enabled: true,
                            alpha: 15,
                            beta: 15,
                            depth: 50,
                            viewDistance: 25
                        }
                    }, plotOptions: {
                        series: {
                            depth: 25,
                            colorByPoint: true
                        }
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    }, credits: {
                        enabled: false
                    },
                    xAxis: {
                        type: 'category',
                        categories: x_values,
                        labels: {
                            style: {
                                fontSize: '13px',
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: '{series.name} : <b>{point.y:f} Nos</b>'
                    },
                    series: [{
                            name: '',
                            colorByPoint: true,
                            data: y_values,
                            dataLabels: {
                                enabled: true,
                                rotation: 180,
                                color: '#FFFFFF',
                                align: 'right',
                                format: '{point.y:f}',
                                y: 10,
                                style: {
                                    fontSize: '0px',
                                }
                            }
                        }]
                });
            }
        });
    }


</script>



<style>
    .outer1 {
        width: 550px;
        color: navy;            
        background-color: #1caf9a;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer2 {
        width: 550px;
        color: navy;
        background-color: #a94442;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer3 {
        width: 550px;
        color: navy;
        background-color: #f0ad4e;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer4 {
        width: 550px;
        color: navy;
        background-color: #ccad4e;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer5 {
        width: 550px;
        color: navy;            
        background-color: #1caf9a;
        border: 2px solid darkblue;
        padding: 5px;
    }

    .ben{
        width: 550px;
    }



    .highcharts-figure, .highcharts-data-table table {
        min-width: 320px; 
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }


    input[type="number"] {
        min-width: 50px;
    }





    .highcharts-figure, .highcharts-data-table table {
        min-width: 310px; 
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }

    .highcharts-figure, .highcharts-data-table table {
        min-width: 310px; 
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }



</style>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

