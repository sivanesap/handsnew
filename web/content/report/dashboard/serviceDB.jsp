<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" href="css/jquery.ui.theme.css">
	<script src="js/jquery-1.4.4.js"></script>
	<script src="js/jquery.ui.core.js"></script>
	<script src="js/jquery.ui.widget.js"></script>
	<script src="js/jquery.ui.mouse.js"></script>
	<script src="js/jquery.ui.sortable.js"></script>
        <SCRIPT LANGUAGE="Javascript" SRC="/tclaccess/js/FusionCharts.js"></SCRIPT>
<style type="text/css">
.link {
	font: normal 12px Arial;
	text-transform:uppercase;
	padding-left:10px;
	font-weight:bold;
}

.link a, a:hover {
	color:#3381cb;
}

</style>
	<style type="text/css">
         #expand {
                width:100%;
        }
	.column { width: 33%; float: left; }
	.portlet { margin: 0 1em 1em 0; }
	.portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; cursor:move; }
	.portlet-header .ui-icon { float: right; }
	.portlet-content { padding: 0.4em; height: 200px; }
	.ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
	.ui-sortable-placeholder * { visibility: hidden; }
	</style>
	<script>
	$(function() {
		$( ".column" ).sortable({
			connectWith: ".column"
		});

		$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header" )
				.addClass( "ui-widget-header ui-corner-all" )
				.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
				.end()
			.find( ".portlet-content" );

		$( ".portlet-header .ui-icon" ).click(function() {
			$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
		});

		$( ".column" ).disableSelection();
	});
	</script>
</head>
<body>



<div class="column" >
<%

String strXML = "<Chart  bgColor='FFFFFF' borderColor='FFFFFF' upperLimit='500' lowerLimit='0' baseFontColor='FFFFFF' majorTMNumber='11' majorTMColor='FFFFFF'  majorTMHeight='8' minorTMNumber='5' minorTMColor='FFFFFF' minorTMHeight='3' toolTipBorderColor='FFFFFF' toolTipBgColor='333333' gaugeOuterRadius='70' gaugeOriginX='110' gaugeOriginY='90' gaugeScaleAngle='230' placeValuesInside='1' gaugeInnerRadius='85%25' annRenderDelay='0' gaugeFillMix='' pivotRadius='10' showPivotBorder='0' pivotFillMix='{CCCCCC},{333333}' pivotFillRatio='50,50' showShadow='0'>	<colorRange>	<color minValue='0' maxValue='12' code='C1E1C1' alpha='12'/> 	<color minValue='13' maxValue='320' code='00CC99' alpha='13'/>	</colorRange> 	<dials><dial value='150' "+
        " borderColor='FFFFFF' bgColor='000000,CCCCCC,000000' borderAlpha='0' baseWidth='10'/>	</dials><annotations>	<annotationGroup xPos='110' yPos='90' showBelow='1'><annotation type='circle' xPos='0' yPos='0' radius='90' startAngle='0' endAngle='360' fillColor='CCCCCC,111111'  fillPattern='linear' fillAlpha='100,100'  fillRatio='50,50' fillAngle='-45'/>			<annotation type='circle' xPos='0' yPos='0' radius='80' startAngle='0' endAngle='360' fillColor='111111,cccccc'  fillPattern='linear' fillAlpha='100,100'  fillRatio='50,50' fillAngle='-45'/>			<annotation type='circle' xPos='0' yPos='0' radius='75' startAngle='0' endAngle='360' color='666666'/>		</annotationGroup>	</annotations></Chart>";
System.out.println("strXML:"+strXML);
%>
	<div class="portlet">
		<div class="portlet-header">Cities - Current Status</div>
		<div class="portlet-content">

               <table align="center" style="margin-left:5px;" >
                <tr>
                    <td >
                    
                     <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                    <jsp:param name="chartSWF" value="/tclaccess/swf/AngularGauge.swf" />
                    <jsp:param name="strURL" value="" />
                    <jsp:param name="strXML" value="<%=strXML%>" />
                    <jsp:param name="chartId" value="productSales" />
                    <jsp:param name="chartWidth" value="200" />
                    <jsp:param name="chartHeight" value="200" />
                    <jsp:param name="debugMode" value="false" />
                    <jsp:param name="registerWithJS" value="false" />
                    </jsp:include>
                    
                    </td>
                </tr>

            </table>
        </div>
	</div>

</div>

<!-- End demo -->





</body>
</html>
