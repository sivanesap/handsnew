<%-- 
    Document   : companyWiseTripReport
    Created on : Sep 6, 2012, 4:01:54 PM
    Author     : Throttle
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/filtergrid.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>



        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>        
                
    </head>



    <script>
        function submitPage(value){
            /*if(document.companyWiseTripReport.companyId.value == 0){
                alert("Please select the company")
                document.companyWiseTripReport.companyId.focus();
            }else if(isEmpty(document.companyWiseTripReport.fromDate.value)){
                alert("Please select the from date")
                document.companyWiseTripReport.fromDate.focus();
            }else if(isEmpty(document.companyWiseTripReport.toDate.value)){
                alert("Please select the to date")
                document.companyWiseTripReport.toDate.focus();
            }else{*/
                document.companyWiseTripReport.action="/throttle/companyWiseReport.do";
                document.companyWiseTripReport.submit();
            //}
        }
        function setFocus(){
            var companyId='<%=request.getAttribute("companyId")%>';
            var fromDate='<%=request.getAttribute("fromDate")%>';
            var toDate='<%=request.getAttribute("toDate")%>';
            if(companyId!='null' && fromDate!='null' && toDate!='null'){
                document.companyWiseTripReport.companyId.value=companyId;
            }if(fromDate!='null'){
                document.companyWiseTripReport.fromDate.value=fromDate;
            }if(toDate!='null'){
                document.companyWiseTripReport.toDate.value=toDate;
            }
        }

    </script>
    <body onload="setFocus();">
        <form name="companyWiseTripReport" method="post">
                <%@ include file="/content/common/path.jsp" %>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>
            <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Company Wise Trip Report</li>
                            </ul>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td  height="25" >Company Name</td>
                                        <td  height="25" ><select name="companyId" class="textbox" id="companyId"  style="width:260px;" >
                                                <option value="0">---Select---</option>
                                                <c:if test = "${customerList != null}" >
                                                    <c:forEach items="${customerList}" var="list">
                                                        <option value='<c:out value="${list.customertypeId}" />'><c:out value="${list.customertypeName}" /></option>
                                                    </c:forEach >
                                                </c:if>
                                            </select>  </td>

                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);"></td>

                                        <td><input type="button" class="button"  onclick="submitPage(0);" value="Search"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <br>
            <c:if test="${companyWiseTripReport == null}">
                <tr><font color="red">No Records Found</font></tr>
            </c:if>
        <br/>
            <c:if test = "${companyWiseTripReport != null}" >
                <%
                    int index = 0;
                    ArrayList companyWiseTripReport = (ArrayList) request.getAttribute("companyWiseTripReport");
                    if (companyWiseTripReport.size() != 0) {
                %>
                <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
                    <tr>
                    <td class="contentsub" height="30">S.No</td>
                    <td class="contentsub" height="30">Vehicle Number</td>
                    <td class="contentsub" height="30">Driver Name</td>
                    <td class="contentsub" height="30">Route Name</td>
                    <td class="contentsub" height="30">Trip Type</td>
                    <td class="contentsub" height="30">Total Tonnage</td>
                    <td class="contentsub" height="30">Delivered Tonnage</td>
                    <td class="contentsub" height="30">Total Ton Amount</td>
                    <td class="contentsub" height="30">Trip Start Date</td>
                    <td class="contentsub" height="30">Trip End Date</td>
                    <td class="contentsub" height="30">Status</td>
                </tr>
                    <c:set var="tonTotal" value="${0}" />
                    <c:set var="dTonTotal" value="${0}" />
                    <c:set var="totalTonAmt" value="${0}" />
                    <c:forEach items="${companyWiseTripReport}" var="comRt">
                        <c:set var="total" value="${total+1}"></c:set>
                        <c:set var="tonTotal" value="${tonTotal + comRt.totalTonnage}" />
                        <c:set var="dTonTotal" value="${dTonTotal + comRt.deliveredTonnage}" />
                        <c:set var="totalTonAmt" value="${totalTonAmt + comRt.totalTonAmount}" />
                        <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }                            
                        %>
                        <%index++;%>
                        <tr>
                            <td class="<%=classText%>"  height="30"><%=index%></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${comRt.regno}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${comRt.driverName}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${comRt.routeName}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${comRt.tripType}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${comRt.totalTonnage}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${comRt.deliveredTonnage}"/></td>
                            <td class="<%=classText%>"  height="30"><fmt:formatNumber type="currency" pattern="##.00" value="${comRt.totalTonAmount}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${comRt.tripStartDate}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${comRt.tripEndDate}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${comRt.status}"/></td>

                        </tr>                        
                    </c:forEach>
                </table>
                <%
                            }
                %>
                <br />
                <table border="0" align="center" width="70%" cellpadding="0" cellspacing="5" style="background: #BDEDFF;" >
                    <tr>
                        <th>Summary</th>
                        <td>Total Tonnage &nbsp;&nbsp; = &nbsp;&nbsp;<fmt:formatNumber pattern="##.00" value="${tonTotal}"/></td>
                        <td>Delivered Tonnage &nbsp;&nbsp; = &nbsp;&nbsp;<fmt:formatNumber pattern="##.00" value="${dTonTotal}"/></td>
                        <td>Total Tonnage Amount &nbsp;&nbsp; = &nbsp;&nbsp;<fmt:formatNumber pattern="##.00" value="${totalTonAmt}"/></td>
                    </tr>
                </table>
            </c:if>          
           
        
            <script language="javascript" type="text/javascript">
                //<![CDATA[
                var recapFilters = {
                    sort_select: true,
                    loader: true,
                    col_3: "select",
                    on_change: true,
                    display_all_text: " [ Show all ] ",
                    rows_counter: true,
                    btn_reset: true,
                    alternate_rows: true,
                    btn_reset_text: "Clear",
                    col_width: ["150px","150px",null,null],

                    sort_select: true,
                    loader: true,
                    col_5: "select",
                    on_change: true,
                    display_all_text: " [ Show all ] ",
                    rows_counter: true,
                    btn_reset: true,
                    alternate_rows: true,
                    btn_reset_text: "Clear",
                    col_width: ["150px","150px",null,null],

                    sort_select: true,
                    loader: true,
                    col_8: "select",
                    on_change: true,
                    display_all_text: " [ Show all ] ",
                    rows_counter: true,
                    btn_reset: true,
                    alternate_rows: true,
                    btn_reset_text: "Clear",
                    col_width: ["150px","150px",null,null]
                }

                setFilterGrid("recap",recapFilters);
                //]]>
            </script>
    </form>
</body>
</html>
