<%-- 
    Document   : vehicleUtilizationReportExcel
    Created on : Dec 23, 2013, 3:59:10 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "StockQtyDetailReport-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
             <c:if test="${stockQtyDetails1!=null}">
                           <table align="center" border="0" id="table" class="sortable" style="width:1000px;" >
                                <thead>
                                    <tr>
                                        <th align="center"><h3>S.No</h3></th>
                                        <th align="center"><h3>Item Name</h3></th>
                                        <th align="center"><h3>Loan Proposal Id</h3></th>
                                        <th align="center"><h3>Serial No</h3></th>
                                        <th align="center"><h3>Qty</h3></th>
                                    </tr> 
                                </thead>
                                <tbody>
                                <% int index = 1;%>
                                    <c:forEach items="${stockQtyDetails1}" var="stock">
                                        <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                          %>
                                        <tr>
                                            <td class="<%=classText%>" ><%=index++%></td>
                                            <td class="<%=classText%>" align="center"><c:out value="${stock.itemName}"/></td>
                                            <td class="<%=classText%>" align="center"><c:out value="${stock.loanProposalId}"/></td>
                                            <td class="<%=classText%>" align="center"><c:out value="${stock.serialNo}"/>
                                            </td>
                                            <td class="<%=classText%>" align="center">1</td>
                                        </tr>

                                    </c:forEach>
                                </tbody>
                            </table>
                                    </c:if>
            <c:if test="${stockQtyDetails == null}">
                <center>
                <font color="blue">Please Wait Your Request is Processing</font>
                </center>
            </c:if>
            <c:if test="${stockQtyDetails != null}">
                <table align="center" border="0" id="table" class="sortable" style="width:1000px;" >
                    <thead>
                        <tr>
                            <th align="center"><h3>S.No</h3></th>
                            <th align="center"><h3>Item Name</h3></th>
                            <th align="center"><h3>Item Code</h3></th>
                            <th align="center"><h3>Serial No</h3></th>
                            <th align="center"><h3>HSN Code</h3></th>
                            <th align="center"><h3>Qty</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                    <% int index = 1;%>
                        <c:forEach items="${stockQtyDetails}" var="stock">
                                <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                                %>
                                    <tr>
                                        <td class="<%=classText%>"  ><%=index++%></td>
                                        <td class="<%=classText%>" align="center"><c:out value="${stock.itemName}"/></td>
                                        <td class="<%=classText%>" align="center"><c:out value="${stock.productCode}"/></td>
                                        <td class="<%=classText%>" align="center"><c:out value="${stock.serialNo}"/></td>
                                        <td class="<%=classText%>" align="center"><c:out value="${stock.hsnCode}"/></td>
                                        <td class="<%=classText%>" align="center"><c:out value="${stock.itemQty}"/></td>
                                    </tr>

                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <br/>
        </form>
    </body>
</html>
