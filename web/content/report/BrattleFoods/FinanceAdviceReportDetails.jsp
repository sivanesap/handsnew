<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>-->
         
    </head>
    <script language="javascript">
//         function submitPage(value){
////                  if(value == "ExportExcel"){
////                     alert("HI"+hi); 
//                     document.financeAdviceReportDL.action = '/throttle/handlefinanceAdviceReportExcel.do?param=ExportExcel';
//                     document.financeAdviceReportDL.submit();
////                    }
//                }     
            
    </script>
    
    <%
                String menuPath = "Finance >> Daily Advance Advice";
                request.setAttribute("menuPath", menuPath);
                String dateval = request.getParameter("dateval");
                String active = request.getParameter("active");
                String type = request.getParameter("type");
    %>
    <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageUser" text="default text"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Settings" text="default text"/></a></li>
                    <li class=""><spring:message code="hrms.label.ManageUser" text="default text"/></li>

                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
    <body>
        <form name="financeAdviceReportDL" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
<!--            <center>
             <input type="button" class="button" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel">
            </center>-->

            <br><br>
            Advice Date: <%=dateval%>
            <br>
            <br>


               <c:if test = "${financeAdviceDetails != null}" >
               <table width="100%" align="center" border="0" id="table" class="sortable">
               <thead>                   
                    <tr height="40">
                        <th><h3>S.No</h3></th>
                        <th><h3>Type</h3></th>
                        <th><h3>Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>CNoteNo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>TripNo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>VehicleNo</h3></th>
                        <th><h3>VehicleType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>Route</h3></th>
                        <th><h3>Driver</h3></th>
                        <th><h3>TripStart</h3></th>
                        <th><h3>Nett Expenses</h3></th>
                        <th><h3>Already PaidAmount</h3></th>
                        <th><h3>Transit Days</h3></th>
                        <th><h3>Journey Day</h3></th>
                        <th><h3>ToBePaid Today</h3></th>
                        <th><h3>Paid</h3></th>
                        <th><h3>action</h3></th>
                    </tr>
                    </thead>
                    <tbody>

                        <% int index = 0;%>
                        <c:forEach items="${financeAdviceDetails}" var="FD">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }


                            %>
                            <tr height="30">
                                <td class="<%=classText%>" align="left"> <%= index + 1%> </td>
                                <td class="<%=classText%>" ><c:out value="${FD.batchType}"/></td>
                                <td class="<%=classText%>" ><c:out value="${FD.customerName}"/></td>
                                <td class="<%=classText%>">
                                    <c:out value="${FD.cnoteName}"/>
                                </td>
                                <td class="<%=classText%>" align="left"> <c:out value="${FD.tripcode}" />||<c:out value="${FD.tripid}" /></td>
                                <td class="<%=classText%>" align="left"><c:out value="${FD.regNo}" /></td>
                                <td class="<%=classText%>" ><c:out value="${FD.vehicleTypeName}"/></td>
                                <td class="<%=classText%>" ><c:out value="${FD.routeName}"/></td>
                                <td class="<%=classText%>" align="left"><c:out value="${FD.driverName}"/></td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${FD.planneddate}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${FD.estimatedexpense}"/> </td>
                                <td class="<%=classText%>" align="left"> <c:out value="${FD.actualadvancepaid}" /></td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${FD.estimatedtransitday}"/> </td>
                                <td class="<%=classText%>" align="left"> <c:out value="${FD.tripday}" /></td>


                                <td class="<%=classText%>" align="left">
                                    <c:if test="${FD.approvalstatus==0 || FD.approvalstatus==3}">
                                    <c:out value="${FD.estimatedadvance}" />
                                    </c:if>
                                    <c:if test="${FD.approvalstatus==1}">
                                    <c:out value="${FD.requestedadvance}" />
                                    </c:if>

                                </td>
                                
                                <%if(active.equals("1")){%>
                                <c:if test="${FD.approvalstatus==0}">
                                <td>-</td>
                                <td class="<%=classText%>" align="left">Waiting for Approval</td>
                                </c:if>



                                
                                <%--c:if test="${FD.approvalstatus==3 && FD.paidstatus=='N'}">
                                <td class="<%=classText%>" align="left"><a href="/throttle/tripSheetApprove.do?tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>
                                                                           &actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&advicedate=<%=dateval%>&billtype=<%=type%>">Request</a>&nbsp;&nbsp;</td>                                
                                </c:if--%>
                                
                                
                                


                                <c:if test="${(FD.approvalstatus==3) && FD.paidstatus=='N'}">
                                    <%if(!type.equalsIgnoreCase("M")){%>
                                    <td>-</td>
                                <td class="<%=classText%>" align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=0">pay</a></td>
                                                                           <%}%>
                                 </c:if>
                                 <c:if test="${(FD.approvalstatus==1 && FD.paidstatus=='N')}">
                                     <%if(!type.equalsIgnoreCase("M")){%>
                                 <td>-</td>
                                 <td class="<%=classText%>" align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=1">pay</a></td>
                                     <%}%>
                                 </c:if>

                                 <c:if test="${(FD.paidstatus!='Y')&& (FD.approvalstatus==1)}">
                                 <%if(type.equalsIgnoreCase("M")){%>
                                 <td>-</td>
                                 <td class="<%=classText%>" align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=1">pay</a></td>
                                 <%}%>
                                 </c:if>

                                <c:if test="${(FD.approvalstatus ==2) && FD.paidstatus !='Y' }">
                                    <td>-</td>
                                <td class="<%=classText%>" align="left">
                                    <c:if test="${(FD.approvalstatus ==2)}">
                                    Rejected
                                    </c:if>
                                    <c:if test="${(FD.approvalstatus ==1)}">
                                    Approved
                                    </c:if>
                                </td>
                                </c:if>


                                                                           

                                <c:if test="${FD.paidstatus=='Y'}">                                
                                <td class="<%=classText%>" align="left"><c:out value="${FD.paidamt}"/></td>
                                <td class="<%=classText%>" align="left">Already Paid</td>
                                </c:if>


                                <%}else{%>

                                <c:if test="${FD.paidstatus=='Y'}">
                                <td class="<%=classText%>" align="left"><font color="green"><c:out value="${FD.paidamt}"/></font></td>
                                <td class="<%=classText%>" align="left"><font color="green">Paid</font></td>
                                </c:if>

                                <c:if test="${FD.paidstatus !='Y'}">
                                    <td class="<%=classText%>" align="center"><font color="red">-</font></td>
                                    <td class="<%=classText%>" align="center"><font color="red">Not Paid</font></td>
                                </c:if>

                                

                                <%}%>
                                <!--
                                <td class="<%=classText%>" align="left"><a href="/throttle/manualfinanceadvice.do?tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>
                                                                           &actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&advicedate=<%=dateval%>&billtype=<%=type%>">Manual Request</a>&nbsp;&nbsp;
                                
                                </td>
                                -->


                            </tr>
                            <% index++;%>
                        </c:forEach>
                    </c:if>


            </tbody>
            </table>
            

            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>


        </form>

    </body>
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
