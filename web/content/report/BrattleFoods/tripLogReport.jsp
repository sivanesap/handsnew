<%--
    Document   : viewTripSheet
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Throttle
--%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "xhtml11.dtd">

<html debug="true">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script type="text/javascript" src="/throttle/js/jschart.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/highcharts.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/exporting.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
   
   
    <script type="text/javascript">
      $(function () {
         //dateTime = (Object) report.getLogTime();
         //temperature = (Object) report.getCurrentTemperature();
         var timeValues = '<%= request.getAttribute("timeValues") %>';
         var tempValues = '<%= request.getAttribute("tempValues") %>';
         var timeValueArray = timeValues.split(",")
         //var tempValueArray = tempValues.split(",");
         var tempValueArray = tempValues.split(",").map(Number);
         //var input = ['9','10','11','12','15','18','21','24','25'];
         var testArray = [9,10,11,12,1,2,3,4,5];
        $('#container').highcharts({
            chart: {
                type: 'line'
            },
            title: {
                text: 'Trip Temperature Report'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                //categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                categories: timeValueArray
            },
            yAxis: {
                title: {
                    text: 'Temperature (�C)'
                }
            },
            tooltip: {
                enabled: false,
                formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y +'�C';
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name: 'temperature',
                //data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3]
                data: tempValueArray
            }]
        });
    });


    </script>

  

</head>
<body>
    <form name="tripLogReport" method="post">

            <div style="margin: 5px">
                <script src="http://code.highcharts.com/highcharts.js"></script>
                <script src="http://code.highcharts.com/modules/exporting.js"></script>
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
<br/>
<br/> <input type="hidden" name="tripId" id="tripId" value="<c:out value="${tripId}"/>"/>
<c:if test="${tripLogDetailsSize != '0'}">
    <table align="center" border="0" id="table" class="sortable" width="100%" >

        <thead>
            <tr height="30">
                <th align="center"><h3>S.No</h3></th>
                <th align="center"><h3>Vehicle No</h3></th>
                <th align="center"><h3>Trip Code</h3></th>
                <th align="center"><h3>Trip Date</h3></th>
                <th align="center"><h3>Current Location</h3></th>
                <th align="center"><h3>Distance Travelled</h3></th>
                <th align="center"><h3>Current Temperature</h3></th>
                <th align="center"><h3>Log Date</h3></th>
            </tr>
        </thead>
        <tbody>
            <% int index = 1;%>
            <c:forEach items="${tripLogDetails}" var="tripLogDetails">
                <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                %>
                <tr>
                    <td class="<%=classText%>"><%=index++%></td>
                    <td  width="90" class="<%=classText%>">
                        <c:out value="${tripLogDetails.vehicleNo}"/>
                        <input type="hidden" value="<c:out value="${tripLogDetails.vehicleNo}"/>" name="vehicleNo" id="vehicleNo"/></td>
                    <td width="90" class="<%=classText%>">
                        <c:out value="${tripLogDetails.tripCode}"/>
                        <input type="hidden" value="<c:out value="${tripLogDetails.tripCode}"/>" name="tripCode" id="tripCode"/>
                    </td>
                    <td class="<%=classText%>" ><c:out value="${tripLogDetails.tripDate}"/></td>
                    <td class="<%=classText%>"  ><c:out value="${tripLogDetails.location}"/></td>
                    <td width="140" class="<%=classText%>"  ><c:out value="${tripLogDetails.distance}"/></td>
                    <td width="140" class="<%=classText%>"  ><c:out value="${tripLogDetails.currentTemperature}"/></td>
                    <td class="<%=classText%>"  ><c:out value="${tripLogDetails.logDate}"/></td>
                </tr>

            </c:forEach>
        </tbody>
    </table>
</c:if>
<center><input type="submit" name="Export excel" id="Export Excel" value="ExportExcel" onclick="submitPage();"/> </center>
<script>
    function submitPage(){
         document.tripLogReport.action = '/throttle/viewTripLogDetails.do?param=ExportExcel';
                        document.tripLogReport.submit();
    }
</script>
</form>
</body>
</html>

