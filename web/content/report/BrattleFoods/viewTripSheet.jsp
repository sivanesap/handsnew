<%--
    Document   : viewTripSheet
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Throttle
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">
        function mailTriggerPage(tripId) {
            window.open('/throttle/viewTripDetailsMail.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function viewCurrentLocation(vehicleRegNo) {
            var url = 'http://ivts.noviretechnologies.com/IVTS/reportEngServ.do?username=serviceuser&password=7C53C003126C10E1091C73F4F945FEB4&companyId=759&reportRef=ref_currentStatusMapServ&param0=759&param1=ref_currentStatusMapServ&param2=&param3=&param4=&param5=&param6=&param7=&param8=&param9=&param10=&param11=' + vehicleRegNo;
            //alert(url);
            window.open(url, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }

        function viewTripDetails(tripId) {
            window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function viewVehicleDetails(vehicleId) {
            window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script type="text/javascript">
        function setValues() {
            if ('<%=request.getAttribute("vehicleTypeId")%>' != 'null') {
                document.getElementById('vehicleTypeId').value = '<%=request.getAttribute("vehicleTypeId")%>';
            }
            if ('<%=request.getAttribute("fromDate")%>' != 'null') {
                document.getElementById('fromDate').value = '<%=request.getAttribute("fromDate")%>';
            }
            if ('<%=request.getAttribute("toDate")%>' != 'null') {
                document.getElementById('toDate').value = '<%=request.getAttribute("toDate")%>';
            }
            if ('<%=request.getAttribute("fleetCenterId")%>' != 'null') {
                document.getElementById('fleetCenterId').value = '<%=request.getAttribute("fleetCenterId")%>';
            }
            if ('<%=request.getAttribute("cityFromId")%>' != 'null') {
                document.getElementById('cityFromId').value = '<%=request.getAttribute("cityFromId")%>';
            }
            if ('<%=request.getAttribute("cityFrom")%>' != 'null') {
                document.getElementById('cityFrom').value = '<%=request.getAttribute("cityFrom")%>';
            }
            if ('<%=request.getAttribute("vehicleId")%>' != 'null') {
                document.getElementById('vehicleId').value = '<%=request.getAttribute("vehicleId")%>';
            }
            if ('<%=request.getAttribute("zoneId")%>' != 'null') {
                document.getElementById('zoneId').value = '<%=request.getAttribute("zoneId")%>';
            }

            if ('<%=request.getAttribute("tripSheetId")%>' != 'null') {
                document.getElementById('tripSheetId').value = '<%=request.getAttribute("tripSheetId")%>';
            }
            if ('<%=request.getAttribute("customerId")%>' != 'null') {
                document.getElementById('customerId').value = '<%=request.getAttribute("customerId")%>';
            }
            if ('<%=request.getAttribute("tripStatusId")%>' != 'null') {
                document.getElementById('tripStatusId').value = '<%=request.getAttribute("tripStatusId")%>';
            }
            if ('<%=request.getAttribute("podStatus")%>' != 'null') {
                document.getElementById('podStatus').value = '<%=request.getAttribute("podStatus")%>';
            }
            if ('<%=request.getAttribute("statusId")%>' != 'null') {
                document.getElementById('stsId').value = '<%=request.getAttribute("statusId")%>';
            }
        }

        function submitPage() {
            document.tripSheet.action = '/throttle/viewTripSheets.do?statusId=' + document.getElementById('stsId').value;
            document.tripSheet.submit();
        }
    </script>

    <script type="text/javascript">
        var httpRequest;
        function getLocation() {
            var zoneid = document.getElementById("zoneId").value;
            if (zoneid != '') {

                // Use the .autocomplete() method to compile the list based on input from user
                $('#cityFrom').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getLocationName.do",
                            dataType: "json",
                            data: {
                                cityId: request.term,
                                zoneId: document.getElementById('zoneId').value
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#cityFromId').val(tmp[0]);
                        $('#cityFrom').val(tmp[1]);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

            }
        }

    </script>

</head>
<body onload="sorter.size(20);
        setValues();">
    <form name="tripSheet" method="post">
        <%@ include file="/content/common/path.jsp" %>
        <%@include file="/content/common/message.jsp" %>
        <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
            <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                    </h2></td>
                <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
            </tr>
            <tr id="exp_table" >
                <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                    <div class="tabs" align="left" style="width:900;">
                        <ul class="tabNavigation">
                            <li style="background:#76b3f1">View Trip Sheet</li>
                        </ul>
                        <div id="first">
                            <table width="100%" cellpadding="0" cellspacing="5" border="0" align="center" class="table4">
                                
                                <tr>
                                    <td width="80" >Business Type </td>
                                    <td width="80" > <select name="businessTypeId" id="businessTypeId" class="textbox" style="height:20px; width:122px;" >
                                                <option value="1">Primary Trips</option>
                                                <option value="2">Secondary Trips</option>
                                        </select></td>
                                    <td  width="80" >Customer Name</td>
                                    <td  width="80"> <select name="customerId" id="customerId" class="textbox" style="height:20px; width:122px;"" >
                                            <c:if test="${customerList != null}">
                                                <option value="" selected>--Select--</option>
                                                <c:forEach items="${customerList}" var="customerList">
                                                    <option value='<c:out value="${customerList.customerName}"/>'><c:out value="${customerList.customerName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="80" >Vehicle Type </td>
                                    <td width="80" > <select name="vehicleTypeId" id="vehicleTypeId" class="textbox" style="height:20px; width:122px;" >
                                            <c:if test="${vehicleTypeList != null}">
                                                <option value="" selected>--Select--</option>
                                                <c:forEach items="${vehicleTypeList}" var="vehicleTypeList">
                                                    <option value='<c:out value="${vehicleTypeList.vehicleTypeName}"/>'><c:out value="${vehicleTypeList.vehicleTypeName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select></td>
                                    <td  width="80" >Vehicle No</td>
                                    <td  width="80"> <select name="vehicleId" id="vehicleId" class="textbox" style="height:20px; width:122px;"" >
                                            <c:if test="${vehicleList != null}">
                                                <option value="" selected>--Select--</option>
                                                <c:forEach items="${vehicleList}" var="vehicleList">
                                                    <option value='<c:out value="${vehicleList.vehicleId}"/>'><c:out value="${vehicleList.regNo}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                    </td>
                                </tr>
                                <tr>

                                    <td width="80" >Fleet Center</td>
                                    <td width="80"> <select name="fleetCenterId" id="fleetCenterId" class="textbox" style="height:20px; width:122px;"" >
                                            <c:if test="${companyList != null}">
                                                <option value="" selected>--Select--</option>
                                                <c:forEach items="${companyList}" var="companyList">
                                                    <option value='<c:out value="${companyList.cmpId}"/>'><c:out value="${companyList.name}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select></td>
                                    <td width="80" >Zone </td>
                                    <td width="80" > <select name="zoneId" id="zoneId" class="textbox" style="height:20px; width:122px;" onchange="getLocation();" >
                                            <c:if test="${zoneList != null}">
                                                <option value="" selected>--Select--</option>
                                                <c:forEach items="${zoneList}" var="zoneList">
                                                    <option value='<c:out value="${zoneList.zoneId}"/>'><c:out value="${zoneList.zoneName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select></td>

                                </tr>
                                <tr>
                                    <td width="80">Location</td>
                                    <td width="80">
                                        <input type="hidden" name="cityFromId" id="cityFromId" value="<c:out value="${cityName}"/>" class="textbox" >
                                        <input type="text" name="cityFrom" id="cityFrom" value="<c:out value="${cityId}"/>" class="textbox">
                                    </td>
                                    <td width="80">Customer</td>
                                    <td  width="80"> <select name="customerId" id="customerId" class="textbox" style="height:20px; width:122px;" >
                                            
                                        </select>
                                    </td>
                                    <td width="80" ><font color="red">*</font>From Date</td>
                                    <td width="80"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="height:15px; width:120px;" onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>


                                </tr>

                                <tr>
                                    <td width="80" ><font color="red">*</font>To Date</td>
                                    <td width="80"><input name="toDate" id="toDate" type="text" class="datepicker" style="height:15px; width:120px;" onClick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>
                                    <td width="80" >Status </td>
                                    <td width="80" > <select name="tripStatusId" id="tripStatusId" class="textbox" style="height:20px; width:122px;" >
                                            <c:if test="${statusDetails != null}">
                                                <option value="" selected>--Select--</option>
                                                <c:forEach items="${statusDetails}" var="statusDetails">
                                                    <option value='<c:out value="${statusDetails.statusId}"/>'><c:out value="${statusDetails.statusName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select></td>
                                    <td width="80" >POD Status </td>
                                    <td  width="80"> <select name="podStatus" id="podStatus" class="textbox" style="height:20px; width:122px;" >
                                            <option value="" selected>--Select--</option>
                                            <option value="0" >POD Uploaded</option>
                                            <option value="1" >POD not Uploaded</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td  colspan="6" align="center">
                                        <input type="hidden"   value="<c:out value="${statusId}"/>" class="text" name="stsId" id="stsId">
                                        <input type="hidden"   value="<c:out value="${tripType}"/>" class="text" name="tripType">
                                        <input type="button"   value="Search" class="button" name="Search" onclick="submitPage();" >
                                </tr>

                            </table>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <br>
        <br>
        <c:if test="${tripDetails != null}">
            <table align="center" border="0" id="table" class="sortable" style="width:1700px;" >
                <thead>
                    <tr height="50">
                        <th><h3>Sno</h3></th>
                        <th><h3>Mail</h3></th>
                            <c:if test="${tripType == 1}">
                            <th><h3>POD</h3></th>
                            </c:if>
                        <th><h3>Alert on Trip</h3></th>
                        <th><h3>Vehicle No </h3></th>
                        <th><h3>Trip Code</h3></th>
                        <th><h3>Reefer </h3></th>
                        <th><h3>Fleet Center with Contact</h3></th>
                        <th><h3>Customer Name </h3></th>
                        <th><h3>Customer Type </h3></th>
                        <th><h3>Billing Type </h3></th>
                        <th><h3>Route </h3></th>
                        <th><h3>Driver Name with Contact </h3></th>
                        <th><h3>Vehicle Type </h3></th>
                        <th><h3>Trip Schedule</h3></th>
                        <th ><h3>Expected Arrival Date and time</h3></th>
                        <th><h3>Trip Status</h3></th>
                        <th><h3>C-Note Edit</h3></th>
                        <th><h3>select</h3></th>
                        <th><h3>Link To Map</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <% int index = 1;%>
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <%
                                String className = "text1";
                                if ((index % 2) == 0) {
                                    className = "text1";
                                } else {
                                    className = "text2";
                                }
                        %>
                        <tr height="30">
                            <td class="<%=className%>" width="40" align="left">
                                <%=index%>
                            </td>
                            <td class="<%=className%>" width="40" align="left">
                                <img src="images/mailSendingImage.jpg" alt="Mail Alert"   title="Mail Alert" style="width: 30px;height: 30px" onclick="mailTriggerPage('<c:out value="${tripDetails.tripSheetId}"/>');"/>
                            </td>
                            <c:if test="${tripType == 1}">
                                <td class="<%=className%>" align="center">
                                    <c:if test="${tripDetails.statusId == 10 || tripDetails.statusId == 12 || tripDetails.statusId == 13  || tripDetails.statusId == 14  || tripDetails.statusId == 16}">
                                        <c:if test="${tripDetails.customerName != 'Empty Trip'}">
                                            <c:if test="${tripDetails.podCount == '0'}">
                                                <a href="viewTripPod.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">  <img src="images/Podinactive.png" alt="Y"   title="click to upload pod"/></a>
                                                </c:if>
                                                <c:if test="${tripDetails.podCount != '0'}">
                                                <a href="viewTripPod.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">  <img src="images/Podactive.png" alt="Y"   title="click to upload pod"/></a>
                                                </c:if>
                                            </c:if>
                                        </c:if>
                                </td>
                            </c:if>
                            <td class="<%=className%>" align="center" width="120">
                                <c:if test="${tripDetails.currentTemperature < '-10'  && tripDetails.currentTemperature > '-18' }">
                                    <img src="images/reefer_green.png" alt="reefer temp deviation"   title="reefer temp deviation"/>
                                </c:if>
                                <c:if test="${tripDetails.currentTemperature > '-10'  && tripDetails.currentTemperature < '-18' }">
                                    <img src="images/reefer_green.png" alt="reefer temp is within specified limit"   title="reefer temp is within specified limit"/>
                                </c:if>


                            </td>
                            <td class="<%=className%>" width="120">
                                <a href="#" onclick="viewVehicleDetails('<c:out value="${tripDetails.vehicleId}"/>')"><c:out value="${tripDetails.vehicleNo}"/></a><br><font color="red"><c:out value="${tripDetails.oldVehicleNo}"/></font></td>
                            <td class="<%=className%>" width="120" >
                                <a href="#" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>');"><c:out value="${tripDetails.tripCode}"/></a>
                            </td>
                            <td class="<%=className%>" align="center">
                                <c:if test="${tripDetails.reeferRequired == 'YES' || tripDetails.reeferRequired == 'Yes' }">
                                    <img src="images/ActiveRefer.png" alt="Yes"   title="Yes"/>
                                </c:if>
                                <c:if test="${tripDetails.reeferRequired == 'NO' || tripDetails.reeferRequired == 'No' }">
                                    <img src="images/DeactiveRefer.png" alt="No"   title="No"/>
                                </c:if>
                            </td>
                            <td class="<%=className%>" width="150"><c:out value="${tripDetails.fleetCenterName}"/><br/><c:out value="${tripDetails.fleetCenterNo}"/></td>


                            <td  align="center" class="<%=className%>" width="150">
                                <c:if test="${tripDetails.customerName != 'Empty Trip'}">
                                    <c:out value="${tripDetails.customerName}"/>
                                </c:if>
                                <c:if test="${tripDetails.customerName == 'Empty Trip'}">
                                    <img src="images/emptytrip.png" alt="EmptyTrip"   title="Empty Trip"/>
                                </c:if></td>

                            <td class="<%=className%>" align="center">
                                <c:if test="${tripDetails.customerType == 'Contract'}">
                                    <img src="images/contract.png" alt="Y"   title="contract customer"/>
                                </c:if>
                                <c:if test="${tripDetails.customerType == 'Walk In'}">
                                    <img src="images/walkin.gif" alt="Y"  title="walkin customer"/>
                                </c:if></td>
                            <td class="<%=className%>" ><c:out value="${tripDetails.billingType}"/></td>
                            <td class="<%=className%>" >                                
                                <c:if test="${tripDetails.routeNameStatus > '2'}">
                                    <img src="images/Black_star1.png" alt="MultiPoint"  title=" MultiPoint"/><c:out value="${tripDetails.routeInfo}"/>
                                </c:if>
                                <c:if test="${tripDetails.routeNameStatus <= '2'}">
                                    <c:out value="${tripDetails.routeInfo}"/>
                                </c:if>
                            </td>

                            <td class="<%=className%>" ><c:out value="${tripDetails.driverName}"/><br/><c:out value="${tripDetails.mobileNo}"/><br><font color="red"><c:out value="${tripDetails.oldDriverName}"/></font></td>
                            <td class="<%=className%>" ><c:out value="${tripDetails.vehicleTypeName}"/></td>
                            <td class="<%=className%>" ><c:out value="${tripDetails.tripScheduleDate}"/> &nbsp; <c:out value="${tripDetails.tripScheduleTime}"/></td>
                            <td class="<%=className%>" width="150"><c:out value="${tripDetails.expectedArrivalDate}"/> &nbsp; <c:out value="${tripDetails.expectedArrivalTime}"/></td>

                            <td class="<%=className%>" ><c:out value="${tripDetails.status}"/></td>


                            <c:if test="${roleId != '1030'}">

                                <td class="<%=className%>" >
                                    <c:if test="${tripDetails.statusId >= 7 && tripDetails.statusId <= 10 && tripDetails.customerOrderReferenceNo != 'Empty Trip'}">
                                        <a href="getConsignmentDetailsForEditAfterTripStart.do?consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>"><c:out value="${tripDetails.cNotes}"/></a>
                                    </c:if>
                                    <c:if test="${tripDetails.statusId >= 7 && tripDetails.statusId <= 10 && tripDetails.customerOrderReferenceNo == 'Empty Trip'}">
                                        &nbsp;
                                    </c:if>    
                                </td>
                                <td class="<%=className%>" >

                                    <!--   <c:if test="${tripDetails.statusId == 8}">
                                           <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>">UnFreeze</a>
                                           &nbsp;<a href="viewPreTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>">PreStart</a>
                                    </c:if>-->
                                    <c:if test="${tripDetails.emptyTripApprovalStatus == 0}">
                                        Empty Trip Waiting For Approval
                                    </c:if>

                                    <c:if test="${tripDetails.emptyTripApprovalStatus == 1}">


                                        <c:if test="${tripDetails.statusId == 6}">
                                            <a href="viewTripSheetForVehicleAllotment.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentOrderNos=<c:out value="${tripDetails.consignmentId}"/>&customerName=<c:out value="${tripDetails.customerName}"/>">AllotVehicle</a>


                                        </c:if>
                                        <c:if test="${tripDetails.statusId == 7}">
                                            <c:if test="${RoleId != 1033}">
                                                <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Freeze</a>
                                            </c:if>
                                        </c:if>
                                        <c:if test="${tripDetails.statusId == 8}">
                                            <c:if test="${RoleId != 1033}">
                                                <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">UnFreeze</a>
                                            </c:if>
                                            <c:if test="${tripDetails.tripCountStatus == 0}">
                                                &nbsp; <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Start</a>
                                            </c:if>
                                            <c:if test="${tripDetails.tripCountStatus == 1}">
                                                &nbsp;Started
                                            </c:if>
                                            <c:if test="${RoleId == 1023 || RoleId == 1033}">

                                                <c:if test="${tripDetails.tripType == 2}">
                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                        <a href="payManualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">pay</a><br>
                                                    </c:if>
                                                </c:if>


                                            </c:if>

                                        </c:if>

                                        <c:if test="${tripDetails.statusId == 10}">
                                            <c:if test="${tripDetails.routeNameStatus > '2'}">
                                                <a href="viewTripRouteDetails.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Loading & Unloading</a>
                                            </c:if>
                                        </c:if>

                                        <c:if test="${tripDetails.statusId == 10}">
                                            <a href="viewWFUTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">WFU</a>
                                            &nbsp;
                                            <a href="viewTripSheetForVehicleChange.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">ChangeVehicle</a>
                                            <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 0}">
                                                &nbsp;
                                            </c:if>
                                            <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 0}">
                                                <a href='/throttle/viewPaymentDetails.do?consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&freightAmount=<c:out value="${tripDetails.orderRevenue}"/>&paymentType=<c:out value="${tripDetails.paymentType}"/>&nextPage=2'>Proceed To Pay</a>
                                            </c:if>
                                            <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 1}">
                                                &nbsp;
                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                            </c:if>
                                            <c:if test="${tripDetails.paymentType == 4 && tripDetails.advanceToPayStatus == 0}">
                                                <a href='/throttle/viewPaymentDetails.do?consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&freightAmount=<c:out value="${tripDetails.orderRevenue}"/>&paymentType=<c:out value="${tripDetails.paymentType}"/>&nextPage=2'>Proceed To Pay</a>
                                            </c:if>
                                            <c:if test="${tripDetails.paymentType == 4 && tripDetails.advanceToPayStatus == 1}">
                                                &nbsp;
                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                            </c:if>
                                            <c:if test="${tripDetails.paymentType == 1 || tripDetails.paymentType == 2 || tripDetails.paymentType == 0 }">
                                                &nbsp;
                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                            </c:if>
                                            &nbsp;

                                            <c:if test="${RoleId == 1023 || RoleId == 1033 || RoleId == 1041 || RoleId == 1032}">  

                                                <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1003}">
                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                        <a href="payManualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">pay</a><br>
                                                    </c:if>
                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) < 1}">
                                                        <b>rcmpaid</b> &nbsp;<a href="manualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>
                                                    </c:if>
                                                </c:if>
                                                <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1002}">
                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                        &nbsp;<a href="manualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>
                                                    </c:if>
                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) < 1}">
                                                        <b>rcmpaid</b> &nbsp;<a href="manualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>
                                                    </c:if>
                                                </c:if>

                                                <c:if test="${tripDetails.tripType != 2}">
                                                    <a href="manualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>
                                                </c:if>


                                            </c:if>
                                        </c:if>
                                        <c:if test="${tripDetails.statusId == 18}">
                                            <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 0}">
                                                &nbsp;
                                            </c:if>
                                            <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 1}">
                                                &nbsp;
                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                            </c:if>
                                            <c:if test="${tripDetails.paymentType == 4 && tripDetails.advanceToPayStatus == 0}">
                                                &nbsp;
                                            </c:if>
                                            <c:if test="${tripDetails.paymentType == 4 && tripDetails.advanceToPayStatus == 1}">
                                                &nbsp;
                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                            </c:if>
                                            <c:if test="${tripDetails.paymentType == 1 || tripDetails.paymentType == 2 || tripDetails.paymentType == 0 }">
                                                &nbsp;
                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                            </c:if>
                                            &nbsp;
                                            <c:if test="${RoleId == 1023 || RoleId == 1033 || RoleId == 1032}">
                                                <a href="manualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>
                                            </c:if>
                                        </c:if>

                                        <c:if test="${tripDetails.statusId == 12}">
                                            <a href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">closure</a>
                                            &nbsp;
                                            <c:if test="${tripDetails.extraExpenseValue == 1}">
                                                <img title="Extra Expense" alt="Extra Expense" src="images/thumbDone.jpg" style="width: 30px;height: 30px" >
                                            </c:if>

                                        </c:if>
                                        <c:if test="${tripDetails.statusId == 13}">
                                            <a href="viewTripSettlement.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Settlement</a>
                                        </c:if>
                                    </c:if>
                                </td>
                            </c:if>
                            <c:if test="${roleId == '1030'}">
                                <td class="<%=className%>" width="150" align="left">&nbsp;</td>
                                <td class="<%=className%>" width="150" align="left">&nbsp;</td>
                            </c:if>
                            <td class="<%=className%>" width="450" align="left">
                                <c:if test="${tripDetails.statusId <= 10}">
                                    <c:if test="${tripDetails.gpsLocation == 'NA'}">
                                        NA
                                    </c:if>
                                    <c:if test="${tripDetails.gpsLocation != 'NA'}">
                                        <a href="#" onClick="viewCurrentLocation('<c:out value="${tripDetails.vehicleNo}" />');"><c:out value="${tripDetails.gpsLocation}" /></a>
                                    </c:if>
                                </c:if>
                            </td>




                        </tr>

                        <%index++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" >5</option>
                    <option value="10">10</option>
                    <option value="20" selected="selected">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
    </form>
</body>
</html>
