<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
       
        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "OrderReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

          <c:if test="${tripDetails != null}">
            <table border="1"  align="center" width="100%" cellpadding="0" cellspacing="1" >
                    <tr >
                          <th ><h3>S.No</th>
                          <th ><h3>Request</th>
                          <th ><h3>Response</th>
                          <th ><h3>Source</th>
                          
                    </tr>
                    <% int index = 1;%>
                 <c:set var="totalTrips" value="0" />
                    <c:set var="totalEarnings" value="0" />
                    <c:set var="totalExpense" value="0" />
                    <c:set var="earnings" value="0" />
                    <c:set var="expense" value="0" />
                    <c:forEach items="${tripDetails}" var="tripDetails">
                         <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>
                        <tr >
                                                        <td  align="center"><%=index++%></td>   
                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.jsonValue}"/></td>
                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.responseValue}"/></td>
                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.fromSource}"/></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
           
             
        </form>
    </body>    
</html>