<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
    </head>
    <script language="javascript">
         function submitPage(value){
        
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                }else{ 
                  if(value == "ExportExcel"){
//                     alert("HI"+hi); 
                     document.financeDetails.action = '/throttle/handlefinanceAdviceExcel.do?param=ExportExcel';
                     document.financeDetails.submit();
                    }
                else{
//                     alert("HI"+Search); 
                        document.financeDetails.action = '/throttle/handlefinanceAdviceExcel.do?param=Search';
                        document.financeDetails.submit();
                    }
                }     
            
        }
    </script>
    <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageUser" text="View Finance Advice"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Settings" text="default text"/></a></li>
                    <li class=""><spring:message code="hrms.label.ManageUser" text="default text"/></li>

                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
    
    <body>
        <form name="financeDetails" method="post" >
            <%@ include file="/content/common/message.jsp" %>
             <table class="table table-info mb10 table-hover" id="bg" >
		    <thead>
		<tr>
		    <th colspan="2" height="30" >View Finance Advice</th>
		</tr>
		    </thead>
    </table>
    <table class="table table-info mb30 table-hover" id="bg" >
     
                           <%!
                           public String NullCheck(String inputString)
                                {
                                        try
                                        {
                                                if ((inputString == null) || (inputString.trim().equals("")))
                                                                inputString = "";
                                        }
                                        catch(Exception e)
                                        {
                                                                inputString = "";
                                        }
                                        return inputString.trim();
                                }
                           %>

                           <%

                            String today="";
                            String fromday="";
                           
                            fromday = NullCheck((String) request.getAttribute("fromdate"));
                            today = NullCheck((String) request.getAttribute("todate"));

                            if(today.equals("") && fromday.equals("")){
                            Date dNow = new Date();
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(dNow);
                            cal.add(Calendar.DATE, 0);
                            dNow = cal.getTime();

                            Date dNow1 = new Date();
                            Calendar cal1 = Calendar.getInstance();
                            cal1.setTime(dNow1);
                            cal1.add(Calendar.DATE, -6);
                            dNow1 = cal1.getTime();

                            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
                            today = ft.format(dNow);
                            fromday = ft.format(dNow1);
                            }

            %>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  style="width:250px;height:40px"  onclick="ressetDate(this);" value="<%=fromday%>"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" onclick="ressetDate(this);" value="<%=today%>"></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                         <td><input type="button" class="btn btn-success"  name="search" onclick="submitPage(this.name);" value="Search"></td>
                                         <td><input type="button" class="btn btn-success"  name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel"></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <br>

            <c:if test = "${financeAdvice != null}" >
                     <table class="table table-info mb30 table-hover" id="table" >
                    <thead>
                        <tr height="40">
                            <th>S.No</th>
                            <th>Advice Date</th>                            
                            <th>Advice Type</th>                            
                            <th>No. of Trips</th>
                            <th>Estimated Amt.</th>
                            <th>Requested Amt.</th>
                            <th>Paid Amt.</th>                            
                            <th>View</th>
                            <th>Finance Details Excel</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${financeAdvice}" var="fd">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr height="30">
                                <td align="left" ><%=sno%></td>
                                <td align="left" ><c:out value="${fd.advicedate}"/> </td>                                

                                <td align="left" >
                                <c:if test="${(fd.batchType=='b') || (fd.batchType=='B')}" >
                                    Batch
                                    </c:if>
                                    <c:if test="${(fd.batchType=='a') || (fd.batchType=='A')}" >
                                    Adhoc
                                    </c:if>
                                    <c:if test="${(fd.batchType=='m') || (fd.batchType=='M')}" >
                                    Manual
                                    </c:if>
                                </td>

                                <td align="left" ><c:out value="${fd.tripday}"/></td>                                
                                <td align="left" >
                                    <c:if test="${(fd.estimatedadvance=='')||(fd.estimatedadvance==null)}" >
                                    0
                                    </c:if>
                                    <c:if test="${(fd.estimatedadvance!='')||(fd.estimatedadvance!=null)}" >
                                    <c:out value="${fd.estimatedadvance}"/>
                                    </c:if>
                                </td>
                                <td align="left" ><c:out value="${fd.requestedadvance}"/></td>
                                <td align="left" ><c:out value="${fd.actualadvancepaid}"/></td>
                                <td align="left" >
                                    <a href="/throttle/viewFinanceReportAdviceDetais.do?dateval=<c:out value="${fd.advicedate}"/>&active=<c:out value="${fd.isactive}"/>&type=<c:out value="${fd.batchType}"/>">view details</a>
                                </td>                                
                                <td align="left" >
                                    <a href="/throttle/handlefinanceAdviceReportExcel.do?dateval=<c:out value="${fd.advicedate}"/>&active=<c:out value="${fd.isactive}"/>&type=<c:out value="${fd.batchType}"/>">Excel Download</a>
                                </td>                                
                                
                            </tr>
                        <%
                                   index++;
                                   sno++;
                        %>
                    </c:forEach>

                    </tbody>
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>

        </form>
    </body>


</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>