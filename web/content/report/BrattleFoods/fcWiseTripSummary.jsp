<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js" type="text/javascript"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
            function submitPage(value) {
                if(value == 'ExportExcel'){
                document.fcWiseTripSummary.action = '/throttle/handleFCWiseTripSummaryReport.do?param=ExportExcel';
                document.fcWiseTripSummary.submit();
                }else{
                document.fcWiseTripSummary.action = '/throttle/handleFCWiseTripSummaryReport.do?param=Search';
                document.fcWiseTripSummary.submit();
                }
            }
            
            function setValue(){
                if('<%=request.getAttribute("page")%>' !='null'){
                var page = '<%=request.getAttribute("page")%>';
                    if(page == 1){
                      submitPage('search');
                    }
                }
            }
            
            
        </script>
<style type="text/css">





.container {width: 960px; margin: 0 auto; overflow: hidden;}
.content {width:800px; margin:0 auto; padding-top:50px;}
.contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

/* STOP ANIMATION */



/* Second Loadin Circle */

.circle1 {
	background-color: rgba(0,0,0,0);
	border:5px solid rgba(100,183,229,0.9);
	opacity:.9;
	border-left:5px solid rgba(0,0,0,0);
/*	border-right:5px solid rgba(0,0,0,0);*/
	border-radius:50px;
	/*box-shadow: 0 0 15px #2187e7; */
/*	box-shadow: 0 0 15px blue;*/
	width:40px;
	height:40px;
	margin:0 auto;
	position:relative;
	top:-50px;
	-moz-animation:spinoffPulse 1s infinite linear;
        -webkit-animation:spinoffPulse 1s infinite linear;
	-ms-animation:spinoffPulse 1s infinite linear;
	-o-animation:spinoffPulse 1s infinite linear;
}

@-moz-keyframes spinoffPulse {
	0% { -moz-transform:rotate(0deg); }
	100% { -moz-transform:rotate(360deg);  }
}
@-webkit-keyframes spinoffPulse {
	0% { -webkit-transform:rotate(0deg); }
	100% { -webkit-transform:rotate(360deg);  }
}
@-ms-keyframes spinoffPulse {
	0% { -ms-transform:rotate(0deg); }
	100% { -ms-transform:rotate(360deg);  }
}
@-o-keyframes spinoffPulse {
	0% { -o-transform:rotate(0deg); }
	100% { -o-transform:rotate(360deg);  }
}



</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.FC Wise Trip Summary" text="FC Wise Trip Summary"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Reports"/></a></li>
            <li class=""><spring:message code="hrms.label.FC Wise Trip Summary" text=" FC Wise Trip Summary"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">    <body onload="setValue();">
        <form name="fcWiseTripSummary" action=""  method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
                                <table class="table table-info mb30 table-hover">
                                    <thead><tr><th colspan="4">FC Wise Trip Summary</th></tr></thead>
                                    <tr>
                                        <td><font color="red">*</font>Trip Start Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker , form-control" style="width:250px;height:40px"" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>Trip End Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker , form-control" style="width:250px;height:40px"" value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="right"><input type="button" class="btn btn-success" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                        <td colspan="3"><input type="button" class="btn btn-success" name="Search"   value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>
                                </table>
            
            <c:if test = "${totalSummaryDetailsNWLoaded == null && totalSummaryDetailsNWEmpty == null}" >
                <center>
                <font color="blue">Please Wait Your Request is Processing</font>
                <div class="container">
                 <div class="content">
                <div class="circle"></div>
                <div class="circle1"></div>
                </div>
            </div>
                </center>
            </c:if>
            <c:if test = "${totalSummaryDetailsNWLoaded != null && totalSummaryDetailsNWEmpty != null}" >
                <table class="table table-info mb30 table-hover sortable" id="table" class="sortable">
                    <thead>
                        <tr height="80">
                            <th><h3></h3></th>
                            <th colspan="3"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;FC wise Operated</h3></th>
                            <th colspan="3"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;FC wise In-Progress</h3></th>
                            <th colspan="3"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;FC wise WFU</h3></th>
                            <th colspan="3"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;FC wise Ended</h3></th>
                            <th colspan="3"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;FC wise Closed</h3></th>
                            <th colspan="3"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;FC wise Settled</h3></th>
                            <th colspan="3"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;FC wise ExtraExpense</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>FC</td>
                            <td>Empty</td>
                            <td>Loaded</td>
                            <td>Total</td>
                            <td>Empty</td>
                            <td>Loaded</td>
                            <td>Total</td>
                            <td>Empty</td>
                            <td>Loaded</td>
                            <td>Total</td>
                            <td>Empty</td>
                            <td>Loaded</td>
                            <td>Total</td>
                            <td>Empty</td>
                            <td>Loaded</td>
                            <td>Total</td>
                            <td>Empty</td>
                            <td>Loaded</td>
                            <td>Total</td>
                            <td>Empty</td>
                            <td>Loaded</td>
                            <td>Total</td>
                        </tr>
                        <tr>
                            <td>NW</td>
                            <td><c:out value="${totalSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${totalSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${totalSummaryDetailsNWEmpty+totalSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${inProgressSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${inProgresstotalSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsNWEmpty+inProgresstotalSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${wfuSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${wfutotalSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${wfuSummaryDetailsNWEmpty+wfutotalSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${endSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${endSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${endSummaryDetailsNWEmpty+endSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${closedSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${closedSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${closedSummaryDetailsNWEmpty+closedSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${settledSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${settledSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${settledSummaryDetailsNWEmpty+settledSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${expSummaryDetailsNWEmptyExp}"/></td>
                            <td><c:out value="${expSummaryDetailsNWLoadedExp}"/></td>
                            <td><c:out value="${expSummaryDetailsNWEmptyExp+expSummaryDetailsNWLoadedExp}"/></td>
                        </tr>
                        <tr>
                            <td>NS</td>
                            <td><c:out value="${totalSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${totalSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${totalSummaryDetailsNSEmpty+totalSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${inProgressSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsNSEmpty+inProgressSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${wfuSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${wfuSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${wfuSummaryDetailsNSEmpty+wfuSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${endSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${endSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${endSummaryDetailsNSEmpty+endSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${closedSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${closedSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${closedSummaryDetailsNSEmpty+closedSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${settledSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${settledSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${settledSummaryDetailsNSEmpty+settledSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${expSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${expSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${expSummaryDetailsNSEmpty+expSummaryDetailsNSLoaded}"/></td>
                        </tr>
                        <tr>
                            <td>Gen</td>
                            <td><c:out value="${totalSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${totalSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${totalSummaryDetailsGNEmpty+totalSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${inProgressSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsGNEmpty+inProgressSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${wfuSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${wfuSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${wfuSummaryDetailsGNEmpty+wfuSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${endSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${endSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${endSummaryDetailsGNEmpty+endSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${closedSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${closedSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${closedSummaryDetailsGNEmpty+closedSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${settledSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${settledSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${settledSummaryDetailsGNEmpty+settledSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${expSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${expSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${expSummaryDetailsGNEmpty+expSummaryDetailsGNLoaded}"/></td>
                        </tr>
                        <tr>
                            <td>Tot</td>
                            <td><c:out value="${totalSummaryDetailsNWEmpty+totalSummaryDetailsNSEmpty+totalSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${totalSummaryDetailsNWLoaded+totalSummaryDetailsNSLoaded+totalSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${totalSummaryDetailsNWEmpty+totalSummaryDetailsNWLoaded+totalSummaryDetailsNSEmpty+totalSummaryDetailsNSLoaded+totalSummaryDetailsGNEmpty+totalSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${inProgressSummaryDetailsNWEmpty+inProgressSummaryDetailsNSEmpty+inProgressSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${inProgresstotalSummaryDetailsNWLoaded+inProgressSummaryDetailsNSLoaded+inProgressSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsNWEmpty+inProgresstotalSummaryDetailsNWLoaded+inProgressSummaryDetailsNSEmpty+inProgressSummaryDetailsNSLoaded+inProgressSummaryDetailsGNEmpty+inProgressSummaryDetailsGNLoaded}"/></td>
                            
                            <td><c:out value="${wfuSummaryDetailsNWEmpty+wfuSummaryDetailsNSEmpty+wfuSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${wfutotalSummaryDetailsNWLoaded+wfuSummaryDetailsNSLoaded+wfuSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${wfuSummaryDetailsNWEmpty+wfutotalSummaryDetailsNWLoaded+wfuSummaryDetailsNSEmpty+wfuSummaryDetailsNSLoaded+wfuSummaryDetailsGNEmpty+wfuSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${endSummaryDetailsNWEmpty+endSummaryDetailsNSEmpty+endSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${endSummaryDetailsNWLoaded+endSummaryDetailsNSLoaded+endSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${endSummaryDetailsNWEmpty+endSummaryDetailsNWLoaded+endSummaryDetailsNSEmpty+endSummaryDetailsNSLoaded+endSummaryDetailsGNEmpty+endSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${closedSummaryDetailsNWEmpty+closedSummaryDetailsNSEmpty+closedSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${closedSummaryDetailsNWLoaded+closedSummaryDetailsNSLoaded+closedSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${closedSummaryDetailsNWEmpty+closedSummaryDetailsNWLoaded+closedSummaryDetailsNSEmpty+closedSummaryDetailsNSLoaded+closedSummaryDetailsGNEmpty+closedSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${settledSummaryDetailsNWEmpty+settledSummaryDetailsNSEmpty+settledSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${settledSummaryDetailsNWLoaded+settledSummaryDetailsNSLoaded+settledSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${settledSummaryDetailsNWEmpty+settledSummaryDetailsNWLoaded+settledSummaryDetailsNSEmpty+settledSummaryDetailsNSLoaded+settledSummaryDetailsGNEmpty+settledSummaryDetailsGNLoaded}"/></td>
                            
                            <td><c:out value="${expSummaryDetailsNWEmptyExp+expSummaryDetailsNSEmpty+expSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${expSummaryDetailsNWLoadedExp+expSummaryDetailsNSLoaded+expSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${expSummaryDetailsNWEmptyExp+expSummaryDetailsNWLoadedExp+expSummaryDetailsNSEmpty+expSummaryDetailsNSLoaded+expSummaryDetailsGNEmpty+expSummaryDetailsGNLoaded}"/></td>
                        </tr>
                    </tbody>
                    
                </table>
            </c:if>
        </form>
       
    </body>    
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>