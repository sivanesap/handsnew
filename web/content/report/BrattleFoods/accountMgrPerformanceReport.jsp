<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%--
    Document   : fcWiseTripSummary
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Throttle
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">


        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function () {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">
            function submitPage(value) {
                if (value == 'ExportExcel') {
                    document.fcWiseTripSummary.action = '/throttle/handleAccountMgrPerformanceReportExcel.do?param=ExportExcel';
                    document.fcWiseTripSummary.submit();
                } else {
                    document.fcWiseTripSummary.action = '/throttle/handleAccountMgrPerformanceReportExcel.do?param=Search';
                    document.fcWiseTripSummary.submit();
                }
            }
        </script>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Account Mgr Performance Summary" text="Account Mgr Performance Summary"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Reports"/></a></li>
                <li class=""><spring:message code="hrms.label.Account Mgr Performance Summary" text=" Account Mgr Performance Summary"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">

                <body onload="sorter.size(50)">
                    <form name="fcWiseTripSummary" method="post">
                        <table class="table table-info mb30 table-hover">
                            <thead><tr><th colspan="4">Account Mgr Performance Summary</th></tr></thead>
                            <tr>
                                <td><font color="red">*</font>From Date</td>
                                <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker , form-control" style="width:250px;height:40px" value="<c:out value="${fromDate}"/>" ></td>
                                <td><font color="red">*</font>To Date</td>
                                <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker , form-control" style="width:250px;height:40px"" value="<c:out value="${toDate}"/>"></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center"><input type="button" class="btn btn-success" name="search" onclick="submitPage(this.name);" value="Search">
                                    <input type="button" class="btn btn-success" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel"></td>
                            </tr>

                        </table>

                        <c:if test="${AccountMgrPerformanceReportDetails != null}">
                            <table  id="table" class="table table-info mb30 table-hover">

                                <thead>
                                    <tr style="height: 80px">
                                        <th><h3> S.No</h3></th>
                                        <th ><h3>Account Manager Name</h3></th>
                                        <th><h3>Total trips</h3></th>
                                        <th><h3>Total Sales</h3></th>
                                        <th><h3>TotalKms</h3></th>
                                        <th><h3>Revenue/km</h3></th>
                                        <th><h3>operational cost/km</h3></th>
                                        <th><h3>Margin/km</h3></th>
                                        <th><h3>WFU Days</h3></th>
                                        <th><h3>Total operational Cost</h3></th>
                                        <th><h3>Loading Turn Around Days</h3></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% int index = 1;%>
                                    <c:set var="marginKm1" value="${0}"/>
                                    <c:set var="marginKm2" value="${0}"/>
                                    <c:set var="count" value="${0}"/>
                                    <c:forEach items="${AccountMgrPerformanceReportDetails}" var="account">
                                        <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                        %>
                                        <c:set var="totaltrips" value="${totaltrips+account.tripNos}"/>
                                        <c:set var="totalrevenue" value="${totalrevenue+account.revenue}"/>
                                        <c:set var="totalkm" value="${totalkm+account.totalkms}"/>
                                        <c:set var="totalwfudays" value="${totalwfudays+(account.wfuHours/24)}"/>
                                        <c:set var="totalCost" value="${totalCost+account.totalCost}"/>
                                        <c:set var="totalloadingTransitHours" value="${totalloadingTransitHours+account.loadingTurnHours}"/>
                                        <c:set var="totalloadingTransitDays" value="${totalloadingTransitDays+(account.loadingTurnHours)/24}"/>
                                        <c:set var="totalRefer" value="${totalRefers+account.totalHms}"/>
                                        <c:set var="count" value="${count+1}"/>
                                        <tr>
                                            <td width="30"  class="text1"><%=index++%></td>
                                            <td  width="30" class="text1"><c:out value="${account.empName}"/></td>
                                            <td  width="30" class="text1" align="right"><c:out value="${account.tripNos}"/></td>
                                            <td  width="30" class="text1" align="right"><c:out value="${account.revenue}"/></td>
                                            <td  width="30" class="text1" align="right"><c:out value="${account.totalkms}"/></td>
                                            <td  width="30" class="text1" align="right">
                                                <c:if test="${account.totalkms > 0.0}">
                                                    <c:set var="marginKm1" value="${account.revenue/account.totalkms}"/>
                                                    <fmt:formatNumber pattern="##0.00" value="${account.revenue/account.totalkms}"/>
                                                </c:if>
                                                <c:if test="${account.totalkms == 0.0}">
                                                    0.00
                                                </c:if>
                                            </td>
                                            <td  width="30" class="text1" align="right">
                                                <c:if test="${account.totalkms > 0.0}">
                                                    <c:set var="marginKm2" value="${account.totalCost/account.totalkms}"/>
                                                    <fmt:formatNumber pattern="##0.00" value="${account.totalCost/account.totalkms}"/>
                                                </c:if>
                                                <c:if test="${account.totalkms == 0.0}">
                                                    0.00
                                                </c:if>
                                            </td>
                                            <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${marginKm1-marginKm2}"/></td>
                                            <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${(account.wfuHours/24)}"/></td>

                                            <td  width="30" class="text1" align="right"><c:out value="${account.totalCost}"/></td>
                                            <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${(account.wfuHours/24)}"/></td>




                                        </tr>

                                        <c:if test="${account.totalkms > 0.0}">
                                            <c:set var="totalrevenueperkm" value="${totalrevenueperkm +(account.revenue/account.totalkms)}"/>
                                        </c:if>
                                        <c:if test="${account.totalkms > 0.0}">

                                            <c:set var="totalcostperkm" value="${totalcostperkm+(account.totalCost/account.totalkms)}"/>
                                        </c:if>
                                        <c:if test="${account.totalkms > 0.0}">

                                            <c:set var="totalmarginperkm" value="${totalmarginperkm+((account.revenue/account.totalkms)-(account.totalCost/account.totalkms))}"/>
                                        </c:if>
                                    </c:forEach>
                                    <tr>  

                                        <td width="30" class="text1"> Grand </td>
                                        <td width="30" class="text1"> Total</td>
                                        <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totaltrips}"/></td>
                                        <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalrevenue}"/></td>
                                        <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalkm}"/></td>
                                        <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalrevenueperkm/count}"/></td>
                                        <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalcostperkm/count}"/></td>
                                        <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalmarginperkm/count}"/></td>
                                        <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalwfudays}"/></td>
                                        <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalCost}"/></td>

                                        <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalloadingTransitDays}"/></td>

                                    </tr>
                                </tbody>

                            </table>


                        </c:if>
                        <p><span style="margin-left: 80px;margin-top:80px"> &nbsp;&nbsp;&nbsp;<font color="red" >Note:Total Operating Cost/km= Diesel for vehicle,Diesel for Reefer,Toll,Mis,Driver Incentive,Challan,Bhatta </font></span> </p>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" >5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50" selected="selected">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 0);
                        </script>

                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>

