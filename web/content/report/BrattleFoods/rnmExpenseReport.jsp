<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
         <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
          <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
            <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
            <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
            <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
          

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">
            function submitPage(value) {
                if (value == 'ExportExcel') {
                    document.fcWiseTripSummary.action = '/throttle/handleRNMExpenseReportExcel.do?param=ExportExcel';
                    document.fcWiseTripSummary.submit();
                } else {
                    document.fcWiseTripSummary.action = '/throttle/handleRNMExpenseReportExcel.do?param=Search';
                    document.fcWiseTripSummary.submit();
                }
            }
        </script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.RNM Spent Analysis Report" text="RNM Spent Analysis Report"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Reports"/></a></li>
            <li class=""><spring:message code="hrms.label.RNM Spent Analysis Report" text=" RNM Spent Analysis Report"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body>
        <form name="fcWiseTripSummary" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp"%>
                                <table class="table table-info mb30 table-hover">
                                    <thead><tr><th colspan="2">RNM Spent Analysis Report</th></tr></thead>
                            <!--        <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                    </tr>   -->
                                    <tr>
                                       <td><font color="red">*</font>Type</td>
                                       <td height="30">
                                           <select id="type" name="type" class="form-control" style="width:250px;height:40px">
                                               <option value="2">Primary</option>
                                               <option value="1">Secondary</option>
                                           </select>
                                       </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center"><input type="button" class="btn btn-success" name="search" onclick="submitPage(this.name);" value="Search">
                                        <input type="button" class="btn btn-success" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel"></td>
                                    </tr>

                                </table>

                     <c:if test="${vehicleList != null }">
                  <table  id="table" class="table table-info mb30 table-hover" >
                    <thead>
                    <tr >
                    <th ><h3> S.No</h3></th>
                    <th style="border-left:1px solid white"><h3>Vehicle no</h3></th>
                    <th colspan="3" align="center" style="border-left:1px solid white"><center><h3><c:out value="${firstmonth}"/></h3></center></th>
                    <th colspan="3" align="center"style="border-left:1px solid white" ><center><h3><c:out value="${secondMonth}"/></h3></center></th>
                    <th colspan="3" align="center" style="border-left:1px solid white" ><center><h3><c:out value="${thirdMonth}"/></h3></center></th>
            
                     
         
<!--                    <th colspan="3" ><h3>Second month</h3></th>
                    <th colspan="3"><h3>Third month</h3></th>-->
                    </tr>
                    <tr style="border-top:1px solid white">
                    <th><h3></h3></th>
                    <th><h3></h3></th>
                    
                    <th style="border-left:1px solid white"><h3>Container</h3></th>
                    <th style="border-left:1px solid white"><h3>Chasis</h3></th>
                    <th style="border-left:1px solid white"><h3>Reffer</h3></th>

                    <th style="border-left:1px solid white"><h3>Container</h3></th>
                    <th style="border-left:1px solid white"><h3>Chasis</h3></th>
                    <th style="border-left:1px solid white"><h3>Reffer</h3></th>

                    <th style="border-left:1px solid white"><h3>Container</h3></th>
                    <th style="border-left:1px solid white"><h3>Chasis</h3></th>
                    <th style="border-left:1px solid white"><h3>Reffer</h3></th>
<!--                    <th><h3>Container</h3></th>
                    <th><h3>Chesis</h3></th>
                    <th><h3>Reffer</h3></th>
                    <th><h3>Container</h3></th>
                    <th><h3>Chesis</h3></th>
                    <th><h3>Reffer</h3></th>-->
                                        </tr>
                    </thead>
                    <tbody>
                   
                   
                        <% int index = 1;%>
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                     
                      
                        <c:forEach items="${vehicleList}" var="vehicle">
                                 <tr>
                                <td width="10" class="text1"><%=index++%></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.regNo}"/></td>
                                 <c:forEach items="${RnMExpenseList1}" var="expense1">
                                      <c:if test="${vehicle.vehicleId == expense1.vehicleId  }">
                                 <td  width="80" class="text1">
                                  <fmt:formatNumber pattern="##0.00" value="${expense1.chasisAmount1}"/>
                                </td>
                                <td width="80" class="text1">
                                  <fmt:formatNumber pattern="##0.00" value="${expense1.referAmount1}"/>
                                </td>
                                <td  width="80" class="text1">
                                   <fmt:formatNumber pattern="##0.00" value="${expense1.containerAmount1}"/>
                                </td>
                                  </c:if>
                                 </c:forEach>
                                    <c:forEach items="${RnMExpenseList2}" var="expense2">
                                    <c:if test="${vehicle.vehicleId == expense2.vehicleId  }">
                                 <td  width="80" class="text1">
                                  <fmt:formatNumber pattern="##0.00" value="${expense2.chasisAmount2}"/>
                                </td>
                                <td  width="80" class="text1">
                                  <fmt:formatNumber pattern="##0.00" value="${expense2.referAmount2}"/>
                                </td>
                                <td  width="80" class="text1">
                                   <fmt:formatNumber pattern="##0.00" value="${expense2.containerAmount2}"/>
                                </td>
                                  </c:if>
                                 </c:forEach>
                                 <c:forEach items="${RnMExpenseList3}" var="expense3">
                                    <c:if test="${vehicle.vehicleId == expense3.vehicleId  }">
                                 <td  width="80" class="text1">
                                  <fmt:formatNumber pattern="##0.00" value="${expense3.chasisAmount3}"/>
                                </td>
                                <td  width="80" class="text1">
                                  <fmt:formatNumber pattern="##0.00" value="${expense3.referAmount3}"/>
                                </td>
                                <td  width="80" class="text1">
                                   <fmt:formatNumber pattern="##0.00" value="${expense3.containerAmount3}"/>
                                </td>
                                  </c:if>
                                 </c:forEach>
                                      
                                            
                            </tr>
                        </c:forEach>
                       
                   
                   

                                          </tbody>
                </table>
                  </c:if>

                
                     
        </form>
    </body>
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

