<%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<html>
    <head>
       <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
          <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
            <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
            <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
            <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">


        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>


        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">
            function submitPage(value){
        
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                }else{
                    if(value == "ExportExcel"){
                        document.BPCLTransaction.action = '/throttle/handleVehicleDriverAdvanceExcel.do?param=ExportExcel';
                        document.BPCLTransaction.submit();
                    }
                    else{   
                        document.BPCLTransaction.action = '/throttle/handleVehicleDriverAdvanceExcel.do?param=Search';
                        document.BPCLTransaction.submit();
                    }
                }
            }
        </script>



    </head>
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.LatestUpdates" text="Latest Updates"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Latest Updates" text="Latest Updates"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
    
    <body onload="sorter.size(1);">
        <form name="BPCLTransaction" method="post">
             
             
 
             <table class="table table-info mb30 table-hover" id="bg" >
		    <thead>
		<tr>
		    <th colspan="4" height="30" >Latest Updates</th>
		</tr>
               </thead>
             
             </table>

            <c:if test="${latestUpdates == null}">
                <center><font color="red">No Records Found</font></center>
            </c:if>
            <c:if test="${latestUpdates != null}">
               <table class="table table-info mb30 table-hover " id="table"  >	
                    <thead>
                        <tr height="50">
                            <th align="center">S.No</th>
                            <th align="center">Status Name</th>
                            <th align="center">Module Name</th>
                            <th align="center">Customer Name</th>
                            <th align="center">Vehicle No</th>
                            <th align="center">Request Amount</th>
                            <th align="center">Paid Amount</th>
                            <th align="center">Route Name</th>
                            <th align="center">Updated By</th>
                            <th align="center">Updated On</th>
                        </tr>
                    </thead>
                            
                    <tbody>
                        <% int index = 1;%>
                        <c:forEach items="${latestUpdates}" var="update">
                            <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                            %>
                            <tr>
                                <td width="2" ><%=index++%></td>
                                <td  width="30" ><c:out value="${update.statusName}"/>&nbsp;</td>
                                <td  width="30" ><c:out value="${update.name}"/>&nbsp;</td>
                                <td  width="30" ><c:out value="${update.customerName}"/>&nbsp;</td>
                                <td width="30" ><c:out value="${update.vehicleNo}"/>&nbsp;</td>
                                <td width="30"  ><c:out value="${update.requestAmount}"/>&nbsp;</td>
                                <td width="30"   ><c:out value="${update.paidAmount}"/>&nbsp;</td>
                                <td width="30"   ><c:out value="${update.routeName}"/>&nbsp;</td>
                                <td width="30"   ><c:out value="${update.createdBy}"/>&nbsp;</td>
                                <td width="30"   ><c:out value="${update.createdDate}"/>&nbsp;</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
           
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
        </form>

    </body>
</div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

