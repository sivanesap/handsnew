<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RunSheet Print</title>
        <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
        <%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
        <%@ taglib uri="http://www.springframework.org/tags/form" prefix="spring" %>
        <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
        <jsp:directive.page import="org.displaytag.sample.*" />
        <jsp:useBean id="now" class="java.util.Date" scope="request" />
        <%@page import="java.text.DecimalFormat"%>
        <%@page import="java.util.ArrayList"%>
    </head>

    <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" language="javascript" src="/throttle/js/qrcode.min.js"></script>
    <script type="text/javascript" language="javascript" src="/throttle/js/qrcode.js"></script>
    <link rel="stylesheet" href="/throttle/css/parcelx.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/throttle/css/rupees.css" type="text/css" media="screen">
    <script type="text/javascript" src="/throttle/js/code39.js"></script>
    <script type="text/javascript">
        function printPage(val)
        {
            var DocumentContainer = document.getElementById(val);
            var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }
        function goBack() {
            document.waybillPrint.action = '/throttle/handleWayBillPaid.do';
            document.waybillPrint.submit();

        }

    </script>
    <style>
        .contentHead{
            table-layout: 10px;
            font-size: 50px;
        }
        @media print {
            div.divFooter {
                position: fixed;
                bottom: 0;
            }
        </style>
        <script>
            function initMap() {
                var service = new google.maps.DirectionsService;
                var start = new google.maps.LatLng("22.81434", "86.18916");
                var mapOptions = {
                    zoom: 7

                };
                var map = new google.maps.Map(document.getElementById('mapdiv'), mapOptions);
                var routePoint = document.getElementsByName("locId");
                var latitude = document.getElementsByName("latitude");
                var longitude = document.getElementsByName("longitude");
                var w = routePoint.length;
                // list of points
                var stations = [];
                var bounds = [];

                bounds.push({
                    lat: "22.81434", lng: "86.18916"
                });

                for (var j = 0; j < w; j++) {

                    stations.push({
                        lat: latitude[j].value, lng: longitude[j].value, name: routePoint[j].value
                    });
                    bounds.push({
                        lat: latitude[j].value, lng: longitude[j].value
                    });
                }

                // Zoom and center map automatically by stations (each station will be in visible map area)
                var lngs = stations.map(function(station) {
                    return station.lng;
                });
                var lats = stations.map(function(station) {
                    return station.lat;
                });
                var lngss = bounds.map(function(station) {
                    return station.lng;
                });
                var latss = bounds.map(function(station) {
                    return station.lat;
                });

                map.fitBounds({
                    west: Math.min.apply(null, lngss),
                    east: Math.max.apply(null, lngss),
                    north: Math.min.apply(null, latss),
                    south: Math.max.apply(null, latss),
                });

                var marker = start;
                var image = '/throttle/images/warehouse.png';
                var beachMarker = new google.maps.Marker({
                    map: map,
                    position: marker,
                    animation: google.maps.Animation.DROP,
                    title: "WAREHOUSE",
                    icon: image

                });
                var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                var labelIndex = 0;
                // Show stations on the map as markers
                for (var i = 0; i < stations.length; i++) {

                    var marker = new google.maps.LatLng(stations[i].lat, stations[i].lng);

                    //            image = '/throttle/images/wowPIC.jpg';

                    var image = '/throttle/images/truck.png';
                    var beachMarker = new google.maps.Marker({
                        map: map,
                        position: marker,
                        animation: google.maps.Animation.DROP,
                        title: stations[i].name,
                        label: labels[labelIndex++ % labels.length]
                    });
                }

                // Divide route to several parts because max stations limit is 25 (23 waypoints + 1 origin + 1 destination)
                for (var i = 0, parts = [], max = 25 - 1; i < stations.length; i = i + max)
                    parts.push(stations.slice(i, i + max + 1));
                // Service callback to process service results
                var service_callback = function(response, status) {
                    if (status != 'OK') {
                        console.log('Directions request failed due to  ' + status);
                        return;
                    }
                    var renderer = new google.maps.DirectionsRenderer;
                    renderer.setMap(map);
                    renderer.setOptions({suppressMarkers: true, preserveViewport: true, polylineOptions: {
                            strokeColor: 'black', strokeWeight: 3, strokeOpacity: 1.0
                        }});
                    renderer.setDirections(response);
                    //            computeTotalDistance(response);

                };

                for (var i = 0; i < parts.length; i++) {
                    // Waypoints does not include first station (origin) and last station (destination)
                    var waypoints = [];
                    for (var j = 0; j < parts[i].length - 1; j++)
                        //                waypoints.push({location: parts[i][j], stopover: false});
                        waypoints.push({location: new google.maps.LatLng(parts[i][j].lat, parts[i][j].lng), stopover: false});
                    // Service options
                    var service_options = {
                        origin: start,
                        destination: new google.maps.LatLng(parts[i][parts[i].length - 1].lat, parts[i][parts[i].length - 1].lng),
                        waypoints: waypoints,
                        optimizeWaypoints: true,
                        travelMode: 'WALKING'
                    };

                    // Send request
                    service.route(service_options, service_callback);

                }


            }



        </script>

    </head>
    <body>
        <%--@ include file="/content/common/path.jsp" --%>
        <center>
            <%-- @include file="/content/common/message.jsp" --%>
        </center>
        <form name="waybillPrint">
            <!--<div class="divFooter">UNCLASSIFIED</div>-->
            <input type="hidden" name="wayBillMode" id="wayBillMode" value="4"/>
            <input type="hidden" class="textbox" name="fromLocation" id="fromLocation" value="<%= session.getAttribute("branchId")%>"/>
            <% String runSheetRefNo = "";%>
            <c:if test = "${getTripFkRunsheetList!= null}" >
                <c:set var="totalWeight" value="${0}" />
                <c:set var="tripType" value="${0}" />
                <c:forEach items="${getTripFkRunsheetList}" var="runsheet">
                    <c:set var="vehicleNo" value="${runsheet.vehicleNo}"/>
                    <c:set var="tripType" value="${runsheet.tripType}"/>
                    <c:set var="tripCode" value="${runsheet.tripCode}"/>
                    <c:set var="vehicleCBM" value="${runsheet.vehicleCBM}"/>
                    <c:set var="vendorName" value="${runsheet.vendorName}"/>
                    <c:set var="vehicleType" value="${runsheet.vehicleType}"/>
                    <c:set var="vehicleCapacity" value="${runsheet.vehicleCapacity}"/>
                    <c:set var="startKm" value="${runsheet.startKM}"/>
                    <c:set var="endKm" value="${runsheet.endKm}"/>
                    <c:set var="tripStartDate" value="${runsheet.tripStartDate}"/>
                    <c:set var="totalWeight" value="${totalWeight+runsheet.packageWeight}" />
                    <c:set var="totalQty" value="${totalQty+runsheet.noOfPackages}" />


                </c:forEach>
            </c:if>
            <div id="printDiv" >
                <div style="font-size:25px;" align="center">
                    <c:forEach items="${warehouseDetails}" var="wh">
                        <u>Flipkart Runsheet List</u><br>
                        <br><font size="4px"><b><c:out value="${wh.whName}"/><br><br>GST NO: </b><u><c:out value="${wh.whGstNo}"/></u></font>
                            </c:forEach>
                </div>
                <br> 
                <div>
                    <table cellspacing="0" width="85%" align="center" style="font-family:Verdana, Helvetica, sans-serif;">
                        <tr align="center">
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr style="height: 30px">
                            <td style="font-size:13px;"><b>Tranporter:&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${vendorName}"/></b></td>                        

                            <td style="font-size:13px;" ><b>Scheduled Date:&nbsp;&nbsp;&nbsp;<c:out value="${tripStartDate}"/></b></td>                        
                            <td style="font-size:13px;"><b>RunSheet No:&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${tripCode}"/></b></td>                        
                            <td style="font-size:13px;" align="center">
                                <div id="qrcode"></div>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:13px;"><b>Vehicle No:&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${vehicleNo}"/></b></td>                        
                            <td style="font-size:13px;"><b>Start Km:&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${startKm}"/></b></td>                        
                            <td style="font-size:13px;"><b>End Km:&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${endKm}"/></b></td>                        
                            <td></td>
                        </tr>
                    </table>    
                </div>
                <br>
                <table cellspacing="0" width="95%" align="center" style="border: 0.75pt solid #000000; border-collapse: collapse;font-family:Verdana, Helvetica, sans-serif;">
                    <c:if test = "${getTripFkRunsheetList != null}">
                        <thead>
                            <tr height="40" style="border-bottom: 1px solid #000;">
                                <th style="font-size:12px;width: 5%;">S.No</th>
                                <th style="font-size:12px;width: 15%;">Shipment Id</th>
                                <th style="font-size:12px;width: 15%;">RunSheet No</th>
                                <th style="font-size:12px;width: 15%;">Customer Name</th>
                                <th style="font-size:12px;width: 15%;">Shipment Type</th>
                                <th style="font-size:12px;width: 15%;">Order Type</th>
                                <th style="font-size:12px;width: 18%;">City</th>                            
                                <th style="font-size:12px;width: 18%;">Pincode</th>                            
                                <th style="font-size:12px;width: 7%;">Product</th>
                                <th style="font-size:12px; width: 5%;">Amount</th>                            
                            </tr>
                        </thead>
                        <tbody>
                            <% 
                                int index = 0, sno = 1;                                   
                                long cftWeight = 0;
                                long totart = 0;String tripcd=""; String tripcdPrev="";
                                String pickup="";
                                String drop="";
                                String pickupAddress="";                            
                                String deliveryAddress="";
                            
                                DecimalFormat df=new DecimalFormat("0"); 
                            %>
                            <c:forEach items="${getTripFkRunsheetList}" var="runsheet">

                                <c:set var="trp" value="${runsheet.consignmentOrderNo}"/>

                                <tr><td colspan="9"></td></tr>
                                <tr>                                
                                    <td align="center" style="font-size:12px; width: 5%;"><%=sno%></td>
                                    <td align="center" style="font-size:12px; width: 5%;"><b><c:out value="${runsheet.consignmentOrderNo}"/> </b></td>
                                    <td align="center" style="font-size:12px; width: 5%;">
                                        <b>
                                            <c:out value="${runsheet.tripCode}"/><br>
                                        </b></td>
                                    <td align="center" style="font-size:12px; width: 5%;"><b><c:out value="${runsheet.customerName}"/> </b></td>
                                    <td align="center" style="font-size:12px; width: 5%;"><b><c:out value="${runsheet.tripType}"/> </b></td>
                                        <td align="center" style="font-size:12px; width: 5%;" ><c:if test="${runsheet.orderType == 1}">
                                        Forward
                                    </c:if>
                               <c:if test="${runsheet.orderType == 2}">
                                       Prexo
                                    </c:if>
                                <c:if test="${runsheet.orderType == 3}">
                                       RVP
                                    </c:if>
                                </td>
                                    <td align="center" style="font-size:12px; width: 5%;">
                                        <c:out value="${runsheet.point2Name}"/>
                                    </td>                               
                                    <td align="center" style="font-size:12px; width: 5%;">
                                        <c:out value="${runsheet.pincode}"/>
                                    </td>                               

                                    <td align="center" style="font-size:12px; width: 5%;"><c:out value="${runsheet.articleName}"/><br><br></td>
                                    <td align="center" style="font-size:12px;"><c:out value="${runsheet.noOfPackages}"/></td>

                                    <%sno++;%>

                                </tr>

                                <% 
                               
                                   index++;
                                %>
                            </c:forEach>
                            <tr><td colspan="9"></td></tr>
                            <tr style="border-top: 1px solid #000;">
                                <td colspan="8"  align="right"><b>Total:&emsp;&emsp;</b></td>                                
                                <td align="center"><b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalQty}" /></b></td>  
                            </tr>
                            <tr style="border-top: 1px solid #000;"><td colspan="9"></td></tr>
                            <!--                        <tr height="25"><td colspan="9">
                            <%--<c:if test = "${mapRunsheetList!= null}" >--%>
                                <center>
                                    <div id="mapdiv"  style="width:1200px; height:400px;alignment-adjust: center;"></div>
                                </center>
                            <%--</c:if>--%>
                        </td></tr>-->
                        </tbody>    
                    </c:if>   
                </table>
                <br>


                <%--c:if test = "${mapRunsheetList != null}">
                    <c:forEach items="${mapRunsheetList}" var="cnl">
                        <% 
                          int index = 0;                               
                        %>
                        <input type="hidden" style="width: 184px;" name="latitude" id="latitude<%=index%>" class="form-control" style="width:250px;height:40px"   value="<c:out value="${cnl.latitude}"/>"  readOnly maxlength="50">
                        <input type="hidden" style="width: 184px;" name="longitude" id="longitude<%=index%>"   value="<c:out value="${cnl.longtitude}"/>" readOnly class="form-control" style="width:250px;height:40px" >
                        <input type="hidden" style="width: 184px;" name="locId" id="locId<%=index%>"   value="<c:out value="${cnl.point1Name}"/>" readOnly class="form-control" style="width:250px;height:40px" >
                        <% index++;%>
                    </c:forEach>
                </c:if>--%>


            </div>

            <div id="divFooter">This is system generated document. No signature required</div>
            <br>
            <center>
                <input type="button" class="button" name="ok" value="Print" onClick="printPage('printDiv');" > &nbsp;&nbsp;&nbsp;
                <!--<input type="button" class="button" name="back" value="Back" onClick="goBack();" > &nbsp;-->
            </center>
            <br>

            <script>
                var qrcode = new QRCode(document.getElementById("qrcode"), {
                    text: "TripList",
                    width: 100,
                    height: 100,
                    colorDark: "#000000",
                    colorLight: "#ffffff",
                    correctLevel: QRCode.CorrectLevel.H
                });
            </script>

        </form>



    </body>
    <script async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&callback=initMap"></script>
</html>









