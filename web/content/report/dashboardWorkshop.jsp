<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %>


<!--<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>-->

<!-- Data from www.netmarketshare.com. Select Browsers => Desktop share by version. Download as tsv. -->
<pre id="tsv" style="display:none"></pre>


<div class="pageheader">
    <h2><i class="fa fa-home"></i> Dashboard <span>Workshop</span></h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li>Dashboard</li>
            <li class="active">Workshop</li>
        </ol>
    </div>
</div>

<div class="contentpanel">

    <div class="row">

        <div class="col-sm-6 col-md-3">
            <div class="panel panel-success panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">

                                <i class="ion ion-android-bus"></i>
                            </div>
                            <div class="col-xs-8">
                                <!--                    <small class="stat-label">Truck Utilisation</small>-->
                                <h4> <spring:message code="dashboard.label.TruckOpenJobcard"  text="JobCardOpen"/></h4>
                                <h1><span id="totalTruckNosSpan"></span></h1>
                            </div>
                        </div><!-- row -->

                        <div class="mb15"></div>

                        <!--                <div class="row">
                                          <div class="col-xs-6">
                                            <small class="stat-label">Pages / Visit</small>
                                            <h4>7.80</h4>
                                          </div>
                        
                                          <div class="col-xs-6">
                                            <small class="stat-label">% New Visits</small>
                                            <h4>76.43%</h4>
                                          </div>
                                        </div> row -->
                    </div><!-- stat -->

                </div><!-- panel-heading -->
            </div><!-- panel -->
        </div><!-- col-sm-6 -->



        <div class="col-sm-6 col-md-3">
            <div class="panel panel-danger panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="ion ion-person-stalker"></i>
                            </div>
                            <div class="col-xs-8">
                                <h4><spring:message code="dashboard.label.NoOfTechnician"  text="Technicians"/></h4>
                                <h1><span id="totalDriversNosSpan"></span></h1>
                            </div>
                        </div><!-- row -->

                        <div class="mb15"></div>

                    </div><!-- stat -->

                </div><!-- panel-heading -->
            </div><!-- panel -->
        </div><!-- col-sm-6 -->


    </div><!-- row -->



    <table>
        <tr>
            <td>
                <div class="col-sm-8 col-md-9">
                    <div class="panel panel-default">

                        <div class="outer1">
                            <div class="panel-btns">
                                <a href="" class="panel-close">&times;</a>
                                <a href="" class="minimize">&minus;</a>
                            </div><!-- panel-btns -->
                            <h5 class="panel-title"><spring:message code="dashboard.label.TruckJobcardMTD"  text="Truck"/></h5>
                            <div id="containerFleetUtilisation1" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                        </div>
                        <!--            <div class="panel-body">


                                        <div class="col-sm-8">

                                        </div>
                                  </div>-->
                    </div>
                </div>
            </td>
        </tr>




    </table>
    <!-- contentpanel -->





</div><!-- mainpanel -->


<%@ include file="../common/NewDesign/settings.jsp" %>
<script>
    loadTrucknos();
    function loadTrucknos() {
//           alert("ben Here");
        $.ajax({
            url: '/throttle/getDashBoardWorkShop.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    if (value.Name == 'TruckName') {
                        $("#totalTruckNosSpan").text(value.Count);
                    } else if (value.Name == 'TechnicianNo') {
                        $("#totalDriversNosSpan").text(value.Count);
                    }



                });
            }
        });

    }
</script>


<style>
    .outer {
        width: 600px;
        color: navy;
        background-color: #1caf9a;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer1 {
        width: 600px;
        color: navy;
        background-color: #428bca;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer2 {
        width: 600px;
        color: navy;
        background-color: #a94442;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer3 {
        width: 600px;
        color: navy;
        background-color: #f0ad4e;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer4 {
        width: 1210px;
        color: navy;
        background-color: pink;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .ben{
        width: 600px;
    }
</style>
<script>

    getTruckJobCardMTD();
    function getTruckJobCardMTD() {
        var x_values = [];
        var x_values_sub = {};
        $.ajax({
            url: '/throttle/getTruckJobCardMTD.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    x_values_sub['name'] = value.Make;
                    x_values_sub['y'] = parseInt(value.Count);
                    x_values.push(x_values_sub);
                    x_values_sub = {};


                });
//                alert(vehicleCountArray)
                $('#containerFleetUtilisation1').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Truck Jobcard MTD'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y}',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });
    }
</script>


<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>





