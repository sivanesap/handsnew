<%--
Document   : RunSheetWaybillPrint
Created on : Mar 27, 2019 :44 PM
Author     : hp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">



<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Run Sheet Waybill Print Print</title>
        <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
        <%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
        <%@ taglib uri="http://www.springframework.org/tags/form" prefix="spring" %>
        <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
        <jsp:directive.page import="org.displaytag.sample.*" />
        <jsp:useBean id="now" class="java.util.Date" scope="request" />
        <%@page import="java.text.DecimalFormat"%>
        <%@page import="java.util.ArrayList"%>
    </head>

    <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
    <link rel="stylesheet" href="/throttle/css/parcelx.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/throttle/css/rupees.css" type="text/css" media="screen">
    <script type="text/javascript" src="/throttle/js/code39.js"></script>
    <script type="text/javascript">
        function printPage(val)
        {
            var DocumentContainer = document.getElementById(val);
            var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }
        function goBack() {
            document.waybillPrint.action = '/throttle/handleWayBillPaid.do';
            document.waybillPrint.submit();

        }

    </script>
    <style>
        .contentHead{
           table-layout: 10px;
           font-size: 50px;
        }
    </style>
        

</head>
<body>
    <%--@ include file="/content/common/path.jsp" --%>
    <center>
        <%-- @include file="/content/common/message.jsp" --%>
    </center>
    <form name="waybillPrint">
        <input type="hidden" name="wayBillMode" id="wayBillMode" value="4"/>
        <input type="hidden" class="textbox" name="fromLocation" id="fromLocation" value="<%= session.getAttribute("branchId")%>"/>
        <% String runSheetRefNo = "";%>
        <c:if test = "${runsheetprintList!= null}" >
            <c:set var="totalWeight" value="${0}" />
            <c:forEach items="${runsheetprintList}" var="runsheet">
                <c:set var="vehicleNo" value="${runsheet.vehicleNo}"/>
                <c:set var="tripStartDate" value="${runsheet.tripStartDate}"/>
                <c:set var="totalWeight" value="${totalWeight+runsheet.packageWeight}" />
            </c:forEach>
                </c:if>
                <div id="printDiv" >
                   <div style="font-size:35px;" align="center">
                       Runsheet Print
                   </div>
                    <br>
                   <div>
                                    <table>
                                       <tr style="height: 30px">
                                            <td style="font-size:25px;"><b>Vehicle No</b></td>
                                            <td style="font-size:20px;"> : <c:out value="${vehicleNo}"/></td>
                                        </tr>
                                        <tr style="height: 30px">
                                            <td style="font-size:25px;"><b>Scheduled Date</b></td>
                                            <td style="font-size:20px;">:<c:out value="${tripStartDate}"/> </td>
                                        </tr>
                                        <tr style="height: 30px">
                                            <td style="font-size:25px;"><b>Total weight</b></td>
                                            <td style="font-size:20px;">:<c:out value="${totalWeight}"/></td>
                                        </tr>
<!--                                        <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>Starting Time: </b></td>
                                            <td style="font-size:10px;" align="center"><b>&emsp;&emsp;&emsp;&emsp;Closing Time: </b></td>
                                        </tr>-->
                                        

                                    </table>    
                               </div>
                                        <br>
                    <table class="contentHead" style="margin: 0px;font-family: Arial, Helvetica, sans-serif;width: 100%;"  align="center"  cellpadding="0"  cellspacing="0" border>
                        <c:if test = "${runsheetprintList != null}">
                            <thead>
                                <tr height="40">
                                    <th style="font-size:15px;width: 5%;">S.No</th>
                                    <th style="font-size:15px;width: 15%;">Consignment Order No</th>
                                    <th style="font-size:15px;width: 15%;">TripCode / Document No</th>
                                    <th style="font-size:15px;width: 18%;">Pickup</th>
                                    <th style="font-size:15px;width: 18%;">Delivery</th>
                                    <th style="font-size:15px;width: 7%;">Article Code</th>
                                    <th style="font-size:15px;width: 7%;">Article Name</th>
                                    <th style="font-size:15px; width: 5%;">No of package</th>
                                    <th style="font-size:15px; width: 5%;">Package weight</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% 
                                    int index = 0, sno = 1;                                   
                                    long cftWeight = 0;
                                    long totart = 0;
                                    String tripPts="";
                                    String pickup="";
                                    String pickupAddress="";
                                    String delivery="";
                                    String deliveryAddress="";
                                    String tripPtsTemp[]=null;
                                    String trpTemp[]=null;
                                    DecimalFormat df=new DecimalFormat("0"); 
                                %>
                                <c:forEach items="${runsheetprintList}" var="runsheet">
                                    <c:set var="tripPoints" value="${runsheet.points}"/>
                                    <tr height="30" >
                                        <td align="center" style="font-size:15px;"><%=sno%></td>
                                        <td align="center" style="font-size:15px;"><b><c:out value="${runsheet.consignmentOrderNo}"/> </b></td>
                                        <td align="center" style="font-size:15px;"><b><c:out value="${runsheet.tripCode}"/> </b></td>
                                        <% 
                                            
                                            ArrayList runsheetprintList = (ArrayList) request.getAttribute("runsheetprintList");
                                             System.out.println("runsheetList.size()AAA"+runsheetprintList.size());  
                                             tripPts = (String) pageContext.getAttribute("tripPoints");
                                             tripPtsTemp = tripPts.split(";");
                                             trpTemp = tripPtsTemp[0].split("~");
                                             pickup= trpTemp[0];
                                             pickupAddress= trpTemp[1];
                                             
                                             trpTemp = tripPtsTemp[1].split("~");
                                             delivery= trpTemp[0];
                                             deliveryAddress= trpTemp[1];
                                             
                                        %>
                                            <td align="center" style="font-size:15px;">
                                                <%= pickup%> <br>
                                                 <%= pickupAddress%> 
                                            </td>
                                            <td align="center" style="font-size:15px;">
                                                <%= delivery%><br>
                                                <%= deliveryAddress%>
                                            </td>
                                        <td align="center" style="font-size:15px;"><b><c:out value="${runsheet.articleId}"/></b> </td>
                                        <td align="center" style="font-size:15px;"><b><c:out value="${runsheet.articleName}"/></b> </td>
                                        <td align="center" style="font-size:18px;"><b><c:out value="${runsheet.noOfPackages}"/></b> </td>
                                        <td align="center" style="font-size:18px;"><b><c:out value="${runsheet.packageWeight}"/></b> </td>
                                    </tr>
                                    
                                    <%sno++;%>
                                </c:forEach>
                                    
                            </tbody>    
                        </c:if>   
                    </table>
                </div>
           
            <br>
            <center>
                <input type="button" class="button" name="ok" value="Print" onClick="printPage('printDiv');" > &nbsp;&nbsp;&nbsp;
                <!--<input type="button" class="button" name="back" value="Back" onClick="goBack();" > &nbsp;-->
            </center>
            <br>
            <br>
        
    </form>
<!--    <script type="text/javascript">//
//        /* <![CDATA[ */
//        function get_object(id) {
//            var object = null;
//            if (document.layers) {
//                object = document.layers[id];
//            } else if (document.all) {
//                object = document.all[id];
//            } else if (document.getElementById) {
//                object = document.getElementById(id);
//            }
//            return object;
//        }
//        //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
//        get_object("inputdata").innerHTML = DrawCode39Barcode('<%=runSheetRefNo%>', 0);
//        /* ]]> */
//    </script>-->
</body>
</html>









