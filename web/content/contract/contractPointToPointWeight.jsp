<%-- 
    Document   : contractPointToPointWeight
    Created on : Jan 27, 2015, 8:35:48 PM
    Author     : Nivan
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>

<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script> 

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Customer Contract Master" text="Customer Contract Master"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Customer" text="Customer"/></a></li>
            <li class=""><spring:message code="hrms.label.Customer Contract Master" text="Customer Contract Master"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>


                <form name="customerContract"  method="post" >

                    <br>
                    <%@ include file="/content/common/message.jsp" %>
                    <br>

                    <table  align="center"  class="table table-info mb30 table-hover" id="bg">
                        <thead>
                            <tr>
                                <th colspan="4" height="30" >Customer Contract Master</th>
                            </tr>
                        </thead>
                        <!--                        <tr>
                                                    <td class="contenthead" colspan="4" >Customer Contract Master</td>
                                                </tr>-->

                        <tr>
                            <td >Customer Name</td>
                            <td ><input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>" class="form-control" style="width:240px;height:40px"><input type="text" name="customerName" id="customerName" value="<c:out value="${customerName}"/>" class="form-control" style="width:240px;height:40px" readonly=""></td>
                            <td >Customer Code</td>
                            <td ><input type="text" name="customerCode" id="customerCode" value="<c:out value="${customerCode}"/>" class="form-control" style="width:240px;height:40px" readonly=""></td>
                        </tr>
                        <tr>
                            <td >Contract From</td>
                            <td ><input type="text" name="contractFrom" readonly id="contractFrom" value="" class="datepicker form-control" style="width:240px;height:40px"></td>
                            <td >Contract To</td>
                            <td ><input type="text" name="contractTo" readonly id="contractTo" value="" class="datepicker form-control" style="width:240px;height:40px"></td>
                        </tr>
                        <tr>
                            <td >Contract No</td>
                            <td ><input type="text" name="contractNo" id="contractNo" value="" class="form-control" style="width:240px;height:40px" ></td>
                            <td >Billing Type</td>
                            <td ><input type="hidden" name="billingTypeId" id="billingTypeId" value="<c:out value="${billingTypeId}"/>"/>
                                <c:if test="${billingTypeList != null}">
                                    <c:forEach items="${billingTypeList}" var="btl">
                                        <c:if test="${billingTypeId == btl.billingTypeId}">
                                            <c:out value="${btl.billingTypeName}"/>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                            </td>
                        </tr>
                    </table>
                    <br>

                    <div id="tabs">
                        <ul class="nav nav-tabs">
                            <!--<li class="active" data-toggle="tab"><a href="#fullTruck"><span>Full Truck</span></a></li>-->
                            <li  class="active" data-toggle="tab"><a href="#weightBreak"><span>Weight Break </span></a></li>
                        </ul>





                        <div id="fullTruck" style="display:none">
                            <div id="mainFullTruck" class="contenthead2" style="width: 72%">
                                <input class="btn btn-success"  type="button" id="btAdd" value="Add Route" class="bt" />
                                <input class="btn btn-success" type="button" id="btRemove" value="Remove Route" class="bt" />
                            </div>

                            <script>
                                var container = "";
                                $(document).ready(function() {
                                    var iCnt = 1;
                                    var rowCnt = 1;
                                    // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                    container = $($("#routeFullTruck")).css({
                                        padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                        borderTopColor: '#999', borderBottomColor: '#999',
                                        borderLeftColor: '#999', borderRightColor: '#999'
                                    });
                                    $(container).last().after('<table id="mainTableFullTruck" ><tr></td>\
                                    <table   id="routeDetails' + iCnt + '"  border="1">\n\
                                    <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                    <td>Sno</td>\n\
                                    <td>Origin</td>\n\
                                    <td>Destination</td>\n\
                                    <td>Travel Km</td>\n\
                                    <td>Travel Hour</td>\n\
                                    <td>Travel Min</td></tr>\n\
                                    <tr><td>Route-' + iCnt + rowCnt + '</td>\n\
                                    <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" />\n\
                                    <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                                    <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/>\n\
                                    <input type="hidden" name="routeIdFullTruck" id="routeIdFullTruck' + iCnt + '" value="" /></td>\n\
                                    <td><input type="text" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                                    </tr>\n\
                                    </table>\n\
                                    <table  id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1"  width="73%">\n\
                                    <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                    <td>Sno</td><td>Movement Type</td><td>Vehicle Type</td><td>Rate With Reefer</td><td>Rate Without Reefer</td></tr>\n\
                                    <tr>\n\
                                    <td>' + rowCnt + '</td>\n\
                                    <td><select name="movementTypeIdFullTruck" id="movementTypeIdFullTruck' + iCnt + '" class="textbox" style="height:22px;width:160px;"><option value="0">--Select--</option><option value="1">Bonded</option><option value="2">NonBonded</option></select></td>\n\
                                    <td><select ype="text" name="vehicleTypeIdFullTruck" id="vehicleTypeIdFullTruck' + iCnt + '" style="height:22px;width:160px;"><option value="0">--Select--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td>\n\
                                    <td><input type="text" name="rateWithReeferFullTruck" id="rateWithReeferFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="rateWithOutReeferFullTruck" id="rateWithOutReeferFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                    </tr></table>\n\
                                    <table border="0" ><tr>\n\
                                    <td><input class="btn btn-success" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>\n\
                                    <input class="btn btn-success" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRow(' + iCnt + rowCnt + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></td>\n\
                                    </tr></table></td></tr></table><br><br>');
                                    callOriginAjaxFullTruck(iCnt);
                                    callDestinationAjaxFullTruck(iCnt);
                                    $('#btAdd').click(function() {
                                        iCnt = iCnt + 1;
                                        $(container).last().after('<table id="mainTableFullTruck" ><tr><td>\
                                    <table   id="routeDetailsFullTruck' + iCnt + '" border="1" width="100%">\n\
                                   <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px">\n\
                                    <td>Sno</td>\n\
                                    <td>Origin</td>\n\
                                    <td>Destination</td>\n\
                                    <td>Travel Km</td>\n\
                                    <td>Travel Hour</td>\n\
                                    <td>Travel Min</td></tr>\n\
                                    <td>Route-' + iCnt + rowCnt + '</td>\n\
                                    <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" />\n\
                                    <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                                    <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/>\n\
                                     <input type="hidden" name="routeIdFullTruck" id="routeIdFullTruck' + iCnt + '" value="" /></td>\n\
                                     <td><input type="text" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                                    </tr>\n\
                                    </table>\n\
                                    <table  id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1" width="100%">\n\
                                    <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                    <td>Sno</td><td>Movement Type</td><td>Vehicle Type</td><td>Rate With Reefer</td><td>Rate Without Reefer</td></tr>\n\
                                    <tr>\n\
                                    <td>' + rowCnt + '</td>\n\
                                    <td><select name="movementTypeIdFullTruck" id="movementTypeIdFullTruck' + iCnt + '" class="textbox" style="height:22px;width:160px;"><option value="0">--Select--</option><option value="1">Bonded</option><option value="2">NonBonded</option></select></td>\n\
                                    <td><select ype="text" name="vehicleTypeIdFullTruck" id="vehicleTypeIdFullTruck' + iCnt + '" style="height:22px;width:160px;"><option value="0">--Select--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td>\n\
                                    <td><input type="text" name="rateWithReeferFullTruck" id="rateWithReeferFullTruck' + iCnt + '" value="" style="height:22px;width:160px;" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                    <td><input type="text" name="rateWithOutReeferFullTruck" id="rateWithOutReeferFullTruck' + iCnt + '" value="" style="height:22px;width:160px;" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                    </tr></table>\n\
                                    <table border="0" ><tr>\n\
                                    <td><input class="btn btn-success"  type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>\n\
                                    <input class="btn btn-success"  type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove" onclick="deleteRow(' + iCnt + rowCnt + ')"  style="width:70px;height:30px;font-weight: bold;padding:1px;"/></td>\n\
                                    </tr></table></td></tr></table><br><br>');
                                        callOriginAjaxFullTruck(iCnt);
                                        callDestinationAjaxFullTruck(iCnt);
                                        $('#mainFullTruck').after(container);
                                    });
                                    $('#btRemove').click(function() {
                                        alert($('#mainTableFullTruck tr').size());
                                        if ($(container).size() > 1) {
                                            $(container).last().remove();
                                            iCnt = iCnt - 1;
                                        }
                                    });
                                });

                                // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                var divValue, values = '';
                                function GetTextValue() {
                                    $(divValue).empty();
                                    $(divValue).remove();
                                    values = '';
                                    $('.input').each(function() {
                                        divValue = $(document.createElement('div')).css({
                                            padding: '5px', width: '200px'
                                        });
                                        values += this.value + '<br />'
                                    });
                                    $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                    $('body').append(divValue);
                                }

                                function addRow(val) {
                                    //alert(val);
                                    var loadCnt = val;
                                    var routeInfoSize = $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size();
                                    var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                    var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                    $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSize + '</td><td><select name="movementTypeIdFullTruck" id="movementTypeIdFullTruck' + loadCnt + '" class="textbox" style="height:22px;width:160px;"><option value="0">--Select--</option><option value="1">Bonded</option><option value="2">NonBonded</option></select></td><td><select type="text" name="vehicleTypeIdFullTruck" id="vehicleTypeIdFullTruck' + loadCnt + '" style="height:22px;width:160px;"><option value="0">--Select--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td><td><input type="text" name="rateWithReeferFullTruck" id="rateWithReeferFullTruck' + loadCnt + '" value="" style="height:22px;width:160px;"/></td><td><input type="text" name="rateWithOutReeferFullTruck" id="rateWithOutReeferFullTruck' + loadCnt + '" value="" style="height:22px;width:160px;"/></td></tr>');
                                    loadCnt++;
                                }
                                function deleteRow(val) {
                                    var loadCnt = val;
                                    var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                    var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                    if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                        $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                        loadCnt = loadCnt - 1;
                                    } else {
                                        alert('One row should be present in table');
                                    }
                                }
                                    </script>

                                    <script>
                                        function callOriginAjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //alert(val);
                                            var pointNameId = 'originNameFullTruck' + val;
                                            var pointIdId = 'originIdFullTruck' + val;
                                            var desPointName = 'destinationNameFullTruck' + val;


                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getTruckCityList.do",
                                                        dataType: "json",
                                                        data: {
                                                            cityName: request.term,
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                        },
                                                        error: function(data, type) {

                                                            //console.log(type);
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Name;
                                                    var id = ui.item.Id;
                                                    //alert(id+" : "+value);
                                                    $('#' + pointIdId).val(id);
                                                    $('#' + pointNameId).val(value);
                                                    $('#' + desPointName).focus();
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                var itemVal = item.Name;
                                                itemVal = '<font color="green">' + itemVal + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + itemVal + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }

                                        function callDestinationAjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //alert(val);
                                            var pointNameId = 'destinationNameFullTruck' + val;
                                            var pointIdId = 'destinationIdFullTruck' + val;
                                            var originPointId = 'originIdFullTruck' + val;
                                            var truckRouteId = 'routeIdFullTruck' + val;
                                            var travelKm = 'travelKmFullTruck' + val;
                                            var travelHour = 'travelHourFullTruck' + val;
                                            var travelMinute = 'travelMinuteFullTruck' + val;

                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getTruckCityList.do",
                                                        dataType: "json",
                                                        data: {
                                                            cityName: request.term,
                                                            originCityId: $("#" + originPointId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                        },
                                                        error: function(data, type) {

                                                            //console.log(type);
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Name;
                                                    var id = ui.item.Id;
                                                    //alert(id+" : "+value);
                                                    $('#' + pointIdId).val(id);
                                                    $('#' + pointNameId).val(value);
                                                    $('#' + travelKm).val(ui.item.TravelKm);
                                                    $('#' + travelHour).val(ui.item.TravelHour);
                                                    $('#' + travelMinute).val(ui.item.TravelMinute);
                                                    $('#' + truckRouteId).val(ui.item.RouteId);
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                var itemVal = item.Name;
                                                itemVal = '<font color="green">' + itemVal + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + itemVal + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }
                                    </script>

                                    <div id="routeFullTruck">

                                    </div>
                                    <center>
                                        <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.Next" text="Next"/>" name="Next" style="width:100px;height:40px;"/></a>
                                                <!--<a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:40px;"/></a>-->
                            </center>
                        </div>

                        <div id="weightBreak">

                            <div id="main"  style="width: 72%">
                                <input class="btn btn-success"  type="button" id="btAddWeightBreak" value="Add Route" class="bt" />
                                <input class="btn btn-success" type="button" id="btRemoveWeightBreak" value="Remove Route" class="bt" />
                            </div>

                            <script>
                                var container1 = "";
                                $(document).ready(function() {
                                    var iCntWeightBreak = 1;
                                    var rowCntWeightBreak = 1;
                                    // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                    container1 = $($("#routeWeightBreak")).css({
                                        padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                        borderTopColor: '#999', borderBottomColor: '#999',
                                        borderLeftColor: '#999', borderRightColor: '#999'
                                    });
                                    $(container1).last().after('<table id="mainTableWeightBreak" ><tr></td>\
                                    <table   id="routeDetailsWeightBreak' + iCntWeightBreak + '"  border="1">\n\
                                    <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px">\n\
                                    <td>Sno</td>\n\
                                    <td>Origin</td>\n\
                                    <td>Destination</td>\n\
                                    <td>Travel Km</td>\n\
                                    <td>Travel Hour</td>\n\
                                    <td>Travel Min</td></tr>\n\
                                    <tr><td>Route-' + iCntWeightBreak + rowCntWeightBreak + '</td>\n\
                                    <td><input type="hidden" name="originIdWeightBreak" id="originIdWeightBreak' + iCntWeightBreak + '" value="" />\n\
                                    <input type="text" name="originNameWeightBreak" id="originNameWeightBreak' + iCntWeightBreak + '" value="" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="hidden" name="destinationIdWeightBreak" id="destinationIdWeightBreak' + iCntWeightBreak + '" value="" />\n\
                                    <input type="text" name="destinationNameWeightBreak" id="destinationNameWeightBreak' + iCntWeightBreak + '" value="" style="height:22px;width:160px;"/>\n\
                                    <input type="hidden" name="routeIdWeightBreak" id="routeIdWeightBreak' + iCntWeightBreak + '" value="" /></td>\n\
                                    <td><input type="text" name="travelKmWeightBreak" id="travelKmWeightBreak' + iCntWeightBreak + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="travelHourWeightBreak" id="travelHourWeightBreak' + iCntWeightBreak + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="travelMinuteWeightBreak" id="travelMinuteWeightBreak' + iCntWeightBreak + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                                    </tr>\n\
                                    </table>\n\
                                    <table  id="routeInfoDetailsWeightBreak' + iCntWeightBreak + rowCntWeightBreak + '" border="1"  >\n\
                                    <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                    <td>Sno</td><td>Movement Type</td><td>From Kg</td><td>To Kg</td><td>Rate With Reefer</td><td>Rate Without Reefer</td></tr>\n\
                                    <tr>\n\
                                    <td>' + rowCntWeightBreak + '</td>\n\
                                    <td><select name="movementTypeIdWeightBreak" id="movementTypeIdWeightBreak' + iCntWeightBreak + '" class="textbox" style="height:22px;width:160px;"><option value="1">Bonded</option></select></td>\n\
                                    <td><input type="text" name="fromKgWeightBreak" id="fromKgWeightBreak' + iCntWeightBreak + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="toKgWeightBreak" id="toKgWeightBreak' + iCntWeightBreak + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="rateWithReeferWeightBreak" id="rateWithReeferWeightBreak' + iCntWeightBreak + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="rateWithOutReeferWeightBreak" id="rateWithOutReeferWeightBreak' + iCntWeightBreak + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                    </tr></table>\n\
                                    <table border="0" ><tr>\n\
                                    <td><input class="btn btn-success" type="button" name="addRouteDetailsWeightBreak" id="addRouteDetailsWeightBreak' + iCntWeightBreak + rowCntWeightBreak + '" value="Add" onclick="addRowWeightBreak(' + iCntWeightBreak + rowCntWeightBreak + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>\n\
                                    <input class="btn btn-success" type="button" name="removeRouteDetailsWeightBreak" id="removeRouteDetailsWeightBreak' + iCntWeightBreak + rowCntWeightBreak + '" value="Remove"  onclick="deleteRowWeightBreak(' + iCntWeightBreak + rowCntWeightBreak + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></td>\n\
                                    </tr></table></td></tr></table><br><br>');
                                    callOriginAjaxWeightBreak(iCntWeightBreak);
                                    callDestinationAjaxWeightBreak(iCntWeightBreak);
                                    $('#btAddWeightBreak').click(function() {
                                        iCntWeightBreak = iCntWeightBreak + 1;
                                        $(container1).last().after('<table id="mainTableWeightBreak" ><tr><td>\
                                    <table   id="routeDetailsWeightBreak' + iCntWeightBreak + '" border="1" >\n\
                                    <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px">\n\
                                    <td>Sno</td>\n\
                                    <td>Origin</td>\n\
                                    <td>Destination</td>\n\
                                    <td>Travel Km</td>\n\
                                    <td>Travel Hour</td>\n\
                                    <td>Travel Min</td></tr>\n\
                                    <td>Route-' + iCntWeightBreak + rowCntWeightBreak + '</td>\n\
                                    <td><input type="hidden" name="originIdWeightBreak" id="originIdWeightBreak' + iCntWeightBreak + '" value="" />\n\
                                    <input type="text" name="originNameWeightBreak" id="originNameWeightBreak' + iCntWeightBreak + '" value="" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="hidden" name="destinationIdWeightBreak" id="destinationIdWeightBreak' + iCntWeightBreak + '" value="" />\n\
                                    <input type="text" name="destinationNameWeightBreak" id="destinationNameWeightBreak' + iCntWeightBreak + '" value="" style="height:22px;width:160px;"/>\n\
                                     <input type="hidden" name="routeIdWeightBreak" id="routeIdWeightBreak' + iCntWeightBreak + '" value="" /></td>\n\
                                     <td><input type="text" name="travelKmWeightBreak" id="travelKmWeightBreak' + iCntWeightBreak + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="travelHourWeightBreak" id="travelHourWeightBreak' + iCntWeightBreak + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="travelMinuteWeightBreak" id="travelMinuteWeightBreak' + iCntWeightBreak + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                                    </tr>\n\
                                    </table>\n\
                                    <table  id="routeInfoDetailsWeightBreak' + iCntWeightBreak + rowCntWeightBreak + '" border="1" >\n\
                                    <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                    <td>Sno</td><td>Movement Type</td><td>From Kg</td><td>To Kg</td><td>Rate With Reefer</td><td>Rate Without Reefer</td></tr>\n\
                                    <tr>\n\
                                    <td>' + rowCntWeightBreak + '</td>\n\
                                    <td><select name="movementTypeIdWeightBreak" id="movementTypeIdWeightBreak' + iCntWeightBreak + '" class="textbox" style="height:22px;width:160px;"><option value="1">Bonded</option></select></td>\n\
                                    <td><input type="text" name="fromKgWeightBreak" id="fromKgWeightBreak' + iCntWeightBreak + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="toKgWeightBreak" id="toKgWeightBreak' + iCntWeightBreak + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="rateWithReeferWeightBreak" id="rateWithReeferWeightBreak' + iCntWeightBreak + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="rateWithOutReeferWeightBreak" id="rateWithOutReeferWeightBreak' + iCntWeightBreak + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                    </tr></table>\n\
                                    <table border="0" ><tr>\n\
                                    <td><input class="btn btn-success"  type="button" name="addRouteDetailsWeightBreak" id="addRouteDetailsWeightBreak' + iCntWeightBreak + rowCntWeightBreak + '" value="Add" onclick="addRowWeightBreak(' + iCntWeightBreak + rowCntWeightBreak + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>\n\
                                    <input class="btn btn-success"  type="button" name="removeRouteDetailsWeightBreak" id="removeRouteDetailsWeightBreak' + iCntWeightBreak + rowCntWeightBreak + '" value="Remove" onclick="deleteRowWeightBreak(' + iCntWeightBreak + rowCntWeightBreak + ')"  style="width:70px;height:30px;font-weight: bold;padding:1px;"/></td>\n\
                                    </tr></table></td></tr></table><br><br>');
                                        callOriginAjaxWeightBreak(iCntWeightBreak);
                                        callDestinationAjaxWeightBreak(iCntWeightBreak);
                                        $('#main').after(container1);
                                    });
                                    $('#btRemoveWeightBreak').click(function() {
                                        alert($('#mainTableWeightBreak tr').size());
                                        if ($('#mainTableWeightBreak tr').size() > 1) {
                                            $('#mainTableWeightBreak tr').last().remove();
                                            iCntWeightBreak = iCntWeightBreak - 1;
                                        }
                                    });
                                });

                                // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                var divValue, values = '';
                                function GetTextValue() {
                                    $(divValue).empty();
                                    $(divValue).remove();
                                    values = '';
                                    $('.input').each(function() {
                                        divValue = $(document.createElement('div')).css({
                                            padding: '5px', width: '200px'
                                        });
                                        values += this.value + '<br />'
                                    });
                                    $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                    $('body').append(divValue);
                                }

                                function addRowWeightBreak(val) {
                                    //alert(val);
                                    var loadCnt = val;
                                    var routeInfoSize = $('#routeInfoDetailsWeightBreak' + loadCnt + ' tr').size();
                                    var addRouteDetails = "addRouteDetailsWeightBreak" + loadCnt;
                                    var routeInfoDetails = "routeInfoDetailsWeightBreak" + loadCnt;
                                    $('#routeInfoDetailsWeightBreak' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSize + '</td><td><select name="movementTypeIdWeightBreak" id="movementTypeIdWeightBreak' + loadCnt + '" class="textbox" style="height:22px;width:160px;"><option value="1">Bonded</option></select></td><td><input type="text" name="fromKgWeightBreak" id="fromKgWeightBreak' + loadCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td><td><input type="text" name="toKgWeightBreak" id="toKgWeightBreak' + loadCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td><td><input type="text" name="rateWithReeferWeightBreak" id="rateWithReeferWeightBreak' + loadCnt + '" value="" style="height:22px;width:160px;"/></td><td><input type="text" name="rateWithOutReeferWeightBreak" id="rateWithOutReeferWeightBreak' + loadCnt + '" value="" style="height:22px;width:160px;"/></td></tr>');
                                    loadCnt++;
                                }
                                function deleteRowWeightBreak(val) {
                                    var loadCnt = val;
                                    var addRouteDetails = "addRouteDetailsWeightBreak" + loadCnt;
                                    var routeInfoDetails = "routeInfoDetailsWeightBreak" + loadCnt;
                                    if ($('#routeInfoDetailsWeightBreak' + loadCnt + ' tr').size() > 2) {
                                        $('#routeInfoDetailsWeightBreak' + loadCnt + ' tr').last().remove();
                                        loadCnt = loadCnt - 1;
                                    } else {
                                        alert('One row should be present in table');
                                    }
                                }
                            </script>

                            <div id="routeWeightBreak">

                            </div>

                            <script>
                                function callOriginAjaxWeightBreak(val) {
                                    // Use the .autocomplete() method to compile the list based on input from user
                                    //alert(val);
                                    var pointNameId = 'originNameWeightBreak' + val;
                                    var pointIdId = 'originIdWeightBreak' + val;
                                    var desPointName = 'destinationNameWeightBreak' + val;


                                    //alert(prevPointId);
                                    $('#' + pointNameId).autocomplete({
                                        source: function(request, response) {
                                            $.ajax({
                                                url: "/throttle/getTruckCityList.do",
                                                dataType: "json",
                                                data: {
                                                    cityName: request.term,
                                                    textBox: 1
                                                },
                                                success: function(data, textStatus, jqXHR) {
                                                    var items = data;
                                                    response(items);
                                                },
                                                error: function(data, type) {

                                                    //console.log(type);
                                                }
                                            });
                                        },
                                        minLength: 1,
                                        select: function(event, ui) {
                                            var value = ui.item.Name;
                                            var id = ui.item.Id;
                                            //alert(id+" : "+value);
                                            $('#' + pointIdId).val(id);
                                            $('#' + pointNameId).val(value);
                                            $('#' + desPointName).focus();
                                            //validateRoute(val,value);

                                            return false;
                                        }

                                        // Format the list menu output of the autocomplete
                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                        //alert(item);
                                        var itemVal = item.Name;
                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                        return $("<li></li>")
                                                .data("item.autocomplete", item)
                                                .append("<a>" + itemVal + "</a>")
                                                .appendTo(ul);
                                    };


                                }

                                function callDestinationAjaxWeightBreak(val) {
                                    // Use the .autocomplete() method to compile the list based on input from user
                                    //alert(val);
                                    var pointNameId = 'destinationNameWeightBreak' + val;
                                    var pointIdId = 'destinationIdWeightBreak' + val;
                                    var originPointId = 'originIdWeightBreak' + val;
                                    var truckRouteId = 'routeIdWeightBreak' + val;
                                    var travelKm = 'travelKmWeightBreak' + val;
                                    var travelHour = 'travelHourWeightBreak' + val;
                                    var travelMinute = 'travelMinuteWeightBreak' + val;

                                    //alert(prevPointId);
                                    $('#' + pointNameId).autocomplete({
                                        source: function(request, response) {
                                            $.ajax({
                                                url: "/throttle/getTruckCityList.do",
                                                dataType: "json",
                                                data: {
                                                    cityName: request.term,
                                                    originCityId: $("#" + originPointId).val(),
                                                    textBox: 1
                                                },
                                                success: function(data, textStatus, jqXHR) {
                                                    var items = data;
                                                    response(items);
                                                },
                                                error: function(data, type) {

                                                    //console.log(type);
                                                }
                                            });
                                        },
                                        minLength: 1,
                                        select: function(event, ui) {
                                            var value = ui.item.Name;
                                            var id = ui.item.Id;
                                            //alert(id+" : "+value);
                                            $('#' + pointIdId).val(id);
                                            $('#' + pointNameId).val(value);
                                            $('#' + travelKm).val(ui.item.TravelKm);
                                            $('#' + travelHour).val(ui.item.TravelHour);
                                            $('#' + travelMinute).val(ui.item.TravelMinute);
                                            $('#' + truckRouteId).val(ui.item.RouteId);
                                            //validateRoute(val,value);

                                            return false;
                                        }

                                        // Format the list menu output of the autocomplete
                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                        //alert(item);
                                        var itemVal = item.Name;
                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                        return $("<li></li>")
                                                .data("item.autocomplete", item)
                                                .append("<a>" + itemVal + "</a>")
                                                .appendTo(ul);
                                    };


                                }
                            </script>
                            <!--<a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:40px;"/></a>-->
                            <a  href="#"><input type="button" class="btn btn-success" value="Save" onclick="saveCustomerContract();" style="width:100px;height:40px;"/></a>      
                        </div>     
                        <script>
                            function saveCustomerContract() {
                                document.customerContract.action = "/throttle/saveCustomerContractDetails.do";
                                document.customerContract.submit();
                            }

//                                    $(".nexttab").click(function() {
//                                        var selected = $("#tabs").tabs("option", "selected");
//                                        $("#tabs").tabs("option", "selected", selected + 1);
//                                    });
//                                    $(".pretab").click(function() {
//                                        var selected = $("#tabs").tabs("option", "selected");
//                                        $("#tabs").tabs("option", "selected", selected - 1);
//                                    });

                        </script>

                        <script>
                            //                    $(".nexttab").click(function() {
                            //                        var selected = $("#tabs").tabs("option", "selected");
                            //                        $("#tabs").tabs("option", "selected", selected + 1);
                            //                    });
                            $('.btnNext').click(function() {
                                $('.nav-tabs > .active').next('li').find('a').trigger('click');
                            });
                            $('.btnPrevious').click(function() {
                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                            });
                        </script>
                    </div>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>  
