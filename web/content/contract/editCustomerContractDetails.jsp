<%-- 
    Document   : contractPointToPointWeight
    Created on : Jan 27, 2015, 8:35:48 PM
    Author     : Nivan
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>



<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        // alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Customer Contract Master" text="Customer Contract Master"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Customer" text="Customer"/></a></li>
            <li class=""><spring:message code="hrms.label.Customer Contract Master" text="Customer Contract Master"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>


                <form name="customerContract"  method="post" >

                    <br>
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <c:if test="${contractDetails != null}">
                        <c:forEach items="${contractDetails}" var="customer">

                            <table  align="center"  class="table table-info mb30 table-hover" id="bg">
                                <thead>
                                    <tr>
                                        <th colspan="4" height="30" >Customer Contract Master</th>
                                    </tr>
                                </thead>

                                <tr>
                                    <td class="text1"><input type="hidden" name="contractId" id="contractId" value="<c:out value="${customer.contractId}"/>" class="form-control" style="width:240px;height:40px">
                                        <input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>" class="form-control" style="width:240px;height:40px">Customer Name</td>
                                    <td class="text1">&nbsp;<input type="hidden" name="customerName" id="customerName" value="<c:out value="${customerName}"/>" class="form-control" style="width:240px;height:40px" readonly=""><c:out value="${customer.customerName}"/></td>
                                    <td class="text1">Customer Code</td>
                                    <td class="text1">&nbsp;<input type="hidden" name="customerCode" id="customerCode" value="<c:out value="${customerCode}"/>" class="form-control" style="width:240px;height:40px" readonly=""><c:out value="${customer.customerCode}"/></td>
                                </tr>
                                <tr>
                                <tr>
                                    <td class="text2">Contract From</td>
                                    <td class="text2"><input type="text" name="contractFrom" id="contractFrom" value="<c:out value="${customer.contractFrom}"/>" class="datepicker form-control" style="width:240px;height:40px"></td>
                                    <td class="text2">Contract To</td>
                                    <td class="text2"><input type="text" name="contractTo" id="contractTo" value="<c:out value="${customer.contractTo}"/>" class="datepicker form-control" style="width:240px;height:40px"></td>
                                </tr>

                                <tr>
                                    <td class="text1">Contract No</td>
                                    <td class="text1">&nbsp;<input type="text" name="contractNo" id="contractNo" value="<c:out value="${customer.contractNo}"/>" class="form-control" style="width:240px;height:40px" ></td>
                                    <td class="text1">Billing Type</td>
                                    <td class="text1"><input type="hidden" name="billingTypeId" id="billingTypeId" value="<c:out value="${billingTypeId}"/>" class="form-control" style="width:240px;height:40px"/>
                                        <c:if test="${billingTypeList != null}">
                                            <c:forEach items="${billingTypeList}" var="btl">
                                                <c:if test="${billingTypeId == btl.billingTypeId}">
                                                    <c:out value="${btl.billingTypeName}"/>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </td>
                                </tr>
                            </table>
                            <br>
                        </c:forEach></c:if>

                        <div id="tabs">
                            <ul class="nav nav-tabs">
                                <!--<li class="active" data-toggle="tab"><a href="#fullTruck"><span>Full Truck</span></a></li>-->
                                <li  class="active" data-toggle="tab"><a href="#weightBreak"><span>Weight Break </span></a></li>
                            </ul>





                            <div id="fullTruck" style="display:none">



                                <div id="routeFullTruck">
                                <c:if test="${contractRouteDetails != null}">
                                    <table align="center"  id="table" class="table table-info mb30 table-hover sortable"  >
                                        <thead>
                                            <tr>
                                                <th align="center">S.No</th>
                                                <th align="center">Vehicle Type</th>
                                                <th align="center">Origin</th>
                                                <th align="center">Destination</th>
                                                <th align="center">Contract Route Code</th>
                                                <th align="center">Effective Date</th>
                                                <th align="center">Travel Kms</th>
                                                <th align="center">Travel Hours</th>
                                                <th align="center">Travel Minutes</th>
                                                <th align="center">Rate With Reefer</th>
                                                <th align="center">Rate Without Reefer</th>
                                                <th align="center">Active Status</th>
                                            </tr> 
                                        </thead>
                                        <tbody>
                                            <% int index = 1;%>
                                            <c:forEach items="${contractRouteDetails}" var="route">
                                                <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                                %>
                                                <tr>
                                                    <td   ><%=index++%></td>
                                                    <td    > 
                                                        <input type="hidden" name="rateContractId" id="rateContractId<%=index%>" value=" <c:out value="${route.rateContractId}"/>"/>
                                                        <input type="hidden" name="routeContractId" id="routeContractId<%=index%>" value=" <c:out value="${route.routeContractId}"/>"/>
                                                        <c:out value="${route.vehicleTypeName}"/></td>
                                                    <td    ><c:out value="${route.firstPointName}"/></td>
                                                    <td    ><c:out value="${route.finalPointName}"/></td>
                                                    <td    ><c:out value="${route.routeContractCode}"/></td>
                                                    <td    ><c:out value="${route.effectiveDate}"/></td>
                                                    <td    ><c:out value="${route.travelKm}"/></td>
                                                    <td    ><c:out value="${route.travelHours}"/></td>
                                                    <td    ><c:out value="${route.travelMinutes}"/></td>
                                                    <td    ><input type="text" name="rateWithReeferTruck" id="rateWithReeferTruck<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${route.rateWithReefer}"/>"/></td>
                                                    <td    ><input type="text" name="rateWithOutReeferTruck" id="rateWithOutReeferTruck<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${route.rateWithoutReefer}"/>"/></td>
                                                    <td    >
                                                        <select name="activeIndRates" id="activeIndRates<%=index%>" onchange="setActiveIndRate('<%=index%>');">
                                                            <option value="Y"  >Active</option>
                                                            <option value="N"  >In Active</option>
                                                        </select>
                                                        <input type="hidden" name="activeIndRate" id="activeIndRate<%=index%>" value="" />
                                                        <script type="text/javascript">
                                                            <c:if test="${route.activeInd == 'Y'}">
                                                            document.getElementById("activeIndRates<%=index%>").value = 'Y';
                                                            document.getElementById("activeIndRate<%=index%>").value = 'Y';
                                                            </c:if>
                                                            <c:if test="${route.activeInd == 'N'}">
                                                            document.getElementById("activeIndRates<%=index%>").value = 'N';
                                                            document.getElementById("activeIndRate<%=index%>").value = 'N';
                                                            </c:if>
                                                        </script>
                                                        <script type="text/javascript">
                                                            function setActiveIndRate(sno) {
                                                                document.getElementById("activeIndRate" + sno).value = document.getElementById("activeIndRates" + sno).value;
                                                            }
                                                        </script>

                                                    </td>
                                                </tr>

                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    <br/>

                                    <script>


                                        var container = "";
                                        function addNewRowFullTruck() {

                                            $("#AddNewRowFullTruck").hide();
                                            var iCnt = 1;
                                            var rowCnt = 1;
                                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                            container = $($("#routeFullTruck")).css({
                                                padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                                borderTopColor: '#999', borderBottomColor: '#999',
                                                borderLeftColor: '#999', borderRightColor: '#999'
                                            });
                                            $(container).last().after('<table id="mainTableFullTruck" ><tr></td>\
                            <table   id="routeDetails' + iCnt + '"  border="1">\n\
                            <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                            <td>Sno</td>\n\
                            <td>Origin</td>\n\
                            <td>Destination</td>\n\
                            <td>Travel Km</td>\n\
                            <td>Travel Hour</td>\n\
                            <td>Travel Min</td></tr>\n\
                            <tr><td>Route-' + iCnt + rowCnt + '</td>\n\
                            <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/></td>\n\
                            <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/>\n\
                            <input type="hidden" name="routeIdFullTruck" id="routeIdFullTruck' + iCnt + '" value="" /></td>\n\
                            <td><input type="text" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                            <td><input type="text" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                            <td><input type="text" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                            </tr>\n\
                            </table>\n\
                            <table  id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1"  width="73%">\n\
                            <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                            <td>Sno</td><td>Movement Type</td><td>Vehicle Type</td><td>Rate With Reefer</td><td>Rate Without Reefer</td></tr>\n\
                            <tr>\n\
                            <td>' + rowCnt + '</td>\n\
                            <td><select name="movementTypeIdFullTruck" id="movementTypeIdFullTruck' + iCnt + '" class="textbox" style="height:22px;width:160px;"><option value="0">--Select--</option><option value="1">Bonded</option><option value="2">NonBonded</option></select></td>\n\
                            <td><select ype="text" name="vehicleTypeIdFullTruck" id="vehicleTypeIdFullTruck' + iCnt + '" style="height:22px;width:160px;"><option value="0">--Select--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="rateWithReeferFullTruck" id="rateWithReeferFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                            <td><input type="text" name="rateWithOutReeferFullTruck" id="rateWithOutReeferFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                            </tr></table>\n\
                            <table border="0" ><tr>\n\
                            <td><input class="btn btn-success" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ')" />\n\
                            <input class="btn btn-success" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRow(' + iCnt + rowCnt + ')" /></td>\n\
                            </tr></table></td></tr></table><br><br>');
                                            callOriginAjaxFullTruck(iCnt);
                                            callDestinationAjaxFullTruck(iCnt);

                                        }

                                        // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                        var divValue, values = '';
                                        function GetTextValue() {
                                            $(divValue).empty();
                                            $(divValue).remove();
                                            values = '';
                                            $('.input').each(function() {
                                                divValue = $(document.createElement('div')).css({
                                                    padding: '5px', width: '200px'
                                                });
                                                values += this.value + '<br />'
                                            });
                                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                            $('body').append(divValue);
                                        }

                                        function addRow(val) {
                                            //alert(val);
                                            var loadCnt = val;
                                            var routeInfoSize = $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size();
                                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                            $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSize + '</td><td><select name="movementTypeIdFullTruck" id="movementTypeIdFullTruck' + loadCnt + '" class="textbox" style="height:22px;width:160px;"><option value="0">--Select--</option><option value="1">Bonded</option><option value="2">NonBonded</option></select></td><td><select ype="text" name="vehicleTypeIdFullTruck" id="vehicleTypeIdFullTruck' + loadCnt + '" style="height:22px;width:160px;"><option value="0">--Select--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td><td><input type="text" name="rateWithReeferFullTruck" id="rateWithReeferFullTruck' + loadCnt + '" value="" style="height:22px;width:160px;"/></td><td><input type="text" name="rateWithOutReeferFullTruck" id="rateWithOutReeferFullTruck' + loadCnt + '" value="" style="height:22px;width:160px;"/></td></tr>');
                                            loadCnt++;
                                        }
                                        function deleteRow(val) {
                                            var loadCnt = val;
                                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                            if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                                $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                                loadCnt = loadCnt - 1;
                                            } else {
                                                alert('One row should be present in table');
                                            }
                                        }
                                            </script>

                                            <script>
                                                function callOriginAjaxFullTruck(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
                                                    //alert(val);
                                                    var pointNameId = 'originNameFullTruck' + val;
                                                    var pointIdId = 'originIdFullTruck' + val;
                                                    var desPointName = 'destinationNameFullTruck' + val;


                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getTruckCityList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    cityName: request.term,
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                },
                                                                error: function(data, type) {

                                                                    //console.log(type);
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Name;
                                                            var id = ui.item.Id;
                                                            //alert(id+" : "+value);
                                                            $('#' + pointIdId).val(id);
                                                            $('#' + pointNameId).val(value);
                                                            $('#' + desPointName).focus();
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        var itemVal = item.Name;
                                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + itemVal + "</a>")
                                                                .appendTo(ul);
                                                    };


                                                }

                                                function callDestinationAjaxFullTruck(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
                                                    //alert(val);
                                                    var pointNameId = 'destinationNameFullTruck' + val;
                                                    var pointIdId = 'destinationIdFullTruck' + val;
                                                    var originPointId = 'originIdFullTruck' + val;
                                                    var truckRouteId = 'routeIdFullTruck' + val;
                                                    var travelKm = 'travelKmFullTruck' + val;
                                                    var travelHour = 'travelHourFullTruck' + val;
                                                    var travelMinute = 'travelMinuteFullTruck' + val;

                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getTruckCityList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    cityName: request.term,
                                                                    originCityId: $("#" + originPointId).val(),
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                },
                                                                error: function(data, type) {

                                                                    //console.log(type);
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Name;
                                                            var id = ui.item.Id;
                                                            //alert(id+" : "+value);
                                                            $('#' + pointIdId).val(id);
                                                            $('#' + pointNameId).val(value);
                                                            $('#' + travelKm).val(ui.item.TravelKm);
                                                            $('#' + travelHour).val(ui.item.TravelHour);
                                                            $('#' + travelMinute).val(ui.item.TravelMinute);
                                                            $('#' + truckRouteId).val(ui.item.RouteId);
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        var itemVal = item.Name;
                                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + itemVal + "</a>")
                                                                .appendTo(ul);
                                                    };


                                                }
                                            </script>

                                </c:if>
                            </div>

                            <center>
                                <input type="button" class="btn btn-success" value="AddNewRow" id="AddNewRowFullTruck" name="AddNewRow" onclick="addNewRowFullTruck();"/></a>
                                <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.Next" text="Next"/>" name="Next" style="width:100px;height:40px;"/></a>
                                        <!--<a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:40px;"/></a>-->
                            </center>
                        </div>

                        <div id="weightBreak">



                            <div id="routeWeightBreak">

                                <c:if test="${contractWeightDetails != null}">
                                    <table align="center"  id="table" class="table table-info mb30 table-hover sortable"  >
                                        <thead>
                                            <tr>
                                                <th align="center">S.No</th>
                                                <th align="center">Origin</th>
                                                <th align="center">Destination</th>
                                                <th align="center">Contract Route Code</th>
                                                <th align="center">Effective Date</th>
                                                <th align="center">Travel Kms</th>
                                                <th align="center">Travel Hours</th>
                                                <th align="center">Travel Minutes</th>
                                                <th align="center">WeightFromKg</th>
                                                <th align="center">WeightToKg</th>
                                                <th align="center">Rate With Reefer</th>
                                                <th align="center">Rate Without Reefer</th>
                                                <th align="center">Active Status</th>
                                            </tr> 
                                        </thead>
                                        <tbody>
                                            <% int index = 1;%>
                                            <c:forEach items="${contractWeightDetails}" var="weight">
                                                <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                                %>
                                                <tr>
                                                    <td   ><%=index++%></td>
                                                    <td    ><input type="hidden" name="weightContractId" id="weightContractId<%=index%>" value=" <c:out value="${weight.contractId}"/>"/> 
                                                        <input type="hidden" name="weightRateContractId" id="weightRateContractId<%=index%>" value=" <c:out value="${weight.rateContractId}"/>"/>
                                                        <input type="hidden" name="weightRouteContractId" id="weightRouteContractId<%=index%>" value=" <c:out value="${weight.routeContractId}"/>"/>

                                                        <c:out value="${weight.firstPointName}"/></td>
                                                    <td    ><c:out value="${weight.finalPointName}"/></td>
                                                    <td    ><c:out value="${weight.routeContractCode}"/></td>
                                                    <td    ><c:out value="${weight.effectiveDate}"/></td>
                                                    <td    ><c:out value="${weight.travelKm}"/></td>
                                                    <td    ><c:out value="${weight.travelHours}"/></td>
                                                    <td    ><c:out value="${weight.travelMinutes}"/></td>
                                                    <td    ><input type="text" name="fromKgWeight" id="fromKgWeight<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${weight.weightFromKg}"/>"/></td>
                                                    <td    ><input type="text" name="toKgWeight" id="toKgWeight<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${weight.weightToKg}"/>"/></td>
                                                    <td    ><input type="text" name="rateWithReeferWeight" id="rateWithReeferWeight<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${weight.rateWithReefer}"/>"/></td>
                                                    <td    ><input type="text" name="rateWithOutReeferWeight" id="rateWithOutReeferWeight<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${weight.rateWithoutReefer}"/>"/></td>
                                                    <td    >
                                                        <select name="activeIndWeights" id="activeIndWeights<%=index%>" onchange="setActiveIndWeight('<%=index%>');">
                                                            <option value="Y"  >Active</option>
                                                            <option value="N"  >In Active</option>
                                                        </select>
                                                        <input type="hidden" name="activeIndWeight" id="activeIndWeight<%=index%>" value="" />
                                                        <script type="text/javascript">
                                                            <c:if test="${weight.activeInd == 'Y'}">
                                                            document.getElementById("activeIndWeight<%=index%>").value = 'Y';
                                                            document.getElementById("activeIndWeights<%=index%>").value = 'Y';
                                                            </c:if>
                                                            <c:if test="${weight.activeInd == 'N'}">
                                                            document.getElementById("activeIndWeight<%=index%>").value = 'N';
                                                            document.getElementById("activeIndWeights<%=index%>").value = 'N';
                                                            </c:if>
                                                        </script>

                                                        <script type="text/javascript">
                                                            function setActiveIndWeight(sno) {
                                                                document.getElementById("activeIndWeight" + sno).value = document.getElementById("activeIndWeights" + sno).value;
                                                            }
                                                        </script>

                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>  

                                    <script>
                                        var container1 = "";
                                        function addNewRowWeightBreak(val) {
//                                            $("#AddNewRowWeightBreak").hide();

                                            var iCntWeightBreak = <%=index%>;
                                            var iCnt1 = 1;
                                            var rowCntWeightBreak = <%=index%>;
                                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                            container1 = $($("#routeWeightBreak")).css({
                                                padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                                borderTopColor: '#999', borderBottomColor: '#999',
                                                borderLeftColor: '#999', borderRightColor: '#999'
                                            });

                                            var routeInfoSize = $('#routeDetailsWeightBreak' + iCntWeightBreak + ' tr').size();
                                            //                                    alert(routeInfoSize);
                                            //                                    iCnt = parseFloat(iCnt) + parseFloat(routeInfoSize / 2);
                                            rowCntWeightBreak = parseFloat(iCntWeightBreak) + parseFloat(routeInfoSize / 2);

                                            $(container1).last().after('<table id="mainTableWeightBreak" ><tr></td>\
                                            <table   id="routeDetailsWeightBreak' + iCntWeightBreak + '"  border="1">\n\
                                            <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                            <td>Sno</td>\n\
                                            <td>Origin</td>\n\
                                            <td>Destination</td>\n\
                                            <td>Travel Km</td>\n\
                                            <td>Travel Hour</td>\n\
                                            <td>Travel Min</td></tr>\n\
                                            <tr><td>Route-' + rowCntWeightBreak + '</td>\n\
                                            <td><input type="hidden" name="originIdWeightBreak" id="originIdWeightBreak' + rowCntWeightBreak + '" value="" />\n\
                                            <input type="text" name="originNameWeightBreak" id="originNameWeightBreak' + rowCntWeightBreak + '" value="" style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="hidden" name="destinationIdWeightBreak" id="destinationIdWeightBreak' + rowCntWeightBreak + '" value="" />\n\
                                            <input type="text" name="destinationNameWeightBreak" id="destinationNameWeightBreak' + rowCntWeightBreak + '" value="" style="height:22px;width:160px;"/>\n\
                                            <input type="hidden" name="routeIdWeightBreak" id="routeIdWeightBreak' + rowCntWeightBreak + '" value="" /></td>\n\
                                            <td><input type="text" name="travelKmWeightBreak" id="travelKmWeightBreak' + rowCntWeightBreak + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="text" name="travelHourWeightBreak" id="travelHourWeightBreak' + rowCntWeightBreak + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="text" name="travelMinuteWeightBreak" id="travelMinuteWeightBreak' + rowCntWeightBreak + '" value="" readOnly style="height:22px;width:160px;"/></td>\n\
                                            </tr>\n\
                                            </table>\n\
                                            <table  id="routeInfoDetailsWeightBreak' + iCnt1 + rowCntWeightBreak + '" border="1"  >\n\
                                            <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                            <td>Sno</td><td>Movement Type</td><td>From Kg</td><td>To Kg</td><td>Rate With Reefer</td><td>Rate Without Reefer</td></tr>\n\
                                            <tr>\n\
                                            <td>' + iCnt1 + '<input type="hidden" name="iCnt1" id="iCnt1' + iCnt1 + '" value="' + rowCntWeightBreak + '"/></td>\n\
                                            <td><select name="movementTypeIdWeightBreak" id="movementTypeIdWeightBreak' + iCnt1 + '" class="textbox" style="height:22px;width:160px;"><option value="1">Bonded</option></td>\n\
                                            <td><input type="text" name="fromKgWeightBreak" id="fromKgWeightBreak' + iCnt1 + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="text" name="toKgWeightBreak" id="toKgWeightBreak' + iCnt1 + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="text" name="rateWithReeferWeightBreak" id="rateWithReeferWeightBreak' + iCnt1 + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="text" name="rateWithOutReeferWeightBreak" id="rateWithOutReeferWeightBreak' + iCnt1 + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                            </tr></table>\n\
                                            <table border="0" ><tr>\n\
                                            <td><input class="btn btn-success" type="button" name="addRouteDetailsWeightBreak" id="addRouteDetailsWeightBreak' + iCnt1 + rowCntWeightBreak + '" value="Add" onclick="addRowWeightBreak(' + iCnt1 + rowCntWeightBreak + ',' + rowCntWeightBreak + ')" />\n\
                                            <input class="btn btn-success" type="button" name="removeRouteDetailsWeightBreak" id="removeRouteDetailsWeightBreak' + iCnt1 + rowCntWeightBreak + '" value="Remove"  onclick="deleteRowWeightBreak(' + iCnt1 + rowCntWeightBreak + ')" /></td>\n\
                                            </tr></table></td></tr></table><br><br>');
                                            callOriginAjaxWeightBreak(rowCntWeightBreak);
                                            callDestinationAjaxWeightBreak(rowCntWeightBreak);

                                        }

                                        // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                        var divValue, values = '';
                                        function GetTextValue() {
                                            $(divValue).empty();
                                            $(divValue).remove();
                                            values = '';
                                            $('.input').each(function() {
                                                divValue = $(document.createElement('div')).css({
                                                    padding: '5px', width: '200px'
                                                });
                                                values += this.value + '<br />'
                                            });
                                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                            $('body').append(divValue);
                                        }

                                        function addRowWeightBreak(val,v1) {
                            
               //alert(val);
                                            var loadCnt = val;
                                            var loadCnt1 = v1;
                                            var routeInfoSize = $('#routeInfoDetailsWeightBreak' + loadCnt + ' tr').size();
                                            var addRouteDetails = "addRouteDetailsWeightBreak" + loadCnt;
                                            var routeInfoDetails = "routeInfoDetailsWeightBreak" + loadCnt;
                                            $('#routeInfoDetailsWeightBreak' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSize + ' <input type="hidden" name="iCnt1" id="iCnt1' + routeInfoSize + '" value="' + v1 + '"/></td><td><select name="movementTypeIdWeightBreak" id="movementTypeIdWeightBreak' + routeInfoSize + '" class="textbox" style="height:22px;width:160px;"><option value="1">Bonded</option></select></td><td><input type="text" name="fromKgWeightBreak" id="fromKgWeightBreak' + routeInfoSize + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td><td><input type="text" name="toKgWeightBreak" id="toKgWeightBreak' + routeInfoSize + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td><td><input type="text" name="rateWithReeferWeightBreak" id="rateWithReeferWeightBreak' + routeInfoSize + '" value="" style="height:22px;width:160px;"/></td><td><input type="text" name="rateWithOutReeferWeightBreak" id="rateWithOutReeferWeightBreak' + routeInfoSize + '" value="" style="height:22px;width:160px;"/></td></tr>');
                                            loadCnt++;
                                        }
                                        function deleteRowWeightBreak(val) {
                                            var loadCnt = val;
                                            var addRouteDetails = "addRouteDetailsWeightBreak" + loadCnt;
                                            var routeInfoDetails = "routeInfoDetailsWeightBreak" + loadCnt;
                                            if ($('#routeInfoDetailsWeightBreak' + loadCnt + ' tr').size() > 2) {
                                                $('#routeInfoDetailsWeightBreak' + loadCnt + ' tr').last().remove();
                                                loadCnt = loadCnt - 1;
                                            } else {
                                                alert('One row should be present in table');
                                            }
                                        }
                                    </script>
                                    <script>
                                        function callOriginAjaxWeightBreak(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //alert(val);
                                            var pointNameId = 'originNameWeightBreak' + val;
                                            var pointIdId = 'originIdWeightBreak' + val;
                                            var desPointName = 'destinationNameWeightBreak' + val;


                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getTruckCityList.do",
                                                        dataType: "json",
                                                        data: {
                                                            cityName: request.term,
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                        },
                                                        error: function(data, type) {

                                                            //console.log(type);
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Name;
                                                    var id = ui.item.Id;
                                                    //alert(id+" : "+value);
                                                    $('#' + pointIdId).val(id);
                                                    $('#' + pointNameId).val(value);
                                                    $('#' + desPointName).focus();
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                var itemVal = item.Name;
                                                itemVal = '<font color="green">' + itemVal + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + itemVal + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }

                                        function callDestinationAjaxWeightBreak(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //alert(val);
                                            var pointNameId = 'destinationNameWeightBreak' + val;
                                            var pointIdId = 'destinationIdWeightBreak' + val;
                                            var originPointId = 'originIdWeightBreak' + val;
                                            var truckRouteId = 'routeIdWeightBreak' + val;
                                            var travelKm = 'travelKmWeightBreak' + val;
                                            var travelHour = 'travelHourWeightBreak' + val;
                                            var travelMinute = 'travelMinuteWeightBreak' + val;

                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getTruckCityList.do",
                                                        dataType: "json",
                                                        data: {
                                                            cityName: request.term,
                                                            originCityId: $("#" + originPointId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                        },
                                                        error: function(data, type) {

                                                            //console.log(type);
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Name;
                                                    var id = ui.item.Id;
                                                    //alert(id+" : "+value);
                                                    $('#' + pointIdId).val(id);
                                                    $('#' + pointNameId).val(value);
                                                    $('#' + travelKm).val(ui.item.TravelKm);
                                                    $('#' + travelHour).val(ui.item.TravelHour);
                                                    $('#' + travelMinute).val(ui.item.TravelMinute);
                                                    $('#' + truckRouteId).val(ui.item.RouteId);
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                var itemVal = item.Name;
                                                itemVal = '<font color="green">' + itemVal + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + itemVal + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }
                                    </script>

                                </c:if>
                                <div id="AddNewRowWeightBreak"> <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="AddNewRow"  name="AddNewRow" onclick="addNewRowWeightBreak(2);"/></a>
                                </div>
                                <br/>
                                <br/>
                            </div>
                            <!--<a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:40px;"/></a>-->

                            <a  class="pretab" href="#"><input type="button" class="btn btn-success" value="Save" name="Save" onclick="submitPage();"/></a>      
                        </div>     
                        <script>
                            function submitPage() {
                                document.customerContract.action = "/throttle/saveEditCustomerContractDetails.do";
                                document.customerContract.method = "post";
                                document.customerContract.submit();
                            }

                        </script>

                        <script>
                            //                    $(".nexttab").click(function() {
                            //                        var selected = $("#tabs").tabs("option", "selected");
                            //                        $("#tabs").tabs("option", "selected", selected + 1);
                            //                    });
                            $('.btnNext').click(function() {
                                $('.nav-tabs > .active').next('li').find('a').trigger('click');
                            });
                            $('.btnPrevious').click(function() {
                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                            });
                        </script>

                    </div>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
