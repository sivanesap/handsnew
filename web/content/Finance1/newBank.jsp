

<%--
    Document   : newBank
    Created on : 20 Oct, 2012, 5:49:25 PM
    Author     : ASHOK
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Parveen Auto Care</title>
<link href="/throttle/css/parveen.css" rel="stylesheet"/>

<script language="javascript" src="/throttle/js/validate.js"></script>
</head>
<script>
  function submitPage()
    {

        if(textValidation(document.add.bankName,'bankName')){
            return;
        }
        if(textValidation(document.add.bankCode,'bankCode')){
            return;
        }
        if(textValidation(document.add.description,'description')){
            return;
        }

        document.add.action='/throttle/saveNewBank.do';
        document.add.submit();
}
function setFocus(){
    document.add.bankName.focus();
    }

</script>
<body onload="setFocus();">
<form name="add" method="post">
<%@ include file="/content/common/path.jsp" %>

<%@ include file="/content/common/message.jsp" %>

<table align="center" width="700" border="0" cellspacing="0" cellpadding="0" class="border">
 <tr height="30">
  <Td colspan="2" class="contenthead">Add Bank</Td>
 </tr>
  <tr height="30">
      <td class="text2"><font color="red">*</font>Name</td>
      <td class="text2"><input name="bankName" type="text" class="textbox" value="" size="50" ></td>
  </tr>
  <tr height="30">
    <td class="text1"><font color="red">*</font>Code</td>
    <td class="text1"><input name="bankCode" type="text" class="textbox" value="" size="50"  ></td>
  </tr>
  <tr height="30">
    <td class="text2"><font color="red">*</font>Address</td>
    <td class="text2">
        <textarea class="textbox" name="address"></textarea>
    </td>
  </tr>
  <tr height="30">
    <td class="text1"><font color="red">*</font>Phone No</td>
    <td class="text1"><input name="phoneNo" type="text" class="textbox" value="" size="50"  ></td>
  </tr>
  <tr height="30">
    <td class="text2"><font color="red">*</font>Account Code</td>
    <td class="text2"><input name="accntCode" type="text" class="textbox" value="" size="50"  ></td>
  </tr>
  <tr height="30">
    <td class="text1"><font color="red">*</font>Description</td>
    <td class="text1"><input name="description" type="text" class="textbox" value="" size="50"  ></td>
  </tr>
</table>
<br>
<br>
<center>
    <input type="button" class="button" value="Save" onclick="submitPage();" />
    &emsp;<input type="reset" class="button" value="Clear">
</center>
</form>
</body>
</html>
