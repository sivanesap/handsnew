<%-- 
    Document   : bpclTransactionHistoryUpload
    Created on : Jan 04, 2014, 12:38:56 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    function submitPage(value) {
        if (value == 'Proceed') {
            document.upload.action = '/throttle/saveUploadEmployee.do';
            document.upload.submit();
        } else {
            document.upload.action = '/throttle/handleUploadEmployee.do';
            document.upload.submit();
        }
    }
</script>

<html>
    <body>
        <form name="upload" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>

            <%
            if(request.getAttribute("errorMessage")!=null){
            String errorMessage=(String)request.getAttribute("errorMessage");                
            %>
            <center><b><font color="red" size="1"><%=errorMessage%></font></b></center>
                        <%}%>
            <!--            <table border="0" cellpadding="0" cellspacing="0" width="780" align="center">
                            <tr>
                                <td class="contenthead" colspan="4">Employee  History Upload</td>
                            </tr>
                            <tr>
                                <td class="text2">Select file</td>
                                <td class="text2"><input type="file" name="importBpclTransaction" id="importBpclTransaction" class="importBpclTransaction"></td>                             
                            </tr>
                            <tr>
                                <td colspan="4" class="text1" align="center" ><input type="button" value="Submit" name="Submit" onclick="submitPage(this.value)" >
                            </tr>
                        </table>-->
            <% int i = 1 ;%>
            <% int index = 0;%> 
            <%int oddEven = 0;%>
            <%  String classText = "";%>    
            <c:if test="${employeeHistoryDetails != null}">
                <table>
                    <th class="contenthead" colspan="42">File Uploaded Details&nbsp;:</th>
                    <tr height="45">
                        <th class="contentsub"><h3>Sno</h3></th>
                        <th class="contentsub"><h3>Status</h3></th>
                        <th class="contentsub"><h3>Employee Name</h3></th>
                        <th class="contentsub"><h3>Date of Birth</h3></th>
                        <th class="contentsub"><h3>Date of Joining </h3></th>
                        <th class="contentsub"><h3>Gender </h3></th>
                        <th class="contentsub"><h3>Blood Group </h3></th>
                        <th class="contentsub"><h3>Marital Status </h3></th>
                        <th class="contentsub"><h3>Father Name</h3></th>
                        <th class="contentsub"><h3>Mobile No</h3></th>
                        <th class="contentsub"><h3>Home Phone</h3></th>
                        <th class="contentsub"><h3>Qualification</h3></th>
                        <th class="contentsub"><h3>Email Id</h3></th>
                        <th class="contentsub"><h3>Department Name</h3></th>
                        <th class="contentsub"><h3>Designation Name</h3></th>
                        <th class="contentsub"><h3>Grade Name</h3></th>
                        <th class="contentsub"><h3>Company Name</h3></th>
                        <th class="contentsub"><h3>Reference Name</h3></th>
                        <th class="contentsub"><h3>Contract Employee</h3></th>
                        <th class="contentsub"><h3>Driver License No</h3></th>
                        <th class="contentsub"><h3>Valid Upto</h3></th>
                        <th class="contentsub"><h3>License Type</h3></th>
                        <th class="contentsub"><h3>License State</h3></th>
                        <th class="contentsub"><h3>Present Address</h3></th>
                        <th class="contentsub"><h3>Present City</h3></th>
                        <th class="contentsub"><h3>Present State</h3></th>
                        <th class="contentsub"><h3>Present Pincode</h3></th>
                        <th class="contentsub"><h3>Permanent Address</h3></th>
                        <th class="contentsub"><h3>Permanent City</h3></th>
                        <th class="contentsub"><h3>Permanent State</h3></th>
                        <th class="contentsub"><h3>Permanent Pincode</h3></th>
                        <th class="contentsub"><h3>Salary Type</h3></th>
                        <th class="contentsub"><h3>Consolidated Salary</h3></th>
                        <th class="contentsub"><h3>Is Eligible for ESI</h3></th>
                        <th class="contentsub"><h3>Is Eligible for PF</h3></th>
                        <th class="contentsub"><h3>Bank Account Number</h3></th>
                        <th class="contentsub"><h3>Bank Name</h3></th>
                        <th class="contentsub"><h3>Branch Name</h3></th>
                        <th class="contentsub"><h3>A/c Holder Name</h3></th>
                        <!--                        <th class="contentsub"><h3>User Access</h3></th>
                                                <th class="contentsub"><h3>User Name</h3></th>
                                                <th class="contentsub"><h3>Password</h3></th>
                                                <th class="contentsub"><h3>Role Name</h3></th>-->
                    </tr>
                    <c:forEach items="${employeeHistoryDetails}" var="employee">
                        <%
                      oddEven = index % 2;
                      if (oddEven > 0) {
                          classText = "text2";
                      } else {
                          classText = "text1";
                        }
                        %>
                        <tr>
                            <c:if test="${employee.status == 'N'}">
                                <td class="<%=classText%>" ><font color="green"><%=i++%></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="status" id="status<%=i%>" value="<c:out value="${employee.status}"/>" /> OK</font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="empCode" id="empCode<%=i%>" value="<c:out value="${employee.empCode}"/>" /><c:out value="${employee.name}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.dateOfBirth}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.DOJ}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.gender}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.bloodGrp}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.martialStatus}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.fatherName}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.mobile}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.phone}"/></font></td>
                                <td class="<%=classText%>" align="right"><font color="green"><c:out value="${employee.qualification}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.email}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.deptName}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.desigName}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.gradeName}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.companyName}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.referenceName}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.contractDriver}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.drivingLicenseNo}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.licenseDate}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.licenseType}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.licenseState}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.addr}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.city}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.state}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.pincode}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.addr1}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.city1}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.state1}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.pincode1}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.salaryType}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.basicSalary}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.esiEligible}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.pfEligible}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.bankAccountNo}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.bankName}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.bankBranchName}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><c:out value="${employee.nomineeName}"/></font></td>
<!--                                 <td class="<%=classText%>" ><font color="green"><input type="hidden" name="userAccess" id="userAccess" value="<c:out value="${employee.userAccess}"/>" /><c:out value="${employee.userAccess}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="userName" id="userName" value="<c:out value="${employee.userName}"/>" /><c:out value="${employee.userName}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="password" id="password" value="<c:out value="${employee.password}"/>" /><c:out value="${employee.password}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="roleName" id="roleName" value="<c:out value="${employee.roleName}"/>" /><c:out value="${employee.roleName}"/></font></td>-->
                            </c:if>
                            <c:if test="${employee.status == 'Y'}">
                                <td class="<%=classText%>" ><font color="red"><%=i++%></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="status" id="status<%=i%>" value="<c:out value="${employee.status}"/>" />Not OK</font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="empCode" id="empCode<%=i%>" value="<c:out value="${employee.empCode}"/>" /><c:out value="${employee.name}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.dateOfBirth}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.DOJ}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.gender}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.bloodGrp}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.martialStatus}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.fatherName}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.mobile}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.phone}"/></font></td>
                                <td class="<%=classText%>" align="right"><font color="red"><c:out value="${employee.qualification}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.email}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.deptName}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.desigName}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.gradeName}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.companyName}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.referenceName}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.contractDriver}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.drivingLicenseNo}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.licenseDate}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.licenseType}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.licenseState}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.addr}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.city}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.state}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.pincode}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.addr1}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.city1}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.state1}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.pincode1}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.salaryType}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.basicSalary}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.esiEligible}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.pfEligible}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.bankAccountNo}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.bankName}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.bankBranchName}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><c:out value="${employee.nomineeName}"/></font></td>
<!--                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="userAccess" id="userAccess" value="<c:out value="${employee.userAccess}"/>" /><c:out value="${employee.userAccess}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="userName" id="userName" value="<c:out value="${employee.userName}"/>" /><c:out value="${employee.userName}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="password" id="password" value="<c:out value="${employee.password}"/>" /><c:out value="${employee.password}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="roleName" id="roleName" value="<c:out value="${employee.roleName}"/>" /><c:out value="${employee.roleName}"/></font></td> -->
                            </c:if>

                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
                <br>
                <br>
                <br>
                <center>
                    <c:if test="${count != null}">
                    <input type="button" class="button" value="Proceed" onclick="submitPage(this.value)"/>
                    </c:if>
                    <c:if test="${count == null}">
                        <font color="red">Please check all records are already exists</font>
                    </c:if>
                </center>

            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>