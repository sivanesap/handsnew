<%-- 
    Document   : addEmployee
    Created on : Nov 7, 2008, 11:34:04 AM
    Author     : vijay
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
 
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>Parveen automobiles</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script> 
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">    
function copyAddress(value)
{
if(value == true){
var addAddr;
var addCity;
var addState;
var pinCode;

addAddr = document.addLect.addr.value ;
addCity = document.addLect.city.value;
addState = document.addLect.state.value;
pinCode = document.addLect.pincode.value;

document.addLect.addr1.value = addAddr;
document.addLect.city1.value = addCity;  
document.addLect.state1.value = addState;
document.addLect.pincode1.value  = pinCode;
}
else 
{
document.addLect.addr1.value  = "";
document.addLect.city1.value = "";  
document.addLect.state1.value = "";


}
}    
function checkPassword (value) {

var error = "";
var illegalChars = /[\W_]/; // allow only letters and numbers
if ((value.length < 6)) {
alert("Enter atleast 6 Characters");
document.addLect.password.focus();
}
else if (illegalChars.test(value)) {
alert("The password contains illegal characters");
document.addLect.password.focus();
}
return false;
}


function isEmail(s)
{

if(/[^@]+@[^@]+\.(com)|(co.in)|(in)$/.test(s))
return false;
alert("Email not in valid form!");
return true;	
}




function isChar(s){
if(!(/^-?\d+$/.test(s))){
return false;
}
return true;
}
//Grade Selection

function fillSelect(desigId) {
var url='/throttle/getGradeForDesig.do?desigId='+desigId;

if (window.ActiveXObject)
{
httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpRequest = new XMLHttpRequest();
}

httpRequest.open("POST", url, true);
httpRequest.onreadystatechange = function() {go(); } ;
httpRequest.send(null);
}

function go() {

if (httpRequest.readyState == 4)
{
if(httpRequest.status == 200)
{         
    if(httpRequest.responseText.valueOf()!=""){           
        var options = httpRequest.responseText.valueOf();
        options  = options.split(',');
        setOptions(options);
        //document.payVendorCredit.validation.value="submit";
        //document.getElementById("messag").innerHTML="";
        
    }else {
        //document.getElementById("messag").innerHTML="Vendor Data Does not Exists";     
        //document.payVendorCredit.validation.value="dontSubmit";
        //document.payVendorCredit.vendor_data.select();
        //document.payVendorCredit.vendor_data.focus();
    }
}
else
{
alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
}
}
}


function setOptions(val)
{
      var gradeId = document.addLect.gradeId;
      var id='';
      var name='';

      gradeId.options.length=1; 
      /*for(var i=0;i<options.length;i++){
        alert(options[i]);
      }   */   
      for(var i=0;i<val.length;i++){          
          if(val[i]!="" ){         
          id = val[i].split('-');     
          name = id[1];
          option1 = new Option(name,id[0]);
          gradeId.options[i] = option1;
          }
      }         
}

function initCs() {

var desig=document.getElementById('desigId').value;
if(desig!="") {
var list=document.getElementById("gradeId");
while (list.childNodes[0]) {
list.removeChild(list.childNodes[0])
}
fillSelect(desig);
}

//fillSelect(document.getElementById('desigId').value);
}

//window.onload=initCs;


//Ajax to Check userName
var httpRequest;
function getProfile(userName)
{

var url = '/throttle/checkUserName.do?userName='+ userName;

if (window.ActiveXObject)
{
httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpRequest = new XMLHttpRequest();
}

httpRequest.open("GET", url, true);

httpRequest.onreadystatechange = function() { processRequest(); } ;

httpRequest.send(null);
}


function processRequest()
{
if (httpRequest.readyState == 4)
{
if(httpRequest.status == 200)
{  
if(trim(httpRequest.responseText)!="") {    

document.getElementById("userNameStatus").innerHTML=httpRequest.responseText;
document.addLect.password.disabled=true;
document.addLect.password.disabled=true;
document.addLect.userName.select();
}else {
document.getElementById("userNameStatus").innerHTML="";
document.addLect.password.disabled=false;
document.addLect.password.focus();
}
}
else
{
alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
}
}
}


function submitPage(value){ 
if(value=='modify'){
        document.addLect.action = '/throttle/searchAlterEmp.do';
        document.addLect.submit();  
        return;
}
if(isEmpty(document.addLect.name.value)){
alert("Employee Name Field Should Not Be Empty");
document.addLect.name.focus();
return;
}else if(isChar(document.addLect.name.value)){
alert("Enter Valid Character for Employee Name ");
document.addLect.name.focus();
return;
}else if(isEmpty(document.addLect.qualification.value)){
alert("Qualification Field Should Not Be Empty");
document.addLect.qualification.focus();
return;
}else if(isChar(document.addLect.qualification.value)){
alert("Enter Valid Character for Qualification ");
document.addLect.qualification.focus();
return;
}else if(isEmpty(document.addLect.dateOfBirth.value)){
alert("Date of Birth Field Should Not Be Empty");
document.addLect.dateOfBirth.focus();
return;
}else if(isEmpty(document.addLect.dateOfJoining.value)){
alert("Date Of Joining Field Should Not Be Empty");
document.addLect.dateOfJoining.focus();
return;
}else if(isEmpty(document.addLect.bloodGrp.value)){
alert("Blood Group Field Should Not Be Empty");
document.addLect.bloodGrp.focus();
return;
}else if(isEmpty(document.addLect.fatherName.value)){
alert("Father Field Should Not Be Empty");
document.addLect.fatherName.focus();
return;
}else if((document.addLect.mobile.value != "") && isDigit(document.addLect.mobile.value)){
alert("Mobile Field Should Be Digit");
document.addLect.mobile.focus();
return;
}else if(isEmpty(document.addLect.phone.value)){
alert("Phone No Field Should Be Digit");
document.addLect.phone.focus();
return;
}else if((document.addLect.email.value!="") && isEmail(document.addLect.email.value)){
alert("Please Enter Valid email Address");
document.addLect.email.focus();
return;
}else if(isEmpty(document.addLect.addr.value)){
alert("Present Address Field Should Not Be Empty");
document.addLect.addr.focus();
return;
}else if(isEmpty(document.addLect.city.value)){
alert("Present City Field Should Not Be Empty");
document.addLect.city.focus();
return;
}else if(isEmpty(document.addLect.state.value)){
alert("Present State Field Should Not Be Empty");
document.addLect.state.focus();
return;
}else if(isEmpty(document.addLect.pincode.value)){
alert("Present Pincode Field Should Not Be Empty");
document.addLect.pincode.focus();
return;
}else if(isEmpty(document.addLect.addr1.value)){
alert("Permanant Address Field Should Not Be Empty");
document.addLect.addr1.focus();
return;
}else if(isEmpty(document.addLect.city1.value)){
alert("Permanant City Field Should Not Be Empty");
document.addLect.city1.focus();
return;
}else if(isEmpty(document.addLect.state1.value)){
alert("Permanant State Field Should Not Be Empty");
document.addLect.state1.focus();
return;
}else if(isEmpty(document.addLect.pincode1.value)){
alert("Permanant Pincode Field Should Not Be Empty");
document.addLect.pincode1.focus();
return;
}else if(document.addLect.deptid.value == "0"){
alert("Select any one Department");
document.addLect.deptid.focus();
return;
}else if(document.addLect.designationId.value == "0"){
alert("Select any one Designation");
document.addLect.designationId.focus();
return;
}else if(document.addLect.gradeId.value == "0"){
alert("Select any one Grade");
document.addLect.gradeId.focus();
return;
}

if(document.addLect.userStatus.checked==true){  
    if(isEmpty(document.addLect.userName.value)){
    alert("User Name Field Should Not Be Empty");
    document.addLect.userName.focus();
    return;
    }else if(!isName(document.addLect.userName.value)){
    alert("Enter Valid Character for User Name ");
    document.addLect.userName.focus();
    return;
    }else if(isEmpty(document.addLect.password.value)){
    alert("Please Enter Password");
    document.addLect.password.focus();
    return;
    }else if(document.addLect.roleId.value == "0"){
    alert("Please Select Employee Role ");
    document.addLect.roleId.focus();
    return;
    }
}

document.addLect.action = '/throttle/addEmployee.do';
document.addLect.submit();

}


function showTab(){
    if(document.addLect.userStatus.checked==true){
    document.getElementById("userStat").style.visibility="visible";
    document.getElementById("userStat1").style.visibility="visible";
    }else{
    document.getElementById("userStat").style.visibility="hidden";
    document.getElementById("userStat1").style.visibility="hidden";    
    } 
}    

function viewPage()
{
    document.addLect.action = '/throttle/EmpViewSearchPage.do';
    document.addLect.submit();
}


</script>
</head>

<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

<body onLoad="document.addLect.name.focus();setImages(0,1,0,0,0,0);">
<form name="addLect" method="post">   
<!-- copy there from end -->
<div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
<div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
<!-- pointer table -->
<table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
<tr>
<td >
<%@ include file="/content/common/path.jsp" %>
</td></tr></table>
<!-- pointer table -->

</div>
</div>
<br>
<br>
<!-- message table -->
<table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
<tr>
<td >
<%@ include file="/content/common/message.jsp" %>
</td></tr></table>
<!-- message table -->
<!-- copy there  end -->

<table  border="0" align="center" width="800" cellpadding="0" cellspacing="0" id="bg" class="border" >

<tr>
<td colspan="4" height="30" class="contenthead"><div class="contenthead">Personal Details</div></td>

</tr>

<tr >
<td class="text1" height="30"><font color="red">*</font>Name</td>
<td class="text1" height="30"><input type="text" name="name" size="20"  class="textbox" ></td>
<td class="text1" height="30"><font color="red">*</font>Qualification </td>
<td class="text1" height="30"><input type="text" name="qualification" size="20" class="textbox" ></td>
</tr>

<tr>
<td class="text2" height="30"><font color="red">*</font>Date Of Birth</td>
<td class="text2" height="30"><input type="text" name="dateOfBirth" readonly="readOnly" size="20" class="textbox"> <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.addLect.dateOfBirth,'dd-mm-yyyy',this)"  style="cursor:default; "/></td>
<td class="text2" height="30"><font color="red">*</font>Date Of Joining </td>
<td class="text2" height="30"><input type="text" name="dateOfJoining" size="20" readonly="readOnly" class="textbox"> <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.addLect.dateOfJoining,'dd-mm-yyyy',this)"  style="cursor:default; "/></td>
</tr>

<tr>
<td class="text1" height="30"><font color="red">*</font>Gender</td>
<td class="text1" height="30"><input type="radio" name="gender" checked value="Male" > Male <input type="radio" name="gender" value="Female" > Female </td>
<td class="text1" height="30"><font color="red">*</font>Blood Group</td>
<td class="text1" height="30"><input type="text" name="bloodGrp" size="20" class="textbox" > </td>
</tr>


<tr>
<td class="text2" height="30"><font color="red">*</font>Martial Status</td>
<td class="text2" height="30"><input type="radio" name="martial_Status" value="Married"  > Married <input type="radio" name="martial_Status" value="Unmarried" checked> UnMarried <input type="radio" name="martial_Status" value="Widow"> Widow</td>
<td class="text2" height="30"><font color="red">*</font>Father Name</td>
<td class="text2" height="30"><input name="fatherName" type="text" class="textbox" size="20"></td>
</tr>

<tr>
<td class="text1" height="30">&nbsp;&nbsp;Mobile No </td>
<td class="text1" height="30"><input type="text" name="mobile" size="20" class="textbox"></td>
<td class="text1" height="30"><font color="red">*</font>Phone No </td>
<td class="text1" height="30"><input type="text" name="phone" size="20" class="textbox" ></td>
</tr>

<tr>
<td class="text2" height="30">&nbsp;&nbsp;Email-Id</td>
<td class="text2" height="30" colspan="4"><input type="text" name="email" size="20" class="textbox"></td>    
</tr>
<tr>
<td class="contenthead" height="30" colspan="4"><b>Present Address</b></td>
</tr>

<tr>
<td class="text2" height="30"><font color="red">*</font>Address</td>
<td class="text2" height="30"> <input type="text" name="addr" size="20" class="textbox"  ></td>
<td class="text2" height="30"><font color="red">*</font>City</td>
<td class="text2" height="30"><input type="text" name="city" size="20" class="textbox" ></td>
</tr>
<tr>
<td class="text1" height="30"><font color="red">*</font>State</td>
<td class="text1" height="30"><input type="text" name="state" size="20" class="textbox" ></td>
<td class="text1" height="30"><font color="red">*</font>Pincode</td>
<td class="text1" height="30"><input type="text" name="pincode" size="20" class="textbox"  ></td>
</tr>
<tr>
<td class="contenthead" height="30" colspan="2"><b>Permanent Address</b> </td>
<td colspan="2" class="contenthead" height="30"><input type="checkbox" name="chec" onClick="copyAddress(this.checked)"> 
If  Temporary Address is Same As Permanent Address Click Here </td>
</tr>
<tr >
<td class="text2" height="30"><font color="red">*</font>Address </td>
<td class="text2" height="30"><input type="text" name="addr1" size="20" class="textbox" value="" ></td>
<td class="text2" height="30"><font color="red">*</font>City</td>
<td class="text2" height="30"><input type="text" name="city1" size="20" class="textbox" value="" ></td>
</tr>
<tr>    
<td class="text1" height="30"><font color="red">*</font>State</td>
<td class="text1" height="30"><input type="text" name="state1" size="20" class="textbox" value="" ></td>
<td class="text1" height="30"><font color="red">*</font>Pincode</td>
<td class="text1" height="30"><input type="text" name="pincode1" size="20" class="textbox" value="" ></td>
</tr>

<tr>
<td colspan="4" height="30" class="contenthead"><B>Official Details</B></td>
</tr>

<tr>
<td class="text2" height="30"><font color="red">*</font>Department Name</td>
<td class="text2" height="30"><select class="textbox" name="deptid" style="width:125px;">
<option value="0">---Select---</option>
<c:if test = "${DeptList != null}" >
<c:forEach items="${DeptList}" var="Dept"> 
<option value='<c:out value="${Dept.departmentId}" />'><c:out value="${Dept.name}" /></option>
</c:forEach >
</c:if>  	
</select></td>
<td class="text2" height="30"><font color="red">*</font>Designation Name </td>
<td class="text2" height="30"><select class="textbox" id="desigId" name="designationId" onChange="fillSelect(this.value)" style="width:125px;">
<option value="0">-Select-</option>
<c:if test = "${DesignaList != null}" >
<c:forEach  items="${DesignaList}" var="Desig"> 
<option value='<c:out value="${Desig.desigId}" />'><c:out value="${Desig.desigName}" /></option>
</c:forEach >
</c:if>
</select>
</td>
</tr>

<tr>  
<td class="text1" height="30"><font color="red">*</font>Grade Name </td>
<td class="text1" height="30"><select class="textbox" name="gradeId" style="width:125px;">
          <option value="0" > -- Select -- </option> 
<c:if test = "${GradeList != null}" >
<c:forEach  items="${GradeList}" var="grade"> 
<option value='<c:out value="${grade.gradeId}" />'><c:out value="${grade.gradeName}" /></option>
</c:forEach >
</c:if>          
</select></td>

<td class="text1" height="30"><font color="red">*</font>Company </td>
<td class="text1" height="30"><select class="textbox" name="cmpId" style="width:125px;">
<option value="0">-Select-</option>
<c:if test = "${companyList!= null}" >
<c:forEach  items="${companyList}" var="comp"> 
<option value='<c:out value="${comp.cmpId}" />'><c:out value="${comp.name}" />(<c:out value="${comp.companyType}" />)</option>
</c:forEach >
</c:if>    
</select></td>
</tr>



<tr >
<td height="30" colspan='4' align='center' class="text2"> <font color="#FF0033" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; "><div align="center" id="userNameStatus" style="height:25px; "></div></font> </td>
<td>
</tr>

<tr >
<td height="30" class="text2">Click here if need user privilege</td>
<td height="30" class="text2"><input type="checkbox" class="textbox" name="userStatus" onClick="showTab();" /></td>
<td height="30" class="text2">&nbsp;</td>
<td height="30" class="text2">&nbsp;</td>
<td>
</tr>

<tr id="userStat" style="visibility:hidden;">
<td height="30" class="text2"><font color="red">*</font>User Name :</td>
<td height="30" class="text2"><input type="text" class="textbox" name="userName"  onchange="getProfile(this.value)" /></td>
<td height="30" class="text2"><font color="red">*</font> Password :</td>
<td height="30" class="text2"><input type="password" class="textbox"  name="password" maxlength="10"  /></td>
<td>
</tr>

<tr id="userStat1"  style="visibility:hidden;">
<td class="text1" height="30"><font color="red">*</font>Employee Role</td>
<td class="text1" height="30"><select class="textbox" name="roleId" style="width:125px;">
<option value="0">-Select-</option>
<c:if test = "${roleList != null}" >
<c:forEach  items="${roleList}" var="role"> 
<option value='<c:out value="${role.roleId}" />'><c:out value="${role.roleName}" /></option>
</c:forEach >
</c:if>    
</select></td>

<td class="text1" height="30">&nbsp;</td>
<td class="text1" height="30">&nbsp;</td>
</tr>
</table>
<br>
<center>
<input type="button" class="button" value="Add" name="add" onClick="submitPage(this.name)">
<input type="button" class="button" value="View" name="View" onClick="viewPage();">
</center>
<br>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
