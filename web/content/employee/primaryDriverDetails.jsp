<%-- 
    Document   : alterEmployee
    Created on : Dec 22, 2013, 4:09:23 PM
    Author     : Administrator
--%>


<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <title>PAPL</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

         
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
            /*$( ".datepicker" ).datepicker({changeMonth: true,changeYear: true, minDate: 0, maxDate: "+10Y +10D" });

                }*/
        </script>
        <script language="javascript">



            function submitPage(value){
                if(value == "search"){
                    if(isEmpty(document.lecture.staffCode.value) && isEmpty(document.lecture.staffName.value)){
                        alert("please enter the staff code or staff name and search");
                        document.lecture.staffCode.focus();
                    }else if(document.lecture.staffCode.value !="" && document.lecture.staffName.value !=""){
                        alert("please enter the any one and serach");
                         document.lecture.staffCode.value="";
                            document.lecture.staffName.value="";
                    }else{
                        document.lecture.action = '/throttle/searchViewEmp.do';
                        document.lecture.submit();
                        return;
                    }
                    
                  }else if(value == 'add'){
                document.lecture.action = '/throttle/addEmployeePage.do';
                document.lecture.submit();
            }
             else if(value == 'ExcelExport'){
                document.lecture.action = "/throttle/handleEmpViewPage.do?param="+value;
                document.lecture.submit();

            }
            else if(value == 'modify'){

                document.lecture.action = '/throttle/searchAlterEmp.do';
                document.lecture.submit();

            }
           
        }   
        
        function importExcel() {
            document.lecture.action = '/throttle/content/employee/employeeImport.jsp';
            document.lecture.submit();
    }

            function copyAddress(value)
            {
                if(value == true){
                    var addAddr;
                    var addCity;
                    var addState;
                    var pinCode;

                    addAddr = document.lecture.addr.value ;
                    addCity = document.lecture.city.value;
                    addState = document.lecture.state.value;
                    pinCode = document.lecture.pincode.value;

                    document.lecture.addr1.value = addAddr;
                    document.lecture.city1.value = addCity;
                    document.lecture.state1.value = addState;
                    document.lecture.pincode1.value  = pinCode;
                }
                else
                {
                    document.lecture.addr1.value  = "";
                    document.lecture.city1.value = "";
                    document.lecture.state1.value = "";
                    document.lecture.pincode1.value   = "";

                }
            }



            function isChar(s){
                if(!(/^-?\d+$/.test(s))){
                    return false;
                }
                return true;
            }

            function isEmail(s)
            {

                if(/[^@]+@[^@]+\.(com)|(co.in)$/.test(s))
                    return false;
                alert("Email not in valid form!");
                return true;
            }

            function initCs() {
                var desig=document.getElementById('desigId');
                //alert(desig.value);
                desig.onchange=function() {
                    if(this.value!="") {
                        var list=document.getElementById("gradeId");
                        while (list.childNodes[0]) {
                            list.removeChild(list.childNodes[0])
                        }
                        fillSelect(this.value);
                    }
                }

                fillSelect(document.getElementById('desigId').value);
            }

            function go() {

                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var response = httpRequest.responseText;
                        var list=document.getElementById("gradeId");
                        var grade=response.split(',');
                        for (i=1; i<grade.length; i++) {
                            var gradeDetails =  grade[i].split('-');
                            var gradeId = gradeDetails[0];
                            var gradeName = gradeDetails[1];
                            var x=document.createElement('option');
                            var name=document.createTextNode(gradeName);
                            x.appendChild(name);
                            x.setAttribute('value',gradeId)
                            list.appendChild(x);
                        }
                    }
                }
            }


            function fillSelect(desigId) {
                //alert(desigId +" design Id ");
                var url='/throttle/getGradeForDesig.do?desigId='+desigId;

                if (window.ActiveXObject)
                {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest)
                {
                    httpRequest = new XMLHttpRequest();
                }

                httpRequest.open("POST", url, true);
                httpRequest.onreadystatechange = function() {go(); } ;
                httpRequest.send(null);
            }



            function showTab(){
                if(document.lecture.userStatus.checked==true){
                    document.getElementById("userStat").style.visibility="visible";
                    document.getElementById("userStat1").style.visibility="visible";
                }else{
                    document.getElementById("userStat").style.visibility="hidden";
                    document.getElementById("userStat1").style.visibility="hidden";
                }
            }




            var httpRequest;
            function getProfile(userName)
            {

                var url = '/throttle/checkUserName.do?userName='+ userName;

                if (window.ActiveXObject)
                {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest)
                {
                    httpRequest = new XMLHttpRequest();
                }

                httpRequest.open("GET", url, true);

                httpRequest.onreadystatechange = function() { processRequest(); } ;

                httpRequest.send(null);
            }


            function processRequest()
            {
                if (httpRequest.readyState == 4)
                {
                    if(httpRequest.status == 200)
                    {
                        if(trim(httpRequest.responseText)!="") {

                            document.getElementById("userNameStatus").innerHTML=httpRequest.responseText;
                            document.lecture.password.value='';
                            document.lecture.userCheck.value='exists';
                            document.lecture.password.disabled=true;
                            //document.lecture.password.disabled=true;
                            document.lecture.userName.select();
                        }else {
                            document.getElementById("userNameStatus").innerHTML="";
                            document.lecture.userCheck.value='notExists';
                            document.lecture.password.disabled=false;
                            document.lecture.password.focus();
                        }
                    }
                    else
                    {
                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                    }
                }
            }

            function nameSearch(){
                var oTextbox = new AutoSuggestControl(document.getElementById("staffName"),
                new ListSuggestions("staffName","/throttle/EmpNameSuggests.do?"));
            }

            function addPage()
            {
                document.lecture.action = '/throttle/addEmployeePage.do';
                document.lecture.submit();
            }

            function viewPage()
            {
                document.lecture.action = '/throttle/EmpViewSearchPage.do';
                document.lecture.submit();
            }


            function codeSearch(mode){
                if(mode == 0){
                    var oTextbox = new AutoSuggestControl(document.getElementById("staffCode"),new ListSuggestions("staffCode","/throttle/handleGetEmpCode.do?"));
                }else {
                    var oTextbox = new AutoSuggestControl(document.getElementById("staffName"),new ListSuggestions("staffName","/throttle/EmpNameSuggests.do?"));
                }

            }

            function setValues(){
                var staffCode = '<%=request.getAttribute("staffCode")%>';
                var staffName = '<%=request.getAttribute("staffName")%>';
//                alert(staffCode);
//                alert(staffName);
                if(staffCode != "null" && staffCode != ""){
                    document.lecture.staffCode.value = staffCode;
                }
                if(staffCode == "null"){
                    document.lecture.staffCode.value = "";
                }
                if(staffName != "null" && staffName != ""){
                    document.lecture.staffName.value = staffName;
                }
                if(staffName == "null"){
                    document.lecture.staffName.value = "";
                }
            }

        </script>
    </head>

  

    <body onload="setImages(1,1,0,0,0,0);setValues();document.lecture.staffId.focus();">
        <form name="lecture" method="post">
            <!-- copy there from end -->
                                <%@ include file="/content/common/path.jsp" %>
                        <%@ include file="/content/common/message.jsp" %>



            <table  border="0" align="center" width="600" cellpadding="0" cellspacing="0" class="border" bgcolor="#FFFFFF" style="vertical-align:top; "  >
                <tr>
                    <td class="text2" height="30">Employee Code</td>
                    <td class="text2" height="30"><input type="text" class="textbox" id="staffCode" name="staffCode" autocomplete="off" onFocus="codeSearch(0)" /></td>
                    <td class="text2" height="30">Employee Name</td>
                    <td class="text2" height="30"><input type="text" class="textbox" id="staffName"  name="staffName" autocomplete="off" onFocus="nameSearch(1)"/></td>
                    <td class="text2" height="30"></td>

                </tr>

                <tr height="35">
                    <td colspan="5" align="center" > &nbsp;
                        <input type="button" class="button" value="Search" name="search" onClick="submitPage(this.name)"/>
               &nbsp;&nbsp;   <input type="button"   value="Generate Excel" class="button" name="ExcelExport" onClick="submitPage(this.name)" style="width:150px">
          &nbsp;&nbsp;<input type="button"   value="Upload Employee Details" class="button" name="search" onClick="importExcel()" style="width:200px">
                    </td> </tr>
                     </table>
                         
           <br/>
           <br/>
           <br/>
           <c:if test = "${employeeDetails != null}" >
                   <% int index1 = 1; %>
            <table width="100%" align="center" border="0" id="table" class="sortable">
                <thead>
                <tr height="45">
                       <th><h3>S No</h3></th>
                      <th><h3>Employee Code</h3></th>
                      <th><h3>Employee Name</h3></th>
                      <th><h3>Date Of Birth</h3></th>
                      <th><h3>Father Name</h3></th>
                      <th><h3>Mobile Number</h3></th>
                      <th><h3>Vehicle No</h3></th>
                      <th><h3>Designation Name</h3></th>
                      <th><h3>Date Of Joining</h3></th>
                      <th><h3>License No</h3></th>
                      <th><h3>Refer By</h3></th>
                      <th><h3>Basic Salary</h3></th>
                      <th><h3>Status</h3></th>
                      <th><h3>Trip Code</h3></th>
                      <th><h3>Trip Status</h3></th>
                      <th><h3>Action</h3></th>
                </tr>
                </thead>
                            <tbody>
                 <c:forEach items="${employeeDetails}" var="employee">
                                 <%
                                        String classText = "";
                                        int oddEven1 = index1 % 2;
                                        if (oddEven1 > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }
                            %>
                                    <c:if test="${employee.status == 'B'}">
                                <tr>
                                    <td class="<%=classText%>" height="30" ><font color="red"><%=index1++%></font></td>
                                    <td class="<%=classText%>" height="30" ><font color="red"><c:out value="${employee.empCode}" />&nbsp;</font></td>
                                    <td class="<%=classText%>" height="30" ><font color="red"><c:out value="${employee.name}" />&nbsp;</font></td>
                                    <td class="<%=classText%>" height="30" ><font color="red"><c:out value="${employee.dateOfBirth}" />&nbsp;</font></td>
                                    <td class="<%=classText%>" height="30" ><font color="red"><c:out value="${employee.fatherName}" />&nbsp;</font></td>
                                    <td class="<%=classText%>" height="30" ><font color="red"><c:out value="${employee.mobile}" />&nbsp;</font></td>
                                    <td class="<%=classText%>" height="30" ><font color="red"><c:out value="${employee.mappedVehicle}" />&nbsp;</font></td>
                                    <td class="<%=classText%>" height="30" ><font color="red"><c:out value="${employee.desigName}" />&nbsp;</font></td>
                                    <td class="<%=classText%>" height="30" ><font color="red"><c:out value="${employee.DOJ}" />&nbsp;</font></td>
                                    <td class="<%=classText%>" height="30" ><font color="red"><c:out value="${employee.drivingLicenseNo}" />&nbsp;</font></td>
                                    <td class="<%=classText%>" height="30" ><font color="red"><c:out value="${employee.referenceName}" />&nbsp;</font></td>
                                    <td class="<%=classText%>" height="30" ><font color="red"><c:out value="${employee.basicSalary}" />&nbsp;</font></td>
                                    <td class="<%=classText%>" height="30" ><font color="red"><c:out value="${employee.status}" />&nbsp;</font></td>
                                    <td class="<%=classText%>" height="30" ><font color="red"><c:out value="${employee.tripCode}" />&nbsp;</font></td>
                                    <td class="<%=classText%>" height="30" ><font color="red"><c:out value="${employee.statusName}" />&nbsp;</font></td>
                                    <td class="<%=classText%>" height="30" ><font color="red">
                                         <c:if test="${employee.statusId == 10 || employee.statusId == 18}">
                                        <a href="searchViewEmp.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>">view</a>
                                         <c:if test="${employee.status == 'Y' }">
                                        <a href="alterEmployeeStatus.do?staffCode=<c:out value="${employee.empCode}"/> &staffId=<c:out value="${employee.staffId}"/>&staffName=<c:out value="${employee.name}"/>">Inactive</a>
                                         </c:if>
                                         </c:if>
                                        <c:if test="${employee.statusId != 10 && employee.statusId != 18}">
                                        <a href="searchViewEmp.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>">view</a>
                                        <c:if test="${employee.status != 'B' }">
                                        <a href="getAlterEmpPage.do?staffCode=<c:out value="${employee.empCode}"/> &staffId=<c:out value="${employee.staffId}"/>&staffName=<c:out value="${employee.name}"/>">Alter</a>
                                         </c:if>
                                         </c:if>
                                        </font></td>
                                    </tr>
                                    </c:if>

                                    <c:if test="${employee.status == 'Y' || employee.status == 'N' }">
                                    <tr>
                                    <td class="<%=classText%>" height="30" ><%=index1++%></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${employee.empCode}" />&nbsp;</td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${employee.name}" />&nbsp;</td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${employee.dateOfBirth}" />&nbsp;</td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${employee.fatherName}" />&nbsp;</td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${employee.mobile}" />&nbsp;</td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${employee.mappedVehicle}" />&nbsp;</td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${employee.desigName}" />&nbsp;</td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${employee.DOJ}" />&nbsp;</td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${employee.drivingLicenseNo}" />&nbsp;</td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${employee.referenceName}" />&nbsp;</td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${employee.basicSalary}" />&nbsp;</td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${employee.status}" />&nbsp;</td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${employee.tripCode}" />&nbsp;</td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${employee.statusName}" />&nbsp;</td>
                                    <td class="<%=classText%>" height="30" >
                                         <c:if test="${employee.statusId == 10 || employee.statusId == 18}">
                                        <a href="searchViewEmp.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>">view</a>
                                         <c:if test="${employee.status == 'Y' }">
                                        <a href="alterEmployeeStatus.do?staffCode=<c:out value="${employee.empCode}"/> &staffId=<c:out value="${employee.staffId}"/>&staffName=<c:out value="${employee.name}"/>">Inactive</a>
                                         </c:if>
                                         </c:if>
                                         <c:if test="${employee.statusId != 10 && employee.statusId != 18}">
                                        <a href="searchViewEmp.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>">view</a>
                                        <c:if test="${employee.status != 'B' }">
                                        <a href="getAlterEmpPage.do?staffCode=<c:out value="${employee.empCode}"/> &staffId=<c:out value="${employee.staffId}"/>&staffName=<c:out value="${employee.name}"/>">Alter</a>
                                         </c:if>
                                         </c:if>
                                    </td>
                                </tr>
                                    </c:if>
                                </tbody>
                            </c:forEach >
                       </table>
              </c:if>
              <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
          
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

