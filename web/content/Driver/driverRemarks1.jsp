<%-- 
    Document   : driverRemarks
    Created on : Sep 8, 2007, 7:03:00 PM
    Author     : entitle
--%>



<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>


        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>
    </head>

    <style type="text/css">
        .blink {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#CC3333;
        }
        .blink1 {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#F2F2F2;
        }
    </style>

    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#tabs").tabs();
        });
    </script>

    <script type="text/javascript">
        function submitPage(value)
        {
            var driName =document.driverRemarks.driName.value;
            var regno =document.driverRemarks.regno.value;
            document.driverRemarks.settlementId.value=value;

            if(!isEmpty(driName) || !isEmpty(regno)){
                document.driverRemarks.action="/throttle/DriverRemarks.do";
                document.driverRemarks.submit();
          
            }else {
                alert("Please Fill the Mandatory Feild");
                document.driverRemarks.driName.focus();
            }           

        <%
                    String settleId = (String) request.getAttribute("settleId");
                    System.out.println("settleId = " + settleId);
                    pageContext.setAttribute("settleId", settleId);
        %>
            }

            function RemarkSavePage(){
                var driName =document.driverRemarks.driName.value;
                var empId =document.driverRemarks.empId.value;
                var embId =document.driverRemarks.embId.value;
                var aliVehileNo =document.driverRemarks.aliVehileNo.value;
                var alighingRemark =document.driverRemarks.alighingRemark.value;
                
                if((!isEmpty(driName) || !isEmpty(aliVehileNo) || !isEmpty(alighingRemark) ))
                {
                    document.driverRemarks.action="/throttle/remarkSaveDriverV.do";
                    document.driverRemarks.submit();                  
                }
                else{
                    alert("Please Enter Driver Name");
                    document.driverRemarks.driName.focus();
                }
            }
            function RemarkSavePage1()
            {
                var driName =document.driverRemarks.driName.value;
                var alighingRemark =document.driverRemarks.alighingRemark.value;
                
                if(!isEmpty(driName) || !isEmpty(alighingRemark))
                {
                    document.driverRemarks.action="/throttle/remarkSaveDriverV.do";
                    document.driverRemarks.submit();
                }
            }

            function getDriverName(){
                var oTextbox = new AutoSuggestControl(document.getElementById("driName"),new ListSuggestions("driName","/throttle/handleDriverSettlement.do?"));

            }
            window.onload = getVehicleNos;

            function getVehicleNos(){
                var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/handleVehicleNo.do?"));
            }

            function setValues(){

                var driName = '<%=request.getAttribute("driName")%>';
                var regno = '<%=request.getAttribute("regno")%>';
                if(driName != "null"){
                    document.driverRemarks.driName.value=driName;
                }else{
                    document.driverRemarks.driName.value="";
                }
                
                if(!regno.valueOf(null)){
                    document.driverRemarks.regno.value=regno;
                }
            }
            function displayCollapse(){
                if(document.getElementById("exp_table").style.display=="block"){
                    document.getElementById("exp_table").style.display="none";
                    document.getElementById("openClose").innerHTML="Open";                }
                else{
                    document.getElementById("exp_table").style.display="block";
                    document.getElementById("openClose").innerHTML="Close";
                }
            }
    </script>
    <body  onload="setValues();">
        <form name="driverRemarks" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <td><input type="hidden" name="settlementId" id="settlementId" value=""/></td>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>
                <table width="870" cellpadding="0" cellspacing="2" align="center" border="0" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr>
                    <td><b>Search</b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;"><!--<img src="/throttle/images/close.jpg" width="25" height="25" />-->Close</span></td>
                </tr>
                <tr id="exp_table"  style="display: block;">
                    <td style="padding:15px;" align="right">
                        <div class="tabs" align="center" style="width:700;">
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red">*</font>Driver Name</td>
                                        <td height="30">
                                            <input name="driName" id="driName" type="text" class="textbox" size="20" value="" onKeyPress="getDriverName();" autocomplete="off">
                                        </td>
                                        <td>Vehicle No</td>
                                        <td><input name="regno" id="regno" type="text" class="textbox" size="20" value="" onKeyPress="getVehicleNos();" autocomplete="off"></td>
                                        <td><input type="button" class="button"  onclick="submitPage(0);" value="Search"></td>
                                    </tr>                                    
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br/>
            <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0" id="report" class="border">




                <br>
                <%int flag = 0;%>
                <c:if test="${driverEmbDetail != null}">
                    <%int index = 0;%>
                    <tr>
                        <td class="contenthead">Driver Name</td>
                        <td class="contenthead">Vehicle Number</td>
                        <td class="contenthead">Embarkment Date</td>
                        <td class="contenthead">Alighting Date</td>
                        <td class="contenthead">Alighting Status</td>
                        <td class="contenthead">Settlement Status</td>
                        <td class="contenthead">Remarks</td>
                    </tr>
                    <c:forEach items="${driverEmbDetail}" var="dri">
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                        %>
                        <tr>
                            <td class="<%=classText%>"><c:out value="${dri.empName}"/></td>
                            <c:choose>
                                <c:when test="${dri.vehicleNo == null}">
                                    <td class="<%=classText%>">&nbsp;</td>
                                </c:when>
                                <c:otherwise>
                                    <td class="<%=classText%>"><c:out value="${dri.vehicleNo}"/></td>
                                </c:otherwise>
                            </c:choose>
                            <c:choose>
                                <c:when test="${dri.embarkDate == null}">
                                    <td class="<%=classText%>">&nbsp;</td>
                                </c:when>
                                <c:otherwise>
                                    <td class="<%=classText%>"><c:out value="${dri.embarkDate}"/></td>
                                </c:otherwise>
                            </c:choose>
                            <c:choose>
                                <c:when test="${dri.alightDate == '1899-11-30' || dri.alightDate == null}">
                                    <td class="<%=classText%>">&nbsp;</td>
                                </c:when>
                                <c:otherwise>
                                    <td class="<%=classText%>"><c:out value="${dri.alightDate}"/></td>
                                </c:otherwise>
                            </c:choose>
                            <c:choose>
                                <c:when test="${dri.alghtingStatus == null}"><td class="<%=classText%>">&nbsp;</td></c:when>
                                <c:otherwise><td class="<%=classText%>"><c:out value="${dri.alghtingStatus}"/></td></c:otherwise>
                            </c:choose>
                            <c:choose>
                                <c:when test="${dri.actInd == null}"><td class="<%=classText%>">&nbsp;</td></c:when>
                                <c:otherwise>
                            <c:if test="${dri.actInd == 'Y'}">
                                <td class="<%=classText%>"><c:out value="Completed"/></td>
                                <%flag = 2;%>
                            </c:if>
                            <c:if test="${dri.actInd == 'N'}">
                                <td class="<%=classText%>"><c:out value="In Progress"/></td>
                                <%flag = 1;%>
                            </c:if>
                                </c:otherwise>
                            </c:choose>
                            <td class="<%=classText%>"><c:out value="${dri.remarks}"/></td>
                        </tr>
                        <tr>
                        <input name="embId" id="embId" type="hidden" value="<c:out value="${dri.embarkId}"/>">
                        <input name="empId" id="empId" type="hidden" value="<c:out value="${dri.driverId}"/>">
                        <input name="aliVehileNo" id="aliVehileNo" type="hidden" value="<c:out value="${dri.vehicleNo}"/>">
                        </tr>
                        <%index++;%>
                    </c:forEach>
                    <tr><td>&nbsp;</td></tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr><td>&nbsp;</td></tr>

                    <tr>

                    <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="TableMain" >

                        <c:if test="${driverEmbDetailVal == null }">
                            <tr>
                                <td class="texttitle2">Remarks</td>
                                <td class="texttitle2"><textarea cols="50%" name="alighingRemark" id="alighingRemark"></textarea></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td align="center"><input type="button" class="button"  onclick="RemarkSavePage();" value="Save"></td>
                            </tr>
                        </c:if>
                        <c:if test="${driverEmbDetailVal != null }">
                            <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table3" >
                                <tr>
                                    <td>
                                        <center><font color="blue"><b>Remark Save successfully  </b></font></center>
                                    </td>
                                </tr>
                            </table>
                        </c:if>
                    </table>
                    </tr>

                </table>
            </c:if>
            <c:if test="${driverEmbDetailSize == 0}">
                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="TableMain" >
                    <tr>

                        <td>
                            <center><font color="red"><b>This Driver Not Yet Embarked </b></font></center>
                        </td>
                    </tr>

                    <c:if test="${driverEmbDetailVal == null }">
                        <tr>
                            <td texttitle2>Remarks</td>
                            <td texttitle2><textarea cols="50%" name="alighingRemark" id="alighingRemark"></textarea></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="center"><input type="button" class="button"  onclick="RemarkSavePage1();" value="Save"></td>
                        </tr>
                    </c:if>
                    <c:if test="${driverEmbDetailVal != null }">
                        <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table3" >
                            <tr>
                                <td>
                                    <center><font color="blue"><b>Remark Save successfully  </b></font></center>
                                </td>
                            </tr>
                        </table>
                    </c:if>
                </table>
            </c:if>

        </form>
    </body>
</html>

