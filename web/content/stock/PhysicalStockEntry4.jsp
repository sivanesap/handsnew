<%-- 
    Document   : PhysicalStockEntry4
    Created on : Nov 1, 2012, 7:55:25 PM
    Author     : admin
--%>

<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.*,java.io.*,java.text.*" errorPage="" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
       <%
        Connection conn = null;
        int count = 0;
        try{

            float newPriceValue = 0.00f;
            float newPriceWithTax = 0.00f;
            float newQty = 0.00f;

            String fileName = "jdbc_url.properties";
            Properties dbProps = new Properties();
            InputStream is = getClass().getResourceAsStream("/"+fileName);
            dbProps.load(is);
            String dbClassName = dbProps.getProperty("jdbc.driverClassName");

            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");

            String companyId = (String) session.getAttribute("companyId");
            float TAX = 14.50f;
            String itemId = request.getParameter("itemId");
            String newStock = request.getParameter("newStock");
            if(newStock == null) {
                newStock = "0";
            } else {
                newQty = Float.parseFloat(newStock.trim());
            }
            String existPriceId = request.getParameter("existPrice");
            if(existPriceId == null || existPriceId.equals("")) {
                existPriceId = "0";
            }
            String newPrice = request.getParameter("newPrice");
            if(newPrice == null || newPrice.equals("")) {
                newPrice = "0";
            }

            newPriceValue = Float.parseFloat(newPrice.trim());
            newPriceWithTax = newPriceValue + (newPriceValue * (TAX / 100)) ;
            String newPriceId = "0";

            if((!existPriceId.equals("0") || newPriceValue > 0) && newQty > 0 ){
                Class.forName(dbClassName).newInstance();
                conn = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
                String getPriceId = " select last_insert_id() ";
                String insertPrice = " INSERT INTO papl_item_price_master (item_id, tax_percentage, price, price_type,price_with_tax, active_ind, created_on) VALUES (?,?,?,?,?,?, now())";
                String insertStock = " INSERT INTO papl_stock_balance (service_point_id, item_id, price_id, quantity) VALUES (?,?,?,?)";
                String updateStock = " UPDATE papl_stock_balance SET quantity = ? WHERE service_point_id = ? AND item_id = ? AND price_id = ?";

                PreparedStatement pstm = conn.prepareStatement(getPriceId);
                PreparedStatement pstm1 = conn.prepareStatement(insertPrice);
                PreparedStatement pstm2 = conn.prepareStatement(insertStock);
                PreparedStatement pstm3 = conn.prepareStatement(updateStock);

                // for New Price Only
                if(newPriceValue > 0 ) {
                    pstm1.setString(1, itemId);
                    pstm1.setFloat(2, TAX);
                    pstm1.setFloat(3, newPriceValue);
                    pstm1.setString(4, "INVOICE");
                    pstm1.setFloat(5, newPriceWithTax);
                    pstm1.setString(6, "Y");
                    pstm1.execute();
                    System.out.println("pstm1  "+pstm1);
                    
                    // to get New price Id
                    ResultSet res = pstm.executeQuery();
                    while(res.next()){
                        newPriceId = res.getString(1);
                    }
                     // to Insert Stock
                    pstm2.setString(1, companyId);
                    pstm2.setString(2, itemId);
                    pstm2.setString(3, newPriceId);
                    pstm2.setString(4, newStock);
                    pstm2.execute();
                } else {

                    pstm3.setString(1, newStock);
                    pstm3.setString(2, companyId);
                    pstm3.setString(3, itemId);
                    pstm3.setString(4, existPriceId);
                    System.out.println(pstm3);
                    pstm3.execute();
                }
                

               
             }

            String url = "PhysicalStockEntry1.jsp?message=Saved Successfully";
            response.sendRedirect(url);

            } catch (IOException ioe){
            System.err.println("Properties loading failed in AppConfig");

        }finally{


            if(conn != null) {
                conn.close();
            }

        }
            %>
    </body>
</html>
