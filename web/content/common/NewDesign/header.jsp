<html >
<head>
  <%@page language="java" contentType="text/html; charset=UTF-8"%>

  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <title>H&S SquareCloud</title>

  <link href="content/NewDesign/css/style.default.css" rel="stylesheet">
  <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
  <%
    String selectedLanguage = (String)session.getAttribute("paramName");
    System.out.println("selectedLanguage:"+selectedLanguage);
    if("ar".equals(selectedLanguage)){
    %>
    <link href="content/NewDesign/css/style.default-rtl.css" rel="stylesheet">
  <%
  }
  %>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->


<script>
          $(document).ready(function () {
           jQuery('body').addClass('stickyheader');
        });

  </script>



</head>

<body class="horizontal-menu-sidebar stickyheader">
<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
  
  <div class="leftpanel sticky-leftpanel" style="background: #418BCA;">
    
    <div class="logopanel">
<!--        <h1><span>[</span> bracket <span>]</span></h1>-->
<!--        <img src="/throttle/images/throttle_logo_header.png" alt="throttle Logo" width="210" height="30" />-->
<img src="/throttle/images/HSSupplyLogo.png" alt="Throttle" style="height:55px;width:70px;"/>
    </div><!-- logopanel -->
        
    <div class="leftpanelinner">    
        <br>
        <br>
        <!-- This is only visible to small devices -->