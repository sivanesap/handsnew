<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.contract.business.ContractTO" %>  
<%@ page import="java.util.*" %>      
<%@ page import="java.http.*" %>      
<%@ page import="ets.domain.service.business.ServiceTO" %>  
</head>
<body>
<form name="manageMaintenance" method="post">
        
<%@ include file="/content/common/path.jsp" %>

<!-- pointer table -->

<!-- message table -->

<%@ include file="/content/common/message.jsp" %>
<%
ArrayList maintenanceDetails = (ArrayList) request.getAttribute("maintenanceDetails"); 
     if(maintenanceDetails.size() != 0){
%>
<br>
<c:if test = "${maintenanceDetails != null}" >
    
<table width="451" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">

<tr>
<td width="45" height="30" class="contentsub"><div class="contentsub">S.No</div></td>
<td width="172" height="30" class="contentsub"><div class="contentsub">Maintenance</div></td>
<td width="158" height="30" class="contentsub"><div class="contentsub">Description</div></td>
<td align="center" width="76" height="30" class="contentsub"><div class="contentsub">Status</div></td>
</tr>
<% 
int index=0; 
%>

      <c:forEach items="${maintenanceDetails}" var="service"> 
<%
    String classText = "";
    int oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
<tr>
<td  height="30" class="<%=classText%>"><%=index+1%></td>

<td  height="30" class="<%=classText%>"><c:out value="${service.serviceName}"/></td>
<td  height="30" class="<%=classText%>"><c:out value="${service.desc}"/> </td>
<td align="center"  height="30" class="<%=classText%>"><c:out value="${service.status}"/>
<%index++;%>
</c:forEach>

</table>
</c:if>
<br>
<center>
<input type="button" class="button" value="Add" name="add" onClick="submitPage(this.name)">
<input type="button" class="button" value="Alter" name="alter" onClick="submitPage(this.name)">
<input type="button" class="button" value="Config" name="config" onClick="submitPage(this.name)">
</center>
<%
}else{
%>
<center>
<input type="button" class="button" value="Add" name="add" onClick="submitPage(this.name)">
</center>
<%
}
%>
<br>
</form>
</body>
<script language="javascript">
function submitPage(value){
if(value == "add"){
document.manageMaintenance.action = '/throttle/addMaintenance.do';
document.manageMaintenance.submit();
}else if(value == 'alter'){
document.manageMaintenance.action = '/throttle/alterMaintenance.do';
document.manageMaintenance.submit();
}else if(value == 'config'){
document.manageMaintenance.action = '/throttle/configMaintenance.do';
document.manageMaintenance.submit();
}
}
</script>

</html>
