<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

    <script language="javascript">
        $(document).ready(function () {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#custName').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "/throttle/getCustomerName.do",
                        dataType: "json",
                        data: {
                            custName: request.term
                        },
                        success: function (data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                $('#customerId').val('');
                                $('#custName').val('');
                            }
                            response(items);
                        },
                        error: function (data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#customerId').val(tmp[0]);
                    $('#custName').val(tmp[1]);
                    return false;
                }
            }).data("autocomplete")._renderItem = function (ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });
        $(document).ready(function () {
            $('#customerCode').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "/throttle/getCustomerCode.do",
                        dataType: "json",
                        data: {
                            customerCode: request.term
                        },
                        success: function (data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                $('#customerId').val('');
                                $('#customerCode').val('');
                            }
                            response(items);
                        },
                        error: function (data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#customerId').val(tmp[0]);
                    $('#customerCode').val(tmp[1]);
                    return false;
                }
            }).data("autocomplete")._renderItem = function (ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
        });


        //    function checkValue(value,id){
        //                if(value == '' && id=='custName'){
        //                   $('#customerCode').attr('readonly', true);
        //                   document.getElementById('customerId').value = '';
        //                }
        //                if(value == '' && id=='customerCode'){
        //                   $('#custName').attr('readonly', true);
        //                   document.getElementById('customerCode').value = '';
        //                }
        //            }
        //

        function submitPage(value) {
            if (value == "add") {
                document.manufacturer.action = '/throttle/handleViewAddCustomer.do';
                document.manufacturer.submit();
            } else if (value == 'search') {
                document.manufacturer.action = '/throttle/handleViewCustomer.do';
                document.manufacturer.submit();
            }
        }
    </script>
    
    <script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

	  <c:if test="${jcList != null}">
	  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
	  </c:if>
<div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="operations.label.ViewTransportCustomerDetails"  text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"> <spring:message code="operations.label.Operation"  text="default text"/></a></li>
          <li class=""><spring:message code="operations.label.ViewTransportCustomerDetails"  text="default text"/></li>

        </ol>
      </div>
      </div>
	  
    
    <div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">

    <body>
        <form name="manufacturer" method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            
           
                                <table  class="table table-info mb30 table-hover">
                                    <thead>
                                     <th colspan="8"> <spring:message code="operations.label.ViewTransportCustomerDetails"  text="default text"/> </th>
                                    </thead>
                                    <tr>
                                        <td height="30"><font color="red">*</font><spring:message code="operations.label.CustomerCode"  text="default text"/></td>
                                        <td><input type="text" name="customerCode" id="customerCode" value="<c:out value="${customerCode}"/>" style="width:260px;height:40px;"  class="form-control" onclick="checkValue(this.value, this.id)"></td>
                                        <td height="30"><font color="red">*</font><spring:message code="operations.label.CustomerName"  text="default text"/></td>
                                        <td><input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>" class="textbox"><input type="text" name="custName" id="custName" value="<c:out value="${custName}"/>" style="width:260px;height:40px;"  class="form-control" onclick="checkValue(this.value, this.id)"></td>

                                    </tr>

                                    <!--<tr>
                                    <td height="30"><font color="red">*</font>Customer Type</td>
                                    <td><select name="contractType" id="contractType" class="textbox" style="width:125px;"  onchange="showPartPayment(this.value)">
                                            <option value="" selected>--Select--</option>
                                            <option value="1" >Primary Contract</option>
                                    <%--<c:if test="${ContractType == '1'}">--%>
                                    <option value="1" selected>Primary Contract</option>
                                    <%--</c:if>--%>
                                    <%--<c:if test="${ContractType == '2'}">--%>
                                    <option value="2" selected>secondary Contract</option>
                                    <%--</c:if>--%>
                                    <option value="2" >secondary Contract</option>
                            </select></td>
                            <td height="30"></td>
                            <td></td>
                            </tr>-->
                                    <tr>
                                        <td colspan="4" align="center">
                                            <input type="button"   value="<spring:message code="operations.label.SEARCH"  text="default text"/>" class="btn btn-success" name="search" onClick="submitPage('search')">&nbsp;&nbsp;
                                            <input type="button"   value="<spring:message code="operations.label.ADD"  text="default text"/>" class="btn btn-success" name="Add" onClick="submitPage('add')">
                                        </td>
                                    </tr>
                                </table>
            <c:if test = "${CustomerLists != null}" >
               <table  class="table table-info mb30 table-hover">
                    <thead>
                        <tr >
                            <th><spring:message code="operations.label.SNo"  text="default text"/></th>
                            <th><spring:message code="operations.label.CustomerCode"  text="default text"/></th>
                            <th><spring:message code="operations.label.CustomerName"  text="default text"/></th>
                            <th><spring:message code="operations.label.Schedule"  text="default text"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0, sno = 1;%>
                        <c:forEach items="${CustomerLists}" var="customer">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr height="30">
                                <td align="left" ><%=sno%></td>
                                <td align="left" ><c:out value="${customer.customerCode}"/> </td>
                                <td align="left" ><c:out value="${customer.custName}"/> </td>
                                <td align="left" ><a href="/throttle/viewMothWiseVehicleSchedule.do?customerId=<c:out value="${customer.custId}"/>&customerName=<c:out value="${customer.custName}" />&customerCode=<c:out value="${customer.customerCode}"/>"><spring:message code="operations.label.Schedule"  text="default text"/></a></td>
                            </tr>
                            <%
                                        index++;
                                        sno++;
                            %>
                        </c:forEach>

                    </tbody>
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span><spring:message code="operations.label.EntriesPerPage"  text="default text"/></span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text"><spring:message code="operations.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="operations.label.of"  text="default text"/> <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

