<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<head>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>


    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




    <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
</head>
<script language="javascript">
    function submitPage() {
        
        if (document.getElementById('wareHouseName').value == '') {
            alert("please enter the WareHouse Name")
            $("#wareHouseName").focus();
        } 
         else if (document.getElementById('hubCity').value == '0') {
            alert("please select the WareHouse City")
            $("#hubCity").focus();
        } 
        else if (document.getElementById('wareHouseAddress').value == '') {
            alert("please enter the WareHouse address")
            $("#wareHouseAddress").focus();}

        else {
            
            $("#add").hide();
            document.manufacturer.action = '/throttle/addWareHouse.do';
            document.manufacturer.submit();
        }
    }
    function setFocus() {
        document.manufacturer.custCode.focus();
    }

    function validatePanNo() {
        var nPANNo = document.getElementById("panNo").value;
        if (nPANNo != "") {
            document.getElementById("panNo").value = nPANNo.toUpperCase();
            var ObjVal = nPANNo;
            var pancardPattern = /^([ABCFGHLJPT]{1})([A-Z]{2})([PCFHABTGL]{1})([A-Z]{1})(\d{4})([A-Z]{1})$/;
            var patternArray = ObjVal.match(pancardPattern);
            if (patternArray == null) {
                alert("PAN Card No Invalid ");
                return false;
            } else {
                return true;
            }
        } else {
            alert("Please enter pan no");
            return false;
        }
    }
</script>

<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#accountManager').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getAccountManager.do",
                    dataType: "json",
                    data: {
                        accountManagerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#accountManagerId').val('');
                            $('#accountManager').val('');
                        } else {
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#accountManager").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#accountManagerId').val(tmp[0]);
                $itemrow.find('#accountManager').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });




    var rowCount = 1;
    var sno = 0;
    var httpRequest;
    var httpReq;
    var rowIndex = 0;
    var styl = '';
    function addRow()
    {

        if (parseInt(rowCount) % 2 == 0)
        {
            styl = "text2";
        } else {
            styl = "text1";
        }




        sno++;

        var tab = document.getElementById("items");
        var iRowCount = tab.getElementsByTagName('tr').length;
//        alert("len:" + iRowCount);
        rowCount = iRowCount;
        var newrow = tab.insertRow(rowCount);
        sno = rowCount;
        rowIndex = rowCount;
        //alert("rowIndex:"+rowIndex);
        var cell = newrow.insertCell(0);
        var cell0 = "<td class='text1' height='25' > <input type='hidden'  name='billingAddressIds' value='' > " + sno + "</td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        var cell = newrow.insertCell(1);
        var cell0 = "<td class='text1' height='25' ><textarea name='billingNameAddress' id='billingNameAddress" + rowIndex + "' cols='100' rows='4' style='width:250px;height:40px'></textarea></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(2);
        var cell1 = "<td class='text1' height='25'><select class='form-control'  name='stateId' id='stateId" + rowIndex + "' required data-placeholder='Choose One' style='width:250px;height:40px'><option value='0'>--Select--</option><c:forEach items='${stateList}' var='Type'><option value='<c:out value='${Type.stateId}'/>'><c:out value='${Type.stateName}'/></option></c:forEach></select></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell1;

        cell = newrow.insertCell(3);
        var cell2 = "<td class='text1' height='25'><input name='gstNo' id='gstNo" + rowIndex + "' class='form-control'   type='text' style='width:250px;height:40px'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell2;

        cell = newrow.insertCell(4);
        cell2 = "<td class='text1' height='25'><select class='form-control' name='activeInd' id='activeInd" + rowIndex + "' style='width:250px;height:40px'><option value='Y'>YES</option> <option value='N'>NO</option></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell2;
        if (rowCount > 1) {
        cell = newrow.insertCell(5);
        var cell2 = "<td class='text1' height='25' ><input type='button' class='btn btn-info'   name='delete'  id='delete" + rowIndex + "'   value='Delete Row' onclick='deleteRow(this);' ></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell2;
        }
//        getItemNames(rowIndex);
//        var itemCode = document.getElementsByName("itemCode");
//        itemCode[rowCount - 1].focus();

        rowIndex++;
        rowCount++;

    }
    </script>
<script>
    function deleteRow(src) {
        rowIndex--;
        sno--;
        var oRow = src.parentNode.parentNode;
        var dRow = document.getElementById('items');
        dRow.deleteRow(oRow.rowIndex);
        document.getElementById("selectedRowCount").value--;
    }

</script>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="1"/>

    <script>
        function setAddValue() {
            var address = document.getElementById("custAddress").value;
            var city = document.getElementById("custCity").value;
            var State = document.getElementById("custState").value;
            var temp = State.split('-');
            var billingAddressIds = document.getElementsByName("billingAddressIds");
            var billingNameAddress = document.getElementsByName("billingNameAddress");
            var stateId = document.getElementsByName("stateId");

            for (var i = 0; i < billingAddressIds.length; i++) {
                billingNameAddress[0].value = address + "," + city;
                stateId[0].value = temp[1];
//                billingNameAddress[0].readOnly = true;
                stateId[0].readOnly = true;
            }
        }
    </script>
    

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Warehouse" text="Add Warehouse"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.WMSMaster" text="WMSMaster"/></a></li>
            <li class=""><spring:message code="hrms.label.AddWarehouse" text="Add Warehouse"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="setFocus();
                    addRow()">
                <form name="manufacturer"  method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover" id="bg" >
                        <thead>
                            <tr align="center">
                                <th colspan="4" align="center"  height="30"><div >Add WareHouse</div></th>
                        </tr>
                        </thead>
                        <tr>
                            <td  height="30"><font color="red">*</font>WareHouse Name</td>
                            <td  height="30"><input name="wareHouseName" id="wareHouseName" type="text" class="form-control" style="width:250px;height:40px" value="" maxlength="50"></td>
                            <td  height="30"><font color="red">*</font>WareHouse Address 1</td>
                            <td  height="30"><textarea class="form-control" style="width:250px;height:40px" name="wareHouseAddress" id="wareHouseAddress" ></textarea></td>
                        </tr>

                        <tr>
                            <td  height="30"><font color="red">*</font>WareHouse Address 2</td>
                            <td  height="30"><textarea class="form-control" style="width:250px;height:40px" name="address2" id="address2" ></textarea></td>
                            <td  height="30"><font color="red">*</font> Gst No</td>
                            <td  height="30"><input name="gstNo" id="gstNo" type="text" class="form-control" style="width:250px;height:40px" value=""  onKeyPress="return onKeyPressBlockNumbers(event);" ></td>
                        </tr>
                            
                        
                         <tr>
                            <td  height="30"><font color="red">*</font>Phone Numer</td>
                            <td  height="30"><input name="phoneNo" id="phoneNo" type="text" class="form-control" style="width:250px;height:40px" value="" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="12"></td>
                           
                            <td  height="30"><font color="red">*</font> Email</td>
                            <td  height="30"><input name="email" id="email" type="text" class="form-control" style="width:250px;height:40px" value=""  onKeyPress="return onKeyPressBlockNumbers(event);" ></td>
                        </tr>

                        <tr>
                            <td  height="30"><font color="red">*</font> City</td>

                            <td  height="30">  <select name="hubCity" id="hubCity" class="form-control" required data-placeholder="Choose One" style="width:250px;height:40px" onchange="setAddValue()">
                                    <option value="">Choose One</option>
                                    <c:if test = "${CityList != null}" >
                                        <c:forEach items="${CityList}" var="Type">
                                            <option value='<c:out value="${Type.cityName}" />-<c:out value="${Type.cityId}" />'><c:out value="${Type.cityName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>
                            <td  width="80" >Active </td>
                            <td  width="80"> <select name="activeInd" id="activeInd" class="form-control" style="width:250px;height:40px" style="width:125px;" >
                                    <option value="" selected>--select--</option>
                                    <option value="Y" >YES</option>
                                    <option value="N" >NO</option>
                                </select>
                            </td>
                        </tr>
                        
                         <tr>
                            <td  height="30"><font color="red">*</font> State</td>

                            <td  height="30">  <select name="state" id="state" class="form-control" required data-placeholder="Choose One" style="width:250px;height:40px" onchange="setAddValue()">
                                    <option value="">Choose One</option>
                                    <c:if test = "${stateList != null}" >
                                        <c:forEach items="${stateList}" var="Type">
                                            <option value='<c:out value="${Type.stateName}" />-<c:out value="${Type.stateId}" />'><c:out value="${Type.stateName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>
                            <td  height="30"><font color="red">*</font>Warehouse Code</td>
                            <td><input type="text" name="bfilCode" id="bfilCode" value="" class="form-control" style="width:250px;height:40px"/></td>
                             
                        </tr>
                         <tr>
                            <td  height="30"><font color="red">*</font>Hub Short Code</td>
                            <td><input type="text" name="hubCode" id="hubCode" value="" class="form-control" style="width:250px;height:40px"/></td>
                            <td  height="30"><font color="red">*</font>Customers</td>
                            <td  height="30">  
                                <select name="customerId" id="customerId" class="form-control" required data-placeholder="Choose One" style="width:250px;height:100px" multiple="">
                                    <option value="">Choose One</option>
                                    <c:if test = "${custList != null}" >
                                        <c:forEach items="${custList}" var="cu">
                                            <option value='<c:out value="${cu.custId}" />'><c:out value="${cu.custName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <script>
                            function validateManager(value) {
                                if (value == "") {
                                    $("#accountManagerId").val('');
                                }
                            }
                        </script>

                    </table>

                    <center>
                    </center>
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    <center>
                        <input type="button" value="Save" id="add" class="btn btn-success" onClick="submitPage();">
                        <!--&emsp;<input type="reset" class="btn btn-success" value="Clear">-->
                    </center>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
