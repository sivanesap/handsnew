<%--
   Document   : createBunkDetails
   Created on : 12 Sep, 2012, 10:58:49 AM
   Author     : Hari
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>
    </head>
    <style type="text/css">
        .blink {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#CC3333;
        }
        .blink1 {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#F2F2F2;
        }
    </style>
    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#tabs").tabs();
        });
    </script>
    <script type="text/javascript">
        function  savePage(){
            var message = "";
            if(document.getElementById("bunkName").value == ""){
                message += "Please Enter Bunk Name \n";
            }
            if(document.getElementById("fuelType").value == ""){
                message += "Please Enter Fuel Type \n";
            }
            if(document.getElementById("currRate").value == ""){
                message += "Please Enter Current Rate \n";
            }
            if(document.getElementById("location").value == ""){
                message += "Please Enter the Location \n";
            }
            if(document.getElementById("state").value == ""){
                message += "Please Enter the State \n";
            }
            if(document.getElementById("activeStatus").value == ""){
                message += "Please select the Active Status \n";
            }
            /*if(document.getElementById("remarks").value == ""){
            message += "Please Enter the Remarks \n";
        }*/
            //alert(message);
            if(message == ""){
                var alterBunk = document.getElementById("alterBunk").value;
                var bunkId = document.getElementById("bunkId").value;
                if(alterBunk == '0'){
                    document.addBunkDetails.action="/throttle/handleAddBunkDetails.do";
                    document.addBunkDetails.submit();
                } else {
                    document.addBunkDetails.action="/throttle/UpdateBunkDetails.do?bunkId="+bunkId;
                    document.addBunkDetails.submit();
                }
            } else {
                alert(message);
            }
        }
    </script>
    <body>
        <form name="addBunkDetails" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp"%>
            <center><font color="blue">&nbsp;</font></center>

            <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                <c:if test="${bunkalterList != null}">
                    <c:forEach items="${bunkalterList}" var="bal">
                        <tr>
                        <input type="hidden" name="alterBunk" id="alterBunk" value="1">
                        <input type="hidden" name="bunkId" id="bunkId" value="<c:out value="${bal.bunkId}"/>">
                        </tr>
                        <tr>
                            <td>Bunk Name</td>
                            <td  height="30"><input name="bunkName" id="bunkName" class="textbox" type="text" size="20" value="<c:out value="${bal.bunkName}"/>" autocomplete="off"></td>
                            <td>Fuel Type</td>
                            <td height="30"><input name="fuelType" id="fuelType" class="textbox" type="text" size="20" value="<c:out value="${bal.fuelType}" />"  autocomplete="off"></td>
                            <td>Current Rate</td>
                            <td height="30"><input name="currRate" id="currRate" class="textbox" type="text" size="20" value="<c:out value="${bal.currRate}"/>" autocomplete="off"></td>
                        </tr>
                        <tr>
                            <td>Location</td>
                            <td><input name="location" id="location" type="text" class="textbox" size="20" value="<c:out value="${bal.currlocation}"/>" autocomplete="off"></td>
                            <td>State</td>
                            <td><input name="state" id="state" type="text" class="textbox" size="20" value="<c:out value="${bal.bunkState}"/>" autocomplete="off"></td>
                            <td>Active Status</td>
                            <td><select name="activeStatus" id="activeStatus" class="textbox" style="width:100px;">
                                    <c:if test="${bal.bunkStatus == 'Y'}">
                                        <option value="Y" selected>Yes</option>
                                        <option value="N">No</option>
                                    </c:if>
                                    <c:if test="${bal.bunkStatus == 'N'}">
                                        <option value="N" selected>No</option>
                                        <option value="Y">Yes</option>
                                    </c:if>
                                </select>
                        </tr>
                        <tr>
                            <td>Remarks</td>
                            <td colspan="5"><textarea cols="50%" name="remarks" id="remarks"><c:out value="${bal.remarks}"></c:out></textarea></td>
                        </tr>
                    </c:forEach>            
                </c:if>
                <c:if test="${bunkalterList == null}">
                    <tr>
                    <input type="hidden" name="alterBunk" id="alterBunk" value="0">
                    <input type="hidden" name="bunkId" id="bunkId" value="">
                    </tr>
                    <tr>
                        <td>Bunk Name</td>
                        <td  height="30"><input name="bunkName" id="bunkName" class="textbox" type="text" size="20" value="" autocomplete="off"></td>
                        <td>Fuel Type</td>
                        <td height="30"><input name="fuelType" id="fuelType" class="textbox" type="text" size="20" value="" autocomplete="off"></td>
                        <td>Current Rate</td>
                        <td height="30"><input name="currRate" id="currRate" class="textbox" type="text" size="20" value="" autocomplete="off"></td>
                    </tr>
                    <tr>
                        <td>Location</td>
                        <td><input name="location" id="location" type="text" class="textbox" size="20" value="" autocomplete="off"></td>
                        <td>State</td>
                        <td><input name="state" id="state" type="text" class="textbox" size="20" value="" autocomplete="off"></td>
                        <td>Active Status</td>
                        <td><select name="activeStatus" id="activeStatus" class="textbox" style="width:100px;">
                                <option value="">--select--</option>
                                <option value="Y">Yes</option>
                                <option value="N">No</option>
                            </select>
                    </tr>
                    <tr>
                        <td>Remarks</td>
                        <td colspan="5"><textarea cols="50%" name="remarks" id="remark"></textarea></td>
                    </tr>
                </c:if>
                <tr>
                    <td align="center" colspan="6">
                        <input type="button" class="button"  onclick="savePage();" value="Save">
                        &emsp;<input type="reset" class="button" value="Clear">
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>