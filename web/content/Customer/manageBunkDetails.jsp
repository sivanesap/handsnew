<%--
    Document   : manageBunkDetails.jsp
    Created on : Sep 10, 2012, 1:36:35 PM
    Author     : Hari
--%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
    </head>
    <style type="text/css">
        .blink {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#CC3333;
        }
        .blink1 {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#F2F2F2;
        }
    </style>

    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#tabs").tabs();
        });
    </script>

    <script type="text/javascript">
        function submitPage(obj) {            
            var bunkName =document.viewbunkDetails.bunkName.value;
            if(obj.name == "search"){
                //if(!isEmpty(bunkName)){
                    document.viewbunkDetails.action="/throttle/viewBunkDetailsV.do";
                    document.viewbunkDetails.submit();
                /*}else {
                    alert("Please Enter Bunk Name");
                    document.viewbunkDetails.bunkName.focus();
                }*/            }
            if(obj.name == "add"){
                document.viewbunkDetails.action="/throttle/handleCreateBunkDetails.do";
                document.viewbunkDetails.submit();
            }
        }

        //function getBunkName(){
        //  var oTextbox = new AutoSuggestControl(document.getElementById("bunkName"),new ListSuggestions("driName","/throttle/handleDriverSettlement.do?"));

        //}
        // window.onload = getVehicleNos;

        //function getVehicleNos(){
        //  var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/handleVehicleNo.do?"));
        // }
        function displayCollapse(){
            if(document.getElementById("exp_table").style.display=="block"){
                document.getElementById("exp_table").style.display="none";
                document.getElementById("openClose").innerHTML="Open";                }
            else{
                document.getElementById("exp_table").style.display="block";
                document.getElementById("openClose").innerHTML="Close";
            }
        }
    </script>
    <body>
        <form name="viewbunkDetails" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <td><input type="hidden" name="settlementId" id="settlementId" value=""/></td>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>

            <table width="870" cellpadding="0" cellspacing="2" align="center" border="0" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr>
                    <td><b>Bunk Master - Search</b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;"><!--<img src="/throttle/images/close.jpg" width="25" height="25" />-->Close</span></td>
                </tr>
                <tr id="exp_table"  style="display: block;">
                    <td style="padding:15px;" align="right">
                        <div class="tabs" align="center" style="width:700;">
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red">*</font>Bunk Name</td>
                                        <td height="30">
                                            <input name="bunkName" id="bunkName" type="text" class="textbox" size="20" value="" onKeyPress="getBunkName();" autocomplete="off">
                                        </td>
                                        <td><input type="button" class="button" name="search" onclick="submitPage(this);" value="Search"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br/>
            <c:if test="${bunkList != null}">
                    <%int flag = 0;
                                int index = 0;
                    %>
                    <table  border="0" class="border" align="center" width="95%" cellpadding="0" id="report" cellspacing="0" id="bg">
                    <tr>
                        <td class="contentsub">S.No</td>
                        <td class="contentsub">Bunk Name</td>
                        <td class="contentsub">Fuel Type</td>
                        <td class="contentsub">Current Rate</td>
                        <td class="contentsub">Location</td>
                        <td class="contentsub">State</td>
                        <td class="contentsub">Status</td>
                        <td class="contentsub">Alter</td>
                    </tr>
                    <c:forEach items="${bunkList}" var="bd">
                        <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                        %>
                        <tr>
                            <td class="<%=classText%>"  height="30"><%=index + 1%></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${bd.bunkName}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${bd.fuelType}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${bd.currRate}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${bd.currlocation}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${bd.bunkState}"/></td>
                            <c:if test="${bd.bunkStatus == 'Y'}">
                                <td class="<%=classText%>"  height="30"><c:out value="Active"/></td>
                                <%flag = 2;%>
                            </c:if>
                            <c:if test="${bd.bunkStatus == 'N'}">
                                <td class="<%=classText%>"  height="30"><c:out value="In Active"/></td>
                                <%flag = 1;%>
                            </c:if>
                            <td class="<%=classText%>"  height="30"><a href="/throttle/alterbunkDetails.do?bunkId=<c:out value="${bd.bunkId}"/>"> Edit</a></td>


                        </tr>
                        <%
                                    index++;
                        %>
                    </c:forEach>
                    <tr>
                        <td colspan="8" >
                            <table width="830" cellpadding="0" cellspacing="2" border="0" align="center">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align="center"><input type="button" class="button" name="add" onclick="submitPage(this);" value="Add"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </c:if>
            <c:if test="${bunkList == null}">
                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align="center"><input type="button" class="button" name="add" onclick="submitPage(this);" value="Add"></td>
                                </tr>
                            </table>
            </c:if>
        </form>
    </body>
</html>