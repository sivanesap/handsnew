<%
        if(session == null) {
            response.sendRedirect("login.do");
        }
        String user = "101";
        Date today = new Date();
        SimpleDateFormat sdftSql = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        int status = 0;
        int aStatus = 0;
        Connection conn = null;
        try{
            //Identity No,Device Id,Trip Id,Issuer Name Code,Amount,datetime            
            String tripId = request.getParameter("TripId");
            String identityNo = request.getParameter("IdentityNo");
            String deviceId = request.getParameter("DeviceId");
            String issuerCode = request.getParameter("IssuerNameCode");
            String amount = request.getParameter("Amount");
            String datetime = request.getParameter("datetime"); //07-08-2012$15:48:46&
            String date = "";
            String time = "";
            if(datetime != null && datetime.contains("$")) {
                date = datetime.split("\\$")[0];
                time = datetime.split("\\$")[1];
                date = date.split("-")[2]+"-"+date.split("-")[1]+"-"+date.split("-")[0];
                datetime = date+" "+time;
            } else if(datetime != null && datetime.contains(" ")) {
                date = datetime.split(" ")[0];
                time = datetime.split(" ")[1];
                date = date.split("-")[2]+"-"+date.split("-")[1]+"-"+date.split("-")[0];
                datetime = date+" "+time;
            }else {
                datetime = sdftSql.format(today);
            }

            String applicationPath = application.getRealPath("WEB-INF/classes");
            applicationPath = applicationPath.replace("\\", "/");
            java.io.FileInputStream fis = new java.io.FileInputStream(applicationPath+"/jdbc_url.properties");
            java.util.Properties props = new java.util.Properties();
            props.load(fis);
            String driverClass = props.getProperty("jdbc.driverClassName");
            String jdbcUrl = props.getProperty("jdbc.url");
            String userName = props.getProperty("jdbc.username");
            String password = props.getProperty("jdbc.password");
            Class.forName(driverClass);
            conn = DriverManager.getConnection(jdbcUrl, userName, password);
            
            Statement stmtTrip = conn.createStatement();
	    ResultSet rsTrip = stmtTrip.executeQuery("SELECT status FROM ra_trip_open_close_epos where TripId="+tripId+" and IdentityNo="+identityNo);
	    String tripStatus="";
	    while(rsTrip.next()){
		tripStatus = rsTrip.getString("status");
	    }
	   if(tripStatus.equals("Open") || tripStatus.equals("Close")){            
		    Statement stmt = conn.createStatement();
		    ResultSet rs = stmt.executeQuery("SELECT Advance_Issuer_Id, Advance_Issuer_Name, LocationId FROM ra_advance_issuername_master_epos where "
			    + "Advance_Issuer_Id='"+issuerCode+"'");
		    String issuerId="", issuerName="", locationId="";
		    while(rs.next()){
			issuerId = rs.getString("Advance_Issuer_Id");
			issuerName = rs.getString("Advance_Issuer_Name");
			locationId = rs.getString("LocationId");
		    }

		    System.out.println("here 1");
                    FPLogUtils.fpErrorLog((new StringBuilder()).append("1"));
		    PreparedStatement pstmt = conn.prepareStatement("INSERT INTO ra_trip_advance_epos "
			    + "( TripId, IdentityNo, DeviceId, Advance_Issuer_Id, "
			    + "Amount, StageId, Remarks, AdvDateTime, Act_Ind, CreatedBy,CreatedDate) VALUES (?,?,?,?,?,?,?,?,?,?,now())");
		    pstmt.setString(1, tripId);
		    pstmt.setString(2, identityNo);
		    pstmt.setString(3, deviceId);
		    pstmt.setString(4, issuerId);
		    pstmt.setString(5, amount);
		    pstmt.setString(6, locationId);
		    pstmt.setString(7, "-");
		    pstmt.setString(8, datetime);
		    pstmt.setString(9, "Y");
		    pstmt.setString(10, user);
		    status = pstmt.executeUpdate();
		    System.out.println("here 2 Status is : "+status);		    
		    if(status==1){

                        Statement stmt2 = conn.createStatement();
                        ResultSet rs2 = stmt.executeQuery("SELECT totalallowance FROM ra_trip_open_close_epos where TripId="+tripId);
                        String tAllowance="";
                        while(rs2.next()){
                            tAllowance = rs2.getString("totalallowance");
                        }
                        if(("").equals(tAllowance) || tAllowance==null){tAllowance="0";}
                        double tAllow = Double.parseDouble(tAllowance) + Double.parseDouble(amount);
                        
                        PreparedStatement pstmt2 = conn.prepareStatement("UPDATE ra_trip_open_close_epos SET totalallowance=? WHERE  TripId=?");
			pstmt2.setDouble(1, tAllow);
			pstmt2.setString(2, tripId);
			aStatus = pstmt2.executeUpdate();
			FPLogUtils.fpErrorLog((new StringBuilder()).append("Exception in EPOS Advance --> "+aStatus));

			System.out.println("datetime : "+datetime);
			String[] temp = null, temp1 = null;
			temp = datetime.split(" ");
			String sDate = temp[0];

			Statement stmt1 = conn.createStatement();
			ResultSet rs1 = stmt1.executeQuery("SELECT Location_Name FROM ra_operation_location_master_epos lm, ra_trip_open_close_epos toc "
				+ "where lm.Location_Id=toc.OpenLocationId and TripId="+tripId);
			String locationName="";
			while(rs1.next()){
			    locationName = rs1.getString("Location_Name");
			}
			PreparedStatement pstmt1 = conn.prepareStatement("INSERT INTO tripallowancedetails "
			    + "(tripsheetid, stageid, date, amount, paidby, remarks, createddate) "
			    + " VALUES (?,?,?,?,?,?,now())");
			pstmt1.setString(1, tripId);
			pstmt1.setString(2, locationName);
			pstmt1.setString(3, sDate);
			pstmt1.setString(4, amount);
			pstmt1.setString(5, issuerName);
			pstmt1.setString(6, "from EPOS");
			aStatus = pstmt1.executeUpdate();
		    }
		    out.print((status+"$").trim());
	    }
	    else{out.println("0$");}
            if(conn != null) {
                conn.close();
            }
        }catch(Exception e) {
            out.println("0$");
            System.out.println("Exception in EPOS Advance "+e.getMessage());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Exception in EPOS Advance --> ").append(e.getMessage()).toString());
        }
        finally{
            if(conn != null) {
                conn.close();

            }
        }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*,java.text.SimpleDateFormat,java.util.Date,ets.domain.util.FPLogUtils" %>