<%
    Connection conn = null;
    try {
        String identityNo = request.getParameter("IdentityNo");
        String routeId = request.getParameter("RouteCode");
        FPLogUtils.fpErrorLog((new StringBuilder()).append("identityNo --> "+identityNo));
        FPLogUtils.fpErrorLog((new StringBuilder()).append("routeId --> "+routeId));

        String applicationPath = application.getRealPath("WEB-INF/classes");
        applicationPath = applicationPath.replace("\\", "/");
        java.io.FileInputStream fis = new java.io.FileInputStream(applicationPath + "/jdbc_url.properties");
        java.util.Properties props = new java.util.Properties();
        props.load(fis);
        String driverClass = props.getProperty("jdbc.driverClassName");
        String jdbcUrl = props.getProperty("jdbc.url");
        String userName = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");
        Class.forName(driverClass);
        String locationId = "";
        conn = DriverManager.getConnection(jdbcUrl, userName, password);        
        Statement stmt1 = conn.createStatement();
        ResultSet rs1 = stmt1.executeQuery("SELECT em.Emp_Id,em.Emp_Name FROM papl_emp_master em, papl_emp_desig ed, ra_vehicle_driver_map_epos vdm "
                + " where em.Emp_Id=ed.Emp_Id and vdm.DriverId=em.Emp_Id and ed.Desig_Id=1034 and em.Active_Ind='Y' and vdm.Act_Ind='Y'"
                + " and vdm.IdentityNo='" + identityNo + "'");
        String empId = "", empName = "";
        while (rs1.next()) {
            empId = rs1.getString("Emp_Id");
            empName = rs1.getString("Emp_Name");
        }
        if(!empId.equals("") && !empName.equals("")){
        	out.print((empId + "$" + empName +"$").trim());
        }else{
        	out.print(("0$Not Assigned$").trim());
        }
        Statement stmt2 = conn.createStatement();
        ResultSet rs2 = stmt2.executeQuery("SELECT Eligible_Count FROM ra_trip_eligible_status_epos where Route_Id="+routeId);
        int eCount = 0;
        while (rs2.next()) {
            String eStatus = rs2.getString("Eligible_Count");
	    if(!eStatus.equals("") && eStatus!=null){
		eCount = Integer.parseInt(eStatus);
	    }
        }        
        Statement stmt3 = conn.createStatement();
        String st = "Open";
	ResultSet rs3 = stmt3.executeQuery("SELECT max(TripId) as LastTripId FROM ra_trip_open_close_epos where Status='"+st+"' and IdentityNo='"+identityNo+"'");
	int tripId = 0;        
	while (rs3.next()) {            
	    int tId = rs3.getInt("LastTripId"); 
	    FPLogUtils.fpErrorLog((new StringBuilder()).append(tId));
	    if(tId!=0 ){            
		tripId = tId;
	    }            
	}
	if (eCount > 3 || tripId != 0) {
	    out.print(("N$").trim());
        } else {
            out.print(("Y$").trim());
        }
        if (conn != null) {
            conn.close();
        }
    } catch (Exception e) {
        out.print("0$");
        FPLogUtils.fpErrorLog((new StringBuilder()).append("Exception in EPOS Trip Eligible and Driver Status --> ").append(e.getMessage()).toString());
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*,ets.domain.util.FPLogUtils" %>