<%
        Date today = new Date();
        SimpleDateFormat sdftSql = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Connection conn = null;
        String date = "";
        String time = "";
        String NewTripId = "";
        int uStatus = 0;        
        int bStatus = 0;
        int tStatus = 0;
        String userId = "101";
        String tripId = request.getParameter("TripId");
        String identityNo = request.getParameter("IdentityNo");
        String deviceId = request.getParameter("DeviceId");
        String inKm = request.getParameter("InKM");        
        String loadedTonnage = request.getParameter("LoadedTonnage");
        String deliveredTonnage = request.getParameter("DeliveredTonnage");
        String closeLocationId = request.getParameter("LocationCode");
        String closeDateTime = request.getParameter("Datetime");
                
        try{
            if(closeDateTime != null && closeDateTime.contains("$")) {
                date = closeDateTime.split("\\$")[0];
                date = date.split("-")[2]+"-"+date.split("-")[1]+"-"+date.split("-")[0];
                time = closeDateTime.split("\\$")[1];
                closeDateTime = date+" "+time;
            } else if(closeDateTime != null && closeDateTime.contains(" ")) {
                date = closeDateTime.split(" ")[0];
                date = date.split("-")[2]+"-"+date.split("-")[1]+"-"+date.split("-")[0];
                time = closeDateTime.split(" ")[1];
                closeDateTime = date+" "+time;
            } else {
                closeDateTime = sdftSql.format(today);
            }
            String applicationPath = application.getRealPath("WEB-INF/classes");
            applicationPath = applicationPath.replace("\\", "/");
            java.io.FileInputStream fis = new java.io.FileInputStream(applicationPath+"/jdbc_url.properties");
            java.util.Properties props = new java.util.Properties();
            props.load(fis);

            String driverClass = props.getProperty("jdbc.driverClassName");
            String jdbcUrl = props.getProperty("jdbc.url");
            String userName = props.getProperty("jdbc.username");
            String password = props.getProperty("jdbc.password");
            Class.forName(driverClass);
            conn = DriverManager.getConnection(jdbcUrl, userName, password);

            Statement stmt1 = conn.createStatement();
            ResultSet rs1 = stmt1.executeQuery("SELECT ccr.Tonnage_Rate FROM ra_customer_contract_rates ccr, ra_trip_open_close_epos toc, "
                    + "papl_contract_details cd where  ccr.Route_Id=toc.RouteId and toc.CustomerId=cd.Cust_ID and ccr.Contract_Id=cd.Contract_Id "
                    + "and ccr.Active_Ind='Y' and cd.Active_Ind='Y' and toc.TripId="+tripId);
            String tonageRate="";
            while(rs1.next()){
                tonageRate = rs1.getString("Tonnage_Rate");
            }
            float totTonAmount = 0.0f;
            System.out.println("tonageRate : "+tonageRate);
            
            FPLogUtils.fpErrorLog((new StringBuilder()).append("tonageRate: "+tonageRate));
            
            if(tonageRate.equals("")){tonageRate="0";}
            totTonAmount = (Float.parseFloat(tonageRate) * Float.parseFloat(loadedTonnage));


            Statement stmt2 = conn.createStatement();
            ResultSet rs2 = stmt1.executeQuery("SELECT Toll_Amount,Driverbata FROM ra_route_master rm, ra_trip_open_close_epos toc where rm.route_id=toc.routeid "
                    + "and toc.TripId="+tripId);
            String bataRate="", tollAmount="";
            while(rs2.next()){
                bataRate = rs2.getString("Driverbata");
                tollAmount = rs2.getString("Toll_Amount");
            }
            float bataAamount = 0.0f;
            //System.out.println("bataRate : "+bataRate);
            
            //FPLogUtils.fpErrorLog((new StringBuilder()).append("bataRate: "+bataRate));
            
            if(bataRate.equals("")){bataRate="0";}
            bataAamount = (Float.parseFloat(bataRate) * Float.parseFloat(loadedTonnage));
	
	    //FPLogUtils.fpErrorLog((new StringBuilder()).append("bataAamount: "+bataAamount));		
            
            PreparedStatement pstmt = conn.prepareStatement("UPDATE ra_trip_open_close_epos SET InKM = ?, CloseLocationId=?, CloseDateTime = ?, "
                    + "TotalTonnage =?, DeliveredTonnage =?, TotalTonAmount = ?, CloseDeviceId = ?, Status = ?, ModifiedBy = ?, ModifiedDate = now()"
                    + " WHERE TripId = ?");
            pstmt.setString(1, inKm);
            pstmt.setString(2, closeLocationId);
            pstmt.setString(3, closeDateTime);
            pstmt.setString(4, loadedTonnage);
            pstmt.setString(5, deliveredTonnage);
            pstmt.setFloat(6, totTonAmount);
            pstmt.setString(7, deviceId);
            pstmt.setString(8, "Close");
            pstmt.setString(9, userId);
            pstmt.setString(10, tripId);
            uStatus = pstmt.executeUpdate();
            
            if(uStatus==1){
                String[] temp = null, temp1 = null;
                temp = closeDateTime.split(" ");
                String sDate = temp[0];
                temp1 = sDate.split("-");
                String sDate1 = temp1[2] + "-" + temp1[1] + "-" + temp1[0];

                System.out.println("closeDateTime : "+closeDateTime);
                PreparedStatement pstmt1 = conn.prepareStatement("INSERT INTO trip_expenses_details (tripsheetid, trip_date, amount, expensesdesc, "
                        + "remarks, createddate, expense_flag) values (?, ?, ?, ?, ?, now(), ?)");
                pstmt1.setString(1, tripId);
                pstmt1.setString(2, sDate);
                pstmt1.setFloat(3, bataAamount);
                pstmt1.setString(4, "from EPOS");
                pstmt1.setString(5, "Driver Bata");
                pstmt1.setString(6, "2");
                bStatus = pstmt1.executeUpdate();

                PreparedStatement pstmt2 = conn.prepareStatement("INSERT INTO trip_expenses_details (tripsheetid, trip_date, amount, expensesdesc, "
                        + "remarks, createddate, expense_flag) values (?, ?, ?, ?, ?, now(), ?)");
                pstmt2.setString(1, tripId);
                pstmt2.setString(2, sDate);
                pstmt2.setFloat(3, Float.parseFloat(tollAmount));
                pstmt2.setString(4, "from EPOS");
                pstmt2.setString(5, "Toll Amount");
                pstmt2.setString(6, "1");
                tStatus = pstmt2.executeUpdate();
            }
            
            FPLogUtils.fpErrorLog((new StringBuilder()).append("bStatus: "+bStatus));
            FPLogUtils.fpErrorLog((new StringBuilder()).append("tStatus: "+tStatus));
            
            out.print((uStatus+"$").trim());
            if(conn != null) {
                conn.close();
            }
        }catch(Exception e) {
            out.println("0$");
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Exception in EPOS Trip Close --> ").append(e.getMessage()).toString());
        }
        finally{
            if(conn != null) {
                conn.close();
               
            }
        }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*,java.util.Date,java.text.SimpleDateFormat,ets.domain.util.FPLogUtils" %>