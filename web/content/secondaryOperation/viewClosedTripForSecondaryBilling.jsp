<%-- 
    Document   : viewclosedtrip
    Created on : Dec 6, 2013, 4:14:16 PM
    Author     : srinientitle
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.secondaryOperation.business.SecondaryOperationTO" %>
        <%@ page import="java.util.*" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
            function submitPage() {
                var customerId = document.enter.customerId.value;
                document.enter.action = '/throttle/viewClosedTripForSecondaryBilling.do';
                document.enter.submit();
            }
            function generateBill() {
                document.enter.action = '/throttle/gererateTripBill.do';
                document.enter.submit();
            }
            function selectCheckCondition() {
                if (document.getElementById("select").checked = true) {
                    document.getElementById("select").value = "selected";
                } else {
                    document.getElementById("select").value = "";
                }
            }
        </script>
    </head>
    
    <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageUser" text="Secondary Bill Generation"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Settings" text="default text"/></a></li>
                    <li class=""><spring:message code="hrms.label.ManageUser" text="default text"/></li>

                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">

    <body>
        <form name="enter" action=""  method="post">
            <%@ include file="/content/common/message.jsp" %>
            <br>
                
                        <table class="table table-info mb10 table-hover" id="bg" >
		    <thead>
		<tr>
		    <th colspan="2" height="10" > Secondary Bill Generation </th>
		</tr>
		    </thead>
    </table>
                           
                                <table class="table table-info mb20 table-hover" id="bg" >
                                    <tr>
                                        <td><font color="red">*</font>Customer</td>
                                        <td height="30">
                                            <select class="form-control" style="width:250px;height:40px" name="customerId" id="customerId"  style="width:125px;"><option value="">-select-</option>
                                                <c:forEach items="${secondaryCustomer}" var="customerList">
                                                    <option value='<c:out value="${customerList.customerId}"/>'><c:out value="${customerList.customerName}"/></option>
                                                </c:forEach>
                                            </select>
                                            <script>
                                                document.getElementById("customerId").value = '<c:out value="${customerId}"/>';
                                            </script>
                                        </td>
                                        <td><font color="red">*</font>Bill Month</td>
                                        <td height="30"><select class="form-control" style="width:250px;height:40px" name="billMonth" id="billMonth">
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05">May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select></td>
                                    <script>
                                        document.getElementById("billMonth").value = '<c:out value="${billMonth}"/>';
                                    </script>
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>Year</td>   
                                        <td><select class="form-control" style="width:250px;height:40px" name="billYear" id="billYear">
                                                <option value="12">2012</option>
                                                <option value="13">2013</option>
                                                <option value="14" selected>2014</option>
                                            </select> </td>   
                                    <script>
                                        document.getElementById("billYear").value = '<c:out value="${billYear}"/>';
                                    </script>

                                    <td colspan="2" align="center"><input type="button" class="btn btn-success"   value="FETCH DATA" onclick="submitPage();"></td>
                                    </tr>


                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>

            <br>
            <br>
            <c:if test="${secondaryBillingList == null}">
                <c:if test="${tripCode != null}">
                    <%int sNumber = 1,index = 0;%>
                     <table class="table table-info mb30 table-hover" id="bg" >
                         <thead>
                        <tr>
                            <td colspan="4">
                                <font>Before billing settle the given trips</font>
                            </td>
                        </tr>
                        <thead>
                        <tr>
                            <th  height="30">S.No</th>
                            <th  height="30">Trip Code</th>
                            <th  height="30">Driver Name</th>
                            <th  height="30">Trip Start Date</th>
                            <th  height="30">Trip End Date</th>
                        </tr>
                        </thead>
                        <c:if test="${tripNotSettled != null}">
                            <c:forEach items="${tripNotSettled}" var="trip">
                                <%   String classText1 = "";
                              int oddEven = index % 2;
                              if (oddEven > 0) {
                                  classText1 = "text2";
                              } else {
                                  classText1 = "text1";
                              }  %>
                                <tr>
                                    <td class="<%=classText1%>" height="30"><%=sNumber%></td>
                                    <td class="<%=classText1%>" height="30"><c:out value="${trip.tripCode}"/></td>
                                    <td class="<%=classText1%>" height="30"><c:out value="${trip.primaryDriverName}"/></td>
                                    <td class="<%=classText1%>" height="30"><c:out value="${trip.tripStartDate}"/></td>
                                    <td class="<%=classText1%>" height="30"><c:out value="${trip.tripEndDate}"/></td>
                                </tr>
                                <%index++;sNumber++;%>
                            </c:forEach>
                        </c:if>
                    </table>
                </c:if>
                <c:if test="${tripCode == null}">
                    <font>No Records Found</font>
                    </c:if>
                </c:if>
                <c:if test="${secondaryBillingList != null}">
                <table style="width: 100%" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="80">
                            <th><h3>S No</h3></th>
                            <th><h3>Customer</h3></th>
                            <th><h3>Extra Km Calc Type</h3></th>
                            <th><h3>Vehicle Type</h3></th>                        
                            <th><h3>Vehicle Count</h3></th>                        
                            <th><h3>Fixed Km /Month/Vehicle</h3></th>                        
                            <th><h3>Fixed Km /Month</h3></th>                        
                            <th><h3>Fixed Cost /Month/Vehicle</h3></th>                        
                            <th><h3>Total Cost /Month</h3></th>                        
                            <th><h3>Extra Charge / Km</h3></th>                        
                            <th><h3>Total Run Km</h3></th>
                            <th><h3>Extra Km Run</h3></th>
                            <th><h3>Extra Km Charge</h3></th>
                            <th><h3>Select</h3></th>
                        </tr>
                    </thead>
                    <c:set var="extraKm" value="0"/>
                    <tbody>
                        <%int index=0;
                        int sno=1;
                        String classText ="";
                        %>              

                        <c:forEach items="${secondaryBillingList}" var="secBill">
                            <tr>
                                <td  height="30"><%=sno%></td>
                                <td  height="30" ><c:out value="${secBill.customerName}"/></td>
                                <td  height="30" style="width:120px"><c:out value="${secBill.extraKmCalculation}"/></td>
                                <td  height="30" ><c:out value="${secBill.vehicleTypeName}"/></td>
                                <td  height="30" align="center" ><c:out value="${secBill.vehicleCount}"/></td>
                                <td  height="30" align="center" ><c:out value="${secBill.fixedKmPerMonth}"/></td>
                                <td  height="30" align="center" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${secBill.fixedKmPerMonth * secBill.vehicleCount}" /></td>
                                <td  height="30" align="center" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${secBill.fixedAmount}" />
                                </td>
                                <td  height="30" align="center" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${secBill.fixedAmount * secBill.vehicleCount}" />
                                </td>
                                <td  height="30" align="center" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${secBill.extraKmCharge}" />
                                </td>
                                <td  height="30" ><c:out value="${secBill.totalKmRun}"/></td>
                                <c:if test="${secBill.totalKmRun > secBill.fixedKmPerMonth  * secBill.vehicleCount}">
                                    <c:set var="extraKm" value="${secBill.totalKmRun - secBill.fixedKmPerMonth  * secBill.vehicleCount}"/>
                                </c:if>
                                <td  height="30" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${extraKm}" /></td>
                                <td  height="30" align="right">
                                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${extraKm * secBill.extraKmCharge}" /></td>
                                <td  height="30" style="width:120px"><a href="/throttle/generateSecondaryBilling.do?vehicleTypeId=<c:out value="${secBill.vehicleTypeId}"/>&customerId=<c:out value="${secBill.customerId}"/>&billedMonth=<c:out value="${secBill.billedMonth}"/>">Generate Bill</a></td>
                            </tr>
                            <%
                            sno++;
                            index++;
                            %>
                        </c:forEach>


                    </tbody>
                </table>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
            </c:if>

        </form>
    </body>    

  
<%@ include file="../common/NewDesign/settings.jsp" %>