<%--
    Document   : driverSettlementReport
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
 <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>



    </head>
    <body >
        <form name="driverSettlement" action=""  method="post">
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>

          <br/>
          <br/>
          <br/>
                                      <c:if test = "${tripSettlementDetailsSize != '0'}" >
        <div id="tabs" >
                <ul>

                    <li><a href="#tripDetails"><span>Trip Details</span></a></li>
                    <li><a href="#expense"><span>Expense Details </span></a></li>
                      <li><a href="#driverAdvance"><span>Driver Advance</span></a></li>
                      <li><a href="#summary"><span>summary</span></a></li>
                </ul>
            <br>
             <div id="tripDetails">

            <c:if test = "${tripSettlementDetails != null}" >
                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr height="45" >
                           <td class="contenthead" align="center">S.No</td>
                           <td class="contenthead" align="center">Trip Code</td>
                           <td class="contenthead" align="center">Vehicle No</td>
                           <td class="contenthead" align="center">Customer Name</td>
                           <td class="contenthead" align="center">Driver Name</td>
                           <td class="contenthead" align="center">Route</td>
                           <td class="contenthead" align="center">Start Date</td>
                           <td class="contenthead" align="center">End Date</td>
                           <td class="contenthead" align="center">Total Run Km</td>
                           <td class="contenthead" align="center">Total Run Hours</td>
                           <td class="contenthead" align="center">Total Days</td>
                           <td class="contenthead"  align="center">Total Weight</td>
                           <td class="contenthead"  align="center">Estimated Expense</td>
                        </tr>
                        <% int index = 0, sno = 1;%>
                        <c:forEach items="${tripSettlementDetails}" var="closure">

                             <c:set var="totalEstimatedExpense" value="${totalEstimatedExpense + closure.rcmExpense}"></c:set>
                              <c:set var="totalRunKms" value="${totalRunKms + closure.totalKmRun}"></c:set>
                             <c:set var="totalRunHours" value="${totalRunHours + closure.totalHours}"></c:set>
                             <c:set var="totalDays" value="${totalDays + closure.totalDays}"></c:set>
                             <c:set var="totalFuelConsumed" value="${totalFuelConsumed + closure.fuelConsumption}"></c:set>
                             <c:set var="driverId" value="${closure.primaryDriverId}"></c:set>
                             <c:set var="primaryDriverName" value="${closure.primaryDriverName}"></c:set>
                             <c:set var="startingBalance" value="${0}"></c:set>
                             <c:set var="tripBalance" value="${closure.advanceAmount - closure.totalExpense }"></c:set>




                           <%
                                        String classText3 = "";
                                        int oddEven = sno % 2;
                                        if (oddEven > 0) {
                                            classText3 = "text1";
                                        } else {
                                            classText3 = "text2";
                                        }
                            %>
                            <tr height="30">

                                <td class="<%=classText3%>" align="center"><%=sno%></td>
                                <td class="<%=classText3%>" align="left">
                                    <a href="#" onclick="viewTripDetails('<c:out value="${closure.tripId}"/>');"><c:out value="${closure.tripCode}"/></a></td>
                                <td class="<%=classText3%>" align="left">
                                       <a href="#" onclick="viewVehicleDetails('<c:out value="${closure.vehicleId}"/>')"><c:out value="${closure.regNo}"/></a></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.customerName}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.primaryDriverName}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.routeInfo}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.startDate}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.endDate}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.totalKmRun}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.totalHours}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.totalDays}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.totalWeight}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.rcmExpense}"/></td>

                            </tr>
                            <%
                                        index++;
                                        sno++;
                            %>
                        </c:forEach>

                </table>



                <br/>
                <br/>

            </c:if>
                   <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>

             </div>
          <div id="expense">
                <c:if test = "${tripSettlementDetails != null}" >
              <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr height="45" >
                           <td class="contenthead" align="center">S.No</td>
                           <td class="contenthead" align="center">Trip Code</td>
                           <td class="contenthead" align="center">Vehicle No</td>
                           <td align="center" class="contenthead" >Route Name</td>
                           <td align="center" class="contenthead" >Fuel Expense</td>
                           <td align="center" class="contenthead" >Toll Expense</td>
                           <td align="center" class="contenthead" >Add Toll Expense</td>
                           <td align="center" class="contenthead" >Misc Expense</td>
                           <td align="center" class="contenthead" >Parking</td>
                           <td align="center" class="contenthead" >Pre-Cooling</td>
                           <td align="center" class="contenthead" >Extra Expense</td>
                           <td align="center" class="contenthead" >System Expense(Fuel + Toll + Add Toll +<br>Misc + Parking + Pre-Cooling)</td>
                           <td class="contenthead"  align="center">Total Expense</td>
                        </tr>
                        <% int index = 0, sno = 1;%>
                        <c:forEach items="${tripSettlementDetails}" var="expense">
                                <c:set var="totalFuelExpense" value="${totalFuelExpense + expense.fuelExpense}"></c:set>
                                <c:set var="totalTollAmount" value="${totalTollAmount + expense.tollAmount}"></c:set>
                                <c:set var="addTotalTollAmount" value="${addTotalTollAmount + expense.additionalTollCost}"></c:set>
                                <c:set var="totalMiscExpense" value="${totalMiscExpense + expense.miscExpense}"></c:set>
                                <c:set var="totalParkingExpense" value="${totalParkingExpense + expense.parkingCost}"></c:set>
                                <c:set var="totalPreCoolingExpense" value="${totalPreCoolingExpense + expense.preCollingCost}"></c:set>
                                <c:set var="totalSystemExpense" value="${totalSystemExpense + expense.systemExpense}"></c:set>
                                <c:set var="totalExtraExpense" value="${totalExtraExpense + expense.extraExpense}"></c:set>
                                <c:set var="totalExpenses" value="${totalExpenses + expense.totalExpense}"></c:set>




                             <%
                                        String classText3 = "";
                                        int oddEven = sno % 2;
                                        if (oddEven > 0) {
                                            classText3 = "text1";
                                        } else {
                                            classText3 = "text2";
                                        }
                            %>
                            <tr height="30">
                                 <td class="<%=classText3%>" align="center"><%=sno%></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.tripCode}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.regNo}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.routeInfo}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.fuelExpense}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.tollAmount}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.additionalTollCost}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.miscExpense}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.parkingCost}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.preCollingCost}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.extraExpense}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.systemExpense}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.totalExpense}"/></td>


                            </tr>
                            <%
                                        index++;
                                        sno++;
                            %>
                        </c:forEach>
                             <tr height="30">
                                 <td  align="center" colspan="4" >Total Expense</td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalFuelExpense}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalTollAmount}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${addTotalTollAmount}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalMiscExpense}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalParkingExpense}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalPreCoolingExpense}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalExtraExpense}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalSystemExpense}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalExpenses}"/></td>


                            </tr>
                </c:if>
                </table>
              <br/>
              <br/>
               <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>
          </div>
          <div id="driverAdvance">
                <c:if test = "${tripSettlementDetails != null}" >
              <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr height="45" >
                            <td  class="contenthead"  align="center">S.No</td>
                           <td class="contenthead"  align="center">Trip Code</td>
                           <td class="contenthead"  align="center">Vehicle No</td>
                           <td class="contenthead"  align="center">Route Name</td>
                           <td class="contenthead"  align="center">Driver Name</td>
                           <td class="contenthead"  align="center">Advance Amount</td>
                        </tr>
                        <% int index = 0, sno = 1;%>
                        <c:forEach items="${tripSettlementDetails}" var="advance">
                              <c:set var="totalAdvanceAmount" value="${ totalAdvanceAmount + advance.advanceAmount}"></c:set>
                             <input type="hidden" name="totalAdvanceAmount" id="totalAdvanceAmount" value="<c:out value="${totalAdvanceAmount}"/>"
                              <%
                                        String classText3 = "";
                                        int oddEven = sno % 2;
                                        if (oddEven > 0) {
                                            classText3 = "text1";
                                        } else {
                                            classText3 = "text2";
                                        }
                            %>
                            <tr height="30">
                                 <td class="<%=classText3%>" align="center"><%=sno%></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.tripCode}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.regNo}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.routeInfo}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.primaryDriverName}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.advanceAmount}"/></td>

                            </tr>
                            <%
                                        index++;
                                        sno++;
                            %>
                        </c:forEach>
                            <tr height="30">
                                <td  align="center">&nbsp;</td>
                                <td  align="left">&nbsp;</td>
                                <td  align="left">&nbsp;</td>
                                <td  align="left">&nbsp;</td>
                                <td  align="left">Total Advance Paid</td>
                                <td  align="left"><c:out value="${totalAdvanceAmount}"/></td>

                            </tr>
                </c:if>

                </table>
              <br/>
              <br/>
               <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>
          </div>
             <c:if test = "${driverSettlementDetails != null}" >
                   <c:forEach items="${driverSettlementDetails}" var="driver">
            <div id="summary">

                 <table  border="1" class="border" align="center" width="800px" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="4" >Driver Settlement Details
                            </td>
                        </tr>


                        <tr height="25">
                            <td class="text1">Driver Name</td>
                            <td class="text1"><label><c:out value="${driver.primaryDriverName}"/></label></td>
                            <td class="text1">Total No of Trips</td>
                            <td class="text1"><label ><c:out value="${tripSettlementDetailsSize}"/></label></td>
                        </tr>

                        <tr height="25">
                            <td class="text2">Total Run Kms</td>
                            <td class="text2"><label><c:out value="${totalRunKms}"/></label></td>
                            <td class="text2">Total Run Hours</td>
                            <td class="text2"><label><c:out value="${totalRunHours}"/></label></td>
                        </tr>



                        <tr height="25">
                            <td class="text1">Total No of Days</td>
                            <td class="text1"><label><c:out value="${totalDays}"/></label></td>
                            <td class="text1" colspan="2"> &nbsp;</td>
                        </tr>

                        <tr height="25">
                            <td class="text2">Toll Amount </td>
                            <td class="text2"><label><c:out value="${totalTollAmount}"/></label></td>
                            <td class="text2">Total Misc Cost</td>
                            <td class="text2"><label><c:out value="${totalMiscExpense}"/></label></td>
                        </tr>

                        <tr height="25">
                            <td class="text1">Total Driver Bhatta </td>
                            <td class="text1"><label><c:out value="${totalDriverBatta}"/></label></td>
                            <td class="text1">Total Driver Incentives </td>
                            <td class="text1"><label><c:out value="${totalDriverIncentive}"/></label></td>
                        </tr>

                        <tr height="25">
                            <td class="text2">Total Estimated Expense </td>
                            <td class="text2"><label><c:out value="${totalEstimatedExpense}"/></label></td>
                            <td class="text2" colspan="2"> &nbsp;</td>
                        </tr>

                        <tr height="25">
                            <td class="text1">Total Route Expense </td>
                            <td class="text1"><label><c:out value="${totalExtraExpense}"/></label></td>
                            <td class="text1">Total System Expense </td>
                            <td class="text1"><label><c:out value="${totalSystemExpense}"/></label></td>
                        </tr>

                        <tr height="25">
                            <td class="text2">Total Fuel Consumed </td>
                            <td class="text2"><label><fmt:formatNumber pattern="##0.00" value="${totalFuelConsumed}"/></label></td>
                            <td class="text2">Total Fuel Expense </td>
                            <td class="text2"><label><fmt:formatNumber pattern="##0.00" value="${totalFuelExpense}"/></label></td>
                        </tr>


                        <tr height="25">
                            <td class="text1">Total Advance Paid </td>
                            <td class="text1"><label><c:out value="${totalAdvanceAmount}"/></label></td>
                            <td class="text1" colspan="2"> &nbsp;</td>

                        </tr>

                        <tr height="25">
                            <td class="text2">Total Expenses </td>
                            <td class="text2"><label><c:out value="${totalExpenses}"/></label></td>
                            <td class="text2" colspan="2"> &nbsp; </td>

                        </tr>
                        <tr height="25">
                            <td class="text1">Starting Balance </td>
                            <td class="text1"><label><c:out value="${driver.startingBalance}"/></label></td>
                            <td class="text1">Ending Balance</td>
                            <td class="text1"><label><c:out value="${driver.endingBalance }"/></label></td>

                        </tr>



                        <tr height="25">
                            <td class="text2">Balance Amount </td>
                            <td class="text2"><label><c:out value="${driver.balanceAmount}"/></label></td>
                            <td class="text2">Payment Mode </td>
                            <td class="text2"><label><c:out value="${driver.paymentMode}"/></label></td>
                        </tr>

                        
                            <tr>
                                <td class="text1">Pay Amount</td>
                                <td class="text1"><label><c:out value="${driver.payAmount}"/></label></td>
                                <td class="text1">Carry Forward Amount</td>
                                <td class="text1"><label><c:out value="${driver.cfAmount}"/></label>
                                  
                                </td>
                            </tr>
                           
                       


                        <tr height="25">
                            <td class="text1">Remarks for Extra Expenses</td>
                            <td class="text1" ><label><c:out value="${driver.settlementRemarks}"/></label></td>
                        </tr>
                    </table>
                <br/>
                <br/>
                   

                <br/>
                <br/>


             </div>
                   </c:forEach>
             </c:if>
               </div>
                                      </c:if>
                 <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
        </form>
    </body>
</html>