<%--
    Document   : routecreate
    Created on : Oct 28, 2013, 3:48:50 PM
    Author     : Administrator
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>  

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>

<script>
    var rowCount = 1;
    var sno = 0;
    var rowCount1 = 1;
    var sno1 = 0;
    var httpRequest;
    var httpReq;
    var styl = "";



    //savefunction
    function submitPage(value) {
        var count1 = 0;
        var count2 = 0;
        if (document.getElementById('extraKmCalculation').value == '0') {
            alert("please select extra km calculation");
            document.getElementById('extraKmCalculation').focus();
        }else if (document.getElementById('routeValidFrom').value == '') {
            alert("please select the route valid from date");
            document.getElementById('routeValidFrom').focus();
        } else if (document.getElementById('routeValidTo').value == '') {
            alert("please select the route valid to date");
            document.getElementById('routeValidTo').focus();
        } else if (document.getElementById('contractCngCost').value == '') {
            alert("please fill cost of cng");
            document.getElementById('contractCngCost').focus();
        } else if (document.getElementById('contractDieselCost').value == '') {
            alert("please fill cost of diesel");
            document.getElementById('contractDieselCost').focus();
        } else if (document.getElementById('rateChangeOfCng').value == '') {
            alert("please fill rate change of cng");
            document.getElementById('rateChangeOfCng').focus();
        } else if (document.getElementById('rateChangeOfDiesel').value == '') {
            alert("please fill rate change of diesel");
            document.getElementById('rateChangeOfDiesel').focus();
        }else{
            document.secondaryRoute.action = '/throttle/saveSecondaryContract.do';
            document.secondaryRoute.submit();
        }

    }






     $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#customerName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getSecondaryCustomerDetails.do",
                        dataType: "json",
                        data: {
                            customerName: request.term
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#customerName").val(ui.item.Name);
                    $("#customerId").val(ui.item.Id);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    $('#fixedKmPerMonth').focus();
                    return false;

                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };


        });

</script>
    
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.SecondaryContract" text="SecondaryContract"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.SecondaryContract" text="SecondaryContract"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">

<body onload="addRow1();">
        <form name="secondaryRoute"  method="post">
            
            
            
             <table class="table table-info mb30 table-hover" id="bg" >	
                <thead>
                <tr  align="left"><th colspan="4">Secondary Contract Details</th></tr>
                </thead>
                <tr >
                    <td><font color='red'>*</font>Customer Name</td>
                    <td>
                        <input type="hidden" name="billType" id="billType" value="<c:out value="${billType}"/>"/>
                        <c:set var="billType" value="${billType}" />
                        <input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>"/>
                        <input type="hidden" name="customerName" id="customerName" value="<c:out value="${customerName}"/>" class="form-control" style="width:250px;height:40px"/><c:out value="${customerName}"/></td>
                    <td><font color='red'>*</font>Extra Km Calculation</td>
                    <td><select name="extraKmCalculation" id="extraKmCalculation" class="form-control" style="width:250px;height:40px">
                                    <option value="0">--Select--</option>
                                    <option value="1">Consolidated Run Km</option>
                                    <option value="2">Vehicle Wise Run Km</option>
                        </select></td>
                </tr>


                <tr >
                    <td><font color='red'>*</font>Valid From</td>
                    <td><input type="text" name="routeValidFrom" id="routeValidFrom" value="" class="datepicker" style="width:250px;height:40px"/></td>
                    <td><font color='red'>*</font>Valid To</td>
                    <td><input type="text" name="routeValidTo" id="routeValidTo" value="" class="datepicker" style="width:250px;height:40px"/></td>
                </tr>
                <tr >
                    <td><font color='red'>*</font>Cost Of CNG (Rs/Kg)</td>
                    <td><input type="text" name="contractCngCost" id="contractCngCost" value="" class="form-control" style="width:250px;height:40px" onKeyPress='return onKeyPressBlockCharacters(event);'/></td>
                    <td><font color='red'>*</font>Cost Of Diesel (Rs/Ltr)</td>
                    <td><input type="text" name="contractDieselCost" id="contractDieselCost" value="" class="form-control" style="width:250px;height:40px" onKeyPress='return onKeyPressBlockCharacters(event);'/></td>
                </tr>
                <tr >
                    <td><font color='red'>*</font>Rate Change For (1 Rs/Kg) Of CNG</td>
                    <td><input type="text" name="rateChangeOfCng" id="rateChangeOfCng" value="" class="form-control" style="width:250px;height:40px" onKeyPress='return onKeyPressBlockCharacters(event);'/></td>
                    <td><font color='red'>*</font>Rate Change For (1 Rs/Ltr) Of Diesel</td>
                    <td><input type="text" name="rateChangeOfDiesel" id="rateChangeOfDiesel" value="" class="form-control" style="width:250px;height:40px" onKeyPress='return onKeyPressBlockCharacters(event);'/></td>
                </tr>
                <tr >
                    <td >Current Diesel Cost (PAN INDIA)</td>
                    <td><input type="hidden" name="dieselCost" id="dieselCost"  value="<%=request.getAttribute("currentDieselFuelPrice")%>" readonly/><%=request.getAttribute("currentDieselFuelPrice")%></td>
                    <td >Current CNG Cost (PAN INDIA)</td>
                    <td><input type="hidden" name="fuelCost" id="fuelCost"  value="<%=request.getAttribute("currentFuelPrice")%>" readonly/><%=request.getAttribute("currentFuelPrice")%></td>
                </tr>
            </table>
            
            



            
            
            
            
            <% int count = 0;%>
            <c:if test="${mileageConfigList != null}">
              
                    <table class="table table-info mb30 table-hover" id="table" >	
                    <thead>
                        <tr height="45">
                            <th>S.No</th>
                            <th>Vehicle Type </th>
                            <th>NoOfVehiclesContracted </th>
                            <c:if test="${billType == 1}">
                                <th>Monthly Fixed KM / Vehicle</th>
                                <th>Monthly Fixed Cost / Vehicle</th>
                                <th>Extra Km Charge / Km</th>
                            </c:if>
                            <c:if test="${billType == 2}">
                            <th>Rate/ Km</th>
                            </c:if>
                            <c:if test="${billType ==  3}">
                            <th>Rate/ Drop</th>
                            </c:if>
                        </tr>
                    </thead>
                    <% int index = 0;
                                int sno = 1;
                    %>
                    <tbody>
                        <c:forEach items="${mileageConfigList}" var="mcl">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>

                             <tr height="30">
                                <td align="left" ><%=sno%></td>
                                <td align="left"  style="width: 200px">
                                    <input type="hidden" name="vehTypeId" id="vehTypeId" value="<c:out value="${mcl.vehicleTypeId}"/>" readonly/><c:out value="${mcl.vehicleTypeName}"/>
                                </td>
                                <td align="left" ><input type="text" class="form-control" style="width:250px;height:40px" name="noOfVehicle" id="noOfVehicle<%=index%>" value="0"  style="width: 90px" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <c:if test="${billType == 1}">
                                <td align="left" ><input type="text" class="form-control" style="width:250px;height:40px" name="fixedKm" id="fixedKm<%=index%>"  value="0" style="width: 90px" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" ><input type="text" class="form-control" style="width:250px;height:40px" name="fixedKmCharge" id="fixedKmCharge<%=index%>"  value="0" style="width: 90px" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                </c:if>
                                <c:if test="${billType == 2 || billType == 3}">
                                <input type="hidden" name="fixedKm" id="fixedKm<%=index%>"  value="0" />
                                <input type="hidden" name="fixedKmCharge" id="fixedKmCharge<%=index%>"  value="0" />
                                </c:if>
                                <td align="left" ><input type="text" class="form-control" style="width:250px;height:40px" name="extraKmCharge" id="extraKmCharge<%=index%>"   value="0" style="width: 90px" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                            </tr>
                            <%sno++;%>
                            <%index++;%>
                        </c:forEach>

                    </tbody>
                </table>
               
               
               
               <table width="100%" align="center" cellpadding="0" cellspacing="0" id="addHumanResource" class="table2">

                <tr>
                    <td  height="30" style="width: 10px;">S No</td>
                    <td  height="30" style="width: 10px;">Type</td>
                    <td  height="30" style="width: 10px;"><font color='red'>*</font>No of Unit</td>
                    <td  height="30" style="width: 10px;"><font color='red'>*</font>UnitCost/Month</td>
                </tr>
                <tr height="25"></tr>
                <tr>
                    <td colspan="4" align="center">
                            <input type="button" class="btn btn-success" name="add" value="add" onclick="addRow1();"/>
                            </td>
                        </tr>
               </table>
               
               
               
               <center>
                              <input type="button" class="btn btn-success" name="Save" value="Save" onclick="submitPage()"/>
               </center>

               <script type="text/javascript">
            var sno = 1;
            var httpRequest;
            var httpReq;
            var styl = "";

            function addRow1() {
                if (parseInt(sno) % 2 == 0)
                {
                    styl = "text2";
                } else {
                    styl = "text1";
                }
                var tab = document.getElementById("addHumanResource");
                var newrow = tab.insertRow(sno);

                cell = newrow.insertCell(0);
                var cell0 = "<td class='text1' height='25' style='width:10px;'> " + sno + "</td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                cell = newrow.insertCell(1);
                cell0 = "<td class='text1' height='25'><select class='form-control' style='width:250px;height:40px' id='hrId"+sno+"'   name='hrId'><option selected value=0>---Select---</option> <c:if test="${humanResourceList != null}" ><c:forEach items="${humanResourceList}" var="hrList"><option  value='<c:out value="${hrList.hrId}" />'><c:out value="${hrList.hrName}" /> </c:forEach > </c:if> </select></td>";
                cell.innerHTML = cell0;

                cell = newrow.insertCell(2);
                var cell0 = "<td class='text1' height='25' ><input type='text' value='' class='form-control' style='width:250px;height:40px'  name='noOfPersons'  id='noOfPersons"+sno+"'   value='' ></td>";
                cell.innerHTML = cell0;

                cell = newrow.insertCell(3);
                var cell0 = "<td class='text1' height='25' ><input type='text' value='' class='form-control' style='width:250px;height:40px'  name='fixedAmount'  id='fixedAmount"+sno+"'   value='' ></td>";
                cell.innerHTML = cell0;



                sno++;


            }
        </script>

            </c:if>
            
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>
