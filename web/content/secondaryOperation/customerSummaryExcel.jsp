<%-- 
    Document   : marginWiseTripSummary
    Created on : Jun 12, 2014, 01:31:16 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
        <form name="summary" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "summary-" + curDate + ".xls";
                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
            
            <c:if test = "${customerPaymentDetails != null}" >
                   <div id="printContent" >          
                <table  align="center" border="0" id="table1" class="table table-info mb30 table-hover sortable">
                    <thead>
                        <tr height="80">
                            <th>S.No</th>
                            <th>Customer Name</th>
                            <th>Bill No</th>
                            <th>Invoice No</th>
                            <th>Invoice Date</th>
                            <th>Order Type</th>
                            <th>Invoice Amount</th>
                            <th>Grand Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${customerPaymentDetails}" var="customer">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                             
                                    <tr height="30">
                                    <td align="left" ><%=sno%></td>
                                    <td align="left" ><c:out value="${customer.customerName}"/> </td>
                                    <td align="left" ><c:out value="${customer.invoiceCode}"/> </td>
                                    <td align="left" ><c:out value="${customer.invoiceNo}"/> </td>
                                    <td align="left" ><c:out value="${customer.invoiceDate}"/> </td>
                                    <td align="left" ><c:out value="${customer.orderType}"/> </td>
                                    <td align="left" ><c:out value="${customer.invoiceAmount}"/> </td>
                                    <td align="left" ><c:out value="${customer.grandTotal}"/> </td>
                                    </tr>
                                     <%
                                       index++;
                                       sno++;
                                    %>
                                
                           
                        </c:forEach>

                    </tbody>
                </table>
                         </div>
                        <br>
                        <br>
<!--                       <tr>
                                <input type="button" class="btn btn-success" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>

                       </tr>-->
                        <!--<center><input type="button" class="btn btn-info"  value="Print" onClick="print('printContent');" style="width:90px;height:30px;">-->
                                                       <!--<input type="button" class="btn btn-success" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</center>-->

                        <script type="text/javascript">
                        function print(val)
                        {
                            var DocumentContainer = document.getElementById(val);
                            var WindowObject = window.open('', "TrackHistoryData",
                                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                            WindowObject.document.writeln(DocumentContainer.innerHTML);
                            WindowObject.document.close();
                            WindowObject.focus();
                            WindowObject.print();
                            WindowObject.close();
                        }
                    </script>
            
          <script language="javascript" type="text/javascript">
                setFilterGrid("table1");
            </script>
            <div id="controls">
<!--                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>-->
<!--                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>-->
<!--                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>-->
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        </form>
        </c:if>
        <!--</form>-->

    </body>    
</html>