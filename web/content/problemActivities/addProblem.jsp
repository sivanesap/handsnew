<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
  
 <script language="javascript" src="/throttle/js/validate.js"></script>   

</head>
<script>
    function submitpage()
    {
      
       
       if(textValidation(document.dept.problemName,"problemName")){
           return;
       }else if(textValidation(document.dept.discription,"discription")){
           return;
       
       }else if(isSelect(document.dept.sectionId,"Section")){
           return;
       }else
       {
         document.dept.action= "/throttle/addProblem.do";
         document.dept.submit();   
       }
       
       
      
    }
</script>
<body>
<form name="dept"  method="post">
 <%@ include file="/content/common/path.jsp" %>
       

<%@ include file="/content/common/message.jsp" %>
 
<table align="center" border="0" cellpadding="0" cellspacing="0" width="300" id="bg" class="border">
<tr height="30">
<td colspan="5" class="contenthead">Add Problem</td>
</tr>
 <tr >
<td class="text2" height="30"><font color=red>*</font>Section</td>
<td class="text2"  ><select class="textbox" name="sectionId" style="width:125px" >
<option value="0">---Select---</option>
<c:if test = "${SectionList != null}" >
<c:forEach items="${SectionList}" var="Dept"> 
<c:if test="${ (Dept.activeInd=='Y') || (Dept.activeInd=='y') }">
<option value='<c:out value="${Dept.sectionId}" />'><c:out value="${Dept.sectionName}" /></option>
</c:if>
</c:forEach >

</c:if>  	
</select></td>
</tr>
<tr>
    <td class="text1" height="30"><font color=red>*</font>Problem Name</td>
<td class="text1" height="30"><input name="problemName" type="text" class="textbox" value=""></td>
</tr>
<tr>
<td class="text2" height="30"><font color=red>*</font> Description</td>
<td class="text2" height="30"><textarea class="textbox" name="discription"></textarea></td>
</tr>
</table>
<br>
<center>
<input type="button" value="Add" class="button" onClick="submitpage();">
&emsp;<input type="reset" class="button" value="Clear">
</center>
</form>
</body>
</html>
