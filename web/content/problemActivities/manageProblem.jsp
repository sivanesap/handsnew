<%-- 
    Document   : manageVendor
    Created on : Mar 8, 2009, 10:51:13 AM
    Author     : karudaiyar Subramaniam
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    </head>
    <script language="javascript">
        function submitPage(value)
        {
            if(value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value =='First' || value =='Last'){
if(value=='GoTo'){
var temp=document.vendorDetail.GoTo.value;       
document.vendorDetail.pageNo.value=temp;
document.vendorDetail.button.value=value;
document.vendorDetail.action = '/throttle/manageProblemAct.do';   
document.vendorDetail.submit();
}else if(value == "First"){
temp ="1";
document.vendorDetail.pageNo.value = temp; 
value='GoTo';
}else if(value == "Last"){
temp =document.vendorDetail.last.value;
document.vendorDetail.pageNo.value = temp; 
value='GoTo';
}
document.vendorDetail.button.value=value;
document.vendorDetail.action = '/throttle/manageProblemAct.do';   
document.vendorDetail.submit();
}else if (value=='add')
{
document.vendorDetail.action ='/throttle/addProblemPage.do';
document.vendorDetail.submit();

}
            
}
    </script>
    
    <body>
        
        <form method="post" name="vendorDetail">
             <%@ include file="/content/common/path.jsp" %>
       
 
<%@ include file="/content/common/message.jsp" %>
 
 <% int index = 0;  %>    
            
            <c:if test = "${ProblemActivities != null}" >
                <table width="500" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">

                    <tr align="center">
                        <td width="33" height="30" class="contentsub"><div class="contentsub">S.No</div></td>
                        <td width="101" height="30" class="contentsub"><div class="contentsub">Problem Name</div></td>
                        <td width="100" height="30" class="contentsub"><div class="contentsub">Section Name</div></td>
                        <td width="161" height="30" class="contentsub"><div class="contentsub">Description</div></td>
                        <td width="63" height="30" class="contentsub"><div class="contentsub">Status</div></td>
			<td width="40" height="30" class="contentsub"><div class="contentsub">&nbsp;</div></td>
                         
                    </tr>
                    <%
                    int pag = 0;
                    if(request.getAttribute("pageNo") != null) {
                        pag = (Integer)request.getAttribute("pageNo");
                    }
                    index = ((pag -1) *10) +1 ;
                    %>
                    
                    <c:forEach items="${ProblemActivities}" var="list"> 
                    
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr  > 
                             
                            
                            <td class="<%=classText %>" height="30"><%= (index) %></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.problemName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.sectionName}"/></td>
                             
                             <td class="<%=classText %>" height="30"><c:out value="${list.discription}"/></td>
                             
                            <input type="hidden" name="sectionIds" value='<c:out value="${list.sectionId}"/>'>
                               
                                
                                
                             
                             
                            <td class="<%=classText %>" height="30">                                
                                <c:if test = "${list.activeInd == 'Y' }" >
                                    Active 
                                </c:if>
                                <c:if test = "${list.activeInd == 'N'}" >
                                    InActive
                                </c:if>
                            </td>
                             <td class="<%=classText %>" align="left"> <a href="/throttle/alterProblemPage.do?problemId=<c:out value='${list.problemId}' />" >Alter </a> </td>
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    
                </table>

            </c:if> 
<br>
<br>
<br>
<table align="center" cellpadding="0" cellspacing="0" border="0">
<tr>
<td>
 <%@ include file="/content/common/pagination.jsp"%>  
 </td>
 
</tr>
</table>

            <center>
                <br>
                <input type="button" name="add" value="Add" onClick="submitPage(this.name)" class="button">
                <input type="hidden" name="reqfor" value="">
            </center>
            <br>
        </form>
    </body>
</html>

