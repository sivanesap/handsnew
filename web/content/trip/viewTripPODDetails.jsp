<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datep    icker.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });



</script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script type="text/javascript">
    function submitExpense() {
        if (document.getElementById("expenseValue").value == "0" || document.getElementById("expenseValue").value == "") {
            alert("please enter the Expense Value");
            return false;
            $("#expenseValue").focus();
        } else if (document.getElementById("expenseDate").value == "") {
            alert("please select the Expense Date");
            return false;
            $("#expenseDate").focus();
        } else if (document.getElementById("expenseType").value == "") {
            alert("please select the Expense Type");
            return false;
            $("#expenseType").focus();
        } else {
            $("#saveExp").hide();
            document.trip.action = '/throttle/saveTripOtherExpenses.do';
            document.trip.submit();
        }
    }


    function submitPOD() {
        $("#savePOD").hide();
        document.trip.action = '/throttle/saveTripPODApproval.do';
        document.trip.submit();
    }
</script>    


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.LHCDetails" text="Trip POD Details"/> </h2>
    <br>
    <body >

        <form name="trip" method="post">


            <div id="tabs" >
                <ul>
                    <li><a href="#podDetail"><span>POD Details</span></a></li>
                    <!--<li><a href="#expenseDetail"><span>Expense Details</span></a></li>-->
                    <!--<li><a href="#otherExpenseDetail"><span>Other Expense Details</span></a></li>-->
                </ul>

                <div id="podDetail">
                    <c:if test="${viewPODDetails != null}">
                        <table  class="table table-info mb30 table-hover" id="bg">
                            <thead> 
                                <tr>
                                    <th width="50" >S No&nbsp;</th>
                                    <th >POD file Name</th>
                                    <th >POD Remarks</th>
                                </tr>
                            </thead>
                            <% int index11 = 1;%>
                            <c:forEach items="${viewPODDetails}" var="POD">
                                <%
                                   String classText3 = "";
                                   int oddEven11 = index11 % 2;
                                   if (oddEven11 > 0) {
                                       classText3 = "text1";
                                   } else {
                                       classText3 = "text2";
                                   }
                                %>
                                <tr>
                                    <td ><%=index11++%></td>
                                    <td >
                                        <a  href="<c:out value="${POD.podFile}"/>"><c:out value="${POD.fileName}"/></a>
                                    </td>
                                    <td ><c:out value="${POD.podRemarks}"/></td>
                                </tr>
                            </c:forEach>

                        </table>
                        <br>
                        <%
                            String var = request.getParameter("status"); 
                            if(!var.equals("1")){
                        %>

                        <table  class="table table-info mb30 table-hover" id="bg">
                            <thead>  <tr>
                                    <th  colspan="4" >POD Approval</th>
                                </tr></thead>

                            <tr>
                            <input type="hidden" name="orderId" Id="orderId" class="textbox" value='<c:out value="${orderId}" />'>
                            <td class="text2"><font color='red'>*</font>Approval</td>
                            <td class="text2">
                                <select class="form-control" name="approvalType" id="approvalType" style="width:250px;height:40px;">
                                    <option  value='0' selected>-Select-</option>
                                    <option  value='1'>Clear POD</option>
                                    <option  value='2'>Non-clear POD</option>
                                </select>
                            </td>
                            <td class="text2" colspan="2"><input type="button" class="btn btn-success" id="savePOD" value="Save" name="Save" onclick="submitPOD();"/></td>
                            </tr>
                        </table>
                        <%}%>       
                    </c:if>
                    <script>
                        function viewPODFiles(tripPodId) {
                            //window.open('/throttle/content/trip/displayBlobData.jsp?tripPodId=' + tripPodId, 'PopupPage', 'height = 500, width = 500, scrollbars = yes, resizable = yes');
                        }
                    </script>
                </div>


                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>

            </div>
        </form>
    </body>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>