<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%@page import="java.text.SimpleDateFormat" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script>

</script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String todayDate = sdf.format(today);
%>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });</script>

<script type="text/javascript" language="javascript">

    function submitPageDone() {
        var txt;
        if (document.getElementById("endKm").value == "") {
            alert("Enter End Km");
        } else {
            var r = confirm("Press a button!");
            if (r == true) {
                document.tripExpense.action = '/throttle/saveIfbTripClosure.do';
                document.tripExpense.submit();
            } else {
                txt = "You pressed Cancel!";
            }
        }

    }
    function closeTrip() {

        document.tripExpense.action = '/throttle/closeTrip.do';
        document.tripExpense.submit();
    }


</script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Runsheet Closure" text="Runsheet Closure"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.Runsheet Closure" text="Runsheet Closure "/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body"> 

            <body onload="">
                <form name="tripExpense" method="post">
                    <br> 
                    <table class="table table-info mb30 table-hover">
                        <% int loopCntr = 0;%>
                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">
                                <% if (loopCntr == 0) {%>

                                <input type="hidden" name="tripCodeEmail" value='<c:out value="${trip.tripCode}"/>' />
                                <input type="hidden" name="customerNameEmail" value='<c:out value="${trip.customerName}"/>' />
                                <input type="hidden" name="routeInfoEmail" value='<c:out value="${trip.routeInfo}"/>' /></td>
                                <input type="hidden" name="tripType" id="tripType" value='<c:out value="${tripType}"/>' /></td>
                                <input type="hidden" name="statusId" id="statusId" value='<c:out value="${statusId}"/>' />

                                <% }%>
                                <% loopCntr++;%>
                            </c:forEach>
                        </c:if>
                    </table>
                    <div id="tabs" >
                        <ul>
                            <li><a href="#tripDetail" style="width: auto"><span>Runsheet Details</span></a></li>
                            <!--<li><a href="#routeDetail"><span>Runsheet Plan </span></a></li>-->                            
                            <li><a href="#statusDetail"><span>Status History</span></a></li>
                        </ul>

                        <div id="tripDetail">
                            <c:if test = "${runsheetprintList != null}" >
                                <c:forEach items="${runsheetprintList}" var="order">
                                    <c:set var="driverName" value="${order.driverName}" />
                                </c:forEach>    
                            </c:if>

                            <table  class="table table-info mb30 table-hover" id="bg">
                                <thead><tr>
                                        <th colspan="6" >Runsheet Details</th>
                                    </tr></thead>
                                    <c:if test = "${tripDetails != null}" >
                                        <c:forEach items="${tripDetails}" var="trip">
                                        <tr>
                                            <td class="classText1" >Runsheet No</td>
                                            <td class="classText1">
                                                <c:out value="${trip.tripCode}" />
                                                <input type="hidden" name="cNotesEmail" value='<c:out value="${trip.cNotes}"/>' />
                                            </td>

                                            <td class="classText1">Scheduled On</td>
                                            <td class="classText1"><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" /> 

                                                <input type="hidden" name="customerName" Id="customerName" class="form-control" style="width:250px;height:40px" value='<c:out value="${trip.customerName}" />'>
                                                <input type="hidden" name="tripSheetId" Id="tripSheetId" class="form-control" style="width:250px;height:40px" value='<c:out value="${trip.tripId}" />'>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td class="classText1">Delivery Person</td>
                                            <td class="classText1">
                                                <c:out value="${driverName}" />
                                            </td>
                                            <td class="classText1">Vehicle No</td>
                                            <td class="classText1">
                                                <c:out value="${trip.vehicleNo}" />
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="classText1">End Km</td>
                                            <td><input name="endKm" id="endKm" type="text" class="form-control" style="width:175px;height:40px" value="" size="20"></td>
                                        </tr>
                                    </c:forEach>                                
                                </c:if>

                                <c:if test = "${runsheetprintList != null}" >
                                    <table class="table table-info mb30 table-hover" id="table" >
                                        <thead>
                                            <tr height="40">
                                                <th>Sno</th>
                                                <th>Order No</th>
                                                <th>City</th>
                                                <th>Product</th>
                                                <th>Serial No</th>
                                                <th>Qty</th>
                                                <th>Shipment Type</th>
<!--                                                <th>Exchange Serial No</th>
                                                <th>Description</th>-->
                                                <th>Status</th>
                                                <th align="center">Select All <input type="checkbox" class="" name="all" id="all" onclick="checkAll(this)"></th>
                                            </tr>
                                        </thead>
                                        <% int index = 0;
                                           int ser=1;
                                           int sno = 1;
                                           int check = 0;
                                        %>

                                        <tbody>
                                            <c:forEach items="${runsheetprintList}" var="order">
                                                <c:set var="startReportTime" value="${order.driverName}" />
                                                <tr height="30">
                                                    <td align="left" ><%=sno%></td>
                                                    <td align="left" ><c:out value="${order.consignmentOrderNo}"/></td>
                                                    <td align="left" ><c:out value="${order.point1Name}"/></td>
                                                    <td align="left" ><c:out value="${order.articleName}"/></td>
                                                    <td align="left" ><c:out value="${order.serialNo}"/></td>
                                                    <td align="left" ><c:out value="${order.noOfPackages}"/></td>
                                                    <td align="left" >
                                                        <c:if test="${order.orderType==1}">Delivery</c:if>
                                                        <c:if test="${order.orderType==2}">Exchange</c:if>
                                                        <c:if test="${order.orderType==3}">PickUp</c:if>
                                                    </td>
                                                        <input type="hidden"  value="<c:out value="${order.noOfPackages}"/>" class="text" name="totalPackages" id="totalPackages">
                                                        <input type="hidden"  value="<c:out value="${order.tripId}"/>" class="text" name="tripId" id="tripId">
                                                        <input type="hidden"  value="<c:out value="${order.vehicleNo}"/>" class="text" name="vehicleNo" id="vehicleNo">                          
                                                    <c:if test="${order.orderType!=1}">
                                                            <input type="hidden" name="returnSerial" class="form-control" id="returnSerial<%=sno%>" style="width:200px;height:35px"/>
                                                        </c:if>
                                                        <c:if test="${order.orderType==1}">
                                                            <input type="hidden" class="form-control" name="returnSerial" id="returnSerial<%=sno%>" style="width:200px;height:35px" readonly/>
                                                        </c:if>
                                                        <c:if test="${order.orderType!=1}">
                                                            <input type="hidden" name="returnDescription" class="form-control" id="returnDescription<%=sno%>" style="width:200px;height:35px"/>
                                                        </c:if>
                                                        <c:if test="${order.orderType==1}">
                                                            <input type="hidden" class="form-control" name="returnDescription" id="returnDescription<%=sno%>" style="width:200px;height:35px" readonly/>
                                                        </c:if>
                                                    <td align="left" >                                                        
                                                        <c:if test="${order.invoiceStatus == 3}">
                                                            UnDelivered
                                                        </c:if>
                                                        <c:if test="${order.invoiceStatus == 4}">
                                                            Delivered
                                                        </c:if>
                                                        <c:if test="${order.invoiceStatus !=4}">

                                                            <input type="hidden" id="serNo<%=ser%>" name="serNo" value="<c:out value="${order.serialNo}"/>">
                                                            <% ser++; %>
                                                        </c:if>

                                                    </td>

                                            <input type="hidden"  value="<c:out value="${order.orderType}"/>" class="text" name="orderType" id="orderType<%=sno%>">                        
                                            <input type="hidden" name="selectedStatus" id="selectedStatus<%=sno%>" value="0" />
                                            <input type="hidden" name="orderId" id="orderId<%=index%>"  value="<c:out value="${order.orderId}"/>"  readOnly/>
                                            <input type="hidden" name="invoiceStatus" id="invoiceStatus<%=index%>"  value="<c:out value="${order.invoiceStatus}"/>"  readOnly/>

                                            <td><input type="checkbox" style="color:dark" id="selectedIndex<%=index%>" name="selectedIndex"  onclick="checkSelectStatus(<%=sno%>, this);" value="<c:out value="${order.serialNo}"/>"></td>
                                            </tr>
                                            <%index++;%>
                                            <%sno++;%>               
                                        </c:forEach>
                                        <input type="hidden" id="ser" name="ser" value="<%=ser%>">
                                        <input type="hidden" id="ind" name="ind" value="<%=index%>">
                                        <script>
                                            function checkAll(element) {
                                                var checkboxes = document.getElementsByName('selectedIndex');
                                                if (element.checked == true) {
                                                    for (var x = 0; x < checkboxes.length; x++) {
                                                        checkboxes[x].checked = true;
                                                        $('#selectedStatus' + (x + 1)).val('1');
                                                    }
                                                } else {
                                                    for (var x = 0; x < checkboxes.length; x++) {
                                                        checkboxes[x].checked = false;
                                                        $('#selectedStatus' + (x + 1)).val('0');
                                                    }
                                                }
                                            }
                                            function checkSelectStatus(sno, obj) {

                                                if (obj.checked == true) {
                                                    document.getElementById("selectedStatus" + sno).value = 1;
                                                } else if (obj.checked == false) {
                                                    document.getElementById("selectedStatus" + sno).value = 0;
                                                }
                                            }
                                        </script>
                                        </tbody>
                                    </table>
                                    <center>
                                        <input type="button" value="Update" class="btn btn-success" onclick="submitPageDone();"/>
                                        <% 
                                           if(check==1){
                                        %>
                                        <%}else{%>
                                        &emsp;
                                        <!--<input type="button" value="Runsheet Close" class="btn btn-success" onclick="closeTrip();"/>-->                                
                                        <%}%>
                                    </center>
                                </c:if>


                        </div>

                        <!--                        <div id="routeDetail">
                        
                        <c:if test = "${tripPointDetails != null}" >
                            <table class="table table-info mb30 table-hover" >
                                <thead><tr >
                                        <th  height="30" >S No</th>
                                        <th  height="30" >Point Name</th>
                                        <th  height="30" >Type</th>

                                        <th  height="30" >Address</th>
                                        <th  height="30" >Planned Date</th>
                                        <th  height="30" >Planned Time</th>
                                    </tr></thead>
                            <% int index2 = 1;%>
                            <c:forEach items="${tripPointDetails}" var="tripPoint">
                                <%
                                            String classText1 = "";
                                            int oddEven = index2 % 2;
                                            if (oddEven > 0) {
                                                classText1 = "text1";
                                            } else {
                                                classText1 = "text2";
                                            }
                                %>
                            <tr >
                                <td class="<%=classText1%>" height="30" ><%=index2++%></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointType}" /></td>

                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                            </tr>
                            </c:forEach >
                        </table>
                        <br/>

                        <table border="0" class="border" align="centver" width="100%" cellpadding="0" cellspacing="0" style="display: none">
                            <c:if test = "${tripDetails != null}" >
                                <c:forEach items="${tripDetails}" var="trip">
                                    <tr>
                                        <td  width="150"> Estimated KM</td>
                                        <td  width="120" > <c:out value="${trip.estimatedKM}" />&nbsp;</td>
                                        <td  colspan="4">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td  width="150"> Estimated Reefer Hour</td>
                                        <td  width="120"> <c:out value="${trip.estimatedTransitHours * 60 / 100}" />&nbsp;</td>
                                        <td  colspan="4">&nbsp;</td>
                                    </tr>

                                </c:forEach>
                            </c:if>
                        </table>
                        </c:if>
                    </div>            -->

                        <div id="statusDetail">
                            <% int index1 = 1;%>

                            <c:if test = "${statusDetails != null}" >
                                <table class="table table-info mb30 table-hover" >
                                    <thead>  <tr >
                                            <th  height="30" >S No</th>
                                            <th  height="30" >Status Name</th>
                                            <th  height="30" >Remarks</th>
                                            <th  height="30" >Created User Name</th>
                                            <th  height="30" >Created Date</th>
                                        </tr></thead>
                                        <c:forEach items="${statusDetails}" var="statusDetails">
                                            <%
                                                        String classText = "";
                                                        int oddEven1 = index1 % 2;
                                                        if (oddEven1 > 0) {
                                                            classText = "text1";
                                                        } else {
                                                            classText = "text2";
                                                        }
                                            %>
                                        <tr >
                                            <td class="<%=classText%>" height="30" ><%=index1++%></td>
                                            <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.statusName}" /></td>
                                            <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripRemarks}" /></td>
                                            <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.userName}" /></td>
                                            <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripDate}" /></td>
                                        </tr>
                                    </c:forEach >
                                </table>
                                <br/>
                                <br/>
                                <br/>
                            </c:if>
                            <br>
                            <br>
                        </div>




                        <script>
                            $(".nexttab").click(function() {
                                var selected = $("#tabs").tabs("option", "selected");
                                $("#tabs").tabs("option", "selected", selected + 1);
                            });
                        </script>

                    </div>
                </form>
            </body>
            <div>
                <div>
                    <div>
                        <%@ include file="../common/NewDesign/settings.jsp" %>