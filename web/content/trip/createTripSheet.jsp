<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%@page import="java.text.SimpleDateFormat"%>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>
    <%@ page import="java.text.DecimalFormat" %>
    <%@ page import="java.text.NumberFormat" %>

    <script language="javascript" src="/throttle/js/validate.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->
    <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>

    <link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

    <link rel="stylesheet" type="text/css" href="/throttle/css/select2-bootstrap.css"/>

    <script type="text/javascript">
     =

        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script type="text/javascript">
        function calculateDate() {
            var endDates = document.getElementById("tripScheduleDate").value;
            var tempDate1 = endDates.split("-");
            var stDates = document.getElementById("preStartLocationPlanDate").value;
            var tempDate2 = stDates.split("-");
            var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0]);  // Feb 1, 2011
            var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0]);              // now
            var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
            if (prevTime.getTime() < thisTime.getTime()) {
                if (difference > 0) {
                    alert(" Selected Date is greater than scheduled date ");
                    document.getElementById("preStartLocationPlanDate").value = document.getElementById("todayDate").value;
                }
            }
        }
        function calculateTime() {
            var endDates = document.getElementById("tripScheduleDate").value;
            var endTimeIds = document.getElementById("tripScheduleTime").value;
            var tempDate1 = endDates.split("-");
            var tempTime3 = endTimeIds.split(" ");
            var tempTime1 = tempTime3[0].split(":");
            if (tempTime3[1] == "PM") {
                tempTime1[0] = 12 + parseInt(tempTime1[0]);
            }
            var stDates = document.getElementById("preStartLocationPlanDate").value;
            var hour = document.getElementById("preStartLocationPlanTimeHrs").value;
            var minute = document.getElementById("preStartLocationPlanTimeMins").value;
            var stTimeIds = hour + ":" + minute + ":" + "00";
            var tempDate2 = stDates.split("-");
            var tempTime2 = stTimeIds.split(":");
            var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0], tempTime2[0], tempTime2[1]);  // Feb 1, 2011
            var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0], parseInt(tempTime1[0]), tempTime1[1]);              // now
            var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
            var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
            if (prevTime.getTime() < thisTime.getTime()) {
                if (hoursDifference > 0) {
                    alert("Selected Time is greater than scheduled Time ");
                    document.getElementById("preStartLocationPlanTimeHrs").focus();
                }
            }
        }
    </script>
    <script type="text/javascript">

        var httpReqRouteCheck;
        var temp = "";
        function ajaxRouteCheck(pointIdValue, pointIdPrev, pointIdNext, val, pointName, pointNamePrev, pointNameNext)
        {

            var url = "/throttle/checkForRoute.do?pointIdValue=" + pointIdValue + "&pointIdPrev=" + pointIdPrev + "&pointIdNext=" + pointIdNext;

            if (window.ActiveXObject)
            {
                httpReqRouteCheck = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
                httpReqRouteCheck = new XMLHttpRequest();
            }
            httpReqRouteCheck.open("GET", url, true);
            httpReqRouteCheck.onreadystatechange = function() {
                processAjaxRouteCheck(val, pointName, pointNamePrev, pointNameNext);
            };
            httpReqRouteCheck.send(null);
        }

        function processAjaxRouteCheck(val, pointName, pointNamePrev, pointNameNext)
        {
            if (httpReqRouteCheck.readyState == 4)
            {
                if (httpReqRouteCheck.status == 200)
                {
                    temp = httpReqRouteCheck.responseText.valueOf();
                    //alert(temp);
                    var tempVal = temp.split('~');
                    if (tempVal[0] == 0) {
                        alert("valid route does not exists for the selected point: " + pointName);
                        document.getElementById("pointName" + val).value = '';
                        document.getElementById("pointOrder" + val).focus();
                    }
                    if (tempVal[0] == 1) {
                        alert("valid route does not exists between " + pointName + " and " + pointNameNext);
                        document.getElementById("pointName" + val).value = '';
                        document.getElementById("pointOrder" + val).focus();
                    }
                    if (tempVal[0] == 2) {
                        document.getElementById("pointOrder" + val).focus();
                        var tempValSplit = tempVal[1].split("-");
                        document.getElementById("pointRouteId" + (val - 1)).value = tempValSplit[0];
                        document.getElementById("pointRouteKm" + (val - 1)).value = tempValSplit[1];
                        document.getElementById("pointRouteReeferHr" + (val - 1)).value = tempValSplit[2];

                        tempValSplit = tempVal[2].split("-");
                        document.getElementById("pointRouteId" + (val + 1)).value = tempValSplit[0];
                        document.getElementById("pointRouteKm" + (val + 1)).value = tempValSplit[1];
                        document.getElementById("pointRouteReeferHr" + (val + 1)).value = tempValSplit[2];
                    }
                } else
                {
                    alert("Error loading page\n" + httpReqRouteCheck.status + ":" + httpReqRouteCheck.statusText);
                }
            }
        }




        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            $(".datepicker").datepicker({
                changeMonth: true, changeYear: true
            });
        });



        var httpReq;
        var temp = "";




        function fetchRouteInfo() {
            var preStartLocationId = document.trip.preStartLocationId.value;
            var originId = document.trip.originId.value;
            var vehicleTypeId = document.trip.vehicleTypeId.value;
            if (preStartLocationId != '') {
                var url = "/throttle/checkPreStartRoute.do?preStartLocationId=" + preStartLocationId + "&originId=" + originId + "&vehicleTypeId=" + vehicleTypeId;

                if (window.ActiveXObject)
                {
                    httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                } else if (window.XMLHttpRequest)
                {
                    httpReq = new XMLHttpRequest();
                }
                httpReq.open("GET", url, true);
                httpReq.onreadystatechange = function() {
                    processFetchRouteCheck();
                };
                httpReq.send(null);
            }

        }

        function processFetchRouteCheck()
        {
            if (httpReq.readyState == 4)
            {
                if (httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    //alert(temp);
                    if (temp != '' && temp != null && temp != 'null') {
                        var tempVal = temp.split('-');
                        document.trip.preStartLocationDistance.value = tempVal[0];
                        document.trip.preStartLocationDurationHrs.value = tempVal[1];
                        document.trip.preStartLocationDurationMins.value = tempVal[2];
                        document.trip.preStartRouteExpense.value = tempVal[3];
                        document.getElementById("preStartLocationDistance").readOnly = true;
                        document.getElementById("preStartLocationDurationHrs").readOnly = true;
                        document.getElementById("preStartLocationDurationMins").readOnly = true;
                        document.getElementById("preStartRouteExpense").readOnly = true;
                    } else {
                        alert('valid route does not exists between pre start location and trip start location');
                        document.getElementById("preStartLocationDistance").readOnly = false;
                        document.getElementById("preStartLocationDurationHrs").readOnly = false;
                        document.getElementById("preStartLocationDurationMins").readOnly = false;
                        document.getElementById("preStartRouteExpense").readOnly = false;
                    }
                    //                        if(tempVal[0] == 0){
                    //                            alert("no match found");
                    //                        }
                } else
                {
                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                }
            }
        }
        //            function processPreStartCheckBox(){
        //
        //                var tempVal = document.getElementById("preStartLocationCheckbox").checked;
        //                if(tempVal){
        //                    document.trip.preStartLocationCheckbox.value=1;
        //                    $("#preStartDetailDiv").hide();
        //                }else{
        //                    document.trip.preStartLocationCheckbox.value=0;
        //                    $("#preStartDetailDiv").show();
        //                }
        //            }
        function setButtons() {
            var temp = document.trip.actionName.value;
            if (temp != '0') {
                if (temp == '1') {
                    $("#actionDiv").show();
                    // $("#tripDiv").show();
                    //                        $("#preStartDiv").show();
                }
            } else {
                //                    $("#preStartDiv").hide();
                var vehicleId = document.trip.vehicleId.value;
                if (vehicleId != '' && vehicleId != '0') {
                    $("#actionDiv").show();
                    //  $("#tripDiv").show();
                } else {
                    $("#actionDiv").show();
                    //  $("#tripDiv").show();
                }


            }
        }
    </script>

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#tabs").tabs();
        });
    </script>

    <script>
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#driver1Name').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getDriverNames.do",
                        dataType: "json",
                        data: {
                            driverName: (request.term).trim(),
                            textBox: 1
                        },
                        success: function(data, textStatus, jqXHR) {
//                                alert("data"+data); Muthu
//                        var items = data;
//                        response(items);
                            var items = data;
                            var primaryDriver = $('#primaryDriver').val();
//                        if (items == '' && primaryDriver != '') {
//                            alert("Invalid Primary Driver Name");
//                            $('#driver1Name').val('');
//                            $('#driver1Name').val('');
//                            $('#driver1Name').focus();
//                        } else {
//                        }
                            response(items);
                        },
                        error: function(data, type) {
                            //console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var id = ui.item.Id;
                    var name = ui.item.Name;
                    var mobile = ui.item.Mobile;
                    var licenseNo = ui.item.License;
//                alert("value" + id);

                    $('#driver1Id').val(id);
                    $('#driver1Name').val(name);
                    $('#mobileNo').val(mobile);
                    $('#licenseNo').val(licenseNo);
                    return false;
                }
                // Format the list menu output of the autocomplete
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
//            alert("item" + item);
                var dlNo = item.License;
                if (dlNo == '') {
                    dlNo = '-';
                }
                var itemVal = item.Name + '(DL:' + dlNo + ')';
//            var temp = itemVal.split('-');
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });
        //end ajax for vehicle Nos
    </script>


    <script type="text/javascript" language="javascript">




        function getDriverName() {
            var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));

        }
    </script>
    <script language="">
        function print(val)
        {
            var DocumentContainer = document.getElementById(val);
            var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }

        function estimateExpense() {
            var orderIds = document.trip.orderId;
            var pointIds = document.trip.pointId;
            var pointTypes = document.trip.pointType;
            var pointOrders = document.trip.pointOrder;
            var pointAddress = document.trip.pointAddress;
            var pointPlanDates = document.trip.pointPlanDate;
            var pointPlanTimes = document.trip.pointPlanTime;

            var orderIdStr = "";
            var pointIdStr = "";
            var pointTypeStr = "";
            var pointOrderStr = "";
            var pointAddressStr = "";
            var pointPlanDateStr = "";
            var pointPlanTimeStr = "";

            for (var i = 0; i < orderIds.length; i++) {
                if (i == 0) {
                    orderIdStr = orderIds[i];
                    pointIdStr = pointIds[i];
                    pointTypeStr = pointTypes[i];
                    pointOrderStr = pointOrders[i];
                    pointAddressStr = pointAddress[i];
                    pointPlanDateStr = pointPlanDates[i];
                    pointPlanTimeStr = pointPlanTimes[i];
                } else {
                    orderIdStr = orderIdStr + "," + orderIds[i];
                    pointIdStr = pointIdStr + "," + pointIds[i];
                    pointTypeStr = pointTypeStr + "," + pointTypes[i];
                    pointOrderStr = pointOrderStr + "," + pointOrders[i];
                    pointAddressStr = pointAddressStr + "," + pointAddress[i];
                    pointPlanDateStr = pointPlanDateStr + "," + pointPlanDates[i];
                    pointPlanTimeStr = pointPlanTimeStr + "," + pointPlanTimes[i];
                }
            }
        }


        //start ajax for vehicle Nos
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#vehicleNo').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getVehicleRegNos.do",
                        dataType: "json",
                        data: {
                            vehicleNo: (request.term).trim(),
                            textBox: 1
                        },
                        success: function(data, textStatus, jqXHR) {
//                                alert(data);
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            //console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    //alert(value);
                    var tmp = value.split('-');

                    $('#vehicleId').val(tmp[0]);
                    //alert("1");
                    $('#vehicleNo').val(tmp[1]);
                    //alert("2");
                    $('#vehicleTonnage').val(tmp[2]);
                    //alert("3");
//                    $('#driver1Id').val(tmp[3]);
                    //alert("4"+tmp[3]);
//                    $('#driver1Name').val(tmp[4]);
                    //alert("5"+tmp[4]);
//                    $('#driver2Id').val(tmp[5]);
                    //alert("6"+tmp[5]);
//                    $('#driver2Name').val(tmp[6]);
                    //alert("7");
//                    $('#driver3Id').val(tmp[7]);
                    //alert("8"+tmp[7]);
//                    $('#driver3Name').val(tmp[8]);


                    //<option value="Cancel">Cancel Order</option>
                    //<option value="Suggest Schedule Change">Suggest Schedule Change</option>
                    //<option value="Hold Order for further Processing">Hold Order for further Processing</option>
                    $('#actionName').empty();
                    var actionOpt = document.trip.actionName;
                    var optionVar = new Option("-select-", '0');
                    actionOpt.options[0] = optionVar;
                    optionVar = new Option("Freeze", '1');
                    actionOpt.options[1] = optionVar;
//                        $('#actionName').append(
//                            $('<option></option>').val(0).html('-select-')
//                            )
//                        $('#actionName').append(
//                            $('<option></option>').val(1).html('Freeze')
//                            )
                    $("#actionDiv").hide();

                    //
                    //alert("9");
                    //$itemrow.find('#vehicleId').val(tmp[0]);
                    //$itemrow.find('#vehicleNo').val(tmp[1]);
                    return false;
                }
                // Format the list menu output of the autocomplete
            }).data("autocomplete")._renderItem = function(ul, item) {
                //alert(item);
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });

        //end ajax for vehicle Nos

        //
        //start ajax for pre location Nos
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#preStartLocation').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCityFromName.do",
                        dataType: "json",
                        data: {
                            cityFrom: request.term,
                            textBox: 1
                        },
                        success: function(data, textStatus, jqXHR) {
//                                alert(data);
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            //console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#preStartLocationId').val(tmp[0]);
                    $('#preStartLocation').val(tmp[1]);
                    return false;
                }
                // Format the list menu output of the autocomplete
            }).data("autocomplete")._renderItem = function(ul, item) {
                //alert(item);
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });
        //end ajax for pre location

        function submitPage() {
            var trans = document.getElementById('vendor').value;
            //       alert("fffff"+trans)

//            if (trans == '1~0') {
//                if (document.getElementById('vehicleNo').value == '' || document.getElementById('vehicleNo').value == '0') {
//                    alert("please select the Vehicle No")
//                    $("#vehicleNo").focus();
//                    return;
//                }
//                if (document.getElementById('driver1Name').value == '' || document.getElementById('driver1Name').value == '0') {
//                    alert("please select the Driver Name")
//                    $("#driver1Name").focus();
//                    return;
//                }
//            }
            var statusCheck = true;
            var temp = document.trip.actionName.value;

            if (temp == '1') { //freeze selected

            } else {
                alert("please select action");
                return;
            }
//            if (document.getElementById('vehicleCategory').value == '2') {
//            } else {
//                if (document.getElementById('vehicleNo').value == '' || document.getElementById('vehicleNo').value == '0') {
//                    alert("please select the Vehicle No")
//                    $("#vehicleNo").focus();
//                    return;
//                }
//                if (document.getElementById('driver1Name').value == '' || document.getElementById('driver1Name').value == '0') {
//                    alert("please select the Driver Name")
//                    $("#driver1Name").focus();
//                    return;
//                }
//            }
            if (statusCheck) {
                $("#save").hide();
                document.trip.action = "/throttle/saveTripSheet.do?";
                document.trip.submit();
            }
        }


        function saveAction() {
            if (document.trip.actionName.value != 0) {
                document.trip.action = "/throttle/saveAction.do?";
                document.trip.submit();
            } else {
                alert("please select your action");
                document.trip.actionName.focus();
            }
        }

        function apprAction() {
            $("#approve").hide();
            document.trip.action = "/throttle/consignmentApproveRequest.do?";
            document.trip.submit();
        }

        function setVehicleValues1() {
            //alert("value3232342354" + value);
            var value = document.trip.vehicleNo.value;
            if (value != 0) {
                var tmp = value.split('-');
                $('#vehicleId').val(tmp[0]);
                //alert("1");
                $('#vehicleNoEmail').val(tmp[1]);
                //alert("2");
                $('#vehicleTonnage').val(tmp[2]);
                //alert("3");
                $('#driver1Id').val(tmp[3]);
                //alert("4"+tmp[3]);
                $('#driver1Name').val(tmp[4]);
                //alert("5");
                $('#driver2Id').val(tmp[5]);
                //alert("6"+tmp[5]);
                $('#driver2Name').val(tmp[6]);
                //alert("7");
                $('#driver3Id').val(tmp[7]);
                //alert("8"+tmp[7]);
                $('#driver3Name').val(tmp[8]);
                $('#actionName').empty();
                var actionOpt = document.trip.actionName;
                var optionVar = new Option("-select-", '0');
                actionOpt.options[0] = optionVar;
                optionVar = new Option("Freeze", '1');
                actionOpt.options[1] = optionVar;
                $("#actionDiv").hide();
            } else {
                $('#vehicleId').val('');
                $('#vehicleNoEmail').val('');
                $('#vehicleTonnage').val('');
                $('#driver1Id').val('');
                $('#driver1Name').val('');
                $('#driver2Id').val('');
                $('#driver2Name').val('');
                $('#driver3Id').val('');
                $('#driver3Name').val('');
                $('#actionName').empty();
                var actionOpt = document.trip.actionName;
                var optionVar = new Option("-select-", '0');
                actionOpt.options[0] = optionVar;
                optionVar = new Option("Cancel Order", 'Cancel');
                actionOpt.options[1] = optionVar;
                optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                actionOpt.options[2] = optionVar;
                optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                actionOpt.options[3] = optionVar;
                $("#actionDiv").show();
            }
        }
        function setVehicleValuesNew() {
            if ($('#vehicleId').val() != '' && $('#vehicleTonnage').val() != '' && $('#driver1Id').val() != '' && $('#driver1Name').val() != '' && $('#driver2Id').val() != '' && $('#driver2Name').val() != '' && $('#driver3Id').val() != '' && $('#driver3Name').val() != '') {
                $('#actionName').empty();
                var actionOpt = document.trip.actionName;
                var optionVar = new Option("-select-", '0');
                actionOpt.options[0] = optionVar;
                optionVar = new Option("Freeze", '1');
                actionOpt.options[1] = optionVar;
                $("#actionDiv").hide();
            }
        }
        function setVehicleValues() {
            var value = document.trip.vehicleNo.value;

            if (value != 0) {
                var tmp = value.split('-');
                $('#vehicleId').val(tmp[0]);
                //alert("1");
                $('#vehicleNoEmail').val(tmp[1]);
                //alert("2");
                $('#vehicleTonnage').val(tmp[2]);
                //alert("3");
//                $('#driver1Id').val(tmp[3]);
//                alert("4===="+tmp[3]);
//                $('#driver1Id').val(tmp[3]);
//                alert("4nnnn==="+tmp[4]);
//                $('#driver1Name').val(tmp[4]);
                //alert("5");
                // $('#driver2Id').val(tmp[5]);
                //alert("6"+tmp[5]);
                // $('#driver2Name').val(tmp[6]);
                //alert("7");
                // $('#driver3Id').val(tmp[7]);
                //alert("8"+tmp[7]);
                // $('#driver3Name').val(tmp[8]);


                $('#permitExpiryDate').text(tmp[9]);
                $('#insuranceExpiryDate').text(tmp[10]);
                $('#roadExpiryDate').text(tmp[11]);
                $('#fcExpiryDate').text(tmp[12]);

                var currentDate = document.getElementById("currentDate").value;
                var nextDate = document.getElementById("nextDate").value;

                var tempDate11 = nextDate.split("/");
                var tempDate21 = tmp[9].split("/");
                var tempDate22 = tmp[10].split("/");
                var tempDate23 = tmp[11].split("/");
                var tempDate24 = tmp[12].split("/");
                var fcExpiry = new Date(tempDate21[2], tempDate21[1], tempDate21[0]);  // Feb 1, 2011
                var insuranceExpiry = new Date(tempDate22[2], tempDate22[1], tempDate22[0]);  // Feb 1, 2011
                var permitExpiry = new Date(tempDate23[2], tempDate23[1], tempDate23[0]);  // Feb 1, 2011
                var roadTaxExpiry = new Date(tempDate24[2], tempDate24[1], tempDate24[0]);  // Feb 1, 2011
                var nextweekTime = new Date(tempDate11[2], tempDate11[1], tempDate11[0]);              // now


//                if (fcExpiry.getTime() <= nextweekTime.getTime()) {
//
//                    alert("FC date is going to expiry.");
//                    resetValue();
//                } else if (insuranceExpiry.getTime() <= nextweekTime.getTime()) {
//                    alert("Insurence date is going to expiry");
//                    resetValue();
//                    document.getElementById("vehicleNo").value = "";
//                } else if (permitExpiry.getTime() <= nextweekTime.getTime()) {
//                    alert("Permit date going to expiry");
//                    resetValue();
//
//                } else if (roadTaxExpiry.getTime() <= nextweekTime.getTime()) {
//                    alert("RoadTax date going to expiry ");
//                    resetValue();
//
//                }
                $('#actionName').empty();
                var actionOpt = document.trip.actionName;
                var optionVar = new Option("-select-", '0');
                actionOpt.options[0] = optionVar;
                optionVar = new Option("Freeze", '1');
                actionOpt.options[1] = optionVar;
                $("#actionDiv").hide();

            } else {
                $('#vehicleId').val('');
                $('#vehicleNoEmail').val('');
                $('#vehicleTonnage').val('');
                $('#driver1Id').val('');
                $('#driver1Name').val('');
                $('#driver2Id').val('');
                $('#driver2Name').val('');
                $('#driver3Id').val('');
                $('#driver3Name').val('');
                $('#actionName').empty();
                $('#fcExpiryDate').text('');
                $('#insuranceExpiryDate').text('');
                $('#permitExpiryDate').text('');
                $('#roadExpiryDate').text('');
                var actionOpt = document.trip.actionName;
                var optionVar = new Option("-select-", '0');
                actionOpt.options[0] = optionVar;
                optionVar = new Option("Cancel Order", 'Cancel');
                actionOpt.options[1] = optionVar;
                optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                actionOpt.options[2] = optionVar;
                optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                actionOpt.options[3] = optionVar;
                $("#actionDiv").show();
            }
//            if (tmp[3] == 0) {
//                alert("Please Map Primary Driver and plan trip");
//                $('#vehicleNo').val(0);
//                $('#vehicleId').val('');
//                $('#vehicleNoEmail').val('');
//                $('#vehicleTonnage').val('');
//                $('#driver1Id').val('');
//                $('#driver1Name').val('');
//                $('#driver2Id').val('');
//                $('#driver2Name').val('');
//                $('#driver3Id').val('');
//                $('#driver3Name').val('');
//                $('#actionName').empty();
//                var actionOpt = document.trip.actionName;
//                var optionVar = new Option("-select-", '0');
//                actionOpt.options[0] = optionVar;
//                optionVar = new Option("Cancel Order", 'Cancel');
//                actionOpt.options[1] = optionVar;
//                optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
//                actionOpt.options[2] = optionVar;
//                optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
//                actionOpt.options[3] = optionVar;
//                $("#actionDiv").show();
//                $("#tripDiv").hide();
//            }
        }
        function computeVehicleCapUtil() {
            setVehicleValues();
            var orderWeight = document.trip.totalWeight.value;
            var vehicleCapacity = document.trip.vehicleTonnage.value;
            if (vehicleCapacity > 0) {
                var utilPercent = (orderWeight / vehicleCapacity) * 100;
                document.trip.vehicleCapUtil.value = utilPercent.toFixed(2);


                var e = document.getElementById("vehicleCheck");
                e.style.display = 'block';
            } else {
                document.trip.vehicleCapUtil.value = 0;
            }
        }

        function computeVehicleCapUtilNew() {
            setVehicleValuesNew();
            var orderWeight = document.trip.totalWeight.value;
            var vehicleCapacity = document.trip.vehicleTonnage.value;
            if (vehicleCapacity > 0) {
                var utilPercent = (orderWeight / vehicleCapacity) * 100;
                //document.trip.vehicleCapUtil.value = utilPercent.toFixed(2);


                var e = document.getElementById("vehicleCheck");
                e.style.display = 'block';
            } else {
                //document.trip.vehicleCapUtil.value = 0;
            }
        }

        function resetValue() {
            document.getElementById('vehicleNo').selectedIndex = 0;
            document.getElementById("driver1Name").value = "";
            document.getElementById("driver2Name").value = "";
            document.getElementById("driver3Name").value = "";
            document.getElementById("vehicleCapUtil").value = "";
            document.getElementById("vehicleTonnage").value = "";
//            $("#save").hide();
            return false;
        }

        function reset()
        {
            // alert("test");
            $('#vehicleNo').each(function() {
                if (this.defaultSelected) {
                    this.selected = true;
                    return false;
                }
            });

        }
    </script>
</head>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.RunSheet Planning" text="RunSheet Planning"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.RunSheet Planning" text="RunSheet Planning"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <script>
                function multiPickupShow() {

                    document.getElementById("pointPlanDate").readOnly = true;
                }

            </script>

            <body onload="multiPickupShow();" >

                <form name="trip" method="post">
                    <%
                    Date today = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String startDate = sdf.format(today);
                     Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.DATE, 7);
                       String nextDate = sdf.format(cal.getTime());
                    %>

                    <input type="hidden" name="currentDate" id="currentDate" value='<%=startDate%>'/>
                    <input type="hidden" name="nextDate" id="nextDate" value='<%=nextDate%>'/>

                    <%
                    String vehicleMileageAndTollRate = "";
                    String vehicleMileage = "0";
                    String tollRate = "0";
                    String fuelPrice = "0";
                    String[] temp = null;
                    if(request.getAttribute("vehicleMileageAndTollRate") != null){
                        vehicleMileageAndTollRate = (String)request.getAttribute("vehicleMileageAndTollRate");
                        temp = vehicleMileageAndTollRate.split("-");
                        vehicleMileage = temp[0];
                        tollRate = temp[1];
                        fuelPrice = temp[2];
                    }
                    %>
                    <input type="hidden" name="vehicleMileage" value="<%=vehicleMileage%>" />
                    <input type="hidden" name="tollRate" value="<%=tollRate%>" />
                    <input type="hidden" name="fuelPrice" value="<%=fuelPrice%>" />
                    <input type="hidden" name="vehicleNoEmail" id="vehicleNoEmail" value="" />

                    <c:set var="cNotes" value="" />
                    <c:set var="routeInfo" value="" />
                    <c:set var="customerName" value="" />
                    <c:set var="customerType" value="" />
                    <c:set var="vehicleType" value="" />
                    <c:set var="billingType" value="" />
                    <c:set var="consginmentRemarks" value="" />
                    <c:set var="tripSchedule" value="" />
                    <c:set var="routeInfo" value="" />
                    <c:set var="totalPoints" value="" />
                    <c:set var="totalHours" value="" />
                    <c:set var="reeferRequired" value="" />
                    <c:set var="totalPackages" value="" />
                    <c:set var="orderRevenue" value="" />
                    <%--<c:set var="orderExpense" value="" />--%>
                    <c:set var="profitMargin" value="" />
                    <c:set var="totalWeight" value="" />
                    <c:set var="productInfo" value="" />
                    <c:set var="vehicleCapUtilValue" value="" />
                    <c:set var="editFreightAmount" value="" />

                    <% int i=1; %>                    
                  
                    <br>
                    <br>
                   
                    <div id="tabs">
                        <ul class="nav nav-tabs">
                            <li class="active" data-toggle="tab"><a href="#tripDetail"><span>RunSheet Details</span></a></li>
                            <li data-toggle="tab"><a href="#orderDetail"><span>Order Details</span></a></li>

<!--                            <c:if test="${cbt == 0}">
                                <li  data-toggle="tab"><a href="#CBTDetail"><span>Load Details</span></a></li>
                                </c:if>
                            <li  data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>                            -->
                        </ul>


                        <script>
                            function setVehicleType(val) {
                                if (val != '0~0') {
                                    document.getElementById("hireVehicleNo").value = "";
//                                    document.trip.elements['hireVehicleNo'].style.display = 'none';
                                    document.getElementById("vehicleNo").style.display = "block";
//                                document.getElementById("veh2").style.display = "block";
//                                   document.getElementById("veh3").style.display = "none";
                                    var originId = $("#originId").val();
                                    var destinationId = $("#destinationId").val();
                                    var ownership = $("#ownership").val();
                                    $('#vehicleNo').empty();
                                    $('#vehicleNo').append($('<option></option>').val(0).html('--select--'))
                                    var vendorTemp = document.getElementById("vendor").value;
                                    var temp = vendorTemp.split("~");
                                    if (temp[1] == '1' || temp[1] == '0') {
                                        document.trip.elements['driverName'].style.display = 'none';
                                        document.trip.elements['driver1Name'].style.display = 'block';
                                        document.trip.elements['Name'].style.display = 'block';//KKK
                                        $("tr.vehCategory").hide();
                                        $('#veh2').show();
                                        $('#veh9').hide();
                                    } else {
                                        document.getElementById("vehicleId").value = "0";
                                        document.trip.elements['driver1Name'].style.display = 'block';
                                        document.trip.elements['driverName'].style.display = 'none';
                                        document.trip.elements['Name'].style.display = 'none';//KKK
                                        $("tr.vehCategory").show();
                                    }

                                    $.ajax({
                                        url: "/throttle/getVendorVehicleType.do",
                                        dataType: "json",
                                        data: {
                                            vendorId: temp[0],
                                            ownership: ownership
                                        },
                                        success: function(data) {
                                            //  alert(data);
                                            if (data != '') {
                                                $('#vehicleTypeName').empty();
                                                $('#vehicleTypeName').append(
                                                        $('<option></option>').val(0).html('--select--'))
                                                //                                            $('#vehicleTypeName').append(
                                                //                                                    $('<option></option>').val(1058).html('20\''))
                                                //                                            $('#vehicleTypeName').append(
                                                //                                                    $('<option></option>').val(1059).html('40\''))
                                                $.each(data, function(i, data) {
                                                    var vehicleTypeId1 = data.Id;
                                                    //alert(vehicleTypeId1);
                                                    var temp1 = vehicleTypeId1.split("~");
                                                    $('#vehicleTypeName').append(
                                                            $('<option style="width:90px"></option>').attr("value", temp1[0]).text(data.Name)
                                                            )
                                                });
                                            } else {
                                                $('#vehicleTypeName').empty();
                                                $('#vehicleTypeName').append(
                                                        $('<option></option>').val(0).html('--select--'))
                                            }
                                        }
                                    });
                                } else {
                                    resetAll();
                                    $('#vehicleTypeName').empty();
                                    $('#vehicleTypeName').append(
                                            $('<option></option>').val(0).html('--select--'))
                                    $('#lhcNo').empty();
                                    $('#lhcNo').append(
                                            $('<option></option>').val(0).html('--select--'))
                                    $('#agreedRate').val(0);
                                }
                            }



//                            function showVehicle() {
//                                var vehCategory = document.getElementById("vehicleCategory").value;
//                                if (vehCategory == '1') {
//                                    document.trip.elements['vehicleNo'].style.display = 'block';
//                                    document.getElementById("hireVehicleNo").value = "";
//                                    document.trip.elements['hireVehicleNo'].style.display = 'none';
//                                    document.getElementById("vehicleNo").style.display = "block";
////                                    document.getElementById("veh2").style.display = "block";
////                                    document.getElementById("veh3").style.display = "none";
//                                    document.getElementById("hireVehicleNo").style.display = "none";
//                                    $('#veh2').show();
//                                    $('#veh9').hide();
//                                } else {
//                                    document.getElementById("vehicleNo").style.display = "none";
////                                    document.getElementById("veh2").style.display = "none";
////                                    document.getElementById("veh3").style.display = "block";
//                                    document.getElementById("hireVehicleNo").style.display = "block";
//                                    document.trip.elements['vehicleNo'].style.display = 'none';
//                                    $('#vehicleNo').empty();
//                                    $('#vehicleNo').append($('<option></option>').val(0).html('--select--'))
//                                    document.getElementById("hireVehicleNo").value = "Hired 001";
//                                    document.trip.elements['hireVehicleNo'].style.display = 'block';
//                                    document.getElementById("ownership").value = '3';
//                                    document.getElementById("projectedExpense").innerHTML = "Rs. " + parseInt(0);
//                                    document.getElementById("orderExpense").value = "0.00";
//                                    $('#veh2').hide();
//                                    $('#veh9').show();
//                                }
//                            }

                            function setVehicle() {
                                // alert("hiii");
                                if($("#ownership").val() == '1'){
                                    $("#veh").show();
                                    $("#veh1").show();
                                    $("#hireVeh1").hide();
                                    $("#lhcSpan").hide();
                                }else if($("#ownership").val() == '2'){
                                    $("#veh").show();
                                    $("#hireVeh1").show();
                                    $("#lhcSpan").show();
                                    $("#veh1").hide();
                                }else{
                                    $("#veh").hide();
                                    $("#veh1").hide();
                                    $("#hireVeh1").hide();
                                    $("#lhcSpan").hide();
                                }
                                var statusId = $("#tripStatus").val();
                                var vendorTemp = document.getElementById("vendor").value;
                                var temp = vendorTemp.split("~");
                                var vehicletypename = $("#vehicleTypeName option:selected").text(); //alert("hiii"+vehicletypename);
                                document.getElementById("vehicleType").value = vehicletypename;
                                var vtid = $("#vehicleTypeName option:selected").val();
                                var vtid = vtid.split("-")[0];
                                document.getElementById("vehicleTypeIdSelected").value = vtid;
                                //var statusId = arr[0];
                                $.ajax({
                                    url: "/throttle/getVendorVehicle.do",
                                    dataType: "json",
                                    data: {
                                        vehicleTypeId: $("#vehicleTypeName").val(),
                                        vendorId: temp[0],
                                        statusId: statusId
                                    },
                                    success: function(data) {
                                        // alert(data);
                                        if (data != '') {
                                            $('#vehicleNo').empty();
                                            $('#vehicleNo').append(
                                                    $('<option style="width:240px"> </option>').val(0).html('--select--'))
                                            $.each(data, function(i, data) {
                                                $('#vehicleNo').append(
                                                        $('<option style="width:240px"></option>').attr("value", data.Id).text(data.Name)
                                                        )
                                            });
                                        } else {
                                            $('#vehicleNo').empty();
                                            $('#vehicleNo').append(
                                                    $('<option style="width:240px"></option>').val(0).html('--select--'))
                                        }
                                    }
                                });
                                $('#actionName').empty();
                                var actionOpt = document.trip.actionName;
                                var optionVar = new Option("-select-", '0');
                                actionOpt.options[0] = optionVar;
                                optionVar = new Option("Freeze", '1');
                                actionOpt.options[1] = optionVar;
                                //                        $('#actionName').append(
                                //                            $('<option></option>').val(0).html('-select-')
                                //                            )
                                //                        $('#actionName').append(
                                //                            $('<option></option>').val(1).html('Freeze')
                                //                            )
                                $("#actionDiv").hide();

                                setVehicleTonnage();
                            }
                            function  setVehicleTonnage() {
                                var temp = "";

                                var vehicleTypeTemp = document.getElementById("vehicleTypeName").value;
                                // alert(vehicleTypeTemp);
                                temp = vehicleTypeTemp.split('-');

                                document.getElementById("vehicleTonnage").value = parseInt((temp[1].trim()));
                                //   document.getElementById("vehicleTypeName").value ="1";// string.valueOf((temp[0].trim()));
                            }


                            function resetAll() {
                                // alert("reset ...");
                                //document.getElementById("vehicleTypeName").value = "0~0";
                                document.getElementById("tripStatus").value = "20";
                                document.getElementById("vehicleNo").value = "0";
                                document.getElementById("driver1Id").value = "0";
                                document.getElementById("driver1Name").value = "";
                                document.getElementById("vehicleCheck").style.display = "none";

                                $("#approvalStatusSpan").text('');
                                var selectedIndex = document.getElementsByName("selectedIndex");
                                for (var i = 0; i < selectedIndex.length; i++) {
                                    document.getElementById("selectedIndex" + (i + 1)).checked = false;
                                }


                                var actionOpt = document.trip.actionName;
                                var optionVar = new Option("-select-", '0');
                                actionOpt.options[0] = optionVar;
                                optionVar = new Option("Cancel Order", 'Cancel');
                                actionOpt.options[1] = optionVar;
                                optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                                actionOpt.options[2] = optionVar;
                                optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                                actionOpt.options[3] = optionVar;
                                $("#actionDiv").show();
                            }

                        </script>
                        <div id="tripDetail">
                            <!--<table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">-->
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th class="contenthead" colspan="6" >Trip Details</th>
                                    </tr>
                                <thead>
                                    <tr style="display:none">                                       
                                        <td >CNote No(s)</td>
                                        <td >
                                            <c:out value="${cNotes}" />
                                            <input type="hidden" name="cNotes" Id="cNotes"  class="form-control" style="width:240px;height:40px" class="textbox" value='<c:out value="${cNotes}" />'>
                                        </td>
                                        <td >Billing Type</td>
                                        <td >
                                            <c:out value="${billingType}" />
                                            <input type="hidden" name="billingType" Id="billingType"  class="form-control" style="width:240px;height:40px"  value='<c:out value="${billingType}" />'>
                                            <input type="hidden" name="billingTypeId" Id="billingTypeId"  class="form-control" style="width:240px;height:40px"  value='<c:out value="${billingTypeId}" />'>
                                        </td>
                                    </tr>
                                    <tr  style="display:none">

                                        <td >Customer Name</td>
                                        <td >
                                            <c:out value="${customerName}" />
                                            <input type="hidden" name="customerName" Id="customerName"  class="form-control" style="width:240px;height:40px"  value='<c:out value="${customerName}" />'>
                                        </td>
                                        <td >Customer Type</td>
                                        <td  >
                                            <c:out value="${customerType}" />
                                            <input type="hidden" name="customerType" Id="customerType"  class="form-control" style="width:240px;height:40px"  value='<c:out value="${customerType}" />'>
                                            <input type="hidden" name="tripType" Id="tripType" class="textbox" value='<c:out value="${tripType}" />'>
                                            <input type="hidden" name="cbt" Id="cbt" class="textbox" value='<c:out value="${cbt}" />'>
                                        </td>
                                    </tr>
                                    <tr  style="display:none">
                                        <td >Route Name</td>
                                        <td >
                                            <c:out value="${routeInfo}" />
                                            <input type="hidden" name="routeInfo" Id="routeInfo"  class="form-control" style="width:240px;height:40px"  value='<c:out value="${routeInfo}" />'>
                                        </td>

                                        <td ></td>
                                        <td  >
                                           
                                            <%--<c:out value="${totalPackages}" />--%>
                                            <%--<c:out value="${reeferRequired}" />--%>
                                            <input type="hidden" name="reeferRequired" Id="reeferRequired"  class="form-control"   value='<c:out value="${reeferRequired}" />'>
                                        </td>
                                    </tr>
                                    
                                    
                                <tr >
                                    <td >Ownership</td>
                                    <td >
                                        <select name="ownership" id="ownership"  class="form-control" style="width:240px;height:40px" >
                                            <option value='0' >--select--</option>
                                            <option value='1' > Dedicated </option>
                                            <option value='2' > Hire </option>
                                        </select>
                                    </td>
                                
                                <td ><font color="red">*</font>Transporter</td>
                                <td >
                                    <select name="vendor" id="vendor" onchange="setVehicleType(this.value);
                                            resetAll();
                                            " class="form-control" style="width:240px;height:35px;">

                                        <c:if test="${vendorList != null}">
                                            <option value="0" selected>--select--</option>
                                            <!--<option value="1~0" >Own</option>-->
                                            <c:forEach items="${vendorList}" var="veh">
                                                <option value='<c:out value="${veh.vendorId}"/>'><c:out value="${veh.vendorName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                </td>
                            </tr>
                                
                                <tr>

                                <input type="hidden" name="totalWeight" Id="totalWeight"  class="form-control" style="width:240px;height:40px"  value='<c:out value="${totalWeight}" />' readonly >
                                <input type="hidden" name="vehicleType" Id="vehicleType"  class="form-control" style="width:240px;height:40px"  value='<c:out value="${vehicleType}" />'>

                                

                                <td ><font color="red">*</font>Vehicle Type</td>
                                <td >
                                    <select name="vehicleTypeName" id="vehicleTypeName" onchange="resetAll();
                                            setVehicle();"
                                            class="form-control" style="width:240px;height:35px;">
                                            <!--setVehicleLHC();-->

                                    </select>
                                    <input type="hidden" name="vehicleTypeIdSelected" Id="vehicleTypeIdSelected" class="form-control" value=''/>
                                </td>
                                

                                <SCRIPT language=Javascript>

                                    function isNumberKey(evt)
                                    {
                                        var charCode = (evt.which) ? evt.which : evt.keyCode;
                                        if (charCode != 46 && charCode > 31
                                                && (charCode < 48 || charCode > 57))
                                            return false;

                                        return true;
                                    }

                                </SCRIPT>
                                <!--<input type="hidden" name="ownership" id="ownership" class="form-control" value="0">-->
                                    <td style="display:none" id="veh">Vehicle No</td>
                                    <td style="display:none" id="veh1">
                                        <select  name="vehicleNo" id="vehicleNo"  class="form-control" onchange="checkIstrackable(this.value)" style="width:240px;height:40px;display:none"  >

                                        </select>
                                        <script>
                                            $('#vehicleNo').select2({placeholder: 'Fill vehicleNo'});
                                        </script>
                                    </td>
                                    <td id="hireVeh1" style="display:none">
                                        <input type="text" id="hireVehicleNo" name="hireVehicleNo" class="form-control" style="width:240px;height:40px;" value="Hired" onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9]/g, '')" maxlength="10"/>
                                    </td>
                                </tr>   
                                <input type="hidden" name="vehicleId" Id="vehicleId"  class="form-control"   value="0">
                                <td style="display:none" align="left" >Vehicle Capacity (MT)</td>
                                <td style="display:none" ><input type="text" name="vehicleTonnage" Id="vehicleTonnage" readonly  class="form-control" style="width:240px;height:40px"  value="<c:out value="${vehicleTonnage}"/>"></td>
                                </tr>
                                
                                <tr style="display:none" id="lhcSpan">
                                     <td >
                                        <font color="red">*</font>LHC No
                                    </td>
                                    <td >
                                        <select name="lhcNo" id="lhcNo" onchange="setRateAgainstLHC(this.value);"  class=" form-control" style="width:240px;height:35px;"  >
                                            <option value="">--Select--</option>
                                        </select>
                                        <input type="hidden" name="lhcId" Id="lhcId" class="form-control" value='0'/>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td >
                                        <font color="red">*</font>Delivery Executive
                                    </td>
                                    <td >
                                        <select name="deliveryExecutiveId" id="deliveryExecutiveId" class=" form-control" style="width:240px;height:35px;"  >
                                        <c:if test="${deliveryExecutiveList != null}">
                                            <option value="0">--Select--</option>
                                            <c:forEach items="${deliveryExecutiveList}" var="del">
                                               <option value='<c:out value="${del.empId}"/>'><c:out value="${del.empName}"/></option>
                                            </c:forEach>
                                        </c:if>

                                        </select>
                                    </td>
                                    <td >
                                        
                                    </td>
                                    <td >
                                        <input type="hidden" name="deliveryHelperId" Id="deliveryHelperId" value="0">
                                    </td>
                                </tr>
                                 <tr>
                                     <td > Driver Name </td>
                                    <td >
                                        <input type="text" name="drivName" Id="drivName" class="form-control" style="width:240px;height:35px;"  value="">
                                        <input type="hidden" name="drivId" Id="drivId" class="form-control" style="width:240px;height:35px;"  value="0">
                                    </td>                                   
                                     <td > Driver Mobile </td>
                                    <td ><input type="text" name="drivMobile" Id="drivMobile" onKeyPress="return onKeyPressBlockCharacters(event);" class="form-control" style="width:240px;height:35px;"  value="">
                                    </td>                                   
                                </tr>
                                <tr>
                                   
                                    <td >Special Instruction</td>
                                    <td ><textarea name="tripRemarks" cols="20" rows="2"  class="form-control" style="width:240px;height:40px" > <c:out value="${consginmentRemarks}" /></textarea></td>
                                     <td > Agreed Rate </td>
                                    <td ><input type="text" name="agreedRate" Id="agreedRate" readonly  class="form-control" style="width:240px;height:35px;"  value="0"/>
                                        <input type="hidden" name="lhcDate" Id="lhcDate" readonly  class="form-control " style="width:240px;height:40px"  value=""/>
                                    </td>                                   
                                </tr> 
                                <tr style="display:none">
                                    <td style="display:none">Veh. Cap [Util%]</td>
                                    <td style="display:none"><input type="text" name="vehicleCapUtil" Id="vehicleCapUtil" readonly  class="form-control" style="width:240px;height:40px"  value='<c:out value="${vehicleCapUtilValue}" />'></td>
                                </tr>
                                <tr>
                                    <td style="display:none">Driver Selection</td>
                                    <td style="display:none"><select id="Name" name="Name" class="form-control" style="width:240px;height:40px;display:none" onchange="setDriverIdName(this.value);" >
                                            <option value="1" >Existing</option>
                                            <option value="2" >New</option>
                                        </select></td>
                                <script>
                                    function setDriverIdName(val) {
                                        if (val == 2) {
                                            document.getElementById("driver1Name").value = "";
                                            document.getElementById("driver1Id").value = "0";
                                            $("#vehOwn").hide();
                                            $("#vehHire").show();
                                        } else {
                                            setVehicleValues();
                                        }

                                    }
                                </script>
                                <td style="display:none">Primary Driver <c:out value="${primaryDriverName}"/> </td>
                                <td style="display:none" class="vehOwn" id="vehOwn" >
                                    <input type="text" name="driver1Name"   Id="driver1Name" class="form-control"  value="<c:out value="${primaryDriverName}"/>" style="display: none;width:240px;height:40px" >
                                    <input type="hidden" name="driver1Id"   Id="driver1Id" class="form-control" style="width:150px;height:30px;"  style="display: none" value="" >
                                    <select id="driverName" name="driverName" onchange="setDriverId();" style="display: none">
                                        <c:if test="${driverList != null}">
                                            <option value="" selected>--Select--</option>
                                            <c:forEach items="${driverList}" var="driverList">
                                                <option value="<c:out value="${driverList.empId}"/>"><c:out value="${driverList.empName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                    <input type="hidden" name="driver1Id" Id="driver1Id" class="form-control" value='0'>

                                </td>
                                <td class="vehHire" id="vehHire" style="display: none">
                                    <input type="text" id="hireVehicleDriver" name="hireVehicleDriver" class="form-control" style="width:150px;height:30px;" value="" />

                                </td>
                                <td style="display:none" > Mobile No </td>
                                <td style="display:none" ><label id="mobileNo"></label></td>
                                <td style="display: none">Secondary Driver(s) </td>
                                <td colspan="5" style="display: none">
                                    <input type="text" name="driver2Name"  readonly Id="driver2Name" class="form-control" style="width:150px;height:30px;" value="<c:out value="${secondaryDriver1Name}"/>"  >
                                    <input type="hidden" name="driver2Id" Id="driver2Id" class="form-control" style="width:150px;height:30px;" value='0'  >
                                    <input type="text" name="driver3Name"  readonly  Id="driver3Name" class="form-control" style="width:150px;height:30px;"value="<c:out value="${secondaryDriver2Name}"/>"  >
                                    <input type="hidden" name="driver3Id" Id="driver3Id"class="form-control" style="width:150px;height:30px;" value='0' >
                                </td>
                                </tr>
                                <tr style="display:none">

                                    <td style="display:none">Trip Status</td>
                                    <td style="display:none">
                                        <select name="tripStatus" id="tripStatus" onchange="setVehicle();"  class="form-control" style="width:240px;height:40px"  disabled>
                                            <c:if test="${statusList != null}">
                                                <c:forEach items="${statusList}" var="status">
                                                    <option value='<c:out value="${status.statusId}"/>' ><c:out value="${status.statusName}"/></option>
                                                </c:forEach>
                                                <!--                                            <option value="0" selected >All Vehicle</option>-->
                                            </c:if>
                                        </select>

                                    </td>
                                </tr>

                                <tr style="display:none" class ="vehCategory" id="vehCate" >
                                    <td style="display:none" >
                                        <font color="red">*</font>Vehicle Category
                                    </td>
                                    <td style="display:none" >
                                        <select name="vehicleCategory" id="vehicleCategory"  class="form-control" style="width:150px;height:30px;" onchange="showVehicle();
                                                resetAll();
                                                getHireVendorTripExpense();
                                                setVehicle();
                                                ">
                                            <option value="0"  >---select---</option>
                                            <option value="1" >Leased </option>
                                            <option value="2" selected >Hire</option>
                                        </select>
                                    </td>

                                </tr>
                                <tr style="display:none">
                                    <td >Product / Temp Info </td>
                                    <td ><c:out value="${productInfo}" /></td>
                                    <td >Trip Schedule</td>
                                    <td ><c:out value="${tripSchedule}" /></td>
                                </tr>
                            </table>
                            <br/>
                            <br/>

                            <center>
                                <font size="2" color="green"><b> 
                                </b></font>
                                <br/>
                            </center>
                        </div>

                        <div id="orderDetail">
                            <!--<table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">-->
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr height="40">
                                        <th class="contenthead" colspan="10" >Order Details</th>
                                    </tr>
                                <thead>
                         <c:if test = "${selectedTripPlanningList != null}" >

                            <thead>
                                <tr height="40">
                                    <th>S.No</th>                                    
                                    <th>Customer</th>                                    
                                    <th>Batch Number</th>
                                    <th>Batch Date</th>
                                    <th>Branch</th>
                                    <th>Loan Proposal ID</th>
                                    <th>Product</th>                                                                        
                                    <th>Supplier</th>
                                    <th>Select</th>
                                </tr>
                            </thead>
                            <% int index = 0;
                               int sno = 1;
                            %>
                            <tbody>
                                <c:forEach items="${selectedTripPlanningList}" var="cnl">

                                <tr height="30">
                                <td align="left" ><%=sno%></td>
                                <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                <input type="hidden" style="width: 184px;" name="orderId" id="orderId<%=index%>" class="form-control" style="width:250px;height:40px"   value="<c:out value="${cnl.orderId}"/>"  readOnly maxlength="50">
                                <td align="left" ><c:out value="${cnl.clientName}"/></td>
                                <td align="left" ><c:out value="${cnl.batchNumber}"/></td>                                        
                                <td align="left" ><c:out value="${cnl.batchDate}"/></td>
                                <td align="left" ><c:out value="${cnl.branchName}"/></td>
                                <td align="left" ><c:out value="${cnl.loanProposalID}"/></td>
                                <td align="left" ><c:out value="${cnl.productID}"/></td>
                                <td align="left" ><c:out value="${cnl.supplierName}"/></td>
                                <td align="left" ><input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno%>" value="<%=sno%>"  onclick="checkSelectStatus(<%=sno%>, this);" />
                                    <input type="text" name="selectedStatus" id="selectedStatus<%=sno%>" value="1" />
                                    <!--<input type="button" class="btn btn-success" name="Schedule" id="Schedule" value="Schedule" onclick="scheduleDelivery('<c:out value="${cnl.orderId}"/>')" />-->
                                </td>

                                </tr>
                                <%index++;%>
                                <%sno++;%>
                            </c:forEach>
                            </tbody>
                            
                        </table>
                    <script> 
                       
                        function checkSelectStatus(sno ,obj){
//                            alert(sno)
                            var val = document.getElementsByName("selectedStatus");
                            if (obj.checked == true) {
                                document.getElementById("selectedStatus" + sno).value = 1;
                            } else if (obj.checked == false) {
                                document.getElementById("selectedStatus" + sno).value = 0;
                            }
                            var vals=0;
                            for(var i=0;i<= val.length;i++){
                            }
                        } 
                    </script>
                            
                    </c:if>
                            
                             <br>
                                    <!--<table border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" >-->
                                    <table class="table table-info mb30 table-hover"  >
                                        <tr>
                                            <td >Action: </td>
                                            <td  >
                                                <select name="actionName" id="actionName" onChange="setButtons();" class="form-control" style="width:150px;height:40px">
                                                    <option value="0">-select-</option>
                                                    <option value="Cancel">Cancel Order</option>
                                                    <option value="Suggest Schedule Change">Suggest Schedule Change</option>
                                                    <option value="Hold Order for further Processing">Hold Order for further Processing</option>
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td  >Remarks</td>
                                            <td  ><textarea name="actionRemarks" rows="3" cols="100" class="form-control" style="width:240px;height:40px"></textarea> </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td  >Do you want to intimate?</td>
                                            <td  >
                                                Client <input type="checkbox"  name="client" value="1" /> &nbsp;
                                                A/C Mgr<input type="checkbox"  name="manager" value="1" /> &nbsp;
                                                Fleet Mgr <input type="checkbox"  name="fleetManager" value="1" /> &nbsp;
                                            </td>
                                        </tr>

                                    </table>
                                    <br>
                        </div>
                                
                                    <center>
                                              <input type="button" class="btn btn-success" value="Generate TripSheet" id="save" name="Save" onclick="submitPage();" />
                                    </center>
                             
                            <script>
                                //                    $(".nexttab").click(function() {
                                //                        var selected = $("#tabs").tabs("option", "selected");
                                //                        $("#tabs").tabs("option", "selected", selected + 1);
                                //                    });
                                $('.btnNext').click(function() {
                                    $('.nav-tabs > .active').next('li').find('a').trigger('click');
                                });
                                $('.btnPrevious').click(function() {
                                    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                                });
                            </script>


                        </div>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>





