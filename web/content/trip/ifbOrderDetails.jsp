<%-- 
    Document   : ifbOrderDetails
    Created on : 17 May, 2021, 8:21:01 AM
    Author     : jibaw
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>



</head>
<script language="javascript" type="text/javascript">
    setFilterGrid("table");
    function orderStatus(orderId) {
        debugger;
        window.open('/throttle/handleViewOrderStatus.do?orderId=' + orderId + '&value=1', 'PopupPage', 'height = 400, width = 800, scrollbars = yes, resizable = yes');
    }
</script>
<script>
    function searchPage() {
        document.invoiceSearch.action = "/throttle/ifbOrderDetails.do";
        document.invoiceSearch.submit();
    }

    function submitPage1() {
        document.invoiceSearch.action = '/throttle/ifbOrderDetails.do?param=exportExcel';
        document.invoiceSearch.submit();
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.View Order List" text="View Order List"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.View Order List" text="View Order List"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="invoiceSearch" method="post">

                    <table class="table table-info mb30 table-hover" id="report" >
                        <thead>
                            <tr>
                                <th colspan="4" height="30" >IFB Orders List</th>
                            </tr>
                        </thead>
                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:250px;height:40px" autocomplete="off" value="<c:out value="${fromDate}"/>"></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" autocomplete="off" value="<c:out value="${toDate}"/>"></td>
                        </tr>


                        <tr>             
                            <td></td>
                            <td></td>
                            <td  colspan="4">
                                <input type="button" class="btn btn-success"   value="Search" onclick="searchPage()"/>
                                <input type="button" class="btn btn-success" name="ExportExcel" id="ExportExcel" onclick="submitPage1(this.name);" value="ExportExcel"/>
                            </td>
                        </tr>
                    </table>

                    <table colspan="4" class="table table-info mb30 table-hover" id="table" >
                        <thead>
                            <tr height="40" colspan="6">
                                <th>Sno</th>
                                <th >Docket No</th>
                                <th >Invoice No</th>
                                <th>Invoice Date</th>  
                                <th>Shipper Name</th>  
                                <th>Material No</th>
                                <th>Material Group</th>
                                <th>Origin Hub</th>
                                <th>Current Hub</th>
                                <th>Item Description</th>
                                <th>Quantity</th>
                                <th>City</th>
                                <th>Pincode</th>
                                <th>Party Name</th>
                                <th>Amount</th>
                                <th>Pincode</th>
                                <th>Invoice Status</th>
                                <th>Order Type</th>
                            </tr>
                        </thead>
                        <% int index = 0;
                                    int sno = 1;
                        %>
                        <tbody>
                            <c:forEach items="${ifbordersList}" var="order">

                                <tr height="30">
                                    <td align="left" ><%=sno%></td>
                                    <td align="left" ><c:out value="${order.lrNo}"/></td>
                                    <td align="left" ><c:out value="${order.invoiceNo}"/></td>
                                    <td align="left" ><c:out value="${order.invoiceDate}"/></td>                        
                                    <td align="left" >IFB</td>
                                    <td align="left" ><c:out value="${order.material}"/></td>
                                    <td align="left" ><c:out value="${order.matlGroup}"/></td>                        
                                    <td align="left" ><c:out value="${order.whName}"/></td>                        
                                    <td align="left" ><c:out value="${order.currentHub}"/></td>                        
                                    <td align="left" ><c:out value="${order.materialDescription}"/></td> 
                                    <td align="left" ><c:out value="${order.qty}"/></td> 
                                    <td align="left" ><c:out value="${order.city}"/></td> 
                                    <td align="left" ><c:out value="${order.pincode}"/></td> 
                                    <td align="left" ><c:out value="${order.dealerName}"/></td> 
                                    <td align="left" ><c:out value="${order.grossValue}"/></td> 
                                    <td align="left" ><c:out value="${order.pincode}"/></td> 
                                    <td align="left" >                     
                                        <c:if test="${order.invoiceStatus == 0}">
                                            Order Uploaded
                                        </c:if>
                                        <c:if test="${order.invoiceStatus == 1}">
                                            LR Updated
                                        </c:if>
                                        <c:if test="${order.invoiceStatus == 2}">
                                            In Trip
                                        </c:if>
                                        <c:if test="${order.invoiceStatus == 3}">
                                            Rejected
                                        </c:if>
                                        <c:if test="${order.invoiceStatus == 4}">
                                            Delivered
                                        </c:if>
                                        <c:if test="${order.invoiceStatus == 5}">
                                            Hub Out
                                        </c:if>
                                    </td>
                                    <td>
                                        <c:if test="${order.orderType==1}">
                                            Delivery
                                        </c:if>
                                        <c:if test="${order.orderType==2}">
                                            Exchange
                                        </c:if>
                                        <c:if test="${order.orderType==3}">
                                            Pickup
                                        </c:if>
                                    </td>
                                </tr>
                                <%index++;%>
                                <%sno++;%>
                            </c:forEach>
                        </tbody>
                    </table>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
