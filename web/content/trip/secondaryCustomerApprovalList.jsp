<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    function submitPage(value) {
        if (value == 'Proceed') {
            document.update.action = '/throttle/saveSecondaryCustomerApprovalMail.do';
            document.update.submit();
        }
    }
</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.SecondarycustomerApproval" text="SecondarycustomerApproval"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.SecondaryOperations" text="SecondaryOperations"/></a></li>
            <li class=""><spring:message code="hrms.label.SecondarycustomerApproval" text="SecondarycustomerApproval"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">    <body>
        <form name="update" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            <% int i = 1 ;%>
            <% int index = 0;%> 
            <%int oddEven = 0;%>
            <%  String classText = "";%>    
            <c:if test="${secondaryCustomerApprovalList != null}">
                <table class="table table-info mb30 table-hover">
                    <thead><tr>
                        <th >S No</th>
                        <th >Customer Code</th>
                        <th >Customer Name</th>
                        <th >Approval Mail Id</th>
                        </tr></thead>
                    <c:forEach items="${secondaryCustomerApprovalList}" var="mail">
                        <%
                      oddEven = index % 2;
                      if (oddEven > 0) {
                          classText = "text2";
                      } else {
                          classText = "text1";
                        }
                        %>
                        <tr>
                                <td  ><font color="green"><%=i++%></font></td>
                                <td  ><font color="green">
                                        <input type="hidden" name="customerId" id="customerId" value="<c:out value="${mail.customerId}"/>" />
                                        <input type="hidden" name="customerCode" id="customerCode" value="<c:out value="${mail.customerCode}"/>" /><c:out value="${mail.customerCode}"/></font></td>
                                <td  ><font color="green"><input type="hidden" name="customerName" id="customerName" value="<c:out value="${mail.customerName}"/>" /><c:out value="${mail.customerName}"/></font></td>
                                <td  ><font color="green"><input type="text" name="approvalMailId" id="approvalMailId" value="<c:out value="${mail.emailId}"/>" class="form-control" style="width:250px;height:40px" /></font></td>
                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
                <br>
                <center>
                    <input type="button" class="btn btn-success" value="Proceed" onclick="submitPage(this.value)"/>
                </center>
            </c:if>
        </form>
    </body>
</div>
</div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>