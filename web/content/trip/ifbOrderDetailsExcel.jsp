<%-- 
    Document   : ifbOrderDetailsExcel
    Created on : 17 May, 2021, 8:23:35 AM
    Author     : jibaw
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Excel";
        request.setAttribute("menuPath", menuPath);
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "IFBOrderDetails-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test = "${ifbordersList != null}" >
                <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                    <thead>
                    <tr height="40">
                    <th>Sno</th>
                    <th >Docket No</th>
                    <th >Invoice No</th>
                    <th>Invoice Date</th>  
                    <th>Shipper Name</th>  
                    <th>Material No</th>
                    <th>Material Group</th>
                    <th>Origin Hub</th>
                    <th>Current Hub</th>
                    <th>Item Description</th>
                    <th>Quantity</th>
                    <th>City</th>
                    <th>Pincode</th>
                    <th>Party Name</th>
                    <th>Amount</th>
                    <th>Pincode</th>
                    <th>Invoice Status</th>
                    <th>Order Type</th>
                        </tr>
                    </thead>
                    <% int index = 0;
                        int sno = 1;
                    %>
                    <tbody>
                        <c:forEach items="${ifbordersList}" var="order">

                        <td align="left" ><%=sno%></td>
                        <td align="left" ><c:out value="${order.lrNo}"/></td>
                        <td align="left" ><c:out value="${order.invoiceNo}"/></td>
                        <td align="left" ><c:out value="${order.invoiceDate}"/></td>                        
                        <td align="left" >IFB</td>
                        <td align="left" ><c:out value="${order.material}"/></td>
                        <td align="left" ><c:out value="${order.matlGroup}"/></td>                        
                        <td align="left" ><c:out value="${order.whName}"/></td>                        
                        <td align="left" ><c:out value="${order.currentHub}"/></td>                        
                        <td align="left" ><c:out value="${order.materialDescription}"/></td> 
                        <td align="left" ><c:out value="${order.qty}"/></td> 
                        <td align="left" ><c:out value="${order.city}"/></td> 
                        <td align="left" ><c:out value="${order.pincode}"/></td> 
                        <td align="left" ><c:out value="${order.dealerName}"/></td> 
                        <td align="left" ><c:out value="${order.grossValue}"/></td> 
                        <td align="left" ><c:out value="${order.pincode}"/></td> 
                        <td align="left" >                     
                                <c:if test="${order.invoiceStatus == 0}">
                                    Order Uploaded
                                </c:if>
                                <c:if test="${order.invoiceStatus == 1}">
                                    LR Updated
                                </c:if>
                                <c:if test="${order.invoiceStatus == 2}">
                                    In Trip
                                </c:if>
                                <c:if test="${order.invoiceStatus == 3}">
                                    Delivered
                                </c:if>
                                <c:if test="${order.invoiceStatus == 4}">
                                    Rejected
                                </c:if>
                                <c:if test="${order.invoiceStatus == 5}">
                                    Hub Out
                                </c:if>
                        </td>
                        <td>
                            <c:if test="${order.orderType==1}">
                                Delivery
                            </c:if>
                            <c:if test="${order.orderType==2}">
                                Exchange
                            </c:if>
                            <c:if test="${order.orderType==3}">
                                Pickup
                            </c:if>
                        </td>
                            </tr>
                            <%index++;%>
                            <%sno++;%>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>

            <br>
            <br>

        </form>
    </body>
</html>