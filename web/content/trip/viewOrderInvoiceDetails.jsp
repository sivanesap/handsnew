<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
    <%@page language="java" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%--<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>--%>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>
    <%@ page import="java.text.DecimalFormat" %>
    <%@ page import="java.text.NumberFormat" %>
    <%@page import="java.text.SimpleDateFormat" %>

    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCVpQCXyHbal5NLsAdEUP5OY8myKMaXefg&signed_in=true&libraries=places"></script>

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>
    <style>
        body {
            background-image: url("img_tree.png");
            background-repeat: no-repeat;
            background-position: right top;
            margin-right: 200px;
        }
    </style>
    
    <script type="text/javascript">
            function printPage(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "trip",
                        "width=600,height=250,top=400,left=550,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
                $("#done").hide();
                $("#cancel").hide();
            }
            function goBack() {
                document.trip.action = "/throttle/handleViewOrderInvoice.do";
                document.trip.method = "post";
                document.trip.submit();
            }


        </script>

    <script type="text/javascript">

        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });



    </script>
    <script  type="text/javascript" src="js/jq-ac-script.js"></script>
    <script type="text/javascript" language="javascript">

        $(document).ready(function() {
            $("#tabs").tabs();
        });
    </script>
</head>
<body>
    <form name="trip" method="post">
        <br>
        <div id="printInvoice">
        <table  align="center" cellpadding="0" cellspacing="0" border="2"  rules="all" frame="hsides" 
                                style="border-collapse: collapse;width:1000px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;height:auto;border-left: none;border-right: none;">     
            <tr>
                <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" colspan="2" >
                  <table width="100%" >
                       <center> 
                            <font size="3"><b><u>TAX INVOICE</u></b></font><br><br>
                            <font size="4"><b><c:out value="${warehouseName}" /></b></font><br><br>
                            <font size="3"> <c:out value="${billingAddress}" /> </font> <br>
                            <font size="3">TEL : <c:out value="${teleNo}" /></font> <br>
                            <font size="3">GST No : <c:out value="${gstNo}" /></font> <br>
                            <font size="3">State Name : <c:out value="${stateName}" />, Code : <c:out value="${companyStateCode}" /></font> <br>
                            <font size="3">Email : <c:out value="${emailId}" /></font> 
                       </center>      
                  </table>
                       <br></td> 
            </tr>
            <tr> 
                <td  style="width: 500px;">
                    <table width="100%">
                        <td  style="border-bottom-style: none;border-right-style: none;border-collapse: collapse;border-bottom-color:#000000;border-bottom-width: 1px;padding: 5px;">
                            <font size="3">&emsp;<b>Invoice No : &emsp;<c:out value="${invoiceCode}" /></b></font><br>   
                            <font size="3">&emsp;<b>Buyer's Order No : &emsp;<c:out value="${orderNo}" /></b></font>
                        </td>    
                    </table>    
                </td>
                <td  style="width: 500px;">
                    <table width="100%">
                        <td  style="border-bottom-style: none;border-right-style:none;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 5px;">
                            <font size="3">&emsp;<b>Invoice Dated : <c:out value="${invoiceDate}" /></b></font><br>
                            <font size="3">&emsp;<b>Batch Dated &nbsp;  : <c:out value="${batchDate}" /></b></font>
                    </table>
                </td>
            </tr>
            <tr> 
                <td  style="width: 500px;">
                    <table width="100%">
                        <td  style="border-bottom-style: none;border-right-style: none;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 5px;">
                            <font size="3">Consignee :</font><br>   
                            <font size="3">&emsp;<b><c:out value="${clientName}" /></b></font><br>
                            <font size="3">&emsp;<c:out value="${addressLine1}" /></font><br>
                            <font size="3">&emsp;<c:out value="${addressLine2}" /></font><br>
                            <font size="3">&emsp;<b>Husband Name</b> : <c:out value="${spouseName}" /></font><br>
                            <font size="3">&emsp;<b>Batch No</b> : <c:out value="${batchNumber}" /></font><br>
                            <font size="3">&emsp;<b>State Name</b> : <c:out value="${state}" /></font>
                        </td>    
                    </table>    
                </td>
                <td  style="width: 500px;">
                    <table width="100%">
                        <td  style="border-bottom-style: none;border-right-style:none;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 5px;">
                            <font size="3">Buyer (if Other than Consignee) :</font><br>   
                            <font size="3">&emsp;<b><c:out value="${clientName}" /> &emsp; <c:out value="${orderNo}" /></b></font><br>
                            <font size="3">&emsp;<c:out value="${addressLine1}" /></font><br>
                            <font size="3">&emsp;<c:out value="${addressLine2}" /></font><br>
                            <font size="3">&emsp;<b>Husband Name</b> : <c:out value="${spouseName}" /></font><br>
                            <font size="3">&emsp;<b>Batch No</b> : <c:out value="${batchNumber}" /></font><br>
                            <font size="3">&emsp;<b>State Name</b> : <c:out value="${state}" /></font>
                    </table>
                </td>
            </tr>
            <tr>
                <td  style="border-collapse: collapse; width: auto;" colspan="2">
                    <table   width="100%" border="1">
                        <tr>
                            <th  height="30" style="text-align: center;width: 50px;"><b>S.No</b></th>
                            <th  height="30" style="text-align: center;width: 350px;"><b>Description of Goods</b></th>
                            <th  height="30" style="text-align: center;width: 100px;"><b>HSN/SAC</b></th>
                            <th  height="30" style="text-align: center;width: 100px;"><b>GST %</b></th>    
                            <th  height="30" style="text-align: center;width: 100px;"><b>Qty</b></th>   
                            <th  height="30" style="text-align: center;width: 100px;"><b>Rate</b></th>
                            <th  height="30" style="text-align: center;width: 100px;"><b>per</b></th>
                            <th  height="30" style="text-align: center;width: 100px;"><b>Amount</b></th>
                        </tr>
                        <tr>
                            <td height="30" style="text-align:right">1</td>
                            <td height="30" style="text-align:center"><c:out value="${productName}"/></td>
                            <td height="30" style="text-align:center"><c:out value="${hsnCode}"/></td>
                            <td height="30" style="text-align:center"><c:out value="${cgstPercentage + sgstPercentage}"/>%</td>
                            <td height="30" style="text-align:center">1.0</td>
                            <td height="30" style="text-align:right"><c:out value="${invoiceValue}"/></td>
                            <td height="30" style="text-align:center">Nos</td>
                            <td height="30" style="text-align:right"><c:out value="${invoiceValue}"/></td>
                        </tr> 
                        <tr height="30">
                            <td></td>
                            <td style="text-align:right">OUTPUT CGST @ &nbsp;<c:out value="${cgstPercentage}"/>%</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right"><c:out value="${cgstValue}"/></td>
                        </tr> 
                        <tr height="30">
                            <td></td>
                            <td style="text-align:right">OUTPUT SGST @ &nbsp;<c:out value="${sgstPercentage}"/>%</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right"><c:out value="${sgstValue}"/></td>
                        </tr> 
                        <tr height="30">
                            <td></td>
                            <td style="text-align:center">Total</td>
                            <td></td>
                            <td></td>
                            <td style="text-align:center">1.0</td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right"><c:out value="${invoiceValue}"/></td>
                        </tr> 
                        <tr height="30">
                            <td colspan="7" style="text-align:right">Total&emsp;</td>
                            <td style="text-align:right"><c:out value="${grandTotal}"/></td>
                        </tr> 
                        <tr>
                            <c:set var="totalAmt" value="${grandTotal}" />
                            <td colspan="8" height="30" align="left"> <p><font size="3">(Rupees :
                                <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWords" >
                                    <% spareTotalRound.setRoundedValue(String.valueOf(pageContext.getAttribute("totalAmt")));%>
                                    <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                                    <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />&nbsp; Only</b>
                                </jsp:useBean>)</font></p>
                            </td>
                        </tr>
                        <tr>
                            <th  colspan="2" rowspan="2" height="30" style="text-align: center;"><b>HSN/SAC</b></th>
                            <th  height="30" rowspan="2" style="text-align: center;"><b>Taxable Value</b></th>
                            <th  colspan="2" height="30" style="text-align: center;"><b>Central Tax</b></th>
                            <th  colspan="2" height="30" style="text-align: center;"><b>State Tax</b></th>
                            <th  rowspan="2" height="30" style="text-align: center;"><b>Total Tax Amount</b></th>
                        </tr>
                        <tr>
                            <th  height="30" style="text-align: center;"><b>Rate</b></th>
                            <th  height="30" style="text-align: center;"><b>Amount</b></th>
                            <th  height="30" style="text-align: center;"><b>Rate</b></th>
                            <th  height="30" style="text-align: center;"><b>Amount</b></th>
                        </tr>
                        <tr>
                            <th  colspan="2" height="30" style="text-align: center;"><b><c:out value="${hsnCode}"/></b></th>
                            <th  height="30" style="text-align: right;"><b><c:out value="${invoiceValue}"/></b></th>
                            <th  height="30" style="text-align: right;"><b><c:out value="${cgstPercentage}"/>%</b></th>
                            <th  height="30" style="text-align: right;"><b><c:out value="${cgstValue}"/></b></th>
                            <th  height="30" style="text-align: right;"><b><c:out value="${sgstPercentage}"/></b></th>
                            <th  height="30" style="text-align: right;"><b><c:out value="${sgstValue}"/></b></th>
                            <th  height="30" style="text-align: right;"><b><c:out value="${cgstValue + sgstValue}"/></b></th>
                        </tr>
                        <tr>
                            <th  colspan="2" height="30" style="text-align: right;"><b>Total&emsp;</b></th>
                            <th  height="30" style="text-align: right;"><b><c:out value="${invoiceValue}"/></b></th>
                            <th  height="30" style="text-align: right;"><b></b></th>
                            <th  height="30" style="text-align: right;"><b><c:out value="${cgstValue}"/></b></th>
                            <th  height="30" style="text-align: right;"><b></b></th>
                            <th  height="30" style="text-align: right;"><b><c:out value="${sgstValue}"/></b></th>
                            <th  height="30" style="text-align: right;"><b></b></th>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" colspan="2" >
                    <table width="100%" >
                        <br><font size="3">&emsp;Remarks:</font><br>
                        <font size="3"><b>&emsp;SN : <c:out value="${serialNumber}" /></b></font><br><br>
                        <font size="3">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<b><u>DECLARATION</u></b></font> <br>
                        <font size="3">&emsp;We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct.</font> <br>
                        <font size="3">&emsp;Warranty on all goods is as per manufacturer's policy & all services to warranty shall be directly provided by manufacturer's authorized service centers.</font><br><br>
                    </table>
                </td> 
            </tr>
            <tr>
                <td colspan="8" height="30" style="padding-bottom: 50px;padding-top: 50px;"> <center><b>This is System Generated Bill And Does Not Require any Signature</b></center></td>
            </tr>
            <br>
        </table>
        <br>
        </div>
        <center>
            <input type="button" class="button" name="ok" value="Print" onClick="printPage('printInvoice');" >&nbsp;&nbsp;&nbsp;
            <input type="button" class="button" name="back" value="Back" onClick="goBack();" > &nbsp;
        </center>
        <br>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>