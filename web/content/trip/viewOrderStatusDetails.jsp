<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>



</head>
<script language="javascript" type="text/javascript">
    setFilterGrid("table");
    function orderStatus(orderId) {
        debugger;
        window.open('/throttle/handleViewOrderStatus.do?orderId=' + orderId + '&value=1', 'PopupPage', 'height = 400, width = 800, scrollbars = yes, resizable = yes');
    }
</script>
<script>
    function searchPage() {
        document.invoiceSearch.action = "/throttle/viewOrderDetails.do";
        document.invoiceSearch.submit();
    }

    function submitPage1() {
        document.invoiceSearch.action = '/throttle/viewOrderDetails.do?param=exportExcel';
        document.invoiceSearch.submit();
    }

</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.View Order List" text="View Order List"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.View Order List" text="View Order List"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
            <body>
        <div class="panel-body">
                <%
                            String menuPath = "Order List >> View / Edit";
                            request.setAttribute("menuPath", menuPath);
                %>
                <div class="contentpanel" style="overflow:auto">
                    <div class="row" align="center"style="margin:5px;">

                        <div class="col-sm-1 col-md-3">
                            <div class="panel panel-success panel-stat">
                                <div class="panel-heading">

                                    <div class="stat">
                                        <div class="row">
                                            <div class="col-xs-4">

                                                <i class="ion ion-android-bus"></i>
                                            </div>
                                            <div class="col-xs-8">

                                                <h4> <spring:message code="dashboard.label.Orders"  text="Orders"/></h4>

                                                <h1><span id="orderList"><c:out value="${orderListSize}"/></span></h1>
                                            </div>
                                        </div><!-- row -->

                                    </div>

                                </div
                            </div>
                        </div>

                    </div>
                </div>
        </div>

        <form name="invoiceSearch" method="post">

            <table class="table table-info mb30 table-hover" id="report" >
                <thead>
                    <tr>
                        <th colspan="4" height="30" >Orders List</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td><font color="red">*</font>From Date</td>
                    <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:250px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                    <td><font color="red">*</font>To Date</td>
                    <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>
                </tr>
                <c:if test="${RoleId==1023||RoleId==1016}">
                    <tr>
                        <td><font color="red">*</font>Warehouse Name</td>
                        <td height="30"><select name="whId" id="whId" class="form-control" style="width:250px;height:40px">
                                <option value="">------select---------</option>
                                <c:forEach items="${whList}" var="wh">
                                    <option value="<c:out value="${wh.warehouseId}"/>"><c:out value="${wh.warehouseName}"/></option>
                                </c:forEach>
                            </select></td>
                        <td><font color="red">*</font>Status</td>
                        <td height="30">
                            <select name="status1" id="status1" class="form-control" style="width:250px;height:40px" onchange="">
                                <option value="">All</option>
                                <option value="0">Order Created</option>
                                <option value="1">Scheduled</option>
                                <option value="2">Invoice Created</option>
                                <option value="3">OFD</option>
                                <option value="4">Delivered</option>
                                <option value="5">Returned</option>
                                <option value="7">Cancelled by 72N</option>
                                <option value="8">Cancelled by H&S</option>
                            </select>
                        </td>
                    </tr>
                    <script>
                        document.getElementById("whId").value ='<c:out value="${whId}"/>';
                        document.getElementById("status1").value ='<c:out value="${status1}"/>';
                    </script>
                </c:if>
                <tr>             
                    <td></td>
                    <td></td>
                    <td  colspan="2">
                        <input type="button" class="btn btn-success"   value="Search" onclick="searchPage()"/>
                        <input type="button" class="btn btn-success" name="ExportExcel" id="ExportExcel" onclick="submitPage1(this.name);" value="ExportExcel"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>

<c:if test = "${ordersList != null}" >
    <table class="table table-info mb30 table-hover" id="table" >
        <thead>
            <tr height="40">
                <th>Sno</th>
                <th width="20%">LoanProposalID</th>
                <th>Customer</th>                    
                <th>Batch Number</th>
                <th>Invoice Number</th>
                <th>Batch Date</th>
                <th>Branch</th>
                <th>Branch Id</th>
                <th>Member Offer Price</th>
                <th>Delivered Date</th>
                <th>Order Status</th>
                <th>Check</th>
                <th>View</th>
            </tr>
        </thead>
        <% int index = 0;
                    int sno = 1;
        %>
        <tbody>
            <c:forEach items="${ordersList}" var="order">

                <tr height="30">
                    <td align="left" ><%=sno%></td>
                    <td align="left" ><c:out value="${order.orderNo}"/></td>
                    <td align="left" ><c:out value="${order.clientName}"/></td>                        
                    <td align="left" ><c:out value="${order.batchNumber}"/></td>
                    <td align="left" ><c:out value="${order.invoiceNo}"/></td>
                    <td align="left" ><c:out value="${order.batchDate}"/></td>
                    <td align="left" ><c:out value="${order.branchName}"/></td>                        
                    <td align="left" ><c:out value="${order.branchId}"/></td>                        
                    <td align="left" ><c:out value="${order.memberOfferPrice}"/></td> 
                    <td align="left">
                        <c:if test="${order.invoiceStatus==4}">
                            <c:out value="${order.modifiedDate}"/>
                        </c:if>
                        <c:if test="${order.invoiceStatus!=4}">
                            NA
                        </c:if>
                    </td>
                    <td align="left" >                           
                        <c:if test="${order.cancel == 1}">
                            Order Canceled
                        </c:if>
                        <c:if test="${order.cancel != 1}">
                            <c:if test="${order.invoiceStatus == 0}">
                                Order Created
                            </c:if>
                            <c:if test="${order.invoiceStatus == 1}">
                                Scheduled
                            </c:if>
                            <c:if test="${order.invoiceStatus == 2}">
                                Invoice Created
                            </c:if>
                            <c:if test="${order.invoiceStatus == 3}">
                                OFD
                            </c:if>
                            <c:if test="${order.invoiceStatus == 4}">
                                Delivered
                            </c:if>
                            <c:if test="${order.invoiceStatus == 5}">
                                Returned
                            </c:if>
                            <c:if test="${order.invoiceStatus == 7}">
                                Cancelled by 72N
                            </c:if>
                            <c:if test="${order.invoiceStatus == 8}">
                                Cancelled by H&S
                            </c:if>
                            <c:if test="${order.invoiceStatus == 9}">
                                OFD
                            </c:if>
                        </c:if>
                        </a>
                    </td>  
                    <td>
                        <c:if test="${order.whStatus == 0}">
                            <font color="red"><b>Warehouse Invalid</b></font>
                            </c:if>
                            <c:if test="${order.whStatus != 0}">
                                <c:if test="${order.branchStatus == 0}">                                
                                <font color="red"><b>Branch Invalid</b></font>
                                </c:if>
                                <c:if test="${order.branchStatus != 0}">
                                    <c:if test="${order.itemStatus == 0}">
                                    <font color="red"><b>Product Invalid</b></font>
                                    </c:if>
                                    <c:if test="${order.itemStatus != 0}">
                                    -
                                </c:if>
                            </c:if>
                        </c:if>


                    </td>
                    <td align="left" >
                        <a href="#" style="color:#ff6699;"  onclick="orderStatus('<c:out value="${order.orderId}"/>')" ><span >View</span> </a></td> 
                </tr>
                <%index++;%>
                <%sno++;%>
            </c:forEach>
        </tbody>
    </table>
</c:if>

<script language="javascript" type="text/javascript">
    setFilterGrid("table");
</script>
</form>
</body>
</div>
</div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>