<%-- 
    Document   : fkOrderDetailsExcel
    Created on : 3 Jun, 2021, 9:55:09 PM
    Author     : Roger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Excel";
        request.setAttribute("menuPath", menuPath);
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "FkOrderDetails-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                <thead>
                    <tr height="40">
                        <th>Sno</th>
                        <th >Shipment Id</th>
                        <th >Ship Date</th>
                        <th >Shipment Type</th>
                        <th >Pay Type</th>
                        <th>Customer Name</th>    
                        <th>Delivery Hub</th>
                        <th>Order Type</th>
                        <th>City</th>
                        <th>Pincode</th>
                        <th>Product info</th>
                        <th>Amount</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <% int index = 0;
                    int sno = 1;
                %>
                <tbody>
                    <c:forEach items="${fkordersList}" var="order">
                    <td align="left" ><%=sno%></td>
                    <td align="left" ><c:out value="${order.shipmentId}"/></td>
                    <td align="left" ><c:out value="${order.billDate}"/></td>
                    <td align="left" >
                        <c:if test="${order.orderType==1}">Forward</c:if>
                        <c:if test="${order.orderType==2}">PREXO</c:if>
                        <c:if test="${order.orderType==3}">RVP</c:if>
                        </td>
                        <td align="left" ><c:out value="${order.shipmentType}"/></td>
                    <td align="left" ><c:out value="${order.customerName}"/></td> 
                    <td align="left" ><c:out value="${order.deliveryHub}"/></td>
                    <td align="left" ><c:out value="${order.orderType}"/></td>                        
                    <td align="left" ><c:out value="${order.city}"/></td>                        
                    <td align="left" ><c:out value="${order.pincode}"/></td>                        
                    <td align="left" ><c:out value="${order.itemName}"/></td>                        
                    <td align="left" ><c:out value="${order.amount}"/></td>                        

                    <td align="left" >                     
                        <c:if test="${order.invoiceStatus == 0}">
                            Order Uploaded
                        </c:if>
                        <c:if test="${order.invoiceStatus == 1}">
                            LR Updated
                        </c:if>
                        <c:if test="${order.invoiceStatus == 2}">
                            In Trip
                        </c:if>
                        <c:if test="${order.invoiceStatus == 3}">
                            Rejected
                        </c:if>
                        <c:if test="${order.invoiceStatus == 4}">
                            Delivered
                        </c:if>
                        <c:if test="${order.invoiceStatus == 5}">
                            Hub Out
                        </c:if>
                    </td>                          

                    </tr>
                    <%index++;%>
                    <%sno++;%>
                </c:forEach>
                </tbody>
            </table>

            <br>
            <br>

        </form>
    </body>
</html>
