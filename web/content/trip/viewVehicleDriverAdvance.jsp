<%@ include file="../common/NewDesign/header.jsp" %>
	<%@ include file="../common/NewDesign/sidemenu.jsp" %> 
	
<html>
    <head>
        <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $( "#datepicker" ).datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $( ".datepicker" ).datepicker({

            /*altField: "#alternate",
                        altFormat: "DD, d MM, yy"*/
            changeMonth: true,changeYear: true
        });

    });
</script>

    </head>
    <script language="javascript">
        
        function submitManualPage(){
            $("#mySubmit").hide();
            document.approve.action = '/throttle/saveVehicleDriverAdvanceApproval.do';
            document.approve.submit();
        }
        function setFocus(){
            document.approve.approveamt.focus();
        }
    </script>
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.DriverAdvance" text="Driver Advance"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Company" text="Driver Advance"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">


    <body onload="setFocus();">
        <form name="approve"  method="post" >
           
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
              <table class="table table-info mb30 table-hover" id="bg" >
		    <thead>
		<tr>
		    <th colspan="4" height="30" >Advance Approval</th>
		</tr>
               </thead>
           
                </tr>
                  <c:if test="${viewVehicleDriverAdvanceList != null}" >
                       <c:forEach items="${viewVehicleDriverAdvanceList}" var="trip">
                <tr>
                    <td  height="30" style="height:25px;border-bottom:1px solid;border-bottom-color: #f2f2f2;font-weight:normal;background:#f2f2f2;font-size:12px;">Trip Code</td>
                    <td  height="30"><c:out value="${trip.tripCode}"/></td>
                </tr>
                <tr>
                    <td  height="30">Customer Name</td>
                    <td  height="30"><c:out value="${trip.customerName}"/></td>
                </tr>
                
                <tr>
                    <td  height="30">CNotes</td>
                    <td  height="30"><c:out value="${trip.cNotes}"/></td>
                </tr>
                <tr>
                    <td  height="30">Route Name</td>
                    <td  height="30"><c:out value="${trip.routeName}"/></td>
                </tr>
                       </c:forEach>
                  </c:if>
                <tr>
                    <td  height="30">Driver Name</td>
                    <td  height="30">
                        <input name="vehicleDriverAdvanceId" id="vehicleDriverAdvanceId" type="hidden" class="form-control" style="width:250px;height:40px" value='<c:out value="${vehicleDriverAdvanceId}"/>' />
                        <input type="hidden" name="primaryDriver" id="primaryDriver" value="<c:out value="${primaryDriver}"/>"/><c:out value="${primaryDriver}"/>
                    </td>
                </tr>
                <tr>
                    <td  height="30">Vehicle No</td>
                    <td  height="30">
                        <input type="hidden" name="vehicleNo" id="vehicleNo" value="<c:out value="${vehicleNo}"/>"/><c:out value="${vehicleNo}"/>
                        <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>"/>
                    </td>
                </tr>
                <tr>
                    <td  height="30">Request On</td>
                    <td  height="30">
                        <input name="advanceDate" class="form-control" style="width:250px;height:40px" type="hidden" value='<c:out value="${advanceDate}"/>'/><c:out value="${advanceDate}"/></td>
                </tr>
                <tr>
                    <td  height="30">Expense Type</td>
                    <td  height="30">
                        <input name="expenseType" class="form-control" style="width:250px;height:40px" type="hidden" value='<c:out value="${expenseType}"/>'/><c:out value="${expenseType}"/></td>
                </tr>

                <tr>
                    <td  height="30"><font color="red">*</font>Req.Advance Amt.</td>
                    <td  height="30"><input name="advanceAmount" type="hidden" class="form-control" style="width:250px;height:40px" value='<c:out value="${advanceAmount}"/>'/><c:out value="${advanceAmount}"/></td>
                </tr>
                <tr>
                    <td  height="30"><font color="red">*</font>Status</td>
                    <td  height="30"><select class="form-control" style="width:240px;height:40px" name="approvestatus">
                            <option value="" >-Select Any One-</option>
                            <option value="2">Approved</option>
                            <option value="3">Rejected</option>
                </select></td>
                </tr>


                <tr>
                    <td  height="30"><font color="red">*</font>Approve Remarks</td>
                    <td  height="30"><textarea class="form-control" style="width:250px;height:40px" name="advanceRemarks"><c:out value="${advanceRemarks}"/></textarea></td>
                </tr>
            </table>
            
            <center>
                
                <input type="button" id="mySubmit" value="Submit" class="btn btn-success" onClick="submitManualPage();">
                
                &emsp;<input type="reset" class="btn btn-success" value="Clear">
            </center>



        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>
