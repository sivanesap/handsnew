<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>

    </head>
    <script language="javascript">
        function submitPage(){
            if(textValidation(document.approve.paidamt,'Paid Amount')){
                return;
            }

            document.approve.action = '/throttle/saveVehicleAdvancePay.do';
            document.approve.submit();
        }
        function setFocus(){
            document.approve.approveamt.focus();
        }
    </script>


    <body onload="setFocus();">
        
        <form name="approve"  method="post" >
           
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg" class="border">
                <tr align="center">
                    <td colspan="2" align="center" class="contenthead" height="30">
                        <div class="contenthead">Trip Payment</div></td>
                </tr>


                 <c:if test="${viewVehicleDriverAdvanceList != null}" >
                       <c:forEach items="${viewVehicleDriverAdvanceList}" var="trip">
                <tr>
                    <td class="text2" height="30" style="height:25px;border-bottom:1px solid;border-bottom-color: #f2f2f2;font-weight:normal;background:#f2f2f2;font-size:12px;">Trip Code</td>
                    <td class="text2" height="30"><c:out value="${trip.tripCode}"/></td>
                </tr>
                <tr>
                    <td class="text1" height="30">Customer Name</td>
                    <td class="text1" height="30"><c:out value="${trip.customerName}"/></td>
                </tr>
                
                <tr>
                    <td class="text2" height="30">CNotes</td>
                    <td class="text2" height="30"><c:out value="${trip.cNotes}"/></td>
                </tr>
                <tr>
                    <td class="text1" height="30">Route Name</td>
                    <td class="text1" height="30"><c:out value="${trip.routeName}"/></td>
                </tr>
                       </c:forEach>
                  </c:if>
                    <c:if test="${vehicleDriverAdvanceList != null}" >
                                      <c:forEach items="${vehicleDriverAdvanceList}" var="advance">
                <tr>
                    <td class="text1" height="30">Vehicle Type</td>
                    <td class="text1" height="30"><c:out value="${advance.vehicleTypeName}" /></td>
                </tr>
                <tr>
                    <td class="text2" height="30">Vehicle No</td>
                    <td class="text2" height="30"><c:out value="${advance.regNo}"/></td>
                </tr>
                <tr>
                    <td class="text1" height="30">Driver Name</td>
                    <td class="text1" height="30"> <c:out value="${advance.primaryDriverName}"/></td>
                </tr>
                <tr>
                    <td class="text2" height="30">Actual Advance Paid</td>
                    <td class="text2" height="30"> <c:out value="${advance.paidAdvance}"/></td>
                </tr>
                <tr>
                    <td class="text1" height="30">Expense Type</td>
                    <td class="text1" height="30"> <c:out value="${advance.expenseType}"/></td>
                </tr>

                <tr>
                    <td class="text1" height="30">Approved Amount</td>
                    <td class="text1" height="30"><c:out value="${advance.requestedAdvance}" /></td>
                </tr>
                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Paid Amt.</td>
                    <td class="text2" height="30"><input name="paidamt" id="paidamt" type="text" class="textbox" value="" onchange="checkamt()"></td>
                </tr>

                   <input name="vehicleDriverAdvanceId" id="vehicleDriverAdvanceId" type="hidden" class="textbox" value='<c:out value="${advance.vehicleDriverAdvanceId}"/>' />
                 <input name="requestedAmount" id="requestedAmount" type="hidden" class="textbox" value='<c:out value="${advance.requestedAdvance}"/>' />
                 <input type="hidden" name="vehicleNoEmail" value='<c:out value="${advance.regNo}"/>'/>
                     <input type="hidden" name="vehicletypeEmail" value='<c:out value="${advance.vehicleTypeName}"/>'/>
                     <input type="hidden" name="driverEmail" value='<c:out value="${advance.primaryDriverName}"/>'/>
                     <input type="hidden" name="approvedAmountEmail" value='<c:out value="${advance.requestedAdvance}"/>'/>
                     <input type="hidden" name="expenseTypeEmail" value='<c:out value="${advance.expenseType}"/>'/>
                     <input name="advanceRemarks" id="advanceRemarks" type="hidden" class="hidden" value='<c:out value="${advance.advanceRemarks}"/>' />
                <input name="usageTypeId" id="usageTypeId" type="hidden" class="textbox" value='<c:out value="${advance.usageTypeId}"/>' />
                   <input name="vehicleId" id="vehicleId" type="hidden" class="textbox" value='<c:out value="${advance.vehicleId}"/>' />
                                      </c:forEach></c:if>
            </table>
            <br>
            <center>
                <input type="button" value="save" class="button" onClick="submitPage();">
                &emsp;
            </center>


            <script>
                function checkamt(){
                    var reqvalue=document.getElementById("requestedAmount").value;                    
                    var paidamt=document.getElementById("paidamt").value;                    
                    var reqpnt = parseInt(reqvalue) * (10/100);
                    var allowvalue = parseInt(reqvalue)+parseInt(reqpnt);
                    
                    if( parseInt(paidamt) > parseInt(allowvalue)){
                        alert("You can pay only 10% max on the request amount.");
                        document.approve.paidamt.value=0;
                        document.getElementById("mySubmit").style.display = "none";
                        document.approve.advancerequestamt.focus();
                    }else{
                        document.getElementById("mySubmit").style.display = "block";
                    }

                }
            </script>
        </form>
    </body>
</html>
