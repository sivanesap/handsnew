<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

</head>
<script>

    function submitPage() {
        var confirms = "";
        if (document.getElementById('reMarks').value == '') {
            alert("please enter the Remarks")
            $("#reMarks").focus();
            return;
        }  else {
            confirms = confirm("Please Confirm");
            if (confirms == true) {
                $("#inv").hide();
                document.invoice.action = "/throttle/saveReconcile.do";
                document.invoice.submit();
            }
        }
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Reconcile" text="Reconcile"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.Reconcile" text="Reconcile"/></li>
        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <%        String menuPath = "Operation >> Reconcile";
                    request.setAttribute("menuPath", menuPath);
                %>
                <form name="invoice" method="post">

                     <c:if test = "${runsheetprintList != null}" >
        <table class="table table-info mb30 table-hover" id="table" >
            <thead>
                <tr height="40">
                    <th>Sno</th>
                    <th>Order No</th>
                    <th>Runsheet No / Document No</th>
                    <th>Address</th>
                    <th>Product</th>
                    <th>Qty</th>
                    <th>Delivery Person</th>
                </tr>
            </thead>
            <% int index = 0;
                        int sno = 1;
            %>
            <tbody>
                <c:forEach items="${runsheetprintList}" var="order">
                    <tr height="30">
                        <td align="left" ><%=sno%></td>
                        <td align="left" ><c:out value="${order.consignmentOrderNo}"/></td>
                        <td align="left" ><c:out value="${order.tripCode}"/>  <br> / (<c:out value="${order.documentNo}"/>)</td>
                        <td align="left" ><c:out value="${order.point1Name}"/></td>
                        <td align="left" ><c:out value="${order.articleName}"/></td>
                        <td align="left" ><c:out value="${order.noOfPackages}"/></td>
                        <td align="left" ><c:out value="${order.driverName}"/>
                            <input type="hidden"  value="<c:out value="${order.noOfPackages}"/>" class="text" name="totalPackages" id="totalPackages">
                            <input type="hidden"  value="<c:out value="${order.tripId}"/>" class="text" name="tripId" id="tripId">
                            <input type="hidden"  value="<c:out value="${order.vehicleNo}"/>" class="text" name="vehicleNo" id="vehicleNo">                        
                        </td>
                    </tr>
                    <%index++;%>
                    <%sno++;%>               
                         </c:forEach>
                        <tr><td>Remarks</td>
                            <td height="30"><textarea type="textarea" name="reMarks" id="reMarks" class="form-control" style="width:300px;height:50px"  value="" />
                            </textarea></td>
                           
                         <td>Expenses</td>
                        <td><input type="text" size="30" value="" class="text" name="expenseValue" id="expenseValue" style="width:250px;height:40px"></td> </tr>
            </tbody>
        </table>
    </c:if>
                    <center><input class="btn btn-success" id="inv" value="Reconcile" onclick="submitPage()"></center>
        </div>
    </div>
</form>
</body>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
