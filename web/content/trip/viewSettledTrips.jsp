<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>


<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.RunSheet" text="RunSheet"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
                <li class=""><spring:message code="hrms.label.RunSheet" text="RunSheet"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="sorter.size(20);
                        setValues();">
                    <form name="tripSheet" method="post">
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <%@include file="/content/common/message.jsp" %>
                        <table class="table table-info mb30 table-hover" id="table" >
                            <thead>
                                <tr height="40">
                                    <th>Sno</th>
                                    <th>Invoice No</th>
                                    <th>City</th>
                                    <th>Product</th>
                                    <th>Serial No</th>
                                    <th>Qty</th>
                                    <th>Delivery executive</th>
                                    <th>LR Number</th>
                                    <th>Shipment Type</th>
                                    <th>Status</th>
                                  </tr>
                            </thead>
                            <% int index = 0;
                               int ser=1;
                               int sno = 1;
                               int check = 0;
                            %>

                            <tbody>
                                <c:forEach items="${runsheetprintList}" var="order">
                                    <c:set var="startReportTime" value="${order.driverName}" />
                                    <tr height="30">
                                        <td align="left" ><%=sno%></td>
                                        <td align="left" ><c:out value="${order.consignmentOrderNo}"/></td>
                                        <td align="left" ><c:out value="${order.point1Name}"/></td>
                                        <td align="left" ><c:out value="${order.articleName}"/></td>
                                        <td align="left" ><c:out value="${order.serialNo}"/></td>
                                        <td align="left" ><c:out value="${order.noOfPackages}"/></td>
                                        <td align="left" ><c:out value="${order.driverName}"/></td>
                                        <td align="left" ><c:out value="${order.lrNumber}"/></td>
                                        <td align="left" >
                                            <c:if test="${order.orderType==1}">Delivery</c:if>
                                            <c:if test="${order.orderType==2}">Exchange</c:if>
                                            <c:if test="${order.orderType==3}">PickUp</c:if>
                                            </td>
                                   
                                <td align="left" >                                                        
                                    <c:if test="${order.invoiceStatus == 3}">
                                        UnDelivered
                                    </c:if>
                                    <c:if test="${order.invoiceStatus == 4}">
                                        Delivered
                                    </c:if>
                                    <c:if test="${order.invoiceStatus !=4&&order.invoiceStatus !=3}">
                                        Not Attempted
                                        <input type="hidden" id="serNo<%=ser%>" name="serNo" value="<c:out value="${order.serialNo}"/>">
                                        <% ser++; %>
                                    </c:if>

                                </td>
                                </tr>
                                <%index++;%>
                                <%sno++;%>               
                            </c:forEach>
                            <input type="hidden" id="ser" name="ser" value="<%=ser%>">
                            <input type="hidden" id="ind" name="ind" value="<%=index%>">
                            <input type="hidden" id="tripSheetId" name="tripSheetId" value="<c:out value="${tripSheetId}"/>">
                            <input type="hidden" id="vehicleId" name="vehicleId" value="<c:out value="${vehicleId}"/>">
                            <input type="hidden" id="orderType" name="orderType" value="<c:out value="${orderType}"/>">
                            <input type="hidden" id="tripType" name="tripType" value="<c:out value="${tripType}"/>">
                            <input type="hidden" id="statusId" name="statusId" value="<c:out value="${statusId}"/>">
                          
                            
                            <script>
                                function checkAll(element) {
                                    var checkboxes = document.getElementsByName('selectedIndex');
                                    if (element.checked == true) {
                                        for (var x = 0; x < checkboxes.length; x++) {
                                            checkboxes[x].checked = true;
                                            $('#selectedStatus' + (x + 1)).val('1');
                                        }
                                    } else {
                                        for (var x = 0; x < checkboxes.length; x++) {
                                            checkboxes[x].checked = false;
                                            $('#selectedStatus' + (x + 1)).val('0');
                                        }
                                    }
                                }
                                function checkSelectStatus(sno, obj) {

                                    if (obj.checked == true) {
                                        document.getElementById("selectedStatus" + sno).value = 1;
                                    } else if (obj.checked == false) {
                                        document.getElementById("selectedStatus" + sno).value = 0;
                                    }
                                }
                            </script>
                            </tbody>
                        </table>
                            
                            <center>
                                                                <input type="button"   value="ExportExcel" class="btn btn-success" name="Export Excel" onclick="submitPage1();" >

                            </center>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                            
                            function submitPage1() {
                                var tripSheetId = document.getElementById("tripSheetId").value;
                                var vehicleId = document.getElementById("vehicleId").value;
                                var orderType = document.getElementById("orderType").value;
                                var tripType = document.getElementById("tripType").value;
                                var statusId = document.getElementById("statusId").value;
            document.tripSheet.action = '/throttle/viewIfbTripExpense.do?tripSheetId='+tripSheetId+'&vehicleId='+vehicleId+'&tripType='+tripType+'&statusId='+statusId+'&admin=No&orderType='+orderType+'&param=exportExcel';
            document.tripSheet.submit();
        }
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>