<%-- 
    Document   : deliveryRequestUpdate
    Created on : Dec 11, 2020, 7:36:59 PM
    Author     : ADMIN
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

</head>
<script>

    function submitPage() {
            document.deliveryRequest.action = "/throttle/updateDeliveryStatus.do";
            document.deliveryRequest.submit();           
    }    
    

 var httpRequest;
    function checkserialNo() {
        var serialNO = document.getElementById('serialNo').value;

            var url = '/throttle/checkSerialNo.do?serialNO='+serialNO;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);

    }


    function processRequest() {
        
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#productCategoryNameStatus").text('Serial No: ' + val+' is Already Exists');
                     var x = document.getElementById('serialNo'); // added quotes around the id
                      x.value = "";
                      $("#invBlock").hide();
                } else {
                    $("#nameStatus").hide();
                    $("#productCategoryNameStatus").text('');
                    $("#invBlock").show();
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
           
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Generate Invoice" text="Delivery Request"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.OUTBOUND" text="OUTBOUND"/></a></li>
            <li class=""><spring:message code="hrms.label.Delivery Request" text="Delivery Request"/></li>
        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <%        String menuPath = "Operation >>Delivery Request";
                    request.setAttribute("menuPath", menuPath);
                %>
                <form name="deliveryRequest" method="post">

                    <table class="table table-info mb30 table-hover" id="report" >
                        <thead>
                            <tr>
                                <th colspan="4" height="30" >Delivery Request</th>
                        <input type="hidden" name="orderId" id="orderId" value="<c:out value="${orderId}"/>" />
                        </tr>
                        </thead>
                        <tr>
                    <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="productCategoryNameStatus" style="color: red"></label></td>
                </tr>
                        <tr>
                            <td>Customer</td>
                            <td height="30"><c:out value="${customerName}"/></td>
                            <td>LoanProposalId</td>
                            <td height="30"><c:out value="${loanProposalId}"/></td>
                        </tr>
                        <tr>
                            <td>Order Date</td>
                            <td height="30"><c:out value="${orderDate}"/></td>
                            <td>Branch</td>
                            <td height="30"><c:out value="${branch}"/></td>                            
                        </tr>
                        <tr>
                            <td>Product</td>
                            <td height="30"><c:out value="${productId}"/></td>
                            <td>Model</td>
                            <td height="30"><c:out value="${model}"/></td>                            
                        </tr>
                        <tr>
                            <td>SerialNo</td>
                            <td height="30"><c:out value="${serialNumber}"/></td>
                            <td>Qty</td>
                            <td height="30"><c:out value="${qty}"/></td>                            
                        </tr>                       
                                                
                         <input name="delRequestId" id="delRequestId" type="hidden"  value="<c:out value="${delRequestId}"/>">
                         <input name="loanProposalId" id="loanProposalId" type="hidden"  value="<c:out value="${loanProposalId}"/>">
                         <input name="itemId" id="itemId" type="hidden"  value="<c:out value="${itemId}"/>">
                         <input name="createdDate" id="createdDate" type="hidden"  value="<c:out value="${createdDate}"/>">
                         <input name="productId" id="productId" type="hidden"  value="<c:out value="${productId}"/>">
                         <input name="serialNumber" id="serialNumber" type="hidden"  value="<c:out value="${serialNumber}"/>">
                         <input name="orderId" id="orderId" type="hidden"  value="<c:out value="${orderId}"/>">
                         <input name="qty" id="qty" type="hidden"  value="<c:out value="${qty}"/>">
                         <input name="customerName" id="customerName" type="hidden"  value="<c:out value="${customerName}"/>">
                         <input name="model" id="model" type="hidden"  value="<c:out value="${model}"/>">
                         <input name="warehouseId" id="warehouseId" type="hidden"  value="<c:out value="${warehouseId}"/>">
                    </table>
                        <center><div id="invBlock"><input class="btn btn-success" id="inv" value="Confirm" onclick="submitPage()"></div></center>
        </div>
    </div>
</form>
</body>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>

