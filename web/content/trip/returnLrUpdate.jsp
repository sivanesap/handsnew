<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%@page import="java.text.SimpleDateFormat" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script>

</script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String todayDate = sdf.format(today);
%>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });</script>

<script type="text/javascript" language="javascript">

    function submitPageDone() {
        var selectedStatus=document.getElementsByName("selectedStatus");
        var count=0;
        for(var i=0;i<selectedStatus.length;i++){
            if(selectedStatus[i].value==1){
                count++;
            }
        }
        if(count>1){
                document.tripExpense.action = '/throttle/saveReturnLrUpdate.do';
                document.tripExpense.submit();
            }else{
                alert("Please choose atleast one order");
            }
    }
    function closeTrip() {

        document.tripExpense.action = '/throttle/closeTrip.do';
        document.tripExpense.submit();
    }


</script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Return LR Update" text="Return LR Update"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.Runsheet Closure" text="Return LR Update "/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body"> 

            <body onload="">
                <form name="tripExpense" method="post">
                    <br> 
                    <%@include file="/content/common/message.jsp" %>
                    <div id="tabs" >
                        <ul>
                            <li><a href="#tripDetail" style="width: auto"><span>Return LR Update</span></a></li>
                            <!--<li><a href="#routeDetail"><span>Runsheet Plan </span></a></li>-->                            
                           
                        </ul>

                        <div id="tripDetail">
                           

                            

                                <c:if test = "${runsheetprintList != null}" >
                                    <table class="table table-info mb30 table-hover" id="table" >
                                        <thead>
                                            <tr height="40">
                                                <th>Sno</th>
                                                <th>Order No</th>
                                                <th>City</th>
                                                <th>Product</th>
                                                <th>Serial No</th>
                                                <th>Qty</th>
                                                <th>Shipment Type</th>
                                                <th>Exchange Serial No</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th align="center">Select All <input type="checkbox" class="" name="all" id="all" onclick="checkAll(this)"></th>
                                            </tr>
                                        </thead>
                                        <% int index = 0;
                                           int ser=1;
                                           int sno = 1;
                                           int check = 0;
                                        %>

                                        <tbody>
                                            <c:forEach items="${runsheetprintList}" var="order">
                                                <c:set var="startReportTime" value="${order.driverName}" />
                                                <tr height="30">
                                                    <td align="left" ><%=sno%></td>
                                                    <td align="left" ><c:out value="${order.consignmentOrderNo}"/></td>
                                                    <td align="left" ><c:out value="${order.point1Name}"/></td>
                                                    <td align="left" ><c:out value="${order.articleName}"/></td>
                                                    <td align="left" ><c:out value="${order.serialNo}"/></td>
                                                    <td align="left" ><c:out value="${order.noOfPackages}"/></td>
                                                    <td align="left" >
                                                        <c:if test="${order.orderType==1}">Delivery</c:if>
                                                        <c:if test="${order.orderType==2}">Exchange</c:if>
                                                        <c:if test="${order.orderType==3}">PickUp</c:if>
                      
                                                        <input type="hidden"  value="<c:out value="${order.noOfPackages}"/>" class="text" name="totalPackages" id="totalPackages">
                                                        <input type="hidden"  value="<c:out value="${order.tripId}"/>" class="text" name="tripId" id="tripId">
                                                        <input type="hidden"  value="<c:out value="${order.vehicleNo}"/>" class="text" name="vehicleNo" id="vehicleNo">                          
                                                    </td>
                                                    <td>
                                                        <c:if test="${order.orderType!=1}">
                                                            <input type="text" name="returnSerial" class="form-control" id="returnSerial<%=sno%>" style="width:200px;height:35px"/>
                                                        </c:if>
                                                        <c:if test="${order.orderType==1}">
                                                            <input type="text" class="form-control" name="returnSerial" id="returnSerial<%=sno%>" style="width:200px;height:35px" readonly/>
                                                        </c:if>
                                                    </td>
                                                    <td>
                                                        <c:if test="${order.orderType!=1}">
                                                            <input type="text" name="returnDescription" class="form-control" id="returnDescription<%=sno%>" style="width:200px;height:35px"/>
                                                        </c:if>
                                                        <c:if test="${order.orderType==1}">
                                                            <input type="text" class="form-control" name="returnDescription" id="returnDescription<%=sno%>" style="width:200px;height:35px" readonly/>
                                                        </c:if>
                                                    </td>
                                                    <td align="left" >                                                        
                                                        <c:if test="${order.invoiceStatus == 3}">
                                                            UnDelivered
                                                        </c:if>
                                                        <c:if test="${order.invoiceStatus == 4}">
                                                            Delivered
                                                        </c:if>
                                                        <c:if test="${order.invoiceStatus !=4}">

                                                            <input type="hidden" id="serNo<%=ser%>" name="serNo" value="<c:out value="${order.serialNo}"/>">
                                                            <% ser++; %>
                                                        </c:if>

                                                    </td>

                                            <input type="hidden"  value="<c:out value="${order.orderType}"/>" class="text" name="orderType" id="orderType<%=sno%>">                        
                                            <input type="hidden" name="selectedStatus" id="selectedStatus<%=sno%>" value="0" />
                                            <input type="hidden" name="orderId" id="orderId<%=index%>"  value="<c:out value="${order.orderId}"/>"  readOnly/>
                                            <input type="hidden" name="invoiceStatus" id="invoiceStatus<%=index%>"  value="<c:out value="${order.invoiceStatus}"/>"  readOnly/>

                                            <td><input type="checkbox" style="color:dark" id="selectedIndex<%=index%>" name="selectedIndex"  onclick="checkSelectStatus(<%=sno%>, this);" value="<c:out value="${order.serialNo}"/>"></td>
                                            </tr>
                                            <%index++;%>
                                            <%sno++;%>               
                                        </c:forEach>
                                        <input type="hidden" id="ser" name="ser" value="<%=ser%>">
                                        <input type="hidden" id="ind" name="ind" value="<%=index%>">
                                        <script>
                                            function checkAll(element) {
                                                var checkboxes = document.getElementsByName('selectedIndex');
                                                if (element.checked == true) {
                                                    for (var x = 0; x < checkboxes.length; x++) {
                                                        checkboxes[x].checked = true;
                                                        $('#selectedStatus' + (x + 1)).val('1');
                                                    }
                                                } else {
                                                    for (var x = 0; x < checkboxes.length; x++) {
                                                        checkboxes[x].checked = false;
                                                        $('#selectedStatus' + (x + 1)).val('0');
                                                    }
                                                }
                                            }
                                            function checkSelectStatus(sno, obj) {

                                                if (obj.checked == true) {
                                                    document.getElementById("selectedStatus" + sno).value = 1;
                                                } else if (obj.checked == false) {
                                                    document.getElementById("selectedStatus" + sno).value = 0;
                                                }
                                            }
                                        </script>
                                        </tbody>
                                    </table>
                                    <center>
                                        <input type="button" value="Update" class="btn btn-success" onclick="submitPageDone();"/>
                                        <% 
                                           if(check==1){
                                        %>
                                        <%}else{%>
                                        &emsp;
                                        <!--<input type="button" value="Runsheet Close" class="btn btn-success" onclick="closeTrip();"/>-->                                
                                        <%}%>
                                    </center>
                                </c:if>


                        </div>
          

                        




                        <script>
                            $(".nexttab").click(function() {
                                var selected = $("#tabs").tabs("option", "selected");
                                $("#tabs").tabs("option", "selected", selected + 1);
                            });
                        </script>

                    </div>
                </form>
            </body>
            <div>
                <div>
                    <div>
                        <%@ include file="../common/NewDesign/settings.jsp" %>