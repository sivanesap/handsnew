<%-- 
    Document   : resendMail
    Created on : Jun 09, 2014, 11:44:29 PM
    Author     : Throttle
--%>
<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery.tokeninput.js"></script>

    <link rel="stylesheet" href="/throttle/css/token-input.css" type="text/css" />
    <link rel="stylesheet" href="/throttle/css/token-input-facebook.css" type="text/css" />

    <script>
        function submitPage() {
//            alert(document.getElementById('mailTO').value);
            if (document.getElementById('mailTO').value == '') {
                alert("Please select to mail Id");
            } else {
                document.mailConfig.action = '/throttle/reSendMailDetails.do';
                document.mailConfig.submit();
            }
        }
    </script>    
</head>

<body>
    <form name="mailConfig">
        <c:if test="${mailNotDeliveredList != null}">
            <c:forEach items="${mailNotDeliveredList}" var="val">
                <table align="center" width="800px" class="table">
                    <tr>
                        <!--<h2 id="prevent-duplicates">Trip Mail</h2>-->
                        <td class="text1">TO</td>
                        <td class="text1">
                            <div>
                                <input type="text" id="mailTO" name="mailTO" value="<c:out value="${val.mailIdTo}"/>" />
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $("#mailTO").tokenInput("/throttle/getTOEmailList.do", {
                                            preventDuplicates: true,
                                            allowCustomEntry: true,
                                            prePopulate: [
                                                {id: '<c:out value="${val.mailIdTo}"/>', name: '<c:out value="${val.mailIdTo}"/>'}
                                            ]
                                        });
                                    });
                                </script>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text1">CC</td>
                        <td class="text1">
                            <div>
                                <input type="text" id="mailCC" name="mailCC"/>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $("#mailCC").tokenInput("/throttle/getCCEmailList.do", {
                                             preventDuplicates: true,
                                             allowCustomEntry: true,
                                             prePopulate: [
                                                {id: '<c:out value="${val.mailIdCc}"/>', name: '<c:out value="${val.mailIdCc}"/>'}
                                            ]
                                        });
                                    });
                                </script>
                            </div>
                        </td>    
                    </tr>
                    <tr>
                        <td class="text2">Subject</td>
                        <td class="text2">
                            <textarea id="subject" name="subject" style="width: auto"><c:out value="${val.mailSubjectTo}"/></textarea>
                            <input type="hidden" id="mailContentTo" name="mailContentTo" value="<c:out value="${val.mailContentTo}"/>" />
                        </td>
                        <td class="text2" colspan="2" ><img src="images/mailSendingImage.jpg" alt="Send Mail"   title="Sen Mail" style="width: 30px;height: 30px" onclick="submitPage()"/>
                            <br>
                            Send Mail</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                    <div style="display:table;">
                        <c:out value="${val.mailContentTo}" escapeXml="false"/>
                    </div>
                    </td>
                    </tr>
                    

                </table>
            </c:forEach>
        </c:if>
        <c:if test="${mailNotDeliveredList == null}">
            <h2 id="prevent-duplicates">No Records Found</h2>
            <script>
                window.close();
            </script>    
        </c:if>
    </form>
</body>
</html>