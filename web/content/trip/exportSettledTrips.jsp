<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
      <%
                String menuPath = "Finance >> Daily Advance Advice";
                request.setAttribute("menuPath", menuPath);
                String dateval = request.getParameter("dateval");
                String active = request.getParameter("active");
                String type = request.getParameter("type");
    %>
     
    <body>

        <form name="tripSheet" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "TripSheetDetails-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>
        
      

            <br>
            <br>
            <br>

<br><br>
            
            <br>
            <br>
            <table class="table table-info mb30 table-hover" id="table" >
                            <thead>
                                <tr height="40">
                                    <th>Sno</th>
                                    <th>Invoice No</th>
                                    <th>City</th>
                                    <th>Product</th>
                                    <th>Serial No</th>
                                    <th>Qty</th>
                                    <th>Delivery executive</th>
                                    <th>LR Number</th>
                                    <th>Shipment Type</th>
                                    <th>Status</th>
                                  </tr>
                            </thead>
                            <% int index = 0;
                               int ser=1;
                               int sno = 1;
                               int check = 0;
                            %>

                            <tbody>
                                <c:forEach items="${runsheetprintList}" var="order">
                                    <c:set var="startReportTime" value="${order.driverName}" />
                                    <tr height="30">
                                        <td align="left" ><%=sno%></td>
                                        <td align="left" ><c:out value="${order.consignmentOrderNo}"/></td>
                                        <td align="left" ><c:out value="${order.point1Name}"/></td>
                                        <td align="left" ><c:out value="${order.articleName}"/></td>
                                        <td align="left" ><c:out value="${order.serialNo}"/></td>
                                        <td align="left" ><c:out value="${order.noOfPackages}"/></td>
                                        <td align="left" ><c:out value="${order.driverName}"/></td>
                                        <td align="left" ><c:out value="${order.lrNumber}"/></td>
                                        <td align="left" >
                                            <c:if test="${order.orderType==1}">Delivery</c:if>
                                            <c:if test="${order.orderType==2}">Exchange</c:if>
                                            <c:if test="${order.orderType==3}">PickUp</c:if>
                                            </td>
                                   
                                <td align="left" >                                                        
                                    <c:if test="${order.invoiceStatus == 3}">
                                        UnDelivered
                                    </c:if>
                                    <c:if test="${order.invoiceStatus == 4}">
                                        Delivered
                                    </c:if>
                                    <c:if test="${order.invoiceStatus !=4&&order.invoiceStatus !=3}">
                                        Not Attempted
                                        <input type="hidden" id="serNo<%=ser%>" name="serNo" value="<c:out value="${order.serialNo}"/>">
                                        <% ser++; %>
                                    </c:if>

                                </td>
                                </tr>
                                <%index++;%>
                                <%sno++;%>               
                            </c:forEach>
                            <input type="hidden" id="ser" name="ser" value="<%=ser%>">
                            <input type="hidden" id="ind" name="ind" value="<%=index%>">
                            <script>
                                function checkAll(element) {
                                    var checkboxes = document.getElementsByName('selectedIndex');
                                    if (element.checked == true) {
                                        for (var x = 0; x < checkboxes.length; x++) {
                                            checkboxes[x].checked = true;
                                            $('#selectedStatus' + (x + 1)).val('1');
                                        }
                                    } else {
                                        for (var x = 0; x < checkboxes.length; x++) {
                                            checkboxes[x].checked = false;
                                            $('#selectedStatus' + (x + 1)).val('0');
                                        }
                                    }
                                }
                                function checkSelectStatus(sno, obj) {

                                    if (obj.checked == true) {
                                        document.getElementById("selectedStatus" + sno).value = 1;
                                    } else if (obj.checked == false) {
                                        document.getElementById("selectedStatus" + sno).value = 0;
                                    }
                                }
                            </script>
                            </tbody>
                        </table>
        </form>
    </body>
</html>