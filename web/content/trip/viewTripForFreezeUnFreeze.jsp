<%@ include file="../common/NewDesign/header.jsp" %>
	<%@ include file="../common/NewDesign/sidemenu.jsp" %> 

<%@page import="java.text.SimpleDateFormat"%>
<html>
    <head>
        <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
<!--        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />-->

<!--        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>-->

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>






        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
        function computeExpense(){

            var distance = document.trip.preStartLocationDistance.value;
            var vehicleMileage = document.trip.vehicleMileage.value;
            var tollRate = document.trip.tollRate.value;
            var fuelPrice = document.trip.fuelPrice.value;

            if(distance.trim() != '' && distance.trim() != '0'){
                var fuelLtrs = parseFloat(distance) / parseFloat(vehicleMileage);
                var fuelCost = (parseFloat(fuelPrice) * fuelLtrs);
                var tollCost = parseFloat(distance) * parseFloat(tollRate);
                var totalCost = (fuelCost + tollCost).toFixed(2);
                document.trip.preStartRouteExpense.value = totalCost;
                document.trip.preStartLocationDurationHrs.value = 0;
                document.trip.preStartLocationDurationMins.value = 0;
            }

        }

                var httpReq;
                var temp = "";




            function fetchRouteInfo() {
                var preStartLocationId = document.trip.preStartLocationId.value;
                var originId = document.trip.originId.value;
                var vehicleTypeId = document.trip.vehicleTypeId.value;
                if(preStartLocationId != ''){
                     var url = "/throttle/checkPreStartRoute.do?preStartLocationId="+preStartLocationId+"&originId="+originId+"&vehicleTypeId="+vehicleTypeId;

                    if (window.ActiveXObject)
                    {
                        httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                    {
                        httpReq = new XMLHttpRequest();
                    }
                    httpReq.open("GET", url, true);
                    httpReq.onreadystatechange = function() {
                        processFetchRouteCheck();
                    };
                    httpReq.send(null);
                }

            }

            function processFetchRouteCheck()
            {
                if (httpReq.readyState == 4)
                {
                    if (httpReq.status == 200)
                    {
                        temp = httpReq.responseText.valueOf();
                        //alert(temp);
                        if(temp != '' && temp != null  && temp != 'null'){
                            var tempVal = temp.split('-');
                            document.trip.preStartLocationDistance.value = tempVal[0];
                            document.trip.preStartLocationDurationHrs.value = tempVal[1];
                            document.trip.preStartLocationDurationMins.value = tempVal[2];
                            document.trip.preStartRouteExpense.value = tempVal[3];
                            document.getElementById("preStartLocationDistance").readOnly = true;
                            document.getElementById("preStartLocationDurationHrs").readOnly = true;
                            document.getElementById("preStartLocationDurationMins").readOnly = true;
                            document.getElementById("preStartRouteExpense").readOnly = true;
                        }else{
                            alert('valid route does not exists between pre start location and trip start location');
                            document.getElementById("preStartLocationDistance").readOnly = false;
                            document.getElementById("preStartLocationDurationHrs").readOnly = false;
                            document.getElementById("preStartLocationDurationMins").readOnly = false;
                            document.getElementById("preStartRouteExpense").readOnly = false;
                        }
//                        if(tempVal[0] == 0){
//                            alert("no match found");
//                        }
                    }
                    else
                    {
                        alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                    }
                }
            }
            function processPreStartCheckBox(){

                var tempVal = document.getElementById("preStartLocationCheckbox").checked;
                if(tempVal){
                    document.trip.preStartLocationCheckbox.value=1;
                    $("#preStartDetailDiv").hide();
                }else{
                    document.trip.preStartLocationCheckbox.value=0;
                    $("#preStartDetailDiv").show();
                }
            }
            function setButtons(){
                var temp = document.trip.actionName.value;
                if(temp != '0'){
                    if(temp == '1'){
                        $("#actionDiv").show();
//                        $("#preStartDiv").show();
                    }else{
//                        $("#preStartDiv").hide();

                    }
                }else {
//                    $("#preStartDiv").hide();
                }
            }

            //start ajax for pre location Nos
                $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#preStartLocation').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getCityFromName.do",
                            dataType: "json",
                            data: {
                                cityFrom: request.term,
                                textBox: 1
                            },
                            success: function(data, textStatus, jqXHR) {
//                                alert(data);
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                //console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#preStartLocationId').val(tmp[0]);
                        $('#preStartLocation').val(tmp[1]);
                        return false;
                    }
                    // Format the list menu output of the autocomplete
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    //alert(item);
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

            });
            //end ajax for pre location
            function saveAction() {
//                alert("test");
        
        if (document.trip.actionRemarks.value == '') {
                alert('please enter Remarks');
                document.getElementById("actionRemarks").focus();
                return;
            }
                var statusCheck = true;
                var temp = document.trip.actionName.value;
                //alert(temp);
                if(temp != '0'){
                    if(temp == '1'){ //freeze selected
//                        var tempVal = document.getElementById("preStartLocationCheckbox").checked;
                        //alert(tempVal);
//                        if(!tempVal){//same checkbox is not selected
//                            //check for distance and route expense;
//                            var distance = document.trip.preStartLocationDistance.value;
//                            //alert(distance);
//                            if(distance == '' || distance == '0'){
//                                alert('please enter pre start distance');
//                                document.trip.preStartLocationDistance.focus();
//                                statusCheck = false;
//                            }
//                        }
                    statusCheck = true;
                    }
                }else if (temp == 0){
                    alert('please select your action');
                    statusCheck = false;
                }
//                 if(statusCheck && !tempVal && temp == '1'){
//                    if(document.trip.preStartLocationPlanDate.value == ''){
//                        alert('please enter pre start plan date');
//                        document.trip.preStartLocationPlanDate.focus();
//                        statusCheck = false;
//                    }
//                }

                if(statusCheck){
                    document.trip.action = "/throttle/saveTripUpdate.do?";
                    document.trip.submit();
                }
            }
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });



    </script>
       <script type="text/javascript">
        function calculateDate(){
        var endDates = document.getElementById("tripScheduleDate").value;
        var tempDate1 = endDates.split("-");
        var stDates = document.getElementById("preStartLocationPlanDate").value;
        var tempDate2 = stDates.split("-");
        var prevTime = new Date(tempDate2[2],tempDate2[1],tempDate2[0]);  // Feb 1, 2011
        var thisTime = new Date(tempDate1[2],tempDate1[1],tempDate1[0]);              // now
        var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
        if(prevTime.getTime() < thisTime.getTime()){
            if(difference > 0){
            alert(" Selected Date is greater than scheduled date ");
            document.getElementById("preStartLocationPlanDate").value = document.getElementById("todayDate").value;
        }
        }
        }
         function calculateTime(){
        var endDates = document.getElementById("tripScheduleDate").value;
        var endTimeIds = document.getElementById("tripScheduleTime").value;
        var tempDate1 = endDates.split("-");
        var tempTime3 = endTimeIds.split(" ");
        var tempTime1 = tempTime3[0].split(":");
        if(tempTime3[1] =="PM"){
          tempTime1[0] = 12 + parseInt(tempTime1[0]);
        }
        var stDates = document.getElementById("preStartLocationPlanDate").value;
        var hour = document.getElementById("preStartLocationPlanTimeHrs").value;
        var minute = document.getElementById("preStartLocationPlanTimeMins").value;
        var stTimeIds = hour + ":" + minute + ":" + "00";
        var tempDate2 = stDates.split("-");
        var tempTime2 = stTimeIds.split(":");
        var prevTime = new Date(tempDate2[2],tempDate2[1],tempDate2[0],tempTime2[0],tempTime2[1]);  // Feb 1, 2011
        var thisTime = new Date(tempDate1[2],tempDate1[1],tempDate1[0],parseInt(tempTime1[0]),tempTime1[1]);              // now
       var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
        var hoursDifference = Math.floor(difference/1000/60/60);
        if(prevTime.getTime() < thisTime.getTime()){
         if(hoursDifference > 0){
            alert("Selected Time is greater than scheduled Time ");
            document.getElementById("preStartLocationPlanTimeHrs").focus();
                 }
     }
     }
        </script>
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>


        <script type="text/javascript" language="javascript">




            function getDriverName() {
                var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));

            }
        </script>
        <script language="">
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }



        </script>

<script type="text/javascript">

            function computeVehicleCapUtil(){
                var orderWeight = document.trip.totalWeight.value;
                var vehicleCapacity = document.trip.vehicleTonnage.value;
                if(vehicleCapacity > 0) {
                    var utilPercent = (orderWeight/vehicleCapacity) * 100;
                    document.trip.vehicleCapUtil.value = utilPercent.toFixed(2);


                    var e = document.getElementById("vehicleCheck");
                    e.style.display = 'block';
                }
            }
            //start ajax for vehicle Nos
                $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#vehicleNo').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getVehicleRegNos.do",
                            dataType: "json",
                            data: {
                                vehicleNo: request.term,
                                textBox: 1
                            },
                            success: function(data, textStatus, jqXHR) {
//                                alert(data);
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                //console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#vehicleId').val(tmp[0]);
                        $('#vehicleNo').val(tmp[1]);
                        $('#vehicleTonnage').val(tmp[2]);
                        $('#driver1Id').val(tmp[3]);
                        $('#driver1Name').val(tmp[4]);
                        $('#driver2Id').val(tmp[5]);
                        $('#driver2Name').val(tmp[6]);
                        $('#driver3Id').val(tmp[7]);
                        $('#driver3Name').val(tmp[8]);
                        //$itemrow.find('#vehicleId').val(tmp[0]);
                        //$itemrow.find('#vehicleNo').val(tmp[1]);
                        return false;
                    }
                    // Format the list menu output of the autocomplete
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    //alert(item);
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            //.append( "<a>"+ item.Name + "</a>" )
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

            });
            //end ajax for vehicle Nos



        </script>



    </head>
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.TripUnFreeze" text="Trip UnFreeze"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.TripUnFreeze" text="Trip UnFreeze"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                

    
    <body >
        
        <form name="trip" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            
            <%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = sdf.format(today);
            %>

            <%
            String vehicleMileageAndTollRate = "";
            String vehicleMileage = "0";
            String tollRate = "0";
            String fuelPrice = "0";
            String[] temp = null;
            if(request.getAttribute("vehicleMileageAndTollRate") != null){
                vehicleMileageAndTollRate = (String)request.getAttribute("vehicleMileageAndTollRate");
                temp = vehicleMileageAndTollRate.split("-");
                vehicleMileage = temp[0];
                tollRate = temp[1];
                fuelPrice = temp[2];
            }
            %>
            <input type="hidden" name="vehicleMileage" value="<%=vehicleMileage%>" />
            <input type="hidden" name="tollRate" value="<%=tollRate%>" />
            <input type="hidden" name="fuelPrice" value="<%=fuelPrice%>" />
            <input type="hidden" name="tripType" value="<c:out value="${tripType}"/>" />
            <input type="hidden" name="statusId" value="<c:out value="${statusId}"/>" />

            
<table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:300;">

                            <div id="first">
                                <c:if test = "${tripDetails != null}" >
                                    <c:forEach items="${tripDetails}" var="trip">
                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                        <td> <c:out value="${trip.orderRevenue}" /></td>
                                            
                                     </tr>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Projected Expense:</b></font></td>
                                        <td> <c:out value="${trip.orderExpense}" /></td>
                                            
                                     </tr>
                                    <c:set var="profitMargin" value="" />
                                     <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                     <c:set var="orderExpense" value="${trip.orderExpense}" />

                                     <input type="hidden" name="orderExpense" Id="orderExpense" class="textbox" value='<c:out value="${trip.orderExpense}" />' >
                                     <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                     <%
                                     String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                     String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
                                     float profitPercentage = 0.00F;
                                     if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                         profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                     }


                                     %>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                        <td>  <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)
                                            <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                                        <td>

                                        <td>
                                     </tr>
                                </table>
                                </c:forEach>
                             </c:if>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            






            <div id="tabs" >
                <ul>
                    <li><a href="#action"><span>Action</span></a></li>
                    <li><a href="#tripDetail"><span>Trip Details</span></a></li>
                    <li><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
<!--                    <li><a href="#driverDetail"><span>Driver</span></a></li>
                    <li><a href="#cleanerDetail"><span>Cleaner</span></a></li>-->
<!--                    <li><a href="#advDetail"><span>Advance</span></a></li>-->
                    <!--<li><a href="#expDetail"><span>Expense Details</span></a></li>-->
<!--                    <li><a href="#summary"><span>Remarks</span></a></li>-->
                </ul>

                <div id="tripDetail">
                   <table class="table table-info mb30 table-hover" id="bg" >
                       <thead>
                        <tr>
                            <th  colspan="6" >Trip Details</th>
                        </tr>
                       </thead>
                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">


                        <tr>
<!--                            <td ><font color="red">*</font>Trip Sheet Date</td>
                            <td ><input type="text" name="tripDate" class="datepicker" value=""></td>-->
                            <td >CNote No(s)</td>
                            <td >
                                <c:out value="${trip.cNotes}" />
                            </td>
                            <td >Billing Type</td>
                            <td >
                                <c:out value="${trip.billingType}" />
                            </td>
                        </tr>
                        <tr>
<!--                            <td >Customer Code</td>
                            <td >BF00001</td>-->
                            <td >Customer Name</td>
                            <td >
                                <c:out value="${trip.customerName}" />
                                <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${customerName}" />'>
                                <input type="hidden" name="tripId" Id="tripId" class="textbox" value='<c:out value="${trip.tripId}" />'>
                                <input type="hidden" name="originId" Id="tripId" class="textbox" value='<c:out value="${trip.originId}" />'>
                                <input type="hidden" name="vehicleTypeId" Id="tripId" class="textbox" value='<c:out value="${trip.vehicleTypeId}" />'>
                                <input type="hidden" name="vehicleId" Id="vehicleId" class="textbox" value="">
                                <input type="hidden" name="driver1Id" Id="driver1Id" class="textbox" value=""  >
                            </td>
                            <td >Customer Type</td>
                            <td  colspan="3" >
                                <c:out value="${trip.customerType}" />
                            </td>
                        </tr>
                        <tr>
                            <td >Route Name</td>
                            <td >
                                <c:out value="${trip.routeInfo}" />
                            </td>
<!--                            <td >Route Code</td>
                            <td  >DL001</td>-->
                            <td >Reefer Required</td>
                            <td  >
                                <c:out value="${trip.reeferRequired}" />
                            </td>
                            <td >Order Est Weight (MT)</td>
                            <td  >
                                <c:out value="${trip.totalWeight}" />
                            </td>
                        </tr>
                        <tr>
                            <td >Vehicle Type</td>
                            <td >
                                <c:out value="${trip.vehicleTypeName}" />
                            </td>
                            <td ><font color="red">*</font>Vehicle No</td>
                            <td >
                                <c:out value="${trip.vehicleNo}" />

                            </td>
                            <td >Vehicle Capacity (MT)</td>
                            <td >
                                <c:out value="${trip.vehicleTonnage}" />

                            </td>
                        </tr>

                        <tr>
                            <td >Veh. Cap [Util%]</td>
                            <td >
                                <c:out value="${trip.vehicleCapUtil}" />
                            </td>
                            <td >Special Instruction</td>
                            <td >-</td>
                            <td >Trip Schedule</td>
                            <td ><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" />
                             <input type="hidden" name="tripScheduleDate"  id="tripScheduleDate" value='<c:out value="${trip.tripScheduleDate}" />'>
                            <input type="hidden" name="tripScheduleTime" id="tripScheduleTime" value='<c:out value="${trip.tripScheduleTime}" />'></td>
                        </tr>

                      
                        <tr>
                            <td ><font color="red">*</font>Driver </td>
                            <td  colspan="5" >
                                 <c:out value="${trip.driverName}" />
                            </td>

                        </tr>
                            </c:forEach>
                       </c:if>
                    </table>
                    
                    
                    
                    
                   <c:if test = "${expiryDateDetails != null}" >
                 <table class="table table-info mb30 table-hover" id="bg" >
                     <thead>
                        <tr>
                            <th  colspan="4" >Vehicle Compliance Check</th>
                        </tr>
                     </thead>
                          <c:forEach items="${expiryDateDetails}" var="expiryDate">
                        <tr>
                            <td >Vehicle FC Valid UpTo</td>
                            <td ><label><font color="green"><c:out value="${expiryDate.fcExpiryDate}" /></font></label></td>
                        </tr>
                        <tr>
                            <td >Vehicle Insurance Valid UpTo</td>
                            <td ><label><font color="green"><c:out value="${expiryDate.insuranceExpiryDate}" /></font></label></td>
                        </tr>
                        <tr>
                            <td >Vehicle Permit Valid UpTo</td>
                            <td ><label><font color="green"><c:out value="${expiryDate.permitExpiryDate}" /></font></label></td>
                        </tr>
                        <tr>
                            <td >Road Tax Valid UpTo</td>
                            <td ><label><font color="green"><c:out value="${expiryDate.roadTaxExpiryDate}" /></font></label></td>
                        </tr>
                        </c:forEach>
                    </table>
                    
                    
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Next" /></a>
                    </center>
                       </c:if>

                </div>
                <div id="routeDetail">
                    
                    <c:if test = "${tripPointDetails != null}" >
                   <table class="table table-info mb30 table-hover"  >
                       <thead>
                        <tr >
                                <th  height="30" >Point Name</th>
                                <th  height="30" >Type</th>
                                <th  height="30" >Route Order</th>
                                <th  height="30" >Address</th>
                                <th  height="30" >Planned Date</th>
                                <th  height="30" >Planned Time</th>
                      </tr>
                       </thead>
                        <c:forEach items="${tripPointDetails}" var="tripPoint">

                        <tr >
                                <td  height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                      </tr>




                        </c:forEach >
                    </table>
                    
                    
                    
                  <table class="table table-info mb30 table-hover"  >
                            <c:if test = "${tripDetails != null}" >
                                <c:forEach items="${tripDetails}" var="trip">
                                    <tr>
                                        <th  width="150"> Estimated KM</th>
                                        <th  width="120" > <c:out value="${trip.estimatedKM}" />&nbsp;</th>
                                        <th  colspan="4">&nbsp;</th>
                                    </tr>
                                    <tr>
                                        <th width="150"> Estimated Reefer Hour</th>
                                        <th  width="120"> <c:out value="${trip.estimatedTransitHours * 60 / 100}" />&nbsp;</th>
                                        <th  colspan="4">&nbsp;</th>
                                    </tr>

                                </c:forEach>
                            </c:if>
                        </table>


                    </c:if>
                    
                    
                    
                    
  
                    
                    
                </div>

                <div id="action">
                    
                    
               <table class="table table-info mb30 table-hover"  >
                        <tr>
                              <input type="hidden" onclick="processPreStartCheckBox();"  name="preStartLocationCheckbox"  id="preStartLocationCheckbox" value="1"  />
                            <td ><font color="red">*</font>Action: </td>
                            <td  >
                                <select name="actionName" id="actionName" class="form-control" style="width:250px;height:40px" onChange="setButtons();">
                                    <option value="0">select</option>
<!--                                    <option value="1">Freeze</option>-->
                                    <option value="2">UnFreeze</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td  ><font color="red">*</font>Remarks</td>
                            <td  ><textarea class="form-control" style="width:250px;height:40px" name="actionRemarks" rows="3" cols="100"></textarea> </td>
                        </tr>
                        <tr>
                            <td  >Do you want to intimate?</td>
                            <td  >
                                Client <input type="checkbox" name="client" value="1" /> &nbsp;
                                A/C Mgr<input type="checkbox" name="manager" value="1" /> &nbsp;
                                Fleet Mgr <input type="checkbox" name="fleetManager" value="1" /> &nbsp;
                            </td>
                        </tr>

                    </table>
                    
                    <table border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" >
                            <tr>
                                <td>
                        <div id="actionDiv" style="display:block;">
                        <input type="button" class="btn btn-success" value="Save Action" name="Next" onclick="saveAction();" />
                        </div>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                    <div id="preStartDiv" style="display:none;">
                                        <table border="0" width="500" align="left" cellpadding="0" cellspacing="0" >
                                            <tr>
                                                <td >Pre Start Location </td>
                                                <td  align="left"  >
<!--                                                    <input type="checkbox" onclick="processPreStartCheckBox();" style="width:10px;" name="preStartLocationCheckbox"  id="preStartLocationCheckbox" value="0" />same as trip start location-->
                                                </td>
                                            </tr>


                                        </table>
                                        <div id="preStartDetailDiv" style="display:block;">
                                        <table border="0" width="500" align="left" cellpadding="0" cellspacing="0" >
                                            <tr>
                                                <td >Pre Start Location  </td>
                                                <td  align="left"  >
                                                    <input  type="text" name="preStartLocation" id="preStartLocation" onBlur="fetchRouteInfo();" value="" class="textbox" />
                                                    <input  type="hidden" name="preStartLocationId" id="preStartLocationId" value="" class="textbox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >preStartLocationPlanDate:</td>
                                                <td ><input  type="text" name="preStartLocationPlanDate"  id="preStartLocationPlanDate"  value='<%=startDate%>' class="datepicker" onchange="calculateDate();" class="datepicker"/>
                                                    <input type="hidden" name="todayDate" id="todayDate" value='<%=startDate%>'/></td>
                                            </tr>
                                            <tr>

                                                    <td >preStartLocationPlanTime:</td>
                                                     <td >HH: <select name="preStartLocationPlanTimeHrs"  id="preStartLocationPlanTimeHrs" onchange="calculateTime();">
                                                        <option value='00'>00</option>
                                                        <option value='01'>01</option>
                                                        <option value='02'>02</option>
                                                        <option value='03'>03</option>
                                                        <option value='04'>04</option>
                                                        <option value='05'>05</option>
                                                        <option value='06'>06</option>
                                                        <option value='07'>07</option>
                                                        <option value='08'>08</option>
                                                        <option value='09'>09</option>
                                                        <option value='10'>10</option>
                                                        <option value='11'>11</option>
                                                        <option value='12'>12</option>
                                                        <option value='13'>13</option>
                                                        <option value='14'>14</option>
                                                        <option value='15'>15</option>
                                                        <option value='16'>16</option>
                                                        <option value='17'>17</option>
                                                        <option value='18'>18</option>
                                                        <option value='19'>19</option>
                                                        <option value='20'>20</option>
                                                        <option value='21'>21</option>
                                                        <option value='22'>22</option>
                                                        <option value='23'>23</option>
                                                        

                                                    </select>
                                                    MI: <select name="preStartLocationPlanTimeMins"  id="preStartLocationPlanTimeMins" >
                                                        <option value='00'>00</option>
                                                        <option value='01'>01</option>
                                                        <option value='02'>02</option>
                                                        <option value='03'>03</option>
                                                        <option value='04'>04</option>
                                                        <option value='05'>05</option>
                                                        <option value='06'>06</option>
                                                        <option value='07'>07</option>
                                                        <option value='08'>08</option>
                                                        <option value='09'>09</option>
                                                        <option value='10'>10</option>
                                                        <option value='11'>11</option>
                                                        <option value='12'>12</option>
                                                        <option value='13'>13</option>
                                                        <option value='14'>14</option>
                                                        <option value='15'>15</option>
                                                        <option value='16'>16</option>
                                                        <option value='17'>17</option>
                                                        <option value='18'>18</option>
                                                        <option value='19'>19</option>
                                                        <option value='20'>20</option>
                                                        <option value='21'>21</option>
                                                        <option value='22'>22</option>
                                                        <option value='23'>23</option>
                                                        <option value='24'>24</option>
                                                        <option value='25'>25</option>
                                                        <option value='26'>26</option>
                                                        <option value='27'>27</option>
                                                        <option value='28'>28</option>
                                                        <option value='29'>29</option>
                                                        <option value='30'>30</option>
                                                        <option value='31'>31</option>
                                                        <option value='32'>32</option>
                                                        <option value='33'>33</option>
                                                        <option value='34'>34</option>
                                                        <option value='35'>35</option>
                                                        <option value='36'>36</option>
                                                        <option value='37'>37</option>
                                                        <option value='38'>38</option>
                                                        <option value='39'>39</option>
                                                        <option value='40'>40</option>
                                                        <option value='41'>41</option>
                                                        <option value='42'>42</option>
                                                        <option value='43'>43</option>
                                                        <option value='44'>44</option>
                                                        <option value='45'>45</option>
                                                        <option value='46'>46</option>
                                                        <option value='47'>47</option>
                                                        <option value='48'>48</option>
                                                        <option value='49'>49</option>
                                                        <option value='50'>50</option>
                                                        <option value='51'>51</option>
                                                        <option value='52'>52</option>
                                                        <option value='53'>53</option>
                                                        <option value='54'>54</option>
                                                        <option value='55'>55</option>
                                                        <option value='56'>56</option>
                                                        <option value='57'>57</option>
                                                        <option value='58'>58</option>
                                                        <option value='59'>59</option>
                                                    </select>
                                                </td>

                                                </tr>

                                            <tr>
                                                <td >preStartLocationDistance</td>
                                                <td ><input  type="text" name="preStartLocationDistance"  id="preStartLocationDistance"  onchange="computeExpense();"  value="" class="textbox" /></td>
                                                </tr>
                                            <tr>
                                                <td >preStartLocationDurationHrs:</td>
                                                <td ><input  type="text" name="preStartLocationDurationHrs"  id="preStartLocationDurationHrs"  value="" class="textbox" /></td>
                                                </tr>
                                            <tr>
                                                <td >preStartLocationDurationMins:</td>
                                                <td ><input  type="text" name="preStartLocationDurationMins"  id="preStartLocationDurationMins"  value="" class="textbox" /></td>
                                                </tr>

                                            <tr>
                                                <td >preStartRouteExpense</td>
                                                <td ><input  type="text" name="preStartRouteExpense"  id="preStartRouteExpense"  value="" class="textbox" /></td>

                                            </tr>

                                        </table>
                                    </div>
                                    </div>

                                </td>
                                </tr>


                        </table>
                </div>
                



                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
                
            </div>
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>