<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>


<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">

        function viewTipPODDetails(orderId, status) {
            document.tripSheet.action = '/throttle/viewTripPODDetails.do?orderId=' + orderId + "&status=" + status;
            document.tripSheet.submit();
        }
        
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script type="text/javascript">
        function setValues() {
            if ('<%=request.getAttribute("fromDate")%>' != 'null') {
                document.getElementById('fromDate').value = '<%=request.getAttribute("fromDate")%>';
            }
            if ('<%=request.getAttribute("toDate")%>' != 'null') {
                document.getElementById('toDate').value = '<%=request.getAttribute("toDate")%>';
            }
        }

        function submitPage() {
            document.tripSheet.action = '/throttle/handleTripPOD.do';
            document.tripSheet.submit();
        }

    </script>


    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.TripSheet" text="Order POD Approval"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
                <li class=""><spring:message code="hrms.label.TripSheet" text="Order POD Approval"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="sorter.size(20);
                        setValues();">
                    <form name="tripSheet" method="post">
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <%@include file="/content/common/message.jsp" %>
                        <table class="table table-info mb30 table-hover" style="width:50%">
                            <thead><tr><th colspan="4">View Order POD</th></tr></thead>
                            <tr>
                                <td ><font color="red">*</font>From Date</td>
                                <td ><input name="fromDate" id="fromDate" type="text" class="datepicker , form-control" style="width:175px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                                <td ><font color="red">*</font>To Date</td>
                                <td ><input name="toDate" id="toDate" type="text" class="datepicker , form-control" style="width:175px;height:40px" onClick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>

                            </tr>

                        </table>
                        <center>
                            <input type="button"   value="Search" class="btn btn-success" name="Search" onclick="submitPage();" > &emsp;
                        </center>
                        <br>
                        <br>
                        <c:if test="${tripDetails != null}">
                            <table class="table table-info mb30 table-hover " id="table"  style="font-style:normal;;font-size:10pt;font-family:Arial;" size="2">
                                <thead>
                                    <tr >
                                        <th >Sno</th>                                                                                
                                        <th >Order No </th>
                                        <th >Batch Date</th>                        
                                        <th >POD Status</th>                        
                                        <th >View</th>                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <% int index = 1;%>
                                    <c:forEach items="${tripDetails}" var="tripDetails">
                                        <%
                                            String className = "text1";
                                            if ((index % 2) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>

                                        <tr height="30">
                                            <td class="<%=className%>" width="40" align="left">
                                                <%=index%>
                                            </td>
                                            <td class="<%=className%>" >
                                                <c:out value="${tripDetails.orderNo}"/>
                                            </td>
                                            <td class="<%=className%>" >
                                                <c:out value="${tripDetails.batchDate}"/>
                                            </td>
                                            <td class="<%=className%>" >
                                                <c:if test="${tripDetails.podStatus == 0}">
                                                    Waiting for Approval
                                                </c:if>
                                                <c:if test="${tripDetails.podStatus == 2}">
                                                    Non-clear
                                                </c:if>
                                                <c:if test="${tripDetails.podStatus == 1}">
                                                    Clear - Approved
                                                </c:if>
                                            </td>
                                            <td class="<%=className%>" width="120">
                                                <c:if test="${tripDetails.podStatus == 0}">                                                    
                                                <a href="#" onclick="viewTipPODDetails('<c:out value="${tripDetails.orderId}"/>', '<c:out value="${tripDetails.podStatus}"/>')"><span class="label label-danger">Approve</span></a>
                                                </c:if>
                                                <c:if test="${tripDetails.podStatus != 0}">
                                                    
                                                <a href="#" onclick="viewTipPODDetails('<c:out value="${tripDetails.orderId}"/>', '<c:out value="${tripDetails.podStatus}"/>')"><span class="label label-Primary">View</span></a>
                                                </c:if>
                                            </td>
                                        </tr>
                                        <%index++;%>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </c:if>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" >5</option>
                                    <option value="10">10</option>
                                    <option value="20" selected="selected">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 0);
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>