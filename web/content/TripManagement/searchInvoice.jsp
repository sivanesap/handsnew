<%--
    Document   : searchInvoice
    Created on : Dec 19, 2012, 5:20:25 PM
    Author     : Entitle
--%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->

        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <%@ page import="ets.domain.security.business.SecurityTO" %>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script src="/throttle/js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="/throttle/js/TableSort.js" language="javascript" type="text/javascript"></script>
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />

        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true
                });
            });
            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    </head>
    <script type="text/javascript">

        function submitPage(value)
        {
            if(document.billG.customer.value==0){
                alert("Customer should not be Empty");
                document.billG.customer.focus();
                return;
            }/*if(document.billG.ownership.value==0){
                alert("Owner ship should not be Empty");
                document.billG.ownership.focus();
                return;
            }*/if(document.billG.billType.value==0){
                alert("Bill Type should not be Empty");
                document.billG.billType.focus();
                return;
            }

            //            if(textValidation(document.billG.fromDate,'From Date')){
            //                return;
            //            }else if(textValidation(document.billG.toDate,'To Date')){
            //                return;
            //            }
            document.billG.action="/throttle/displyTripList.do";
            document.billG.submit();
        }

    </script>
    <body>
        <form name="billG" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <table cellpadding="0" cellspacing="2" align="center" border="0" width="700" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr>
                    <td><b>Search Invoice</b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;">Close</span>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding:15px;" align="left">
                        <div class="tabs" align="center" style="width:900px">
                            <table width="700" cellpadding="0" cellspacing="1" border="0" align="left" class="table4">
                                <tr>
                                    <td  height="30"><font color="red">*</font> <b>Customer</b></td>
                                    <td height="30">
                                        <select  style="width:125px;" name="customer" id="customer">
                                            <option selected   value=0>-select-</option>
                                            <c:if test = "${customerList != null}" >
                                                <c:forEach items="${customerList}" var="cust">
                                                    <option  value='<c:out value="${cust.customerId}" />'><c:out value="${cust.customerName}" /> </option>
                                                </c:forEach >
                                            </c:if>
                                        </select>  </td>
                                    <td  height="30"><font color="red">*</font>Ownership</td>
                                    <td height="30">
                                        <select  style="width:125px;" name="ownership" id="ownership">
                                            <option selected   value=''>-select-</option>
                                            <option  value='1'>Own</option>
                                            <option  value='2'>Attach</option>
                                        </select>
                                    </td>
                                    <td  height="30"><font color="red">*</font>Bill Type</td>
                                    <td height="30">
                                        <select  style="width:125px;" name="billType" id="billType">
                                            <option selected   value='0'>-select-</option>
                                            <option  value='Depot'>Depot</option>
                                            <option  value='Tancem'>Tancem</option>
                                            <option  value='Party'>Party</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td  height="30">From Date</td>
                                    <td  height="30"><input type="text" name="fromDate" class="datepicker" ></td>
                                    <td >To Date</td>
                                    <td ><input type="text" name="toDate" class="datepicker" ></td>
                                    <td  height="30" colspan="2">
                                        <center> <input type="button"  value="Fetch Data"  class="button" name="Fetch Data" onClick="submitPage(this.value)"></center>
                                    </td>
                                </tr>
                                <tr>
                                    <c:if test="${lastInvoiceDetail != null}">
                                        <c:forEach items="${lastInvoiceDetail}" var="lid">
                                            <td colspan="2">
                                                <b>Last Invoice Date : <c:out value="${lid.invoiceDate}"/> </b><br>
                                            </td>
                                            <td colspan="2">
                                                <b>From : <c:out value="${lid.fromDate}"/> To : <c:out value="${lid.toDate}"/>  </b><br>
                                            </td>

                                        </c:forEach>
                                    </c:if>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>


            </table>
        </form>

    </body>

</html>