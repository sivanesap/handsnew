<%-- 
    Document   : printbillgeneration
    Created on : Dec 7, 2013, 10:07:20 AM
    Author     : srinientitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/filtergrid.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script>
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
        </script>
    </head>
    <body>
        <form name="viewbillGeneration" method="post">
            <div id="print">
                <table width="95%" align="center" cellpadding="0" cellspacing="0" border="0" class="border" >
                    <h1 align="center">Invoice</h1>
                    <br>
                    <tr>
                        <td valign="top">
                            <h4>Laxman Logistics Pvt Ltd.</h4>
                            <h3><p>15/1, Asaf Ali Road</p>
                                <p>New Delhi</p>
                                <p>110002</p></h3>
                        </td>
                        <td>

                            <table width="100%" height="200" border="0">
                                <tr>
                                    <td width="50%">Invoice No :<b><c:out value="${invoiceNo}"/></b></td>
                                    <td>Dated :<b><c:out value="${invoicedate}"/></b></td>
                                </tr>
                                <tr>
                                    <td>Delivery Note:<b>HR 55 Q 7013</b></td>
                                    <td>Mode/Terms Of Pament</td>
                                </tr>
                                <tr>
                                    <td>Suppliers's Ref. <b>IFL/LLPL/13-14/4525</b></td>
                                    <td>Other Reference(s)</td>
                                </tr>
                            </table>
                        </td>


                    </tr>
                    <tr>
                        <td valign="top">
                            <h3>Buyer</h3>
                            <h4><c:out value="${customerName}"/></h4>
                            <h3><p><c:out value="${customerAddress}"/></p>
                                
                        </td>
                        <td>
                            <table width="100%" height="200" border="0">
                                <tr>
                                    <td>Buyer's Order No :<b></b></td>
                                    <td>Dated :<b></b></td>
                                </tr>
                                <tr>
                                    <td width="50%">Delivery Document No:<b>HR 55 Q 7013</b></td>
                                    <td>Dated :<b><c:out value="${invoicedate}"/></b></td>
                                </tr>
                                <tr>
                                    <td>Despatched through. <b>14086</b></td>
                                    <td>Destination :<c:out value="${destinationName}"/> <b></b></td>
                                </tr>
                               
                            </table>
                        </td>


                    </tr>
                </table>

                <br>
                <br>
                    <c:if test="${invoiceList !=null}">
                <table width="95%" align="center" cellpadding="0" cellspacing="0" border="0" class="border" >
                    <tr>
                    
                        <td  >S.No</td>
                        <td >Description Of Goods</td>
                        <td >No of Packages</td>
                        <td >Total Package Weight</td>
                        <td align='right' >Amount</td>
                    
                    </tr>
                    <%
                    String  grandTotal=(String)request.getAttribute("grandTotal");
                    ArrayList routeList=(ArrayList)request.getAttribute("routeList");
                    ArrayList totalPackagesList=(ArrayList)request.getAttribute("totalPackagesList");
                    ArrayList totalPackagesWeightList=(ArrayList)request.getAttribute("totalPackagesWeightList");
                    ArrayList freightList=(ArrayList)request.getAttribute("freightList");
                    %>
                    <%int sno=1;%>
                    <%
                    for(int i=0;i<routeList.size();i++){
                        String routeName=(String)routeList.get(i);
                        String totalPackage=(String)totalPackagesList.get(i);
                        String totalWeightage=(String)totalPackagesWeightList.get(i);
                        String freight=(String)totalPackagesWeightList.get(i);
                    %>
                    <tr>
                        <td><%=sno++%></td>
                        <td><%=routeName%></td>
                        <td><%=totalPackage%></td>
                        <td><%=totalWeightage%></td>
                        <td align='right'><%=freight%></td>
                    </tr>
                    <%}%>
                    <tr><td colspan="4" style="text-align: right;">Total Charges</td><td><%=grandTotal%></td>
                    </tr>
                    
</table>
                    </c:if>

                <table width="95%" align="center" cellpadding="0" cellspacing="0" class="border">
                    <tr>
                        <td><lable>Amount Chargeable (in words)</lable><br>
                    <b>Rupees:</b><%
                    String  grandTotal1=(String)request.getAttribute("grandTotal");
                            float spareTotal = 0.00F;
                            spareTotal = Float.parseFloat(grandTotal1);
                        %>
                        <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWords" >
                            <% spareTotalRound.setRoundedValue(String.valueOf(spareTotal));%>
                            <%-- <b><jsp:getProperty name="spareTotalRound" property="roundedValue" /> </b> --%>
                            <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                            <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />Only</b>
                        </jsp:useBean>                  
                    <br>                     
                    <lable>Remarks:</lable><br>
                    <lable>Being Amount provided for the Freight charges from Rajpura-</lable><br>
                    <lable>Cochin vide vehicle no. HR 55 Q 7013/14086</lable><br>
                    <lable>Company service Tax No : <b>AABCL5755FSD002</b> </lable><br>
                    <lable>Company's PAN : <b>AABCL5755F</b> </lable><br>
                    <lable>Declaration: </lable><br>
                    <lable>Service Tax payable by Consignor / Consignee</lable><br>
                    <lable>This is to certify and confirm that we have not availed</lable><br>
                    <lable>Cenvat credit of duty on inputs and capital ggodsand</lable><br>
                    <lable>Providing transport service to you under the provision of </lable><br>
                    <lable>the cenvat credit rules 2004 and also have not availed</lable><br>
                    <lable>benifit of notification no. 12/2003 Service tax dated june</lable><br>
                    <lable>2003 (ide Notification no. 32/2004 Service tax dated on 03/12/2004)</lable><br>
                        </td>
                    </tr>
                    <tr><td align="right"> for Laxman Logistics Pvt Ltd.</td></tr>
                    <tr><td align="right"> Authorised .</td></tr>

</table>
            </div>
            <br>
            <br>

            <center>
                <input type="button" value="Print" onClick="print('print');" >
            </center>
        </form>
    </body>
</html>
