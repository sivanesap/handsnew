<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });

    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });

</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script>

    function submitPage(val) {
        var contractTypeId = $("#contractTypeId").val();
        var count = 0;

        if (contractTypeId == 2 || contractTypeId == 3) {
            count = checkOnDemandContract();
        }

        if (isEmpty(document.vehicleVendorContract.startDate.value)) {
            alert('please enter Contract From Date');
            document.getElementById("startDate").focus();
            return false;
        }

        if (isEmpty(document.vehicleVendorContract.endDate.value)) {
            alert('please enter Contract To Date');
            document.getElementById("endDate").focus();
            return false;
        }

        if (count == 0) {
            document.vehicleVendorContract.action = '/throttle/saveDriverVendorContract.do?type='+val;
            document.vehicleVendorContract.submit();
        }
    }

    function checkOnDemandContract() {
        var spot = $("[name=rate]");
        var count = 0;
        for (var i = 0; i < spot.length; i++) {
            if (spot[i].value == '') {
                alert("please enter the contract amount")
                spot[i].focus();
                count = 1;
                return count;
            }
        }
        return count;
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Vendor Contract</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor Contract</a></li>
            <li class="active">Create Fleet Vendor Contract</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <%--  <%try{%>--%>

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>
                <form name="vehicleVendorContract" method="post" >
                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover" style="width:70%">
                        <thead>  <tr >
                                <th  colspan="4" >Vendor Contract Info</th>
                            </tr></thead>
                        <tr >
                            <td>Vendor Name</td>
                            <td><input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>" class="form-control">
                                <input type="hidden" name="vendorName" id="vendorName" value="<c:out value="${vendorName}"/>" class="form-control">
                                <c:out value="${vendorName}"/></td>
                            <td>Contract Type</td>
                            <td>
                                <select name="contractTypeId" id="contractTypeId"  onchange="checkContractType(this.value);" style="height:22px;width:150px;">
                                    <!--<option value="0" selected>--Select--</option>-->
                                    <!--<option value="1" >Dedicated</option>-->
                                    <option value="2" >Market Hire</option>
                                    <!--<option value="3" selected>Both</option>-->
                                </select>
                            </td>
                        </tr>
                        <tr >
                            <td >Contract From</td>
                            <td ><input type="text" name="startDate" id="startDate" value="<c:out value="${startDate}" />" class="datepicker form-control"  style="height:22px;width:150px;color:black">
                            <input type="hidden" name="contractId" id="contractId" value="0" >
                            </td>

                            <td  >Contract To</td>
                            <td ><input type="text" name="endDate" id="endDate" value="<c:out value="${endDate}" />" class="datepicker form-control"  style="height:22px;width:150px;color:black"></td>
                        </tr>
                        <tr >
                            <td  >Payment Type</td>
                            <td>
                                <select name="paymentType" id="paymentType"   style="height:22px;width:150px;">
                                    <option value="1" >Monthly</option>
                                    <option value="2" >Trip Wise</option>
                                    <option value="3" >FortNight</option>
                                </select>
                            </td>
                            <td  colspan="2"></td>
                        </tr>
                    </table>
                    <br>

                    <div id="tabs" >
                        <ul class="nav nav-tabs">
                            <li data-toggle="tab"><a href="#driverContract"><span>Driver Contract</span></a></li>
                        </ul>
                        
                        <div id="driverContract">
                            <div id="driverCont">
                                <script>
                                    var driverCon = "";
                                    $(document).ready(function () {
                                        var iCnt = 1;
                                        var rowCnt = 1;
                                        // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                        driverCon = $($("#driverCont")).css({
                                            padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                            borderTopColor: '#999', borderBottomColor: '#999',
                                            borderLeftColor: '#999', borderRightColor: '#999'
                                        });
                                        $(driverCon).last().after('<table id="mainTableDriverContract" width="100%"><tr></td>\
                                        <table  class="table table-info mb30 table-hover" id="driverContractTypeWise' + iCnt + rowCnt + '" border="1">\n\
                                        <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                        <td>Sno</td><td>Driver Type</td><td>Contract Type</td><td>Rate</td></tr>\n\
                                        <tr>\n\
                                        <td>' + rowCnt + '<input type="hidden" name="iCnt0" id="iCnt0' + iCnt + '" value="' + iCnt + '"/></td>\n\
                                        <td><select  type="text" name="driverWorkTypeId" id="driverWorkTypeId' + iCnt + '" style="height:25px;width:150px;"><option value="0">--Select--</option><option value="1">Batch</option><option value="2">Heavy</option></select></td>\n\
                                        <td><select  type="text" name="driverWorkContractTypeId" id="driverWorkContractTypeId' + iCnt + '" style="height:25px;width:150px;"><option value="0">--Select--</option><option value="12">12 Hrs</option><option value="24">24 Hrs</option></select></td>\n\
                                        <td><input  type="text" name="rate" id="rate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:25px;width:150px;"/>\n\
                                            <input type="hidden" name="contractHireId" id="contractHireId' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:25px;width:150px;"/>\n\
                                            <input type="hidden" name="activeStatus" id="activeStatus' + iCnt + '" value="Y" style="height:25px;width:150px;"/></td>\n\
                                        </tr></table><br>\n\
                                        <table ><tr>\n\
                                        <td><input class="btn btn-success" type="button" name="addDriverContract" id="addDriverContract' + iCnt + rowCnt + '" value="Add" onclick="addRowDriverContract(' + iCnt + rowCnt + ',' + rowCnt + ')" />\n\
                                        <input class="btn btn-success" type="button" name="removeDriverContract" id="removeDriverContract' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRowDriverContract(' + iCnt + rowCnt + ')" /></td>\n\
                                       </tr></table></td></tr></table><br><br>');
                                    });

                                    // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                    var divValue, values = '';
                                    function GetTextValue() {
                                        $(divValue).empty();
                                        $(divValue).remove();
                                        values = '';
                                        $('.input').each(function () {
                                            divValue = $(document.createElement('div')).css({
                                                padding: '5px', width: '200px'
                                            });
                                            values += this.value + '<br />'
                                        });
                                        $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                        $('body').append(divValue);
                                    }

                                    function addRowDriverContract(val, v1) {
                                        //                            alert(val);
                                        //                            alert(v1);
                                        var loadCnt = val;
                                        var loadCnt1 = v1;
                                        var routeInfoSize = $('#driverContractTypeWise' + loadCnt + ' tr').size();
                                        //                            alert(routeInfoSize);
                                        var addDriverConDetails = "addDriverCon" + loadCnt;
                                        var driverConDetails = "driverContractTypeWise" + loadCnt;
                                        $('#driverContractTypeWise' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSize + '<input type="hidden" name="iCnt0" id="iCnt0' + loadCnt + '" value="' + loadCnt1 + '"/></td>\n\
                                    <td><select type="text" name="driverWorkTypeId" id="driverWorkTypeId' + loadCnt + '" style="height:25px;width:150px;"><option value="0">--Select--</option><option value="1">Batch</option><option value="2">Heavy</option></select></td>\n\
                                    <td><select  type="text" name="driverWorkContractTypeId" id="driverWorkContractTypeId' + loadCnt + '" style="height:25px;width:150px;"><option value="0">--Select--</option><option value="12">12 Hrs</option><option value="24">24 Hrs</option></select></td>\n\
                                    <td><input type="text" name="rate" id="rate' + loadCnt + '" value="0" style="height:25px;width:150px;"/>\n\
                                        <input type="hidden" name="activeStatus" id="activeStatus' + loadCnt + '" value="Y" style="height:25px;width:150px;"/>\n\
                                        <input type="hidden" name="contractHireId" id="contractHireId' + loadCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:25px;width:150px;"/></td>\n\</tr>');
                                        loadCnt++;
                                    }
                                    function deleteRowDriverContract(val) {
                                        var loadCnt = val;
                                        //                            alert(val);
                                        var addDriverConDetails = "addDriverCon" + loadCnt;
                                        var driverConDetails = "driverContractTypeWise" + loadCnt;
                                        if ($('#driverContractTypeWise' + loadCnt + ' tr').size() > 2) {
                                            $('#driverContractTypeWise' + loadCnt + ' tr').last().remove();
                                            loadCnt = loadCnt - 1;
                                        } else {
                                            alert('One row should be present in table');
                                        }
                                    }
                                </script>

                            </div>
                            <center>
                                <input type="button" class="btn btn-info" value="Save" onclick="submitPage('1');" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>
                            </center>
                        </div>
                                

                        <script>
                            $('.btnNext').click(function() {
                                $('.nav-tabs > .active').next('li').find('a').trigger('click');
                            });
                            $('.btnPrevious').click(function() {
                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                            });
                        </script>

                                

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                    <%--   <%}catch(Exception e)
                 {
                        out.println(e.toString());
                    }
               %>--%>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>