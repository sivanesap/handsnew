<%--
    Document   : contractPointToPointWeight
    Created on : Jan 27, 2015, 8:35:48 PM
    Author     : Nivan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function () {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<html>
    <body>

        <style>
            body {
                font:13px verdana;
                font-weight:normal;
            }
        </style>


       <form name="vehicleVendorContract" method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
             <input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>"/>
            <input type="hidden" name="contractTypeId" Id="contractTypeId" value="<c:out value="${billingTypeId}"/>"/>
            <%-- <c:if test="${vendorVehicleContractLists != null}">
                <c:forEach items="${vendorVehicleContractLists}" var="vendorContract">--%>
                    <table width="980" align="center" class="border">

                        <tr>
                            <td class="contenthead" colspan="4" >vendorVehicleContractLists</td>
                        </tr>

                        <tr>
                            <td class="text1">Vendor Name</td>
                            <td class="text1"><input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>" class="textbox"><c:out value="${vendorName}"/></td>
                            <td class="text1">Contract Type</td>
                             <td class="text2"><select name="contractTypeId" id="contractTypeId" class="textbox" style="width:125px;" >


                           <c:if test="${contractTypeId == '0'}">
                                <option value="0" selected>--Select--</option>
                                <option value="1" >Dedicated</option>
                                <option value="2" >Hired</option>
                                <option value="3" >Both</option>
                               </c:if>
                                    <c:if test="${contractTypeId == '1'}">
                                <option value="1" selected>Dedicated</option>
                                <option value="2" >Hired</option>
                                <option value="3" >Both</option>

                               </c:if>
                                    <c:if test="${contractTypeId == '2'}">
                                <option value="1">Dedicated</option>
                                <option value="2" selected>Hired</option>
                                <option value="3" >Both</option>

                               </c:if>
                                  <c:if test="${contractTypeId == '3'}">
                                <option value="1">Dedicated</option>
                                <option value="2" >Hired</option>
                                <option value="3" selected>Both</option>

                               </c:if>
                                 </select>   </td>
                        </tr>
                        <tr>
                    <td class="text2">Contract From</td>
                    <td class="text2"><c:out value="${startDate}"/></td>
                    <td class="text2">Contract To</td>
                    <td class="text2">
                        <input type="hidden" class="datepicker" name="endDateOld" id="endDateold" value="<c:out value="${endDate}"/>"/>
                        <input type="text" class="datepicker" name="endDate" id="endDate" value="<c:out value="${endDate}"/>"/>
                    </td>
                </tr>
                        <tr>
                               <td class="text1">Payment Type </td>
                    <td  class="text1"> <select name="paymentType" id="paymentType" class="textbox" style="width:125px;" >

                            <c:if test="${paymentType == '0'}">
                                <option value="0" selected>--Select--</option>
                                <option value="1" >Monthly</option>
                                <option value="2" >Trip Wise</option>
                                <option value="3" >FortNight</option>

                            </c:if>
                            <c:if test="${paymentType == 1}">
                                <option value="1" selected>Monthly</option>
                                <option value="2" >Advance</option>
                                <option value="3" >FortNight</option>

                            </c:if>
                            <c:if test="${paymentType == 2}">
                                <option value="1" >Monthly</option>
                                <option value="2" selected>Advance</option>
                                <option value="3" >FortNight</option>

                            </c:if>
                            <c:if test="${paymentType == 3}">
                                <option value="1" >Monthly</option>
                                <option value="2" >Advance</option>
                                <option value="3" selected>FortNight</option>

                            </c:if>

                        </select></td>

                        </tr>
                    </table>

                            <%--  </c:forEach></c:if>--%>
            <br>
                <div id="tabs">
                <ul class="">
                    <li><a href="#deD"><span>DEDICATED</span></a></li>
                    <li><a href="#fullTruck"><span>ON DEMAND</span></a></li>

                </ul>

                    <div>

                <div id="deD">



                    <div id="routeWeightBreak">


                          <c:if test="${dedicateList != null}">
                            <table align="center" border="0" id="table" class="sortable" style="width:1000px;" >
                                <thead>
                                    <tr>
                                        <th align="center"><h3>S.No</h3></th>
                                <th align="center"><h3>Vehicle Type</h3></th>
                                <th align="center"><h3>Vehicle Units</h3></th>
                                <th align="center"><h3>Trailer Type</h3></th>
                                <th align="center"><h3>Trailer Units</h3></th>
                                <th align="center"><h3>Contract Category</h3></th>
                                <th align="center"><h3>Fixed Cost Per Vehicle & Month</h3></th>
                                <th colspan="2" align="center"><h3>Fixed Duration Per day</h3><br><br><table  class="contenthead" border="1"><tr><td width="250px;"><center>Hours</center></td><td width="212px;"><center>Minutes</center></td></tr></table></th>
                                <th align="center"><h3>Total Fixed Cost Per Month</h3></th>
                                <th align="center"><h3>Rate Per KM</h3></th>
                                <th align="center"><h3>Rate Exceeds Limit KM</h3></th>
                                <th align="center"><h3>Max Allowable KM Per Month</h3></th>

                                <th align="center" colspan="2"><h3>Over Time Cost Per HR</h3><br><table  class="contenthead" border="1"><tr><td width="250px;"><center>Work Days</center></td><td width="212px;"><center>Holidays</center></td></tr></table></th>
                                     <th align="center"><h3>Additional Cost</h3></th>
                                     <th align="center"><h3>Active Status</h3></th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                        <% int index1 = 1;%>
                                          <c:forEach items="${dedicateList}" var="weight">
                                        <%
                                            String classText1 = "";
                                            int oddEven1 = index1 % 2;
                                            if (oddEven1 > 0) {
                                                classText1 = "text2";
                                            } else {
                                                classText1 = "text1";
                                            }
                                        %>
                                            <tr>
                                                <td class="<%=classText1%>"  ><%=index1++%></td>
                                                <td class="<%=classText1%>"   ><c:out value="${weight.vehicleTypeIdDedicate}"/></td>
                                                <td class="<%=classText1%>"   ><c:out value="${weight.vehicleUnitsDedicate}"/></td>
                                                <td class="<%=classText1%>"   ><c:out value="${weight.trailorTypeDedicate}"/></td>
                                                <td class="<%=classText1%>"   ><c:out value="${weight.trailorUnitsDedicate}"/></td>
                                                <td class="<%=classText1%>"   >
                                                    <c:if test="${weight.contractCategory == '1'}" >
                                                   Litre
                                                    </c:if>
                                                    <c:if test="${weight.contractCategory == '2'}" >
                                                   Gallon
                                                    </c:if>
                                                     <c:if test="${weight.contractCategory == '3'}" >
                                                         KiloGram
                                                    </c:if>
                                                </td>
                                                <td class="<%=classText1%>"   ><input type="text" name="fixedCost" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${weight.fixedCost}"/>"/></td>
                                                <td class="<%=classText1%>"   ><input type="text" name="fixedHrs" id="fixedHrs<%=index1%>" value="<c:out value="${weight.fixedHrs}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                <td class="<%=classText1%>"   ><input type="text" name="fixedMin" id="fixedMin<%=index1%>" value="<c:out value="${weight.fixedMin}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                <td class="<%=classText1%>"   ><input type="text" name="totalFixedCost" id="totalFixedCost<%=index1%>" value="<c:out value="${weight.totalFixedCost}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                <td class="<%=classText1%>"   ><input type="text" name="rateCost" id="rateCost<%=index1%>" value="<c:out value="${weight.rateCost}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                <td class="<%=classText1%>"   ><input type="text" name="rateLimit" id="rateLimit<%=index1%>" value="<c:out value="${weight.rateLimit}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                <td class="<%=classText1%>"   ><c:out value="${weight.maxAllowableKM}"/></td>
                                                <td class="<%=classText1%>"   ><input type="text" name="workingDays" id="workingDays<%=index1%>" value="<c:out value="${weight.workingDays}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                <td class="<%=classText1%>"   ><input type="text" name="holidays" id="holidays<%=index1%>" value="<c:out value="${weight.holidays}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                <td class="<%=classText1%>"   ><input type="text" name="addCostDedicate" id="addCostDedicate<%=index1%>" value="<c:out value="${weight.addCostDedicate}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                <td class="<%=classText1%>"   >
                                                  <select name="activeInd" id="activeInd">
                                                    <c:if test="${weight.activeInd == 'Y'}" >
                                                    <option value="Y" selected>Active</option>
                                                    <option value="N">In-Active</option>
                                                    </c:if>
                                                    <c:if test="${weight.activeInd == 'N'}" >
                                                    <option value="N" selected>In-Active</option>
                                                    <option value="Y" >Active</option>
                                                    </c:if>
                                                </select></td>
                                                </td>
                                            </tr>
                                   </c:forEach>
                                    </tbody>
                            </table
                                             </c:if>

                    </div>

<a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
<div> <a  class="pretab" href="#"><input type="button" class="button" value="NewRow" name="AddNewRow" onclick="addNewRow();"/></a>

 </div>
                </div>

           <script>
                        var contain = "";
                        function addNewRow() {

                            var iCnt = 1;
                            var rowCnt = 1;
                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                            contain = $($("#routedeD")).css({
                                padding: '5px', margin: '20px', width: '100%', border: '0px dashed',
                                borderTopColor: '#999', borderBottomColor: '#999',
                                borderLeftColor: '#999', borderRightColor: '#999'
                            });
  $(contain).last().after('<table id="mainTableFullTruck" width="100%"><tr></td>\n\
       <table  class="contenthead" id="routeDetails' + iCnt + '"  border="1">\n\
                            <tr><td>Sno</td>\n\
                            <td>Origin</td>\n\
                            <td>Destination</td>\n\
                            <td>Travel Km</td>\n\
                            <td>Travel Hour</td>\n\
                            <td>Travel Min</td></tr>\n\
                            <tr><td>Route&nbsp; ' + iCnt  + '</td>\n\
                            <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/></td>\n\
                            <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/>\n\</td>\n\
                            <td><input type="text" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            </tr>\n\
                            </table>\n\
                            <table class="contentsub" id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1">\n\
                            <tr><td>Sno</td><td>Vehicle Type</td><td>Vehicle Units</td><td>Trailer Type</td><td>Trailer Units</td><td>Spot Cost Per Trip</td><td>Additional Cost</td></tr>\n\
                            <tr>\n\
                            <td>' + rowCnt + '</td>\n\
                            <td><select ype="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="vehicleUnits" id="vehicleUnits' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><select type="text" name="trailerType" id="trailerType' + iCnt + '"><option value="0">--Select--</option><c:if test="${vehicleList  != null}"><c:forEach items="${vehicleList }" var="veh"><option value="<c:out value="${veh.trailerId}"/>"><c:out value="${veh.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="trailorTypeUnits" id="trailorTypeUnits' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="spotCost" id="spotCost' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            </tr></table>\n\
                            <table border="" width=><tr>\n\
                            <td><input class="button" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ')" />\n\
                            <input class="button" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRow(' + iCnt + rowCnt + ')" /></td>\n\
                    </tr></table></td></tr></table><br><br>');
                            callOriginAjaxFullTruck(iCnt);
                            callDestinationAjaxFullTruck(iCnt);
                            $('#btAdd').click(function () {
                                iCnt = iCnt + 1;
                                $(container).last().after('<table id="mainTableFullTruck" ><tr><td>\
                            <table  class="contenthead" id="routeDetailsFullTruck' + iCnt + '" border="1" width="100%">\n\
                            <tr><td>Sno</td>\n\
                            <td>Origin</td>\n\
                            <td>Destination</td>\n\
                            <td>Travel Km</td>\n\
                            <td>Travel Hour</td>\n\
                            <td>Travel Min</td></tr>\n\
                            <td>' + iCnt  + '</td>\n\
                            <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/></td>\n\
                            <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/>\n\
                             <input type="hidden" name="routeIdFullTruck" id="routeIdFullTruck' + iCnt + '" value="" /></td>\n\
                             <td><input type="text" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            </tr>\n\
                            </table>\n\
                            <table class="contentsub" id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1" width="100%">\n\
                            <tr><td>Sno</td><td>Vehicle Type</td><td>Vehicle Units</td><td>Trailer Type</td><td>Trailer Units</td><td>Spot Cost Per Trip</td><td>Additional Cost</td></tr>\n\
                            <tr>\n\
                            <td>' + rowCnt + '</td>\n\
                            <td><select ype="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="vehicleUnits" id="vehicleUnits' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><select type="text" name="trailerType" id="trailerType' + iCnt + '"><option value="0">--Select--</option><c:if test="${vehicleList  != null}"><c:forEach items="${vehicleList }" var="veh"><option value="<c:out value="${veh.trailerId}"/>"><c:out value="${veh.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="trailorTypeUnits" id="trailorTypeUnits' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="spotCost" id="spotCost' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            </tr></table>\n\
                            <table border="" width=""><tr>\n\
                            <td><input class="button"  type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ')" />\n\
                            <input class="button"  type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove" onclick="deleteRow(' + iCnt + rowCnt + ')"  /></td>\n\
                           </tr></table></td></tr></table><br><br>');
                                callOriginAjaxFullTruck(iCnt);
                                callDestinationAjaxFullTruck(iCnt);
                                $('#mainFullTruck').after(container);
                            });
                            $('#btRemove').click(function () {
                                alert($('#mainTableFullTruck tr').size());
                                if ($(container).size() > 1) {
                                    $(container).last().remove();
                                    iCnt = iCnt - 1;
                                }
                            });
                        }

                        // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                        var divValue, values = '';
                        function GetTextValue() {
                            $(divValue).empty();
                            $(divValue).remove();
                            values = '';
                            $('.input').each(function () {
                                divValue = $(document.createElement('div')).css({
                                    padding: '5px', width: '200px'
                                });
                                values += this.value + '<br />'
                            });
                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                            $('body').append(divValue);
                        }

                                                $(document).ready(function(){
                              $("#mAllow").keyup(function(){
                                  alert($(this).val());
                              });
                          })



                       function addRows(val) {
                            //alert(val);
                            var loadCnt = val;
                        //    alert(loadCnt)
                            //alert("loadCnt");
                            //var loadCnt1 = ve;
                            var routeInfoSize = $('#routeDetails1' + loadCnt + ' tr').size();
                           // alert(routeInfoSize);
                           var routeInfoSizeSub = routeInfoSize - 2;
                            var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                            var routeInfoDetails = "routeDetails1" + loadCnt;

                            $('#routeDetails1' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSizeSub + '</td>\n\
                           <%--   <td><input type="text" name="vehicleTypeIddeDTemp" id="vehicleTypeIddeDTemp' + loadCnt + '" /></td>--%>\n\
                           <td><select ype="text" name="vehicleTypeIdDedicate" id="vehicleTypeIdDedicate' + loadCnt + '" onchange="onSelectVal(this.value,'+loadCnt+');"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                           <td><input type="text" name="vehicleUnitsDedicate" id="vehicleUnitsDedicate' + loadCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                           <td><select ype="text" name="trailorTypeDedicate" id="trailorTypeDedicate' + loadCnt + '"><option value="0">--Select--</option><c:if test="${vehicleList  != null}"><c:forEach items="${vehicleList }" var="veh"><option value="<c:out value="${veh.trailerId}"/>"><c:out value="${veh.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                           <td><input type="text" name="trailorUnitsDedicate" id="trailorUnitsDedicate' + loadCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><select ype="text" name="contractCategory" id="contractCategory' + loadCnt + '"><option value="">--Select--</option><option value="1">Fixed</option><option value="2">Actual</option></select></td>\n\
                           <td><input type="text" name="fixedCost" id="fixedCost' + loadCnt+ '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                           <td width="10px;"><select ype="text" name="fixedHrs" id="fixedHrs' + loadCnt + '" class="textbox">\n\
                                      <option value="00">00</option>\n\
                                    <option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option></select>\n\
                           </td>\n\
                            <td width="260px;"><select ype="text" name="fixedMin" id="fixedMin' + loadCnt + '" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>\n\
                           </td>\n\
                            <td><input type="text" name="totalFixedCost" id="totalFixedCost' + loadCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="rateCost" id="rateCost' + loadCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="rateLimit" id="rateLimit' + loadCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="maxAllowableKM" id="maxAllowableKM"' + loadCnt + '" value="" onkeyup="sLIMIT();" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="workingDays" id="workingDays' + loadCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="holidays" id="holidays' + loadCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="addCostDedicate" id="addCostDedicate' + loadCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><a href="/throttle/configureVehicleTrailerPage.do?vehicleTypeIddeD='+$("#vehicleTypeIddeDTemp"+loadCnt).get()+'" >Configure</a></td>\n\
                          </tr>');
       // alert("loadCnt = "+loadCnt)
                            loadCnt++;
       //   alert("loadCnt = +"+loadCnt)
                        }
                        function onSelectVal(val,countVal){
                            document.getElementById("vehicleTypeIddeDTemp"+countVal).value = val;

                        }


                    function deleteRows(val) {
                            var loadCnt = val;
                       //     alert(loadCnt);
                            var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                            var routeInfoDetails = "routeDetails1" + loadCnt;
                           // alert(routeInfoDetails);
                            if ($('#routeDetails1' + loadCnt + ' tr').size() > 2) {
                                $('#routeDetails1' + loadCnt + ' tr').last().remove();
                                loadCnt = loadCnt - 1;
                            } else {
                                alert('One row should be present in table');
                            }
                        }



                            </script>

                            <script>
                                function callOriginAjaxdeD(val) {
                                    // Use the .autocomplete() method to compile the list based on input from user
                                    //alert(val);
                                    var pointNameId = 'originNamedeD' + val;
                                    var pointIdId = 'originIddeD' + val;
                                    var desPointName = 'destinationNamedeD' + val;


                                    //alert(prevPointId);
                                    $('#' + pointNameId).autocomplete({
                                        source: function (request, response) {
                                            $.ajax({
                                                url: "/throttle/getTruckCityList.do",
                                                dataType: "json",
                                                data: {
                                                    cityName: request.term,
                                                    textBox: 1
                                                },
                                                success: function (data, textStatus, jqXHR) {
                                                    var items = data;
                                                    response(items);
                                                },
                                                error: function (data, type) {

                                                    //console.log(type);
                                                }
                                            });
                                        },
                                        minLength: 1,
                                        select: function (event, ui) {
                                            var value = ui.item.Name;
                                            var id = ui.item.Id;
                                            //alert(id+" : "+value);
                                            $('#' + pointIdId).val(id);
                                            $('#' + pointNameId).val(value);
                                            $('#' + desPointName).focus();
                                            //validateRoute(val,value);

                                            return false;
                                        }

                                        // Format the list menu output of the autocomplete
                                    }).data("autocomplete")._renderItem = function (ul, item) {
                                        //alert(item);
                                        var itemVal = item.Name;
                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                        return $("<li></li>")
                                                .data("item.autocomplete", item)
                                                .append("<a>" + itemVal + "</a>")
                                                .appendTo(ul);
                                    };


                                }

                                function callDestinationAjaxdeD(val) {
                                    // Use the .autocomplete() method to compile the list based on input from user
                                    //alert(val);
                                    var pointNameId = 'destinationNamedeD' + val;
                                    var pointIdId = 'destinationIddeD' + val;
                                    var originPointId = 'originIddeD' + val;
                                    var truckRouteId = 'routeIddeD' + val;
                                    var travelKm = 'travelKmdeD' + val;
                                    var travelHour = 'travelHourdeD' + val;
                                    var travelMinute = 'travelMinutedeD' + val;

                                    //alert(prevPointId);
                                    $('#' + pointNameId).autocomplete({
                                        source: function (request, response) {
                                            $.ajax({
                                                url: "/throttle/getTruckCityList.do",
                                                dataType: "json",
                                                data: {
                                                    cityName: request.term,
                                                    originCityId: $("#" + originPointId).val(),
                                                    textBox: 1
                                                },
                                                success: function (data, textStatus, jqXHR) {
                                                    var items = data;
                                                    response(items);
                                                },
                                                error: function (data, type) {

                                                    //console.log(type);
                                                }
                                            });
                                        },
                                        minLength: 1,
                                        select: function (event, ui) {
                                            var value = ui.item.Name;
                                            var id = ui.item.Id;
                                            //alert(id+" : "+value);
                                            $('#' + pointIdId).val(id);
                                            $('#' + pointNameId).val(value);
                                            $('#' + travelKm).val(ui.item.TravelKm);
                                            $('#' + travelHour).val(ui.item.TravelHour);
                                            $('#' + travelMinute).val(ui.item.TravelMinute);
                                            $('#' + truckRouteId).val(ui.item.RouteId);
                                            //validateRoute(val,value);

                                            return false;
                                        }

                                        // Format the list menu output of the autocomplete
                                    }).data("autocomplete")._renderItem = function (ul, item) {
                                        //alert(item);
                                        var itemVal = item.Name;
                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                        return $("<li></li>")
                                                .data("item.autocomplete", item)
                                                .append("<a>" + itemVal + "</a>")
                                                .appendTo(ul);
                                    };


                                }

                            </script></div>
                           <div>
           <div id="fullTruck">
                        <div id="routeFullTruck">
                             <c:if test="${hireList != null}">
                            <table align="center" border="0" id="table" class="sortable" style="width:1000px;" >
                                <thead>
                                    <tr>
                                        <th align="center"><h3>S.No</h3></th>
                                <th align="center"><h3>Vehicle Type</h3></th>
                                <th align="center"><h3>Vehicle Units<br><br></h3></th>
                                <th align="center"><h3>Origin</h3></th>
                                <th align="center"><h3>Destination</h3></th>

                                <th align="center"><h3>Trailer Type</h3></th>
                                <th align="center"><h3>Trailer Units</h3></th>
                                <th align="center"><h3>Spot Cost Trip</h3></th>
                                <th align="center"><h3>Additional Cost</h3></th>
                                <th align="center"><h3>Travel Kms</h3></th>
                                <th align="center"><h3>Travel Hours</h3></th>
                                <th align="center"><h3>Travel Minutes</h3></th>
                                <th align="center"><h3>Active Status</h3></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <% int index = 1;%>
                                  <c:forEach items="${hireList}" var="route">
                                        <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                        %>
                                        <tr>
                                            <td class="<%=classText%>"><%=index++%></td>
                                            <td class="<%=classText%>"><c:out value="${route.vehicleTypeId}"/></td>
                                            <td class="<%=classText%>"><c:out value="${route.vehicleUnits}"/></td>
                                            <td class="<%=classText%>"><c:out value="${route.originNameFullTruck}"/></td>
                                            <td class="<%=classText%>"><c:out value="${route.destinationNameFullTruck}"/></td>

                                            <td class="<%=classText%>"><c:out value="${route.trailerType}"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${route.trailorTypeUnits}"/></td>
                                            <td class="<%=classText%>"   ><input type="text" name="spotCost" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${route.spotCost}"/>"/></td>
                                            <td class="<%=classText%>"   ><input type="text" name="additionalCost" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${route.additionalCost}"/>"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${route.travelKmFullTruck}"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${route.travelHourFullTruck}"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${route.travelMinuteFullTruck}"/></td>
                                            <td class="<%=classText%>"   >
                                               <select name="activeInd" id="activeInd">
                                                    <c:if test="${route.activeInd == 'Y'}" >
                                                    <option value="Y" selected>Active</option>
                                                    <option value="N">In-Active</option>
                                                    </c:if>
                                                    <c:if test="${route.activeInd == 'N'}" >
                                                    <option value="N" selected>In-Active</option>
                                                    <option value="Y" >Active</option>
                                                    </c:if>
                                                </select>
                                            </td>
                                        </tr>
                                     </c:forEach>
                                </tbody>
                            </table>
                                     </c:if>
                    </div>



                       <a  class="pretab" href="#"><input type="button" class="button" value="Previous" name="Previous" /></a>
                        <div> <a  class="nexttab" href="#"><input type="button" class="button" value="AddNewRww" name="AddNewRow" onclick="addNewRowRate();"/></a>
                            <a  href="#"><input type="button" class="button" value="Save" onclick="saveVendorContract();" /></a>
                        </div>

                </div>
                    <script>
                        var container = "";
                       function addNewRowRate() {
                            var iCnt = 1;
                            var rowCnt = 1;
                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                            container = $($("#routeFullTruck")).css({
                                padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                borderTopColor: '#999', borderBottomColor: '#999',
                                borderLeftColor: '#999', borderRightColor: '#999'
                            });

                            $(container).last().after('<table id="mainTableFullTruck" width="100%"><tr></td>\n\
                            <table  class="contenthead" id="routeDetails' + iCnt + '"  border="1">\n\
                            <tr><td>Sno</td>\n\
                            <td>Origin</td>\n\
                            <td>Destination</td>\n\
                            <td>Travel Km</td>\n\
                            <td>Travel Hour</td>\n\
                            <td>Travel Min</td></tr>\n\
                            <tr><td>Route&nbsp; ' + iCnt  + '</td>\n\
                            <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/></td>\n\
                            <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/>\n\</td>\n\
                            <td><input type="text" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            </tr>\n\
                            </table>\n\
                            <table class="contentsub" id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1">\n\
                            <tr><td>Sno</td><td>Vehicle Type</td><td>Vehicle Units</td><td>Trailer Type</td><td>Trailer Units</td><td>Spot Cost Per Trip</td><td>Additional Cost</td></tr>\n\
                            <tr>\n\
                            <td>' + rowCnt + '</td>\n\
                            <td><select ype="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="vehicleUnits" id="vehicleUnits' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><select type="text" name="trailerType" id="trailerType' + iCnt + '"><option value="0">--Select--</option><c:if test="${vehicleList  != null}"><c:forEach items="${vehicleList }" var="veh"><option value="<c:out value="${veh.trailerId}"/>"><c:out value="${veh.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="trailorTypeUnits" id="trailorTypeUnits' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="spotCost" id="spotCost' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            </tr></table>\n\
                            <table border="" width=><tr>\n\
                            <td><input class="button" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ')" />\n\
                            <input class="button" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRow(' + iCnt + rowCnt + ')" /></td>\n\
                    </tr></table></td></tr></table><br><br>');
                            callOriginAjaxFullTruck(iCnt);
                            callDestinationAjaxFullTruck(iCnt);
                            $('#btAdd').click(function () {
                                iCnt = iCnt + 1;
                                $(container).last().after('<table id="mainTableFullTruck" ><tr><td>\
                            <table  class="contenthead" id="routeDetailsFullTruck' + iCnt + '" border="1" width="100%">\n\
                            <tr><td>Sno</td>\n\
                            <td>Origin</td>\n\
                            <td>Destination</td>\n\
                            <td>Travel Km</td>\n\
                            <td>Travel Hour</td>\n\
                            <td>Travel Min</td></tr>\n\
                            <td>' + iCnt  + '</td>\n\
                            <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/></td>\n\
                            <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/>\n\
                             <input type="hidden" name="routeIdFullTruck" id="routeIdFullTruck' + iCnt + '" value="" /></td>\n\
                             <td><input type="text" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            </tr>\n\
                            </table>\n\
                            <table class="contentsub" id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1" width="100%">\n\
                            <tr><td>Sno</td><td>Vehicle Type</td><td>Vehicle Units</td><td>Trailer Type</td><td>Trailer Units</td><td>Spot Cost Per Trip</td><td>Additional Cost</td></tr>\n\
                            <tr>\n\
                            <td>' + rowCnt + '</td>\n\
                            <td><select ype="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="vehicleUnits" id="vehicleUnits' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><select type="text" name="trailerType" id="trailerType' + iCnt + '"><option value="0">--Select--</option><c:if test="${vehicleList  != null}"><c:forEach items="${vehicleList }" var="veh"><option value="<c:out value="${veh.trailerId}"/>"><c:out value="${veh.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="trailorTypeUnits" id="trailorTypeUnits' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="spotCost" id="spotCost' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            </tr></table>\n\
                            <table border="" width=""><tr>\n\
                            <td><input class="button"  type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ')" />\n\
                            <input class="button"  type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove" onclick="deleteRow(' + iCnt + rowCnt + ')"  /></td>\n\
                           </tr></table></td></tr></table><br><br>');
                                callOriginAjaxFullTruck(iCnt);
                                callDestinationAjaxFullTruck(iCnt);
                                $('#mainFullTruck').after(container);
                            });
                            $('#btRemove').click(function () {
                                alert($('#mainTableFullTruck tr').size());
                                if ($(container).size() > 1) {
                                    $(container).last().remove();
                                    iCnt = iCnt - 1;
                                }
                            });
}
                        // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                        var divValue, values = '';
                        function GetTextValue() {
                            $(divValue).empty();
                            $(divValue).remove();
                            values = '';
                            $('.input').each(function () {
                                divValue = $(document.createElement('div')).css({
                                    padding: '5px', width: '200px'
                                });
                                values += this.value + '<br />'
                            });
                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                            $('body').append(divValue);
                        }

                       function addRow(val,v1) {
                            //alert(val);
                            var loadCnt = val;
                            var loadCnt1 = v1;
                            var routeInfoSize = $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size();
                            //alert(routeInfoSize);
                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                            $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSize + '</td><td><select type="text" name="vehicleTypeId" id="vehicleTypeId' + loadCnt + '"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td><td><input type="text" name="vehicleUnits" id="vehicleUnits' + loadCnt + '" value=""/></td><td><select type="text" name="trailerType" id="trailerType' + loadCnt + '"><option value="0">--Select--</option><c:if test="${vehicleList  != null}"><c:forEach items="${vehicleList }" var="veh"><option value="<c:out value="${veh.trailerId}"/>"><c:out value="${veh.seatCapacity}"/></option></c:forEach></c:if></select></td><td><input type="text" name="trailorTypeUnits" id="trailorTypeUnits' + loadCnt + '" value=""/></td><td><input type="text" name="spotCost" id="spotCost' + loadCnt + '" value=""/></td><td><input type="text" name="additionalCost" id="additionalCost' + loadCnt + '" value="" /></td></tr>');
                            loadCnt++;
                        }
                        function deleteRow(val) {
                            var loadCnt = val;

                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                            if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                loadCnt = loadCnt - 1;
                            } else {
                                alert('One row should be present in table');
                            }
                        }
                            </script>

                            <script>
                                function callOriginAjaxFullTruck(val) {
                                    // Use the .autocomplete() method to compile the list based on input from user
                                    //alert(val);
                             //       var pointNameId = 'originNameFullTruck' + val;
                                    var pointNameId = 'originNameFullTruck' + val;
                                    var pointId = 'originIdFullTruck' + val;
                                    var desPointName = 'destinationNameFullTruck' + val;


                                    //alert(prevPointId);
                                    $('#' + pointNameId).autocomplete({
                                        source: function (request, response) {
                                            $.ajax({
                                                url: "/throttle/getCityFromList.do",
                                                dataType: "json",
                                                data: {
//                                                    cityName: request.term,
                                                    cityName: $("#" + pointNameId).val(),
//                                                    cityName: $("#" + pointNameId).val(),
                                                    textBox: 1
                                                },
                                                success: function (data, textStatus, jqXHR) {
                                                    var items = data;
                                                    response(items);
                                                    //alert("asdfasdf")
                                                },
                                                error: function (data, type) {
                                                }
                                            });
                                        },
                                        minLength: 1,
                                        select: function (event, ui) {
                                            var value = ui.item.Value;
                                            //alert("value ="+value);
                                            var temp = value.split("-");
                                            var textId = temp[0];
                                            var textName = temp[1];
                                            var id = ui.item.Id;
                                            //alert("id ="+id);
                                            //alert(id+" : "+value);
                                            $('#' + pointId).val(textId);
                                            $('#' + pointNameId).val(textName);
                                            $('#' + desPointName).focus();
                                            //validateRoute(val,value);

                                            return false;
                                        }

                                        // Format the list menu output of the autocomplete
                                    }).data("autocomplete")._renderItem = function (ul, item) {
                                        //alert(item);
                                        //var temp [] = "";
                                        var itemVal = item.Value;
                                        var temp = itemVal.split("-");
                                        var textId = temp[0];
                                        var t2 = temp[1];
//                                        alert("t1 = "+t1)
//                                        alert("t2 = "+t2)
                                        //alert("itemVal = "+itemVal)
                                        t2 = '<font color="green">' + t2 + '</font>';
                                        return $("<li></li>")
                                                .data("item.autocomplete", item)
                                                .append("<a>" + t2 + "</a>")
                                                .appendTo(ul);
                                    };


                                }


//
//
//                                }
             function callDestinationAjaxFullTruck(val) {
                                    // Use the .autocomplete() method to compile the list based on input from user
                                    //alert(val);
                                    var pointNameId = 'destinationNameFullTruck' + val;
                                    var pointIdId = 'destinationIdFullTruck' + val;
                                    var originPointId = 'originIdFullTruck' + val;
                                    var truckRouteId = 'routeIdFullTruck' + val;
                                    var travelKm = 'travelKmFullTruck' + val;
                                    var travelHour = 'travelHourFullTruck' + val;
                                    var travelMinute = 'travelMinuteFullTruck' + val;


//                                    if($("#" + originPointId).val() == ""){
//                                        alert("id inside condition ==")
//                                    }else{
//                                        alert("else inside condition ==")
//                                    }
                                    $('#' + pointNameId).autocomplete({
                                        source: function (request, response) {
                                            $.ajax({
                                                url: "/throttle/getCityToList.do",
                                                dataType: "json",
                                                data: {
                                                    cityName: $("#" + pointNameId).val(),
                                                    originCityId: $("#" + originPointId).val(),
                                                    textBox: 1
                                                },
                                                success: function (data, textStatus, jqXHR) {
                                                    var items = data;
                                                    response(items);
                                                },
                                                error: function (data, type) {

                                                    //console.log(type);
                                                }
                                            });
                                        },
                                        minLength: 1,
                                        select: function (event, ui) {
                                            var value = ui.item.cityName;
                                            var id = ui.item.Id;
                                            //alert(id+" : "+value);
                                            $('#' + pointIdId).val(id);
                                            $('#' + pointNameId).val(value);
                                            $('#' + travelKm).val(ui.item.Distance);
                                            $('#' + travelHour).val(ui.item.TravelHour);
                                            $('#' + travelMinute).val(ui.item.TravelMinute);
                                            $('#' + truckRouteId).val(ui.item.RouteId);
                                            //validateRoute(val,value);

                                            return false;
                                        }

                                        // Format the list menu output of the autocomplete
                                    }).data("autocomplete")._renderItem = function (ul, item) {
                                        //alert(item);
                                        var itemVal = item.cityName;
                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                        return $("<li></li>")
                                                .data("item.autocomplete", item)
                                                .append("<a>" + itemVal + "</a>")
                                                .appendTo(ul);
                                    };
                                }
                            </script>
                           </div>

                <script>

                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                    $(".pretab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected - 1);
                    });

                </script>



            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
