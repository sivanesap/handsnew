<%-- 
    Document   : newjsp
    Created on : May 3, 2013, 4:35:31 PM
    Author     : Entitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>Multiple selections</title>
<script type="text/javascript">
// For: http://www.webdeveloper.com/forum/newreply.php?do=postreply&t=240209

function pickAll(ndx) {
  var sel = document.getElementsByTagName('select');
  for (var i=0; i<sel.length; i++) {
    sel[i].selectedIndex = ndx;
  }
}
</script>
</head>
<body>
<select id="SBox0" onclick="pickAll(this.selectedIndex)">
<option>a</option>
<option>b</option>
<option>c</option>
</select>
<br>

<select id="SBox1" onclick="pickAll(this.selectedIndex)">
<option>a</option>
<option>b</option>
<option>c</option>
</select>
<br>

<select id="SBox2" onclick="pickAll(this.selectedIndex)">
<option>a</option>
<option>b</option>
<option>c</option>
</select>
</body>
</html>