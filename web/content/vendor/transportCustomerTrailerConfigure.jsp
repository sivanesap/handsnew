<%-- 
    Document   : configureVehicleTrailerPage
    Created on : Apr 27, 2015, 10:27:19 AM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib uri='http://java.sun.com/jstl/core' prefix='c'%>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<!--<script language="javascript" type="text/javascript" src="/throttle/js/ajaxFunction.js"></script>-->

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });</script>

<script>
    function savePage(val) {
//        alert("Val"+val);
// document.getElementById("trailerNo"+val).value="";
        document.vehicleVendorContract.action = "/throttle/saveTrailerConf.do";
        document.vehicleVendorContract.submit();

    }

</script>
<script>
    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }


    function extractNumber(obj, decimalPlaces, allowNegative)
    {
        var temp = obj.value;

        // avoid changing things if already formatted correctly
        var reg0Str = '[0-9]*';
        if (decimalPlaces > 0) {
            reg0Str += '\\.?[0-9]{0,' + decimalPlaces + '}';
        } else if (decimalPlaces < 0) {
            reg0Str += '\\.?[0-9]*';
        }
        reg0Str = allowNegative ? '^-?' + reg0Str : '^' + reg0Str;
        reg0Str = reg0Str + '$';
        var reg0 = new RegExp(reg0Str);
        if (reg0.test(temp))
            return true;

        // first replace all non numbers
        var reg1Str = '[^0-9' + (decimalPlaces != 0 ? '.' : '') + (allowNegative ? '-' : '') + ']';
        var reg1 = new RegExp(reg1Str, 'g');
        temp = temp.replace(reg1, '');

        if (allowNegative) {
            // replace extra negative
            var hasNegative = temp.length > 0 && temp.charAt(0) == '-';
            var reg2 = /-/g;
            temp = temp.replace(reg2, '');
            if (hasNegative)
                temp = '-' + temp;
        }

        if (decimalPlaces != 0) {
            var reg3 = /\./g;
            var reg3Array = reg3.exec(temp);
            if (reg3Array != null) {
                // keep only first occurrence of .
                //  and the number of places specified by decimalPlaces or the entire string if decimalPlaces < 0
                var reg3Right = temp.substring(reg3Array.index + reg3Array[0].length);
                reg3Right = reg3Right.replace(reg3, '');
                reg3Right = decimalPlaces > 0 ? reg3Right.substring(0, decimalPlaces) : reg3Right;
                temp = temp.substring(0, reg3Array.index) + '.' + reg3Right;
            }
        }

        obj.value = temp;
    }
    function blockNonNumbers(obj, e, allowDecimal, allowNegative)
    {
        var key;
        var isCtrl = false;
        var keychar;
        var reg;

        if (window.event) {
            key = e.keyCode;
            isCtrl = window.event.ctrlKey
        }
        else if (e.which) {
            key = e.which;
            isCtrl = e.ctrlKey;
        }

        if (isNaN(key))
            return true;

        keychar = String.fromCharCode(key);

        // check for backspace or delete, or if Ctrl was pressed
        if (key == 8 || isCtrl)
        {
            return true;
        }

        reg = /\d/;
        var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
        var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;

        return isFirstN || isFirstD || reg.test(keychar);
    }


</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script>
    function checkTrailerAvalible() {
        
        var trailorUnits =<c:out value="${trailerUnit}"/>;
//        alert("vehicleUnits ==== " + vehicleUnits);
//        alert("trailorUnits ==== " + trailorUnits);
        
        if (trailorUnits == "" && trailorUnits == "0") {
            $("#trailers1").hide();
            $("#trailers").hide();
            $("#tabs").tabs("select", 2);
            $("ul#myTab li:nth-child(1)").addClass("active");
            $("#vehicles").addClass("active");
        }
    }
    function checkTrailerNos(val) {
//        alert("Sno"+val);
        var sno = val - 1;
        var trailerNoOld = document.getElementById("trailerNo" + sno).value;
        var trailerNoNew = document.getElementById("trailerNo" + val).value;
//        alert("trailerNoOld"+trailerNoOld);
//        alert("trailerNoNew"+trailerNoNew);
        if (trailerNoNew == trailerNoOld) {
            document.getElementById("checkTrailerNo").value = "1";
            document.getElementById("trailerNo" + val).value = "";
            document.getElementById("trailerNo" + val).focus();
            alert("Trailer No's already Exit's");
        } else {
            document.getElementById("checkTrailerNo").value = "0";
        }
    }
    
</script>


<html>
    <body onload="checkTrailerAvalible();">

        <style>
            body {
                font:13px verdana;
                font-weight:normal;
            }
        </style>



        <form name="vehicleVendorContract" method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <input type="hidden" name="ownership" id="ownership" value="2"/>
            <input type="hidden" name="custId" id="custId" value="<c:out value="${custId}"/>" class="textbox">
            <input type="hidden" name="regNoCheck" value='exists' >
            <input type="hidden" name="Status" id="Status" value='' >
            <input type="hidden" name="contractId" id="contractId" value='<c:out value="${contractId}"/>' >
            <input type="hidden" name="trailerStatus" id="trailerStatus" value='' >
            <input type="hidden" name="checkTrailerNo" id="checkTrailerNo" value='' >
            <input type="hidden" name="checkVehicleRegNo" id="checkVehicleRegNo" value='' >
            <div id="tabs">
                
<input type="hidden" name="trailerTypeId" id="trailerTypeId" value="<c:out value="${trailerTypeId}"/>" class="textbox">
                <div id="trailers">

                    <script>
                        var container = "";
                        $(document).ready(function() {
                            var iCnt = 1;
                            var rowCnt = 1;
                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                            container = $($("#routeFullTruck")).css({
                                padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                borderTopColor: '#999', borderBottomColor: '#999',
                                borderLeftColor: '#999', borderRightColor: '#999'
                            });
                            $(container).last().after('<table   id="routeDetails' + iCnt + '"  border="1">\n\
                            <tr class="contenthead"><td>TrailerType</td><td>Trailer Units</td>\n\
                            </tr>\n\
                            <tr><td><c:out value="${trailerType}"/></td>\n\
                            <td><c:out value="${trailerUnit}"/></td>\n\
                            </tr></td>\n\
                          </tr></tr>\n\
                         </table>\n\
                            <table class="contentsub" id="routeInfoDetailsFullTrucks' + iCnt + '" border="1"  width="73%">\n\
                            <tr>\n\
                            <td>Sno</td>\n\
                            <td>TrailerNo</td>\n\
                            <td>Agreed Date</td>\n\
                            <td>Remarks</td>\n\
                            </tr>\n\
                            <tr><td>' + rowCnt + '</td>\n\
                            <input type="hidden" name="trailerId" id="trailerId' + iCnt + '" value=""/>\n\
                            <td><input type="hidden" name="trailerTypeIdcheck" id="trailerTypeIdcheck' + iCnt + '" value="<c:out value="${trailerTypeId}"/>" class="textbox">\n\
                             <input type="text" name="trailerNo" id="trailerNo' + iCnt + '" value="" onchange="getTrailerDetails(' + iCnt + ');checkTrailerNos(' + iCnt + ');"/></td>\n\
                           <td><input type="text" name="agreedDate" id="agreedDate' + iCnt + '" class="datepicker" /></td>\n\
                             <td><input type="text" name="trailerRemarks" id="trailerRemarks' + iCnt + '" value="" /></td>\n\
                           <table><tr>\n\
                            <td><input class="button" type="button" name="addRouteDetailsFullTrucks" id="addRouteDetailsFullTrucks' + iCnt + rowCnt + '" value="Add" onclick="addRows(' + (parseFloat(iCnt) + parseFloat(rowCnt)) + ',' + iCnt + ',' +<c:out value="${trailerUnit}"/> + ')" />\n\
                            <input class="button" type="button" name="removeRouteDetailsFullTrucks" id="removeRouteDetailsFullTrucks' + iCnt + '" value="Remove"  onclick="deleteRows(' + iCnt + ')" /></td>\n\
                            </tr></table></td></tr></table><br><br>');autoFill(iCnt);

                            $('#btAdd').click(function() {
//                                if(<c:out value="${trailerUnit}"/> = iCnt){

                                iCnt = iCnt + 1;
                                $(container).last().after('<table id="mainTableFullTruck" ><tr><td>\
                              </tr></table></td></tr></table><br><br>');

                                $('#mainFullTruck').after(container);
//                                }else{
//                                    alert("test 421525");
//                                }
                            });
                            $('#btRemove').click(function() {
//                                alert($('#mainTableFullTruck tr').size());
                                if ($(container).size() > 1) {
                                    $(container).last().remove();
                                    iCnt = iCnt - 1;
                                }
                            });
                        });

                        // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                        var divValue, values = '';
                        function GetTextValue() {
                            $(divValue).empty();
                            $(divValue).remove();
                            values = '';
                            $('.input').each(function() {
                                divValue = $(document.createElement('div')).css({
                                    padding: '5px', width: '200px'
                                });
                                values += this.value + '<br />'
                            });
                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                            $('body').append(divValue);
                        }

                        function addRows(val, valu, trailerUnit) {
//                          alert("trailerUnit" + trailerUnit);
//                          alert(val);
                            var loadCnt = val;
                            var count12 = <c:out value="${trailerUnit}"/>;

                            //  alert(count12);
                            var routeInfoSize = $('#routeInfoDetailsFullTrucks' + valu + ' tr').size();
                            //  alert(routeInfoSize);
                            if (parseInt(count12) >= parseInt(routeInfoSize)) {
                                var addRouteDetails = "addRouteDetailsFullTrucks" + valu;
                                var routeInfoDetails = "routeInfoDetailsFullTrucks" + valu;
                                $('#routeInfoDetailsFullTrucks' + valu + ' tr').last().after('<tr><td>' + routeInfoSize + '</td><td><input type="hidden" name="trailerType" id="trailerType' + loadCnt + '" value="<c:out value="${trailerType}"/>"\n\
                                 <input type="hidden" name="trailerId" id="trailerId' + loadCnt + '" value=""/>\n\
                            <input type="hidden" name="trailerTypeIdcheck" id="trailerTypeIdcheck' + loadCnt + '" value="<c:out value="${trailerTypeId}"/>" class="textbox">\n\
                             <input type="text" name="trailerNo" id="trailerNo' + loadCnt + '" value=""  onchange="getTrailerDetails(' + loadCnt + ');checkTrailerNos(' + loadCnt + ');"/></td>\n\
                            <td><input type="text" name="agreedDate" id="agreedDate' + loadCnt + '" class="datepicker" /></td>\n\
                            <td><input type="text" name="trailerRemarks" id="trailerRemarks' + loadCnt + '" value="" /></td></tr>');autoFill(loadCnt);
                                loadCnt++;
                            }
                        }
                        function deleteRows(val) {
                            var loadCnt = val;
                            // alert(loadCnt);
                            var addRouteDetails = "addRouteDetailsFullTrucks" + loadCnt;
                            var routeInfoDetails = "routeInfoDetailsFullTrucks" + loadCnt;
                            if ($('#routeInfoDetailsFullTrucks' + loadCnt + ' tr').size() > 2) {
                                $('#routeInfoDetailsFullTrucks' + loadCnt + ' tr').last().remove();
                                loadCnt = loadCnt - 1;
                            } else {
                                alert('One row should be present in table');
                            }
                        }


                        var httpRequest;
                        function getTrailerDetails(val) {
//                alert("entered= "+val);
//                alert(document.vehicleVendorContract.vehicleRegNo.value);
                            var trailerNo = document.getElementById("trailerNo" + val).value;
                            if (trailerNo != '') {
                                var url = '/throttle/checkTrailerExists.do?trailerNo=' + trailerNo;
                                //alert("url----"+url);
                                if (window.ActiveXObject)
                                {
                                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                }
                                else if (window.XMLHttpRequest)
                                {
                                    httpRequest = new XMLHttpRequest();
                                }
                                httpRequest.open("POST", url, true);
                                httpRequest.onreadystatechange = function() {
                                    go2(val);
//                        alert("url----"+url);
                                };
                                httpRequest.send(null);
                            }
                        }


                        function go2(val) {
//                        alert("hi");
                            if (httpRequest.readyState == 4) {
                                if (httpRequest.status == 200) {
                                    var response = httpRequest.responseText;
                                    var temp = response.split('-');
//                         alert("hi111");
                                    if (response != "") {
                                        alert('Trailer Already Exists');
                                        document.getElementById("trailerStatus").innerHTML = httpRequest.responseText.valueOf() + " Already Exists";
                                        document.getElementById("trailerNo" + val).value = "";
                                        document.getElementById("trailerNo" + val).focus();
                                        document.getElementById("trailerNo" + val).select();
                                        document.vehicleVendorContract.regNoCheck.value = 'exists';
                                    } else
                                    {
                                        document.vehicleVendorContract.regNoCheck.value = 'Notexists';
                                        document.getElementById("Status").innerHTML = "";
                                        document.getElementById("trailerStatus").innerHTML = "";
                                    }
                                }
                            }
                        }
                                </script>
      <script>
         function autoFill(sno){



         $(document).ready(function () {

         //var sno=1;
                // Use the .autocomplete() method to compile the list based on input from user
                $('#trailerNo'+sno).autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "/throttle/getTrailerNoForTransportCustomer.do",
                            dataType: "json",
                            data: {
                                trailerNo: (request.term).trim(),
                                trailerTypeId: 1
                            },
                            success: function (data, textStatus, jqXHR) {
//                                alert(data);
                                var items = data;
                                response(items);
                            },
                            error: function (data, type) {
                                //console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function (event, ui) {
                        var value = ui.item.Name;

                        var tmp = value.split('-');

                        $('#trailerId'+sno).val(tmp[0]);
                        $('#trailerNo'+sno).val(tmp[1]);
                        

                        return false;
                    }
                    // Format the list menu output of the autocomplete
                }).data("autocomplete")._renderItem = function (ul, item) {
                    //alert(item);
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            //.append( "<a>"+ item.Name + "</a>" )
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

            });


         }




                                </script>


                                <div id="routeFullTruck">

                                </div>
<!--                               <a  class="pretab" href=""><input type="button" class="button" value="Previous" name="Previous" /></a>-->
                                <!--                            <a  href="#vehicles"><input type="button" class="button" value="Save" onclick="saveVendorContract();" /></a>-->

                            </div>

                            <!--<script>
                                function saveVendorContract(){
                                    document.customerContract.action = "/throttle/saveVehicleVendorContract.do";
                                    document.customerContract.submit();
                                }
                             </script>-->
                            


                <script>

                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                    $(".pretab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected - 1);
                    });
                </script>

            </div>
            <center>
                <a  href=""><input type="button" class="button" value="Save" onclick="savePage(this.val);" /></a>
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>