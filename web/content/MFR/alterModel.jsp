
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
 
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">

function submitpage(value)
{
    
var checValidate = selectedItemValidation();    
    if(checValidate !='fail' ){    
    document.modify.action='/throttle/modifyModel.do';
    document.modify.submit();
    }
}

function setSelectbox(i)
{
var selected=document.getElementsByName("selectedIndex") ;
selected[i].checked = 1;
}

function selectedItemValidation(){
var index = document.getElementsByName("selectedIndex");
var mfrNames = document.getElementsByName("mfrIds");
var typeNames = document.getElementsByName("typeIds");
var fuelNames = document.getElementsByName("fuelIds");
var modelNames = document.getElementsByName("modelNames");
var desc = document.getElementsByName("descriptions");
var chec=0;

       
for(var i=0;(i<index.length && index.length!=0);i++){
        if(index[i].checked){
        chec++;
        if( mfrNames[i].value=='0' ){
            alert("Please Select Manufacturer");
            return 'fail';
        }else if(textValidation(desc[i],"Desription")){
            return 'fail';
        }else if( typeNames[i].value=='0' ){
            alert("Please Select Vehicle Type");
            return 'fail';               
        }else if( fuelNames[i].value=='0' ){
            alert("Please Select Fuel Type");
            return 'fail';                             
        }else if(textValidation(modelNames[i],"Model")){
            return 'fail';             
        }
        }
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
return 'fail';
//desigName[0].focus();
//break;
}
//document.modify.action='/throttle/modifyModel.do';
//document.modify.submit();
return 'pass';
}

function isChar(s){
if(!(/^-?\d+$/.test(s))){
return false;
}
return true;
}
</script>

<div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.Model"  text="Model"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="trucks.label.FleetName"  text="Fleet"/></a></li>
          <li class="active"><spring:message code="stores.label.Model"  text="Model"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
<body >
<form method="post" name="modify"> 

<%@ include file="/content/common/message.jsp" %>
</td></tr></table>

 <% int index = 0; %>        

<table class="table table-info mb30 table-hover">

<c:if test = "${ModelList != null}" > 
 
<thead>
<tr> 	 		  	
   
<th height="30"><div><spring:message code="trucks.label.SNo"  text="default text"/></div></th>
<th height="30"><div><spring:message code="trucks.label.MFR"  text="default text"/> </div></th>
<th height="30"><div><spring:message code="trucks.label.Model"  text="default text"/></div></th>
<th height="30"><div>
        <c:if test="${fleetTypeId == 1}">
        <spring:message code="trucks.label.Vehicle"  text="default text"/>
        </c:if>
        <c:if test="${fleetTypeId == 2}">
        <spring:message code="trucks.label.Trailer"  text="default text"/>
        </c:if>
        <spring:message code="trucks.label.Type"  text="default text"/></div></th>
<th height="30"><div><spring:message code="trucks.label.FuelType"  text="default text"/></div> </th>
<th height="30"><div><spring:message code="trucks.label.Description"  text="default text"/></div></th>
<th height="30"><div><spring:message code="trucks.label.Status"  text="default text"/></div></th>
<th height="30"><div><spring:message code="trucks.label.select"  text="default text"/></div></th>
</tr>
</thead>
</c:if> 
<c:if test = "${ModelList != null}" >
<c:forEach items="${ModelList}" var="model"> 		
<%

String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text2";
} else {
classText = "text1";
}
%>
<tr>
<td  height="30"><%=index + 1%></td>    

<td   >
<input type="hidden" name="modelIds" value=<c:out value="${model.modelId}" />  >
<select class="form-control" name="mfrIds" onchange="setSelectbox(<%= index %>) " >
<option value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
<c:if test = "${MfrList != null}" >
<c:forEach items="${MfrList}" var="Dept"> 

<c:choose>

<c:when test="${model.mfrId==Dept.mfrId}" >
<option selected value='<c:out value="${Dept.mfrId}" />'><c:out value="${Dept.mfrName}" /></option>
</c:when>

<c:otherwise>
<option value='<c:out value="${Dept.mfrId}" />'><c:out value="${Dept.mfrName}" /></option>
</c:otherwise>


</c:choose>

</c:forEach >
</c:if>  	
</select>


</td>

<td  height="30"><input type="text" class="form-control" name="modelNames" value="<c:out value="${model.modelName}"/>" onchange="setSelectbox(<%= index %>)"></td>


 <td   ><select class="form-control" name="typeIds" onchange="setSelectbox(<%= index %>) " >
<option   value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
<c:if test = "${TypeList != null}" >
<c:forEach items="${TypeList}" var="Type"> 

<c:choose>

<c:when test="${model.typeId==Type.typeId}" >
<option selected value='<c:out value="${Type.typeId}" />'><c:out value="${Type.typeName}" /></option>
</c:when>

<c:otherwise>
<option value='<c:out value="${Type.typeId}" />'><c:out value="${Type.typeName}" /></option>
</c:otherwise>


</c:choose>




</c:forEach >
</c:if>  	
</select></td>

<td  ><select class="form-control" name="fuelIds" onchange="setSelectbox(<%= index %>) ">
<option value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
<c:if test = "${FuelList != null}" >
<c:forEach items="${FuelList}" var="Fuel"> 


<c:choose>

<c:when test="${model.fuelId==Fuel.fuelId}" >
<option selected value='<c:out value="${Fuel.fuelId}" />'><c:out value="${Fuel.fuelName}" /></option>
</c:when>

<c:otherwise>
<option value='<c:out value="${Fuel.fuelId}" />'><c:out value="${Fuel.fuelName}" /></option>
</c:otherwise>


</c:choose>



 

</c:forEach >
</c:if>  	
</select></td>

 <td  height="30"><input type="text" class="form-control" name="descriptions" value="<c:out value="${model.description}"/>" onchange="setSelectbox(<%= index %>)"></td>
 <td><select class="form-control" name="activeInds" onchange="setSelectbox(<%= index %>) " >
<c:choose>
<c:when test="${model.activeInd == 'Y'}">
<option value="Y" selected><spring:message code="trucks.label.Active"  text="default text"/></option>
<option value="N"><spring:message code="trucks.label.InActive"  text="default text"/></option>
</c:when>
<c:otherwise>
<option value="Y"><spring:message code="trucks.label.Active"  text="default text"/></option>
<option value="N" selected><spring:message code="trucks.label.InActive"  text="default text"/></option>
</c:otherwise>
</c:choose>
</select>

</td>
<td width="77" height="30" ><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
</tr>
<%
index++;
%>
</c:forEach >
</c:if> 
</table>
<center>
<br>
<input type="hidden" name="fleetTypeId" value="<c:out value="${fleetTypeId}"/>">
<input type="button" name="save" value="<spring:message code="trucks.label.Save"  text="default text"/>" onClick="submitpage(this.name)" class="btn btn-success" />
<input type="hidden" name="reqfor" value="designa" />
</center>
<br>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>
