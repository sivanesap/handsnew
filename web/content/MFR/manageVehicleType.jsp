
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <script language="javascript">
        function submitPage(value)
        {
            if (value=='add')
                {
                    document.desigDetail.action ='/throttle/addTypePage.do';
                }else if(value == 'modify'){
                
                document.desigDetail.action ='/throttle/alterTypePage.do';
            }
            document.desigDetail.submit();
        }
    </script>
    <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="trucks.label.VehicleType"  text="VehicleType"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
          <li class="active"><spring:message code="trucks.label.VehicleType"  text="VehicleType"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
    <body>
        
        <form method="post" name="desigDetail">
<%@ include file="/content/common/message.jsp" %>
 <% int index = 0;  %>    
            <br>
            <c:if test = "${TypeList != null}" >
                <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                   
                    <tr align="center">
                        <th  ><div ><spring:message code="trucks.label.SNo"  text="default text"/></div></th>
                        <th  ><div ><spring:message code="trucks.label.VehicleType"  text="default text"/></div></th>
                        <th  ><div ><spring:message code="trucks.label.VehicleTonnage"  text="default text"/></div></th>
                        <th  ><div ><spring:message code="trucks.label.VehicleCapacity(CBM)"  text="default text"/></div></th>
                        <th  ><div ><spring:message code="trucks.label.Description"  text="default text"/></div></th>
                        <th  ><div ><spring:message code="trucks.label.Status"  text="default text"/></div></th>
                       
                    </tr>
                    </thead>
                    <%

                    %>
                    
                    <c:forEach items="${TypeList}" var="list"> 	
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr  width="420"  >
                            <td><%=index + 1%></td>
                            <td><input type="hidden" name="typeId" value='<c:out value="${list.typeId}"/>'> <c:out value="${list.typeName}"/></td>
                            <td>&nbsp;&nbsp;<c:out value="${list.tonnage}"/></td>
                            <td><c:out value="${list.capacity}"/></td>
                            <td><c:out value="${list.description}"/></td>
                            <td>                                
                                <c:if test = "${list.activeInd == 'Y'}" >
                                    <spring:message code="trucks.label.Active"  text="default text"/>
                                </c:if>
                                <c:if test = "${list.activeInd == 'N'}" >
                                    <spring:message code="trucks.label.InActive"  text="default text"/>
                                </c:if>
                            </td>
                             
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    
                </table>
            </c:if> 
            <center>
                <br>
                <input type="button" name="add" value="<spring:message code="trucks.label.Add"  text="default text"/>" onClick="submitPage(this.name)" class="btn btn-success" >
                <c:if test = "${TypeList != null}" >
                    <input type="button" value="<spring:message code="trucks.label.Alter"  text="default text"/>" name="modify" onClick="submitPage(this.name)" class="btn btn-success" style="display: none">
                </c:if>
                <input type="hidden" name="reqfor" value="">
            </center>
            <br>
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
         <script language="javascript" type="text/javascript">
	                        setFilterGrid("table");
	                    </script>
	                    <div id="controls">
	                        <div id="perpage">
	                            <select onchange="sorter.size(this.value)">
	                                <option value="5" selected="selected">5</option>
	                                <option value="10">10</option>
	                                <option value="20">20</option>
	                                <option value="50">50</option>
	                                <option value="100">100</option>
	                            </select>
	                            <span>Entries Per Page</span>
	                        </div>
	                        <div id="navigation">
	                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
	                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
	                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
	                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
	                        </div>
	                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
	                    </div>
	                    <script type="text/javascript">
	                        var sorter = new TINY.table.sorter("sorter");
	                        sorter.head = "head";
	                        sorter.asc = "asc";
	                        sorter.desc = "desc";
	                        sorter.even = "evenrow";
	                        sorter.odd = "oddrow";
	                        sorter.evensel = "evenselected";
	                        sorter.oddsel = "oddselected";
	                        sorter.paginate = true;
	                        sorter.currentid = "currentpage";
	                        sorter.limitid = "pagelimit";
	                        sorter.init("table", 1);
                    </script>
        </form>
    </body>
</div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>