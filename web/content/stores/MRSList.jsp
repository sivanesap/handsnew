

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
         <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.mrs.business.MrsTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        <title>MRSList</title>
    </head>



<script>
function addRow(val){
    //if(document.getElementById(val).innerHTML == ""){
    document.getElementById(val).innerHTML = "<input type='text' size='7' class='textbox' >";
    //}else{
    //document.getElementById(val).innerHTML = "";
    //}
}

function showTable()
{
var selectedMrs = document.getElementsByName("selectedIndex");
var counter=0;
for(var i=0;i<selectedMrs.length;i++){
    if(selectedMrs[i].checked == 1){
        counter++;
        break;
    }
}
if(counter ==0){
 alert("Please Select MRS");
}else{
    document.spareIssue.action = "/throttle/mrsSummary.do";
    document.spareIssue.submit();
}
}
function newWO(){
        window.open('/throttle/content/stores/OtherServiceStockAvailability.html', 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }

function submitPag(val){
    if(validate() == '0'){
        return;
    }else if(val=='purchaseOrder'){
        document.spareIssue.purchaseType.value="1012"
        document.spareIssue.action = '/throttle/generateMpr.do'
        document.spareIssue.submit();
    }else if(val == 'localPurchase'){
        document.spareIssue.purchaseType.value="1011"
        document.spareIssue.action = '/throttle/generateMpr.do'
        document.spareIssue.submit();
    }
}



function searchSubmit(){

    if(document.spareIssue.toDate.value==''){
        alert("Please Enter From Date");
    }else if(document.spareIssue.fromDate.value==''){
        alert("Please Enter to Date");
    }
    document.spareIssue.action = '/throttle/MRSList.do'
    document.spareIssue.submit();


}


function validate()
{
var selectedMrs = document.getElementsByName("selectedIndex1");
var reqQtys = document.getElementsByName("reqQtys");
var vendorIds = document.getElementsByName("vendorIds");
var counter=0;
    for(var i=0;i<selectedMrs.length;i++){
        if(selectedMrs[i].checked == 1){
            counter++;
            if( parseFloat(reqQtys[i].value)== 0  ||  isFloat(reqQtys[i].value) ){
                alert("Please Enter Valid Required Quantity");
                reqQtys[i].select();
                reqQtys[i].focus();
                return '0'
            }
            if( vendorIds[i].value=='0' ){
                alert("Please Select Vendor");
                vendorIds[i].focus();
                return '0'
            }
        }
    }
    if(counter==0){
        alert("Please Select Atleast one item");
        return '0';
    }
    return counter;
}


function selectBox(val){
    var checkBoxs = document.getElementsByName("selectedIndex1");
    checkBoxs[val].checked=1;
}

            function setDate(fDate,tDate){
                if(fDate != 'null'){
                document.spareIssue.fromDate.value=fDate;
                }
                if(tDate != 'null'){
                document.spareIssue.toDate.value=tDate;
                }

            }


<%--function setServicePoint()
{    
    if('<%=request.getAttribute("compId")%>' !=null)
        {            
            document.spareIssue.compId.value= '<%=request.getAttribute("compId")%>';
        }
}--%>
</script>

<body onLoad="setDate('<%= request.getAttribute("fromDate") %>','<%= request.getAttribute("toDate") %>');">

<form name="spareIssue"  method="post" >
            <%@ include file="/content/common/path.jsp" %>


<%@ include file="/content/common/message.jsp" %>



            <table align="center" border="0" cellpadding="0" cellspacing="0" width="500" id="bg" class="border">
                <tr>
                    <td colspan="4" align="center" class="contenthead" height="30"><div class="contenthead">MRS List</div> </td>
                </tr>
                <tr>
                    <td class="text1" height="30">From Date</td>
                    <td class="text1" height="30">
                        <input type="text" class="textbox"  name="fromDate" value="" >
                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.spareIssue.fromDate,'dd-mm-yyyy',this)"/>
                    </td>
                    <td class="text1" height="30">TO Date</td>
                    <td class="text1" height="30">
                        <input type="text" class="textbox"  readonly name="toDate" value="" >
                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.spareIssue.toDate,'dd-mm-yyyy',this)"/>
                    </td>
                </tr>
                <%--<c:if test="${servicePtList !=null}">
                <tr>
                    <td class="text2" height="30">Service Point </td>
                    <td  class="text2" height="30">
                        <select name="compId" class="textbox">
                            <option value="0" selected>--Select--</option>
                            <c:forEach items="${servicePtList}" var="comp">
                                <option value="<c:out value="${comp.companyId}"/>"><c:out value="${comp.companyName}"/></option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>                
                </c:if>--%>
                <tr>
                    <td>&nbsp; </td>
                </tr>
                <tr>
                    <td colspan="4" align="center" class="text2" height="30">
                        <input type="button" class="button" readonly name="search" value="search" onClick="searchSubmit();" >
                    </td>
                </tr>

            </table>
            <br>
            <br>

<% int index=0;
    String classText = "";
    int oddEven =0;
%>
<c:if test = "${mrsLists != null}" >


            <table align="center" border="0" cellpadding="0" cellspacing="0" width="900" id="bg" class="border">

                <tr>
                    <td class="contentsub" height="30"><div class="contentsub">Queue No</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Job Card No</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Work Order No</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Counter Sale No</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">MRS No</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Vehicle No</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Created Date/Time</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Approval Status</div></td>
                    <td class="contentsub" height="30"><div class="contentsub"></div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Stock Details</div></td>
                </tr>

<c:forEach items="${mrsLists}" var="mrs">
<%

    oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
                <tr>
                    <td class="<%=classText %>" height="30"><%= index+100 %></td>
                    <td class="<%=classText %>" height="30"><c:out value="${mrs.mrsJobCardNumber}"/></td>
                    <td class="<%=classText %>" height="30"><c:out value="${mrs.rcWorkId}"/></td>
                    <c:if test="${mrs.counterId!=0 && mrs.nettAmount!=0.00}">
                    <td class="<%=classText %>" height="30"><a href="/throttle/generateMrsGdn.do?counterId=<c:out value="${mrs.counterId}"/>&reqFor=print"><c:out value="${mrs.counterId}"/></a></td>
                    </c:if>
                    <c:if test="${mrs.counterId==0 || mrs.nettAmount==0.00}">
                    <td class="<%=classText %>" height="30"><c:out value="${mrs.counterId}"/></td>
                    </c:if>
                    <td class="<%=classText %>" height="30"><a href="/throttle/ViewMRSDetail.do?mrsId=<c:out value="${mrs.mrsNumber}"/>&counterId=<c:out value="${mrs.counterId}"/>" ><c:out value="${mrs.mrsNumber}"/></a></td>
                    <td class="<%=classText %>" height="30"><c:out value="${mrs.mrsVehicleNumber}"/></td>
                    <td class="<%=classText %>" height="30"><c:out value="${mrs.mrsCreatedDate}"/></td>
                    <td class="<%=classText %>" height="30"><c:out value="${mrs.mrsApprovalStatus}"/></td>
                    <c:if test="${mrs.nettAmount!=0.00}">
                    <td class="<%=classText %>" height="30">Issue</td>
                    </c:if>

                    <c:if test="${mrs.nettAmount==0.00}">
                    <td class="<%=classText %>" height="30"><a href="/throttle/handleViewMrsIssue.do?mrsId=<c:out value="${mrs.mrsNumber}"/>&counterId=<c:out value="${mrs.counterId}"/>" >Issue</a></td>
                    </c:if>

                    <td class="<%=classText %>" height="30"><input type="checkbox" name='selectedIndex' value=<%=index %> ></td>
                    <input type="hidden" name="mrsIds" value=<c:out value="${mrs.mrsNumber}"/> >
                </tr>
 <%
   index++;
 %>
</c:forEach>

         </table>
            <br>
            <center> <input type="button" value="show" onClick="showTable();"  class="button" >&nbsp;</center>
</c:if>
<br>


<c:if test="${ mrsSummaryList!=null }" >
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" id="bg">
                <tr>
                    <td colspan="12" align="center" class="contenthead" height="30">Required Items Info </td>
                </tr>
                <tr>
                    <td class="contentsub" height="30">Item <br>Code</td>
                    <td class="contentsub" height="30">PAPL <br>Code</td>
                    <td class="contentsub" height="30">Item <br>Name</td>
                    <td class="contentsub" height="30">New <br>Stock</td>
                    <td class="contentsub" height="30">RC <br>Stock</td>
                    <td class="contentsub" height="30">OtherServPoint<br>Stock</td>
                    <td class="contentsub" height="30">Required<br>Qty</td>
                    <td class="contentsub" height="30">PORaised<br>Qty</td>
                    <td class="contentsub" height="30">Local<br>PurchQty</td>
                    <td class="contentsub" height="30">Vendors</td>
                    <td class="contentsub" height="30">Quantity</td>
                    <td class="contentsub" height="30">&nbsp;</td>
                    <td class="contentsub" height="30">&nbsp;</td>
                </tr>
<%  index = 0; %>

<c:forEach items="${mrsSummaryList}" var="mrsSumm">
<%
    oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>

                <tr>
                    <input type="hidden" name="itemIds" value=<c:out value="${mrsSumm.mrsItemId}" /> >
                    <td class="<%= classText %>" height="30"> <c:out value="${mrsSumm.mrsItemMfrCode }" /> </td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.mrsPaplCode}" /></td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.mrsItemName}" /></td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.mrsLocalServiceItemSum}" /></td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.rcQty}" /></td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.mrsOtherServiceItemSum}" /></td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.requiredQty}" /></td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.vendorPoQty}" /></td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.localPoQty}" /></td>
                    <td class="<%= classText %>" height="30"> <select name="vendorIds" class="textbox">
                    <c:if test = "${vendorList != null}" >
                        <option value='0' >--select--</option>
                        <c:forEach items="${vendorList}" var="vend">
                        <c:out value="${vend.vendorId}" />
                                <c:if test="${vend.itemId==mrsSumm.mrsItemId}" >
                                    <option value=<c:out value="${vend.vendorId}" /> > <c:out value="${vend.vendorName}" />  </option>
                                </c:if>
                        </c:forEach>
                    </c:if>
                    </select>
                    </td>
                    <td class="<%= classText %>" height="30"><input type="text" name="reqQtys" size="5" class="textbox" value="" onChange="selectBox(<%= index %>);"  > </td>
                    <td class="<%= classText %>" height="30"><input type="checkbox" name="selectedIndex1" class="textbox" value="<%= index %>"   > </td>
                </tr>
<% index++; %>
</c:forEach>

</table>

<input type="hidden" name="purchaseType" value="" >
<br>
</c:if>
        </form>
    </body>
</html>
