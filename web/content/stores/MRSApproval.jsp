

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.mrs.business.MrsTO" %> 
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>       
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        
        <title>MRSList</title>
    </head>



<script>


function newWO(){
        window.open('/throttle/content/stores/OtherServiceStockAvailability.html', 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }

function showTable()
{
    document.getElementById("showTable").style.visibility = "visible";
}


function submt()
{
    if(validate() == '0'){
        return
    }
    
    else{        
    document.spareIssue.action = "/throttle/handleMrsApprovalSummary.do";    
    document.spareIssue.submit();
    }
}

function submitPag(val){
    if(val =='search'){
        if(document.spareIssue.toDate.value==''){
            alert("Please Enter From Date");
        }else if(document.spareIssue.fromDate.value==''){
            alert("Please Enter to Date");
        }    
        
        document.spareIssue.action = '/throttle/MRSApproval.do'
        document.spareIssue.submit();    
    }
} 


            function setDate(fDate,tDate){
                if(fDate != 'null'){
                document.spareIssue.fromDate.value=fDate;
                }
                if(tDate != 'null'){
                document.spareIssue.toDate.value=tDate;
                }
                                
            }


function validate()
{
var selectedMrs = document.getElementsByName("selectedIndex");
var counter=0;

    for(var i=0;i<selectedMrs.length;i++){
        if(selectedMrs[i].checked == 1){
            counter++;                        
        }       
    }
    if(counter==0){
        alert("Please Select Any One Mrs");
        return '0';
    }     
    return counter;
}

</script>

<body onLoad="setDate('<%= request.getAttribute("fromDate") %>','<%= request.getAttribute("toDate") %>')">        

<form name="spareIssue" method="post" >
<%@ include file="/content/common/path.jsp" %>


<%@ include file="/content/common/message.jsp" %>

<% int index=0;
int oddEven =0;
    String classText = ""; %>    

  <table align="center" border="0" cellpadding="0" cellspacing="0" width="500" id="bg" class="border">
                <tr>
                    <td colspan="4" align="center" class="contenthead" height="30"><div class="contenthead">MRS Approval</div> </td>
                </tr>                  
                <tr>
                    <td class="text1" height="30">From Date</td>
                    <td class="text1" height="30">
                        <input type="text" class="textbox" name="fromDate" value="" >
                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.spareIssue.fromDate,'dd-mm-yyyy',this)"/>    
                    </td>               
                    <td class="text1" height="30">TO Date</td>
                    <td class="text1" height="30">
                        <input type="text" class="textbox"  readonly name="toDate" value="" >
                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.spareIssue.toDate,'dd-mm-yyyy',this)"/>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp; </td>
                </tr>                
                <tr>
                    <td colspan="4" align="center" class="text2" height="30">
                        <input type="button" class="button" readonly name="search" value="search" onClick="submitPag('search');" >
                    </td>
                </tr>                
                
            </table>


<br>
<br>


    <c:if test = "${mrsLists != null}" >
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="650" id="bg" class="border">
               
                <tr>
                    <td class="contentsub" align="left" width="190" height="30"><div class="contentsub">Queue No</div></td>
                    <td class="contentsub" align="left" width="180" height="30"><div class="contentsub">JobCard No</div></td>
                    <td class="contentsub" align="left" width="155" height="30"><div class="contentsub">MRS No</div></td>
                    <td class="contentsub" align="left" width="135" height="30"><div class="contentsub">Vehicle No</div></td>
                    <td class="contentsub" align="left" width="140" height="30"><div class="contentsub">Created Date/Time</div></td>
                    <td class="contentsub" align="left" width="150" height="30"><div class="contentsub">Approval</div></td>
                    <td class="contentsub" align="left" height="30">&nbsp;</td>
                </tr>


      <c:forEach items="${mrsLists}" var="mrs"> 
<%
    oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
                <tr>
                    <td class="<%=classText %>" align="left"  width="70" height="30"><%=index+1%></td>
                    <td class="<%=classText %>" align="left" width="160" height="30"><c:out value="${mrs.mrsJobCardNumber}"/></td>
                    <td class="<%=classText %>" align="left" width="125" height="30"><a href="/throttle/MRSDetail.do?mrsId=<c:out value="${mrs.mrsNumber}"/>" ><c:out value="${mrs.mrsNumber}"/></a></td>
                    <input type="hidden" name="mrsIds" value="<c:out value="${mrs.mrsNumber}"/>" >
                    <td class="<%=classText %>" align="left" width="105" height="30"><c:out value="${mrs.mrsVehicleNumber}"/></td>
                    <td class="<%=classText %>" align="left"  width="550" height="30"><c:out value="${mrs.mrsCreatedDate}"/></td>
                    <c:if test="${mrs.mrsStatus=='Hold'}" >
                    <td class="<%=classText %>" align="left" width="110"height="30"><a href="/throttle/MRSApprovalScreen.do?mrsId=<c:out value="${mrs.mrsNumber}"/>" >Approve</a></td>
                    </c:if>
                    <c:if test="${mrs.mrsStatus!='Hold'}" >
                    <td class="<%=classText %>" align="left" width="110"height="30"><a href="/throttle/mrsStatus.do?status=<c:out value="${mrs.mrsStatus}"/>&mrsId=<c:out value="${mrs.mrsNumber}"/>&mrsJobCardNumber=<c:out value="${mrs.mrsJobCardNumber}"/>">View</a></td>
                    </c:if>                    
                    <td class="<%=classText %>" align="left" height="30"><input type="checkbox" name="selectedIndex" value="<%= index %>" class="textbox" > </td>
                    <input type="hidden" name="directToPage" value="mrsApprovalList" > 
                 </tr>
 <%
   index++;
 %>
</c:forEach>
              
            </table>
            <br>
            <center> <input type="button" name="show" class="button" value="show" onClick="submt();"> </center>
</c:if>    
<br>
    
<c:if test="${ mrsSummaryList!=null }" >
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" id="bg">
                <tr>
                    <td colspan="12" align="center" class="contenthead" height="30">Required Items Info </td>
                </tr>
                <tr>
                    <td class="contentsub" height="30">Item<br> Code</td>
                    <td class="contentsub" height="30">PAPL<br> Code</td>
                    <td class="contentsub" height="30">Item <br>Name</td>
                    <td class="contentsub" height="30">New<br>Stock</td>
                    <td class="contentsub" height="30">RC<br>Stock</td>                    
                    <td class="contentsub" height="30">OtherServPoint<br>Stock</td>
                    <td class="contentsub" height="30">Required<br> Qty</td>
                    <td class="contentsub" height="30">PORaised<br> Qty</td>
                    <td class="contentsub" height="30">Local<br> PurchQty</td>
                    <td class="contentsub" height="30">&nbsp;</td>
                </tr>
<%  index = 0; %>                

<c:forEach items="${mrsSummaryList}" var="mrsSumm">    
<%    
    oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>

                <tr>
                    <input type="hidden" name="itemIds" value=<c:out value="${mrsSumm.mrsItemId}" /> >
                    <td class="<%= classText %>" height="30"> <c:out value="${mrsSumm.mrsItemMfrCode }" /> </td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.mrsPaplCode}" /></td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.mrsItemName}" /></td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.mrsLocalServiceItemSum}" /></td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.rcQty}" /></td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.mrsOtherServiceItemSum}" /></td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.requiredQty}" /></td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.vendorPoQty}" /></td>
                    <td class="<%= classText %>" height="30"><c:out value="${mrsSumm.localPoQty}" /></td>                    
                </tr>    
<% index++; %>    
</c:forEach>   
   
</table>
<input type="hidden" name="purchaseType" value="" >
<br>   
</c:if> 
    
        </form>
    </body>
</html>
