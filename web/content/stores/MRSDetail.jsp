
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.mrs.business.MrsTO" %> 
    </head><title>Store</title>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script>
        
        
    function submitPage(value)
    {
        document.spareIssue.mrsApprovalStatus.value = value;
        selectedItemValidation(value);   
    }
    function newWO(val){           
        window.open('/throttle/OtherServiceStockAvailability.do?itemId='+val, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }
    function selectedItemValidation(value){
        var index = document.getElementsByName("selectedIndex");
        var issueQty = document.getElementsByName("issueQtys");
        var newItem = document.getElementsByName("newItems");
        var rcItem = document.getElementsByName("rcItems");
        var chec=0;
        
        if(value != 'Rejected'){
        for(var i=0;(i<index.length && index.length!=0);i++){
            chec++;
            if(floatValidation(issueQty[i],'Issue Quantity')){       
                return;
            }   
        }
        }
        
        if(value == 'Rejected'){
            for(var i=0;(i<index.length && index.length!=0);i++){
                issueQty[i].value='0';
            }
        }
        
        if(textValidation(document.spareIssue.mrsApprovalRemarks,'Remarks')){   
            return;
        }
        document.spareIssue.action='/throttle/handleAddMrsApprovedQuantity.do';           
        document.spareIssue.submit();
    }
    function setFocus(){
        
        document.spareIssue.issueQtys0.focus();
    }
    </script>
    
    
    <body onload="setFocus();">
        <form name="spareIssue"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            
          
            <%@ include file="/content/common/message.jsp" %>
            
            <c:if test = "${vehicleDetails != null}" >
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg" class="border">
                    <tr>
                        <td class="contenthead" colspan="4" align="center" height="30"><div class="contenthead">Vehicle Details</div></td>
                    </tr>
                    <% int index = 0;%>
                    <c:forEach items="${vehicleDetails}" var="vehicle"> 
                        <tr>
                            <td class="text2" height="30">Vehicle Number</td>
                            <td class="text2" height="30"><c:out value="${vehicle.mrsVehicleNumber}"/></td>
                            <td class="text2" height="30">KM</td>
                            <td class="text2" height="30"><c:out value="${vehicle.mrsVehicleKm}"/></td>
                        </tr>
                        
                        <tr>
                            <td class="text1" height="30">Vehicle Type</td>
                            <td class="text1" height="30"><c:out value="${vehicle.mrsVehicleType}"/></td>
                            <td class="text1" height="30">MFR</td>
                            <td class="text1" height="30"><c:out value="${vehicle.mrsVehicleMfr}"/></td>
                        </tr>
                        
                        <tr>
                            <td class="text2" height="30">Use Type</td>
                            <td class="text2" height="30"><c:out value="${vehicle.mrsVehicleUsageType}"/></td>
                            <td class="text2" height="30">Model</td>
                            <td class="text2" height="30"><c:out value="${vehicle.mrsVehicleModel}"/></td>
                        </tr>
                        
                        <tr>
                            <td class="text1" height="30">Engine No</td>
                            <td class="text1" height="30"><c:out value="${vehicle.mrsVehicleNumber}"/></td>
                            <td class="text1" height="30">Chasis No</td>
                            <td class="text1" height="30"><c:out value="${vehicle.mrsVehicleChassisNumber}"/></td>
                        </tr>
                        
                        <tr>
                            <td class="text2" height="30">Technician</td>
                            <td class="text2" height="30"><c:out value="${vehicle.mrsTechnician}"/></td>
                            <td class="text2" height="30">Job Card No.</td>
                            <td class="text2" height="30"><c:out value="${vehicle.mrsJobCardNumber}"/></td>
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach>
                </table>
                <% index = 0;%>
                <c:if test = "${mrsDetails != null}" >
                    <c:forEach items="${mrsDetails}" var="mrs"> 
                    <%if (index == 0) {%>
                    
                    <input type="hidden" name="mrsNumber" value="<c:out value="${mrs.mrsNumber}"/>">   
                           
                           <% index++;
            }%>
                           </c:forEach>
                </c:if>
                <br>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="850" id="bg" class="border">
                    <tr>
                        <% index = 0;%>
                        <c:if test = "${mrsDetails != null}" >
                            <c:forEach items="${mrsDetails}" var="mrs"> 
                                <%if (index == 0) {%>
                                <td colspan="12" align="center" class="text2" height="30"><strong>MRS Number :<c:out value="${mrs.mrsNumber}"/></strong></td>  
                                <% index++;
            }%>
                            </c:forEach>
                        </c:if>
                    </tr>
                    <c:if test = "${mrsDetails != null}" >
                        <tr>
                            <td class="contentsub" align="left"  height="30">MFR<br> Code</td>
                            <td class="contentsub" align="left" height="30">PAPL <br>Code</td>
                            <td class="contentsub" align="left"  height="30">Item<br> Name</td>
                            <td class="contentsub" align="left" height="30">Requested<br> Qty</td>
                            <td class="contentsub" align="left" height="30">New Item <br>Stock</td>
                            <td class="contentsub" align="left"  height="30">RC Item <br>Stock</td>  
                            <td class="contentsub" align="left" height="30">OtherService<br>Point Stk</td>
                            <td class="contentsub" align="left" height="30">PO Raised <br>Qty</td>
                            <td class="contentsub" align="left" height="30">Local <br>PurchQty</td>
                            <td class="contentsub" align="left" height="30">Other MRS<br> Req</td>
                            <td class="contentsub" align="left" height="30">Issue <br>Qty</td>
                        </tr>
                        
                        <% index = 0;%>
                        
                        <c:forEach items="${mrsDetails}" var="mrs"> 
                            <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                            %>
                            <tr>
                                <td class="<%=classText %>" height="30"><c:out value="${mrs.mrsItemMfrCode}"/></td>
                                <td class="<%=classText %>" height="30"><c:out value="${mrs.mrsPaplCode}"/></td>
                                <td class="<%=classText %>" height="30"><input type="hidden" name="itemIds" value="<c:out value="${mrs.mrsItemId}"/>"><c:out value="${mrs.mrsItemName}"/></td>
                                <td class="<%=classText %>" height="30"><input type="hidden" name="requestedQtys" value="<c:out value="${mrs.mrsRequestedItemNumber}"/>"><c:out value="${mrs.mrsRequestedItemNumber}"/></td>
                                <td class="<%=classText %>" height="30"><input type="hidden" name="newItems" value="<c:out value="${mrs.mrsLocalServiceItemSum}" />"><c:out value="${mrs.mrsLocalServiceItemSum}" /></td>
                                <td class="<%=classText %>" height="30"><input type="hidden" name="rcItems" value="<c:out value="${mrs.rcQty}" />"><c:out value="${mrs.rcQty}" /></td>
                                <td class="<%=classText %>" height="30"><a href="#" onClick="newWO(<c:out value="${mrs.mrsItemId}"/>);" ><c:out value="${mrs.mrsOtherServiceItemSum}"/></a></td>
                                <td class="<%=classText %>" height="30"><c:out value="${mrs.vendorPoQty}" /></td>
                                <td class="<%=classText %>" height="30"><c:out value="${mrs.localPoQty}" /></td>
                                <td class="<%=classText %>" height="30"><c:out value="${mrs.count}"/></td>
                                <td class="<%=classText %>" height="30"><input type="text" name="issueQtys" value='<c:out value="${mrs.mrsRequestedItemNumber}"/>' id="issueQtys<%= index %>" class="textbox" size="5" ></td>
                                
                                <input type="hidden" name="positionIds" value="<c:out value="${mrs.positionId}"/>"  >
                                       <input type="hidden" checked name="selectedIndex" value="<%= index %>"  >
                            </tr>
                            <% index++; %>
                        </c:forEach>
                    </c:if>
                </table>
                <br>
                <div align="center" class="text2"> Remarks &nbsp;
                    <textarea class="textbox" cols="20" rows="2" name="mrsApprovalRemarks">Approve Mrs</textarea>
                </div>
                <br>
                <center>
                    <input type="button" value="Approve" name="Approved" class="button" onclick="submitPage(this.name);" > &nbsp;
                    <input type="button" value="Reject" class="button" name="Rejected" onclick="submitPage(this.name);">
                    <input type="hidden" value="" name="mrsApprovalStatus">                    
                </center>
                <br>
                <br>
            </c:if>
        </form>
    </body>
</html>
