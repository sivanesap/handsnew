
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.mrs.business.MrsTO" %> 
        <title>MRS Issue History </title>
    </head>
    <body>
        
        
        <script>
            function submitPage(val){
                if(val=='approve'){
                    document.mpr.status.value="APPROVED"
                    document.mpr.action="/throttle/approveMpr.do"     
                    document.mpr.submit();
                }else if(val=='reject'){
                    document.mpr.status.value="REJECTED"                
                    document.mpr.action="/throttle/approveMpr.do"                
                    document.mpr.submit();
                }
            }            
            
        </script>
        
        <form name="mpr"  method="post" >                    
            <%@ include file="/content/common/path.jsp" %>            
            <!-- pointer table -->
           
            <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>                
            <br>
<% int index = 0; %>                

            <br>    
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" id="bg" class="border">              
                <%index = 0;
            String classText = "";
            int oddEven = 0;
                %>
                <c:if test = "${itemList != null}" >
                <tr>
                    <td colspan="9" align="center" class="text2" height="30"><strong>MRS Issue History</strong> </td>
                </tr>  
                        <tr>
                            <td class="contenthead" height="30"><div class="contenthead">Sno</div></td>
                            <td class="contenthead" height="30"><div class="contenthead">Mfr Code</div></td>
                            <td class="contenthead" height="30"><div class="contenthead">Papl Code</div></td>                       
                            <td class="contenthead" height="30"><div class="contenthead">Item Name</div></td>
                            <td class="contenthead" height="30"><div class="contenthead">Tyre No</div></td>
                            <td class="contenthead" height="30"><div class="contenthead">Uom</div></td>
                            <td class="contenthead" height="30"><div class="contenthead">Quantity</div></td>
                            <td class="contenthead" height="30"><div class="contenthead">Action Type</div></td>
                            <td class="contenthead" height="30"><div class="contenthead">Issued On</div></td>
                            <!--<td class="text2" height="30"><b>Fault Status</b> </td>    -->                                                                                                  
                        </tr>                
                    <c:forEach items="${itemList}" var="item"> 
                        <%
            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            
                            <td class="<%=classText %>" height="30"><%= index+1 %> </td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.mrsItemMfrCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.mrsPaplCode}"/></td>                       
                            <td class="<%=classText %>" height="30"><c:out value="${item.mrsItemName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.tyreNo}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.uomName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.quantityRI}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.actionType}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.issuedOn}"/></td>
                            <!--<td class="<%=classText %>" height="30"><c:out value="${item.faultStatus}"/></td>  --> 
                        </tr>
                        <%
                        index++;
                        %>
                    </c:forEach>                               
            </table> 
            <br>
             <center>   
            <input type="button" class="button" name="reject" value="print" onClick="" >
            </center>  
        </c:if>               
        </form>
    </body>
</html>
