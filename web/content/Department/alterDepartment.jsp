<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
    <head>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
         <script language="javascript" src="/throttle/js/validate.js"></script>     
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.department.business.DepartmentTO" %>  
        <%@ page import="java.util.*" %>          
    </head>
    <script>
        function submitpage(indx)
        {
            selectedItemValidation();           
        }
      function selectedItemValidation(){
var index = document.getElementsByName("selectedIndex");
var name = document.getElementsByName("names");
var description = document.getElementsByName("descriptions");
var status = document.getElementsByName("stats");
var chec=0;
for(var i=0;(i<index.length && index.length!=0);i++){
if(index[i].checked){
chec++;
if(textValidation(name[i],'Department Name')){       
        return;
   }     
 if(textValidation(description[i],'Department Description')){ 
        return;
   }     
 if(textValidation(status[i],'Department Status')){ 
        return;
   }  
   document.manageDepartment.action='/throttle/handleUpdateDepartment.do';
   document.manageDepartment.submit();
}
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
name[0].focus();
}
}  
        
        function setSelectbox(i)
        {
            var selected=document.getElementsByName("selectedIndex") ;
            selected[i].checked = 1;
        }    
        
    </script>
    <body>
       
                    
                    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Alter Department" text="Alter Department"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="HRMS"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Alter Department" text="Alter Department"/></li>
		
		                </ol>
		            </div>
        </div>
                                    
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
        <form name="manageDepartment"  method="post" >
            
            <%--<%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>--%>
           
                 <c:if test = "${DepartmentLists != null}" >
           <table class="table table-info mb30 table-hover"  id="table">
               <thead>
                <tr >   
                    <th  align="center"  > &nbsp; &nbsp; Name</th>
                    <th  align="center" > &nbsp;&nbsp; Description</th>
                    <th  align="center" > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;Status</th>
                    <th  align="center" >Select</th>
                </tr>
               </thead>
                <% int index = 0;%>               
                    <c:forEach items="${DepartmentLists}" var="department"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>  
                            
                            <td align="left" ><input name="departmentIds" type="hidden" class="textbox" value="<c:out value='${department.departmentId}'/>" ><input name="names" type="text" class="form-control" style="width:250px;height:40px" onchange="setSelectbox('<%= index %>');"  value=<c:out value="${department.name}"/>></td>
                            <td align="left" ><textarea class="form-control" style="width:250px;height:40px" name="descriptions"  onchange="setSelectbox('<%= index %>');" ><c:out value="${department.description}"/></textarea></td>
                            <td align="center" > <select name="stats" class="form-control" style="width:250px;height:40px" onchange="setSelectbox('<%= index %>');" >
                                    <c:if test="${(department.status=='y') || (department.status=='Y')}" >
                                        <option value="Y" selected>Active</option><option value="N">InActive</option>
                                    </c:if>
                                    <c:if test="${(department.status=='n') || (department.status=='N')}" >
                                        <option value="Y" >Active</option><option value="N" selected>InActive</option>                           
                                    </c:if>                            
                                </select>
                            </td>
                            <td width="87" height="30" ><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
                        </tr> 
                        <%
            index++;

                        %>
                    </c:forEach>                
            </table>
            <br>
            <center><input type="button" class="btn btn-success" value="Save" onclick="submitpage(this.value);"/></center> 
            <input type="hidden" name="reqfor" value="" />
            <br>
        </form>
          </c:if> 
            <script language="javascript" type="text/javascript">
             setFilterGrid("table");
         </script>
         <div id="controls">
             <div id="perpage">
                 <select onchange="sorter.size(this.value)">
                     <option value="5"  selected="selected">5</option>
                     <option value="10">10</option>
                     <option value="20">20</option>
                     <option value="50">50</option>
                     <option value="100">100</option>
                 </select>
                 <span>Entries Per Page</span>
             </div>
             <div id="navigation">
                 <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                 <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                 <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                 <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
             </div>
             <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
         </div>
         <script type="text/javascript">
             var sorter = new TINY.table.sorter("sorter");
             sorter.head = "head";
             sorter.asc = "asc";
             sorter.even = "evenrow";
             sorter.odd = "oddrow";
             sorter.evensel = "evenselected";
             sorter.oddsel = "oddselected";
             sorter.paginate = true;
             sorter.currentid = "currentpage";
             sorter.limitid = "pagelimit";
             sorter.init("table", 7);
        </script>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>