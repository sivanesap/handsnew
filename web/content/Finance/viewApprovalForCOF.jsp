<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
    </head>
    <script language="javascript">
        function submitPage() {

            document.customer.action = '/throttle/updateCOFApproval.do';
            document.customer.submit();
        }
    </script>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Consignment Approval Request" text="COF Approval"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
                <li class=""><spring:message code="hrms.label.Consignment Approval" text="COF Approval"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">

                <body>
                    <form name="customer" method="post" >
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <%@ include file="/content/common/message.jsp" %>
                        <c:if test = "${COFApproval != null}" >
                            <c:forEach items="${COFApproval}" var="cof">
                                <table class="table table-info mb30 table-hover">
                                    <thead><tr><th colspan="6">View Consignment Approval </th></tr></thead>

                                    <%!
                                    public String NullCheck(String inputString)
                                         {
                                                 try
                                                 {
                                                         if ((inputString == null) || (inputString.trim().equals("")))
                                                                         inputString = "";
                                                 }
                                                 catch(Exception e)
                                                 {
                                                                         inputString = "";
                                                 }
                                                 return inputString.trim();
                                         }
                                    %>

                                    <%

                                     String today="";
                                     String fromday="";

                                     fromday = NullCheck((String) request.getAttribute("fromdate"));
                                     today = NullCheck((String) request.getAttribute("todate"));

                                     if(today.equals("") && fromday.equals("")){
                                     Date dNow = new Date();
                                     Calendar cal = Calendar.getInstance();
                                     cal.setTime(dNow);
                                     cal.add(Calendar.DATE, 0);
                                     dNow = cal.getTime();

                                     Date dNow1 = new Date();
                                     Calendar cal1 = Calendar.getInstance();
                                     cal1.setTime(dNow1);
                                     cal1.add(Calendar.DATE, -6);
                                     dNow1 = cal1.getTime();

                                     SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
                                     today = ft.format(dNow);
                                     fromday = ft.format(dNow1);
                                     }

                                    %>
                                    <tr>
                                        <td>Consignment Note</td>
                                        <td><c:out value="${cof.cnotes}"/></td>
                                        <td>Trip Code</td>
                                        <td ><c:out value="${cof.tripCode}"/>
                                        <input type="hidden" name="tripId" id="tripId" value="<c:out value="${cof.tripId}"/>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Customer</td>
                                        <td><c:out value="${cof.customerName}"/></td>
                                        <td>Vehicle Type</td>
                                        <td><c:out value="${cof.vehicleTypeName}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Route</td>
                                        <td><c:out value="${cof.routeName}"/></td>
                                        <td>Claim No</td>
                                        <td><c:out value="${cof.claimNo}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Nature Of Loss</td>
                                        <td><c:out value="${cof.natureOfLoss}"/></td>
                                        <td>Actuale weight</td>
                                        <td><c:out value="${cof.actualewight}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Delivered weight</td>
                                        <td><c:out value="${cof.deliveredWeight}"/></td>
                                        <td>Claim Amount</td>
                                        <td><c:out value="${cof.claimAmount}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Amount Loss</td>
                                        <td><c:out value="${cof.amountLoss}"/></td>
                                        <td>Cof Date</td>
                                        <td><c:out value="${cof.cofDate}"/></td>
                                    </tr>
                                    <tr><td>Status</td>
                                        <td>Loss of Pallets</td>
                                        <td><c:out value="${cof.pallets}"/></td>
                                        <td>
                                            <select name="cofStatus"  id="cofStatus" class="form-control"  style="width:175px;height:40px" >                                                    
                                                <option value="Y">Approve</option>
                                                <option value="N">Reject</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><input type="button" value="Save" class="btn btn-success" id="saveButton" onClick="submitPage(this.value);" style="width:100px;height:35px;"></td>
                                    </tr>
                                </table>
                            </c:forEach>
                        </c:if>

                        <c:if test = "${COFApproval != null}" >
                            <table class="table table-info mb30 table-hover " id="table" style="display:none">
                                <thead>
                                    <tr height="40">
                                        <th>S.No</th>                            
                                        <th>Consignment Note</th>
                                        <th>Trip Code</th>
                                        <th>Customer</th>
                                        <th>Vehicle Type</th>
                                        <th>Route</th>                            
                                        <th>Claim No</th>
                                        <th>Nature Of Loss</th>
                                        <th>Actuale weight</th>
                                        <th>Delivered Weight</th>
                                        <th>Claim Amount</th>
                                        <th>Amount Loss</th>
                                        <th>Cof Date</th>
                                        <th>Status</th>                            
                                    </tr>
                                </thead>
                                <tbody>
                                    <% int index = 0,sno = 1;%>
                                    <c:forEach items="${COFApproval}" var="cof">
                                        <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                        %>
                                        <tr height="30">
                                            <td align="left" class="text2"><%=sno%></td>
                                            <td align="left" class="text2"><c:out value="${cof.cnotes}"/> </td>
                                            <td align="left" class="text2"><c:out value="${cof.tripCode}"/> </td>
                                            <td align="left" class="text2"><c:out value="${cof.customerName}"/> </td>
                                            <td align="left" class="text2"><c:out value="${cof.vehicleTypeName}"/> </td>
                                            <td align="left" class="text2"><c:out value="${cof.routeName}"/> </td>
                                            <td align="left" class="text2"><c:out value="${cof.claimNo}"/> </td>
                                            <td align="left" class="text2"><c:out value="${cof.natureOfLoss}"/> </td>
                                            <td align="left" class="text2"><c:out value="${cof.actualewight}"/> </td>
                                            <td align="left" class="text2"><c:out value="${cof.deliveredWeight}"/> </td>
                                            <td align="left" class="text2"><c:out value="${cof.claimAmount}"/> </td>
                                            <td align="left" class="text2"><c:out value="${cof.amountLoss}"/> </td>
                                            <td align="left" class="text2"><c:out value="${cof.cofDate}"/> </td>
                                            <td align="left" class="text2">
                                                <a href="/throttle/updateCOFApproval.do?tripSheetId=<c:out value="${cof.tripId}"/>">Approval</a>
                                            </td>

                                        </tr>
                                        <%
                                                   index++;
                                                   sno++;
                                        %>
                                    </c:forEach>

                                </tbody>
                            </table>
                        </c:if>
<!--                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>-->

                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>
