<%-- 
    Document   : paymentEntry
    Created on : Mar 21, 2013, 11:58:48 AM
    Author     : Entitle
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <%@ page import="ets.domain.contract.business.ContractTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        
         <script type="text/javascript">
              function setInvoiceDetails(){
                var invoiceName = document.getElementById('invoiceIdTemp').options[document.getElementById('invoiceIdTemp').selectedIndex].text;

                var invoiceNames = document.getElementsByName('invoiceNames');

                    var errStatus = false;
                    for (var i = 0; i < invoiceNames.length; i++) {
                         if(invoiceNames[i].value == invoiceName){
                             alert("adjustment for this po no is already done. please select different po no");
                             errStatus = true;
                         }
                    }
                if(!errStatus){
                    var invoiceId = document.getElementById("invoiceIdTemp").value;
                    var temp = invoiceId.split("~");
                    document.getElementById("invoiceId").value = temp[0];
                    document.getElementById("grandTotal").value = temp[1];
                    document.getElementById("payAmount").value = temp[2];
                    document.getElementById("pendingAmount").value = temp[3];
                    document.getElementById("debitedAmount").value = temp[4];
                }
            }
            </script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        
         <script type="text/javascript">
        function submitPage(){
            if($('#receiptDate').val() == ''){
                    alert("Please select date");
                    $('#receiptDate').focus();
                }else if($('#remarks').val() == ''){
                    alert("Please enter the remarks");
                    $('#remarks').focus();
                }else if($('#invoiceIdTemp').val() == 0){
                    alert("Please select invoice");
                    $('#invoiceIdTemp').focus();
                }else if($('#payAmount').val() == 0 || $('#payAmount').val() == ''){
                    alert("Please enter debit amount");
                    $('#payAmount').focus();
                }else if($('#receiptAmount').val() < 0){
                    alert("Please check debit amount should not negative");
                    $('#payAmount').focus();
                }else {
               document.manufacturer.action = "/throttle/saveDebitNote.do";
                document.manufacturer.submit();
                }
        }
       
       
    </script>

    </head>
  
    <body>
        <form name="manufacturer" method="post" >
            <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                <br>
                <tr>
                    <td width="80" align="left">
                        Finance >> Debit Note
                    </td></tr></table>
            <br/>
            <br/>
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp"%>
                    </td>
                </tr>
            </table>
            <br/>
             <table align="center" border="0" cellpadding="0" cellspacing="0" width="1000" id="bg" class="border">
                <tr height="30">
                    <td colspan="9" class="contenthead" height="30">
                        <div class="contenthead" align="center">Debit Note Details</div></td>
                </tr>
                     <tr height = "30" class="text2">
                                <td colspan="2" class="text2" align="center" height="30">
                        Vendor Name  </td><td colspan="7" align="center"  class="text2" height="30"><c:out value="${vendorName}"/>
                   <input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>"/> </td>
                        </tr>
                         <tr height = "30"class="text1">   </tr>
                        <tr height = "30"class="text1">                              
                         <td ><font color="red">*</font>Date</td>
                          <td ><input type="text" name="receiptDate" id="receiptDate" class="datepicker" value="" >  
                     
                         <td  >&nbsp;</td>
                         <td ><font color="red">*</font> Remarks</td>
                           <td >&nbsp;</td>
                           <td ><textarea  name="remarks" id="remarks" ></textarea></td>
                              <td colspan="2">&nbsp;</td></tr>
                        </tr>

                        <tr height = "30"class="text1">   </tr>
                        <tr height = "30" class="text2">
                           
                            </td>
                            <td colspan="9" >
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="1000" id="bg" class="border">
                                    <tr height="30">
                                        <td colspan="6" class="contenthead" height="30">
                                            <div class="contenthead" align="center">&emsp;&emsp;Debit</div></td>
                                    </tr>

                                            <tr height = "30"class="text1">
                                             <td ><font color="red">*</font>Purchase Invoice No</td>
                                             <td ><font color="red">*</font>Purchase Value</td>
                                             <td ><font color="red">*</font>Yet To Pay</td>
                                             <td ><font color="red">*</font>Debited Amount</td>
                                             <td ><font color="red">*</font>Debit Amount</td>
                                             <td ><font color="red">*</font>Total Amount</td>
                                             <td >&nbsp;</td>
                                            </tr>
                                             <tr height = "30"class="text1">
                     
                <input type="hidden" name="invoiceId" id="invoiceId" value=""/>
                       <td  > <select name="invoiceIdTemp" id="invoiceIdTemp" class="textbox" style="height:20px; width:122px;" onchange="setInvoiceDetails();" >
                               <c:if test="${invoiceList != null}">
                                <option value="0" selected>--Select--</option>
                                <c:forEach items="${invoiceList}" var="invoiceList">
                                    <option value='<c:out value="${invoiceList.invoiceId}"/>~<c:out value="${invoiceList.grandTotal}"/>~<c:out value="${invoiceList.payAmount}"/>~<c:out value="${invoiceList.pendingAmount}"/>~<c:out value="${invoiceList.debitNoteAmount}"/>'><c:out value="${invoiceList.invoiceCode}"/></option>
                                </c:forEach>
                            </c:if>
                                </select>
                                                <td ><input type="text" name="grandTotal" id="grandTotal" class="textbox" value="" ></td>
                                                <td ><input type="text" name="pendingAmount" id="pendingAmount" class="textbox" value="" ></td>
                                               
                                                 <td ><input type="text" name="debitedAmount" id="debitedAmount" class="textbox" value="" ></td>
                                                 <td ><input type="text" name="payAmount" id="payAmount" class="textbox" value="" onchange="setAmount();" onKeyPress='return onKeyPressBlockCharacters(event);'></td>
                                                 <td ><input type="text" name="receiptAmount" id="receiptAmount" class="textbox" value="" readonly="" ></td>         
                                </tr>
                                </table>
                                 <script type="text/javascript">
                                    function setAmount(){
                                        var value1 = document.manufacturer.pendingAmount.value;
                                        var value2 = document.manufacturer.payAmount.value;
                                       document.manufacturer.receiptAmount.value = parseFloat(value1) - parseFloat(value2);
                                    }
                                </script>
                            </td>
                        </tr>
             </table>
            <br/>
            <br/>
             
            <br/>
            <br/>
            <center>
                        <td align="center" colspan="9"><input type="Button" name="Click" value =" Save "   class="button" onClick="submitPage();"/>
            </center>   
               
    </body>
</html>