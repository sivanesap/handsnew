<%--
    Document   : financeGroup
    Created on : 22 Oct, 2012, 3:34:29 PM
    Author     : ASHOK
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.finance.business.FinanceTO" %>
        <%@ page import="java.util.*" %>

        <title> Manage Group</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
    </head>
    <body>
        <form method="post" action="/throttle/addGroupPage.do">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <c:if test = "${groupLists != null}" >
                <table align="center" width="650" border="0" cellspacing="0" cellpadding="0" class="border">

                    <tr height="30">
                        <td  align="left" class="contenthead" scope="col"><b>S.No</b></td>
                        <td  align="left" class="contenthead" scope="col"><b>Group code</b></td>
                        <td  align="left" class="contenthead" scope="col"><b>Group Name</b></td>
                        <td  align="left" class="contenthead" scope="col"><b>Description</b></td>
                        <td  align="left" class="contenthead" scope="col"><b>Edit</b></td>
                    </tr>
                    <% int index = 0;%>
                    <c:forEach items="${groupLists}" var="GL">
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text1";
                                    } else {
                                        classText = "text2";
                                    }
                        %>
                        <tr height="30">
                            <td class="<%=classText%>"  align="left"> <%= index + 1%> </td>
                            <td class="<%=classText%>" align="left"> <c:out value="${GL.groupcode}" /></td>
                            <td class="<%=classText%>"  align="left"> <c:out value="${GL.groupname}"/> </td>
                            <td class="<%=classText%>"  align="left"> <c:out value="${GL.groupdesc}"/> </td>
                            <td class="<%=classText%>" align="left"> <a href="/throttle/alterGroupDetail.do?groupid=<c:out value='${GL.groupid}' />" > Edit </a> </td>
                        </tr>
                        <% index++;%>
                    </c:forEach>
                </c:if>
            </table>
            <br>
            <br>
            <center><input type="submit" class="button" value="Add" /></center>
        </form>
    </body>
</html>
