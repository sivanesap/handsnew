<%-- 
    Document   : financeLevelMaster
    Created on : Mar 6, 2013, 9:45:58 PM
    Author     : Entitle
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Add Level master</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>

                <script language="javascript" src="/throttle/js/validate.js"></script>
    </head>
    <script>

        function setLevelValues(levelValue)
        {
            var temp = levelValue.split("~");
            //alert(temp[0]+":::"+temp[1]+":::"+temp[2]);
            document.add.primaryID.value = temp[0];
            document.add.levelgroupId.value = temp[1];
            document.add.fullName.value = temp[2];

        }
        function submitPage()
        {
            if(isEmpty(document.getElementById("LevelName").value)){
                alert("Sub Level Name Should Not Be Empty");
                document.add.LevelName.focus();
                return;
            } else if(document.add.primaryLevelList.value == "0"){
                alert("Select Level type");
                document.add.primaryLevelList.focus();
                return;
            } else if(isEmpty(document.add.description.value)){
                alert("Please Enter Description");
                document.add.description.focus();
                return;
            }
            document.add.action='/throttle/saveNewLevel.do';
            document.add.submit();
            
        }
          

    </script>
    <body>
        <form name="add" method="post">
            <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>

            <table align="center" width="900" border="0" cellspacing="0" cellpadding="0" class="border">
                <tr height="30">
                    <Td colspan="4" class="contenthead">Add Level</Td>
                </tr>
                <input type="hidden" name="primaryID" id="primaryID" />
                <input type="hidden" name="levelgroupId" id="LevelGroupID" />
                <input type="hidden" name="fullName" id="fullName"  />
                <tr height="30">
                    <td class="text1"><font color="red">*</font>Level Name</td>
                    <td class="text1">
                        <input name="LevelName" type="text" class="textbox" id="LevelName" value="" size="20">
                    </td>
                    <td class="text1" height="30"><font color="red">*</font>Primary / Level master</td>
                    <td class="text1" height="30">
                        <select class="textbox" name="primaryLevelList" id="primaryLevelList" style="width:350px;" onchange="setLevelValues(this.value)">
                            <option value="0">---Select---</option>
                            <c:if test = "${levelMasterList != null}" >
                                <c:forEach items="${levelMasterList}" var="levelM">
                                    <option value='<c:out value="${levelM.groupID}" />~<c:out value="${levelM.levelgroupId}" />~<c:out value="${levelM.levelgroupName}" />'><c:out value="${levelM.levelgroupName}" /></option>
                                </c:forEach >
                            </c:if>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Description</td>
                    <td class="text2" height="30">
                        <input type="text" name="description" id="description" style="width: 120px" class="textbox">
                    </td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                </tr>

            </table>
            <br>
            <br>
            <center>
                <input type="button" class="button" value="Save" onclick="submitPage();" />
                &emsp;<input type="reset" class="button" value="Clear">


            </center>
        </form>
    </body>
</html>

