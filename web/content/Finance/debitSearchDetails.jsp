<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

         $(document).ready(function(){
             $("#showTable").hide();

             });
        </script>
        <script type="text/javascript">
            function submitPage(value) {
                
               $("#showTable").show();  
            }
            
           
        </script>


    </head>
    <body onload="setValue();sorter.size(10);">
        <form name="accountReceivable" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Debit Details</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red">*</font>Customer Name</td>
                                        <td height="30">
                                            <select class="textbox" name="customerId" id="customerId"  style="width:125px;">
                                                <option value="">---Select---</option>
                                                <option value="">Sakthi Traders</option>
                                                <option value="">Palanisamy .K</option>
                                                <option value="">MALNI TRADERS</option>
                                                <option value="">Sarada Company</option>
                                                
                                            </select>
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value=""></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center"><input type="button" class="button" name="Search"   value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>

<br>
            <br>
           
            <br>
            <br>
            <br>
            <div id="showTable">
                <table style="width: 1100px" align="center" border="0" id="table1" class="sortable">
                    <thead>
                        <tr height="45" >
                            <th><h3 align="center">S.No</h3></th>
                            <th><h3 align="center">Customer Name</h3></th>
                            <th><h3 align="center">Debit Note No</h3></th>
                            <th><h3 align="center">Date</h3></th>
                            <th><h3 align="center">Debit Amount</h3></th>
                            <th><h3 align="center">Reason</h3></th>
                            <th><h3 align="center">Delete</h3></th>
                            <th><h3 align="center">Modify</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                            
                            <tr height="30">

                                <td align="center">1</td>
                                <td align="left">Sakthi Traders</td>
                                <td align="left">DBNV0123</td>
                                <td align="left">01-02-2014</td>
                                <td align="left">10000</td>
                                <td align="left">Ok</td>
                                <td align="left"><input type="checkbox" name="edit" value="" id="edit" onclick="modifyExpenseDetails()"/></td>
                                <td align="left"><input type="checkbox" name="edit1" value="" id="edit1" onclick="modifyExpenseDetails()"/></td>
                               
                                
                               </tr>
                            <tr height="30">

                                <td align="center">2</td>
                                <td align="left">Palanisamy .K</td>
                                <td align="left">DBNV012378</td>
                                <td align="left">03-02-2014</td>
                                <td align="left">15000</td>
                                <td align="left">Ok</td>
                                <td align="left"><input type="checkbox" name="edit" value="" id="edit" onclick="modifyExpenseDetails()"/></td>
                                <td align="left"><input type="checkbox" name="edit1" value="" id="edit1" onclick="modifyExpenseDetails()"/></td>
                               
                                
                               </tr>
                                <tr height="30">

                                <td align="center">3</td>
                                <td align="left">MALNI TRADERS</td>
                                <td align="left">DBNV0124</td>
                                <td align="left">07-02-2014</td>
                                <td align="left">25000</td>
                                <td align="left">Ok</td>
                                <td align="left"><input type="checkbox" name="edit" value="" id="edit" onclick="modifyExpenseDetails()"/></td>
                                <td align="left"><input type="checkbox" name="edit1" value="" id="edit1" onclick="modifyExpenseDetails()"/></td>
                               
                                
                               </tr>
                            <tr height="30">

                                <td align="center">4</td>
                                <td align="left">Sarada Company</td>
                                <td align="left">DBNV012512</td>
                                <td align="left">07-01-2014</td>
                                <td align="left">15000</td>
                                <td align="left">Ok</td>
                                <td align="left"><input type="checkbox" name="edit" value="" id="edit" onclick="modifyExpenseDetails()"/></td>
                                <td align="left"><input type="checkbox" name="edit1" value="" id="edit1" onclick="modifyExpenseDetails()"/></td>
                               
                                
                               </tr>
                            <tr height="30">

                                <td align="center">5</td>
                                <td align="left">Sri Ramji Traders</td>
                                <td align="left">DBNV012345</td>
                                <td align="left">15-02-2014</td>
                                <td align="left">10000</td>
                                <td align="left">Ok</td>
                                <td align="left"><input type="checkbox" name="edit" value="" id="edit" onclick="modifyExpenseDetails()"/></td>
                                <td align="left"><input type="checkbox" name="edit1" value="" id="edit1" onclick="modifyExpenseDetails()"/></td>
                               
                                
                               </tr>

                    </tbody>
                </table>
                <center>
                                        <td><input type="button" class="button" name="ExportExcel"   value="Print" onclick="submitPage(this.name);"></td>
                                        <td><input type="button" class="button" name="Search"   value="Search" onclick="submitPage(this.name);"></td>
                </center>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
            
            </div>
        </form>
    </body>    
</html>