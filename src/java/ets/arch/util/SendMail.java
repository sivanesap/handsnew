/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.arch.util;

/**
 *
 * @author arun
 */
import java.io.IOException;
import java.util.Properties;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import org.apache.commons.mail.EmailException;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.*;
import java.util.*;
import java.text.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;

public class SendMail extends Thread {

    public SendMail() {
    }
    TripBP tripBP;

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    String SMTP = "";
    int PORT = 0;
    String Frommailid = "";
    String Password = "";
    String EmailSubject = "";
    String BodyMessage = "";
    String Toemailid = "";
    String CCemailid = "";
    int Status = 0;
    int MailSendingId = 0;
    public SendMail(String smtp, int port, String frommailid, String password, String emailSubject,
            String bodyMessage, String toemailid, String ccemailid,int mailSendingId){
        super(smtp);
        SMTP = smtp;
        PORT = port;
        Frommailid = frommailid;
        Password = password;
        EmailSubject = emailSubject;
        BodyMessage = bodyMessage;
        Toemailid = toemailid;
        CCemailid = ccemailid;
       MailSendingId = mailSendingId;
//        Toemailid = "Throttle@entitlesolutions.com";
//        CCemailid = "nithya@entitlesolutions.com";

        
    }

    public void run() {

        try {

            Properties props = System.getProperties();
            props.put("mail.smtp.starttls.enable", "true"); // added this line
            props.put("mail.smtp.host", SMTP);
            props.put("mail.smtp.user", Frommailid);
            props.put("mail.smtp.password", Password);
            props.put("mail.smtp.port", PORT);
            props.put("mail.smtp.auth", "true");
            /*
             if ("smtp.gmail.com".equalsIgnoreCase(SMTP)) {
             props.put("mail.smtp.user", Frommailid);
             props.put("mail.smtp.password", Password);
             props.put("mail.smtp.port", PORT);
             props.put("mail.smtp.auth", "true");
             } else {//godaddy server
             props.put("mail.smtp.user", "");
             props.put("mail.smtp.password", "");
             props.put("mail.smtp.port", "25");
             //props.put("mail.smtp.auth", "true");
             }
             */
            if (Toemailid.indexOf("alok.shrivastava@brattlefoods.com") >= 0
                    || Toemailid.indexOf("dipak.sutar@brattlefoods.com") >= 0) {
                if (!CCemailid.equals("") && !CCemailid.equals("-")) {
                    CCemailid = CCemailid + "," + "nileshkumar@entitlesolutions.com";
                } else {
                    CCemailid = "nileshkumar@entitlesolutions.com";
                }
            }

            String[] to = Toemailid.split(","); // added this line

            Session session = Session.getDefaultInstance(props, null);
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(Frommailid));

            InternetAddress[] toAddress = new InternetAddress[to.length];

            //////System.out.println("Toemailid:" + Toemailid);
            //////System.out.println("CCemailid:" + CCemailid);

            if (!Toemailid.equals("") && !Toemailid.equals("-")) {
                for (int i = 0; i < to.length; i++) { // changed from a while loop
                    toAddress[i] = new InternetAddress(to[i]);
                }

                for (int i = 0; i < toAddress.length; i++) { // changed from a while loop
                    message.addRecipient(Message.RecipientType.TO, toAddress[i]);
                }
            }

            ////////////// cc part////////////////////
            if (CCemailid.equals("-") || CCemailid.equals("")) {
                CCemailid = "nileshkumar@entitlesolutions.com";
            } else {
                CCemailid = CCemailid + ",nileshkumar@entitlesolutions.com";
            }
            if (!CCemailid.equals("-")) {
                String[] cc = CCemailid.split(","); // added this line
                int ccLen = cc.length;
                InternetAddress[] ccAddress = new InternetAddress[ccLen];
                int j = 0;
                for (j = 0; (j < cc.length && !CCemailid.equals("")); j++) { // changed from a while loop
                    ccAddress[j] = new InternetAddress(cc[j]);
                }
                for (j = 0; j < ccAddress.length; j++) { // changed from a while loop
                    message.addRecipient(Message.RecipientType.CC, ccAddress[j]);
                }
            }
            //////System.out.println("Toemailid1:" + Toemailid);
            //////System.out.println("CCemailid1:" + CCemailid);
            message.setSubject(EmailSubject);
            message.setContent(BodyMessage, "text/html");
            Transport transport = session.getTransport("smtp");
            transport.connect(SMTP, Frommailid, Password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            //////System.out.println("Email sent Successfully....");
            TripTO tripTO = new TripTO();
            int userId = 1081;
             Status = 1;
            String fileName = "jdbc_url.properties";
            Properties dbProps = new Properties();
            InputStream is = getClass().getResourceAsStream("/"+fileName);
            dbProps.load(is);//this may throw IOException
            String dbClassName = dbProps.getProperty("jdbc.driverClassName");
            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");
            int updateStatus = 0;
            Connection con = null;
            try {
                Class.forName(dbClassName).newInstance();
                con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
                try{
                PreparedStatement updateMailDetails = null;
                String sql = "INSERT INTO \n" +
                "ts_mail_master (Mail_Type_Id, Mail_Subject_TO, Mail_Content_TO, Mail_Id_TO, Mail_Id_CC, Mail_Deliveried_Status, Active_Ind, Created_By, Created_On)  \n" +
                "VALUES(?,?,?,?,?,?,?,?,now())";
                updateMailDetails = con.prepareStatement(sql);
                updateMailDetails.setInt(1,2);
                updateMailDetails.setString(2,EmailSubject);
                updateMailDetails.setString(3,BodyMessage);
                updateMailDetails.setString(4,Toemailid);
                updateMailDetails.setString(5,CCemailid);
                updateMailDetails.setInt(6,1);
                updateMailDetails.setString(7,"Y");
                updateMailDetails.setInt(8,MailSendingId);
                updateStatus = updateMailDetails.executeUpdate();
                //////System.out.println("updateStatus = " + updateStatus);

            } catch (SQLException e) {
                    e.printStackTrace();
                    //////System.out.println("Table doesn't exist.");
                }
            con.close();
             }  catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            String exp = e.toString();
            //////System.out.println("Unable to send Email....");
             TripTO tripTO = new TripTO();
            int userId = 1081;
            Status = 0;
            Status = 1;
            String fileName = "jdbc_url.properties";
            Properties dbProps = new Properties();
            InputStream is = getClass().getResourceAsStream("/"+fileName);
            try{
            dbProps.load(is);//this may throw IOException
            }catch(Exception ex){
                ex.printStackTrace();
            }
            String dbClassName = dbProps.getProperty("jdbc.driverClassName");
            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");
            int updateStatus = 0;
            Connection con = null;
            try {
                Class.forName(dbClassName).newInstance();
                con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
                try {
                    PreparedStatement updateMailDetails = null;
                    String sql = "INSERT INTO \n"
                            + "ts_mail_master (Mail_Type_Id, Mail_Subject_TO, Mail_Content_TO, Mail_Id_TO, Mail_Id_CC, Mail_Deliveried_Status, Active_Ind, Exception_Occured, Created_By, Created_On)  \n"
                            + "VALUES(?,?,?,?,?,?,?,?,?,now())";
                    updateMailDetails = con.prepareStatement(sql);
                    updateMailDetails.setInt(1, 2);
                    updateMailDetails.setString(2, EmailSubject);
                    updateMailDetails.setString(3, BodyMessage);
                    updateMailDetails.setString(4, Toemailid);
                    updateMailDetails.setString(5, CCemailid);
                    updateMailDetails.setInt(6, 0);
                    updateMailDetails.setString(7, "Y");
                    updateMailDetails.setString(8, exp);
                    updateMailDetails.setInt(9, MailSendingId);
                    updateStatus = updateMailDetails.executeUpdate();
                    //////System.out.println("updateStatus = " + updateStatus);

                } catch (SQLException ex) {
                    e.printStackTrace();
                    //////System.out.println("Table doesn't exist.");
                }
                con.close();
            } catch (Exception ex) {
                e.printStackTrace();
            }
        }
        
        
    }

    public static void main(String args[]) throws IOException, EmailException {

        String subject = "Entitle";
        String content = "Welcome";

        String to = "aruninfoit84@gmail.com";
        String cc = "";
        String smtp = "smtp.gmail.com";
        int emailPort = 587;
//        String frommailid = "transport@brattlefoods.com";
//        String password = "Brattle@12345";
        String frommailid = "support@entitlesolutions.com";
        String password = "admin123";
        int mailSendingId = 0 ;

        new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc,mailSendingId).start();

    }
}
