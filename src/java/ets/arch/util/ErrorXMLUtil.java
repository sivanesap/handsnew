/*----------------------------------------------------------------------------
 * ErrorXMLUtil.java
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
 ---------------------------------------------------------------------------*/
package ets.arch.util;
/**
 * Modification Log
 * ---------------------------------------------------------------------
 * Ver   Date              Modified By               Description
 * ---------------------------------------------------------------------
 * 1.0   26 Mar 2008       Srinivasan.R             Initial Version
 *
 ***********************************************************************/

import java.util.HashMap;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ets.arch.exception.ErrorDefinition;
import ets.arch.exception.FPResourceManager;
import ets.domain.util.FPLogUtils;

/**
 *
 * ErrorXMLUtil:This class has methods specific to loading ExceptionXML.
 * When the getInstance() method is called for the first time,
 * a public static instance is created. This is a singleton.
 * From the next call, the very same instance is returned across
 * the application.
 *
 * @author Srinivasan.R
 * @version 1.00 26 Mar 2008
 * @since   event Portal Iteration 0
 */


public class ErrorXMLUtil extends XMLUtil {

     //exceptions.xml constants
     private static final String EXCEPTION = "exception";
     private static final String ERROR_CODE = "Cd";
     private static final String ERROR_MESSAGE = "ErMsg";
     private static final String ERROR_CAUSE = "Cause";
     private static final String ERROR_RECOVERY = "Recovery";
     private static final String ERROR_SEVERITY = "Severity";
     private static final int DEFAULT_HASHMAP_SIZE = 100;
     // public static HashMap exceptionMappings;
     private HashMap exceptionMappings;

   /* Use this whenever you want to log info into FPtrace.log file.
      This will be used in all the codes to aid debugging*/



     private static ErrorXMLUtil errorXMLUtilInstance = new ErrorXMLUtil();



     private ErrorXMLUtil() {
        initializeData();
     }

   /**
     * Gets the instance of this class.
     * When this is called for the first time, it initializes a local hashmap.
     * @return ErrorXMLUtil errorXMLUtilInstance
     * This represents an instance of this class
     *
     */
     public static ErrorXMLUtil getInstance() {
         return errorXMLUtilInstance;
     }

   /**
     * For the first time, The XML has to be loaded. Hence this is called.
     */
     private void initializeData() {

         String exceptionMappingsUrl = null;

      // get the exception filename from the properties file.
      String  exceptFileName=
               FPResourceManager.getInstance().getProperties("exceptionFileName");
         //Following 2 lines were commented and 3rd new line was added to test
      //it in a non web contatiner environment
         FPLogUtils.fpDebugLog("exceptFileName:"+exceptFileName);
         exceptionMappingsUrl = Thread.currentThread().getContextClassLoader().getResource(exceptFileName).toString();
         FPLogUtils.fpDebugLog("exceptionMappingsUrl:"+exceptionMappingsUrl);
         setExceptionMappings(loadExceptionDefinitions(exceptionMappingsUrl));
         //setExceptionMappings(loadExceptionDefinitions(exceptFileName));

      }


   /**
     * This Loads all the Exception Definitions.
     * @param location - location
     * This represents path from where the xml is to be loaded.
     * @return HashMap   The HashMap containing all the Error XML dace.
     * This mapping has the Error No as the key and the remaining object
     * is of the type errorDefinition.
     *
     */
     private HashMap loadExceptionDefinitions(String location){

        Element root = loadDocument(location);
        return getExceptionDefinitions(root);

     }

   /**
     * This gets all the Exception Definitions.
     * @param Element root    This represents the complete tree
     *  structure representing the XML.
     * @return HashMap        The HashMap containing all the Error XML dace.
     * This mapping has the Error No as the key and
     * the remaining object is of the type errorDefinition.
     *
     */
     private HashMap getExceptionDefinitions(Element root) {

        int exceptionListLength = 0;

        HashMap exceptionMappings = new HashMap(DEFAULT_HASHMAP_SIZE);

        NodeList exceptionList = root.getElementsByTagName(EXCEPTION);

        exceptionListLength = exceptionList.getLength();

        for (int exceptionLoop = 0; exceptionLoop < exceptionListLength; exceptionLoop++) {
           Node exceptionNode = exceptionList.item(exceptionLoop);

           ErrorDefinition errorDefinition =
               new ErrorDefinition(/*errorCode, errorMessage, errorCause,
                                     errorRecovery, errorSeverity*/);

           if (exceptionNode != null) {
              //exception attribute details
            errorDefinition.setErrorCode(getSubTagValue(exceptionNode, ERROR_CODE));
            errorDefinition.setErrorMessage(getSubTagValue(exceptionNode, ERROR_MESSAGE));
            errorDefinition.setErrorCause(getSubTagValue(exceptionNode, ERROR_CAUSE));
            errorDefinition.setErrorRecovery(getSubTagValue(exceptionNode, ERROR_RECOVERY));
            errorDefinition.setErrorSeverity(getSubTagValue(exceptionNode, ERROR_SEVERITY));
           }
              //HashMap of errorCode and mapping
           exceptionMappings.put(errorDefinition.getErrorCode(),errorDefinition);
        }
      //Writing into trace file.
        return exceptionMappings;
     }

      /**
      * Returns the exceptionMappings HashMap
      * @return HashMap exceptionMappings
      */
      public HashMap getExceptionMappings () {
         return exceptionMappings;
      }
      /**
      * Sets the value of exceptionMappings
      * @param  exceptionMappingsIn - Exception Mapping
      */
      public void setExceptionMappings (HashMap exceptionMappingsIn) {
         exceptionMappings = new HashMap(DEFAULT_HASHMAP_SIZE);
         exceptionMappings = exceptionMappingsIn;
      }

}
