/*----------------------------------------------------------------------------
 * ErrorDefinition.java
 * Jan 26, 2008
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
 ---------------------------------------------------------------------------*/
package ets.arch.exception;
/**
*
* Modification Log
* ---------------------------------------------------------------------
* Ver   Date              Modified By               Description
* ---------------------------------------------------------------------
* 1.0   26 Jan 2008       Srinivasan.R             Initial Version
*
***********************************************************************/

/**
*
* ErrorDefinition:This holds the Error Definition details as defnied in exception.xml
* Used for creating the Hash Map when loading the exception.xml
* @author Srinivasan Ramakrishnan
* @version 1.00 26 Jan 2008
* @since   Event Portal Iteration 0
*/
public class ErrorDefinition {

   // The error Code
   private String errorCode;

   //The error name
   //private String errorName;

   //The error cause
   private String errorCause;

   //The error recovery
   private String errorRecovery;

   //The error Severity
   private String errorSeverity;

   //The error message
   private String errorMessage;

   /**
    * constructor
    */
   public ErrorDefinition() {
   }

   /**
    * constructor
    * @param   errorCode       The error code to identify the error
    * @param   errorMessage    The explanation of the error
    * @param   errorCause      The cause of the error
    * @param   errorRecovery   The recovery step to be taken
    * @param   errorSeverity   The severity of the error message
    */
   public ErrorDefinition(String errorCode, String errorMessage,
		   String  errorCause,String errorRecovery, String errorSeverity) {
      this.errorCode = errorCode;
      this.errorMessage = errorMessage;
      this.errorCause = errorCause;
      this.errorRecovery = errorRecovery;
      this.errorSeverity = errorSeverity;
   }
   /**
   * Adds parameters which substitute placeholders in the message
   * @param parameter The parameter to be added
   */
   public void addParameter(Object parameter) {
   }
   /**
   * Returns the error Cause read from the Dictionary
   * @return Cause of the exception
   */
   public String getErrorCause () {
      return errorCause;
   }
   /**
   * Returns the error Code read from the Dictionary
   * @return Error Code
   */
   public String getErrorCode() {
      return errorCode;
   }
   /**
   * Returns the error message from the Dictionary
   * @return Message associated with the exception
   */
   public String getErrorMessage () {
      return errorMessage;
   }

   /**
   * Returns the error recovery from the Dictionary
   * @return Action to be performed to recover from the exception
   */
   public String getErrorRecovery () {
      return errorRecovery;
   }
   /**
   * Returns the error Severity from the Dictionary
   * @return Severity of the exception
   */
   public String getErrorSeverity () {
      return errorSeverity;
   }
   /**
   * Sets the error Cause with values read from the Dictionary
   * @param  errorCause - Cause of the exception
   *@return void
   */
   public void setErrorCause (String errorCause) {
      this.errorCause = errorCause;
   }
   /**
   * Sets the error Code with values read from the Dictionary
   * @param errorCode Error code associated with the exception
   *@return void
   */
   public void setErrorCode(String errorCode) {
      this.errorCode = errorCode;
   }
   /**
   * Sets the error message with values read from the Dictionary
   * @param errorMessage - Error message
   * @return void
   */
   public void setErrorMessage (String errorMessage) {
      this.errorMessage = errorMessage;
   }

   /**
   * Sets the error recovery with values read from the Dictionary
   * @param    errorRecovery Action to be performed to recover from the
   * exception
   *@return void
   */
   public void setErrorRecovery (String errorRecovery) {
      this.errorRecovery = errorRecovery;
   }
   /**
   * Sets the error Severity with values read from the Dictionary
   * @param    errorSeverity The Error severity ranges from 1 to 5 . 1 being
   * lowest while 5 being the highest

   *@return void
   */
   public void setErrorSeverity (String errorSeverity) {
      this.errorSeverity = errorSeverity;
   }
}
