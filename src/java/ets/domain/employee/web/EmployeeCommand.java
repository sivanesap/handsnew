/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.employee.web;

import java.sql.Blob;

/**
 *
 * @author karudaiyar Subramaniam
 */
public class EmployeeCommand {
    public EmployeeCommand() {
    }
    private byte[] driverPhotoFileAsByteArray;
        private byte[] driverLicenseFileAsByteArray;
        private Blob driverLicenseFile;
        private Blob driverPhotoFile;
        private String photoFile = "";
        private String licenseFile = "";
        private String bankName = "";
        private String bankBranchName = "";
    private String accountHolderName = "";
    private String empCode = "";
    private String remarks = "";
    private String desigName = "";
    private String[] desigIds = null;
    private String[] desigNames = null;
    private String[] activeInds = null;
    private String activeInd = null;
    private String[] selectedIndex = null;
    private String  designationId = "";
    private String gradeName = "";
    private String description = "";
    private String[] gradeIds = null;
    private String[] gradeNames = null;
    private String[] descriptions = null;
    private String desigId = "";
    private String deptId = "";
    private String deptName = "";
    private String deptCode = "";
    private String Description = "";
    private String[] deptCodes = null;
    private String[] deptNames = null;
    private String[] desc = null;
    private String fromMonth = "";
    private String endMonth = "";
    private String semName = "";
    private String[] semesterId = null;
    private String[] semesteNames = null;
    private String[] fromMonths = null;
    private String[] endMonths = null;
    private String courseCode = "";
    private String courseName = "";
    private String duration = "";
    private String totSems = "";
    private String deparmentId = "";
    private String[] courseIds = null;
    private String[] courseCodes = null;
    private String[] courseNames = null;
    private String[] totalSems = null;
    private String[] courseDurations = null;
    private String subjectCode = "";
    private String subjectName = "";
    private String subjectType = "";
    private String[] subjectIds = null;
    private String[] subjectCodes = null;
    private String[] subjectNames = null;
    private String[] subjectTypes = null;
    private String courseId = "";
    private String name = "";
    private String qualification = "";
    private String dateOfBirth = "";
    private String orgEmpId = "";
    private String dateOfJoining = "";
    private String gender = "";
    private String bloodGrp = "";
    private String martialStatus = "";
    private String fatherName = "";
    private String mobile = "";
    private String phone = "";
    private String email = "";
    private String address = "";
    private String city = "";
    private String state = "";
    private String pincode = "";
    private String address1 = "";
    private String city1 = "";
    private String state1 = "";
    private String pincode1 = "";
    private String subjectId = "";
    private String batchName = "";
    private int userId = 0;    
    private String[] batchIds = null;
    private String father_Name = "";
    private String DOB = "";
    private String DOJ = "";
    private String blood_Grp = "";
    private String martial_Status = "";
    private String addr = "";
    private String addr1 = "";
    private String gradeId = "";
    private String departmentId = "";
    private String userName = "";
    private String password = "";
    private String deptid = "";
    private String staffid = "";
    private String staffname = "";
    private String dateofBirth = "";
    private String departId = "";
    private String subTypeName = "";
    private String subTypeId = "";
    private String staffId = "";
    private String staffNames = "";
    private String[] subTypeIds = null;
    private String[] subTypeNames = null;
    private String staffName = null;
    private String[] assignedSem = null;
    private String[] assignedSubs = null;
    private String semId="";
    private String semeId="";
    private String[] mandatorys= null;

    
    private String[] electives= null;
    private String[] practicals= null;
    private String[] projects=null;
    private String roleId = "";
    private String entryTypeId = "";
    private String quotaId = "";
    private String certId = "";
    private String certName = "";
    private String credit = "";
    private String[] credits = null;
    private String batchId = "";
    private String period = "";
    private String[] periods = null;
    private String batId= "";    
    private String electiveId = "";
    private String elec = "";
    private String practical = "";
    private String  lecture= "";        
    private String tutorial = "";      
    private String[]  lectures= null;        
    private String[] tutorials = null;
    private String[] semIds =null;
    private String cmpId ="";
    private String userRoleId ="";

    private String referenceName ="";
    private String terminate ="";
    
    
    private String licenseExpDate = "";
    private String drivingLicenseNo = "";
    private String licenseType = "";
    private String licenseState = "";
    private String driverType = "";



    private String salaryType = "";
    float basicSalary = 0.00f;
    private String esiEligible = "";
    private String pfEligible = "";
    private String nomineeName = "";
    private String nomineeRelation = "";
    private int nomineeAge = 0;


    private int yearOfExperience = 0;
    private String preEmpDesignation = "";
    private String prevCompanyName = "";
    private String prevCompanyAddress = "";
    private String prevCompanyCity = "";
    private String preEmpState = "";
    private String preEmpPincode = "";
    private String prevCompanyContact = "";

     private String nomineeDob = "";
    private String vendorCompany = "";
    private String[] relationName = null;
    private String[] relation = null;
    private String[] relationDOB = null;
    private String[] relationAge = null;
    private String staffUserId = "";
    private String chec = "";
    private String[] empRelationId = null;
    private String empRelationSize = "";
    private String vehicleType = "";
    private String vehicleCategory = "";
    private String tripType = "";
    private String salaryStructureId = "";
    private String fromKm = "";
    private String toKm = "";
    private String kmAmount = "";
    private String extraKmAmount = "";
    private String fromHours = "";
    private String toHours = "";
    private String hoursAmount = "";
    private String extraHoursAmount = "";
    private String perDayAmount = "";
    private String staffCode = "";

    private String contractDriver = "";
    private String bankAccountNo = "";


    public String getReferenceName() {
        return referenceName;
    }

    public void setReferenceName(String referenceName) {
        this.referenceName = referenceName;
    }

    public String getTerminate() {
        return terminate;
    }

    public void setTerminate(String terminate) {
        this.terminate = terminate;
    }

    

    public String getDesigName() {
        return desigName;
    }

    public void setDesigName(String desigName) {
        this.desigName = desigName;
    }

    public String[] getDesigIds() {
        return desigIds;
    }

    public void setDesigIds(String[] desigIds) {
        this.desigIds = desigIds;
    }

    public String[] getDesigNames() {
        return desigNames;
    }

    public void setDesigNames(String[] desigNames) {
        this.desigNames = desigNames;
    }

    public String[] getActiveInds() {
        return activeInds;
    }

    public void setActiveInds(String[] activeInds) {
        this.activeInds = activeInds;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getDesignationId() {
        return designationId;
    }

    public void setDesignationId(String designationId) {
        this.designationId = designationId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getGradeIds() {
        return gradeIds;
    }

    public void setGradeIds(String[] gradeIds) {
        this.gradeIds = gradeIds;
    }

    public String[] getGradeNames() {
        return gradeNames;
    }

    public void setGradeNames(String[] gradeNames) {
        this.gradeNames = gradeNames;
    }

    public String[] getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String[] descriptions) {
        this.descriptions = descriptions;
    }

    public String getDesigId() {
        return desigId;
    }

    public void setDesigId(String desigId) {
        this.desigId = desigId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

     

    public String[] getDeptCodes() {
        return deptCodes;
    }

    public void setDeptCodes(String[] deptCodes) {
        this.deptCodes = deptCodes;
    }

    public String[] getDeptNames() {
        return deptNames;
    }

    public void setDeptNames(String[] deptNames) {
        this.deptNames = deptNames;
    }

    public String[] getDesc() {
        return desc;
    }

    public void setDesc(String[] desc) {
        this.desc = desc;
    }

    public String getFromMonth() {
        return fromMonth;
    }

    public void setFromMonth(String fromMonth) {
        this.fromMonth = fromMonth;
    }

    public String getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(String endMonth) {
        this.endMonth = endMonth;
    }

    public String getSemName() {
        return semName;
    }

    public void setSemName(String semName) {
        this.semName = semName;
    }

    public String[] getSemesterId() {
        return semesterId;
    }

    public void setSemesterId(String[] semesterId) {
        this.semesterId = semesterId;
    }

    public String[] getSemesteNames() {
        return semesteNames;
    }

    public void setSemesteNames(String[] semesteNames) {
        this.semesteNames = semesteNames;
    }

    public String[] getFromMonths() {
        return fromMonths;
    }

    public void setFromMonths(String[] fromMonths) {
        this.fromMonths = fromMonths;
    }

    public String[] getEndMonths() {
        return endMonths;
    }

    public void setEndMonths(String[] endMonths) {
        this.endMonths = endMonths;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getTotSems() {
        return totSems;
    }

    public void setTotSems(String totSems) {
        this.totSems = totSems;
    }

    public String getDeparmentId() {
        return deparmentId;
    }

    public void setDeparmentId(String deparmentId) {
        this.deparmentId = deparmentId;
    }

    public String[] getCourseIds() {
        return courseIds;
    }

    public void setCourseIds(String[] courseIds) {
        this.courseIds = courseIds;
    }

    public String[] getCourseCodes() {
        return courseCodes;
    }

    public void setCourseCodes(String[] courseCodes) {
        this.courseCodes = courseCodes;
    }

    public String[] getCourseNames() {
        return courseNames;
    }

    public void setCourseNames(String[] courseNames) {
        this.courseNames = courseNames;
    }

    public String[] getTotalSems() {
        return totalSems;
    }

    public void setTotalSems(String[] totalSems) {
        this.totalSems = totalSems;
    }

    public String[] getCourseDurations() {
        return courseDurations;
    }

    public void setCourseDurations(String[] courseDurations) {
        this.courseDurations = courseDurations;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(String subjectType) {
        this.subjectType = subjectType;
    }

    public String[] getSubjectIds() {
        return subjectIds;
    }

    public void setSubjectIds(String[] subjectIds) {
        this.subjectIds = subjectIds;
    }

    public String[] getSubjectCodes() {
        return subjectCodes;
    }

    public void setSubjectCodes(String[] subjectCodes) {
        this.subjectCodes = subjectCodes;
    }

    public String[] getSubjectNames() {
        return subjectNames;
    }

    public void setSubjectNames(String[] subjectNames) {
        this.subjectNames = subjectNames;
    }

    public String[] getSubjectTypes() {
        return subjectTypes;
    }

    public void setSubjectTypes(String[] subjectTypes) {
        this.subjectTypes = subjectTypes;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDateOfJoining() {
        return dateOfJoining;
    }

    public void setDateOfJoining(String dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBloodGrp() {
        return bloodGrp;
    }

    public void setBloodGrp(String bloodGrp) {
        this.bloodGrp = bloodGrp;
    }

    public String getMartialStatus() {
        return martialStatus;
    }

    public void setMartialStatus(String martialStatus) {
        this.martialStatus = martialStatus;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getCity1() {
        return city1;
    }

    public void setCity1(String city1) {
        this.city1 = city1;
    }

    public String getState1() {
        return state1;
    }

    public void setState1(String state1) {
        this.state1 = state1;
    }

    public String getPincode1() {
        return pincode1;
    }

    public void setPincode1(String pincode1) {
        this.pincode1 = pincode1;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String[] getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(String[] batchIds) {
        this.batchIds = batchIds;
    }

    public String getFather_Name() {
        return father_Name;
    }

    public void setFather_Name(String father_Name) {
        this.father_Name = father_Name;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getDOJ() {
        return DOJ;
    }

    public void setDOJ(String DOJ) {
        this.DOJ = DOJ;
    }

    public String getBlood_Grp() {
        return blood_Grp;
    }

    public void setBlood_Grp(String blood_Grp) {
        this.blood_Grp = blood_Grp;
    }

    public String getMartial_Status() {
        return martial_Status;
    }

    public void setMartial_Status(String martial_Status) {
        this.martial_Status = martial_Status;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeptid() {
        return deptid;
    }

    public void setDeptid(String deptid) {
        this.deptid = deptid;
    }

    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    public String getStaffname() {
        return staffname;
    }

    public void setStaffname(String staffname) {
        this.staffname = staffname;
    }

    public String getDateofBirth() {
        return dateofBirth;
    }

    public void setDateofBirth(String dateofBirth) {
        this.dateofBirth = dateofBirth;
    }

    public String getDepartId() {
        return departId;
    }

    public void setDepartId(String departId) {
        this.departId = departId;
    }

    public String getSubTypeName() {
        return subTypeName;
    }

    public void setSubTypeName(String subTypeName) {
        this.subTypeName = subTypeName;
    }

    public String getSubTypeId() {
        return subTypeId;
    }

    public void setSubTypeId(String subTypeId) {
        this.subTypeId = subTypeId;
    }

 public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffNames() {
        return staffNames;
    }

    public void setStaffNames(String staffNames) {
        this.staffNames = staffNames;
    }

    public String getCmpId() {
        return cmpId;
    }

    public void setCmpId(String cmpId) {
        this.cmpId = cmpId;
    }

    public String getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(String userRoleId) {
        this.userRoleId = userRoleId;
    }

    public String[] getAssignedSem() {
        return assignedSem;
    }

    public void setAssignedSem(String[] assignedSem) {
        this.assignedSem = assignedSem;
    }

    public String[] getAssignedSubs() {
        return assignedSubs;
    }

    public void setAssignedSubs(String[] assignedSubs) {
        this.assignedSubs = assignedSubs;
    }

    public float getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(float basicSalary) {
        this.basicSalary = basicSalary;
    }

    public String getBatId() {
        return batId;
    }

    public void setBatId(String batId) {
        this.batId = batId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getCertId() {
        return certId;
    }

    public void setCertId(String certId) {
        this.certId = certId;
    }

    public String getCertName() {
        return certName;
    }

    public void setCertName(String certName) {
        this.certName = certName;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String[] getCredits() {
        return credits;
    }

    public void setCredits(String[] credits) {
        this.credits = credits;
    }

    public String getDriverType() {
        return driverType;
    }

    public void setDriverType(String driverType) {
        this.driverType = driverType;
    }

    public String getDrivingLicenseNo() {
        return drivingLicenseNo;
    }

    public void setDrivingLicenseNo(String drivingLicenseNo) {
        this.drivingLicenseNo = drivingLicenseNo;
    }

    public String getLicenseExpDate() {
        return licenseExpDate;
    }

    public void setLicenseExpDate(String licenseExpDate) {
        this.licenseExpDate = licenseExpDate;
    }

    public String getElec() {
        return elec;
    }

    public void setElec(String elec) {
        this.elec = elec;
    }

    public String getElectiveId() {
        return electiveId;
    }

    public void setElectiveId(String electiveId) {
        this.electiveId = electiveId;
    }

    public String[] getElectives() {
        return electives;
    }

    public void setElectives(String[] electives) {
        this.electives = electives;
    }

    public String getEntryTypeId() {
        return entryTypeId;
    }

    public void setEntryTypeId(String entryTypeId) {
        this.entryTypeId = entryTypeId;
    }

    public String getEsiEligible() {
        return esiEligible;
    }

    public void setEsiEligible(String esiEligible) {
        this.esiEligible = esiEligible;
    }

    public String getLecture() {
        return lecture;
    }

    public void setLecture(String lecture) {
        this.lecture = lecture;
    }

    public String[] getLectures() {
        return lectures;
    }

    public void setLectures(String[] lectures) {
        this.lectures = lectures;
    }

    public String getLicenseState() {
        return licenseState;
    }

    public void setLicenseState(String licenseState) {
        this.licenseState = licenseState;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String[] getMandatorys() {
        return mandatorys;
    }

    public void setMandatorys(String[] mandatorys) {
        this.mandatorys = mandatorys;
    }

    public int getNomineeAge() {
        return nomineeAge;
    }

    public void setNomineeAge(int nomineeAge) {
        this.nomineeAge = nomineeAge;
    }

    public String getNomineeName() {
        return nomineeName;
    }

    public void setNomineeName(String nomineeName) {
        this.nomineeName = nomineeName;
    }

    public String getNomineeRelation() {
        return nomineeRelation;
    }

    public void setNomineeRelation(String nomineeRelation) {
        this.nomineeRelation = nomineeRelation;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String[] getPeriods() {
        return periods;
    }

    public void setPeriods(String[] periods) {
        this.periods = periods;
    }

    public String getPfEligible() {
        return pfEligible;
    }

    public void setPfEligible(String pfEligible) {
        this.pfEligible = pfEligible;
    }

    public String getPractical() {
        return practical;
    }

    public void setPractical(String practical) {
        this.practical = practical;
    }

    public String[] getPracticals() {
        return practicals;
    }

    public void setPracticals(String[] practicals) {
        this.practicals = practicals;
    }

    public String getPreEmpDesignation() {
        return preEmpDesignation;
    }

    public void setPreEmpDesignation(String preEmpDesignation) {
        this.preEmpDesignation = preEmpDesignation;
    }

    public String getPreEmpPincode() {
        return preEmpPincode;
    }

    public void setPreEmpPincode(String preEmpPincode) {
        this.preEmpPincode = preEmpPincode;
    }

    public String getPreEmpState() {
        return preEmpState;
    }

    public void setPreEmpState(String preEmpState) {
        this.preEmpState = preEmpState;
    }

    public String getPrevCompanyAddress() {
        return prevCompanyAddress;
    }

    public void setPrevCompanyAddress(String prevCompanyAddress) {
        this.prevCompanyAddress = prevCompanyAddress;
    }

    public String getPrevCompanyCity() {
        return prevCompanyCity;
    }

    public void setPrevCompanyCity(String prevCompanyCity) {
        this.prevCompanyCity = prevCompanyCity;
    }

    public String getPrevCompanyContact() {
        return prevCompanyContact;
    }

    public void setPrevCompanyContact(String prevCompanyContact) {
        this.prevCompanyContact = prevCompanyContact;
    }

    public String getPrevCompanyName() {
        return prevCompanyName;
    }

    public void setPrevCompanyName(String prevCompanyName) {
        this.prevCompanyName = prevCompanyName;
    }

    public String[] getProjects() {
        return projects;
    }

    public void setProjects(String[] projects) {
        this.projects = projects;
    }

    public String getQuotaId() {
        return quotaId;
    }

    public void setQuotaId(String quotaId) {
        this.quotaId = quotaId;
    }

    public String getSalaryType() {
        return salaryType;
    }

    public void setSalaryType(String salaryType) {
        this.salaryType = salaryType;
    }

    public String getSemId() {
        return semId;
    }

    public void setSemId(String semId) {
        this.semId = semId;
    }

    public String[] getSemIds() {
        return semIds;
    }

    public void setSemIds(String[] semIds) {
        this.semIds = semIds;
    }

    public String getSemeId() {
        return semeId;
    }

    public void setSemeId(String semeId) {
        this.semeId = semeId;
    }

    public String[] getSubTypeIds() {
        return subTypeIds;
    }

    public void setSubTypeIds(String[] subTypeIds) {
        this.subTypeIds = subTypeIds;
    }

    public String[] getSubTypeNames() {
        return subTypeNames;
    }

    public void setSubTypeNames(String[] subTypeNames) {
        this.subTypeNames = subTypeNames;
    }

    public String getTutorial() {
        return tutorial;
    }

    public void setTutorial(String tutorial) {
        this.tutorial = tutorial;
    }

    public String[] getTutorials() {
        return tutorials;
    }

    public void setTutorials(String[] tutorials) {
        this.tutorials = tutorials;
    }

    public int getYearOfExperience() {
        return yearOfExperience;
    }

    public void setYearOfExperience(int yearOfExperience) {
        this.yearOfExperience = yearOfExperience;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public String getChec() {
        return chec;
    }

    public void setChec(String chec) {
        this.chec = chec;
    }

    public String[] getEmpRelationId() {
        return empRelationId;
    }

    public void setEmpRelationId(String[] empRelationId) {
        this.empRelationId = empRelationId;
    }

    public String getEmpRelationSize() {
        return empRelationSize;
    }

    public void setEmpRelationSize(String empRelationSize) {
        this.empRelationSize = empRelationSize;
    }

    public String getExtraHoursAmount() {
        return extraHoursAmount;
    }

    public void setExtraHoursAmount(String extraHoursAmount) {
        this.extraHoursAmount = extraHoursAmount;
    }

    public String getExtraKmAmount() {
        return extraKmAmount;
    }

    public void setExtraKmAmount(String extraKmAmount) {
        this.extraKmAmount = extraKmAmount;
    }

    public String getFromHours() {
        return fromHours;
    }

    public void setFromHours(String fromHours) {
        this.fromHours = fromHours;
    }

    public String getFromKm() {
        return fromKm;
    }

    public void setFromKm(String fromKm) {
        this.fromKm = fromKm;
    }

    public String getHoursAmount() {
        return hoursAmount;
    }

    public void setHoursAmount(String hoursAmount) {
        this.hoursAmount = hoursAmount;
    }

    public String getKmAmount() {
        return kmAmount;
    }

    public void setKmAmount(String kmAmount) {
        this.kmAmount = kmAmount;
    }

    public String getNomineeDob() {
        return nomineeDob;
    }

    public void setNomineeDob(String nomineeDob) {
        this.nomineeDob = nomineeDob;
    }

    public String getPerDayAmount() {
        return perDayAmount;
    }

    public void setPerDayAmount(String perDayAmount) {
        this.perDayAmount = perDayAmount;
    }

    public String[] getRelation() {
        return relation;
    }

    public void setRelation(String[] relation) {
        this.relation = relation;
    }

    public String[] getRelationAge() {
        return relationAge;
    }

    public void setRelationAge(String[] relationAge) {
        this.relationAge = relationAge;
    }

    public String[] getRelationDOB() {
        return relationDOB;
    }

    public void setRelationDOB(String[] relationDOB) {
        this.relationDOB = relationDOB;
    }

    public String[] getRelationName() {
        return relationName;
    }

    public void setRelationName(String[] relationName) {
        this.relationName = relationName;
    }

    public String getSalaryStructureId() {
        return salaryStructureId;
    }

    public void setSalaryStructureId(String salaryStructureId) {
        this.salaryStructureId = salaryStructureId;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public String getStaffUserId() {
        return staffUserId;
    }

    public void setStaffUserId(String staffUserId) {
        this.staffUserId = staffUserId;
    }

    public String getToHours() {
        return toHours;
    }

    public void setToHours(String toHours) {
        this.toHours = toHours;
    }

    public String getToKm() {
        return toKm;
    }

    public void setToKm(String toKm) {
        this.toKm = toKm;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getVehicleCategory() {
        return vehicleCategory;
    }

    public void setVehicleCategory(String vehicleCategory) {
        this.vehicleCategory = vehicleCategory;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVendorCompany() {
        return vendorCompany;
    }

    public void setVendorCompany(String vendorCompany) {
        this.vendorCompany = vendorCompany;
    }

    public String getContractDriver() {
        return contractDriver;
    }

    public void setContractDriver(String contractDriver) {
        this.contractDriver = contractDriver;
    }

    public String getBankAccountNo() {
        return bankAccountNo;
    }

    public void setBankAccountNo(String bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getBankBranchName() {
        return bankBranchName;
    }

    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Blob getDriverLicenseFile() {
        return driverLicenseFile;
    }

    public void setDriverLicenseFile(Blob driverLicenseFile) {
        this.driverLicenseFile = driverLicenseFile;
    }

    public byte[] getDriverLicenseFileAsByteArray() {
        return driverLicenseFileAsByteArray;
    }

    public void setDriverLicenseFileAsByteArray(byte[] driverLicenseFileAsByteArray) {
        this.driverLicenseFileAsByteArray = driverLicenseFileAsByteArray;
    }

    public Blob getDriverPhotoFile() {
        return driverPhotoFile;
    }

    public void setDriverPhotoFile(Blob driverPhotoFile) {
        this.driverPhotoFile = driverPhotoFile;
    }

    public byte[] getDriverPhotoFileAsByteArray() {
        return driverPhotoFileAsByteArray;
    }

    public void setDriverPhotoFileAsByteArray(byte[] driverPhotoFileAsByteArray) {
        this.driverPhotoFileAsByteArray = driverPhotoFileAsByteArray;
    }

    public String getLicenseFile() {
        return licenseFile;
    }

    public void setLicenseFile(String licenseFile) {
        this.licenseFile = licenseFile;
    }

    public String getPhotoFile() {
        return photoFile;
    }

    public void setPhotoFile(String photoFile) {
        this.photoFile = photoFile;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getOrgEmpId() {
        return orgEmpId;
    }

    public void setOrgEmpId(String orgEmpId) {
        this.orgEmpId = orgEmpId;
    }
    
    
}
