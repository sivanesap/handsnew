/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.scrap.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.scrap.data.ScrapDAO;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author karudaiyar Subramaniam
 */
public class ScrapBP {
    
    private ScrapDAO scrapDAO;
    int nosAmount = 0;
    
    public ScrapDAO getScrapDAO() {
        return scrapDAO;
    }

    public void setScrapDAO(ScrapDAO scrapDAO) {
        this.scrapDAO = scrapDAO;
    }
    
    /**
     * This method used to Insert Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises    -m
     */
     public ArrayList processDisposeInList() throws FPRuntimeException, FPBusinessException {
        ArrayList disposeInList = new ArrayList();
        disposeInList = (ArrayList) scrapDAO.doDisposeInList();
        if (disposeInList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return disposeInList;
    }
    
 
   /**
     * This method used to Insert Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
     public int processSaveScrap(int kgRate,int totalAmountAll,ArrayList kgAmountUnique,ArrayList noAmountUnique,int userId,int companyId,ScrapTO scrapTO) throws FPRuntimeException, FPBusinessException {
        int status=0;
        status = (int) scrapDAO.doDisposeAllList(kgRate,totalAmountAll,kgAmountUnique,noAmountUnique,userId,companyId,scrapTO);
        if (status  == 0) {
            throw new FPBusinessException("EM-SCR-01");
        }
        return status;
    }   
     
     //processSectionWiseList
      /**
     * This method used to Insert Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
     public ArrayList processSectionWiseList(ScrapTO scrapTO  ) throws FPRuntimeException, FPBusinessException {
       ArrayList sectionWiseList = new ArrayList();
        sectionWiseList = (ArrayList) scrapDAO.doSectionWiseList(scrapTO);
        if (sectionWiseList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return sectionWiseList;
    }  
     
     public ArrayList scrapApprovalItems(ScrapTO scrapTO  ) throws FPRuntimeException, FPBusinessException {
       ArrayList scrapItems = new ArrayList();
        scrapItems = (ArrayList) scrapDAO.getScrapApprovalItems(scrapTO);
        if (scrapItems.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
        }
        return scrapItems;
    }  
     
     public int processScrapQty(ScrapTO scrapTO  , int userId) throws FPRuntimeException, FPBusinessException {
       int scrapQty = 0;
        scrapQty = scrapDAO.doScrapItemsQty(scrapTO,userId);
        return scrapQty;
    }  
     
     public int processRejectScrapReq(ScrapTO scrapTO  , int userId) throws FPRuntimeException, FPBusinessException {
       int scrapQty = 0;
        scrapQty = scrapDAO.doRejectScrapReq(scrapTO,userId);
        return scrapQty;
    }  
     
     public int processdoScrapReturn(ScrapTO scrapTO  , int userId) throws FPRuntimeException, FPBusinessException {
       int scrapQty = 0;
        scrapQty = scrapDAO.doScrapReturnStock(scrapTO,userId);
        return scrapQty;
    }  
     
}
