/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.scrap.web;

/**
 *
 * @author karudaiyar Subramaniam
 */
public class ScrapCommand {
    
     String sectionId="";
     String sectionName="";
     String paplCode="";
     String itemName="";
     String uomName="";  
     String unitPrice="";
     String quandity="";
     String servicePointName="";
     String servicePointId="";
     String compId="";
     String compName="";
     String kgunitPrice="";
     String scrapId="";
     String kgTotalWeight="";
     String kgRate="";
     String toMakeKgTotal="";
     String totalAmountAll="";
      String uomId="";
      
      
               int kgTotalWg=0;
               int kgTotalAmount=0;
      
      
      
      
    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }
      
    public String getKgRate() {
        return kgRate;
    }

    public void setKgRate(String kgRate) {
        this.kgRate = kgRate;
    }

    public String getKgTotalWeight() {
        return kgTotalWeight;
    }

    public void setKgTotalWeight(String kgTotalWeight) {
        this.kgTotalWeight = kgTotalWeight;
    }

    public String getToMakeKgTotal() {
        return toMakeKgTotal;
    }

    public void setToMakeKgTotal(String toMakeKgTotal) {
        this.toMakeKgTotal = toMakeKgTotal;
    }

    public String getTotalAmountAll() {
        return totalAmountAll;
    }

    public void setTotalAmountAll(String totalAmountAll) {
        this.totalAmountAll = totalAmountAll;
    }
     
     String[] selectedIndex=null;
     String[] sectionIds=null;
     String[] sectionNames=null;
     String[] paplCodes=null;
     String[] itemNames=null;
     String[] uomNames=null;
     String[] itemIds=null;
     String[] uomIds=null;
     String[] quanditys=null;
     String[] servicePointIds=null;
     String[] servicePointNames=null;
     String[] compIds=null;
     String[] compNames=null;
  
     String[] nquanditys=null;
     String[] oquanditys=null;
     String[] unitPrices=null;
     String[] unitPricesForOthers=null;
     String[] scrapIds=null;
     
     String fromDate = "";
     String toDate = "";
     

    public String getScrapId() {
        return scrapId;
    }

    public void setScrapId(String scrapId) {
        this.scrapId = scrapId;
    }

     
     
    public String[] getScrapIds() {
        return scrapIds;
    }

    public void setScrapIds(String[] scrapIds) {
        this.scrapIds = scrapIds;
    }

    
  
    public String[] getUnitPricesForOthers() {
        return unitPricesForOthers;
    }

    public void setUnitPricesForOthers(String[] unitPricesForOthers) {
        this.unitPricesForOthers = unitPricesForOthers;
    }

    public String[] getUnitPrices() {
        return unitPrices;
    }

    public void setUnitPrices(String[] unitPrices) {
        this.unitPrices = unitPrices;
    }

    public String[] getNquanditys() {
        return nquanditys;
    }

    public void setNquanditys(String[] nquanditys) {
        this.nquanditys = nquanditys;
    }

    public String[] getOquanditys() {
        return oquanditys;
    }

    public void setOquanditys(String[] oquanditys) {
        this.oquanditys = oquanditys;
    }
     
     
     
    public String[] getCompIds() {
        return compIds;
    }

    public void setCompIds(String[] compIds) {
        this.compIds = compIds;
    }

    public String[] getCompNames() {
        return compNames;
    }

    public void setCompNames(String[] compNames) {
        this.compNames = compNames;
    }
     
     

    public String getKgunitPrice() {
        return kgunitPrice;
    }

    public void setKgunitPrice(String kgunitPrice) {
        this.kgunitPrice = kgunitPrice;
    }
     
     

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }
     
     
     

    public String getQuandity() {
        return quandity;
    }

    public void setQuandity(String quandity) {
        this.quandity = quandity;
    }

    public String[] getQuanditys() {
        return quanditys;
    }

    public void setQuanditys(String[] quanditys) {
        this.quanditys = quanditys;
    }

    public String getServicePointId() {
        return servicePointId;
    }

    public void setServicePointId(String servicePointId) {
        this.servicePointId = servicePointId;
    }

    public String[] getServicePointIds() {
        return servicePointIds;
    }

    public void setServicePointIds(String[] servicePointIds) {
        this.servicePointIds = servicePointIds;
    }

    public String getServicePointName() {
        return servicePointName;
    }

    public void setServicePointName(String servicePointName) {
        this.servicePointName = servicePointName;
    }

    public String[] getServicePointNames() {
        return servicePointNames;
    }

    public void setServicePointNames(String[] servicePointNames) {
        this.servicePointNames = servicePointNames;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }
     
     
     
    public String[] getUomIds() {
        return uomIds;
    }

    public void setUomIds(String[] uomIds) {
        this.uomIds = uomIds;
    }
     
     
    public String[] getItemIds() {
        return itemIds;
    }

    public void setItemIds(String[] itemIds) {
        this.itemIds = itemIds;
    }
     
     

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String[] getItemNames() {
        return itemNames;
    }

    public void setItemNames(String[] itemNames) {
        this.itemNames = itemNames;
    }

    public String getPaplCode() {
        return paplCode;
    }

    public void setPaplCode(String paplCode) {
        this.paplCode = paplCode;
    }

    public String[] getPaplCodes() {
        return paplCodes;
    }

    public void setPaplCodes(String[] paplCodes) {
        this.paplCodes = paplCodes;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String[] getSectionIds() {
        return sectionIds;
    }

    public void setSectionIds(String[] sectionIds) {
        this.sectionIds = sectionIds;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String[] getSectionNames() {
        return sectionNames;
    }

    public void setSectionNames(String[] sectionNames) {
        this.sectionNames = sectionNames;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String[] getUomNames() {
        return uomNames;
    }

    public void setUomNames(String[] uomNames) {
        this.uomNames = uomNames;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
     
     

}
