/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.company.business;

/**
 *
 * @author vinoth
 */
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapSession;
import ets.domain.company.data.CompanyDAO;
import ets.domain.company.business.CompanyTO;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import java.util.ArrayList;
import java.util.Iterator;

public class CompanyBP {

    private CompanyDAO companyDAO;

    public CompanyDAO getCompanyDAO() {
        return companyDAO;
    }

    public void setCompanyDAO(CompanyDAO companyDAO) {
        this.companyDAO = companyDAO;
    }

    public int processInsertNewCompany(CompanyTO companyTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapSession session = companyDAO.getSqlMapClient().openSession();
        try{
        session.startTransaction();
        status = companyDAO.doInsertNewCompany(companyTO, userId,session);
        session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            //////System.out.println("am here 7");
            try {
            //////System.out.println("am here 7a");
                session.endTransaction();
            //////System.out.println("am here 7b");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            //////System.out.println("am here 8");

        }
        return status;
    }

    public ArrayList processGetCompanyList() throws FPBusinessException, FPRuntimeException {
        ArrayList companyList = new ArrayList();
        companyList = companyDAO.getCompanyList();
         if (companyList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return companyList;
    }


    //Get Make Data BP Start
    public ArrayList processGetMakeList() throws FPBusinessException, FPRuntimeException {
    ArrayList makeList = new ArrayList();
    makeList = companyDAO.getMakeList();
     if (makeList.size() == 0) {
        throw new FPBusinessException("EM-GEN-01");
    }
    return makeList;
    }

    public ArrayList processGetTyreLifeCycle() throws FPBusinessException, FPRuntimeException {
        ArrayList companyList = new ArrayList();
        companyList = companyDAO.GetTyreLifeCycleList();
         if (companyList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return companyList;
    }
//Get Make Data BP End


    public ArrayList processActiveCompanyList() throws FPBusinessException, FPRuntimeException {
        ArrayList companyList = new ArrayList();
        ArrayList activeCompanyList = new ArrayList();
        CompanyTO comp = null;
        Iterator itr;
        companyList = companyDAO.getCompanyList();
        itr = companyList.iterator();
        while(itr.hasNext()){
           comp = new CompanyTO();
           comp = (CompanyTO) itr.next();
           if(comp.getActiveInd().equalsIgnoreCase("Y")){
               activeCompanyList.add(comp);
           }           
        }
        if (activeCompanyList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return activeCompanyList;
    }

    public ArrayList processGetCompanyDetail(int companyId) throws FPRuntimeException, FPBusinessException {
        ArrayList comp = new ArrayList();
        comp = (ArrayList) companyDAO.getCompanyDetail(companyId);
        if (comp.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return comp;
    }

    public ArrayList processGetCompanyTypes() throws FPRuntimeException, FPBusinessException {
        ArrayList comp = new ArrayList();
        comp = (ArrayList) companyDAO.getCompanyTypes();         
        return comp;
    }

    public int processUpdateCompany(CompanyTO companyTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = companyDAO.doUpdateCompany(companyTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

 

}
