/*---------------------------------------------------------------------------
 * ServiceDAO.java
 * Mar 3, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.stockTransfer.data;

import ets.arch.exception.FPRuntimeException;
import ets.domain.stockTransfer.business.StockTransferTO;
import ets.domain.util.FPLogUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class StockTransferDAO extends SqlMapClientDaoSupport {

    private final static String CLASS = "stockTransferDAO";

    public ArrayList getServicePtList(int companyType, int companyId) {
        Map map = new HashMap();
        map.put("companyType", companyType);
        map.put("companyId", companyId);
        ArrayList servicePtList = new ArrayList();
        try {
            servicePtList = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.getServicePtList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "servicePtList", sqlException);
        }
        return servicePtList;
    }

    public ArrayList getServicePtList(int companyType) {
        Map map = new HashMap();
        map.put("companyType", companyType);
        ArrayList servicePtList = new ArrayList();
        try {
            servicePtList = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.ServicePtList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "servicePtList", sqlException);
        }
        return servicePtList;
    }

    public ArrayList getRequestList(int companyId) {
        Map map = new HashMap();
        map.put("companyId", companyId);
        ArrayList requestList = new ArrayList();
        try {
            requestList = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.getRequests", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "requestList", sqlException);
        }
        return requestList;
    }

    public String getOtherStockReqList(String requestId, int itemId) {
        Map map = new HashMap();
        map.put("requestId", requestId);
        map.put("itemId", itemId);
        String otherRequest = "";
        try {
            otherRequest = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getOtherRequest", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "otherRequest", sqlException);
        }
        return otherRequest;
    }

    public ArrayList getItemList(int companyId, String requestId) {
        Map map = new HashMap();
        map.put("servicePtId", companyId);
        map.put("requestId", requestId);
        ArrayList itemList = new ArrayList();
        try {
            StockTransferTO stocktransferTO = new StockTransferTO();
            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.getItemList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "itemList", sqlException);
        }
        return itemList;
    }

    public int insertApprovedQty(int userId, ArrayList List, StockTransferTO stocktransferTO) {
        Map map = new HashMap();
        int status = 0;
        int counter = 0;
        int approveId = 0;
        map.put("userId", userId);
        map.put("approvalStatus", stocktransferTO.getApprovalStatus());
        map.put("remarks", stocktransferTO.getRemarks());
        map.put("requestId", stocktransferTO.getRequestId());
        try {
            Iterator itr = List.iterator();
            StockTransferTO listTO = null;
            while (itr.hasNext()) {
                listTO = (StockTransferTO) itr.next();
                map.put("itemId", listTO.getItemId());
                map.put("requestedQty", listTO.getRequestedQty());
                map.put("approvedQty", listTO.getApprovedQty());
                if (counter == 0) {
                    approveId = (Integer) getSqlMapClientTemplate().insert("stockTransfer.insertStatus", map);
                    System.out.println("approveId in DAO:"+approveId);
                  }
                counter++;
                map.put("approveId", approveId);
                status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertApprovedQty", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-PR-01", CLASS, "status", sqlException);
        }
        return approveId;
    }

    public ArrayList getapprovedStockList(int companyId) {
        Map map = new HashMap();
        map.put("companyId", companyId);
        ArrayList approvedStockList = new ArrayList();
        try {

            approvedStockList = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.ApprovedStockList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "approvedStockList", sqlException);
        }
        return approvedStockList;

    }

    public ArrayList getapprovedStockListAll(int requestId) {
        Map map = new HashMap();
        map.put("requestId", requestId);
        ArrayList approvedStockListAll = new ArrayList();
        try {

            approvedStockListAll = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.ApprovedStockListAll", map);
            System.out.println("approvedStockListAll="+approvedStockListAll.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "approvedStockListAll", sqlException);
        }
        return approvedStockListAll;

    }
    public ArrayList getRCApprovedStockListAll(int requestId) {
        Map map = new HashMap();
        map.put("requestId", requestId);
        ArrayList approvedStockListAll = new ArrayList();
        try {

            approvedStockListAll = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.ApprovedStockListAllNEW", map);
            System.out.println("approvedStockListAll="+approvedStockListAll.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "approvedStockListAll", sqlException);
        }
        return approvedStockListAll;

    }

    public ArrayList getReceivedStockListAll(StockTransferTO stocktransferTO, int companyId) {
        Map map = new HashMap();
        map.put("servicePtId", stocktransferTO.getServicePointId());
        map.put("companyId", companyId);
        ArrayList receivedStockListAll = new ArrayList();
        try {

            receivedStockListAll = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.ReceivedStockList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "receivedStockListAll", sqlException);
        }
        return receivedStockListAll;

    }

    public ArrayList getPriceList(int requestId, int companyId) {
        Map map = new HashMap();
        ArrayList priceList = new ArrayList();
        try {
            map.put("companyId", companyId);
            map.put("requestId", requestId);
            System.out.println("In getPriceList Dao");
            priceList = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.PriceList", map);
            System.out.println("AfterPrice List Dao");
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "priceList", sqlException);
        }
        return priceList;

    }

    public ArrayList getRcItemList(int requestId,int itemId, int companyId) {
        int from_service_point=0;
        int rc_item_id=0;
        Map map = new HashMap();
        ArrayList rcItemList = new ArrayList();
        try {
            map.put("requestId", requestId);
            map.put("itemId", itemId);
            
 //Hari
            System.out.println("ItemId-->"+itemId);
//            from_service_point = (Integer)getSqlMapClientTemplate().queryForObject("stockTransfer.getFromServicePoint", map);
//            System.out.println("FFFFFFrom"+from_service_point);
//            map.put("from",from_service_point);
//
//            rc_item_id = (Integer)getSqlMapClientTemplate().queryForObject("stockTransfer.checkRcItemId", map);
//            System.out.println("Rc Item Id for From Service Point-->"+rc_item_id);
//            if(rc_item_id!=0)
//            {
//            map.put("companyId", from_service_point);
//            }
//           // else
            
            map.put("from",companyId);
            rc_item_id = (Integer)getSqlMapClientTemplate().queryForObject("stockTransfer.checkRcItemId", map);
            System.out.println("Rc From Login Company Id--->"+rc_item_id);
            if(rc_item_id!=0)
            {
            map.put("companyId", companyId);
            }
            else{
            from_service_point = (Integer)getSqlMapClientTemplate().queryForObject("stockTransfer.getFromServicePoint", map);
            System.out.println("FFFFFFrom"+from_service_point);
            map.put("from",from_service_point);

            rc_item_id = (Integer)getSqlMapClientTemplate().queryForObject("stockTransfer.checkRcItemId", map);
            System.out.println("Rc Item Id for From Service Point-->"+rc_item_id);
            map.put("companyId", from_service_point);

            }
            
//Hari End
            rcItemList = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.getRcItemList", map);
            System.out.println("RcItemList-->"+rcItemList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "RcItemList", sqlException);
        }
        return rcItemList;

    }

    public int insertTransferItems(ArrayList List, ArrayList tyreList, StockTransferTO stocktransferTO) {
        Map map = new HashMap();
        ArrayList priceList = new ArrayList();
        int status = 0;
        int i = 0;
        String GdId = "";
        String priceId = "";
        String activeInd = "Y";
        String gdType = "ST";
        Float priceWithTax=0f;
        String priceType= "INVOICE";
        map.put("approveId", stocktransferTO.getApprovedId());
        map.put("gdType", gdType);
        map.put("remarks", stocktransferTO.getRemarks());
        map.put("activeInd", activeInd);
        map.put("userId", stocktransferTO.getUserId());
        map.put("companyId", stocktransferTO.getCompanyId());
        try {
            System.out.println("Delivery List Size-->"+List.size());
            System.out.println("tyreList Size-->"+tyreList.size());
            Iterator itr = List.iterator();
            Iterator tyreItr = tyreList.iterator();
            StockTransferTO listTO = null;
            StockTransferTO listTO1 = null;
            GdId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getRcGdId", map);
            System.out.println("checked gdId="+GdId);
            if (GdId == null) {
                status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertGoodsMaster", map);
                System.out.println("insertGoodsMaster" + status);
                GdId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getGdId", map);
            System.out.println("generated GdId="+GdId);
            }
            map.put("GdId", GdId);

            while (itr.hasNext()) {
                System.out.println("In Item Block");
                listTO = new StockTransferTO();
                listTO = (StockTransferTO) itr.next();
                    System.out.println("In Item Block ItemId-->"+listTO.getItemId()
                        +"Tax"+listTO.getTax()+"Price-->"+listTO.getPrice()+"getIssuedQty:"+listTO.getIssuedQty()
                        +"company Id"+map.get("companyId"));
                if(  !listTO.getIssuedQty().equalsIgnoreCase("0")    ){
                    map.put("itemId", listTO.getItemId());
                    map.put("issuedQty", listTO.getIssuedQty());
                    map.put("price", listTO.getPrice());
                    map.put("priceType", listTO.getPricetype());
                    System.out.println("In DAO ItemId-->"+listTO.getItemId()+"Tax-->"+listTO.getTax()+"Price"+listTO.getPrice()+"Price Type-->"+listTO.getPricetype());
                    map.put("tax", listTO.getTax());
                    map.put("amount", listTO.getAmount());
                    status=0;
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertTransferItems", map);
                    System.out.println("Transfrd Items Inserted ==>"+status);

                    //priceId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getPriceId", map);
                    priceList = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.getPriceId", map);

                    System.out.println("Price Id -->"+priceId);

                    float issuedQty = Float.parseFloat(listTO.getIssuedQty());
                    System.out.println("Issued Qty--> "+issuedQty);
                    map.put("issuedQty", issuedQty);

                    System.out.println("In Item Block ItemId-->"+listTO.getItemId()+"priceId-->"+priceId
                        +"Tax"+listTO.getTax()+"Price-->"+listTO.getPrice()+"price type"+listTO.getPricetype()
                        +"company Id"+map.get("companyId"));
                    Iterator itr1 = priceList.iterator();
                    float stockQty = 0.00F;
                    float updateQty = 0.00F;
                    float remainingQty = 0.00F;
                    while (itr1.hasNext()) {
                        listTO1 = new StockTransferTO();
                        listTO1 = (StockTransferTO) itr1.next();
                        priceId = listTO1.getPriceId();
                        stockQty = Float.parseFloat(listTO1.getStockQty());
                        map.put("priceId", priceId);
                        if(stockQty < issuedQty){
                            issuedQty = issuedQty - stockQty;
                            map.put("issuedQty", stockQty);
                        }else {
                            map.put("issuedQty", issuedQty);
                            issuedQty = 0;
                        }
                        status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateTransferStockBalance", map);
                        System.out.println("Stock Bal"+status);
                        if(issuedQty == 0){
                            break;
                        }
                    }




                }
            }

            while (tyreItr.hasNext()) {
                System.out.println("In Tyre bLock");
                listTO = new StockTransferTO();
                listTO = (StockTransferTO) tyreItr.next();
                map.put("itemId", listTO.getItemId());
                map.put("tyreId", listTO.getTyreId());
                map.put("tax", listTO.getTax());
                map.put("price", listTO.getPrice());
                map.put("amount", listTO.getAmount());
                map.put("issuedQty", listTO.getIssuedQty());
                map.put("Qty", -1);

                System.out.println("In Tyre Block ItemId-->"+listTO.getItemId()+"TyreId-->"+listTO.getTyreId()
                        +"Tax"+listTO.getTax()+"Price-->"+listTO.getPrice()+"price TYpe"+listTO.getPricetype()
                        +"nothing Tyre Tyepe"+listTO.getTyreType()+"God Price Id-->"+listTO.getPriceId());
//                priceId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getPriceIdReceived", map);
                priceId= listTO.getPriceId();
                System.out.println("Price Id i got is insertTransferItems-->"+priceId);
                //Hari
//                if (priceId == null) {
//                            System.out.println("price Id not exists insertTransferItems");
//                            priceWithTax= Float.parseFloat(listTO.getPrice()) +(Float.parseFloat(listTO.getPrice()) * Float.parseFloat(listTO.getTax())/100);
//                            System.out.println("Price With Tax -->"+priceWithTax);
//                            map.put("priceWithTax",priceWithTax);
//                            map.put("tax",listTO.getTax());
//                            map.put("priceType",priceType);
//                            map.put("activeInds",'Y');
//                            status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertItemPrice", map);
//                            System.out.println("Price Id Inserted insertTransferItems"+status);
//                            priceId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getPriceIdReceived", map);
//                            System.out.println("Price Id i re got is-->"+priceId);
//
//                }
//                if(priceId!=null)
////Hari
                map.put("priceId", priceId);


                if (listTO.getTyreType().equalsIgnoreCase("NEW")) {
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertTyreTransferItems", map);
                    map.put("issuedQty", "-"+listTO.getIssuedQty());
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateStockBalance", map);

                    System.out.println("New Tyre Updat status-->"+status);

                } else {
                    System.out.println("RT Tyre Block");
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.addTyresGd", map);
                    System.out.println("RC Transfered Items-->"+status);
                    map.put("issuedQty", "-"+listTO.getIssuedQty());
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateRcStockBalance", map);

                    System.out.println("RC Stock Bal-->"+status);


                }
                    //update servicePoint by vijay
//                  status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateTyreDetails", map);
            }
            status = (Integer) getSqlMapClientTemplate().update("stockTransfer.makeRequestInactive", map);
            System.out.println("Request In active-->"+status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "TransferItems", sqlException);
        }
        return status;

    }
    public int insertTransferItemsORIG(ArrayList List, ArrayList tyreList, StockTransferTO stocktransferTO) {
        Map map = new HashMap();
        int status = 0;
        int i = 0;
        String GdId = "";
        String priceId = "";
        String activeInd = "Y";
        String gdType = "ST";
        Float priceWithTax=0f;
        String priceType= "INVOICE";
        map.put("approveId", stocktransferTO.getApprovedId());
        map.put("gdType", gdType);
        map.put("remarks", stocktransferTO.getRemarks());
        map.put("activeInd", activeInd);
        map.put("userId", stocktransferTO.getUserId());
        map.put("companyId", stocktransferTO.getCompanyId());
        try {
            System.out.println("Delivery List Size-->"+List.size());
            System.out.println("tyreList Size-->"+tyreList.size());
            Iterator itr = List.iterator();
            Iterator tyreItr = tyreList.iterator();
            StockTransferTO listTO = null;
            GdId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getRcGdId", map);
            System.out.println("checked gdId="+GdId);
            if (GdId == null) {
                status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertGoodsMaster", map);
                System.out.println("insertGoodsMaster" + status);
                GdId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getGdId", map);
            System.out.println("generated GdId="+GdId);
            }
            map.put("GdId", GdId);

            while (itr.hasNext()) {
                System.out.println("In Item Block");
                listTO = new StockTransferTO();
                listTO = (StockTransferTO) itr.next();
                System.out.println("issuedQty in DAO--->"+listTO.getIssuedQty());
                if(  !listTO.getIssuedQty().equalsIgnoreCase("0")    ){
                    map.put("itemId", listTO.getItemId());
                    map.put("issuedQty", listTO.getIssuedQty());
                    map.put("price", listTO.getPrice());
                    map.put("priceType", listTO.getPricetype());
                    System.out.println("In DAO ItemId-->"+listTO.getItemId()+"Tax-->"+listTO.getTax()+"Price"+listTO.getPrice()+"Price Type-->"+listTO.getPricetype());
                    map.put("tax", listTO.getTax());
                    map.put("amount", listTO.getAmount());
                    status=0;
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertTransferItems", map);
                    System.out.println("Transfrd Items Inserted ==>"+status);
                    priceId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getPriceId", map);
                    System.out.println("Price Id -->"+priceId);
                    map.put("priceId", priceId);
                    float issuedQty = -Float.parseFloat(listTO.getIssuedQty());
                    System.out.println("Issued Qty--> "+issuedQty);
                    map.put("issuedQty", issuedQty);

                    System.out.println("In Item Block ItemId-->"+listTO.getItemId()+"priceId-->"+priceId
                        +"Tax"+listTO.getTax()+"Price-->"+listTO.getPrice()+"price TYpe"+listTO.getPricetype()
                        +"nothing");

                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateStockBalance", map);
                    System.out.println("Stock Bal"+status);
                }    
            }

            while (tyreItr.hasNext()) {
                System.out.println("In Tyre bLock");
                listTO = new StockTransferTO();
                listTO = (StockTransferTO) tyreItr.next();
                map.put("itemId", listTO.getItemId());
                map.put("tyreId", listTO.getTyreId());
                map.put("tax", listTO.getTax());
                map.put("price", listTO.getPrice());
                map.put("amount", listTO.getAmount());
                map.put("issuedQty", listTO.getIssuedQty());
                map.put("Qty", -1);

                System.out.println("In Tyre Block ItemId-->"+listTO.getItemId()+"TyreId-->"+listTO.getTyreId()
                        +"Tax"+listTO.getTax()+"Price-->"+listTO.getPrice()+"price TYpe"+listTO.getPricetype()
                        +"nothing Tyre Tyepe"+listTO.getTyreType());
                priceId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getPriceIdReceived", map);
                System.out.println("Price Id i got is insertTransferItems-->"+priceId);
                //Hari
                if (priceId == null) {
                            System.out.println("price Id not exists insertTransferItems");
                            priceWithTax= Float.parseFloat(listTO.getPrice()) +(Float.parseFloat(listTO.getPrice()) * Float.parseFloat(listTO.getTax())/100);
                            System.out.println("Price With Tax -->"+priceWithTax);
                            map.put("priceWithTax",priceWithTax);
                            map.put("tax",listTO.getTax());
                            map.put("priceType",priceType);
                            map.put("activeInds",'Y');
                            status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertItemPrice", map);
                            System.out.println("Price Id Inserted insertTransferItems"+status);
                            priceId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getPriceIdReceived", map);
                            System.out.println("Price Id i re got is-->"+priceId);

                }
                if(priceId!=null)
                map.put("priceId", priceId);
//Hari

                
                if (listTO.getTyreType().equalsIgnoreCase("NEW")) {
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertTyreTransferItems", map);
                    map.put("issuedQty", "-"+listTO.getIssuedQty());
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateStockBalance", map);

                    System.out.println("New Tyre Updat status-->"+status);

                } else {
                    System.out.println("RT Tyre Block");
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.addTyresGd", map);
                    System.out.println("RC Transfered Items-->"+status);
                    map.put("issuedQty", "-"+listTO.getIssuedQty());
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateRcStockBalance", map);

                    System.out.println("RC Stock Bal-->"+status);

                    
                }
                    //update servicePoint by vijay
//                  status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateTyreDetails", map);
            }
            status = (Integer) getSqlMapClientTemplate().update("stockTransfer.makeRequestInactive", map);
            System.out.println("Request In active-->"+status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "TransferItems", sqlException);
        }
        return status;

    }

    public int doInsertRcItems(ArrayList List, StockTransferTO stocktransferTO) {
        Map map = new HashMap();
        int status = 0;
        int i = 0;
        String GdId = "";
        String priceId = "";
        String activeInd = "Y";
        String gdType = "ST";
        String remarks = "";
        int Qty = -1;
        map.put("approveId", stocktransferTO.getApprovedId());
        map.put("gdType", gdType);
        map.put("remarks", remarks);
        map.put("activeInd", activeInd);
        map.put("userId", stocktransferTO.getUserId());
        map.put("companyId", stocktransferTO.getCompanyId());
        map.put("Qty", Qty);
        try {
            Iterator itr = List.iterator();
            StockTransferTO listTO = null;
            GdId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getRcGdId", map);
            if (GdId == null) {
                status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertGoodsMaster", map);
                System.out.println("insertGoodsMaster" + status);
                GdId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getGdId", map);
            }
            map.put("GdId", GdId);
            while (itr.hasNext()) {
                listTO = (StockTransferTO) itr.next();
                map.put("itemId", listTO.getItemId());
                map.put("price", listTO.getPrice());
                map.put("rcItemId", listTO.getRcItemId());
                System.out.println("rcItemId ="+listTO.getRcItemId() );
                status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertRcDelivery", map);
                System.out.println("insertRcDelivery" + status);
                status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateRcStockBalance", map);
                System.out.println("updateRcStockBalance" + status);
                status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateRcItemStatus", map);
                System.out.println("updateRcItemStatus" + status);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "TransferItems", sqlException);
        }
        return status;

    }
    public int doInsertRcItemsNew(ArrayList List, StockTransferTO stocktransferTO) {
        Map map = new HashMap();
        int status = 0;
        int i = 0;
        String GdId = "";
        String priceId = "";
        String activeInd = "Y";
        String gdType = "ST";
        String remarks = "";
        int Qty = -1;
        map.put("approveId", stocktransferTO.getApprovedId());
        map.put("gdType", gdType);
        map.put("remarks", remarks);
        map.put("activeInd", activeInd);
        map.put("userId", stocktransferTO.getUserId());
        map.put("companyId", stocktransferTO.getCompanyId());
        map.put("Qty", Qty);
        int issueQty = 0;
        ArrayList rcItemList = null;
        try {
            Iterator itr = List.iterator();
            Iterator rcItr = null;
            StockTransferTO listTO = null;
            StockTransferTO rcTO = null;
            GdId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getRcGdId", map);
            System.out.println("GdId Prior:"+GdId);
            if (GdId == null) {
                status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertGoodsMaster", map);
                System.out.println("insertGoodsMaster" + status);
                GdId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getGdId", map);
            }
            System.out.println("GdId Later:"+GdId);
            map.put("GdId", GdId);
            while (itr.hasNext()) {
                listTO = (StockTransferTO) itr.next();
                map.put("itemId", listTO.getItemId());

                issueQty = 0;
                if(listTO.getIssuedQty() != null && !"".equals(listTO.getIssuedQty())) {
                    issueQty = Integer.parseInt(listTO.getIssuedQty());
                }
                System.out.println("itemId:"+listTO.getItemId());
                System.out.println("issueQty:"+issueQty);
                if(issueQty > 0){
                    map.put("issueQty", issueQty);
                    rcItemList = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.getRcItemIssueList", map);
                    System.out.println("rcItemList size:"+rcItemList.size());
                    rcItr = rcItemList.iterator();
                    
                    while (rcItr.hasNext()) {
                        rcTO = (StockTransferTO) rcItr.next();
                        map.put("price", rcTO.getPrice());
                        map.put("rcItemId", rcTO.getRcItemId());
                        System.out.println("rcItemId ="+rcTO.getRcItemId() );
                        status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertRcDelivery", map);
                        System.out.println("insertRcDelivery" + status);
                        status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateRcStockBalance", map);
                        System.out.println("updateRcStockBalance" + status);
                        status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateRcItemStatus", map);
                        System.out.println("updateRcItemStatus" + status);
                    }
                }

            }
            status = (Integer) getSqlMapClientTemplate().update("stockTransfer.makeRequestInactive", map);
            System.out.println("Request In active-->"+status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "TransferItems", sqlException);
        }
        return status;

    }

    public ArrayList getTransferedItems(String gdId, StockTransferTO stocktransferTO) {
        Map map = new HashMap();
        map.put("gdId", gdId);
        System.out.println("gdId" + gdId);
        map.put("servicePtId", stocktransferTO.getServicePointId());
        System.out.println("stocktransferTO.getServicePointId()" + stocktransferTO.getServicePointId());
        ArrayList itemList = new ArrayList();
        try {
            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.getTransferedItems", map);
            System.out.println("itemList" + itemList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "itemList", sqlException);
        }
        return itemList;
    }

    public ArrayList getTransferedRcItems(String gdId, StockTransferTO stocktransferTO) {
        Map map = new HashMap();
        map.put("gdId", gdId);
        map.put("servicePtId", stocktransferTO.getServicePointId());
        map.put("itemId", stocktransferTO.getItemId());
        ArrayList itemList = new ArrayList();
        try {
            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.getTransferedRcItems", map);
            System.out.println("itemList" + itemList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "itemList", sqlException);
        }
        return itemList;
    }

    //accepted Qty is mapped as Issued Qty (as Required)
    public int doReceivedNewItems(ArrayList List, StockTransferTO stocktransferTO) {
        Map map = new HashMap();
        int status = 0;
        int counter = 0;
        int grId = 0;
        String priceId = "";
        String sbPriceId = "";
        String activeInd = "N";
        Float priceWithTax=0f;
        String priceType="INVOICE";
        map.put("gdId", Integer.parseInt(stocktransferTO.getGdId()));
        map.put("remarks", stocktransferTO.getRemarks());
        map.put("activeInd", activeInd);
        map.put("userId", stocktransferTO.getUserId());
        map.put("companyId", stocktransferTO.getCompanyId());
        
        try {
            Iterator itr = List.iterator();
            StockTransferTO listTO = null;
            grId = (Integer) getSqlMapClientTemplate().insert("stockTransfer.insertGoodsReceivedMaster", map);
            System.out.println("generated receiveId-->"+grId);
            map.put("grId", grId);
            
                    while (itr.hasNext()) {
                        listTO = new StockTransferTO();
                        listTO = (StockTransferTO) itr.next();
                        map.put("itemId", listTO.getItemId());
                        map.put("issuedQty", listTO.getAcceptedQty());
                        map.put("price", listTO.getPrice());
                        map.put("tax", listTO.getTax());
                        float rate = (Float.valueOf(listTO.getPrice())) * (Float.valueOf(listTO.getAcceptedQty()));
                        priceId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getPriceIdReceived", map);
                        System.out.println("PriceId I got"+priceId);
                        String amount = String.valueOf(rate);
                        System.out.println("amount=" + amount);
                        map.put("amount", amount);
                        if (priceId == null) {
                            System.out.println("price Id not exists");
                            priceWithTax= Float.parseFloat(listTO.getPrice()) +(Float.parseFloat(listTO.getPrice()) * Float.parseFloat(listTO.getTax())/100);
                            System.out.println("Price With Tax -->"+priceWithTax);
                            map.put("priceWithTax",priceWithTax);
                            map.put("tax",listTO.getTax());
                            map.put("priceType",priceType);
                            map.put("activeInds",'Y');
                            status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertItemPrice", map);
                            System.out.println("Price Id Inserted"+status);
                            priceId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getPriceIdReceived", map);
                            System.out.println("inserted priceId="+priceId);
                            map.put("priceId", priceId);
                            status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertStockBalance", map);
                            System.out.println("Stock Update->"+status);
                        } else {
                            System.out.println("priceId exists");

                            System.out.println("companyId in DAO-->"+stocktransferTO.getCompanyId());
//                            bala
                            System.out.println("priceId i got DAO-->"+priceId);
                            map.put("priceId",priceId); 
                            sbPriceId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getSBPriceId", map);
                            System.out.println("sbPriceId="+sbPriceId);
                            
                            if (sbPriceId == null) {
                                System.out.println("New stock balance Not exists");
                                status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertStockBalance", map);
                            } else {
                                map.put("priceId", sbPriceId);
                                System.out.println("new stock balance exists");
                                status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateStockBalance", map);
                            }
                        }
                        if (listTO.getTyreNo().equalsIgnoreCase("0")) {
                            System.out.println("Received Item Entry in received details");
                            status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertGoodsReceivedDetails", map);
                        } else {
                            map.put("tyreId", listTO.getTyreId());
                            System.out.println("Received tyre Entry in received details");
                            status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateTyreMaster", map);
                            System.out.println("Supply Tyre Master renamed its spId-->"+status);
                            status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateStRtHistory", map); 
                            status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertTyreGoodsReceivedDetails", map);
                            System.out.println("Receive details in GR->"+status);
                        }                    
                }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "updateReceivedItems", sqlException);
        }
        return grId;
    }

    public int updateRcItems(ArrayList List, StockTransferTO stocktransferTO) {
        System.out.println("Hi i am in updateRc");
        Map map = new HashMap();
        int status = 0;
        int counter = 0;
        String grId = "";
        String priceId = "";
        String acceptedQty = "1";
        String activeInd = "N";
        map.put("gdId", stocktransferTO.getGdId());
        map.put("grId", stocktransferTO.getGrId());
        map.put("activeInd", activeInd);
        map.put("userId", stocktransferTO.getUserId());
        map.put("companyId", stocktransferTO.getCompanyId());

        try {
            System.out.println("Hi i am in updateRc try");
            Iterator itr = List.iterator();
            StockTransferTO listTO = null;
//            grId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getRcGrId", map);
//            if (grId == null) {
//                status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertGoodsReceivedMaster", map);
//                grId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getGrId", map);
//            }
            
            while (itr.hasNext()) {
                listTO = new StockTransferTO();
                listTO = (StockTransferTO) itr.next();
                map.put("itemId", listTO.getItemId());
                map.put("rcItemId", listTO.getTyreId());
                map.put("tyreId", listTO.getTyreId() ); // For general Item tyreId contains rc_item_id
                map.put("issuedQty", acceptedQty);
                map.put("Qty", acceptedQty);
                map.put("price", listTO.getPrice());
                map.put("tax",listTO.getTax());
                System.out.println("RCStatus Checking");
                status = (Integer) getSqlMapClientTemplate().queryForObject("stockTransfer.checkRcStkExists", map);
                System.out.println("status size"+status);
                if (status == 0) {
                    System.out.println("rc stk balance not exists");
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertRcStockBalance", map);
                    System.out.println("rcstk inserted successfully" + status);
                } else {
                    System.out.println("rc stk balance exists");
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateRcStockBalance", map);
                    System.out.println("rcstk Updated successfully" + status);
                }
                
                if (listTO.getTyreNo().equalsIgnoreCase("0")) {
                    System.out.println("rc Item entry started");
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertRcReceivedDetails", map);
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateRcItemMaster", map);
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateStRcHistory", map);
                } else {
                    System.out.println("rc Tyres entry started");
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertRtReceivedDetails", map);
                    System.out.println("RT received Details-->"+status);
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateTyreMaster", map);
                    System.out.println("RT Update Tyre Master-->"+status);
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateStRtHistory", map);
                    System.out.println("RT History-->"+status);
                }
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "ReceiveRcItems", sqlException);
        }
        return status;

    }

    public int updateRcItemsNew(ArrayList List, StockTransferTO stocktransferTO) {
        System.out.println("Hi i am in updateRc");
        Map map = new HashMap();
        int status = 0;
        int counter = 0;
        String grId = "";
        String priceId = "";
        String acceptedQty = "1";
        String activeInd = "N";
        map.put("gdId", stocktransferTO.getGdId());
        map.put("grId", stocktransferTO.getGrId());
        map.put("activeInd", activeInd);
        map.put("userId", stocktransferTO.getUserId());
        map.put("companyId", stocktransferTO.getCompanyId());

        try {
            System.out.println("Hi i am in updateRc try");
            Iterator itr = List.iterator();
            StockTransferTO listTO = null;
//            grId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getRcGrId", map);
//            if (grId == null) {
//                status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertGoodsReceivedMaster", map);
//                grId = (String) getSqlMapClientTemplate().queryForObject("stockTransfer.getGrId", map);
//            }

            while (itr.hasNext()) {
                listTO = new StockTransferTO();
                listTO = (StockTransferTO) itr.next();
                map.put("itemId", listTO.getItemId());
                map.put("rcItemId", listTO.getRcItemId());
                //map.put("tyreId", listTO.getTyreId() ); // For general Item tyreId contains rc_item_id
                map.put("tyreId", "0" );
                map.put("issuedQty", acceptedQty);
                map.put("Qty", acceptedQty);
                map.put("price", listTO.getPrice());
                map.put("tax",listTO.getTax());
                System.out.println("RCStatus Checking");
                status = (Integer) getSqlMapClientTemplate().queryForObject("stockTransfer.checkRcStkExists", map);
                System.out.println("status size"+status);
                if (status == 0) {
                    System.out.println("rc stk balance not exists");
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertRcStockBalance", map);
                    System.out.println("rcstk inserted successfully" + status);
                } else {
                    System.out.println("rc stk balance exists");
                    status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateRcStockBalance", map);
                    System.out.println("rcstk Updated successfully" + status);
                }


                System.out.println("rc Item entry started");
                status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertRcReceivedDetails", map);
                status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateRcItemMaster", map);
                status = (Integer) getSqlMapClientTemplate().update("stockTransfer.updateStRcHistory", map);

            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-ST-01", CLASS, "ReceiveRcItems", sqlException);
        }
        return status;

    }

// Raja stock Transfer
    public int doGenerateStRequest(StockTransferTO itemTO) {
        Map map = new HashMap();
        int status = 0;
        int requestId = 0;
        try {
            map.put("fromServiceId", itemTO.getFromSpId());
            map.put("toServiceId", itemTO.getToSpId());
            map.put("requiredDate", itemTO.getReqDate());
            map.put("reqType", itemTO.getReqType());
            map.put("remarks", itemTO.getDesc());
            map.put("userId", itemTO.getUserId());
            status = (Integer) getSqlMapClientTemplate().update("stockTransfer.generateStkTransferReq", map);
            if (status > 0) {
                requestId = (Integer) getSqlMapClientTemplate().queryForObject("stockTransfer.getReqId", map);
                map.put("requestId", requestId);
                for (int i = 0; i < itemTO.getItemIds().length; i++) {
                    System.out.println("test="+itemTO.getItemIds()[i]);
                    if(itemTO.getItemIds()[i] != null  &&  (! itemTO.getItemIds()[i].equals("") ) &&  (! itemTO.getReqQuant()[i].equals("") )  ){
                        map.put("itemId", itemTO.getItemIds()[i]);
                        map.put("quantity", itemTO.getReqQuant()[i]);
                        status = (Integer) getSqlMapClientTemplate().update("stockTransfer.insertStkTransferDetails", map);
                    }
                }
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doGenerateStRequest Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "doGenerateStRequest", sqlException);
        }
        return status;
    }

    public ArrayList getStRequestList(StockTransferTO purchaseTO) {
        Map map = new HashMap();
        ArrayList mprList = new ArrayList();
        ArrayList mprStatusList = new ArrayList();
        Iterator itr;
        StockTransferTO purchTO = new StockTransferTO();
        String[] temp = null;
        try {
            map.put("companyId", purchaseTO.getCompanyId());
            mprList = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.getStRequests", map);
            itr = mprList.iterator();
            while (itr.hasNext()) {
                purchTO = (StockTransferTO) itr.next();
                temp = purchTO.getRequestId().split("~");
                purchTO.setRequestId(temp[0]);
                purchTO.setStatus(temp[1]);
                mprStatusList.add(purchTO);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStRequestList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-PR-01", CLASS, "getStRequestList", sqlException);
        }
        return mprStatusList;
    }

    public ArrayList getTyrePriceList(int companyId) {
        Map map = new HashMap();
        ArrayList tyreList = new ArrayList();
        try {
            map.put("companyId", companyId);
            System.out.println("B4 getTyrePriceList");
            tyreList = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.getTyrePriceList", map);
            System.out.println("A4 getTyrePriceList");
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTyrePriceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTyrePriceList", sqlException);
        }
        return tyreList;
    }
//Hari

public ArrayList getReqIdDetail(int requestId) {
        Map map = new HashMap();
        ArrayList reqIdDetail = new ArrayList();
        try {
            map.put("requestId", requestId);
            System.out.println("B4 processReqIdDetail");
            reqIdDetail = (ArrayList) getSqlMapClientTemplate().queryForList("stockTransfer.getReqIdDetail", map);
            System.out.println("A4 processReqIdDetail");
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTyrePriceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTyrePriceList", sqlException);
        }
        return reqIdDetail;
    }

}


