/*---------------------------------------------------------------------------
 * ServiceCommand.java
 * Mar 5, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.stockTransfer.business;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class StockTransferTO {

    private String companyId = "";
    private String categoryName = "";
    private String companyName = "";
    private String servicePtName = "";
    private String approvedQty = "";
    private String newQty = "";
    private String rcQty = "";
    private String requiredDate = "";
    private String requestId = "";
    private String itemName = "";
    private String itemId = "";
    private String mfrCode = "";
    private String paplCode = "";
    private String requestedQty = "";
    //private String rcQty = "";
    private String approvalStatus = "";
    private String remarks = "";
    private String requestPtRcItem = "";
    private String requestPtNewItem = "";
    private String approvePtRcItem = "";
    private String approvePtNewItem = "";
    private String requestPtId = "";
    private String otherStockReq = "";
    private String servicePointId = "";
    private String servicePointName = "";
    private String status = "";
    private String uomName = "";
    private String approvedQuandity = "";
    private String price = "";
    private String vat = "";
    private String priceId = "";
    private String approvedId = "";
    private String userId = "";
    private String amount = "";
    private String gdId = "";
    private String issuedQty = "";
    private String stockQty = "";
    private String acceptedQty = "";
    private String[] itemIds = null;
    private String[] requestedQtys = null;
    private String[] approvedQtys = null;
    private String[] selectedIndex = null;
    private String[] issuedQtys = null;
    private String[] priceIds = null;
    private String[] acceptedQtys = null;
    //receive rc wo
    private String vendorId = "";
    private String woId = "";
    private String rcItemId = "";
    private String rcPrice = "";
    private String rcStatus = "";
    private String[] rcPrices = null;
    private String[] itemAmounts = null;
    private String[] rcItemIds = null;
     private String[] arrLength = null;
    private String[] rcStatuses = null;
    // Raja Stock transfer
    private String fromSpId = "0";
    private String toSpId = "0";
    private String Desc = "";
    private String ReqDate = "";
    private String createdDate = "";
    private String elapsedDays = "";
    private String[] ReqQuant = null;
    private String categoryId = "";
    private String tyreType = "";
    private String tyreNo = "";
    private String tyreId = "";
    private String itemType = "";
    private String reqType = "";
    private String grId = "";
    private String issuedDate = "";
    private String tax = "";
    private String pricetype = "";


    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getApprovedQty() {
        return approvedQty;
    }

    public void setApprovedQty(String approvedQty) {
        this.approvedQty = approvedQty;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getMfrCode() {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode) {
        this.mfrCode = mfrCode;
    }

    public String getPaplCode() {
        return paplCode;
    }

    public void setPaplCode(String paplCode) {
        this.paplCode = paplCode;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequiredDate() {
        return requiredDate;
    }

    public void setRequiredDate(String requiredDate) {
        this.requiredDate = requiredDate;
    }

    public String getRequestedQty() {
        return requestedQty;
    }

    public void setRequestedQty(String requestedQty) {
        this.requestedQty = requestedQty;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String[] getApprovedQtys() {
        return approvedQtys;
    }

    public void setApprovedQtys(String[] approvedQtys) {
        this.approvedQtys = approvedQtys;
    }

    public String[] getItemIds() {
        return itemIds;
    }

    public void setItemIds(String[] itemIds) {
        this.itemIds = itemIds;
    }

    public String[] getRequestedQtys() {
        return requestedQtys;
    }

    public void setRequestedQtys(String[] requestedQtys) {
        this.requestedQtys = requestedQtys;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getApprovePtNewItem() {
        return approvePtNewItem;
    }

    public void setApprovePtNewItem(String approvePtNewItem) {
        this.approvePtNewItem = approvePtNewItem;
    }

    public String getApprovePtRcItem() {
        return approvePtRcItem;
    }

    public void setApprovePtRcItem(String approvePtRcItem) {
        this.approvePtRcItem = approvePtRcItem;
    }

    public String getRequestPtNewItem() {
        return requestPtNewItem;
    }

    public void setRequestPtNewItem(String requestPtNewItem) {
        this.requestPtNewItem = requestPtNewItem;
    }

    public String getRequestPtRcItem() {
        return requestPtRcItem;
    }

    public void setRequestPtRcItem(String requestPtRcItem) {
        this.requestPtRcItem = requestPtRcItem;
    }

    public String getRequestPtId() {
        return requestPtId;
    }

    public void setRequestPtId(String requestPtId) {
        this.requestPtId = requestPtId;
    }

    public String getOtherStockReq() {
        return otherStockReq;
    }

    public void setOtherStockReq(String otherStockReq) {
        this.otherStockReq = otherStockReq;
    }

    public String getApprovedId() {
        return approvedId;
    }

    public void setApprovedId(String approvedId) {
        this.approvedId = approvedId;
    }

    public String getApprovedQuandity() {
        return approvedQuandity;
    }

    public void setApprovedQuandity(String approvedQuandity) {
        this.approvedQuandity = approvedQuandity;
    }

    public String[] getIssuedQtys() {
        return issuedQtys;
    }

    public void setIssuedQtys(String[] issuedQtys) {
        this.issuedQtys = issuedQtys;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getServicePointId() {
        return servicePointId;
    }

    public void setServicePointId(String servicePointId) {
        this.servicePointId = servicePointId;
    }

    public String getServicePointName() {
        return servicePointName;
    }

    public void setServicePointName(String servicePointName) {
        this.servicePointName = servicePointName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String[] getPriceIds() {
        return priceIds;
    }

    public void setPriceIds(String[] priceIds) {
        this.priceIds = priceIds;
    }

    public String getIssuedQty() {
        return issuedQty;
    }

    public void setIssuedQty(String issuedQty) {
        this.issuedQty = issuedQty;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getGdId() {
        return gdId;
    }

    public void setGdId(String gdId) {
        this.gdId = gdId;
    }

    public String getAcceptedQty() {
        return acceptedQty;
    }

    public void setAcceptedQty(String acceptedQty) {
        this.acceptedQty = acceptedQty;
    }

    public String[] getAcceptedQtys() {
        return acceptedQtys;
    }

    public void setAcceptedQtys(String[] acceptedQtys) {
        this.acceptedQtys = acceptedQtys;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getWoId() {
        return woId;
    }

    public void setWoId(String woId) {
        this.woId = woId;
    }

    public String getRcItemId() {
        return rcItemId;
    }

    public void setRcItemId(String rcItemId) {
        this.rcItemId = rcItemId;
    }

    public String[] getItemAmounts() {
        return itemAmounts;
    }

    public void setItemAmounts(String[] itemAmounts) {
        this.itemAmounts = itemAmounts;
    }

    public String[] getRcItemIds() {
        return rcItemIds;
    }

    public void setRcItemIds(String[] rcItemIds) {
        this.rcItemIds = rcItemIds;
    }

    public String[] getRcStatuses() {
        return rcStatuses;
    }

    public void setRcStatuses(String[] rcStatuses) {
        this.rcStatuses = rcStatuses;
    }

    public String getRcStatus() {
        return rcStatus;
    }

    public void setRcStatus(String rcStatus) {
        this.rcStatus = rcStatus;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String Desc) {
        this.Desc = Desc;
    }

    public String getReqDate() {
        return ReqDate;
    }

    public void setReqDate(String ReqDate) {
        this.ReqDate = ReqDate;
    }

    public String[] getReqQuant() {
        return ReqQuant;
    }

    public void setReqQuant(String[] ReqQuant) {
        this.ReqQuant = ReqQuant;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getElapsedDays() {
        return elapsedDays;
    }

    public void setElapsedDays(String elapsedDays) {
        this.elapsedDays = elapsedDays;
    }

    public String getServicePtName() {
        return servicePtName;
    }

    public void setServicePtName(String servicePtName) {
        this.servicePtName = servicePtName;
    }

    public String[] getArrLength() {
        return arrLength;
    }

    public void setArrLength(String[] arrLength) {
        this.arrLength = arrLength;
    }

    public String getRcPrice() {
        return rcPrice;
    }

    public void setRcPrice(String rcPrice) {
        this.rcPrice = rcPrice;
    }

    public String[] getRcPrices() {
        return rcPrices;
    }

    public void setRcPrices(String[] rcPrices) {
        this.rcPrices = rcPrices;
    }

    public String getStockQty() {
        return stockQty;
    }

    public void setStockQty(String stockQty) {
        this.stockQty = stockQty;
    }

    public String getRcQty() {
        return rcQty;
    }

    public void setRcQty(String rcQty) {
        this.rcQty = rcQty;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getTyreType() {
        return tyreType;
    }

    public void setTyreType(String tyreType) {
        this.tyreType = tyreType;
    }

    public String getTyreNo() {
        return tyreNo;
    }

    public void setTyreNo(String tyreNo) {
        this.tyreNo = tyreNo;
    }

    public String getTyreId() {
        return tyreId;
    }

    public void setTyreId(String tyreId) {
        this.tyreId = tyreId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getGrId() {
        return grId;
    }

    public void setGrId(String grId) {
        this.grId = grId;
    }

    public String getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(String issuedDate) {
        this.issuedDate = issuedDate;
    }

    public String getFromSpId() {
        return fromSpId;
    }

    public void setFromSpId(String fromSpId) {
        this.fromSpId = fromSpId;
    }

    public String getToSpId() {
        return toSpId;
    }

    public void setToSpId(String toSpId) {
        this.toSpId = toSpId;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getPricetype() {
        return pricetype;
    }

    public void setPricetype(String pricetype) {
        this.pricetype = pricetype;
    }

    public String getNewQty() {
        return newQty;
    }

    public void setNewQty(String newQty) {
        this.newQty = newQty;
    }

    public String getReqType() {
        return reqType;
    }

    public void setReqType(String reqType) {
        this.reqType = reqType;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }
    
    
    
    
}
