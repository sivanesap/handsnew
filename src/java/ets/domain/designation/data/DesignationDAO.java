/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
-------------------------------------------------------------------------*/
package ets.domain.designation.data;

import ets.arch.exception.FPRuntimeException;
import ets.domain.designation.business.DesignationTO;
import ets.domain.util.FPLogUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

/******************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver    Date                               Author                    Change
 * ----------------------------------------------------------------------------
 * 1.0   __DATE__                Your_Name ,Entitle      Created
 *
 ******************************************************************************/
public class DesignationDAO extends SqlMapClientDaoSupport {

    /** Creates a new instance of __NAME__ */
    public DesignationDAO() {
    }
    private final static String CLASS = "DesignationDAO";

    /**
     * This method used to Get Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDesignaList() {
        Map map = new HashMap();
        ArrayList DesignaList = new ArrayList();
        try {
            DesignaList = (ArrayList) getSqlMapClientTemplate().queryForList("designation.DesignaList", map);
          
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "DesignaList", sqlException);
        }
        return DesignaList;

    }
     /**
     * This method used to Insert Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doDesigDetails(DesignationTO designationTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("desigName", designationTO.getDesigName());
        map.put("desigDesc", designationTO.getDescription());

        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("designation.insertDesignation", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }
    
    public String doCheckDesigName(String designationName) {
        boolean status = false;
        String desigName = "";
        Map map = new HashMap();
        try {
           
            map.put("designationName", designationName);
            desigName = (String) getSqlMapClientTemplate().queryForObject("designation.checkdesignationName", map);
          
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkDesigName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "checkDesigName", sqlException);
        }
        return desigName;
    }
    
     /**
     * This method used to Modify Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doDesignaDetailsModify(ArrayList List, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {
            Iterator itr = List.iterator();
            DesignationTO designationTO = null;
            while (itr.hasNext()) {
                designationTO = (DesignationTO) itr.next();
                map.put("desigId", designationTO.getDesigId());
                map.put("desigName", designationTO.getDesigName());
                map.put("Description", designationTO.getDescription()); 
                map.put("Active_Ind", designationTO.getActiveInd());
             
               
                status = (Integer) getSqlMapClientTemplate().update("designation.updateDesigna", map);
              
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,"getUserAuthorisedFunctions", sqlException);
        }
        return status;



    }
    
     /**
     * This method used to Get Desgintion Name.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getDesigName(int desigId) {
        Map map = new HashMap();
        map.put("desigId", desigId);
       
        String desigName = null;
        try {
            desigName = (String) getSqlMapClientTemplate().queryForObject("designation.desigName", map);
           
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "desigName", sqlException);
        }
        return desigName;


    }
    
    /**
     * This method used to Get Grade Details According to Designations
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
  
    
    
    
    
    public ArrayList getGradeList(int desigId) {

        Map map = new HashMap();
        map.put("desigId", desigId);
        ArrayList GradeList = new ArrayList();
        try {
            GradeList = (ArrayList) getSqlMapClientTemplate().queryForList("designation.GradeList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GradeList", sqlException);
        }
        return GradeList;

    }
    
     /**
     * This method used to Insert Grade Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doGradeDetails(DesignationTO designationTO, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("desigId", designationTO.getDesigId());
        map.put("gradeName", designationTO.getGradeName());
        map.put("description", designationTO.getDescription());
        int status = 0;
        int gradeId = 0;
        try {
            
            status = (Integer) getSqlMapClientTemplate().update("designation.insertGrade", map);
 
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;


    }
    
     /**
     * This method used to Get Grade Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getGradeList() {
        Map map = new HashMap();
        ArrayList GradeList = new ArrayList();
        try {
            GradeList = (ArrayList) getSqlMapClientTemplate().queryForList("designation.GradeList1", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GradeList", sqlException);
        }
        return GradeList;

    }
    
     /**
     * This method used to Modify grade  Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doGradeDetailsModify(ArrayList List, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {
            Iterator itr = List.iterator();
            DesignationTO designationTO = null;
            while (itr.hasNext()) {
                designationTO = (DesignationTO) itr.next();
                map.put("gradeId", designationTO.getGradeId());
                map.put("gradeName", designationTO.getGradeName());
                map.put("description", designationTO.getDescription());
                map.put("activeInd", designationTO.getActiveInd());
                map.put("userId", UserId);
                status = (Integer) getSqlMapClientTemplate().update("designation.updateGrade", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }

    /**
     * This method used to Get Active Grade Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getActiveGradeList(int desigId) {
        Map map = new HashMap();
        ArrayList gradeList = new ArrayList();
        try {
            map.put("desigId", desigId);
            gradeList = (ArrayList) getSqlMapClientTemplate().queryForList("designation.getActiveGrade", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GradeList", sqlException);
        }
        return gradeList;
    }    
    
 }

