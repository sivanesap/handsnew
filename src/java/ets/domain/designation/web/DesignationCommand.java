/*-------------------------------------------------------------------------
  * __NAME__.java
  * __DATE__
  *
  * Copyright (c) Entitle.
  * All Rights Reserved.
  *
  * This software is the confidential and proprietary information of
  * Entitle ("Confidential Information"). You shall
  * not disclose such Confidential Information and shall use it only in
  * accordance with the terms of the license agreement you entered into
  * with Entitle.
  -------------------------------------------------------------------------*/

package ets.domain.designation.web;
/******************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver    Date                               Author                    Change
 * ----------------------------------------------------------------------------
 * 1.0   __DATE__                Your_Name ,Entitle      Created
 *
 ******************************************************************************/

public class DesignationCommand {

    /** Creates a new instance of __NAME__ */
    public DesignationCommand() {
    }
    
    private String desigName = "";
    private String[] desigIds = null;
    private String[] desigNames = null;
    private String[] activeInds = null;
     private String activeInd = null;
    private String[] selectedIndex = null;
    private String  designationId = "";
    private String gradeName = "";
    private String description = "";
    private String[] gradeIds = null;
    private String[] gradeNames = null;
    private String[] descriptions = null;
    private String desigId = "";

    public String getDesigId() {
        return desigId;
    }

    public void setDesigId(String desigId) {
        this.desigId = desigId;
    }

    public String[] getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String[] descriptions) {
        this.descriptions = descriptions;
    }

    

    public String[] getGradeIds() {
        return gradeIds;
    }

    public void setGradeIds(String[] gradeIds) {
        this.gradeIds = gradeIds;
    }

    public String[] getGradeNames() {
        return gradeNames;
    }

    public void setGradeNames(String[] gradeNames) {
        this.gradeNames = gradeNames;
    }

    public String[] getActiveInds() {
        return activeInds;
    }

    public void setActiveInds(String[] activeInds) {
        this.activeInds = activeInds;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getDesigIds() {
        return desigIds;
    }

    public void setDesigIds(String[] desigIds) {
        this.desigIds = desigIds;
    }

    public String getDesigName() {
        return desigName;
    }

    public void setDesigName(String desigName) {
        this.desigName = desigName;
    }

    public String[] getDesigNames() {
        return desigNames;
    }

    public void setDesigNames(String[] desigNames) {
        this.desigNames = desigNames;
    }

    public String getDesignationId() {
        return designationId;
    }

    public void setDesignationId(String designationId) {
        this.designationId = designationId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }
    

}
