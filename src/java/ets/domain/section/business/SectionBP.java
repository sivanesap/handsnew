package ets.domain.section.business;

 
import ets.domain.racks.business.RackTO;
import ets.domain.company.business.CompanyTO;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.section.data.SectionDAO;
import ets.domain.vehicle.data.VehicleDAO;
import java.util.ArrayList;
import java.util.Iterator;

public class SectionBP {

    private SectionDAO sectionDAO;
    private VehicleDAO vehicleDAO;

    public SectionDAO getSectionDAO() {
        return sectionDAO;
    }

    public void setSectionDAO(SectionDAO sectionDAO) {
        this.sectionDAO = sectionDAO;
    }

    public VehicleDAO getVehicleDAO() {
        return vehicleDAO;
    }

    public void setVehicleDAO(VehicleDAO vehicleDAO) {
        this.vehicleDAO = vehicleDAO;
    }

   
    
    //processGetSectionList
    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises -m
     */
    public ArrayList processGetSectionList() throws FPRuntimeException, FPBusinessException {
        
        ArrayList MfrList = new ArrayList();
         MfrList = sectionDAO.getSectionList();
         if (MfrList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
        }
        return  MfrList;
    }
    //processGetCategoryList
     /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetCategoryList() throws FPRuntimeException, FPBusinessException {
        
        ArrayList categoryList = new ArrayList();
         categoryList = sectionDAO.getCategoryList();
          
        return  categoryList;
    }
    /**
     * This method used to Insert Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises -m
     */
    public int processInsertSectionDetails(SectionTO sectionTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = sectionDAO.doSectionDetails(sectionTO, UserId);
        if (insertStatus == 0) {
//            throw new FPBusinessException("EM-SEC-01");
        }
        return insertStatus;
    }
    
     /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises -m
     */
    public int processModifySectionDetails(ArrayList List, int UserId) throws FPRuntimeException, FPBusinessException  {
        int insertStatus = 0;
        insertStatus = sectionDAO.doSectionDetailsModify(List, UserId);
        if (insertStatus == 0) {
//            throw new FPBusinessException("EM-SEC-02");
        }
        return insertStatus;
    }
    
     /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetUomList() throws FPRuntimeException, FPBusinessException {
        
        ArrayList MfrList = new ArrayList();
         MfrList = sectionDAO.getUomList();
          
        return  MfrList;
    }
   
    
    //parts starts here ...
    //processInsertPartsDetails
    
     /**
     * This method used to Insert Parts Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises  -m
     */
    public void processInsertPartsDetails(SectionTO sectionTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int itemId = 0;
        ArrayList spList = new ArrayList();
        Iterator itr ;
        CompanyTO compTO;
        int spId = 0;
        itemId = sectionDAO.doPartsDetails(sectionTO, UserId);
        System.out.println("last insert Itemid="+itemId);
        if (itemId == 0) {
            throw new FPBusinessException("EM-PRT-01");
        }
        spList = vehicleDAO.getspList();
        itr = spList.iterator();
        while(itr.hasNext()){
            compTO = new CompanyTO();
            compTO = (CompanyTO) itr.next();
            spId = Integer.parseInt(compTO.getSpId());
            sectionDAO.doInsertStock(spId,itemId);
        }        
    }
    
    
    //getAjaxModelList
    
     /**
     * This method used to Get parts  throw ajax Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String processGetModels(int mfrId) throws FPRuntimeException, FPBusinessException {
        ArrayList models = new ArrayList();
        int counter = 0;
        SectionTO rack = null;
        models = sectionDAO.getAjaxModelList(mfrId);
       
        
        Iterator itr;
        String model = "";
        if (models.size() == 0) {
            model = "";
        }else{
            itr = models.iterator();
            while(itr.hasNext()){
                rack = new SectionTO();
                rack = (SectionTO) itr.next();
                if(counter == 0){
                    model = rack.getModelId() + "-" + rack.getModelName();
                    counter++;
                }else{
                    model = model + "~" + rack.getModelId() + "-" + rack.getModelName();                    
                }
            }
        }
        return model;
    }
    
   // processGetSubRack
     /**
     * This Ajax  method used to Get  sub Rack List  Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String processGetSubRack(int rackId) throws FPRuntimeException, FPBusinessException {
        ArrayList models = new ArrayList();
        int counter = 0;
        RackTO rack = null;
        models = sectionDAO.getAjaxSubRackList(rackId);
       
        
        Iterator itr;
        String model = "";
        if (models.size() == 0) {
            model = "";
        }else{
            itr = models.iterator();
            while(itr.hasNext()){
                rack = new RackTO();
                rack = (RackTO) itr.next();
                if(counter == 0){
                    model = rack.getSubRackId() + "-" + rack.getSubRackName();
                    counter++;
                }else{
                    model = model + "~" + rack.getSubRackId() + "-" + rack.getSubRackName();                    
                }
            }
        }
        return model;
    }
    
     /**
     * This method used to get Parts Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
     public ArrayList getpartsDetails(SectionTO sectionTO) throws FPRuntimeException, FPBusinessException {
        ArrayList List = new ArrayList();
        List = sectionDAO.getPartsDetails(sectionTO);
        if (List.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return List;
           }
     
      /**
     * This method used to Insert Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
     public ArrayList processGetPartsDetail(int itemId) throws FPRuntimeException, FPBusinessException {
        ArrayList partsDetail = new ArrayList();
        partsDetail = (ArrayList) sectionDAO.getAlterPartsDetail(itemId);
         
        return partsDetail;
    }
 
     //alter
     
      /**
     * This method used to Update Parts  Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises  -m
     */
    public int processUpdatePartsDetails(SectionTO sectionTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = sectionDAO.doPartsUpdateDetails(sectionTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-PRT-02");
        }
        return insertStatus;
    }
    
    //getTotalParts
      /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int getTotalParts(SectionTO sectionTO) throws FPRuntimeException, FPBusinessException {
        
        int records=0;
         records = sectionDAO.getTotalParts(sectionTO);
         
        return  records;
    }
    
    //getpartsDetails(startIndex,endIndex)
      /**
     * This method used to Insert Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
     public ArrayList getpartsDetails(int startIndex,int endIndex,SectionTO sectionTO,int companyId) throws FPRuntimeException, FPBusinessException {
        ArrayList indexedPartsDetail = new ArrayList();
        ArrayList newList = new ArrayList();
        String lastPurchased = "";
        Iterator itr;
        float qty = 0;
        SectionTO sec;
        indexedPartsDetail = (ArrayList) sectionDAO.getPartsByPage(startIndex,endIndex,sectionTO);
        itr = indexedPartsDetail.iterator();
        
        while(itr.hasNext()){
            sec = new SectionTO();
            sec = (SectionTO) itr.next();
            qty = sectionDAO.getItemStock(Integer.parseInt(sec.getItemId()),companyId);
            sec.setStockLevel(String.valueOf(qty));
            //Hari
            //qty = sectionDAO.getItemStockLastIssued(Integer.parseInt(sec.getItemId()),companyId);
            System.out.println("Last Issued Qty-->"+qty+"for Item Id-->"+sec.getItemId());
            //sec.setLastIssuedQty(String.valueOf(qty));

            //qty = sectionDAO.getItemStockLastDelivered(Integer.parseInt(sec.getItemId()),companyId);
            System.out.println("Last Transfered Qty-->"+qty+"for Item Id-->"+sec.getItemId());
            //sec.setLastTransferedQty(String.valueOf(qty));


            //lastPurchased = sectionDAO.getItemStockLastPurchased(Integer.parseInt(sec.getItemId()),companyId);
            System.out.println("In Section BP LastPurchased Have-->"+lastPurchased);

//            String temp[]=lastPurchased.split("-");
//            System.out.println("Temp[0]-->"+temp[0]+"Temp[1]-->"+temp[1]);
//            System.out.println("Last Purchased Qty-->"+qty+"for Item Id-->"+sec.getItemId());
//            sec.setLastPurchasedQty(String.valueOf(temp[0]));
//            sec.setPrice(String.valueOf(temp[1]));
            //Hari
            newList.add(sec);
        }
          
        return newList;
    }
     
     
    public String processItemSuggests(String itemName, String mfrId, String modelId ) {
        String itemNames = "";
        itemName = itemName + "%";
        itemNames = sectionDAO.getItemSuggests(itemName,mfrId, modelId );
        return itemNames;
    }     
    
    public String processCheckItemExists(String choice,String param) {
        String itemNames = "";        
        itemNames = sectionDAO.checkItemExists(choice,param );
        return itemNames;
    }

 //Hari Start
    /**
     * This method used to Get Group Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises  -m
     */
    public ArrayList processGetGroupList() {
        ArrayList groupList = new ArrayList();
        groupList = sectionDAO.getGroupList();
        return groupList;
    }

    public ArrayList processGetVendorList() {
        ArrayList venList = new ArrayList();
        venList = sectionDAO.getVendorList();
        return venList;
    }

    public ArrayList processGetGroupListForParts() {
        ArrayList groupList = new ArrayList();
        groupList = sectionDAO.getGroupListForParts();
        return groupList;
    }
//alter

      /**
     * This method used to Insert Group Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises  -m
     */
    public int processInsertGroup(SectionTO sectionTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = sectionDAO.doGroupInsertDetails(sectionTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-GRP-01");
        }
        return insertStatus;
    }


    public int processUpdateGroup(ArrayList List, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = sectionDAO.doUpdateGroup(List, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-GRP-02");
        }
        return status;
    }
    
    public ArrayList getServiceTypeMaster(SectionTO sectionTO) throws FPRuntimeException, FPBusinessException {
        ArrayList serviceTypeList = new ArrayList();
        serviceTypeList = sectionDAO.getServiceTypeMaster(sectionTO);
        return serviceTypeList;
    }
    
    public String checkServiceTypeName(String serviceName) {
        String serviceTypeName = "";        
        serviceTypeName = sectionDAO.checkServiceTypeName(serviceName);
        return serviceTypeName;
    }
    
    public int insertServiceType(SectionTO sectionTO,int userId) {
        int insertServiceType = 0;        
        insertServiceType = sectionDAO.insertServiceType(sectionTO,userId);
        return insertServiceType;
    }
    public int updateServiceType(SectionTO sectionTO,int userId) {
        int updateServiceType = 0;        
        updateServiceType = sectionDAO.updateServiceType(sectionTO,userId);
        return updateServiceType;
    }

 }

