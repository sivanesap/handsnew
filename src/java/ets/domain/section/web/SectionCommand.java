package ets.domain.section.web;

/**
 *
 * @author vidya
 */
public class SectionCommand {

   String[] serviceTypeIds=null;
   String serviceTypeName="";
   String serviceTypeId="";
   String serviceTypeDesc="";
   String status="";
   String pageNo="";
   String totalPages=""; 
    String startIndex="";
    String endIndex="";
 
    
    
    private String sectionId="";
    private String sectionCode="";
    private String sectionName="";
    private String description="";
    private String activeInd="";
    private String rackId="";
    private String[] sectionIds;
    private String[] sectionNames;
    private String[] sectionCodes;
    private String[] descriptions;
    private String[] activeInds;
    
         private String uom="";
         private String itemCode="";
         private String paplCode="";
         private String itemName="";
         private String maxQuandity="";
         private String roLevel="";
         private String specification="";
         private String reConditionable="";
         private String scrapUomId="";
         private String scrapUomName="";
         private String mfrId="";
         private String modelId="";
         private String uomId="";
         private String itemId="";
         private String subRackId="";
         private String vendorId="";
          private String[] selectedIndex;
          
          
          private String categoryId="";
           private String categoryName="";
           //Hari
           private String vatId=null;
           private String groupId=null;
           private String groupName=null;
           private String[] groupIdList;
           private String[] groupNameList;
           private String[] groupDescriptionList;
           private String[] groupStatusList;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }


    public String[] getGroupDescriptionList() {
        return groupDescriptionList;
    }

    public void setGroupDescriptionList(String[] groupDescriptionList) {
        this.groupDescriptionList = groupDescriptionList;
    }

    public String[] getGroupIdList() {
        return groupIdList;
    }

    public void setGroupIdList(String[] groupIdList) {
        this.groupIdList = groupIdList;
    }

    public String[] getGroupNameList() {
        return groupNameList;
    }

    public void setGroupNameList(String[] groupNameList) {
        this.groupNameList = groupNameList;
    }

    public String[] getGroupStatusList() {
        return groupStatusList;
    }

    public void setGroupStatusList(String[] groupStatusList) {
        this.groupStatusList = groupStatusList;
    }



    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }


    public String getVatId() {
        return vatId;
    }

    public void setVatId(String vatId) {
        this.vatId = vatId;
    }


    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
           
           

    public String getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(String endIndex) {
        this.endIndex = endIndex;
    }

    public String getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(String startIndex) {
        this.startIndex = startIndex;
    }

          
          
    public String getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }

          
          
    public String getPageNo() {
        return pageNo;
    }

    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }

          
    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getSubRackId() {
        return subRackId;
    }

    public void setSubRackId(String subRackId) {
        this.subRackId = subRackId;
    }
          
          
    public String getScrapUomName() {
        return scrapUomName;
    }

    public void setScrapUomName(String scrapUomName) {
        this.scrapUomName = scrapUomName;
    }
         
 private String mfrCode;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

 
 
    public String getMfrCode() {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode) {
        this.mfrCode = mfrCode;
    }
 
    public String getScrapUomId() {
        return scrapUomId;
    }

    public void setScrapUomId(String scrapUomId) {
        this.scrapUomId = scrapUomId;
    }

         
    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }
         
         
         

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }
         

    
         

    public String getReConditionable() {
        return reConditionable;
    }

    public void setReConditionable(String reConditionable) {
        this.reConditionable = reConditionable;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }
         
         

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getMaxQuandity() {
        return maxQuandity;
    }

    public void setMaxQuandity(String maxQuandity) {
        this.maxQuandity = maxQuandity;
    }

    public String getPaplCode() {
        return paplCode;
    }

    public void setPaplCode(String paplCode) {
        this.paplCode = paplCode;
    }

    public String getRoLevel() {
        return roLevel;
    }

    public void setRoLevel(String roLevel) {
        this.roLevel = roLevel;
    }
         
         

    public String[] getActiveInds() {
        return activeInds;
    }

    public void setActiveInds(String[] activeInds) {
        this.activeInds = activeInds;
    }

    public String[] getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String[] descriptions) {
        this.descriptions = descriptions;
    }

    public String[] getSectionIds() {
        return sectionIds;
    }

    public void setSectionIds(String[] sectionIds) {
        this.sectionIds = sectionIds;
    }

    public String[] getSectionNames() {
        return sectionNames;
    }

    public void setSectionNames(String[] sectionNames) {
        this.sectionNames = sectionNames;
    }
    
    

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }
    
    
    
    public String getRackId() {
        return rackId;
    }

    public void setRackId(String rackId) {
        this.rackId = rackId;
    }

    public String getSectionCode() {
        return sectionCode;
    }

    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }

    public String[] getSectionCodes() {
        return sectionCodes;
    }

    public void setSectionCodes(String[] sectionCodes) {
        this.sectionCodes = sectionCodes;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public String getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(String serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getServiceTypeDesc() {
        return serviceTypeDesc;
    }

    public void setServiceTypeDesc(String serviceTypeDesc) {
        this.serviceTypeDesc = serviceTypeDesc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String[] getServiceTypeIds() {
        return serviceTypeIds;
    }

    public void setServiceTypeIds(String[] serviceTypeIds) {
        this.serviceTypeIds = serviceTypeIds;
    }
   
}
