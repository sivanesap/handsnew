/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.department.business;

/**
 *
 * @author vinoth
 */
public class DepartmentTO {

    private String departmentId = "";
    private String name = "";
    private String description = "";
    private String status = "";
    private String gradeName = "";

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
}