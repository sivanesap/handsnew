/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.ops.web;

/**
 *
 * @author vijay
 */
public class OpsCommand {
    private String awbType = "";
    private String airlineName = "";
    private String allowedTrucks = "";
    private String noOfTrucks = "";
    //TTK
    private String totalVolumeActual = "";
    private String fromDate = "";
    private String toDate = "";
    private String awbTotalGrossWeight = "";
    private String awbTotalPackages = "";
    private String awbReceivedPackages = "";
    private String awbPendingPackages = "";

    private String payBill = "";
    private String tspPerkg = "";
    private String tsrPerkg = "";
    private String customerNetAmount = "";
    private String shipmentType = "";
    private String consignmentEfoNo = "";
    private String contractFrom = "";
    private String consignmentNoteEmno = "";
    private String consignmentNoteAtdNo = "";
    private String contractTo = "";
    private String contractNo = "";
    private String[] originIdFullTruck = null;
    private String[] originNameFullTruck = null;
    private String[] destinationIdFullTruck = null;
    private String[] destinationNameFullTruck = null;
    private String[] routeIdFullTruck = null;
    private String[] travelKmFullTruck = null;
    private String[] travelHourFullTruck = null;
    private String[] travelMinuteFullTruck = null;
    private String[] movementTypeIdFullTruck = null;
    private String[] vehicleTypeIdFullTruck = null;
    private String[] rateWithReeferFullTruck = null;
    private String[] rateWithOutReeferFullTruck = null;
    private String[] originIdWeightBreak = null;
    private String[] originNameWeightBreak = null;
    private String[] destinationIdWeightBreak = null;
    private String[] destinationNameWeightBreak = null;
    private String[] routeIdWeightBreak = null;
    private String[] travelKmWeightBreak = null;
    private String[] travelHourWeightBreak = null;
    private String[] travelMinuteWeightBreak = null;
    private String[] movementTypeIdWeightBreak = null;
    private String[] fromKgWeightBreak = null;
    private String[] toKgWeightBreak = null;
    private String[] rateWithReeferWeightBreak = null;
    private String[] rateWithOutReeferWeightBreak = null;
    
    
    
    private String commodity = "";
    private String productRate = "";
    private String[] volumes = null;
    private String[] vehicleNo = null;
    private String[] vehicleId = null;
    private String contract = "";
    private String totalChargeableWeights = "";
    private String totalVolume = "";
    private String rateValue = "";
    private String orderReferenceAwb = "";
    private String orderReferenceAwbNo = "";
    private String awbOrigin = "";
    private String awbOriginId = "";
    private String awbDestination = "";
    private String awbDestinationId = "";
    private String awbOrderDeliveryDate = "";
    private String awbDestinationRegion = "";
    private String awbMovementType = "";
    private String[] noOfPieces = null;
    private String[] grossWeight = null;
    private String[] chargeableWeight = null;
    private String[] chargeableWeightId = null;
    private String[] uom = null;
    private String[] lengths = null;
    private String[] widths = null;
    private String[] heights = null;
    private String[] dgHandlingCode = null;
    private String[] unIdNo = null;
    private String[] classCode = null;
    private String[] pkgInstruction = null;
    private String[] pkgGroup = null;
    private String[] netQuantity = null;
    private String[] netUnits = null;
    private String[] truckTravelKm = null;
    private String[] truckTravelHour = null;
    private String[] truckTravelMinute = null;
    private String[] fleetTypeId = null;
    private String[] truckDepDate = null;
    private String[] truckDestinationId = null;
    private String[] truckRouteId = null;
    private String freightReceipt = "";
    private String wareHouseName = "";
    private String wareHouseLocation = "";
    private String shipmentAcceptanceDate = "";
    private String shipmentAccpHour = "";
    private String shipmentAccpMinute = "";
    private String currencyType = "";
    private String rateMode = "";
    private String rateMode1 = "";
    private String perKgRate = "";
    private String rateValue1 = "";
    private String[] bookingReferenceRemarks = null;
    private String orderDeliveryHour = "";
    private String orderDeliveryMinute = "";
    //TTK
    private String customerTypeId = "";
    private String paymentType = "";
    private String entryType = "";
    private String consignmentDate = "";
    private String orderReferenceNo = "";
    private String orderReferenceRemarks = "";
    private String productCategoryId = "";
    private String consignmentNoteNo = "";
    private String customerId = "";
    private String customerName = "";
    private String customerCode = "";
    private String customerAddress = "";
    private String pincode = "";
    private String customerMobileNo = "";
    private String mailId = "";
    private String customerPhoneNo = "";
    private String billingTypeId = "";
    private String contractId = "";
    private String destination = "";
    private String origin = "";
    private String businessType = "";
    private String multiPickup = "";
    private String multiDelivery = "";
    private String consignmentOrderInstruction = "";
    private String[] productCodes = null;
    private String[] batchCode = null;
    private String[] productNames = null;
    private String[] packagesNos = null;
    private String[] weights = null;
    private String totalPackage = "";
    private String totalWeightage = "";
    private String ServiceType = "";
    private String vehicleTypeId = "";
    private String reeferRequired = "";
    private String routeContractId = "";
    private String routeId = "";
    private String contractRateId = "";
    private String totalKm = "";
    private String totalHours = "";
    private String totalMinutes = "";
    private String rateWithReefer = "";
    private String rateWithoutReefer = "";
    private String vehicleRequiredDate = "";
    private String vehicleRequiredHour = "";
    private String vehicleRequiredMinute = "";
    private String vehicleInstruction = "";
    private String consignorName = "";
    private String consignorPhoneNo = "";
    private String consignorAddress = "";
    private String consigneeName = "";
    private String consigneePhoneNo = "";
    private String consigneeAddress = "";
    private String totFreightAmount = "";
    private String docCharges = "";
    private String odaCharges = "";
    private String multiPickupCharge = "";
    private String multiDeliveryCharge = "";
    private String handleCharges = "";
    private String otherCharges = "";
    private String unloadingCharges = "";
    private String loadingCharges = "";
    private String subTotal = "";
    private String totalCharges = "";
    private String walkinCustomerName = "";
    private String walkinCustomerCode = "";
    private String walkinCustomerAddress = "";
    private String walkinPincode = "";
    private String walkinCustomerMobileNo = "";
    private String walkinMailId = "";
    private String walkinCustomerPhoneNo = "";
    private String walkInBillingTypeId = "";
    private String walkinFreightWithReefer = "";
    private String walkinFreightWithoutReefer = "";
    private String walkinRateWithReeferPerKg = "";
    private String walkinRateWithoutReeferPerKg = "";
    private String walkinRateWithReeferPerKm = "";
    private String walkinRateWithoutReeferPerKm = "";
    private String consignmentOrderId = "";
    private String[] truckOriginId = null;
    private String[] pointName = null;
    private String[] pointType = null;
    private String[] pointOrder = null;
    private String[] pointAddresss = null;
    private String[] multiplePointRouteId = null;
    private String[] pointPlanDate = null;
    private String[] pointPlanHour = null;
    private String[] pointPlanMinute = null;
    private String[] consignmentArticleId = null;
    private String[] consignmentRouteCourseId = null;
        private String fromAddress = "";
        private String awbOriginRegion = "";
        private String toAddress = "";
        private String fleetDetails = "";
        private String []consignmentSbNo;
        private String []consignmentOrderNos;
        private String customsRemark = "";
        private String flightDate = "";
        private String consignmentTpNo = "";
        private String consignmentEgmNo = "";
        private String operatorName = "";
        private String flightNo = "";
        private String manifestDate = "";        
        private String lodingPoint = "";
        private String unLodingPoint = "";
        private String []usedVol ;
        private String []usedCapacity ;
        private String []vehicleRegNo;
        private String []totalTsaGoods;
        private String orderReferenceEnd = "";
        private String transhipmentNo = "";

    public String getServiceType() {
        return ServiceType;
    }

    public void setServiceType(String ServiceType) {
        this.ServiceType = ServiceType;
    }

    public String[] getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String[] batchCode) {
        this.batchCode = batchCode;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneePhoneNo() {
        return consigneePhoneNo;
    }

    public void setConsigneePhoneNo(String consigneePhoneNo) {
        this.consigneePhoneNo = consigneePhoneNo;
    }

    public String getConsignmentDate() {
        return consignmentDate;
    }

    public void setConsignmentDate(String consignmentDate) {
        this.consignmentDate = consignmentDate;
    }

    public String getConsignmentNoteNo() {
        return consignmentNoteNo;
    }

    public void setConsignmentNoteNo(String consignmentNoteNo) {
        this.consignmentNoteNo = consignmentNoteNo;
    }

    public String getConsignmentOrderInstruction() {
        return consignmentOrderInstruction;
    }

    public void setConsignmentOrderInstruction(String consignmentOrderInstruction) {
        this.consignmentOrderInstruction = consignmentOrderInstruction;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getConsignorPhoneNo() {
        return consignorPhoneNo;
    }

    public void setConsignorPhoneNo(String consignorPhoneNo) {
        this.consignorPhoneNo = consignorPhoneNo;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getContractRateId() {
        return contractRateId;
    }

    public void setContractRateId(String contractRateId) {
        this.contractRateId = contractRateId;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerMobileNo() {
        return customerMobileNo;
    }

    public void setCustomerMobileNo(String customerMobileNo) {
        this.customerMobileNo = customerMobileNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhoneNo() {
        return customerPhoneNo;
    }

    public void setCustomerPhoneNo(String customerPhoneNo) {
        this.customerPhoneNo = customerPhoneNo;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDocCharges() {
        return docCharges;
    }

    public void setDocCharges(String docCharges) {
        this.docCharges = docCharges;
    }

    public String getEntryType() {
        return entryType;
    }

    public void setEntryType(String entryType) {
        this.entryType = entryType;
    }

    public String getHandleCharges() {
        return handleCharges;
    }

    public void setHandleCharges(String handleCharges) {
        this.handleCharges = handleCharges;
    }

    public String getLoadingCharges() {
        return loadingCharges;
    }

    public void setLoadingCharges(String loadingCharges) {
        this.loadingCharges = loadingCharges;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getMultiDelivery() {
        return multiDelivery;
    }

    public void setMultiDelivery(String multiDelivery) {
        this.multiDelivery = multiDelivery;
    }

    public String getMultiDeliveryCharge() {
        return multiDeliveryCharge;
    }

    public void setMultiDeliveryCharge(String multiDeliveryCharge) {
        this.multiDeliveryCharge = multiDeliveryCharge;
    }

    public String getMultiPickup() {
        return multiPickup;
    }

    public void setMultiPickup(String multiPickup) {
        this.multiPickup = multiPickup;
    }

    public String getMultiPickupCharge() {
        return multiPickupCharge;
    }

    public void setMultiPickupCharge(String multiPickupCharge) {
        this.multiPickupCharge = multiPickupCharge;
    }

    public String[] getMultiplePointRouteId() {
        return multiplePointRouteId;
    }

    public void setMultiplePointRouteId(String[] multiplePointRouteId) {
        this.multiplePointRouteId = multiplePointRouteId;
    }

    public String getOdaCharges() {
        return odaCharges;
    }

    public void setOdaCharges(String odaCharges) {
        this.odaCharges = odaCharges;
    }

    public String getOrderReferenceNo() {
        return orderReferenceNo;
    }

    public void setOrderReferenceNo(String orderReferenceNo) {
        this.orderReferenceNo = orderReferenceNo;
    }

    public String getOrderReferenceRemarks() {
        return orderReferenceRemarks;
    }

    public void setOrderReferenceRemarks(String orderReferenceRemarks) {
        this.orderReferenceRemarks = orderReferenceRemarks;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(String otherCharges) {
        this.otherCharges = otherCharges;
    }

    public String[] getPackagesNos() {
        return packagesNos;
    }

    public void setPackagesNos(String[] packagesNos) {
        this.packagesNos = packagesNos;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String[] getPointAddresss() {
        return pointAddresss;
    }

    public void setPointAddresss(String[] pointAddresss) {
        this.pointAddresss = pointAddresss;
    }

    public String[] getPointName() {
        return pointName;
    }

    public void setPointName(String[] pointName) {
        this.pointName = pointName;
    }

    public String[] getPointOrder() {
        return pointOrder;
    }

    public void setPointOrder(String[] pointOrder) {
        this.pointOrder = pointOrder;
    }

    public String[] getPointPlanDate() {
        return pointPlanDate;
    }

    public void setPointPlanDate(String[] pointPlanDate) {
        this.pointPlanDate = pointPlanDate;
    }

    public String[] getPointPlanHour() {
        return pointPlanHour;
    }

    public void setPointPlanHour(String[] pointPlanHour) {
        this.pointPlanHour = pointPlanHour;
    }

    public String[] getPointPlanMinute() {
        return pointPlanMinute;
    }

    public void setPointPlanMinute(String[] pointPlanMinute) {
        this.pointPlanMinute = pointPlanMinute;
    }

    public String[] getPointType() {
        return pointType;
    }

    public void setPointType(String[] pointType) {
        this.pointType = pointType;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String[] getProductCodes() {
        return productCodes;
    }

    public void setProductCodes(String[] productCodes) {
        this.productCodes = productCodes;
    }

    public String[] getProductNames() {
        return productNames;
    }

    public void setProductNames(String[] productNames) {
        this.productNames = productNames;
    }

    public String getRateWithReefer() {
        return rateWithReefer;
    }

    public void setRateWithReefer(String rateWithReefer) {
        this.rateWithReefer = rateWithReefer;
    }

    public String getRateWithoutReefer() {
        return rateWithoutReefer;
    }

    public void setRateWithoutReefer(String rateWithoutReefer) {
        this.rateWithoutReefer = rateWithoutReefer;
    }

    public String getReeferRequired() {
        return reeferRequired;
    }

    public void setReeferRequired(String reeferRequired) {
        this.reeferRequired = reeferRequired;
    }

    public String getRouteContractId() {
        return routeContractId;
    }

    public void setRouteContractId(String routeContractId) {
        this.routeContractId = routeContractId;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getTotFreightAmount() {
        return totFreightAmount;
    }

    public void setTotFreightAmount(String totFreightAmount) {
        this.totFreightAmount = totFreightAmount;
    }

    public String getTotalCharges() {
        return totalCharges;
    }

    public void setTotalCharges(String totalCharges) {
        this.totalCharges = totalCharges;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(String totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public String getTotalPackage() {
        return totalPackage;
    }

    public void setTotalPackage(String totalPackage) {
        this.totalPackage = totalPackage;
    }

    public String getTotalWeightage() {
        return totalWeightage;
    }

    public void setTotalWeightage(String totalWeightage) {
        this.totalWeightage = totalWeightage;
    }

    public String getUnloadingCharges() {
        return unloadingCharges;
    }

    public void setUnloadingCharges(String unloadingCharges) {
        this.unloadingCharges = unloadingCharges;
    }

    public String[] getUom() {
        return uom;
    }

    public void setUom(String[] uom) {
        this.uom = uom;
    }

    public String getVehicleInstruction() {
        return vehicleInstruction;
    }

    public void setVehicleInstruction(String vehicleInstruction) {
        this.vehicleInstruction = vehicleInstruction;
    }

    public String getVehicleRequiredDate() {
        return vehicleRequiredDate;
    }

    public void setVehicleRequiredDate(String vehicleRequiredDate) {
        this.vehicleRequiredDate = vehicleRequiredDate;
    }

    public String getVehicleRequiredHour() {
        return vehicleRequiredHour;
    }

    public void setVehicleRequiredHour(String vehicleRequiredHour) {
        this.vehicleRequiredHour = vehicleRequiredHour;
    }

    public String getVehicleRequiredMinute() {
        return vehicleRequiredMinute;
    }

    public void setVehicleRequiredMinute(String vehicleRequiredMinute) {
        this.vehicleRequiredMinute = vehicleRequiredMinute;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getWalkInBillingTypeId() {
        return walkInBillingTypeId;
    }

    public void setWalkInBillingTypeId(String walkInBillingTypeId) {
        this.walkInBillingTypeId = walkInBillingTypeId;
    }

    public String getWalkinCustomerAddress() {
        return walkinCustomerAddress;
    }

    public void setWalkinCustomerAddress(String walkinCustomerAddress) {
        this.walkinCustomerAddress = walkinCustomerAddress;
    }

    public String getWalkinCustomerCode() {
        return walkinCustomerCode;
    }

    public void setWalkinCustomerCode(String walkinCustomerCode) {
        this.walkinCustomerCode = walkinCustomerCode;
    }

    public String getWalkinCustomerMobileNo() {
        return walkinCustomerMobileNo;
    }

    public void setWalkinCustomerMobileNo(String walkinCustomerMobileNo) {
        this.walkinCustomerMobileNo = walkinCustomerMobileNo;
    }

    public String getWalkinCustomerName() {
        return walkinCustomerName;
    }

    public void setWalkinCustomerName(String walkinCustomerName) {
        this.walkinCustomerName = walkinCustomerName;
    }

    public String getWalkinCustomerPhoneNo() {
        return walkinCustomerPhoneNo;
    }

    public void setWalkinCustomerPhoneNo(String walkinCustomerPhoneNo) {
        this.walkinCustomerPhoneNo = walkinCustomerPhoneNo;
    }

    public String getWalkinFreightWithReefer() {
        return walkinFreightWithReefer;
    }

    public void setWalkinFreightWithReefer(String walkinFreightWithReefer) {
        this.walkinFreightWithReefer = walkinFreightWithReefer;
    }

    public String getWalkinFreightWithoutReefer() {
        return walkinFreightWithoutReefer;
    }

    public void setWalkinFreightWithoutReefer(String walkinFreightWithoutReefer) {
        this.walkinFreightWithoutReefer = walkinFreightWithoutReefer;
    }

    public String getWalkinMailId() {
        return walkinMailId;
    }

    public void setWalkinMailId(String walkinMailId) {
        this.walkinMailId = walkinMailId;
    }

    public String getWalkinPincode() {
        return walkinPincode;
    }

    public void setWalkinPincode(String walkinPincode) {
        this.walkinPincode = walkinPincode;
    }

    public String getWalkinRateWithReeferPerKg() {
        return walkinRateWithReeferPerKg;
    }

    public void setWalkinRateWithReeferPerKg(String walkinRateWithReeferPerKg) {
        this.walkinRateWithReeferPerKg = walkinRateWithReeferPerKg;
    }

    public String getWalkinRateWithReeferPerKm() {
        return walkinRateWithReeferPerKm;
    }

    public void setWalkinRateWithReeferPerKm(String walkinRateWithReeferPerKm) {
        this.walkinRateWithReeferPerKm = walkinRateWithReeferPerKm;
    }

    public String getWalkinRateWithoutReeferPerKg() {
        return walkinRateWithoutReeferPerKg;
    }

    public void setWalkinRateWithoutReeferPerKg(String walkinRateWithoutReeferPerKg) {
        this.walkinRateWithoutReeferPerKg = walkinRateWithoutReeferPerKg;
    }

    public String getWalkinRateWithoutReeferPerKm() {
        return walkinRateWithoutReeferPerKm;
    }

    public void setWalkinRateWithoutReeferPerKm(String walkinRateWithoutReeferPerKm) {
        this.walkinRateWithoutReeferPerKm = walkinRateWithoutReeferPerKm;
    }

    public String[] getWeights() {
        return weights;
    }

    public void setWeights(String[] weights) {
        this.weights = weights;
    }

    public String[] getTruckDestinationId() {
        return truckDestinationId;
    }

    public void setTruckDestinationId(String[] truckDestinationId) {
        this.truckDestinationId = truckDestinationId;
    }

    public String[] getTruckOriginId() {
        return truckOriginId;
    }

    public void setTruckOriginId(String[] truckOriginId) {
        this.truckOriginId = truckOriginId;
    }

    public String[] getTruckRouteId() {
        return truckRouteId;
    }

    public void setTruckRouteId(String[] truckRouteId) {
        this.truckRouteId = truckRouteId;
    }

    public String[] getTruckDepDate() {
        return truckDepDate;
    }

    public void setTruckDepDate(String[] truckDepDate) {
        this.truckDepDate = truckDepDate;
    }


    public String getAwbOrigin() {
        return awbOrigin;
    }

    public void setAwbOrigin(String awbOrigin) {
        this.awbOrigin = awbOrigin;
    }

    public String getAwbDestination() {
        return awbDestination;
    }

    public void setAwbDestination(String awbDestination) {
        this.awbDestination = awbDestination;
    }

    public String getAwbOrderDeliveryDate() {
        return awbOrderDeliveryDate;
    }

    public void setAwbOrderDeliveryDate(String awbOrderDeliveryDate) {
        this.awbOrderDeliveryDate = awbOrderDeliveryDate;
    }

    public String getAwbDestinationRegion() {
        return awbDestinationRegion;
    }

    public void setAwbDestinationRegion(String awbDestinationRegion) {
        this.awbDestinationRegion = awbDestinationRegion;
    }

    public String getAwbMovementType() {
        return awbMovementType;
    }

    public void setAwbMovementType(String awbMovementType) {
        this.awbMovementType = awbMovementType;
    }

    public String getOrderReferenceAwb() {
        return orderReferenceAwb;
    }

    public void setOrderReferenceAwb(String orderReferenceAwb) {
        this.orderReferenceAwb = orderReferenceAwb;
    }

    public String getOrderReferenceAwbNo() {
        return orderReferenceAwbNo;
    }

    public void setOrderReferenceAwbNo(String orderReferenceAwbNo) {
        this.orderReferenceAwbNo = orderReferenceAwbNo;
    }

    public String[] getNoOfPieces() {
        return noOfPieces;
    }

    public void setNoOfPieces(String[] noOfPieces) {
        this.noOfPieces = noOfPieces;
    }

    public String[] getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(String[] grossWeight) {
        this.grossWeight = grossWeight;
    }

    public String[] getChargeableWeight() {
        return chargeableWeight;
    }

    public void setChargeableWeight(String[] chargeableWeight) {
        this.chargeableWeight = chargeableWeight;
    }

    public String[] getChargeableWeightId() {
        return chargeableWeightId;
    }

    public void setChargeableWeightId(String[] chargeableWeightId) {
        this.chargeableWeightId = chargeableWeightId;
    }

    public String[] getLengths() {
        return lengths;
    }

    public void setLengths(String[] lengths) {
        this.lengths = lengths;
    }

    public String[] getWidths() {
        return widths;
    }

    public void setWidths(String[] widths) {
        this.widths = widths;
    }

    public String[] getHeights() {
        return heights;
    }

    public void setHeights(String[] heights) {
        this.heights = heights;
    }

    public String[] getDgHandlingCode() {
        return dgHandlingCode;
    }

    public void setDgHandlingCode(String[] dgHandlingCode) {
        this.dgHandlingCode = dgHandlingCode;
    }

    public String[] getUnIdNo() {
        return unIdNo;
    }

    public void setUnIdNo(String[] unIdNo) {
        this.unIdNo = unIdNo;
    }

    public String[] getClassCode() {
        return classCode;
    }

    public void setClassCode(String[] classCode) {
        this.classCode = classCode;
    }

    public String[] getPkgInstruction() {
        return pkgInstruction;
    }

    public void setPkgInstruction(String[] pkgInstruction) {
        this.pkgInstruction = pkgInstruction;
    }

    public String[] getPkgGroup() {
        return pkgGroup;
    }

    public void setPkgGroup(String[] pkgGroup) {
        this.pkgGroup = pkgGroup;
    }

    public String[] getNetQuantity() {
        return netQuantity;
    }

    public void setNetQuantity(String[] netQuantity) {
        this.netQuantity = netQuantity;
    }

    public String[] getNetUnits() {
        return netUnits;
    }

    public void setNetUnits(String[] netUnits) {
        this.netUnits = netUnits;
    }

    public String[] getTruckTravelKm() {
        return truckTravelKm;
    }

    public void setTruckTravelKm(String[] truckTravelKm) {
        this.truckTravelKm = truckTravelKm;
    }

    public String[] getTruckTravelHour() {
        return truckTravelHour;
    }

    public void setTruckTravelHour(String[] truckTravelHour) {
        this.truckTravelHour = truckTravelHour;
    }

    public String[] getTruckTravelMinute() {
        return truckTravelMinute;
    }

    public void setTruckTravelMinute(String[] truckTravelMinute) {
        this.truckTravelMinute = truckTravelMinute;
    }

    public String[] getFleetTypeId() {
        return fleetTypeId;
    }

    public void setFleetTypeId(String[] fleetTypeId) {
        this.fleetTypeId = fleetTypeId;
    }

    public String getFreightReceipt() {
        return freightReceipt;
    }

    public void setFreightReceipt(String freightReceipt) {
        this.freightReceipt = freightReceipt;
    }

    public String getWareHouseName() {
        return wareHouseName;
    }

    public void setWareHouseName(String wareHouseName) {
        this.wareHouseName = wareHouseName;
    }

    public String getWareHouseLocation() {
        return wareHouseLocation;
    }

    public void setWareHouseLocation(String wareHouseLocation) {
        this.wareHouseLocation = wareHouseLocation;
    }

    public String getShipmentAcceptanceDate() {
        return shipmentAcceptanceDate;
    }

    public void setShipmentAcceptanceDate(String shipmentAcceptanceDate) {
        this.shipmentAcceptanceDate = shipmentAcceptanceDate;
    }

    public String getShipmentAccpHour() {
        return shipmentAccpHour;
    }

    public void setShipmentAccpHour(String shipmentAccpHour) {
        this.shipmentAccpHour = shipmentAccpHour;
    }

    public String getShipmentAccpMinute() {
        return shipmentAccpMinute;
    }

    public void setShipmentAccpMinute(String shipmentAccpMinute) {
        this.shipmentAccpMinute = shipmentAccpMinute;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }

    public String getRateMode() {
        return rateMode;
    }

    public void setRateMode(String rateMode) {
        this.rateMode = rateMode;
    }

    public String[] getBookingReferenceRemarks() {
        return bookingReferenceRemarks;
    }

    public void setBookingReferenceRemarks(String[] bookingReferenceRemarks) {
        this.bookingReferenceRemarks = bookingReferenceRemarks;
    }

    public String getOrderDeliveryHour() {
        return orderDeliveryHour;
    }

    public void setOrderDeliveryHour(String orderDeliveryHour) {
        this.orderDeliveryHour = orderDeliveryHour;
    }

    public String getOrderDeliveryMinute() {
        return orderDeliveryMinute;
    }

    public void setOrderDeliveryMinute(String orderDeliveryMinute) {
        this.orderDeliveryMinute = orderDeliveryMinute;
    }

    public String getProductRate() {
        return productRate;
    }

    public void setProductRate(String productRate) {
        this.productRate = productRate;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getRateValue() {
        return rateValue;
    }

    public void setRateValue(String rateValue) {
        this.rateValue = rateValue;
    }

    public String[] getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String[] vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String[] getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String[] vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getTotalChargeableWeights() {
        return totalChargeableWeights;
    }

    public void setTotalChargeableWeights(String totalChargeableWeights) {
        this.totalChargeableWeights = totalChargeableWeights;
    }

    public String getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(String totalVolume) {
        this.totalVolume = totalVolume;
    }

    public String[] getVolumes() {
        return volumes;
    }

    public void setVolumes(String[] volumes) {
        this.volumes = volumes;
    }

    public String getAwbOriginId() {
        return awbOriginId;
    }

    public void setAwbOriginId(String awbOriginId) {
        this.awbOriginId = awbOriginId;
    }

    public String getAwbDestinationId() {
        return awbDestinationId;
    }

    public void setAwbDestinationId(String awbDestinationId) {
        this.awbDestinationId = awbDestinationId;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getContractFrom() {
        return contractFrom;
    }

    public void setContractFrom(String contractFrom) {
        this.contractFrom = contractFrom;
    }

    public String getContractTo() {
        return contractTo;
    }

    public void setContractTo(String contractTo) {
        this.contractTo = contractTo;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String[] getOriginIdFullTruck() {
        return originIdFullTruck;
    }

    public void setOriginIdFullTruck(String[] originIdFullTruck) {
        this.originIdFullTruck = originIdFullTruck;
    }

    public String[] getOriginNameFullTruck() {
        return originNameFullTruck;
    }

    public void setOriginNameFullTruck(String[] originNameFullTruck) {
        this.originNameFullTruck = originNameFullTruck;
    }

    public String[] getDestinationIdFullTruck() {
        return destinationIdFullTruck;
    }

    public void setDestinationIdFullTruck(String[] destinationIdFullTruck) {
        this.destinationIdFullTruck = destinationIdFullTruck;
    }

    public String[] getDestinationNameFullTruck() {
        return destinationNameFullTruck;
    }

    public void setDestinationNameFullTruck(String[] destinationNameFullTruck) {
        this.destinationNameFullTruck = destinationNameFullTruck;
    }

    public String[] getRouteIdFullTruck() {
        return routeIdFullTruck;
    }

    public void setRouteIdFullTruck(String[] routeIdFullTruck) {
        this.routeIdFullTruck = routeIdFullTruck;
    }

    public String[] getTravelKmFullTruck() {
        return travelKmFullTruck;
    }

    public void setTravelKmFullTruck(String[] travelKmFullTruck) {
        this.travelKmFullTruck = travelKmFullTruck;
    }

    public String[] getTravelHourFullTruck() {
        return travelHourFullTruck;
    }

    public void setTravelHourFullTruck(String[] travelHourFullTruck) {
        this.travelHourFullTruck = travelHourFullTruck;
    }

    public String[] getTravelMinuteFullTruck() {
        return travelMinuteFullTruck;
    }

    public void setTravelMinuteFullTruck(String[] travelMinuteFullTruck) {
        this.travelMinuteFullTruck = travelMinuteFullTruck;
    }

    public String[] getMovementTypeIdFullTruck() {
        return movementTypeIdFullTruck;
    }

    public void setMovementTypeIdFullTruck(String[] movementTypeIdFullTruck) {
        this.movementTypeIdFullTruck = movementTypeIdFullTruck;
    }

    public String[] getVehicleTypeIdFullTruck() {
        return vehicleTypeIdFullTruck;
    }

    public void setVehicleTypeIdFullTruck(String[] vehicleTypeIdFullTruck) {
        this.vehicleTypeIdFullTruck = vehicleTypeIdFullTruck;
    }

    public String[] getOriginIdWeightBreak() {
        return originIdWeightBreak;
    }

    public void setOriginIdWeightBreak(String[] originIdWeightBreak) {
        this.originIdWeightBreak = originIdWeightBreak;
    }

    public String[] getOriginNameWeightBreak() {
        return originNameWeightBreak;
    }

    public void setOriginNameWeightBreak(String[] originNameWeightBreak) {
        this.originNameWeightBreak = originNameWeightBreak;
    }

    public String[] getDestinationIdWeightBreak() {
        return destinationIdWeightBreak;
    }

    public void setDestinationIdWeightBreak(String[] destinationIdWeightBreak) {
        this.destinationIdWeightBreak = destinationIdWeightBreak;
    }

    public String[] getDestinationNameWeightBreak() {
        return destinationNameWeightBreak;
    }

    public void setDestinationNameWeightBreak(String[] destinationNameWeightBreak) {
        this.destinationNameWeightBreak = destinationNameWeightBreak;
    }

    public String[] getRouteIdWeightBreak() {
        return routeIdWeightBreak;
    }

    public void setRouteIdWeightBreak(String[] routeIdWeightBreak) {
        this.routeIdWeightBreak = routeIdWeightBreak;
    }

    public String[] getTravelKmWeightBreak() {
        return travelKmWeightBreak;
    }

    public void setTravelKmWeightBreak(String[] travelKmWeightBreak) {
        this.travelKmWeightBreak = travelKmWeightBreak;
    }

    public String[] getTravelHourWeightBreak() {
        return travelHourWeightBreak;
    }

    public void setTravelHourWeightBreak(String[] travelHourWeightBreak) {
        this.travelHourWeightBreak = travelHourWeightBreak;
    }

    public String[] getTravelMinuteWeightBreak() {
        return travelMinuteWeightBreak;
    }

    public void setTravelMinuteWeightBreak(String[] travelMinuteWeightBreak) {
        this.travelMinuteWeightBreak = travelMinuteWeightBreak;
    }

    public String[] getMovementTypeIdWeightBreak() {
        return movementTypeIdWeightBreak;
    }

    public void setMovementTypeIdWeightBreak(String[] movementTypeIdWeightBreak) {
        this.movementTypeIdWeightBreak = movementTypeIdWeightBreak;
    }

    public String[] getFromKgWeightBreak() {
        return fromKgWeightBreak;
    }

    public void setFromKgWeightBreak(String[] fromKgWeightBreak) {
        this.fromKgWeightBreak = fromKgWeightBreak;
    }

    public String[] getToKgWeightBreak() {
        return toKgWeightBreak;
    }

    public void setToKgWeightBreak(String[] toKgWeightBreak) {
        this.toKgWeightBreak = toKgWeightBreak;
    }

    public String[] getRateWithReeferFullTruck() {
        return rateWithReeferFullTruck;
    }

    public void setRateWithReeferFullTruck(String[] rateWithReeferFullTruck) {
        this.rateWithReeferFullTruck = rateWithReeferFullTruck;
    }

    public String[] getRateWithOutReeferFullTruck() {
        return rateWithOutReeferFullTruck;
    }

    public void setRateWithOutReeferFullTruck(String[] rateWithOutReeferFullTruck) {
        this.rateWithOutReeferFullTruck = rateWithOutReeferFullTruck;
    }

    public String[] getRateWithReeferWeightBreak() {
        return rateWithReeferWeightBreak;
    }

    public void setRateWithReeferWeightBreak(String[] rateWithReeferWeightBreak) {
        this.rateWithReeferWeightBreak = rateWithReeferWeightBreak;
    }

    public String[] getRateWithOutReeferWeightBreak() {
        return rateWithOutReeferWeightBreak;
    }

    public void setRateWithOutReeferWeightBreak(String[] rateWithOutReeferWeightBreak) {
        this.rateWithOutReeferWeightBreak = rateWithOutReeferWeightBreak;
    }

    public String[] getConsignmentArticleId() {
        return consignmentArticleId;
    }

    public void setConsignmentArticleId(String[] consignmentArticleId) {
        this.consignmentArticleId = consignmentArticleId;
    }

    public String[] getConsignmentRouteCourseId() {
        return consignmentRouteCourseId;
    }

    public void setConsignmentRouteCourseId(String[] consignmentRouteCourseId) {
        this.consignmentRouteCourseId = consignmentRouteCourseId;
    }

    public String getConsignmentOrderId() {
        return consignmentOrderId;
    }

    public void setConsignmentOrderId(String consignmentOrderId) {
        this.consignmentOrderId = consignmentOrderId;
    }

    public String[] getConsignmentSbNo() {
        return consignmentSbNo;
    }

    public void setConsignmentSbNo(String[] consignmentSbNo) {
        this.consignmentSbNo = consignmentSbNo;
    }

    public String getCustomsRemark() {
        return customsRemark;
    }

    public void setCustomsRemark(String customsRemark) {
        this.customsRemark = customsRemark;
    }

    public String getFleetDetails() {
        return fleetDetails;
    }

    public void setFleetDetails(String fleetDetails) {
        this.fleetDetails = fleetDetails;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String[] getConsignmentOrderNos() {
        return consignmentOrderNos;
    }

    public void setConsignmentOrderNos(String[] consignmentOrderNos) {
        this.consignmentOrderNos = consignmentOrderNos;
    }

    public String getConsignmentEgmNo() {
        return consignmentEgmNo;
    }

    public void setConsignmentEgmNo(String consignmentEgmNo) {
        this.consignmentEgmNo = consignmentEgmNo;
    }

    public String getConsignmentTpNo() {
        return consignmentTpNo;
    }

    public void setConsignmentTpNo(String consignmentTpNo) {
        this.consignmentTpNo = consignmentTpNo;
    }

    public String getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(String flightDate) {
        this.flightDate = flightDate;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getLodingPoint() {
        return lodingPoint;
    }

    public void setLodingPoint(String lodingPoint) {
        this.lodingPoint = lodingPoint;
    }

    public String getManifestDate() {
        return manifestDate;
    }

    public void setManifestDate(String manifestDate) {
        this.manifestDate = manifestDate;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getUnLodingPoint() {
        return unLodingPoint;
    }

    public void setUnLodingPoint(String unLodingPoint) {
        this.unLodingPoint = unLodingPoint;
    }

    public String[] getVehicleRegNo() {
        return vehicleRegNo;
    }

    public void setVehicleRegNo(String[] vehicleRegNo) {
        this.vehicleRegNo = vehicleRegNo;
    }

    public String[] getTotalTsaGoods() {
        return totalTsaGoods;
    }

    public void setTotalTsaGoods(String[] totalTsaGoods) {
        this.totalTsaGoods = totalTsaGoods;
    }

    public String getConsignmentNoteAtdNo() {
        return consignmentNoteAtdNo;
    }

    public void setConsignmentNoteAtdNo(String consignmentNoteAtdNo) {
        this.consignmentNoteAtdNo = consignmentNoteAtdNo;
    }

    public String getConsignmentNoteEmno() {
        return consignmentNoteEmno;
    }

    public void setConsignmentNoteEmno(String consignmentNoteEmno) {
        this.consignmentNoteEmno = consignmentNoteEmno;
    }

    public String[] getUsedCapacity() {
        return usedCapacity;
    }

    public void setUsedCapacity(String[] usedCapacity) {
        this.usedCapacity = usedCapacity;
    }

    public String[] getUsedVol() {
        return usedVol;
    }

    public void setUsedVol(String[] usedVol) {
        this.usedVol = usedVol;
    }

    public String getRateMode1() {
        return rateMode1;
    }

    public void setRateMode1(String rateMode1) {
        this.rateMode1 = rateMode1;
    }

    public String getPerKgRate() {
        return perKgRate;
    }

    public void setPerKgRate(String perKgRate) {
        this.perKgRate = perKgRate;
    }

    public String getRateValue1() {
        return rateValue1;
    }

    public void setRateValue1(String rateValue1) {
        this.rateValue1 = rateValue1;
    }

    public String getAwbOriginRegion() {
        return awbOriginRegion;
    }

    public void setAwbOriginRegion(String awbOriginRegion) {
        this.awbOriginRegion = awbOriginRegion;
    }

    public String getOrderReferenceEnd() {
        return orderReferenceEnd;
    }

    public void setOrderReferenceEnd(String orderReferenceEnd) {
        this.orderReferenceEnd = orderReferenceEnd;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public String getConsignmentEfoNo() {
        return consignmentEfoNo;
    }

    public void setConsignmentEfoNo(String consignmentEfoNo) {
        this.consignmentEfoNo = consignmentEfoNo;
    }

    public String getPayBill() {
        return payBill;
    }

    public void setPayBill(String payBill) {
        this.payBill = payBill;
    }

    public String getTspPerkg() {
        return tspPerkg;
    }

    public void setTspPerkg(String tspPerkg) {
        this.tspPerkg = tspPerkg;
    }

    public String getTsrPerkg() {
        return tsrPerkg;
    }

    public void setTsrPerkg(String tsrPerkg) {
        this.tsrPerkg = tsrPerkg;
    }

    public String getCustomerNetAmount() {
        return customerNetAmount;
    }

    public void setCustomerNetAmount(String customerNetAmount) {
        this.customerNetAmount = customerNetAmount;
    }

    public String getTranshipmentNo() {
        return transhipmentNo;
    }

    public void setTranshipmentNo(String transhipmentNo) {
        this.transhipmentNo = transhipmentNo;
    }

    public String getAwbTotalPackages() {
        return awbTotalPackages;
    }

    public void setAwbTotalPackages(String awbTotalPackages) {
        this.awbTotalPackages = awbTotalPackages;
    }

    public String getAwbReceivedPackages() {
        return awbReceivedPackages;
    }

    public void setAwbReceivedPackages(String awbReceivedPackages) {
        this.awbReceivedPackages = awbReceivedPackages;
    }

    public String getAwbPendingPackages() {
        return awbPendingPackages;
    }

    public void setAwbPendingPackages(String awbPendingPackages) {
        this.awbPendingPackages = awbPendingPackages;
    }

    public String getAwbTotalGrossWeight() {
        return awbTotalGrossWeight;
    }

    public void setAwbTotalGrossWeight(String awbTotalGrossWeight) {
        this.awbTotalGrossWeight = awbTotalGrossWeight;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getTotalVolumeActual() {
        return totalVolumeActual;
    }

    public void setTotalVolumeActual(String totalVolumeActual) {
        this.totalVolumeActual = totalVolumeActual;
    }

    public String getAllowedTrucks() {
        return allowedTrucks;
    }

    public void setAllowedTrucks(String allowedTrucks) {
        this.allowedTrucks = allowedTrucks;
    }

    public String getNoOfTrucks() {
        return noOfTrucks;
    }

    public void setNoOfTrucks(String noOfTrucks) {
        this.noOfTrucks = noOfTrucks;
    }

    public String getAwbType() {
        return awbType;
    }

    public void setAwbType(String awbType) {
        this.awbType = awbType;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }
   
    
    
    
}
