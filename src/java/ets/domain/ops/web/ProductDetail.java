package ets.domain.ops.web;

public class ProductDetail {

    String CUSTOMERDOC = null;
    String TURCKNO = null;
    String LOADSTATUS = null;
    String LOADTIME = null;
    String UNLOADSTATUS = null;
    String UNLOADTIME = null;
    String PODRECEIVED = null;
    String AWB = null;
    String awbNumber = null;
    String parcel = null;
    String origin = null;
    String destination = null;
    String barcode = null;
    String loadStatus = null;
    String loadedTime = null;
    String truckNo = null;
    String customerDocRef = null;
    String unloadStatus = null;
    String unloadTime = null;
    String podRecieved = null;

    String productNumber = null;
    String productName = null;
    String price = null;
    String quantity = null;

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAwbNumber() {
        return awbNumber;
    }

    public void setAwbNumber(String awbNumber) {
        this.awbNumber = awbNumber;
    }

    public String getParcel() {
        return parcel;
    }

    public void setParcel(String parcel) {
        this.parcel = parcel;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getLoadStatus() {
        return loadStatus;
    }

    public void setLoadStatus(String loadStatus) {
        this.loadStatus = loadStatus;
    }

    public String getLoadedTime() {
        return loadedTime;
    }

    public void setLoadedTime(String loadedTime) {
        this.loadedTime = loadedTime;
    }

    public String getTruckNo() {
        return truckNo;
    }

    public void setTruckNo(String truckNo) {
        this.truckNo = truckNo;
    }

    public String getCustomerDocRef() {
        return customerDocRef;
    }

    public void setCustomerDocRef(String customerDocRef) {
        this.customerDocRef = customerDocRef;
    }

    public String getUnloadStatus() {
        return unloadStatus;
    }

    public void setUnloadStatus(String unloadStatus) {
        this.unloadStatus = unloadStatus;
    }

    public String getUnloadTime() {
        return unloadTime;
    }

    public void setUnloadTime(String unloadTime) {
        this.unloadTime = unloadTime;
    }

    public String getPodRecieved() {
        return podRecieved;
    }

    public void setPodRecieved(String podRecieved) {
        this.podRecieved = podRecieved;
    }

    public String getAWB() {
        return AWB;
    }

    public void setAWB(String AWB) {
        this.AWB = AWB;
    }

    public String getLOADSTATUS() {
        return LOADSTATUS;
    }

    public void setLOADSTATUS(String LOADSTATUS) {
        this.LOADSTATUS = LOADSTATUS;
    }

    public String getLOADTIME() {
        return LOADTIME;
    }

    public void setLOADTIME(String LOADTIME) {
        this.LOADTIME = LOADTIME;
    }

    public String getTURCKNO() {
        return TURCKNO;
    }

    public void setTURCKNO(String TURCKNO) {
        this.TURCKNO = TURCKNO;
    }

    public String getCUSTOMERDOC() {
        return CUSTOMERDOC;
    }

    public void setCUSTOMERDOC(String CUSTOMERDOC) {
        this.CUSTOMERDOC = CUSTOMERDOC;
    }

    public String getUNLOADSTATUS() {
        return UNLOADSTATUS;
    }

    public void setUNLOADSTATUS(String UNLOADSTATUS) {
        this.UNLOADSTATUS = UNLOADSTATUS;
    }

    public String getUNLOADTIME() {
        return UNLOADTIME;
    }

    public void setUNLOADTIME(String UNLOADTIME) {
        this.UNLOADTIME = UNLOADTIME;
    }

    public String getPODRECEIVED() {
        return PODRECEIVED;
    }

    public void setPODRECEIVED(String PODRECEIVED) {
        this.PODRECEIVED = PODRECEIVED;
    }
    

}
