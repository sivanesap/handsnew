// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PurchaseCommand.java

package ets.domain.purchase.web;


public class PurchaseCommand
{

    //    CLPL Vat by ashok start
        private String effectiveDate = "" ;

    public PurchaseCommand()
    {
        purchaseTypeId = "";
        vendorId = "";
        desc = "";
        userId = "";
        mprId = "";
        itemId = "";
        reqQty = "";
        approvedQty = "";
        status = "";
        fromDate = "";
        toDate = "";
        reqDate = "";
        toSpId = "";
        supplyId = "";
        approvedQtys = null;
        itemIds = null;
        reqQtys = null;
        vendorIds = null;
        selectedIndex = null;
        inVoiceAmount = "";
        mrps = null;
        itemMrp = null;
        discounts = null;
        receivedQtys = null;
        acceptedQtys = null;
        itemAmounts = null;
        dcNumber = "";
        inVoiceNumber = "";
        inVoiceDate = "";
        inventoryStatus = "";
        mfrCode = "";
        paplCode = "";
        categoryId = "";
        searchAll = "";
        returnedQtys = null;
        retQtys = null;
        unitPrice = null;
        tax = null;
        discount = null;
        remarks = "";
        transactionType = "";
        poId = "";
        freight = "";
        vatId = "";
        vat = "";
        vendorId1 = "";
        grnIds = null;
        orderTypes = null;
        payDates = null;
        invoiceAmounts = null;
        paidAmounts = null;
        remarkss = null;
        selectedInd = null;
        paymentIds = null;
        reqFor=null;
//        bala
        totalQtys = null;
        unusedQtys = null;
//        bala ends
    }

    public String[] getUnusedQtys() {
        return unusedQtys;
    }

    public void setUnusedQtys(String[] unusedQtys) {
        this.unusedQtys = unusedQtys;
    }

    public String[] getTotalQtys() {
        return totalQtys;
    }

    public void setTotalQtys(String[] totalQtys) {
        this.totalQtys = totalQtys;
    }

    public String getReqFor() {
        return reqFor;
    }

    public void setReqFor(String reqFor) {
        this.reqFor = reqFor;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public String getApprovedQty()
    {
        return approvedQty;
    }

    public void setApprovedQty(String approvedQty)
    {
        this.approvedQty = approvedQty;
    }

    public String[] getApprovedQtys()
    {
        return approvedQtys;
    }

    public void setApprovedQtys(String approvedQtys[])
    {
        this.approvedQtys = approvedQtys;
    }

    public String getDesc()
    {
        return desc;
    }

    public void setDesc(String desc)
    {
        this.desc = desc;
    }

    public String getItemId()
    {
        return itemId;
    }

    public void setItemId(String itemId)
    {
        this.itemId = itemId;
    }

    public String[] getItemIds()
    {
        return itemIds;
    }

    public void setItemIds(String itemIds[])
    {
        this.itemIds = itemIds;
    }

    public String getMprId()
    {
        return mprId;
    }

    public void setMprId(String mprId)
    {
        this.mprId = mprId;
    }

    public String getPurchaseTypeId()
    {
        return purchaseTypeId;
    }

    public void setPurchaseTypeId(String purchaseTypeId)
    {
        this.purchaseTypeId = purchaseTypeId;
    }

    public String getReqQty()
    {
        return reqQty;
    }

    public void setReqQty(String reqQty)
    {
        this.reqQty = reqQty;
    }

    public String[] getReqQtys()
    {
        return reqQtys;
    }

    public void setReqQtys(String reqQtys[])
    {
        this.reqQtys = reqQtys;
    }

    public String[] getSelectedIndex()
    {
        return selectedIndex;
    }

    public void setSelectedIndex(String selectedIndex[])
    {
        this.selectedIndex = selectedIndex;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getVendorId()
    {
        return vendorId;
    }

    public void setVendorId(String vendorId)
    {
        this.vendorId = vendorId;
    }

    public String[] getVendorIds()
    {
        return vendorIds;
    }

    public void setVendorIds(String vendorIds[])
    {
        this.vendorIds = vendorIds;
    }

    public String getFromDate()
    {
        return fromDate;
    }

    public void setFromDate(String fromDate)
    {
        this.fromDate = fromDate;
    }

    public String getToDate()
    {
        return toDate;
    }

    public void setToDate(String toDate)
    {
        this.toDate = toDate;
    }

    public String getReqDate()
    {
        return reqDate;
    }

    public void setReqDate(String reqDate)
    {
        this.reqDate = reqDate;
    }

    public String getToSpId()
    {
        return toSpId;
    }

    public void setToSpId(String toSpId)
    {
        this.toSpId = toSpId;
    }

    public String getDcNumber()
    {
        return dcNumber;
    }

    public void setDcNumber(String dcNumber)
    {
        this.dcNumber = dcNumber;
    }

    public String getInVoiceNumber()
    {
        return inVoiceNumber;
    }

    public void setInVoiceNumber(String inVoiceNumber)
    {
        this.inVoiceNumber = inVoiceNumber;
    }

    public String getInventoryStatus()
    {
        return inventoryStatus;
    }

    public void setInventoryStatus(String inventoryStatus)
    {
        this.inventoryStatus = inventoryStatus;
    }

    public String getSupplyId()
    {
        return supplyId;
    }

    public void setSupplyId(String supplyId)
    {
        this.supplyId = supplyId;
    }

    public String[] getAcceptedQtys()
    {
        return acceptedQtys;
    }

    public void setAcceptedQtys(String acceptedQtys[])
    {
        this.acceptedQtys = acceptedQtys;
    }

    public String[] getItemAmounts()
    {
        return itemAmounts;
    }

    public void setItemAmounts(String itemAmounts[])
    {
        this.itemAmounts = itemAmounts;
    }

    public String[] getMrps()
    {
        return mrps;
    }

    public void setMrps(String mrps[])
    {
        this.mrps = mrps;
    }

    public String[] getReceivedQtys()
    {
        return receivedQtys;
    }

    public void setReceivedQtys(String receivedQtys[])
    {
        this.receivedQtys = receivedQtys;
    }

    public String getInVoiceAmount()
    {
        return inVoiceAmount;
    }

    public void setInVoiceAmount(String inVoiceAmount)
    {
        this.inVoiceAmount = inVoiceAmount;
    }

    public String getCategoryId()
    {
        return categoryId;
    }

    public void setCategoryId(String categoryId)
    {
        this.categoryId = categoryId;
    }

    public String getMfrCode()
    {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode)
    {
        this.mfrCode = mfrCode;
    }

    public String getPaplCode()
    {
        return paplCode;
    }

    public void setPaplCode(String paplCode)
    {
        this.paplCode = paplCode;
    }

    public String getSearchAll()
    {
        return searchAll;
    }

    public void setSearchAll(String searchAll)
    {
        this.searchAll = searchAll;
    }

    public String[] getReturnedQtys()
    {
        return returnedQtys;
    }

    public void setReturnedQtys(String returnedQtys[])
    {
        this.returnedQtys = returnedQtys;
    }

    public String[] getTax()
    {
        return tax;
    }

    public void setTax(String tax[])
    {
        this.tax = tax;
    }

    public String[] getUnitPrice()
    {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice[])
    {
        this.unitPrice = unitPrice;
    }

    public String[] getDiscount()
    {
        return discount;
    }

    public void setDiscount(String discount[])
    {
        this.discount = discount;
    }

    public String getTransactionType()
    {
        return transactionType;
    }

    public void setTransactionType(String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String[] getDiscounts()
    {
        return discounts;
    }

    public void setDiscounts(String discounts[])
    {
        this.discounts = discounts;
    }

    public String[] getRetQtys()
    {
        return retQtys;
    }

    public void setRetQtys(String retQtys[])
    {
        this.retQtys = retQtys;
    }

    public String getInVoiceDate()
    {
        return inVoiceDate;
    }

    public void setInVoiceDate(String inVoiceDate)
    {
        this.inVoiceDate = inVoiceDate;
    }

    public String getPoId()
    {
        return poId;
    }

    public void setPoId(String poId)
    {
        this.poId = poId;
    }

    public String getFreight()
    {
        return freight;
    }

    public void setFreight(String freight)
    {
        this.freight = freight;
    }

    public String getVat()
    {
        return vat;
    }

    public void setVat(String vat)
    {
        this.vat = vat;
    }

    public String getVatId()
    {
        return vatId;
    }

    public void setVatId(String vatId)
    {
        this.vatId = vatId;
    }

    public String getVendorId1()
    {
        return vendorId1;
    }

    public void setVendorId1(String vendorId1)
    {
        this.vendorId1 = vendorId1;
    }

    public String[] getGrnIds()
    {
        return grnIds;
    }

    public void setGrnIds(String grnIds[])
    {
        this.grnIds = grnIds;
    }

    public String[] getInvoiceAmounts()
    {
        return invoiceAmounts;
    }

    public void setInvoiceAmounts(String invoiceAmounts[])
    {
        this.invoiceAmounts = invoiceAmounts;
    }

    public String[] getOrderTypes()
    {
        return orderTypes;
    }

    public void setOrderTypes(String orderTypes[])
    {
        this.orderTypes = orderTypes;
    }

    public String[] getPaidAmounts()
    {
        return paidAmounts;
    }

    public void setPaidAmounts(String paidAmounts[])
    {
        this.paidAmounts = paidAmounts;
    }

    public String[] getPayDates()
    {
        return payDates;
    }

    public void setPayDates(String payDates[])
    {
        this.payDates = payDates;
    }

    public String[] getRemarkss()
    {
        return remarkss;
    }

    public void setRemarkss(String remarkss[])
    {
        this.remarkss = remarkss;
    }

    public String[] getSelectedInd()
    {
        return selectedInd;
    }

    public void setSelectedInd(String selectedInd[])
    {
        this.selectedInd = selectedInd;
    }

    public String[] getPaymentIds()
    {
        return paymentIds;
    }

    public void setPaymentIds(String paymentIds[])
    {
        this.paymentIds = paymentIds;
    }

    public String[] getItemMrp() {
        return itemMrp;
    }

    public void setItemMrp(String[] itemMrp) {
        this.itemMrp = itemMrp;
    }

    

    private String purchaseTypeId;
    private String vendorId;
    private String desc;
    private String userId;
    private String mprId;
    private String itemId;
    private String reqQty;
    private String approvedQty;
    private String status;
    private String fromDate;
    private String toDate;
    private String reqDate;
    private String toSpId;
    private String supplyId;
    private String approvedQtys[];
    private String itemIds[];
    private String reqQtys[];
    private String vendorIds[];
    private String selectedIndex[];
    private String inVoiceAmount;
    private String mrps[];
    private String discounts[];
    private String receivedQtys[];
    private String acceptedQtys[];
    private String itemAmounts[];
    private String dcNumber;
    private String inVoiceNumber;
    private String inVoiceDate;
    private String inventoryStatus;
    private String mfrCode;
    private String paplCode;
    private String categoryId;
    private String searchAll;
    private String returnedQtys[];
    private String retQtys[];
    private String unitPrice[];
    private String tax[];
    private String discount[];
    private String remarks;
    private String transactionType;
    private String poId;
    private String freight;
    private String vatId;
    private String vat;
    private String vendorId1;
    private String grnIds[];
    private String orderTypes[];
    private String payDates[];
    private String invoiceAmounts[];
    private String paidAmounts[];
    private String remarkss[];
    private String selectedInd[];
    private String paymentIds[];
    private String reqFor;
//    bala
    private String totalQtys[];
    private String unusedQtys[];
    private String itemMrp[];

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }


}
