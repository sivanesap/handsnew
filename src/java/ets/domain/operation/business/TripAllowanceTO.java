/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.operation.business;

/**
 *
 * @author Administrator
 */
public class TripAllowanceTO {

    private String tripCode = "";
    private String tripId = "";
    private String returnTripId = "";
    private String tripStageId = "";
    private String tripAllowanceDate = "";
    private String tripAllowanceAmount = "";
    private String tripAllowancePaidById = "";
    private String tripAllowancePaidBy = "";
    private String tripAllowanceRemarks = "";
    private String locationName = "";

    public String getTripAllowanceAmount() {
        return tripAllowanceAmount;
    }

    public void setTripAllowanceAmount(String tripAllowanceAmount) {
        this.tripAllowanceAmount = tripAllowanceAmount;
    }

    public String getTripAllowanceDate() {
        return tripAllowanceDate;
    }

    public void setTripAllowanceDate(String tripAllowanceDate) {
        this.tripAllowanceDate = tripAllowanceDate;
    }

    public String getTripAllowancePaidBy() {
        return tripAllowancePaidBy;
    }

    public void setTripAllowancePaidBy(String tripAllowancePaidBy) {
        this.tripAllowancePaidBy = tripAllowancePaidBy;
    }

    public String getTripAllowanceRemarks() {
        return tripAllowanceRemarks;
    }

    public void setTripAllowanceRemarks(String tripAllowanceRemarks) {
        this.tripAllowanceRemarks = tripAllowanceRemarks;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getTripStageId() {
        return tripStageId;
    }

    public void setTripStageId(String tripStageId) {
        this.tripStageId = tripStageId;
    }

    public String getTripAllowancePaidById() {
        return tripAllowancePaidById;
    }

    public void setTripAllowancePaidById(String tripAllowancePaidById) {
        this.tripAllowancePaidById = tripAllowancePaidById;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getReturnTripId() {
        return returnTripId;
    }

    public void setReturnTripId(String returnTripId) {
        this.returnTripId = returnTripId;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }
    
    
}
