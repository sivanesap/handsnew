package ets.domain.racks.business;

import ets.domain.racks.data.RackDAO;
import ets.domain.racks.business.RackTO;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import java.util.ArrayList;
import java.util.Iterator;

public class RackBP {

    private RackDAO rackDAO;

    public RackDAO getRackDAO() {
        return rackDAO;
    }

    public void setRackDAO(RackDAO rackDAO) {
        this.rackDAO = rackDAO;
    }

    public ArrayList processRackList() throws FPBusinessException, FPRuntimeException {
        ArrayList rackList = new ArrayList();
        rackList = rackDAO.getRackList();
        if (rackList.size() == 0) {
            throw new FPBusinessException("EM-RACK-01");
        }
        return rackList;
    }

    public int processInsertRack(RackTO rackTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = rackDAO.doInsertRack(rackTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-RACK-02");
        }
        return status;
    }

    public int processUpdateRack(ArrayList List, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = rackDAO.doUpdateRack(List, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-RACK-03");
        }
        return status;
    }

    public ArrayList processSubRackList() throws FPBusinessException, FPRuntimeException {
        ArrayList rackList = new ArrayList();
        rackList = rackDAO.getSubRackList();
        if (rackList.size() == 0) {
            throw new FPBusinessException("EM-SRACK-01");
        }
        return rackList;
    }

    public int processInsertSubRack(RackTO rackTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = rackDAO.doInsertSubRack(rackTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-SRACK-02");
        }
        return status;
    }
    public int processUpdateSubRack(ArrayList List,int userId) throws FPBusinessException, FPRuntimeException {
        int status=0;
        status = rackDAO.doUpdateSubRack(List, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-SRACK-03");
        }
        return status;
    }
    
    //processGetSectionList
    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetSectionList() throws FPRuntimeException, FPBusinessException {
        
        ArrayList MfrList = new ArrayList();
         MfrList = rackDAO.getSectionList();
         if (MfrList.size() == 0) {
            throw new FPBusinessException("EM-MFR-01");
        }
        return  MfrList;
    }
    
    /**
     * This method used to Insert Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processInsertSectionDetails(RackTO rackTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = rackDAO.doSectionDetails(rackTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-MFR-02");
        }
        return insertStatus;
    }
    
     /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processModifySectionDetails(ArrayList List, int UserId) throws FPRuntimeException, FPBusinessException  {
        int insertStatus = 0;
        insertStatus = rackDAO.doSectionDetailsModify(List, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-MFR-02");
        }
        return insertStatus;
    }
    
     /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetUomList() throws FPRuntimeException, FPBusinessException {
        
        ArrayList MfrList = new ArrayList();
         MfrList = rackDAO.getUomList();
         if (MfrList.size() == 0) {
            throw new FPBusinessException("EM-MFR-01");
        }
        return  MfrList;
    }
   
    
    //parts starts here ...
    //processInsertPartsDetails
    
     /**
     * This method used to Insert Parts Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processInsertPartsDetails(RackTO rackTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = rackDAO.doPartsDetails(rackTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-PRT-02");
        }
        return insertStatus;
    }
    
    
    //getAjaxModelList
    
     /**
     * This method used to Get parts  throw ajax Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String processGetModels(int mfrId) throws FPRuntimeException, FPBusinessException {
        ArrayList models = new ArrayList();
        int counter = 0;
        RackTO rack = null;
        models = rackDAO.getAjaxModelList(mfrId);
       
        
        Iterator itr;
        String model = "";
        if (models.size() == 0) {
            model = "";
        }else{
            itr = models.iterator();
            while(itr.hasNext()){
                rack = new RackTO();
                rack = (RackTO) itr.next();
                if(counter == 0){
                    model = rack.getModelId() + "-" + rack.getModelName();
                    counter++;
                }else{
                    model = model + "~" + rack.getModelId() + "-" + rack.getModelName();                    
                }
            }
        }
        return model;
    }
    
   // processGetSubRack
     /**
     * This Ajax  method used to Get  sub Rack List  Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String processGetSubRack(int rackId) throws FPRuntimeException, FPBusinessException {
        ArrayList models = new ArrayList();
        int counter = 0;
        RackTO rack = null;
        models = rackDAO.getAjaxSubRackList(rackId);
       
        
        Iterator itr;
        String model = "";
        if (models.size() == 0) {
            model = "";
        }else{
            itr = models.iterator();
            while(itr.hasNext()){
                rack = new RackTO();
                rack = (RackTO) itr.next();
                if(counter == 0){
                    model = rack.getSubRackId() + "-" + rack.getSubRackName();
                    counter++;
                }else{
                    model = model + "~" + rack.getSubRackId() + "-" + rack.getSubRackName();                    
                }
            }
        }
        return model;
    }
    
     /**
     * This method used to get Parts Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
     public ArrayList getpartsDetails(RackTO rackTO) throws FPRuntimeException, FPBusinessException {
        ArrayList List = new ArrayList();
        List = rackDAO.getPartsDetails(rackTO);
        if (List.size() == 0) {
            throw new FPBusinessException("EM-PRT-01");
        }
        return List;
           }
     
      /**
     * This method used to Insert Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
     public ArrayList processGetPartsDetail(int itemId) throws FPRuntimeException, FPBusinessException {
        ArrayList partsDetail = new ArrayList();
        partsDetail = (ArrayList) rackDAO.getAlterPartsDetail(itemId);
        if (partsDetail.size() == 0) {
            throw new FPBusinessException("EM-PRT-01");
        }
        return partsDetail;
    }
 
      /**
     * This method used to Update Parts  Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processUpdatePartsDetails(RackTO rackTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = rackDAO.doPartsUpdateDetails(rackTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-PRT-02");
        }
        return insertStatus;
    }
 }

