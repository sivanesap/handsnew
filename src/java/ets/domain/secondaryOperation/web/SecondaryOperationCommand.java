/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.secondaryOperation.web;

/**
 *
 * @author Throttle
 */
public class SecondaryOperationCommand {

    private String primaryDriver = "";
    private String custId = "";
    private String billYear = "";
    private String billMonth = "";
    private String extraKmCalculation = "";
    private String contractCngCost = "";
    private String contractDieselCost = "";
    private String rateChangeOfCng = "";
    private String rateChangeOfDiesel = "";
    private String averageKM = "";
    private String preCoolingHr = "";
    private String loadingHr = "";
    private String humanId = "";
    private String contractId = "";
    private String id = "";
    private String primaryDriverId = "";
    private String primaryDriverName = "";
    private String fromDate = "";
    private String toDate = "";
    private String noOfVehicles = "";
    private String vehicleTypeName = "";
    private String fixedKm = "";
    private String fixedKmCharge = "";
    private String extraKmCharge = "";
    private String vehicleTypeId = "";
    private String latitudePosition = "";
    private String longitudePosition = "";
    private String status = "";
    private String pointName = "";
//    private String customerId = "";
    private String pointType = "";
    private String pointAddress = "";
    private String cityId = "";
    private String pointId = "";
    private String totalKm = "";
    private String routeId = "";
    private String routeContractId = "";
    private String destination = "";
    private String customerTypeId = "0";
    private String entryType = "";
    private String consignmentNoteNo = "";
    private String consignmentDate = "";
    private String orderReferenceNo = "";
    private String orderReferenceRemarks = "";
    private String productCategoryId = "0";
    private String customerAddress = "";
    private String pincode = "";
    private String customerMobileNo = "";
    private String mailId = "";
    private String customerPhoneNo = "";
    private String origin = "";
    private String businessType = "0";
    private String multiPickup = "";
    private String multiDelivery = "";
    private String consignmentOrderInstruction = "";
    private String serviceType = "0";
    private String reeferRequired = "";
    private String contractRateId = "0";
    private String vehicleRequiredDate = "";
    private String vehicleRequiredHour = "";
    private String vehicleRequiredMinute = "";
    private String vehicleInstruction = "";
    private String consignorName = "";
    private String consignorPhoneNo = "";
    private String consignorAddress = "";
    private String consigneeName = "";
    private String consigneePhoneNo = "";
    private String consigneeAddress = "";
    private String rateWithReefer = "";
    private String rateWithoutReefer = "";
    private String totalPackage = "";
    private String totalWeightage = "";
    private String totalHours = "";
    private String totalMinutes = "";
    private String totFreightAmount = "";
    private String billingTypeId = "";
    private String scheduleDate = "";
    private String customerCode = "";
    private String dateName = "";
    private String customerName = "";
    private String nextWeek = "";
    private String[] secRouteId = null;
    private String[] scheduledate = null;
    private String secondaryRouteId = "";
    private String secondaryRouteCode = "";
    private String secondaryRouteName = "";
    private String routeName = "";
    private String fuelCost = "";
    private String customerId = "";
    private String fixedKmPerMonth = "";
    private String fixedReeferHours = "";
    private String fixedReeferMinutes = "";
    private String routeValidFrom = "";
    private String routeValidTo = "";
    private String distance = "";
    private String totalReeferHours = "";
    private String totalReeferMinutes = "";
    private String totalWaitMinutes = "";
//    raju
//    private String customerName = "";
    private String invoiceCode = "";
    private String invoiceNo = "";
    private String invoiceDate = "";
    private String invoiceAmount = "";
    private String grandTotal = "";
    private String invoiceStatus = "";
    private String payAmount = "";
    private String orderType = "";
    
//    private String invoiceCode = "";
    
    

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getFuelCost() {
        return fuelCost;
    }

    public void setFuelCost(String fuelCost) {
        this.fuelCost = fuelCost;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFixedKmPerMonth() {
        return fixedKmPerMonth;
    }

    public void setFixedKmPerMonth(String fixedKmPerMonth) {
        this.fixedKmPerMonth = fixedKmPerMonth;
    }

    public String getFixedReeferHours() {
        return fixedReeferHours;
    }

    public void setFixedReeferHours(String fixedReeferHours) {
        this.fixedReeferHours = fixedReeferHours;
    }

    public String getFixedReeferMinutes() {
        return fixedReeferMinutes;
    }

    public void setFixedReeferMinutes(String fixedReeferMinutes) {
        this.fixedReeferMinutes = fixedReeferMinutes;
    }

    public String getRouteValidFrom() {
        return routeValidFrom;
    }

    public void setRouteValidFrom(String routeValidFrom) {
        this.routeValidFrom = routeValidFrom;
    }

    public String getRouteValidTo() {
        return routeValidTo;
    }

    public void setRouteValidTo(String routeValidTo) {
        this.routeValidTo = routeValidTo;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(String totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public String getTotalReeferHours() {
        return totalReeferHours;
    }

    public void setTotalReeferHours(String totalReeferHours) {
        this.totalReeferHours = totalReeferHours;
    }

    public String getTotalReeferMinutes() {
        return totalReeferMinutes;
    }

    public void setTotalReeferMinutes(String totalReeferMinutes) {
        this.totalReeferMinutes = totalReeferMinutes;
    }

//    public String getTotalWaitMinutes() {
//        return totalWaitMinutes;
//    }
//
//    public void setTotalWaitMinutes(String totalWaitMinutes) {
//        this.totalWaitMinutes = totalWaitMinutes;
//    }

    public String getDateName() {
        return dateName;
    }

    public void setDateName(String dateName) {
        this.dateName = dateName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getNextWeek() {
        return nextWeek;
    }

    public void setNextWeek(String nextWeek) {
        this.nextWeek = nextWeek;
    }

    public String[] getSecRouteId() {
        return secRouteId;
    }

    public void setSecRouteId(String[] secRouteId) {
        this.secRouteId = secRouteId;
    }

    public String[] getScheduledate() {
        return scheduledate;
    }

    public void setScheduledate(String[] scheduledate) {
        this.scheduledate = scheduledate;
    }

    public String getSecondaryRouteId() {
        return secondaryRouteId;
    }

    public void setSecondaryRouteId(String secondaryRouteId) {
        this.secondaryRouteId = secondaryRouteId;
    }

    public String getSecondaryRouteCode() {
        return secondaryRouteCode;
    }

    public void setSecondaryRouteCode(String secondaryRouteCode) {
        this.secondaryRouteCode = secondaryRouteCode;
    }

    public String getSecondaryRouteName() {
        return secondaryRouteName;
    }

    public void setSecondaryRouteName(String secondaryRouteName) {
        this.secondaryRouteName = secondaryRouteName;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getEntryType() {
        return entryType;
    }

    public void setEntryType(String entryType) {
        this.entryType = entryType;
    }

    public String getConsignmentNoteNo() {
        return consignmentNoteNo;
    }

    public void setConsignmentNoteNo(String consignmentNoteNo) {
        this.consignmentNoteNo = consignmentNoteNo;
    }

    public String getConsignmentDate() {
        return consignmentDate;
    }

    public void setConsignmentDate(String consignmentDate) {
        this.consignmentDate = consignmentDate;
    }

    public String getOrderReferenceNo() {
        return orderReferenceNo;
    }

    public void setOrderReferenceNo(String orderReferenceNo) {
        this.orderReferenceNo = orderReferenceNo;
    }

    public String getOrderReferenceRemarks() {
        return orderReferenceRemarks;
    }

    public void setOrderReferenceRemarks(String orderReferenceRemarks) {
        this.orderReferenceRemarks = orderReferenceRemarks;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCustomerMobileNo() {
        return customerMobileNo;
    }

    public void setCustomerMobileNo(String customerMobileNo) {
        this.customerMobileNo = customerMobileNo;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getCustomerPhoneNo() {
        return customerPhoneNo;
    }

    public void setCustomerPhoneNo(String customerPhoneNo) {
        this.customerPhoneNo = customerPhoneNo;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getMultiPickup() {
        return multiPickup;
    }

    public void setMultiPickup(String multiPickup) {
        this.multiPickup = multiPickup;
    }

    public String getMultiDelivery() {
        return multiDelivery;
    }

    public void setMultiDelivery(String multiDelivery) {
        this.multiDelivery = multiDelivery;
    }

    public String getConsignmentOrderInstruction() {
        return consignmentOrderInstruction;
    }

    public void setConsignmentOrderInstruction(String consignmentOrderInstruction) {
        this.consignmentOrderInstruction = consignmentOrderInstruction;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getReeferRequired() {
        return reeferRequired;
    }

    public void setReeferRequired(String reeferRequired) {
        this.reeferRequired = reeferRequired;
    }

    public String getContractRateId() {
        return contractRateId;
    }

    public void setContractRateId(String contractRateId) {
        this.contractRateId = contractRateId;
    }

    public String getVehicleRequiredDate() {
        return vehicleRequiredDate;
    }

    public void setVehicleRequiredDate(String vehicleRequiredDate) {
        this.vehicleRequiredDate = vehicleRequiredDate;
    }

    public String getVehicleRequiredHour() {
        return vehicleRequiredHour;
    }

    public void setVehicleRequiredHour(String vehicleRequiredHour) {
        this.vehicleRequiredHour = vehicleRequiredHour;
    }

    public String getVehicleRequiredMinute() {
        return vehicleRequiredMinute;
    }

    public void setVehicleRequiredMinute(String vehicleRequiredMinute) {
        this.vehicleRequiredMinute = vehicleRequiredMinute;
    }

    public String getVehicleInstruction() {
        return vehicleInstruction;
    }

    public void setVehicleInstruction(String vehicleInstruction) {
        this.vehicleInstruction = vehicleInstruction;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getConsignorPhoneNo() {
        return consignorPhoneNo;
    }

    public void setConsignorPhoneNo(String consignorPhoneNo) {
        this.consignorPhoneNo = consignorPhoneNo;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneePhoneNo() {
        return consigneePhoneNo;
    }

    public void setConsigneePhoneNo(String consigneePhoneNo) {
        this.consigneePhoneNo = consigneePhoneNo;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getRateWithReefer() {
        return rateWithReefer;
    }

    public void setRateWithReefer(String rateWithReefer) {
        this.rateWithReefer = rateWithReefer;
    }

    public String getRateWithoutReefer() {
        return rateWithoutReefer;
    }

    public void setRateWithoutReefer(String rateWithoutReefer) {
        this.rateWithoutReefer = rateWithoutReefer;
    }

    public String getTotalPackage() {
        return totalPackage;
    }

    public void setTotalPackage(String totalPackage) {
        this.totalPackage = totalPackage;
    }

    public String getTotalWeightage() {
        return totalWeightage;
    }

    public void setTotalWeightage(String totalWeightage) {
        this.totalWeightage = totalWeightage;
    }

    public String getTotFreightAmount() {
        return totFreightAmount;
    }

    public void setTotFreightAmount(String totFreightAmount) {
        this.totFreightAmount = totFreightAmount;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getRouteContractId() {
        return routeContractId;
    }

    public void setRouteContractId(String routeContractId) {
        this.routeContractId = routeContractId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getLatitudePosition() {
        return latitudePosition;
    }

    public void setLatitudePosition(String latitudePosition) {
        this.latitudePosition = latitudePosition;
    }

    public String getLongitudePosition() {
        return longitudePosition;
    }

    public void setLongitudePosition(String longitudePosition) {
        this.longitudePosition = longitudePosition;
    }

    public String getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(String pointAddress) {
        this.pointAddress = pointAddress;
    }

    public String getPointId() {
        return pointId;
    }

    public void setPointId(String pointId) {
        this.pointId = pointId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getPrimaryDriverId() {
        return primaryDriverId;
    }

    public void setPrimaryDriverId(String primaryDriverId) {
        this.primaryDriverId = primaryDriverId;
    }

    public String getPrimaryDriverName() {
        return primaryDriverName;
    }

    public void setPrimaryDriverName(String primaryDriverName) {
        this.primaryDriverName = primaryDriverName;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getExtraKmCharge() {
        return extraKmCharge;
    }

    public void setExtraKmCharge(String extraKmCharge) {
        this.extraKmCharge = extraKmCharge;
    }

    public String getFixedKm() {
        return fixedKm;
    }

    public void setFixedKm(String fixedKm) {
        this.fixedKm = fixedKm;
    }

    public String getFixedKmCharge() {
        return fixedKmCharge;
    }

    public void setFixedKmCharge(String fixedKmCharge) {
        this.fixedKmCharge = fixedKmCharge;
    }

    public String getNoOfVehicles() {
        return noOfVehicles;
    }

    public void setNoOfVehicles(String noOfVehicles) {
        this.noOfVehicles = noOfVehicles;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getHumanId() {
        return humanId;
    }

    public void setHumanId(String humanId) {
        this.humanId = humanId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAverageKM() {
        return averageKM;
    }

    public void setAverageKM(String averageKM) {
        this.averageKM = averageKM;
    }

    public String getLoadingHr() {
        return loadingHr;
    }

    public void setLoadingHr(String loadingHr) {
        this.loadingHr = loadingHr;
    }

    public String getPreCoolingHr() {
        return preCoolingHr;
    }

    public void setPreCoolingHr(String preCoolingHr) {
        this.preCoolingHr = preCoolingHr;
    }

    public String getExtraKmCalculation() {
        return extraKmCalculation;
    }

    public void setExtraKmCalculation(String extraKmCalculation) {
        this.extraKmCalculation = extraKmCalculation;
    }

    public String getContractCngCost() {
        return contractCngCost;
    }

    public void setContractCngCost(String contractCngCost) {
        this.contractCngCost = contractCngCost;
    }

    public String getContractDieselCost() {
        return contractDieselCost;
    }

    public void setContractDieselCost(String contractDieselCost) {
        this.contractDieselCost = contractDieselCost;
    }

    public String getRateChangeOfCng() {
        return rateChangeOfCng;
    }

    public void setRateChangeOfCng(String rateChangeOfCng) {
        this.rateChangeOfCng = rateChangeOfCng;
    }

    public String getRateChangeOfDiesel() {
        return rateChangeOfDiesel;
    }

    public void setRateChangeOfDiesel(String rateChangeOfDiesel) {
        this.rateChangeOfDiesel = rateChangeOfDiesel;
    }
    
    public String getBillMonth() {
        return billMonth;
    }

    public void setBillMonth(String billMonth) {
        this.billMonth = billMonth;
    }


    public String getBillYear() {
        return billYear;
    }

    public void setBillYear(String billYear) {
        this.billYear = billYear;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getPrimaryDriver() {
        return primaryDriver;
    }

    public void setPrimaryDriver(String primaryDriver) {
        this.primaryDriver = primaryDriver;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    public String getTotalWaitMinutes() {
        return totalWaitMinutes;
    }

    public void setTotalWaitMinutes(String totalWaitMinutes) {
        this.totalWaitMinutes = totalWaitMinutes;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    
}
