/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.fuelManagement.web;

/**
 *
 * @author vinoth
 */
public class FuelManagementCommand {

    private String kmPerLitter = "";
    private String kmPerLitterLoaded = "";
    private String ageingId = "";
    private String mfrAge = "";
    private String mfrToAge = "";
    private String status = "";
    private String[] vehicleTypeIds ;
    private String[] costPerKm ;
    private String[] tollCostPerKm ;
    private String[] miscAmt ;
    private String[] driverIncentive ;
    private String[] etcAmount ;
    //start at 18-05-2015
    private String[] vehicleTypeCostId ;

    private String vehTypeId = "1";
    private String companyId = "";
    private String servicePtId = "";
    private String owningCompId = "";
    private String usingCompId = "";
    private String vehicleId = "";
    private String usageTypeId = "0";
    private String regNo = "";
    private String vehNo = "";
    private String tankCapacity = "";
    private String takRead = "";
    private String activeInd = "";
    private String tankReading = "";
    private String present = "";
    private String previous = "";
    private String rate = "";
    private String date = "";
    private String fuelFilled = "";
    private String fuel = "";
    private String totalKm = "";
    private String avgKm = "";
    private String place = "";
    private String couponNo = "";
    private String amount = "";
    private String outFillKm = "";
    private String outFill = "0";
    private String fromDate = "";
    private String toDate = "";
    private String toKM = "";
    private String fromKm = "";
    private String minLevel = "";
    private String toYear = "";
    private String fromYear = "";
    private String mfrName = "";
    private String runReading = "";
    private String modelName = "";
    private String modelId = "0";
    private String mfrId = "0";
    private String milleage = "";
    private String outFillTime = "1";
    private String endDate = "";
    private String startDate = "";
    private String fuelPrice = "";
    private String fuelPriceId = "";
    private String fuelLocatName = "";
    private String fillingStationId = "";
    private String bunkName = "";
    private String bunkId = "";
    private String[] vehTypeIds = null;
    private String[] companyIds = null;
    private String[] servicePtIds = null;
    private String[] vehicleIds = null;
    private String[] selectedIndex = null;
    private String[] regNos = null;
    private String[] dats = null;
    private String[] rates = null;
    private String[] fuels = null;
    private String[] presentReadings = null;
    private String[] places = null;
    private String[] outFillKms = null;
    private String[][] avgs = null;
    private String[][] inFills = null;
    private String[][] outFills = null;
    private String vehicleTypeId = "0";

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getServicePtId() {
        return servicePtId;
    }

    public void setServicePtId(String servicePtId) {
        this.servicePtId = servicePtId;
    }

    public String getVehTypeId() {
        return vehTypeId;
    }

    public void setVehTypeId(String vehTypeId) {
        this.vehTypeId = vehTypeId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String[] getCompanyIds() {
        return companyIds;
    }

    public void setCompanyIds(String[] companyIds) {
        this.companyIds = companyIds;
    }

    public String[] getServicePtIds() {
        return servicePtIds;
    }

    public void setServicePtIds(String[] servicePtIds) {
        this.servicePtIds = servicePtIds;
    }

    public String[] getVehTypeIds() {
        return vehTypeIds;
    }

    public void setVehTypeIds(String[] vehTypeIds) {
        this.vehTypeIds = vehTypeIds;
    }

    public String[] getVehicleIds() {
        return vehicleIds;
    }

    public void setVehicleIds(String[] vehicleIds) {
        this.vehicleIds = vehicleIds;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String[] getRegNos() {
        return regNos;
    }

    public void setRegNos(String[] regNos) {
        this.regNos = regNos;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFuelFilled() {
        return fuelFilled;
    }

    public void setFuelFilled(String fuelFilled) {
        this.fuelFilled = fuelFilled;
    }

    public String getPresent() {
        return present;
    }

    public void setPresent(String present) {
        this.present = present;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getTankCapacity() {
        return tankCapacity;
    }

    public void setTankCapacity(String tankCapacity) {
        this.tankCapacity = tankCapacity;
    }

    public String getTankReading() {
        return tankReading;
    }

    public void setTankReading(String tankReading) {
        this.tankReading = tankReading;
    }

    public String[] getDats() {
        return dats;
    }

    public void setDats(String[] dats) {
        this.dats = dats;
    }

    public String[] getFuels() {
        return fuels;
    }

    public void setFuels(String[] fuels) {
        this.fuels = fuels;
    }

    public String[] getPresentReadings() {
        return presentReadings;
    }

    public void setPresentReadings(String[] presentReadings) {
        this.presentReadings = presentReadings;
    }

    public String[] getRates() {
        return rates;
    }

    public void setRates(String[] rates) {
        this.rates = rates;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAvgKm() {
        return avgKm;
    }

    public void setAvgKm(String avgKm) {
        this.avgKm = avgKm;
    }

    public String getCouponNo() {
        return couponNo;
    }

    public void setCouponNo(String couponNo) {
        this.couponNo = couponNo;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getOutFillKm() {
        return outFillKm;
    }

    public void setOutFillKm(String outFillKm) {
        this.outFillKm = outFillKm;
    }

    public String getOutFill() {
        return outFill;
    }

    public void setOutFill(String outFill) {
        this.outFill = outFill;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromKm() {
        return fromKm;
    }

    public void setFromKm(String fromKm) {
        this.fromKm = fromKm;
    }

    public String getToKM() {
        return toKM;
    }

    public void setToKM(String toKM) {
        this.toKM = toKM;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getMinLevel() {
        return minLevel;
    }

    public void setMinLevel(String minLevel) {
        this.minLevel = minLevel;
    }

    public String getMilleage() {
        return milleage;
    }

    public void setMilleage(String milleage) {
        this.milleage = milleage;
    }

    public String getFromYear() {
        return fromYear;
    }

    public void setFromYear(String fromYear) {
        this.fromYear = fromYear;
    }

    public String getMfrName() {
        return mfrName;
    }

    public void setMfrName(String mfrName) {
        this.mfrName = mfrName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getToYear() {
        return toYear;
    }

    public void setToYear(String toYear) {
        this.toYear = toYear;
    }

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getOutFillTime() {
        return outFillTime;
    }

    public void setOutFillTime(String outFillTime) {
        this.outFillTime = outFillTime;
    }

    public String[] getOutFillKms() {
        return outFillKms;
    }

    public void setOutFillKms(String[] outFillKms) {
        this.outFillKms = outFillKms;
    }

    public String[] getPlaces() {
        return places;
    }

    public void setPlaces(String[] places) {
        this.places = places;
    }

    public String getUsageTypeId() {
        return usageTypeId;
    }

    public void setUsageTypeId(String usageTypeId) {
        this.usageTypeId = usageTypeId;
    }

    public String getVehNo() {
        return vehNo;
    }

    public void setVehNo(String vehNo) {
        this.vehNo = vehNo;
    }

    public String getOwningCompId() {
        return owningCompId;
    }

    public void setOwningCompId(String owningCompId) {
        this.owningCompId = owningCompId;
    }

    public String getUsingCompId() {
        return usingCompId;
    }

    public void setUsingCompId(String usingCompId) {
        this.usingCompId = usingCompId;
    }

    public String getTakRead() {
        return takRead;
    }

    public void setTakRead(String takRead) {
        this.takRead = takRead;
    }

    public String getRunReading() {
        return runReading;
    }

    public void setRunReading(String runReading) {
        this.runReading = runReading;
    }

    public String[][] getAvgs() {
        return avgs;
    }

    public void setAvgs(String[][] avgs) {
        this.avgs = avgs;
    }

    public String[][] getInFills() {
        return inFills;
    }

    public void setInFills(String[][] inFills) {
        this.inFills = inFills;
    }

    public String[][] getOutFills() {
        return outFills;
    }

    public void setOutFills(String[][] outFills) {
        this.outFills = outFills;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getFillingStationId() {
        return fillingStationId;
    }

    public void setFillingStationId(String fillingStationId) {
        this.fillingStationId = fillingStationId;
    }

    public String getFuelLocatName() {
        return fuelLocatName;
    }

    public void setFuelLocatName(String fuelLocatName) {
        this.fuelLocatName = fuelLocatName;
    }

    public String getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(String fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public String getFuelPriceId() {
        return fuelPriceId;
    }

    public void setFuelPriceId(String fuelPriceId) {
        this.fuelPriceId = fuelPriceId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getBunkId() {
        return bunkId;
    }

    public void setBunkId(String bunkId) {
        this.bunkId = bunkId;
    }

    public String getBunkName() {
        return bunkName;
    }

    public void setBunkName(String bunkName) {
        this.bunkName = bunkName;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String[] getCostPerKm() {
        return costPerKm;
    }

    public void setCostPerKm(String[] costPerKm) {
        this.costPerKm = costPerKm;
    }

    public String[] getDriverIncentive() {
        return driverIncentive;
    }

    public void setDriverIncentive(String[] driverIncentive) {
        this.driverIncentive = driverIncentive;
    }

    public String[] getEtcAmount() {
        return etcAmount;
    }

    public void setEtcAmount(String[] etcAmount) {
        this.etcAmount = etcAmount;
    }

    public String[] getMiscAmt() {
        return miscAmt;
    }

    public void setMiscAmt(String[] miscAmt) {
        this.miscAmt = miscAmt;
    }

    public String[] getTollCostPerKm() {
        return tollCostPerKm;
    }

    public void setTollCostPerKm(String[] tollCostPerKm) {
        this.tollCostPerKm = tollCostPerKm;
    }

    public String[] getVehicleTypeCostId() {
        return vehicleTypeCostId;
    }

    public void setVehicleTypeCostId(String[] vehicleTypeCostId) {
        this.vehicleTypeCostId = vehicleTypeCostId;
    }

    public String[] getVehicleTypeIds() {
        return vehicleTypeIds;
    }

    public void setVehicleTypeIds(String[] vehicleTypeIds) {
        this.vehicleTypeIds = vehicleTypeIds;
    }

    public String getMfrAge() {
        return mfrAge;
    }

    public void setMfrAge(String mfrAge) {
        this.mfrAge = mfrAge;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAgeingId() {
        return ageingId;
    }

    public void setAgeingId(String ageingId) {
        this.ageingId = ageingId;
    }

    public String getKmPerLitter() {
        return kmPerLitter;
    }

    public void setKmPerLitter(String kmPerLitter) {
        this.kmPerLitter = kmPerLitter;
    }

    public String getMfrToAge() {
        return mfrToAge;
    }

    public void setMfrToAge(String mfrToAge) {
        this.mfrToAge = mfrToAge;
    }

    public String getKmPerLitterLoaded() {
        return kmPerLitterLoaded;
    }

    public void setKmPerLitterLoaded(String kmPerLitterLoaded) {
        this.kmPerLitterLoaded = kmPerLitterLoaded;
    }

    
}
