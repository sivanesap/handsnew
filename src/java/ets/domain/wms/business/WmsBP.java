package ets.domain.wms.business;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.secondaryOperation.business.SecondaryOperationTO;
import ets.domain.wms.data.WmsDAO;
import ets.domain.wms.business.WmsTO;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Iterator;

public class WmsBP {

    public static ArrayList getWhList(SecondaryOperationTO SecondaryOperationTO) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public WmsBP() {
    }
    WmsDAO wmsDAO;

    public WmsDAO getWmsDAO() {
        return wmsDAO;
    }

    public void setWmsDAO(WmsDAO wmsDAO) {
        this.wmsDAO = wmsDAO;
    }

    /**
     * This method used to Get Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises ..
     */
    public ArrayList getAsnMasterList(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList asnMasterList = new ArrayList();
        asnMasterList = wmsDAO.getAsnMasterList(wmsTo);
        return asnMasterList;
    }

    public ArrayList getAsnDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList asnDetails = new ArrayList();
        asnDetails = wmsDAO.getAsnDetails(wmsTo);
        return asnDetails;
    }

    public ArrayList getviewDeliveryRequest(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList asnMasterList = new ArrayList();
        asnMasterList = wmsDAO.getviewDeliveryRequest(wmsTo);
        return asnMasterList;
    }

    public ArrayList getDeliveryRequestUpdate(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getDeliveryRequestUpdate = new ArrayList();
        getDeliveryRequestUpdate = wmsDAO.getDeliveryRequestUpdate(wmsTo);
        return getDeliveryRequestUpdate;
    }

    public int getDeliveryRequestFinalUpdate(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            status = wmsDAO.getDeliveryRequestFinalUpdate(wmsTo, userId, session);

            if (status > 0) {
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int saveSerialTempDetails(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = wmsDAO.saveSerialTempDetails(wmsTo, userId);
        return insertStatus;
    }

    //     public int saveGrnSerialDetails(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
    //        int insertStatus = 0;
    //        insertStatus = wmsDAO.saveGrnSerialDetails(wmsTo, userId);
    //        return insertStatus;
    //    }
    public int saveGrnSerialDetails(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            insertStatus = wmsDAO.saveGrnSerialDetails(wmsTo, userId, session);
            System.out.println("saveGrnSerialDetails---" + insertStatus);
            session.commitTransaction();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return insertStatus;
    }

    public int saveGrnSerialStockDetails(WmsTO wmsTo, String actualFilePath, String actualFilePath1, String fileSaved, String fileSaved1, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();

            String path = wmsDAO.grnImageFileUpload(wmsTo, actualFilePath, fileSaved, session);
            String path1 = wmsDAO.grnEwayImageFileUpload(wmsTo, actualFilePath1, fileSaved1, session);
            insertStatus = wmsDAO.saveGrnSerialStockDetails(wmsTo, path, userId, session);
            System.out.println("saveGrnSerialStockDetails--BP-" + insertStatus);
            session.commitTransaction();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return insertStatus;
    }

    public ArrayList getSerialTempDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList serialTempDetails = new ArrayList();
        serialTempDetails = wmsDAO.getSerialTempDetails(wmsTo);
        return serialTempDetails;
    }

    public ArrayList getConnectBillUpload(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getConnectBillUpload = new ArrayList();
        getConnectBillUpload = wmsDAO.getConnectBillUpload(wmsTo);

        return getConnectBillUpload;
    }

    public int insertConnectBillUpload(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        int insertConnectBillUpload = 0;
        insertConnectBillUpload = wmsDAO.insertConnectBillUpload(wmsTo);
        return insertConnectBillUpload;
    }

    public int clearConnectBillMappingTemp(int userId) throws FPRuntimeException, FPBusinessException {
        int clearConnectBillMappingTemp = 0;
        clearConnectBillMappingTemp = wmsDAO.clearConnectBillMappingTemp(userId);
        return clearConnectBillMappingTemp;
    }

    public int updateRpWorkOrderStatus(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        int updateRpWorkOrderStatus = 0;
        updateRpWorkOrderStatus = wmsDAO.updateRpWorkOrderStatus(wmsTo);
        return updateRpWorkOrderStatus;
    }

    public int saveConnectBillMapping(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        int saveConnectBillMapping = 0;
        saveConnectBillMapping = wmsDAO.saveConnectBillMapping(wmsTo);
        return saveConnectBillMapping;
    }

    public int setRpWorkOrderDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        int setRpWorkOrderDetails = 0;
        setRpWorkOrderDetails = wmsDAO.setRpWorkOrderDetails(wmsTo);
        return setRpWorkOrderDetails;
    }

    public ArrayList getRpWoProductList(WmsTO wmsTo) throws FPRuntimeException {
        ArrayList getrpWoPn = new ArrayList();
        getrpWoPn = wmsDAO.getRpWoProductList(wmsTo);
        return getrpWoPn;
    }

    public ArrayList getrpWorkOrderDetails(WmsTO wmsTo) throws FPRuntimeException {
        ArrayList getrpWorkOrderDetails = new ArrayList();
        getrpWorkOrderDetails = wmsDAO.getrpWorkOrderDetails(wmsTo);
        return getrpWorkOrderDetails;
    }

    public ArrayList rpWorkOrderDetailsView(WmsTO wmsTo) throws FPRuntimeException {
        ArrayList rpWorkOrderDetailsView = new ArrayList();
        rpWorkOrderDetailsView = wmsDAO.rpWorkOrderDetailsView(wmsTo);
        return rpWorkOrderDetailsView;
    }

    public ArrayList getSerialTempDetailsUnsed(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList serialTempDetails = new ArrayList();
        serialTempDetails = wmsDAO.getSerialTempDetailsUnsed(wmsTo);
        return serialTempDetails;
    }

    public String serialNoAlreadyExists(String SerialNo) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.serialNoAlreadyExists(SerialNo);
        return status;
    }

    public String checkRpExistSerialNo(String SerialNo) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.checkRpExistSerialNo(SerialNo);
        return status;
    }

    public String rpSelectSerialId(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.rpSelectSerialId(wms);
        return status;
    }

    public String checkExistSapCode(String sapCode) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.checkExistSapCode(sapCode);
        return status;
    }

    public String checkExistSkuCode(String skuCode) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.checkExistSkuCode(skuCode);
        return status;
    }

    public String getRpExistSerialNo(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.getRpExistSerialNo(wms);
        return status;
    }

    public String checkEwayBillNo(String ewayNo) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.checkEwayBillNo(ewayNo);
        return status;
    }

    public String checkRouteCode(String routeCode) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.checkRouteCode(routeCode);
        return status;
    }

    public String invoiceNoAlreadyExists(String invoiceNo) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.InvoiceNoAlreadyExists(invoiceNo);
        return status;
    }

    public ArrayList getDockInList(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList getDockInList = new ArrayList();
        getDockInList = wmsDAO.getDockInList(wmsTo);
        return getDockInList;
    }

    public ArrayList getDockInDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList dockInDetails = new ArrayList();
        dockInDetails = wmsDAO.getDockInDetails(wmsTo);
        return dockInDetails;
    }

    public ArrayList getBinList(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getBinList = new ArrayList();
        getBinList = wmsDAO.getBinList(wmsTo);
        return getBinList;
    }

    public ArrayList grnViewDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList grnViewDetails = new ArrayList();
        grnViewDetails = wmsDAO.grnViewDetails(wmsTo);
        return grnViewDetails;
    }

    public ArrayList viewSerialNumberDetails(String grnId) throws FPRuntimeException, FPBusinessException {
        ArrayList viewSerialNumberDetails = new ArrayList();
        viewSerialNumberDetails = wmsDAO.viewSerialNumberDetails(grnId);
        return viewSerialNumberDetails;
    }

    public ArrayList getOrderDetailsList(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList ordersList = new ArrayList();
        ordersList = wmsDAO.getOrderDetailsList(wmsTo);
        return ordersList;
    }

    public ArrayList getWareHouseList() throws FPRuntimeException, FPBusinessException {
        ArrayList getWareHouseList = new ArrayList();
        getWareHouseList = wmsDAO.getWareHouseList();
        return getWareHouseList;
    }

    public String getBarcodeMaster() throws FPRuntimeException, FPBusinessException {
        String getBarcodeMaster = "";
        getBarcodeMaster = wmsDAO.getBarcodeMaster();
        return getBarcodeMaster;
    }

    public int saveTempGrnDetails(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
        int saveTempGrnDetails = 0;
        saveTempGrnDetails = wmsDAO.saveTempGrnDetails(wmsTo, userId);
        return saveTempGrnDetails;
    }

    public int updateSerialTempDetails(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
        int updateSerialTempDetails = 0;
        updateSerialTempDetails = wmsDAO.updateSerialTempDetails(wmsTo, userId);
        return updateSerialTempDetails;
    }

    public int generateBarcode(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
        int generateBarcode = 0;
        generateBarcode = wmsDAO.generateBarcode(wmsTo, userId);
        return generateBarcode;
    }

    public int delGrnTempSerial(String tempId, String grnId) throws FPRuntimeException, FPBusinessException {
        int delGrnTempSerial = 0;
        delGrnTempSerial = wmsDAO.delGrnTempSerial(tempId, grnId);
        return delGrnTempSerial;
    }

    public ArrayList getPoCode(WmsTO wmsTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getPoCode = new ArrayList();
        getPoCode = wmsDAO.getPoCode(wmsTO, userId);
        return getPoCode;
    }

    public ArrayList getCustList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getCustList = new ArrayList();
        getCustList = wmsDAO.getCustList(wmsTO);
        return getCustList;
    }

    public ArrayList getItemList(int userId, String custId) throws FPRuntimeException, FPBusinessException {
        ArrayList getItemList = new ArrayList();
        getItemList = wmsDAO.getItemList(userId, custId);
        return getItemList;
    }

    public int saveGtnDetails(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
        int saveGtnDetails = 0;
        saveGtnDetails = wmsDAO.saveGtnDetails(wmsTo, userId);
        return saveGtnDetails;
    }

    public ArrayList getReceivedGateIn(int userId) throws FPRuntimeException, FPBusinessException {

        ArrayList getReceivedGateIn = new ArrayList();
        getReceivedGateIn = wmsDAO.getReceivedGateIn(userId);
        return getReceivedGateIn;
    }

    public ArrayList getGtnDetails(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {

        ArrayList gtnDetails = new ArrayList();
        gtnDetails = wmsDAO.getGtnDetails(wmsTo, userId);
        return gtnDetails;
    }

    public ArrayList getUserDetails(int UserId) throws FPRuntimeException, FPBusinessException {

        ArrayList getUserDetails = new ArrayList();
        getUserDetails = wmsDAO.getUserDetails(UserId);
        return getUserDetails;
    }

    public ArrayList getBinDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList getBinDetails = new ArrayList();
        getBinDetails = wmsDAO.getBinDetails(wmsTo);
        return getBinDetails;
    }

    public ArrayList getImPOList(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getImPOList = new ArrayList();
        getImPOList = wmsDAO.getImPOList(wmsTo);
        return getImPOList;
    }

    public ArrayList rpBinDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList rpBinDetails = new ArrayList();
        rpBinDetails = wmsDAO.rpBinDetails(wmsTo);
        return rpBinDetails;
    }

    public ArrayList rpStorageDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList rpStorageDetails = new ArrayList();
        rpStorageDetails = wmsDAO.rpStorageDetails(wmsTo);
        return rpStorageDetails;
    }

    public ArrayList getProList(int userId, WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList getProList = new ArrayList();
        getProList = wmsDAO.getProList(userId, wmsTo);
        return getProList;
    }

    public ArrayList proListDetails(int userId, WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList proListDetails = new ArrayList();
        proListDetails = wmsDAO.proListDetails(userId, wmsTo);
        return proListDetails;
    }

    public ArrayList getSerialDets(String gtnId, String grnId, String itemId, String asnId) throws FPRuntimeException, FPBusinessException {

        ArrayList getSerialDets = new ArrayList();
        getSerialDets = wmsDAO.getSerialDet(gtnId, grnId, itemId, asnId);
        return getSerialDets;
    }

    public ArrayList getStateList() throws FPRuntimeException, FPBusinessException {

        ArrayList getStateList = new ArrayList();
        getStateList = wmsDAO.getStateList();
        return getStateList;
    }

    public ArrayList getWhList() throws FPRuntimeException, FPBusinessException {
        ArrayList getWhList = new ArrayList();
        getWhList = wmsDAO.getWhList();
        return getWhList;
    }
    public ArrayList AddwhList() throws FPRuntimeException, FPBusinessException {
        ArrayList AddwhList = new ArrayList();
        AddwhList = wmsDAO.AddwhList();
        return AddwhList;
    }

    public ArrayList warehouseOrderReport(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList warehouseOrderReport = new ArrayList();
        warehouseOrderReport = wmsDAO.warehouseOrderReport(wms);
        return warehouseOrderReport;
    }

    public ArrayList getItemLists(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getItemList = new ArrayList();
        getItemList = wmsDAO.getItemLists(userId);
        return getItemList;
    }

    public int insertAsnDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        int status = 0;

        try {

            status = wmsDAO.insertAsnDetails(wmsTo);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public ArrayList processGetItemList(String whId) throws FPRuntimeException, FPBusinessException {

        ArrayList getItemList = new ArrayList();
        getItemList = wmsDAO.processGetItemList(whId);
        return getItemList;

    }

    public ArrayList getFkRunsheetDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getFkRunsheetDetails = new ArrayList();
        getFkRunsheetDetails = wmsDAO.getFkRunsheetDetails(wms);
        return getFkRunsheetDetails;
    }

    public ArrayList getSerialNumber(String itemId, String userWhId) throws FPRuntimeException, FPBusinessException {

        ArrayList getSerialNumber = new ArrayList();
        getSerialNumber = wmsDAO.processGetSerialNumber(itemId, userWhId);
        return getSerialNumber;

    }

    public ArrayList getWareToHouseList(String userWhId) throws FPRuntimeException, FPBusinessException {
        ArrayList getWareHouseList = new ArrayList();
        getWareHouseList = wmsDAO.getWareToHouseList(userWhId);
        return getWareHouseList;
    }

    public int updateConnectLrNo(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int updateConnectLrNo = 0;
        updateConnectLrNo = wmsDAO.updateConnectLrNo(wms);
        return updateConnectLrNo;
    }

    public int insertTransferQty(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        int status = 0;

        try {
            status = wmsDAO.insertTransferQty(wmsTo);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public ArrayList processVendorList() throws FPRuntimeException, FPBusinessException {
        ArrayList vendorList = new ArrayList();
        vendorList = wmsDAO.ProcessVendorList();
        return vendorList;
    }

    public ArrayList getConnectVendorList(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getConnectVendorList = new ArrayList();
        getConnectVendorList = wmsDAO.getConnectVendorList(wms);
        return getConnectVendorList;
    }

    public ArrayList getVendorList() throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = new ArrayList();
        statusList = wmsDAO.getVendorList();
        return statusList;
    }

    public ArrayList barCodeDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList barCodeDetails = new ArrayList();
        barCodeDetails = wmsDAO.barCodeDetails(wmsTO);
        return barCodeDetails;
    }

    public String getUserWarehouse(int userId) throws FPRuntimeException, FPBusinessException {
        String getUserWarehouse = "";
        getUserWarehouse = wmsDAO.getUserWarehouse(userId);
        return getUserWarehouse;
    }

    public ArrayList serialNumberDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList serialNumberDetails = new ArrayList();
        serialNumberDetails = wmsDAO.getserialNumberDetails(wms);
        return serialNumberDetails;
    }

    public int saveRunsheetUpload(WmsTO wmsTO, int userId) {
        int saveRunsheetUpload = 0;
        saveRunsheetUpload = wmsDAO.saveRunsheetUpload(wmsTO, userId);
        return saveRunsheetUpload;
    }

    public int uploadRunsheet(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int uploadRunsheet = 0;
        uploadRunsheet = wmsDAO.uploadRunsheet(wmsTO);
        return uploadRunsheet;
    }

    public int ifbOrderUpload(WmsTO wmsTO, int userId, String whId) {
        int ifbOrderUpload = 0;
        ifbOrderUpload = wmsDAO.ifbOrderUpload(wmsTO, userId, whId);
        return ifbOrderUpload;
    }

    public int saveConnectSrn(WmsTO wmsTO, int userId, String whId) {
        int saveConnectSrn = 0;
        saveConnectSrn = wmsDAO.saveConnectSrn(wmsTO, userId, whId);
        return saveConnectSrn;
    }

    public int ifbReturnOrderUpload(WmsTO wmsTO, int userId, String whId) {
        int ifbOrderUpload = 0;
        ifbOrderUpload = wmsDAO.ifbReturnOrderUpload(wmsTO, userId, whId);
        return ifbOrderUpload;
    }

    public int saveReturnIfbOrder(String[] orderId, String[] index) {
        int saveReturnIfbOrder = 0;
        saveReturnIfbOrder = wmsDAO.saveReturnIfbOrder(orderId, index);
        return saveReturnIfbOrder;
    }

    public int deleteIfbUploadTemp(int userId) {
        int deleteIfbUploadTemp = 0;
        deleteIfbUploadTemp = wmsDAO.deleteIfbUploadTemp(userId);
        return deleteIfbUploadTemp;
    }

    public int deleteConnectSrnTemp(int userId) {
        int deleteConnectSrnTemp = 0;
        deleteConnectSrnTemp = wmsDAO.deleteConnectSrnTemp(userId);
        return deleteConnectSrnTemp;
    }

    public int deleteNormalOrderTemp(int userId) {
        int deleteNormalOrderTemp = 0;
        deleteNormalOrderTemp = wmsDAO.deleteNormalOrderTemp(userId);
        return deleteNormalOrderTemp;
    }

    public int deleteReplacementOrderTemp(int userId) {
        int deleteReplacementOrderTemp = 0;
        deleteReplacementOrderTemp = wmsDAO.deleteReplacementOrderTemp(userId);
        return deleteReplacementOrderTemp;
    }

    public int deleteReturnOrderTemp(int userId) {
        int deleteReturnOrderTemp = 0;
        deleteReturnOrderTemp = wmsDAO.deleteReturnOrderTemp(userId);
        return deleteReturnOrderTemp;
    }

    public int deleteRunsheetTemp(int userId) {
        int deleteRunsheetTemp = 0;
        deleteRunsheetTemp = wmsDAO.deleteRunsheetTemp(userId);
        return deleteRunsheetTemp;
    }

    public ArrayList getRunsheetUpload(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getRunsheetUpload = new ArrayList();
        getRunsheetUpload = wmsDAO.getRunsheetUpload(wms);
        return getRunsheetUpload;
    }

    public ArrayList getIFBList(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getIFBList = new ArrayList();
        getIFBList = wmsDAO.getIFBList(wms);
        return getIFBList;
    }

    public int uploadIfbOrder(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int uploadIfbOrder = 0;
        uploadIfbOrder = wmsDAO.uploadIfbOrder(wmsTO);
        return uploadIfbOrder;
    }

    public int saveIfbLr(String[] lrId, String[] ewayBillNo, String[] ewayExpiry, String[] selected) throws FPRuntimeException, FPBusinessException {
        int saveIfbLr = 0;
        saveIfbLr = wmsDAO.saveIfbLr(lrId, ewayBillNo, ewayExpiry, selected);
        return saveIfbLr;
    }

    public ArrayList ifbLRList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList ifbLRList = wmsDAO.ifbLRList(wmsTO);
        return ifbLRList;
    }

    public ArrayList getIfbRunsheetList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getIfbRunsheetList = wmsDAO.getIfbRunsheetList(wmsTO);
        return getIfbRunsheetList;
    }

    public ArrayList getIfbReturnOrders(WmsTO wmsTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getIfbReturnOrders = wmsDAO.getIfbReturnOrders(wmsTO, userId);
        return getIfbReturnOrders;
    }

    public ArrayList getFkRunsheetList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getFkRunsheetList = wmsDAO.getFkRunsheetList(wmsTO);
        return getFkRunsheetList;
    }

    public ArrayList fkFinanceApproval(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList fkFinanceApproval = wmsDAO.fkFinanceApproval(wmsTO);
        return fkFinanceApproval;
    }

    public ArrayList fkManagerApproval(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList fkManagerApproval = wmsDAO.fkManagerApproval(wmsTO);
        return fkManagerApproval;
    }

    public ArrayList getwmsItemReport(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getwmsItemReport = wmsDAO.getwmsItemReport(wmsTO);
        return getwmsItemReport;
    }

    public ArrayList getwmsItemReportView(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getwmsItemReport = wmsDAO.getwmsItemReportView(wmsTO);
        return getwmsItemReport;
    }

    public int UpdateSku(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        int status = 0;

        try {

            status = wmsDAO.UpdateSku(wmsTo);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public ArrayList hubOutList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList hubOutList = wmsDAO.hubOutList(wmsTO);
        return hubOutList;
    }

    public ArrayList hubOutListView(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList hubOutListView = wmsDAO.hubOutListView(wmsTO);
        return hubOutListView;
    }

    public ArrayList hubOutPrintDetails(String lrId, String compId) throws FPRuntimeException, FPBusinessException {
        ArrayList hubOutPrintDetails = wmsDAO.hubOutPrintDetails(lrId, compId);
        return hubOutPrintDetails;
    }

    public ArrayList getIfbOrderDetails(String lrId) throws FPRuntimeException, FPBusinessException {
        ArrayList getIfbOrderDetails = wmsDAO.getIfbOrderDetails(lrId);
        return getIfbOrderDetails;
    }

    public ArrayList getIfbOrderRelease(int userId, WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getIfbOrderDetails = wmsDAO.getIfbOrderRelease(userId, wms);
        return getIfbOrderDetails;
    }

    public ArrayList getRejectionMaster(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getRejectionMaster = wmsDAO.getRejectionMaster(wms);
        return getRejectionMaster;
    }

    public int updateIfbHubout(String[] ewayBillNo, String[] ewayExpiry, String[] orderIds, String[] selectedStatus, String[] hubId, String vehicleNo, String driverName, String driverMobile, String date, String vendorId) throws FPRuntimeException, FPBusinessException {
        int updateIfbHubout = 0;
        updateIfbHubout = wmsDAO.updateIfbHubout(ewayBillNo, ewayExpiry, orderIds, selectedStatus, hubId, vehicleNo, driverName, driverMobile, date, vendorId);
        return updateIfbHubout;
    }

    public int updateHubIn(String[] orderIds, String[] selectedStatus) throws FPRuntimeException, FPBusinessException {
        int updateHubIn = 0;
        updateHubIn = wmsDAO.updateHubIn(orderIds, selectedStatus);
        return updateHubIn;
    }

    public int updateIfbQty(String[] orderIds, String[] qty, String[] index, String[] cust) throws FPRuntimeException, FPBusinessException {
        int updateIfbQty = 0;
        updateIfbQty = wmsDAO.updateIfbQty(orderIds, qty, index, cust);
        return updateIfbQty;
    }

    public int updateReleaseIfbOrder(WmsTO wms, int userId) throws FPRuntimeException, FPBusinessException {
        int updateReleaseIfbOrder = 0;
        updateReleaseIfbOrder = wmsDAO.updateReleaseIfbOrder(wms, userId);
        return updateReleaseIfbOrder;
    }

    public int updateFinanceapproval(WmsTO wms, int userId) throws FPRuntimeException, FPBusinessException {
        int updateFinanceapproval = 0;
        updateFinanceapproval = wmsDAO.updateFinanceapproval(wms, userId);
        return updateFinanceapproval;
    }

    public int rejectFinanceApproval(WmsTO wms, int userId) throws FPRuntimeException, FPBusinessException {
        int rejectFinanceapproval = 0;
        rejectFinanceapproval = wmsDAO.rejectFinanceapproval(wms, userId);
        return rejectFinanceapproval;
    }

    public int updateManagerapproval(WmsTO wms, int userId) throws FPRuntimeException, FPBusinessException {
        int updateManagerapproval = 0;
        updateManagerapproval = wmsDAO.updateManagerapproval(wms, userId);
        return updateManagerapproval;
    }

    public ArrayList ifbHubinList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList ifbHubinList = wmsDAO.ifbHubinList(wmsTO);
        return ifbHubinList;
    }

    public ArrayList getCustomerContract(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getContractList = new ArrayList();
        getContractList = wmsDAO.getCustomerContract(wms);
        return getContractList;
    }

    public int saveNormalOrderTemp(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int saveNormalOrderTemp = 0;
        saveNormalOrderTemp = wmsDAO.saveNormalOrderTemp(wmsTO);
        return saveNormalOrderTemp;
    }

    public int SaveOrderApproval(WmsTO wmsTO, int userId) {
        int SaveOrderApproval = 0;
        SaveOrderApproval = wmsDAO.SaveOrderApproval(wmsTO, userId);

        return SaveOrderApproval;
    }

    public ArrayList getreplacementList(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getreplacementList = new ArrayList();
        getreplacementList = wmsDAO.getreplacementList(wms);
        return getreplacementList;
    }

    public int uploadReplacementOrder(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int uploadReplacementOrder = 0;
        uploadReplacementOrder = wmsDAO.uploadReplacementOrder(wmsTO);
        return uploadReplacementOrder;
    }

    public int saveReplacementOrderUpload(WmsTO wmsTO, int userId) {
        int saveReplacementOrderUpload = 0;
        saveReplacementOrderUpload = wmsDAO.saveReplacementOrderUpload(wmsTO, userId);

        return saveReplacementOrderUpload;
    }

    public ArrayList getreturnList(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getreturnList = new ArrayList();
        getreturnList = wmsDAO.getreturnList(wms);
        return getreturnList;
    }

    public int uploadReturnordertemp(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int uploadReturnordertemp = 0;
        uploadReturnordertemp = wmsDAO.uploadReturnordertemp(wmsTO);
        return uploadReturnordertemp;
    }

    public int updateFkReconcile(WmsTO wmsTO, String[] orderIds) throws FPRuntimeException, FPBusinessException {
        int updateFkReconcile = 0;
        updateFkReconcile = wmsDAO.updateFkReconcile(wmsTO, orderIds);
        return updateFkReconcile;
    }

    public int returnContractUpload(WmsTO wmsTO, int userId) {
        SqlMapClient session = wmsDAO.getSqlMapClient();
        int returnContractUpload = 0;
        returnContractUpload = wmsDAO.returnContractUpload(wmsTO, userId);
        return returnContractUpload;
    }

    public ArrayList ifbLRUploadList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList ifbLRUploadList = wmsDAO.ifbLRUploadList(wmsTO);
        return ifbLRUploadList;
    }

    public ArrayList ifbLRPrintlist(String[] lrIds, String[] selected) throws FPRuntimeException, FPBusinessException {
        ArrayList ifbLRPrintlist = wmsDAO.ifbLRPrintlist(lrIds, selected);
        return ifbLRPrintlist;
    }

    public ArrayList getDealerMaster(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getDealerMaster = wmsDAO.getDealerMaster(wmsTO);
        return getDealerMaster;
    }

    public ArrayList getFkReconcile(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getFkReconcile = wmsDAO.getFkReconcile(wmsTO);
        return getFkReconcile;
    }

    public ArrayList getFkReconcileMaster(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getFkReconcileMaster = wmsDAO.getFkReconcileMaster(wmsTO);
        return getFkReconcileMaster;
    }

    public ArrayList ifbPodDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList ifbPodDetails = wmsDAO.ifbPodDetails(wmsTO);
        return ifbPodDetails;
    }

    public ArrayList ifbPodDetailsDownload(String[] podIds) throws FPRuntimeException, FPBusinessException {
        ArrayList ifbPodDetails = wmsDAO.ifbPodDetailsDownload(podIds);
        return ifbPodDetails;
    }

    public ArrayList getFkCashDeposit(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getFkCashDeposit = wmsDAO.getFkCashDeposit(wmsTO);
        return getFkCashDeposit;
    }

    public int saveIfbPodFile(String podRemarks, String podId, String file, String fileName) throws FPRuntimeException, FPBusinessException {
        int saveIfbPodFile = 0;
        saveIfbPodFile = wmsDAO.saveIfbPodFile(podRemarks, podId, file, fileName);
        return saveIfbPodFile;
    }

    public int approveIfbPod(String podId) throws FPRuntimeException, FPBusinessException {
        int approveIfbPod = 0;
        approveIfbPod = wmsDAO.approveIfbPod(podId);
        return approveIfbPod;
    }

    public int updateCashDeposit(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int updateFkReconcile = 0;
        updateFkReconcile = wmsDAO.updateCashDeposit(wmsTO);
        return updateFkReconcile;
    }

    public ArrayList fkCashDepositDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList fkPodDetails = wmsDAO.fkCashDepositDetails(wmsTO);
        return fkPodDetails;
    }

    public int approveFkDeposit(String depositId) throws FPRuntimeException, FPBusinessException {
        int approveFkPod = 0;
        approveFkPod = wmsDAO.approveFkDeposit(depositId);
        return approveFkPod;
    }

    public int addFkRvpRunsheet(String[] shipmentId, String[] name, String[] address, String[] codAmount, String[] status, String[] date, String[] itemName, int userId) throws FPRuntimeException, FPBusinessException {
        int approveFkPod = 0;
        approveFkPod = wmsDAO.addFkRvpRunsheet(shipmentId, name, address, codAmount, status, date, itemName, userId);
        return approveFkPod;
    }

    public int saveFkDepositFile(String podRemarks, String depositId, String file, String fileName) throws FPRuntimeException, FPBusinessException {
        int saveFkPodFile = 0;
        saveFkPodFile = wmsDAO.saveFkDepositFile(podRemarks, depositId, file, fileName);
        return saveFkPodFile;
    }

    public ArrayList showReconcile(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList ifbPodDetails = wmsDAO.showReconcile(wmsTO);
        return ifbPodDetails;
    }

    public ArrayList getShipmentDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getShipmentDetails = wmsDAO.getShipmentDetails(wmsTO);
        return getShipmentDetails;
    }

    public ArrayList ifbRunsheetReport(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList ifbRunsheetReport = wmsDAO.ifbRunsheetReport(wmsTO);
        return ifbRunsheetReport;
    }

    public ArrayList fkRunsheetReport(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList fkRunsheetReport = wmsDAO.fkRunsheetReport(wmsTO);
        return fkRunsheetReport;
    }

    public ArrayList fkReconcileView(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList fkReconcileView = wmsDAO.fkReconcileView(wmsTO);
        return fkReconcileView;
    }

    public ArrayList ifbShipmentReport(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList ifbShipmentReport = wmsDAO.ifbShipmentReport(wmsTO);
        return ifbShipmentReport;
    }

    public ArrayList getProductTypeList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getProductTypeList = new ArrayList();
        getProductTypeList = wmsDAO.getProductTypeList(wmsTO);
        return getProductTypeList;
    }

    public ArrayList getSelectedProductType(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getSelectedProductType = new ArrayList();
        getSelectedProductType = wmsDAO.getSelectedProductType(wmsTO);
        return getSelectedProductType;
    }

    public ArrayList getHubManagerList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getHubManagerList = new ArrayList();
        getHubManagerList = wmsDAO.getHubManagerList(wmsTO);
        return getHubManagerList;
    }

    public ArrayList fkOrderUploadView(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList fkOrderUploadView = new ArrayList();
        fkOrderUploadView = wmsDAO.fkOrderUploadView(wmsTO);
        return fkOrderUploadView;
    }

    public int updateCycleCountAssign(String[] cycleIds, String[] activeInds) throws FPRuntimeException, FPBusinessException {
        int updateCycleCountAssign = 0;
        updateCycleCountAssign = wmsDAO.updateCycleCountAssign(cycleIds, activeInds);
        return updateCycleCountAssign;
    }

    public int saveCycleCountAssign(String empId, String custId, String day, String[] productType, String[] productTypeIds, String[] selected) throws FPRuntimeException, FPBusinessException {
        int saveCycleCountAssign = 0;
        saveCycleCountAssign = wmsDAO.saveCycleCountAssign(empId, custId, day, productType, productTypeIds, selected);
        return saveCycleCountAssign;
    }

    public int insertDispatchDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int updateFkReconcile = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            updateFkReconcile = wmsDAO.insertDispatchDetails(wmsTO, session);
            if (updateFkReconcile > 0) {
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return updateFkReconcile;
    }

    public ArrayList getSupplierList() throws FPRuntimeException, FPBusinessException {

        ArrayList getWhList = new ArrayList();
        getWhList = wmsDAO.getSupplierList();
        return getWhList;
    }

    public ArrayList getDealerList() throws FPRuntimeException, FPBusinessException {

        ArrayList getWhList = new ArrayList();
        getWhList = wmsDAO.getDealerList();
        return getWhList;
    }

    public ArrayList getSuppwhList() throws FPRuntimeException, FPBusinessException {

        ArrayList getWhList = new ArrayList();
        getWhList = wmsDAO.getSuppwhList();
        return getWhList;
    }

    public ArrayList getVehicleTypeList() throws FPRuntimeException, FPBusinessException {

        ArrayList getWhList = new ArrayList();
        getWhList = wmsDAO.getVehicleTypeList();
        return getWhList;
    }

    public ArrayList getCustInvoiceDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getCustInvoiceDetails = new ArrayList();
        getCustInvoiceDetails = wmsDAO.getCustInvoiceDetails(wmsTO);
        return getCustInvoiceDetails;
    }

    public int insertInvoiceDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int updateFkReconcile = 0;
        updateFkReconcile = wmsDAO.insertInvoiceDetails(wmsTO);
        return updateFkReconcile;
    }

    public int connectUploadInvoice(WmsTO wmsTO, int userId, String whId, String supplyId) {
        int ifbOrderUpload = 0;
        ifbOrderUpload = wmsDAO.connectUploadInvoice(wmsTO, userId, whId, supplyId);
        return ifbOrderUpload;
    }

    public int deleteInvoiceConnectTemp(int userId) {
        int deleteIfbUploadTemp = 0;
        deleteIfbUploadTemp = wmsDAO.deleteInvoiceConnectTemp(userId);
        return deleteIfbUploadTemp;
    }

    public int uploadconnectInvoice(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int uploadIfbOrder = 0;
        uploadIfbOrder = wmsDAO.uploadconnectInvoice(wmsTO);
        return uploadIfbOrder;
    }

    public ArrayList getconnectInvoice(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getconnectInvoice = new ArrayList();
        getconnectInvoice = wmsDAO.getconnectInvoice(wms);
        return getconnectInvoice;
    }

    public ArrayList getDispatchDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getDispatchDetails = new ArrayList();
        getDispatchDetails = wmsDAO.getDispatchDetails(wms);
        return getDispatchDetails;
    }

    public ArrayList getWareHouse(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getWareHouse = new ArrayList();
        getWareHouse = wmsDAO.getWareHouse(wms);
        return getWareHouse;
    }

    public ArrayList getdispatchList(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getdispatchList = new ArrayList();
        getdispatchList = wmsDAO.getdispatchList(wms);
        return getdispatchList;
    }

    public ArrayList getdispatch(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getdispatch = new ArrayList();
        getdispatch = wmsDAO.getdispatch(wms);
        return getdispatch;
    }

    public ArrayList getVechicleDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getVehicleDetails = new ArrayList();
        getVehicleDetails = wmsDAO.getVechicleDetails(wms);
        return getVehicleDetails;
    }

    public int updateDcGenerate(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int updateDcGenerate = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            updateDcGenerate = wmsDAO.updateDcGenerate(wmsTO, session);
            if (updateDcGenerate > 0) {
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return updateDcGenerate;
    }

    public int uploadConnectSrn(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int uploadConnectSrn = 0;
        uploadConnectSrn = wmsDAO.uploadConnectSrn(wmsTO);
        return uploadConnectSrn;
    }

    public ArrayList getconnectlrnumber(String dispatchid) throws FPRuntimeException, FPBusinessException {
        ArrayList getconnectlrnumber = new ArrayList();
        getconnectlrnumber = wmsDAO.getconnectlrnumber(dispatchid);
        return getconnectlrnumber;
    }

    public ArrayList getImLrDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getImLrDetails = new ArrayList();
        getImLrDetails = wmsDAO.getImLrDetails(wms);
        return getImLrDetails;
    }

    public ArrayList getImSubLrDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getImSubLrDetails = new ArrayList();
        getImSubLrDetails = wmsDAO.getImSubLrDetails(wms);
        return getImSubLrDetails;
    }

    public ArrayList invoiceMaster(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceMaster = new ArrayList();
        invoiceMaster = wmsDAO.invoiceMaster(wms);
        return invoiceMaster;
    }

    public ArrayList getconnectsublrnumber(String dispatchid) throws FPRuntimeException, FPBusinessException {
        ArrayList getconnectsublrnumber = new ArrayList();
        getconnectsublrnumber = wmsDAO.getconnectsublrnumber(dispatchid);
        return getconnectsublrnumber;
    }

    public ArrayList getCellList(String supplyId) throws FPRuntimeException, FPBusinessException {
        ArrayList getCellList = new ArrayList();
        getCellList = wmsDAO.getCellList(supplyId);
        return getCellList;
    }

    public ArrayList getStorageDetails() throws FPRuntimeException, FPBusinessException {
        ArrayList getStorageDetails = new ArrayList();
        getStorageDetails = wmsDAO.getStorageDetails();
        return getStorageDetails;
    }

    public ArrayList getbinDetails() throws FPRuntimeException, FPBusinessException {
        ArrayList getbinDetails = new ArrayList();
        getbinDetails = wmsDAO.getBinDetails();
        return getbinDetails;
    }

    public ArrayList getRackDetails2() throws FPRuntimeException, FPBusinessException {
        ArrayList getRackDetails2 = new ArrayList();
        getRackDetails2 = wmsDAO.getRackDetails2();
        return getRackDetails2;
    }

    public ArrayList getBinkDetails2() throws FPRuntimeException, FPBusinessException {
        ArrayList getBinkDetails2 = new ArrayList();
        getBinkDetails2 = wmsDAO.getBinkDetails2();
        return getBinkDetails2;
    }

    public ArrayList getrackDetails() throws FPRuntimeException, FPBusinessException {
        ArrayList getrackDetails = new ArrayList();
        getrackDetails = wmsDAO.getRackDetails();
        return getrackDetails;
    }

    public int insertStoragewareHouse(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int insertStoragewareHouse = 0;
        insertStoragewareHouse = wmsDAO.insertStoragewareHouse(wmsTO);
        return insertStoragewareHouse;
    }

    public int insertStorageMaster(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int insertStorageMaster = 0;
        insertStorageMaster = wmsDAO.insertStorageMaster(wmsTO);
        return insertStorageMaster;
    }

    public int updateStorageMaster(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int updateStorageMaster = 0;
        updateStorageMaster = wmsDAO.updateStorageMaster(wmsTO);
        return updateStorageMaster;
    }

    public ArrayList getStorageWareHouse() throws FPRuntimeException, FPBusinessException {
        ArrayList getStorageWareHouse = new ArrayList();
        getStorageWareHouse = wmsDAO.getStorageWareHouse();
        return getStorageWareHouse;
    }

    public ArrayList getReceivedgrn(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getReceivedgrn = new ArrayList();
        getReceivedgrn = wmsDAO.getReceivedgrn(userId);
        return getReceivedgrn;
    }

    public ArrayList getGrnDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList getGrnDetails = new ArrayList();
        getGrnDetails = wmsDAO.getGrnDetails(wmsTo);
        return getGrnDetails;
    }

    public ArrayList getGrnproductDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList getGrnproductDetails = new ArrayList();
        getGrnproductDetails = wmsDAO.getGrnproductDetails(wmsTo);
        return getGrnproductDetails;
    }

    public String checkconnectSerialNo(String SerialNo, String grnId) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.checkconnectSerialNo(SerialNo, grnId);
        return status;
    }

    public int inserconnectSerialNumber(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int inserconnectSerialNumber = 0;
        inserconnectSerialNumber = wmsDAO.inserconnectSerialNumber(wmsTO);
        return inserconnectSerialNumber;
    }

    public int insertNonScanSerialNumber(String uom, String storageId, String rackId, String binId, String quantity, WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        int insertNonScanSerialNumber = 0;
        insertNonScanSerialNumber = wmsDAO.insertNonScanSerialNumber(uom, storageId, rackId, binId, quantity, wmsTo);
        return insertNonScanSerialNumber;
    }

    public ArrayList getConnectSerialNumberDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getConnectSerialNumberDetails = new ArrayList();
        getConnectSerialNumberDetails = wmsDAO.getConnectSerialNumberDetails(wmsTO);
        return getConnectSerialNumberDetails;
    }

    public int delConnectSerial(String serialNo, String grnId, String itemId) throws FPRuntimeException, FPBusinessException {
        int delConnectSerial = 0;
        delConnectSerial = wmsDAO.delconnectSerialno(serialNo, grnId, itemId);
        return delConnectSerial;
    }

    public int deleteConnectgrnTemp(int userId) {
        int deleteConnectgrnTemp = 0;
        deleteConnectgrnTemp = wmsDAO.deleteConnectgrnTemp(userId);
        return deleteConnectgrnTemp;
    }

    public int uploadconnectGrn(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int uploadconnectGrn = 0;
        uploadconnectGrn = wmsDAO.uploadconnectGrn(wmsTO);
        return uploadconnectGrn;
    }

    public ArrayList getConnectgrnDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getConnectgrnDetails = new ArrayList();
        getConnectgrnDetails = wmsDAO.getConnectgrnDetails(wms);
        return getConnectgrnDetails;
    }

    public int connectGrnUpload(WmsTO wmsTO, int userId, String whId) {
        int connectGrnUpload = 0;
        connectGrnUpload = wmsDAO.connectGrnUpload(wmsTO, userId, whId);
        return connectGrnUpload;
    }

    public ArrayList getgrnView(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getproductMasters = new ArrayList();
        getproductMasters = wmsDAO.getgrnView(userId);
        return getproductMasters;
    }

    public ArrayList getConnectGrnReport(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getproductMasters = new ArrayList();
        getproductMasters = wmsDAO.getConnectGrnReport(wms);
        return getproductMasters;
    }

    public ArrayList getRpOrderMaster(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getRpOrderMaster = new ArrayList();
        getRpOrderMaster = wmsDAO.getRpOrderMaster(wms);
        return getRpOrderMaster;
    }

    public ArrayList rpTripPlanningDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList rpTripPlanningDetails = new ArrayList();
        rpTripPlanningDetails = wmsDAO.rpTripPlanningDetails(wms);
        return rpTripPlanningDetails;
    }

    public ArrayList getRpOrderDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getRpOrderDetails = new ArrayList();
        getRpOrderDetails = wmsDAO.getRpOrderDetails(wms);
        return getRpOrderDetails;
    }

    public int saveGrnView(WmsTO wmsTO, int userId) throws FPRuntimeException, FPBusinessException {
        int saveGrnView = 0;
        saveGrnView = wmsDAO.saveGrnView(wmsTO, userId);
        return saveGrnView;
    }

    public ArrayList getRackList(String loactionId) throws FPRuntimeException, FPBusinessException {
        ArrayList getRackList = new ArrayList();
        getRackList = wmsDAO.getRackList(loactionId);
        return getRackList;
    }

    public ArrayList getbinList(String binId) throws FPRuntimeException, FPBusinessException {
        ArrayList getRackList = new ArrayList();
        getRackList = wmsDAO.getbinList(binId);
        return getRackList;
    }

    public int editconnectSerialNumber(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int editconnectSerialNumber = 0;
        editconnectSerialNumber = wmsDAO.editconnectSerialNumber(wmsTO);
        return editconnectSerialNumber;
    }

    public ArrayList getConnectsubmitSerialNumber(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getConnectsubmitSerialNumber = new ArrayList();
        getConnectsubmitSerialNumber = wmsDAO.getConnectsubmitSerialNumber(userId);
        return getConnectsubmitSerialNumber;
    }

    public ArrayList getConnectPickList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getConnectPickList = new ArrayList();
        getConnectPickList = wmsDAO.getConnectPickList(wmsTO);
        return getConnectPickList;
    }

    public ArrayList getConnectSerialList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getConnectSerialList = new ArrayList();
        getConnectSerialList = wmsDAO.getConnectSerialList(wmsTO);
        return getConnectSerialList;
    }

    public ArrayList getConnectDispatchViewDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getConnectDispatchViewDetails = new ArrayList();
        getConnectDispatchViewDetails = wmsDAO.getConnectDispatchViewDetails(wmsTO);
        return getConnectDispatchViewDetails;
    }

    public int insertSerialNumber(WmsTO wmsTO, int userId, String whId) throws FPRuntimeException, FPBusinessException {
        int saveGrnView = 0;
        saveGrnView = wmsDAO.insertSerialNumber(wmsTO, userId, whId);
        return saveGrnView;
    }
    public int instaSendMail(WmsTO wmsTO, int userId, String whId) throws FPRuntimeException, FPBusinessException {
        int instaSendMail = 0;
        instaSendMail = wmsDAO.instaSendMail(wmsTO, userId, whId);
        return instaSendMail;
    }
    public int instaPoSendMail(WmsTO wmsTO, int userId, String whId) throws FPRuntimeException, FPBusinessException {
        int instaPoSendMail = 0;
        instaPoSendMail = wmsDAO.instaPoSendMail(wmsTO, userId, whId);
        return instaPoSendMail;
    }
    public int instaGrnSendMail(WmsTO wmsTO, int userId, String whId) throws FPRuntimeException, FPBusinessException {
        int instaGrnSendMail = 0;
        instaGrnSendMail = wmsDAO.instaGrnSendMail(wmsTO, userId, whId);
        return instaGrnSendMail;
    }

    public int deleteConnectDispatchInvoice(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int deleteConnectDispatchInvoice = 0;
        deleteConnectDispatchInvoice = wmsDAO.deleteConnectDispatchInvoice(wmsTO);
        return deleteConnectDispatchInvoice;
    }

    public ArrayList getVehicleTatList(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getVehicleTatList = new ArrayList();
        getVehicleTatList = wmsDAO.getVehicleTatList(wms);
        return getVehicleTatList;
    }

    public ArrayList getSerialNumber(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getSerialNumber = new ArrayList();
        getSerialNumber = wmsDAO.getSerialNumber(wms);
        return getSerialNumber;
    }

    public int updateSerialNumber(WmsTO wmsTO) {
        int updateSerialNumber = 0;
        updateSerialNumber = wmsDAO.updateSerialNumber(wmsTO);
        return updateSerialNumber;
    }

    public int insertConnectPicklistMaster(WmsTO wmsTO) {
        int insertConnectPicklistMaster = 0;
        insertConnectPicklistMaster = wmsDAO.insertConnectPicklistMaster(wmsTO);
        return insertConnectPicklistMaster;
    }

    public ArrayList getItemQty(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getItemQty = new ArrayList();
        getItemQty = wmsDAO.getItemQty(wms);
        return getItemQty;
    }

    public ArrayList getConnectPickedSerial(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getConnectPickedSerial = new ArrayList();
        getConnectPickedSerial = wmsDAO.getConnectPickedSerial(wms);
        return getConnectPickedSerial;
    }

    public int updatePicklist(WmsTO wmsTO) {
        int updatePicklist = 0;
        updatePicklist = wmsDAO.updatePicklist(wmsTO);
        return updatePicklist;
    }

    public int clearConnectPicklist(WmsTO wmsTO) {
        int clearConnectPicklist = 0;
        clearConnectPicklist = wmsDAO.clearConnectPicklist(wmsTO);
        return clearConnectPicklist;
    }

    public ArrayList getDispatchList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getDispatchList = wmsDAO.getDispatchList(wmsTO);
        return getDispatchList;
    }

    public ArrayList getSubLrList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getDispatchList = wmsDAO.getSubLrList(wmsTO);
        return getDispatchList;
    }

    public int saveConnectPodFile(String podRemarks, String dispatchId, String file, String fileName, String subLr, String subLrId) throws FPRuntimeException, FPBusinessException {
        int saveConnectPodFile = 0;
        saveConnectPodFile = wmsDAO.saveConnectPodFile(podRemarks, dispatchId, file, fileName, subLr, subLrId);
        return saveConnectPodFile;
    }

    public ArrayList getPodList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getPodList = wmsDAO.getPodList(wmsTO);
        return getPodList;
    }
    public ArrayList getimPodList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getimPodList = wmsDAO.getimPodList(wmsTO);
        return getimPodList;
    }
    
    
  
    public ArrayList viewConnectStockDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList viewConnectStockDetails = wmsDAO.viewConnectStockDetails(wmsTO);
        return viewConnectStockDetails;
    }

    public ArrayList viewConnectSerialNumberDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList viewConnectSerialNumberDetails = wmsDAO.viewConnectSerialNumberDetails(wmsTO);
        return viewConnectSerialNumberDetails;
    }

    public ArrayList checkConnectSerialNo(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList checkConnectSerialNo = wmsDAO.checkConnectSerialNo(wmsTO);
        return checkConnectSerialNo;
    }

    public ArrayList getConnectSerialNoDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getConnectSerialNoDetails = wmsDAO.getConnectSerialNoDetails(wmsTO);
        return getConnectSerialNoDetails;
    }

    public int updateConnectPodApproval(String podId, String status, WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int updateConnectPodApproval = 0;
        updateConnectPodApproval = wmsDAO.updateConnectPodApproval(podId, status, wms);
        return updateConnectPodApproval;
    }

    public int updateConnectPicklist(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int updateConnectPicklist = 0;
        updateConnectPicklist = wmsDAO.updateConnectPicklist(wms);
        return updateConnectPicklist;
    }

    public int checkConnectSerialNoExist(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int checkConnectSerialNoExist = 0;
        checkConnectSerialNoExist = wmsDAO.checkConnectSerialNoExist(wms);
        return checkConnectSerialNoExist;
    }

    public int checkConnectStockDetail(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int checkConnectStockDetail = 0;
        checkConnectStockDetail = wmsDAO.checkConnectStockDetail(wms);
        return checkConnectStockDetail;
    }

    public ArrayList flurencoPickslipList(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList flurencoPickslipList = new ArrayList();
        flurencoPickslipList = wmsDAO.flurencoPickslipList(wms);
        return flurencoPickslipList;
    }

    public ArrayList transferTypeList(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList transferTypeList = new ArrayList();
        transferTypeList = wmsDAO.transferTypeList(wms);
        return transferTypeList;
    }

    public ArrayList flurencoPickslipDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList flurencoPickslipDetails = new ArrayList();
        flurencoPickslipDetails = wmsDAO.flurencoPickslipDetails(wms);
        return flurencoPickslipDetails;
    }

    public int updateFlurencoPickUpDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int updateFlurencoPickUpDetails = 0;
        updateFlurencoPickUpDetails = wmsDAO.updateFlurencoPickUpDetails(wms);
        return updateFlurencoPickUpDetails;
    }

    public int connectOpenPackSerialSave(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int connectOpenPackSerialSave = 0;
        connectOpenPackSerialSave = wmsDAO.connectOpenPackSerialSave(wms);
        return connectOpenPackSerialSave;
    }

    public int insertConnectPicklistDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int insertConnectPicklistDetails = 0;
        insertConnectPicklistDetails = wmsDAO.insertConnectPicklistDetails(wms);
        return insertConnectPicklistDetails;
    }

    public ArrayList dcFreightDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList dcFreightDetails = new ArrayList();
        dcFreightDetails = wmsDAO.dcFreightDetails(wms);
        return dcFreightDetails;
    }

    public ArrayList docketNoUpdateDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList docketNoUpdateDetails = new ArrayList();
        docketNoUpdateDetails = wmsDAO.docketNoUpdateDetails(wms);
        return docketNoUpdateDetails;
    }

    public ArrayList connectLrView(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList connectLrView = new ArrayList();
        connectLrView = wmsDAO.connectLrView(wms);
        return connectLrView;
    }

    public ArrayList podUploadDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList podUploadDetails = new ArrayList();
        podUploadDetails = wmsDAO.podUploadDetails(wms);
        return podUploadDetails;
    }

    public ArrayList getConnectSrnTemp(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getConnectSrn = new ArrayList();
        getConnectSrn = wmsDAO.getConnectSrnTemp(wms);
        return getConnectSrn;
    }

    public ArrayList getConnectSrn(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getConnectSrn = new ArrayList();
        getConnectSrn = wmsDAO.getConnectSrn(wms);
        return getConnectSrn;
    }

    public ArrayList getConnectInvoiceReport(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getConnectInvoiceReport = new ArrayList();
        getConnectInvoiceReport = wmsDAO.getConnectInvoiceReport(wms);
        return getConnectInvoiceReport;
    }

    public String getConnectDeliveryDate(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        String getConnectDeliveryDate = "";
        getConnectDeliveryDate = wmsDAO.getConnectDeliveryDate(wms);
        return getConnectDeliveryDate;
    }

    public ArrayList getConnectInvoiceProductDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getConnectInvoiceProductDetails = new ArrayList();
        getConnectInvoiceProductDetails = wmsDAO.getConnectInvoiceProductDetails(wms);
        return getConnectInvoiceProductDetails;
    }

    public ArrayList connectGetBillReceiving(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList connectGetBillReceiving = new ArrayList();
        connectGetBillReceiving = wmsDAO.connectGetBillReceiving(wms);
        return connectGetBillReceiving;
    }

    public ArrayList rpStockDetailsView(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList rpStockDetailsView = new ArrayList();
        rpStockDetailsView = wmsDAO.rpStockDetailsView(wms);
        return rpStockDetailsView;
    }

    public ArrayList rpStockDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList rpStockDetails = new ArrayList();
        rpStockDetails = wmsDAO.rpStockDetails(wms);
        return rpStockDetails;
    }

    public ArrayList rpStockTransferView(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList rpStockTransferView = new ArrayList();
        rpStockTransferView = wmsDAO.rpStockTransferView(wms);
        return rpStockTransferView;
    }

    public ArrayList rpStockTransfer(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList rpStockTransfer = new ArrayList();
        rpStockTransfer = wmsDAO.rpStockTransfer(wms);
        return rpStockTransfer;
    }

    public ArrayList rpStockReceive(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList rpStockReceive = new ArrayList();
        rpStockReceive = wmsDAO.rpStockReceive(wms);
        return rpStockReceive;
    }

    public ArrayList connectIndentRequest(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList connectIndentRequest = new ArrayList();
        connectIndentRequest = wmsDAO.connectIndentRequest(wms);
        return connectIndentRequest;
    }

    public ArrayList connectIndentRequestDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList connectIndentRequestDetails = new ArrayList();
        connectIndentRequestDetails = wmsDAO.connectIndentRequestDetails(wms);
        return connectIndentRequestDetails;
    }

    public ArrayList getRpProductList(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getRpProductList = new ArrayList();
        getRpProductList = wmsDAO.getRpProductList(wms);
        return getRpProductList;
    }

    public ArrayList imMaterialMasterDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList imMaterialMasterDetails = new ArrayList();
        imMaterialMasterDetails = wmsDAO.imMaterialMasterDetails(wms);
        return imMaterialMasterDetails;
    }

    public ArrayList imMaterialGrnMaster(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList imMaterialGrnMaster = new ArrayList();
        imMaterialGrnMaster = wmsDAO.imMaterialGrnMaster(wms);
        return imMaterialGrnMaster;
    }

    public ArrayList imMaterialGrnDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList imMaterialGrnDetails = new ArrayList();
        imMaterialGrnDetails = wmsDAO.imMaterialGrnDetails(wms);
        return imMaterialGrnDetails;
    }

    public ArrayList imMaterialCustomerMaster(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList imMaterialCustomerMaster = new ArrayList();
        imMaterialCustomerMaster = wmsDAO.imMaterialCustomerMaster(wms);
        return imMaterialCustomerMaster;
    }
    public ArrayList imMaterialIndent(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList imMaterialIndent = new ArrayList();
        imMaterialIndent = wmsDAO.imMaterialIndent(wms);
        return imMaterialIndent;
    }

    public int updateFreightAmount(int userId, String[] check, String[] lrnNo, String[] freightAmt, String[] haltingAmt, String[] unloadingAmt, String[] otherAmt, String[] totalAmt, String[] remarks) throws FPRuntimeException, FPBusinessException {
        int updateFreightAmount = 0;
        updateFreightAmount = wmsDAO.updateFreightAmount(userId, check, lrnNo, freightAmt, haltingAmt, unloadingAmt, otherAmt, totalAmt, remarks);
        return updateFreightAmount;
    }

    public int updateDocketnumber(String[] check, String[] lrnNo, String[] docketNo) throws FPRuntimeException, FPBusinessException {
        int updateDocketnumber = 0;
        updateDocketnumber = wmsDAO.updateDocketnumber(check, lrnNo, docketNo);
        return updateDocketnumber;
    }

    public int uploadPOD(String file, String saveFile, String check, String lrnNo) throws FPRuntimeException, FPBusinessException {
        int uploadPOD = 0;
        uploadPOD = wmsDAO.uploadPOD(file, saveFile, check, lrnNo);
        return uploadPOD;
    }

    public int uploadPODFiles(String file, String saveFile, String lrnNo, String subLrId, String dispatchid, String plannedTime) throws FPRuntimeException, FPBusinessException {
        int uploadPODFiles = 0;
        uploadPODFiles = wmsDAO.uploadPODFiles(file, saveFile, lrnNo, subLrId, dispatchid, plannedTime);
        return uploadPODFiles;
    }

    public int processInsertPODDetails(WmsTO wmsTO, String index, int UserId, String[] actualFilePath, String[] fileSaved, String[] remarks) throws FPRuntimeException, FPBusinessException {
        //    public int processInsertVendorDetails(VendorTO vendorTO,String[] mfrids,String index,int UserId) throws FPRuntimeException, FPBusinessException {
        int vendorId = 0;
        vendorId = wmsDAO.processInsertPODDetails(wmsTO, UserId, actualFilePath, fileSaved, remarks);

        return vendorId;
    }

    public int updateConnectInvoiceUpdate(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int updateConnectInvoiceUpdate = 0;
        updateConnectInvoiceUpdate = wmsDAO.updateConnectInvoiceUpdate(wmsTO);
        return updateConnectInvoiceUpdate;
    }

    public int cancelConnectInvoice(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int cancelConnectInvoice = 0;
        cancelConnectInvoice = wmsDAO.cancelConnectInvoice(wmsTO);
        return cancelConnectInvoice;
    }

    public int connectSaveBillReceiving(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int connectSaveBillReceiving = 0;
        connectSaveBillReceiving = wmsDAO.connectSaveBillReceiving(wmsTO);
        return connectSaveBillReceiving;
    }

    public int saveIndentRequest(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int saveIndentRequest = 0;
        saveIndentRequest = wmsDAO.saveIndentRequest(wmsTO);
        return saveIndentRequest;
    }

    public int insertConnectIndentDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int insertConnectIndentDetails = 0;
        insertConnectIndentDetails = wmsDAO.insertConnectIndentDetails(wmsTO);
        return insertConnectIndentDetails;
    }

    public int checkConnectExistingVehicleTat(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int checkConnectExistingVehicleTat = 0;
        checkConnectExistingVehicleTat = wmsDAO.checkConnectExistingVehicleTat(wmsTO);
        return checkConnectExistingVehicleTat;
    }

    public int saveRpStockInward(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int saveRpStockInward = 0;
        saveRpStockInward = wmsDAO.saveRpStockInward(wmsTO);
        return saveRpStockInward;
    }

    public int insertRpStockTransferDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int insertRpStockTransferDetails = 0;
        insertRpStockTransferDetails = wmsDAO.insertRpStockTransferDetails(wmsTO);
        return insertRpStockTransferDetails;
    }

    public int insertImMaterialMaster(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int insertImMaterialMaster = 0;
        insertImMaterialMaster = wmsDAO.insertImMaterialMaster(wmsTO);
        return insertImMaterialMaster;
    }

    public int saveStockReceive(WmsTO wmsTO, String[] detailId) throws FPRuntimeException, FPBusinessException {
        int saveStockReceive = 0;
        saveStockReceive = wmsDAO.saveStockReceive(wmsTO, detailId);
        return saveStockReceive;
    }

    public int updateConnectIndentRequestDetails(WmsTO wms, String[] tatId, String[] indentDetailId, String[] inDate, String[] inTime, String[] selectedStatus) throws FPRuntimeException, FPBusinessException {
        int updateConnectIndentRequestDetails = 0;
        updateConnectIndentRequestDetails = wmsDAO.updateConnectIndentRequestDetails(wms, tatId, indentDetailId, inDate, inTime, selectedStatus);
        return updateConnectIndentRequestDetails;
    }

    public int updateConnectDeliveryDate(String[] lrnNo, String[] deliveryDate, String[] remarks, WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int updateConnectDeliveryDate = 0;
        updateConnectDeliveryDate = wmsDAO.updateConnectDeliveryDate(lrnNo, deliveryDate, remarks, wmsTO);
        return updateConnectDeliveryDate;
    }

    public int insertImMaterialCustomerMaster(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int insertImMaterialCustomerMaster = 0;
        insertImMaterialCustomerMaster = wmsDAO.insertImMaterialCustomerMaster(wmsTO);
        return insertImMaterialCustomerMaster;
    }

    public int insertImPoOrder(WmsTO wms, String[] materialName, String[] materialDescription, String[] poQty , String[] pending , String[] id ,String[] uom ,String[] unitPrice , String[] price) throws FPRuntimeException, FPBusinessException {
        int insertImPoOrder = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            insertImPoOrder = wmsDAO.insertImPoOrder(wms, materialName, materialDescription, poQty ,pending,id, session , uom , unitPrice , price);
            if (insertImPoOrder > 0) {
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return insertImPoOrder;
    }

    public int insertImMaterialGrn(WmsTO wms, String[] poQty, String[] qty, String[] poStatus, String[] poId ,String[] id , String[] good , String[] damage , String[] shortage , String[] balance) throws FPRuntimeException, FPBusinessException {
        int insertImMaterialGrn = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            insertImMaterialGrn = wmsDAO.insertImMaterialGrn(wms, poQty, qty, poStatus, poId, id ,good , damage , shortage , balance ,session);
            if (insertImMaterialGrn > 0) {
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return insertImMaterialGrn;
    }

    public ArrayList getConnectInvoiceExcelTemplate() throws FPRuntimeException, FPBusinessException {
        ArrayList getConnectInvoiceExcelTemplate = new ArrayList();
        getConnectInvoiceExcelTemplate = wmsDAO.getConnectInvoiceExcelTemplate();
        return getConnectInvoiceExcelTemplate;
    }

    public ArrayList getImMaterialCustomerMaster(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getImMaterialCustomerMaster = new ArrayList();
        getImMaterialCustomerMaster = wmsDAO.getImMaterialCustomerMaster(wms);
        return getImMaterialCustomerMaster;
    }

    public ArrayList imMaterialPOView(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList imMaterialPOView = new ArrayList();
        imMaterialPOView = wmsDAO.imMaterialPOView(wms);
        return imMaterialPOView;
    }

    public ArrayList getImMaterialPOViewDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getImMaterialPOViewDetails = new ArrayList();
        getImMaterialPOViewDetails = wmsDAO.getImMaterialPOViewDetails(wms);
        return getImMaterialPOViewDetails;
    }

    public ArrayList getImPOMaterialList(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getImPOMaterialList = new ArrayList();
        getImPOMaterialList = wmsDAO.getImPOMaterialList(wms);
        return getImPOMaterialList;
    }

    public int insertDeTat(WmsTO wmsTO) {
        int insertDeTat = 0;
        insertDeTat = wmsDAO.insertDeTat(wmsTO);
        return insertDeTat;
    }

    public int updateDeTat(WmsTO wmsTO) {
        int updateDeTat = 0;
        updateDeTat = wmsDAO.updateDeTat(wmsTO);
        return updateDeTat;
    }

    public ArrayList deliveryExecutiveDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList deliveryExecutiveDetails = new ArrayList();
        deliveryExecutiveDetails = wmsDAO.deliveryExecutiveDetails(wms);
        return deliveryExecutiveDetails;
    }

    public int updateConnectLrVendor(WmsTO wmsTO) {
        int updateConnectLrVendor = 0;
        updateConnectLrVendor = wmsDAO.updateConnectLrVendor(wmsTO);
        return updateConnectLrVendor;
    }

    public String binNameExists(String binName) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.binNameAlreadyExists(binName);
        return status;
    }

    public String rackNameAlreadyExists(String rackName) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.rackNameAlreadyExists(rackName);
        return status;
    }

    public String storageNameAlreadyExists(String storageName) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.storageNameAlreadyExists(storageName);
        return status;
    }

    public ArrayList getCityList() throws FPRuntimeException, FPBusinessException {
        ArrayList getCityList = new ArrayList();
        getCityList = wmsDAO.getCityList();
        return getCityList;
    }

    public ArrayList getImMaterialIssueMaster(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getImMaterialIssueMaster = new ArrayList();
        getImMaterialIssueMaster = wmsDAO.getImMaterialIssueMaster(wms);
        return getImMaterialIssueMaster;
    }

    public ArrayList getImMaterialIssueDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getImMaterialIssueDetails = new ArrayList();
        getImMaterialIssueDetails = wmsDAO.getImMaterialIssueDetails(wms);
        return getImMaterialIssueDetails;
    }

    public ArrayList getImMaterialGroup(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getImMaterialGroup = new ArrayList();
        getImMaterialGroup = wmsDAO.getImMaterialGroup(wms);
        return getImMaterialGroup;
    }

    public ArrayList getImMaterialUom(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getImMaterialUom = new ArrayList();
        getImMaterialUom = wmsDAO.getImMaterialUom(wms);
        return getImMaterialUom;
    }

    public ArrayList imMaterialStockView(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList imMaterialStockView = new ArrayList();
        imMaterialStockView = wmsDAO.imMaterialStockView(wms);
        return imMaterialStockView;
    }

    public int updateImGrnIssue(String[] floorQty, String[] usedQty, String[] excess, String[] shortage, String[] status, String[] detailId, String[] floorPackQty, String[] materialIds, WmsTO wms,int userId) throws FPRuntimeException, FPBusinessException {
        int update = 0;
        update = wmsDAO.updateImGrnIssue(floorQty, usedQty, excess, shortage, status, detailId, floorPackQty, materialIds, wms,userId);
        return update;
    }

    public int insertImMaterialGrnIssue(WmsTO wms, String[] skuCode, String[] uom, String[] stockQty, String[] issueQty) throws FPRuntimeException, FPBusinessException {
        int insertImMaterialGrnIssue = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            insertImMaterialGrnIssue = wmsDAO.insertImMaterialGrnIssue(wms, skuCode, uom, stockQty, issueQty, session);
            if (insertImMaterialGrnIssue > 0) {
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return insertImMaterialGrnIssue;
    }

    public int getInstaMart(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int getInstaMart = 0;
        getInstaMart = wmsDAO.getInstaMart(wmsTO);
        return getInstaMart;
    }

    public ArrayList getInstaMartProduct(WmsTO wmsTO) throws FPRuntimeException {
        ArrayList getInstaMartProduct = new ArrayList();
        getInstaMartProduct = wmsDAO.getInstaMartProduct(wmsTO);
        return getInstaMartProduct;
    }

    public int insertImProductMaster(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        int insertImProductMaster = 0;
        insertImProductMaster = wmsDAO.insertImProductMaster(wms);
        return insertImProductMaster;
    }

    public ArrayList getImProductMasterTemp(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getImProductMasterTemp = new ArrayList();
        getImProductMasterTemp = wmsDAO.getImProductMasterTemp(wms);
        return getImProductMasterTemp;
    }

    public int imProductMasterToMainTable(WmsTO wmsTO, int userId, String whId) {
        int imProductMasterToMainTable = 0;
        imProductMasterToMainTable = wmsDAO.imProductMasterToMainTable(wmsTO, userId, whId);
        return imProductMasterToMainTable;
    }

    public ArrayList vehicleUtilizationCust(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleUtilizationCust = new ArrayList();
        vehicleUtilizationCust = wmsDAO.vehicleUtilizationCust(wms);
        return vehicleUtilizationCust;
    }

    public ArrayList getTripDetailsReport(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getTripDetailsReport = new ArrayList();
        getTripDetailsReport = wmsDAO.getTripDetailsReport(wms);
        return getTripDetailsReport;
    }

    public ArrayList designationName(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList designationName = new ArrayList();
        designationName = wmsDAO.designationName(wms);
        return designationName;
    }

    public ArrayList getEmployeeId(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList getEmployeeId = new ArrayList();
        getEmployeeId = wmsDAO.getEmployeeId(wmsTo);
        return getEmployeeId;
    }

    public ArrayList getImVendorList(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getImVendorList = new ArrayList();
        getImVendorList = wmsDAO.getImVendorList(wmsTo);
        return getImVendorList;
    }

    public ArrayList getImSkuDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getImSkuDetails = new ArrayList();
        getImSkuDetails = wmsDAO.getImSkuDetails(wmsTo);
        return getImSkuDetails;
    }

    public ArrayList getImChildSkuDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getImChildSkuDetails = new ArrayList();
        getImChildSkuDetails = wmsDAO.getImChildSkuDetails(wmsTo);
        return getImChildSkuDetails;
    }

    public ArrayList getImGrnInbound(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getImGrnInbound = new ArrayList();
        getImGrnInbound = wmsDAO.getImGrnInbound(wmsTo);
        return getImGrnInbound;
    }

    public ArrayList getImGrnInboundDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getImGrnInboundDetails = new ArrayList();
        getImGrnInboundDetails = wmsDAO.getImGrnInboundDetails(wmsTo);
        return getImGrnInboundDetails;
    }

    public ArrayList getImPackagingMaster(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getImPackagingMaster = new ArrayList();
        getImPackagingMaster = wmsDAO.getImPackagingMaster(wmsTo);
        return getImPackagingMaster;
    }

    public ArrayList getImPackagingDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getImPackagingDetails = new ArrayList();
        getImPackagingDetails = wmsDAO.getImPackagingDetails(wmsTo);
        return getImPackagingDetails;
    }

    public ArrayList getImDealerList(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList getImDealerList = new ArrayList();
        getImDealerList = wmsDAO.getImDealerList(wmsTo);
        return getImDealerList;
    }

    public ArrayList getImInvoiceMaster(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getImInvoiceMaster = new ArrayList();
        getImInvoiceMaster = wmsDAO.getImInvoiceMaster(wmsTo);
        return getImInvoiceMaster;
    }

    public ArrayList getImGetCrateScanned(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getImGetCrateScanned = new ArrayList();
        getImGetCrateScanned = wmsDAO.getImGetCrateScanned(wmsTo);
        return getImGetCrateScanned;
    }

    public int deleteImCrateScanned(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        int deleteImCrateScanned = 0;
        deleteImCrateScanned = wmsDAO.deleteImCrateScanned(wmsTo);
        return deleteImCrateScanned;
    }

    public ArrayList getImInvoiceDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getImInvoiceDetails = new ArrayList();
        getImInvoiceDetails = wmsDAO.getImInvoiceDetails(wmsTo);
        return getImInvoiceDetails;
    }

    public int imInvoiceUpload(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
        int imInvoiceUpload = 0;
        imInvoiceUpload = wmsDAO.imInvoiceUpload(wmsTo, userId);
        return imInvoiceUpload;
    }

    public int insertImDealerMaster(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int insertImDealerMaster = 0;
        insertImDealerMaster = wmsDAO.insertImDealerMaster(wmsTO);
        return insertImDealerMaster;
    }

    public ArrayList getImDealerMaster(WmsTO wmsTO) throws FPRuntimeException {
        ArrayList getImDealerMaster = new ArrayList();
        getImDealerMaster = wmsDAO.getImDealerMaster(wmsTO);
        return getImDealerMaster;
    }

    public int insertImVendorMaster(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int insertImVendorMaster = 0;
        insertImVendorMaster = wmsDAO.insertImVendorMaster(wmsTO);
        return insertImVendorMaster;
    }

    public int updateConnectTranshipment(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int updateConnectTranshipment = 0;
        updateConnectTranshipment = wmsDAO.updateConnectTranshipment(wmsTO);
        return updateConnectTranshipment;
    }

    public ArrayList imVendorMasterDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList imMaterialMasterDetails = new ArrayList();
        imMaterialMasterDetails = wmsDAO.imVendorMasterDetails(wms);
        return imMaterialMasterDetails;
    }

    public int updateImGrnInboundDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int updateImGrnInboundDetails = 0;
        updateImGrnInboundDetails = wmsDAO.updateImGrnInboundDetails(wmsTO);
        return updateImGrnInboundDetails;
    }

    public int saveImGrnInbound(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int saveImGrnInbound = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            saveImGrnInbound = wmsDAO.saveImGrnInbound(wmsTO, session);
            session.commitTransaction();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return saveImGrnInbound;
    }

    public ArrayList imCratesReport(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList cratesReport = new ArrayList();
        cratesReport = wmsDAO.imCratesReport(wmsTo);
        return cratesReport;
    }

    public ArrayList imCratesReportView(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList cratesReportView = new ArrayList();
        cratesReportView = wmsDAO.imCratesReportView(wms);
        return cratesReportView;
    }

    public int saveImPackagingDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int saveImPackagingDetails = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            saveImPackagingDetails = wmsDAO.saveImPackagingDetails(wmsTO, session);
            session.commitTransaction();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return saveImPackagingDetails;
    }

    public ArrayList getImMachineList(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getImMachineList = new ArrayList();
        getImMachineList = wmsDAO.getImMachineList(wms);
        return getImMachineList;
    }

    public int updateFreightAmountNew(int userId, String[] check, String[] lrnNo, String[] lrNo, String[] vendorName, String[] customerName, String[] dispatchQuantity, String[] billQuantity, String[] freightAmt, String[] otherAmt, String[] totalAmt) throws FPRuntimeException, FPBusinessException {
        int updateFreightAmountnew = 0;
        updateFreightAmountnew = wmsDAO.updateFreightAmountNew(userId, check, lrnNo, lrNo, vendorName, customerName, dispatchQuantity, billQuantity, freightAmt, otherAmt, totalAmt);
        return updateFreightAmountnew;
    }

    public ArrayList imFreightDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList imFreightDetails = new ArrayList();
        imFreightDetails = wmsDAO.imFreightDetails(wms);
        return imFreightDetails;
    }

    public int dzProductUpload(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int dzProductUpload = 0;
        dzProductUpload = wmsDAO.dzProductUpload(wms);
        return dzProductUpload;
    }

    public ArrayList getdzProductUploadTemp(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getdzProductUploadTemp = new ArrayList();
        getdzProductUploadTemp = wmsDAO.getdzProductUploadTemp(wms);
        return getdzProductUploadTemp;
    }

    public int delDzProductUploadTemp(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int delDzProductUploadTemp = 0;
        delDzProductUploadTemp = wmsDAO.delDzProductUploadTemp(wms);
        return delDzProductUploadTemp;
    }

    public int dzInsertIntoProductMaster(WmsTO wmsTO) {
        int dzInsertIntoProductMaster = 0;
        dzInsertIntoProductMaster = wmsDAO.dzInsertIntoProductMaster(wmsTO);
        return dzInsertIntoProductMaster;
    }

    public int getSaveEditPage(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int getSaveEditPage = 0;
        getSaveEditPage = wmsDAO.getSaveEditPage(wms);
        return getSaveEditPage;
    }

    public ArrayList getdzProductUpload(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getdzProductUpload = new ArrayList();
        getdzProductUpload = wmsDAO.getdzProductUpload(wms);
        return getdzProductUpload;
    }

    public int delDzGrnDetailsTemp(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int delDzGrnDetailsTemp = 0;
        delDzGrnDetailsTemp = wmsDAO.delDzGrnDetailsTemp(wms);
        return delDzGrnDetailsTemp;
    }

    public int delDzInvoiceDetailsTemp(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int delDzInvoiceDetailsTemp = 0;
        delDzInvoiceDetailsTemp = wmsDAO.delDzInvoiceDetailsTemp(wms);
        return delDzInvoiceDetailsTemp;
    }

    public int dzGrnUpload(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int dzGrnUpload = 0;
        dzGrnUpload = wmsDAO.dzGrnUpload(wms);
        return dzGrnUpload;
    }

    public ArrayList getdzGrnDetailsTemp(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getdzGrnDetailsTemp = new ArrayList();
        getdzGrnDetailsTemp = wmsDAO.getdzGrnDetailsTemp(wms);
        return getdzGrnDetailsTemp;
    }

    public int insertDzGrnUpload(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int dzGrnUpload = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        dzGrnUpload = wmsDAO.insertDzGrnUpload(wms, session);
        return dzGrnUpload;
    }

    public ArrayList getdzGrnUpload(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getdzGrnUpload = new ArrayList();
        getdzGrnUpload = wmsDAO.getdzGrnUpload(wms);
        return getdzGrnUpload;
    }

    public ArrayList getdzInvoiceUpload(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getdzInvoiceUpload = new ArrayList();
        getdzInvoiceUpload = wmsDAO.getdzInvoiceUpload(wms);
        return getdzInvoiceUpload;
    }

    public int dzInvoiceUploadExcel(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int dzInvoiceUploadExcel = 0;

        dzInvoiceUploadExcel = wmsDAO.dzInvoiceUploadExcel(wms);
        return dzInvoiceUploadExcel;
    }

    public int dzInvoiceUpload(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int dzInvoiceUpload = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            dzInvoiceUpload = wmsDAO.dzInvoiceUpload(wms, session);
            session.commitTransaction();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return dzInvoiceUpload;
    }

    public ArrayList getDzInvoiceTemp(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getDzInvoiceTemp = new ArrayList();
        getDzInvoiceTemp = wmsDAO.getDzInvoiceTemp(wms);
        return getDzInvoiceTemp;
    }

    public ArrayList getDzGrnDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getDzGrnDetails = new ArrayList();
        getDzGrnDetails = wmsDAO.getDzGrnDetails(wms);
        return getDzGrnDetails;
    }

    public ArrayList getDzStockDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getDzStockDetails = new ArrayList();
        getDzStockDetails = wmsDAO.getDzStockDetails(wms);
        return getDzStockDetails;
    }

    public int insertDailyOutward(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int insertDailyOutward = 0;
        insertDailyOutward = wmsDAO.insertDailyOutward(wmsTO);
        return insertDailyOutward;
    }

    public ArrayList getDailyOutWard(WmsTO wmsTO) throws FPRuntimeException {
        ArrayList getDailyOutWard = new ArrayList();
        getDailyOutWard = wmsDAO.getDailyOutWard(wmsTO);
        return getDailyOutWard;
    }

    public ArrayList imDailyLUDetails(WmsTO wmsTO) throws FPRuntimeException {
        ArrayList imDailyLUDetails = new ArrayList();
        imDailyLUDetails = wmsDAO.imDailyLUDetails(wmsTO);
        return imDailyLUDetails;
    }

    public int insertimDailyLUDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int insertimDailyLUDetails = 0;
        insertimDailyLUDetails = wmsDAO.insertimDailyLUDetails(wmsTO);
        return insertimDailyLUDetails;
    }

    public ArrayList dzPickListMaster(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList dzPickListMaster = new ArrayList();
        dzPickListMaster = wmsDAO.dzPickListMaster(wms);
        return dzPickListMaster;
    }
    
      public ArrayList dzPickListMasterPick(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList dzPickListMasterPick = new ArrayList();
        dzPickListMasterPick = wmsDAO.dzPickListMasterPick(wms);
        return dzPickListMasterPick;
    }


    public ArrayList dzPickListMasterView(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList dzPickListMasterView = new ArrayList();
        dzPickListMasterView = wmsDAO.dzPickListMasterView(wms);
        return dzPickListMasterView;
    }

    public ArrayList getDzLrInvoiceDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getDzLrInvoiceDetails = new ArrayList();
        getDzLrInvoiceDetails = wmsDAO.getDzLrInvoiceDetails(wms);
        return getDzLrInvoiceDetails;
    }

    public ArrayList getAllDzGrnDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getAllDzGrnDetails = new ArrayList();
        getAllDzGrnDetails = wmsDAO.getAllDzGrnDetails(wms);
        return getAllDzGrnDetails;
    }

    public ArrayList getCratesReportDealerBased(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getCratesReportDealerBased = new ArrayList();
        getCratesReportDealerBased = wmsDAO.getCratesReportDealerBased(wms);
        return getCratesReportDealerBased;
    }

    public int insertDZLrStatus(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int insertDZLrStatus = 0;
         SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            insertDZLrStatus = wmsDAO.insertDZLrStatus(wmsTO, session);
            session.commitTransaction();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return insertDZLrStatus;
    }

    public ArrayList getDzLrMaster(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getDzLrMaster = new ArrayList();
        getDzLrMaster = wmsDAO.getDzLrMaster(wms);
        return getDzLrMaster;
    }

    public ArrayList getDzInvoiceLrDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getDzInvoiceLrDetails = new ArrayList();
        getDzInvoiceLrDetails = wmsDAO.getDzInvoiceLrDetails(wmsTo);
        return getDzInvoiceLrDetails;
    }
    public ArrayList getTempCurrentDataCrate(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getTempCurrentDataCrate = new ArrayList();
        getTempCurrentDataCrate = wmsDAO.getTempCurrentDataCrate(wmsTo);
        return getTempCurrentDataCrate;
    }
    public ArrayList getInventoryExportData(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getInventoryExportData = new ArrayList();
        getInventoryExportData = wmsDAO.getInventoryExportData(wmsTo);
        return getInventoryExportData;
    }    

    public ArrayList getDzLrDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getDzLrDetails = new ArrayList();
        getDzLrDetails = wmsDAO.getDzLrDetails(wms);
        return getDzLrDetails;
    }

    public ArrayList dzSubLrDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList dzSubLrDetails = new ArrayList();
        dzSubLrDetails = wmsDAO.dzSubLrDetails(wms);
        return dzSubLrDetails;
    }

    public ArrayList dzCratesReportView(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList dzCratesReportView = new ArrayList();
        dzCratesReportView = wmsDAO.dzCratesReportView(wms);
        return dzCratesReportView;
    }

    public ArrayList dzCratesReport(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList dzCratesReport = new ArrayList();
        dzCratesReport = wmsDAO.dzCratesReport(wmsTo);
        return dzCratesReport;
    }

    public ArrayList getImPodDeatails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getImPodDeatails = new ArrayList();
        getImPodDeatails = wmsDAO.getImPodDeatails(wms);
        return getImPodDeatails;
    }

    public ArrayList dzFreightDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList dzFreightDetails = new ArrayList();
        dzFreightDetails = wmsDAO.dzFreightDetails(wms);
        return dzFreightDetails;
    }

    public int dzUpdateFreightAmountNew(int userId, String[] check, String[] lrnNo, String[] lrNo, String[] vendorName, String[] customerName, String[] dispatchQuantity, String[] billQuantity, String[] freightAmt, String[] otherAmt, String[] totalAmt) throws FPRuntimeException, FPBusinessException {
        int dzUpdateFreightAmountNew = 0;
        dzUpdateFreightAmountNew = wmsDAO.dzUpdateFreightAmountNew(userId, check, lrnNo, lrNo, vendorName, customerName, dispatchQuantity, billQuantity, freightAmt, otherAmt, totalAmt);
        return dzUpdateFreightAmountNew;
    }
    
   public ArrayList imPodUploadDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList imPodUploadDetails = new ArrayList();
        imPodUploadDetails = wmsDAO.imPodUploadDetails(wms);
        return imPodUploadDetails;
    }    
   
    public int processInsertImPODDetails(WmsTO wmsTO, String index, int UserId, String[] actualFilePath, String[] fileSaved, String[] remarks) throws FPRuntimeException, FPBusinessException {
        //    public int processInsertVendorDetails(VendorTO vendorTO,String[] mfrids,String index,int UserId) throws FPRuntimeException, FPBusinessException {
        int vendorId = 0;
        vendorId = wmsDAO.processInsertImPODDetails(wmsTO, UserId, actualFilePath, fileSaved, remarks);

        return vendorId;
    }    
    
    public ArrayList getDzInvoiceTempCount(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getDzInvoiceTempCount = new ArrayList();
        getDzInvoiceTempCount = wmsDAO.getDzInvoiceTempCount(wms);
        return getDzInvoiceTempCount;
    }
    
    public ArrayList crateInventoryCount(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList crateInventoryCount = new ArrayList();
        crateInventoryCount = wmsDAO.crateInventoryCount(wms);
        return crateInventoryCount;
    }

    public ArrayList crateInventoryGet(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList crateInventoryGet = new ArrayList();
        crateInventoryGet = wmsDAO.crateInventoryGet(wms);
        return crateInventoryGet;
    }    
    public ArrayList imIndentGet(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList imIndentGet = new ArrayList();
        imIndentGet = wmsDAO.imIndentGet(wms);
        return imIndentGet;
    }    
    public int crateInventorySet(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        int insertConnectBillUpload = 0;
        insertConnectBillUpload = wmsDAO.insertConnectBillUpload(wmsTo);
        return insertConnectBillUpload;
    }    
    public int insertCrateInventoryMaster(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int insertCrateInventoryMaster = 0;
        insertCrateInventoryMaster = wmsDAO.insertCrateInventoryMaster(wms);
        return insertCrateInventoryMaster;
    }
    public ArrayList getCrateDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getCrateDetails = new ArrayList();
        getCrateDetails = wmsDAO.getCrateDetails(wms);
        return getCrateDetails;
    }
    public ArrayList getCrateDetailsReport(WmsTO wms,String fromDate,String toDate) throws FPRuntimeException, FPBusinessException {
        ArrayList getCrateDetailsReport = new ArrayList();
        getCrateDetailsReport = wmsDAO.getCrateDetailsReport(wms,fromDate,toDate);
        return getCrateDetailsReport;
    }
    public ArrayList getCrateDetailsReportPer(WmsTO wms,String fromDate,String toDate) throws FPRuntimeException, FPBusinessException {
        ArrayList getCrateDetailsReportPer = new ArrayList();
        getCrateDetailsReportPer = wmsDAO.getCrateDetailsReportPer(wms,fromDate,toDate);
        return getCrateDetailsReportPer;
    }
    public ArrayList getImPodList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getImPodList = wmsDAO.getImPodList(wmsTO);
        return getImPodList;
    }
    
    public ArrayList getDzInvoiceErro(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getDzInvoiceErro = new ArrayList();
        getDzInvoiceErro = wmsDAO.getDzInvoiceErro(wmsTo);
        return getDzInvoiceErro;
    }  
    
    public ArrayList getDzGrnDetailsExport(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getDzGrnDetailsExport = new ArrayList();
        getDzGrnDetailsExport = wmsDAO.getDzGrnDetailsExport(wms);
        return getDzGrnDetailsExport;
    }
    
    public int insertImIndentOrder(WmsTO wms, String[] materialName, String[] materialDescription, String[] uom ,String[] poQty) throws FPRuntimeException, FPBusinessException {
        int insertImIndentOrder = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            insertImIndentOrder = wmsDAO.insertImIndentOrder(wms, materialName, materialDescription, uom , poQty, session);
            if (insertImIndentOrder > 0) {
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return insertImIndentOrder;
    }
    
     public ArrayList imMaterialPo(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList imMaterialPo = new ArrayList();
        imMaterialPo = wmsDAO.imMaterialPo(wms);
        return imMaterialPo;
    }
      public ArrayList imPoMaterialForGrn(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList imPoMaterialForGrn = new ArrayList();
        imPoMaterialForGrn = wmsDAO.imPoMaterialForGrn(wms);
        return imPoMaterialForGrn;
    } 
    
    public ArrayList towerLRDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList towerLRDetails = new ArrayList();
        towerLRDetails = wmsDAO.towerLRDetails(wms);
        return towerLRDetails;
    }
    public ArrayList employeeHireDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList employeeHireDetails = new ArrayList();
        employeeHireDetails = wmsDAO.employeeHireDetails(wms);
        return employeeHireDetails;
    }
    
    public ArrayList towerLRDetails2(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList towerLRDetails2 = new ArrayList();
        towerLRDetails2 = wmsDAO.towerLRDetails2(wms);
        return towerLRDetails2;
    }
    
   public ArrayList dzPodUploadDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList dzPodUploadDetails = new ArrayList();
        dzPodUploadDetails = wmsDAO.dzPodUploadDetails(wms);
        return dzPodUploadDetails;
    }    
   
    
    public ArrayList getWareHouseListForTR(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getWareHouseListForTR = new ArrayList();
        getWareHouseListForTR = wmsDAO.getWareHouseListForTR(wms);
        return getWareHouseListForTR;
    }
   
    public ArrayList getIndentView(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getIndentView = new ArrayList();
        getIndentView = wmsDAO.getIndentView(wms);
        return getIndentView;
    }   
      public ArrayList employeeHireDetailsTemp(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList employeeHireDetailsTemp = new ArrayList();
        employeeHireDetailsTemp = wmsDAO.employeeHireDetailsTemp(wms);
        return employeeHireDetailsTemp;
    }
      
    public int insertDzHireUpload(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int insertDzHireUpload = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        insertDzHireUpload = wmsDAO.insertDzHireUpload(wms, session);
        return insertDzHireUpload;
    }      
   
   public int empHireUpload(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int empHireUpload = 0;
        empHireUpload = wmsDAO.empHireUpload(wms);
        return empHireUpload;
    }    
    
    public int delEmpHireTemp(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int delEmpHireTemp = 0;
        delEmpHireTemp = wmsDAO.delEmpHireTemp(wms);
        return delEmpHireTemp;
    }
   
    public int updateImPodApproval(String podId, String status, WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int updateImPodApproval = 0;
        updateImPodApproval = wmsDAO.updateImPodApproval(podId, status, wms);
        return updateImPodApproval;
    }    
    
    
    public int updateApproveStatus(WmsTO wms,int userId) throws FPRuntimeException, FPBusinessException {
        int updateApproveStatus = 0;
        updateApproveStatus = wmsDAO.updateApproveStatus(wms,userId);
        return updateApproveStatus;
    }    
    
    public int updateAuthStatus(WmsTO wms,int userId) throws FPRuntimeException, FPBusinessException {
        int updateAuthStatus = 0;
        updateAuthStatus = wmsDAO.updateAuthStatus(wms,userId);
        return updateAuthStatus;
    }    
    public String getAuthStatus(WmsTO wms) throws FPRuntimeException, FPBusinessException {
     String   getAuthStatus = wmsDAO.getAuthStatus(wms);
        return getAuthStatus;
    }    
    
    public ArrayList staffAttendanceReport(WmsTO wms) throws FPRuntimeException, FPBusinessException {
     ArrayList   staffAttendanceReport = wmsDAO.staffAttendanceReport(wms);
        return staffAttendanceReport;
    }    
   public String insertImMaterialGrnReturn(WmsTO wms, String[] skuCode, String[] uom, String[] stockQty, String[] returnQty) throws FPRuntimeException, FPBusinessException {
        String returnNo = "";
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            returnNo = wmsDAO.insertImMaterialGrnReturn(wms, skuCode, uom, stockQty, returnQty, session);
            if (returnNo!=null && returnNo!="") {
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return returnNo;
    } 
   
   public ArrayList getImMaterialReturnMaster(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getImMaterialReturnMaster = new ArrayList();
        getImMaterialReturnMaster = wmsDAO.getImMaterialReturnMaster(wms);
        return getImMaterialReturnMaster;
    }

   public ArrayList getImMaterialReturnDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList getImMaterialReturnDetails = new ArrayList();
        getImMaterialReturnDetails = wmsDAO.getImMaterialReturnDetails(wms);
        return getImMaterialReturnDetails;
    }
  public int updateImGrnReturn(String[] floorQty, String[] usedQty, String[] excess, String[] shortage, String[] status, String[] detailId, String[] floorPackQty, String[] materialIds, WmsTO wms,int userId) throws FPRuntimeException, FPBusinessException {
        int update = 0;
        update = wmsDAO.updateImGrnReturn(floorQty, usedQty, excess, shortage, status, detailId, floorPackQty, materialIds, wms,userId);
        return update;
    }
   public ArrayList getDzPodList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getDzPodList = wmsDAO.getDzPodList(wmsTO);
        return getDzPodList;
    }
  
public int updateDzPodApproval(String podId, String status, WmsTO wms) throws FPRuntimeException, FPBusinessException {
        int updateDzPodApproval = 0;
        updateDzPodApproval = wmsDAO.updateDzPodApproval(podId, status, wms);
        return updateDzPodApproval;
    }    
    
 public String getImDeliveryDate(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        String getImDeliveryDate = "";
        getImDeliveryDate = wmsDAO.getImDeliveryDate(wms);
        return getImDeliveryDate;
    }
  public String getDzDeliveryDate(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        String getDzDeliveryDate = "";
        getDzDeliveryDate = wmsDAO.getDzDeliveryDate(wms);
        return getDzDeliveryDate;
    }

    
}
