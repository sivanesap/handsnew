package ets.domain.wms.business;

/**
 *
 * @author hp
 */
public class WmsTO {

    public WmsTO() {
    }
    private String returnDate = "";
    private String returnTime = "";
    private String returnQty = "";
    private String staff = "";
    private String houseKeeping = "";
    private String security = "";
    private String deliveryExecutive = "";
    private String dataEntryOperator = "";
    private String executive = "";
    private String others = "";
    private String attendanceDate = "";
    
    private String totStaff = "";
    private String totHouseKeeping = "";
    private String totSecurity = "";
    private String totDeliveryExecutive = "";
    private String totDataEntryOperator = "";
    private String totExecutive = "";
    private String totOthers = "";
    
    private String createdTime = "";
    private String approvedUser = "";
    private String authUser = "";
    private String approvedDate = "";
    private String authDate = "";
    private String authStatus = "";
    private String totalCrates = "";
    private String custAddress = "";
    private String wareHouseAddress = "";
    private String taxType = "";
    private String unitPrice = "";
    private String materialName = "";
    private String startKm = "";
    private String podName1 = "";
    private String podName2 = "";
    private String podName3 = "";
    private String remarks1 = "";
    private String remarks2 = "";
    private String remarks3 = "";
    private String plannedTime = "";
    private String docketNo = "";
    private String totalAmt = "";
    private String otherAmt = "";
    private String unloadingAmt = "";
    private String haltingAmt = "";
    private String statusType = "";
    private String freightAmount = "";
    private String dispatchDetailId = "";
    private String lrnNo = "";
    private String consigneeName = "";
    private String preTaxValue = "";
    private String deliveryDate = "";

    private double unusedQty = 0;
    private double actQty = 0;
    private double reqQty = 0;
    private String binId = "";
    private String binName = "";
    private String lrDate = "";
    private String whId = "";
    private String invoiceNo = "";
    private String ewaybillNo = "";
    private String invoiceDate = "";
    private String grnId = "";
    private String requestQty = "";
    private String qty = "";
    private String serialNos[] = null;
    private String whIds[] = null;
    private String serialNo = "";
    private String asnDetId = "";
    private String custName = "";
    private String itemName = "";
    private String skuCode = "";
    private String hsnCode = "";
    private String actualQuantity = "";
    private String depositId = "";
    private String depositCode = "";
    private String depositType = "";
    private String activeType = "";
    private String depositAmount = "";
    private String itemId = "";
    private String productId = "";
    private String serialNumber = "";
    private String orderId = "";
    private String orderType = "";
    private String batchDate = "";
    private String branchName = "";
    private String delRequestId = "";
    private String model = "";
    private String productID = "";
    private String createdDate = "";
    private String path = "";
    private String reMarks = "";
    private String loanProposalId = "";
    private String barCode = "";
    private String asnId = "";
    private String asnNo = "";
    private String asnDate = "";
    private String vendorName = "";
    private String asnPath = "";
    private String asnRemarks = "";
    private String CustomerName = "";
    private String userId = "";
    private String warehouseId = "";
//    kova
    private String gtnNo = "";
    private String gtnId = "";
    private String gtnDate = "";
    private String gtnTime = "";
    private String custId = "";
    private String dock = "";
    private String vehicleType = "";
    private String vehicleNo = "";
    private String driverName = "";
    private String remarks = "";

    private String grnNo = "";
    private String grnDate = "";
    private String asnReqId = "";
    private String excessQty = "";
    private String fromDate = "";
    private String toDate = "";
    private String whName = "";
    private String proCon = "";
    private String ipQty[] = null;
    private String uom[] = null;
    private String proCond[] = null;
    private String uom1 = "";
    private String inpQty = "";
    private String proCode = "";
    private String proId = "";
    private String proName = "";
    private String CustId = "";
    private String poName = "";
    private String poNos[] = null;
    private String poQty[] = null;
    private String poQuantity = "";
    private String empId = "";
    private String empName = "";
    private String gtnDetId = "";
    private String empUserId = "";
    private String excess = "";
    private String damaged = "";
    private String shortage = "";
    private String binFlag = "";
    private String binIds[] = null;
    private String serialNoTemp = "";
    private String binIdTemp = "";
    private String proConTemp = "";
    private String serialNoTemp1 = "";
    private String binIdTemp1 = "";
    private String proConTemp1 = "";
    private Double asnQty = null;
    private String prevPoQuantity = "";
    private String serials = "";
    private String tempIds = "";
    private String uoms = "";
    private String bins = "";
    private String proCons = "";
    private String grnId1 = "";
    private String barcode = "";
    private String stateId = "";
    private String stateName = "";
    private String tripQty = "";
    private String ofd = "";
    private String delivered = "";
    private String returned = "";
    private String cancelled = "";
    private String cancelled1 = "";
    private int count = 0;
    private String warehouseToId = "";
    private String[] selectedIndex = null;
    private String vendorId = "";

    private String poDate = "";
    private String PoRequestId = "";
    private String poNumber = "";

    private String remark = "";
    private String Quantity = "";
    private String stockId = "";
    private String asnid = "";
    private String loanid = "";
    private String statusName = "";
    private String runSheetId = "";
    private String trackingId = "";
    private String mobileNo = "";
    private String name = "";
    private String address = "";
    private String shipmentType = "";
    private String codAmount = "";
    private String assignedUser = "";
    private String status = "";
    private String date = "";
    private String amountPaid = "";
    private String receiverName = "";
    private String receiver = "";
    private String relation = "";
    private String mobile = "";
    private String signature = "";
    private String vertical = "";
    private String orderNo = "";
    private String productDetails = "";
    private String billDoc = "";
    private String billDate = "";
    private String purchaseOrderNumber = "";
    private String matlGroup = "";
    private String material = "";
    private String materialDescription = "";
    private String billQty = "";
    private String city = "";
    private String cityName = "";
    private String shipTo = "";
    private String soldTo = "";
    private String partyName = "";
    private String gstinNumber = "";
    private String grossValue = "";
    private String pincode = "";
    private String flag = "";
    private String error = "";
    private String lrId = "";
    private String lrNo = "";
    private String ewayBillNo = "";
    private String ewayExpiry = "";
    private String skuId = "";
    private String pickupaddress = "";
    private String itemNos = "";
    private String pickupDate = "";
    private String lastStatus = "";
    private String shipmentId = "";
    private String pinCode = "";
    private String deliveryHub = "";
    private String assignedExecutive = "";
    private String origin = "";
    private String currentHub = "";
    private String note = "";
    private String amount = "";
    private String weight = "";
    private String lastModified = "";
    private String lastReceived = "";
    private String shipmentTierType = "";
    private String reverseShipmentId = "";
    private String bagStatus = "";
    private String type = "";
    private String price = "";
    private String latestStatus = "";
    private String currentLocation = "";
    private String latestUpdateDateTime = "";
    private String firstReceivedHub = "";
    private String firstReceiveDateTime = "";
    private String hubNotes = "";
    private String csNotes = "";
    private String numberofAttempts = "";
    private String consignmentId = "";
    private String customerPromiseDate = "";
    private String logisticsPromiseDate = "";
    private String onHoldByOpsReason = "";
    private String onHoldByOpsDate = "";
    private String bagId = "";
    private String firstAssignedHubName = "";
    private String deliveryPinCode = "";
    private String lastReceivedDateTime = "";
    private String exchange = "";
    private String rejectionId = "";
    private String reason = "";
    private String dispatchId = "";
    private String dealerId = "";
    private String dealerName = "";
    private String dealerCode = "";
    private String tripId = "";
    private String cardAmount = "";
    private String prexoAmount = "";
    private String deCod = "";
    private String totalCod = "";
    private String totalDeCod = "";
    private String totalCardAmount = "";
    private String totalPrexoAmount = "";
    private String tripStatusId = "";
    private String reconcileId = "";
    private String reconcileCode = "";
    private String runsheetId = "";
    private String tripCode = "";
    private String fileName = "";
    private String podId = "";
    private String filePath = "";
    private String podStatus = "";
    private String invoiceStatus = "";
    private String inbound = "";
    private String prexo = "";
    private String rvp = "";
    private String roleId = "";
    private String cycleId = "";
    private String productTypeId = "";
    private String productTypeName = "";
    private String reconcileIds[] = null;
    private String deCods[] = null;
    private String cardAmounts[] = null;
    private String prexoAmounts[] = null;
    private String shipmentTypes[] = null;
    private String vol = "";
    private String taxVol = "";
    private String wgt = "";
    private String invoiceNos = "";
    private String totQty = "";
    private String totVol = "";
    private String totWgt = "";
    private String totTaxVol = "";
    private String taxValE[] = null;
    private String volE[] = null;
    private String wgtE[] = null;
    private String customerIdE[] = null;
    private String invoiceNoE[] = null;
    private String qtyE[] = null;
    private String orderIdE[] = null;
    private String customerId = "";
    private String suppId = "";
    private String vehicleTypeId = "";
    private String suppName = "";
    private String typeId = "";
    private String typeName = "";
    private String invoiceNumber = "";
    private String quantity = "";
    private String plantCode = "";
    private String shipToPartycode = "";
    private String shipToPartyName = "";
    private String materialCode = "";
    private String billedQuantity = "";
    private String basicPrice = "";
    private String mRP = "";
    private String pretaxValue = "";
    private String taxValue = "";
    private String invoiceAmount = "";
    private String purchaseOrderNo = "";
    private String purchaseOrderDate = "";
    private String saleOrderNo = "";
    private String saleOrderDate = "";
    private String currency = "";
    private String hSNCode = "";
    private String cGSTAmount = "";
    private String sGSTAmount = "";
    private String iGSTAmount = "";
    private String deliveryNumber = "";
    private String matGrptext = "";
    private String discountAmount = "";
    private String invoiceId = "";
    private String vechileRegNo = "";
    private String dispatchNo = "";
    private String productName = "";
    private String tatId = "";
    private String phoneNumber = "";
    private String vehicleNumber = "";
    private String dispatchid = "";
    private String Tansferid = "";
    private String loadingStart = "";
    private String pendingReason = "";
    private String loadingEnd = "";
    private String[] invoiceIds = null;
    private String[] customerIds = null;
    private String[] quantitys = null;
    private String[] pendingReasons = null;
    private String[] dcQty = null;
    private String lrdate = "";
    private String lrtime = "";
    private String lrNumber = "";
    private String sublrNumber = "";
    private String consinorName = "";
    private String consinorAddress = "";
    private String consinorCity = "";
    private String consinorPhone = "";
    private String consinorGst = "";
    private String boxQty = "";
    private String cft = "";
    private String packQTy = "";
    private String packQty = "";
    private String actualwgt = "";
    private String[] itemsides = null;
    private String[] pendingQty = null;
    private String supplierName = "";
    private String supplierId = "";
    private String rackId = "";
    private String rackName = "";
    private String storageId = "";
    private String[] storageIds = null;
    private String storageName = "";
    private String storageWhid = "";
    private String grnQty = "";
    private String[] locatioId = null;
    private String[] rackIdE = null;
    private String serialId = "";
    private String gateInwardDate = "";
    private String gateInwardNo = "";
    private String processType = "";
    private String modeofTransport = "";
//     private String vehicleNo = "";
    private String transporterCode = "";
    private String transporterName = "";
    private String SerialNo = "";
    private String giQuantity = "";
//     private  String uom = "";
    private String billNum = "";
//      private String billDate = "";
    private String matDocNo = "";
    private String matDocDate = "";
    private String matDocYear = "";
    private String postingDate = "";
    private String suppPlantCode = "";
    private String suppPlantDesc = "";
    private String recPlantCode = "";
    private String recPlantDesc = "";
    private String grQty = "";
    private String storageLocation = "";
//      String purchaseOrderNo = "";
    private String deliveryNo = "";
    private String movementType = "";
    private String createdBy = "";
    private String cancelledNo = "";

    private String grnID = "";
    private String cancelledNumber = "";
    private String recplantDes = "";
    private String recplantCode = "";
    private String supPlantDes = "";
    private String supPlantCode = "";
    private String materialDes = "";
    private String vehicleNO = "";
    private String itemIds[] = null;
    private String stockIds[] = null;
    private String pretaxValues = "";
    private String invoiceAmounts = "";
    private String[] supplyId = null;
    private String basePrice = "";
    private String[] serialIds = null;
    private String subLr = "";
    private String subLrId = "";
    private String[] disinvoiceAmt = null;
    private String[] disTaxvalue = null;
    private String[] disPerTax = null;
    private String taxvalue = "";
    private String pertaxvalue = "";
    private String totalQty = "";
    private String orderRefNo = "";
    private String paymentType = "";
    private String pickupTime = "";
    private String transferType = "";
    private String partyCode = "";
    private String stockQty = "";
    private String scannable = "";
    private String locationId = "";
    private String looseQty = "";
    private String picklistIds = "";
    private String plannedDate = "";
    private String whAddress = "";
    private String partyDcNos = "";
    private String dcDate = "";
    private String orderDate = "";
    private String tatType = "";
    private String loadingStartTime = "";
    private String loadingEndTime = "";
    private String podLink1 = "";
    private String podLink2 = "";
    private String podLink3 = "";
    private String sapRefNo = "";
    private String invoiceTime = "";
    private String categoryName = "";
    private String billNo = "";
    private String appointmentDate = "";
    private String pendingQuantity = "";
    private String referenceNo = "";
    private String periodFrom = "";
    private String periodTo = "";
    private String dateOfReceived = "";
    private String dateOfEntry = "";
    private String receivedBy = "";
    private String gstAmt = "";
    private String billAmt = "";
    private String billId = "";
    private String billDetailId = "";
    private String createdOn = "";
    private String plant = "";
    private String fromLocation = "";
    private String toLocation = "";
    private String indentDate = "";
    private String deliveryType = "";
    private String indentId = "";
    private String indentDetailId = "";
    private String indentStatus = "";
    private String supervisorName = "";
    private String[] driverMobileNos = null;
    private String[] driverNames = null;
    private String[] vehicleNos = null;
    private String expectedDate = "";
    private String workOrderDate = "";
    private String workOrderId = "";
    private String workOrderNo = "";
    private String orderDetailId = "";
    private String[] invoiceDetailIds = null;
    private String inDate = "";
    private String inTime = "";
    private String[] deliveryDates = null;
    private String transferNo = "";
    private String fromWhId = "";
    private String toWhId = "";
    private String fromWhName = "";
    private String toWhName = "";
    private String uom2 = "";
    private String transferId = "";
    private String transferDetailId = "";
    private String id = "";
    private String stockType = "";
    private String custCode = "";
    private String materialId = "";
    private String size = "";
    private String unit = "";
    private String address1 = "";
    private String poNo = "";
    private String poId = "";
    private String grnTime = "";
    private String transportCode = "";
    private String issueDate = "";
    private String issueTime = "";
    private String issueId = "";
    private String issueDetailId = "";
    private String binLocation = "";
    private String issueNo = "";
    private String groupId = "";
    private String groupName = "";
    private String uomId = "";
    private String uomName = "";
    private String uomCode = "";
    private String time = "";
    private String productDescription = "";

    private String minimumStockQty = "";
    private String temperature = "";
    private String storage = "";

    private String floorQty = "";
    private String usedQty = "";

    private String tripCount = "";
    private String deviceId = "";
    private String deviceNo = "";
    private String apiTripId = "";
    private String outTime = "";
    private String desigId = "";
    private String designationName = "";
    private String transhipmentWhId = "";
    private String transhipmentStatus = "";
    private String modifiedBy = "";
    private String modifiedOn = "";
    private String tacVal = "";
    private String driverMobileNo = "";
    private String tripDateStart = "";
    private String dispatchDate = "";
    private String tripStartTime = "";
    private String[] invoiceDates = null;
    private String vendorCode = "";
    private String state = "";
    private String getContactPerson = "";
    private String activeInd = "";
    private String[] productIds = null;
    private String[] invoiceIdE = null;
    private String[] invoiceDetailIdE = null;
    private String contactPerson = "";
    private String gstNo = "";
    private String machineId = "";
    private String machineName = "";
    private String packId = "";
    private String totKgQty = "";
    private String totPcQty = "";
    private String totPacketQty = "";
    private String[] productDescriptions = null;
    private String[] acceptedQty = null;
    private String[] rejectedQty = null;
    private String[] updatedTime = null;
    private String[] updatedDate = null;
    private String[] detailIds = null;
    private String subLrid = "";
    private String[] invoiceAmountL = null;
    private String[] noOfCrates1 = null;
    private String noOfCrates = "";
    private String district = "";
    private String industries = "";
    private String street = "";
    private String estimtedDate = "";
    private String custOrderNo = "";
    private String custOrderId = "";
    private String custSubOrderId = "";
    private String optionName = "";
    private String optionValue = "";
    private String productVariantId = "";
    private String subCategoryName = "";
    private String detailId = "";
    private String productLineEntryId = "";
    private String orderedQty = "";
    private String receivedQty = "";
    private String diffQty = "";
    private String totalOrderedQty = "";
    private String totalReceivedQty = "";
    private String totalDiffQty = "";
    private String billToLocationId = "";
    private String billToLocationName = "";
    private String shipToLocationId = "";
    private String shipToLocationName = "";
    private String orderedQty1 = "";
    private String receivedQty1 = "";
    private String poRefernceNo = "";
    private String sendAmt = "";
    private String receivedAmt = "";
    private String totalSendAmt = "";
    private String totalReceivedAmt = "";
    private String productCode = "";
    private String scannedCode = "";
    private String scannedQty = "";
    private String balanceQty = "";
    private String outwardQty = "";
    private String milk = "";
    private String productId1 = "";
    private String toNo = "";
    private String inwardQty = "";
    private String pendingQty1 = "";
    private String outwardDate = "";
    private String outwardTime = "";
    private String inwardDate = "";
    private String inwardTime = "";
    private String sublrId = "";
    private String totalPendingQty = "";
    private String sub = "";
    private String startTime = "";
    private String available = "";
    private String notMap = "";
    private String inwardPending = "";
    private String outwardPending = "";
    private String crateCode = "";
    private String poOrderedQty = "";
    private String poReceivedQty = "";
    private String inQty = ""; 
    private String poQty1 = ""; 
    private String noOfDrops = ""; 
    private String current = ""; 
    private String good = ""; 
    private String damage = ""; 
    private String scanId = "";
    private String crateNo = "";
    private String crateId = "";
    private String inId = "";
    private String inNo = "";
    private String ctc = "";
    private String location = "";
    private String designation = "";
    private String category = "";
    private String client = "";
    private String returnId = "";
    private String returnNo = "";
    private String returnDetailId = "";
   
    
    public String getReturnDetailId() {
        return returnDetailId;
    }

    public void setReturnDetailId(String returnDetailId) {
        this.returnDetailId = returnDetailId;
    }
    
    
    public String getReturnNo() {
        return returnNo;
    }

    public void setReturnNo(String returnNo) {
        this.returnNo = returnNo;
    }
    
    

    public String getReturnId() {
        return returnId;
    }

    public void setReturnId(String returnId) {
        this.returnId = returnId;
    }
    

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getCtc() {
        return ctc;
    }

    public void setCtc(String ctc) {
        this.ctc = ctc;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLeavingDate() {
        return leavingDate;
    }

    public void setLeavingDate(String leavingDate) {
        this.leavingDate = leavingDate;
    }
    private String leavingDate = "";

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public String getGood() {
        return good;
    }

    public void setGood(String good) {
        this.good = good;
    }

    public String getDamage() {
        return damage;
    }

    public void setDamage(String damage) {
        this.damage = damage;
    }

    public String getNoOfDrops() {
        return noOfDrops;
    }

    public void setNoOfDrops(String noOfDrops) {
        this.noOfDrops = noOfDrops;
    }

    public String getPoQty1() {
        return poQty1;
    }

    public void setPoQty1(String poQty1) {
        this.poQty1 = poQty1;
    }

    public String getInQty() {
        return inQty;
    }

    public void setInQty(String inQty) {
        this.inQty = inQty;
    }

    public String getPoOrderedQty() {
        return poOrderedQty;
    }

    public void setPoOrderedQty(String poOrderedQty) {
        this.poOrderedQty = poOrderedQty;
    }

    public String getPoReceivedQty() {
        return poReceivedQty;
    }

    public void setPoReceivedQty(String poReceivedQty) {
        this.poReceivedQty = poReceivedQty;
    }
    public String getInId() {
        return inId;
    }

    public void setInId(String inId) {
        this.inId = inId;
    }

    public String getInNo() {
        return inNo;
    }

    public void setInNo(String inNo) {
        this.inNo = inNo;
    }
    

    public String getCrateCode() {
        return crateCode;
    }

    public void setCrateCode(String crateCode) {
        this.crateCode = crateCode;
    }
    

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getNotMap() {
        return notMap;
    }

    public void setNotMap(String notMap) {
        this.notMap = notMap;
    }

    public String getInwardPending() {
        return inwardPending;
    }

    public void setInwardPending(String inwardPending) {
        this.inwardPending = inwardPending;
    }

    public String getOutwardPending() {
        return outwardPending;
    }

    public void setOutwardPending(String outwardPending) {
        this.outwardPending = outwardPending;
    }
    
    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
    private String endTime = "";
    

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }
    

    public String getTotalPendingQty() {
        return totalPendingQty;
    }

    public void setTotalPendingQty(String totalPendingQty) {
        this.totalPendingQty = totalPendingQty;
    }
    
    public String getSublrId() {
        return sublrId;
    }

    public void setSublrId(String sublrId) {
        this.sublrId = sublrId;
    }
    

    public String getOutwardDate() {
        return outwardDate;
    }

    public void setOutwardDate(String outwardDate) {
        this.outwardDate = outwardDate;
    }

    public String getOutwardTime() {
        return outwardTime;
    }

    public void setOutwardTime(String outwardTime) {
        this.outwardTime = outwardTime;
    }

    public String getInwardDate() {
        return inwardDate;
    }

    public void setInwardDate(String inwardDate) {
        this.inwardDate = inwardDate;
    }

    public String getInwardTime() {
        return inwardTime;
    }

    public void setInwardTime(String inwardTime) {
        this.inwardTime = inwardTime;
    }

    public String getPendingQty1() {
        return pendingQty1;
    }

    public void setPendingQty1(String pendingQty1) {
        this.pendingQty1 = pendingQty1;
    }

    public String getInwardQty() {
        return inwardQty;
    }

    public void setInwardQty(String inwardQty) {
        this.inwardQty = inwardQty;
    }

    public String getToNo() {
        return toNo;
    }

    public void setToNo(String toNo) {
        this.toNo = toNo;
    }
    

    public String getScanId() {
        return scanId;
    }

    public void setScanId(String scanId) {
        this.scanId = scanId;
    }

    public String getCrateNo() {
        return crateNo;
    }

    public void setCrateNo(String crateNo) {
        this.crateNo = crateNo;
    }

    public String getCrateId() {
        return crateId;
    }

    public void setCrateId(String crateId) {
        this.crateId = crateId;
    }

    public String getProductId1() {
        return productId1;
    }

    public void setProductId1(String productId1) {
        this.productId1 = productId1;
    }

    public String getMilk() {
        return milk;
    }

    public void setMilk(String milk) {
        this.milk = milk;
    }

    public String getPackets() {
        return packets;
    }

    public void setPackets(String packets) {
        this.packets = packets;
    }

    public String getTons() {
        return tons;
    }

    public void setTons(String tons) {
        this.tons = tons;
    }

    public String getFandv() {
        return fandv;
    }

    public void setFandv(String fandv) {
        this.fandv = fandv;
    }
    private String packets = "";
    private String tons = "";
    private String fandv = "";


    public String getBalanceQty() {
        return balanceQty;
    }

    public void setBalanceQty(String balanceQty) {
        this.balanceQty = balanceQty;
    }

    public String getOutwardQty() {
        return outwardQty;
    }

    public void setOutwardQty(String outwardQty) {
        this.outwardQty = outwardQty;
    }

    public String getScannedCode() {
        return scannedCode;
    }

    public void setScannedCode(String scannedCode) {
        this.scannedCode = scannedCode;
    }

    public String getScannedQty() {
        return scannedQty;
    }

    public void setScannedQty(String scannedQty) {
        this.scannedQty = scannedQty;
    }

    
    public String getSendAmt() {
        return sendAmt;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setSendAmt(String sendAmt) {
        this.sendAmt = sendAmt;
    }

    public String getReceivedAmt() {
        return receivedAmt;
    }

    public void setReceivedAmt(String receivedAmt) {
        this.receivedAmt = receivedAmt;
    }

    public String getTotalSendAmt() {
        return totalSendAmt;
    }

    public void setTotalSendAmt(String totalSendAmt) {
        this.totalSendAmt = totalSendAmt;
    }

    public String getTotalReceivedAmt() {
        return totalReceivedAmt;
    }

    public void setTotalReceivedAmt(String totalReceivedAmt) {
        this.totalReceivedAmt = totalReceivedAmt;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getIndustries() {
        return industries;
    }

    public void setIndustries(String industries) {
        this.industries = industries;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTotalOrderedQty() {
        return totalOrderedQty;
    }

    public void setTotalOrderedQty(String totalOrderedQty) {
        this.totalOrderedQty = totalOrderedQty;
    }

    public String getTotalReceivedQty() {
        return totalReceivedQty;
    }

    public void setTotalReceivedQty(String totalReceivedQty) {
        this.totalReceivedQty = totalReceivedQty;
    }

    public String getTotalDiffQty() {
        return totalDiffQty;
    }

    public void setTotalDiffQty(String totalDiffQty) {
        this.totalDiffQty = totalDiffQty;
    }

    public String getOrderedQty1() {
        return orderedQty1;
    }

    public void setOrderedQty1(String orderedQty1) {
        this.orderedQty1 = orderedQty1;
    }

    public String getReceivedQty1() {
        return receivedQty1;
    }

    public void setReceivedQty1(String receivedQty1) {
        this.receivedQty1 = receivedQty1;
    }

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    public String getProductLineEntryId() {
        return productLineEntryId;
    }

    public void setProductLineEntryId(String productLineEntryId) {
        this.productLineEntryId = productLineEntryId;
    }

    public String getOrderedQty() {
        return orderedQty;
    }

    public void setOrderedQty(String orderedQty) {
        this.orderedQty = orderedQty;
    }

    public String getReceivedQty() {
        return receivedQty;
    }

    public void setReceivedQty(String receivedQty) {
        this.receivedQty = receivedQty;
    }

    public String getDiffQty() {
        return diffQty;
    }

    public void setDiffQty(String diffQty) {
        this.diffQty = diffQty;
    }

    public String getBillToLocationId() {
        return billToLocationId;
    }

    public void setBillToLocationId(String billToLocationId) {
        this.billToLocationId = billToLocationId;
    }

    public String getBillToLocationName() {
        return billToLocationName;
    }

    public void setBillToLocationName(String billToLocationName) {
        this.billToLocationName = billToLocationName;
    }

    public String getShipToLocationId() {
        return shipToLocationId;
    }

    public void setShipToLocationId(String shipToLocationId) {
        this.shipToLocationId = shipToLocationId;
    }

    public String getShipToLocationName() {
        return shipToLocationName;
    }

    public void setShipToLocationName(String shipToLocationName) {
        this.shipToLocationName = shipToLocationName;
    }

    public String getPoRefernceNo() {
        return poRefernceNo;
    }

    public void setPoRefernceNo(String poRefernceNo) {
        this.poRefernceNo = poRefernceNo;
    }

    public String getEstimtedDate() {
        return estimtedDate;
    }

    public void setEstimtedDate(String estimtedDate) {
        this.estimtedDate = estimtedDate;
    }

    public String getCustOrderNo() {
        return custOrderNo;
    }

    public void setCustOrderNo(String custOrderNo) {
        this.custOrderNo = custOrderNo;
    }

    public String getCustOrderId() {
        return custOrderId;
    }

    public void setCustOrderId(String custOrderId) {
        this.custOrderId = custOrderId;
    }

    public String getCustSubOrderId() {
        return custSubOrderId;
    }

    public void setCustSubOrderId(String custSubOrderId) {
        this.custSubOrderId = custSubOrderId;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public String getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(String optionValue) {
        this.optionValue = optionValue;
    }

    public String getProductVariantId() {
        return productVariantId;
    }

    public void setProductVariantId(String productVariantId) {
        this.productVariantId = productVariantId;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getNoOfCrates() {
        return noOfCrates;
    }

    public void setNoOfCrates(String noOfCrates) {
        this.noOfCrates = noOfCrates;
    }

    public String[] getInvoiceAmountL() {
        return invoiceAmountL;
    }

    public void setInvoiceAmountL(String[] invoiceAmountL) {
        this.invoiceAmountL = invoiceAmountL;
    }

    public String[] getNoOfCrates1() {
        return noOfCrates1;
    }

    public void setNoOfCrates1(String[] noOfCrates1) {
        this.noOfCrates1 = noOfCrates1;
    }

    public String getSubLrid() {
        return subLrid;
    }

    public void setSubLrid(String subLrid) {
        this.subLrid = subLrid;
    }

    public String getTotKgQty() {
        return totKgQty;
    }

    public void setTotKgQty(String totKgQty) {
        this.totKgQty = totKgQty;
    }

    public String getTotPcQty() {
        return totPcQty;
    }

    public void setTotPcQty(String totPcQty) {
        this.totPcQty = totPcQty;
    }

    public String getTotPacketQty() {
        return totPacketQty;
    }

    public void setTotPacketQty(String totPacketQty) {
        this.totPacketQty = totPacketQty;
    }

    public String getPackId() {
        return packId;
    }

    public void setPackId(String packId) {
        this.packId = packId;
    }

    public String[] getAcceptedQty() {
        return acceptedQty;
    }

    public void setAcceptedQty(String[] acceptedQty) {
        this.acceptedQty = acceptedQty;
    }

    public String[] getRejectedQty() {
        return rejectedQty;
    }

    public void setRejectedQty(String[] rejectedQty) {
        this.rejectedQty = rejectedQty;
    }

    public String[] getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(String[] updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String[] getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String[] updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String[] getDetailIds() {
        return detailIds;
    }

    public void setDetailIds(String[] detailIds) {
        this.detailIds = detailIds;
    }

    public String[] getProductDescriptions() {
        return productDescriptions;
    }

    public void setProductDescriptions(String[] productDescriptions) {
        this.productDescriptions = productDescriptions;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getMachineId() {
        return machineId;
    }

    public void setMachineId(String machineId) {
        this.machineId = machineId;
    }

    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
    private String emailId = "";

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGetContactPerson() {
        return getContactPerson;
    }

    public void setGetContactPerson(String getContactPerson) {
        this.getContactPerson = getContactPerson;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getTacVal() {
        return tacVal;
    }

    public void setTacVal(String tacVal) {
        this.tacVal = tacVal;
    }

    public String getTranshipmentWhId() {
        return transhipmentWhId;
    }

    public void setTranshipmentWhId(String transhipmentWhId) {
        this.transhipmentWhId = transhipmentWhId;
    }

    public String getTranshipmentStatus() {
        return transhipmentStatus;
    }

    public void setTranshipmentStatus(String transhipmentStatus) {
        this.transhipmentStatus = transhipmentStatus;
    }

    public String[] getInvoiceDates() {
        return invoiceDates;
    }

    public void setInvoiceDates(String[] invoiceDates) {
        this.invoiceDates = invoiceDates;
    }

    public String getTripStartTime() {
        return tripStartTime;
    }

    public void setTripStartTime(String tripStartTime) {
        this.tripStartTime = tripStartTime;
    }

    public String getDispatchDate() {
        return dispatchDate;
    }

    public void setDispatchDate(String dispathDate) {
        this.dispatchDate = dispathDate;
    }

    public String getDriverMobileNo() {
        return driverMobileNo;
    }

    public void setDriverMobileNo(String driverMobileNo) {
        this.driverMobileNo = driverMobileNo;
    }

    public String getTripDateStart() {
        return tripDateStart;
    }

    public void setTripDateStart(String tripDateStart) {
        this.tripDateStart = tripDateStart;
    }

    public String getDesignationName() {
        return designationName;
    }

    public void setDesignationName(String designationName) {
        this.designationName = designationName;
    }

    private String designationId = "";

    public String getDesignationId() {
        return designationId;
    }

    public void setDesignationId(String designationId) {
        this.designationId = designationId;
    }

    public String getDesigId() {
        return desigId;
    }

    public void setDesigId(String desigId) {
        this.desigId = desigId;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getApiTripId() {
        return apiTripId;
    }

    public void setApiTripId(String apiTripId) {
        this.apiTripId = apiTripId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getTripCount() {
        return tripCount;
    }

    public void setTripCount(String tripCount) {
        this.tripCount = tripCount;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getMinimumStockQty() {
        return minimumStockQty;
    }

    public void setMinimumStockQty(String minimumStockQty) {
        this.minimumStockQty = minimumStockQty;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getUsedQty() {
        return usedQty;
    }

    public void setUsedQty(String usedQty) {
        this.usedQty = usedQty;
    }

    public String getFloorQty() {
        return floorQty;
    }

    public void setFloorQty(String floorQty) {
        this.floorQty = floorQty;
    }

    public String getUomCode() {
        return uomCode;
    }

    public void setUomCode(String uomCode) {
        this.uomCode = uomCode;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getIssueDetailId() {
        return issueDetailId;
    }

    public void setIssueDetailId(String issueDetailId) {
        this.issueDetailId = issueDetailId;
    }

    public String getBinLocation() {
        return binLocation;
    }

    public void setBinLocation(String binLocation) {
        this.binLocation = binLocation;
    }

    public String getIssueNo() {
        return issueNo;
    }

    public void setIssueNo(String issueNo) {
        this.issueNo = issueNo;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssueTime() {
        return issueTime;
    }

    public void setIssueTime(String issueTime) {
        this.issueTime = issueTime;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getTransportCode() {
        return transportCode;
    }

    public void setTransportCode(String transportCode) {
        this.transportCode = transportCode;
    }

    public String getPoNo() {
        return poNo;
    }

    public String getGrnTime() {
        return grnTime;
    }

    public void setGrnTime(String grnTime) {
        this.grnTime = grnTime;
    }

    public void setPoNo(String poNo) {
        this.poNo = poNo;
    }

    public String getPoId() {
        return poId;
    }

    public void setPoId(String poId) {
        this.poId = poId;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getMaterialId() {
        return materialId;
    }

    public void setMaterialId(String materialId) {
        this.materialId = materialId;
    }

    public String getCustCode() {
        return custCode;
    }

    public void setCustCode(String custCode) {
        this.custCode = custCode;
    }

    public String[] getInvoiceIdE() {
        return invoiceIdE;
    }

    public void setInvoiceIdE(String[] invoiceIdE) {
        this.invoiceIdE = invoiceIdE;
    }

    public String[] getInvoiceDetailIdE() {
        return invoiceDetailIdE;
    }

    public void setInvoiceDetailIdE(String[] invoiceDetailIdE) {
        this.invoiceDetailIdE = invoiceDetailIdE;
    }

    public String[] getProductIds() {
        return productIds;
    }

    public void setProductIds(String[] productIds) {
        this.productIds = productIds;
    }

    public String getStockType() {
        return stockType;
    }

    public void setStockType(String stockType) {
        this.stockType = stockType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String[] getInvoiceDetailIds() {
        return invoiceDetailIds;
    }

    public void setInvoiceDetailIds(String[] invoiceDetailIds) {
        this.invoiceDetailIds = invoiceDetailIds;
    }

    public String getTransferDetailId() {
        return transferDetailId;
    }

    public void setTransferDetailId(String transferDetailId) {
        this.transferDetailId = transferDetailId;
    }

    public String getToWhId() {
        return toWhId;
    }

    public void setToWhId(String toWhId) {
        this.toWhId = toWhId;
    }

    public String getTransferId() {
        return transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    public String getUom2() {
        return uom2;
    }

    public void setUom2(String uom2) {
        this.uom2 = uom2;
    }

    public String getToWhName() {
        return toWhName;
    }

    public void setToWhName(String toWhName) {
        this.toWhName = toWhName;
    }

    public String getFromWhName() {
        return fromWhName;
    }

    public void setFromWhName(String fromWhName) {
        this.fromWhName = fromWhName;
    }

    public String getFromWhId() {
        return fromWhId;
    }

    public void setFromWhId(String fromWhId) {
        this.fromWhId = fromWhId;
    }

    public String getTransferNo() {
        return transferNo;
    }

    public void setTransferNo(String transferNo) {
        this.transferNo = transferNo;
    }

    public String getInDate() {
        return inDate;
    }

    public void setInDate(String inDate) {
        this.inDate = inDate;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getWorkOrderNo() {
        return workOrderNo;
    }

    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    public String getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(String orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public String getSupervisorName() {
        return supervisorName;
    }

    public void setSupervisorName(String supervisorName) {
        this.supervisorName = supervisorName;
    }

    public String getIndentDetailId() {
        return indentDetailId;
    }

    public void setIndentDetailId(String indentDetailId) {
        this.indentDetailId = indentDetailId;
    }

    public String getIndentStatus() {
        return indentStatus;
    }

    public void setIndentStatus(String indentStatus) {
        this.indentStatus = indentStatus;
    }

    public String[] getDeliveryDates() {
        return deliveryDates;
    }

    public void setDeliveryDates(String[] deliveryDates) {
        this.deliveryDates = deliveryDates;
    }

    public String getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(String workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getWorkOrderDate() {
        return workOrderDate;
    }

    public void setWorkOrderDate(String workOrderDate) {
        this.workOrderDate = workOrderDate;
    }

    public String getExpectedDate() {
        return expectedDate;
    }

    public void setExpectedDate(String expectedDate) {
        this.expectedDate = expectedDate;
    }

    public String[] getDriverMobileNos() {
        return driverMobileNos;
    }

    public void setDriverMobileNos(String[] driverMobileNos) {
        this.driverMobileNos = driverMobileNos;
    }

    public String[] getDriverNames() {
        return driverNames;
    }

    public void setDriverNames(String[] driverNames) {
        this.driverNames = driverNames;
    }

    public String[] getVehicleNos() {
        return vehicleNos;
    }

    public void setVehicleNos(String[] vehicleNos) {
        this.vehicleNos = vehicleNos;
    }

    public String getIndentId() {
        return indentId;
    }

    public void setIndentId(String indentId) {
        this.indentId = indentId;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(String toLocation) {
        this.toLocation = toLocation;
    }

    public String getIndentDate() {
        return indentDate;
    }

    public void setIndentDate(String indentDate) {
        this.indentDate = indentDate;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant() {
        this.plant = plant;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String billId) {
        this.createdOn = createdOn;
    }

    public String getBillDetailId() {
        return billDetailId;
    }

    public void setBillDetailId(String billId) {
        this.billDetailId = billDetailId;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getPeriodFrom() {
        return periodFrom;
    }

    public void setPeriodFrom(String periodFrom) {
        this.periodFrom = periodFrom;
    }

    public String getPeriodTo() {
        return periodTo;
    }

    public void setPeriodTo(String periodTo) {
        this.periodTo = periodTo;
    }

    public String getDateOfReceived() {
        return dateOfReceived;
    }

    public void setDateOfReceived(String dateOfReceived) {
        this.dateOfReceived = dateOfReceived;
    }

    public String getDateOfEntry() {
        return dateOfEntry;
    }

    public void setDateOfEntry(String dateOfEntry) {
        this.dateOfEntry = dateOfEntry;
    }

    public String getReceivedBy() {
        return receivedBy;
    }

    public void setReceivedBy(String receivedBy) {
        this.receivedBy = receivedBy;
    }

    public String getGstAmt() {
        return gstAmt;
    }

    public void setGstAmt(String gstAmt) {
        this.gstAmt = gstAmt;
    }

    public String getBillAmt() {
        return billAmt;
    }

    public void setBillAmt(String billAmt) {
        this.billAmt = billAmt;
    }

    public String getPendingQuantity() {
        return pendingQuantity;
    }

    public void setPendingQuantity(String pendingQuantity) {
        this.pendingQuantity = pendingQuantity;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getInvoiceTime() {
        return invoiceTime;
    }

    public void setInvoiceTime(String invoiceTime) {
        this.invoiceTime = invoiceTime;
    }

    public String getSapRefNo() {
        return sapRefNo;
    }

    public void setSapRefNo(String sapRefNo) {
        this.sapRefNo = sapRefNo;
    }

    public String getPodLink1() {
        return podLink1;
    }

    public void setPodLink1(String podLink1) {
        this.podLink1 = podLink1;
    }

    public String getPodLink2() {
        return podLink2;
    }

    public void setPodLink2(String podLink2) {
        this.podLink2 = podLink2;
    }

    public String getPodLink3() {
        return podLink3;
    }

    public void setPodLink3(String podLink3) {
        this.podLink3 = podLink3;
    }

    public String getLoadingStartTime() {
        return loadingStartTime;
    }

    public void setLoadingStartTime(String loadingStartTime) {
        this.loadingStartTime = loadingStartTime;
    }

    public String getLoadingEndTime() {
        return loadingEndTime;
    }

    public void setLoadingEndTime(String loadingEndTime) {
        this.loadingEndTime = loadingEndTime;
    }

    public String getTatType() {
        return tatType;
    }

    public void setTatType(String tatType) {
        this.tatType = tatType;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getDcDate() {
        return dcDate;
    }

    public void setDcDate(String dcDate) {
        this.dcDate = dcDate;
    }

    public String getPartyDcNos() {
        return partyDcNos;
    }

    public void setPartyDcNos(String partyDcNos) {
        this.partyDcNos = partyDcNos;
    }

    public String getSoldTo() {
        return soldTo;
    }

    public void setSoldTo(String soldTo) {
        this.soldTo = soldTo;
    }

    public String getWhAddress() {
        return whAddress;
    }

    public void setWhAddress(String whAddress) {
        this.whAddress = whAddress;
    }

    public String getPicklistIds() {
        return picklistIds;
    }

    public void setPicklistIds(String picklistIds) {
        this.picklistIds = picklistIds;
    }

    public String getLooseQty() {
        return looseQty;
    }

    public void setLooseQty(String looseQty) {
        this.looseQty = looseQty;
    }

    public String[] getStorageIds() {
        return storageIds;
    }

    public void setStorageIds(String[] storageIds) {
        this.storageIds = storageIds;
    }

    public String getPackQty() {
        return packQty;
    }

    public void setPackQty(String packQty) {
        this.packQty = packQty;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getScannable() {
        return scannable;
    }

    public void setScannable(String scannable) {
        this.scannable = scannable;
    }

    public String getStorageId() {
        return storageId;
    }

    public void setStorageId(String storageId) {
        this.storageId = storageId;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getStockQty() {
        return stockQty;
    }

    public void setStockQty(String stockQty) {
        this.stockQty = stockQty;
    }

    public String getPartyCode() {
        return partyCode;
    }

    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getOrderRefNo() {
        return orderRefNo;
    }

    public void setOrderRefNo(String orderRefNo) {
        this.orderRefNo = orderRefNo;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTaxvalue() {
        return taxvalue;
    }

    public void setTaxvalue(String taxvalue) {
        this.taxvalue = taxvalue;
    }

    public String getPertaxvalue() {
        return pertaxvalue;
    }

    public void setPertaxvalue(String pertaxvalue) {
        this.pertaxvalue = pertaxvalue;
    }

    public String getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(String totalQty) {
        this.totalQty = totalQty;
    }

    public String[] getDisinvoiceAmt() {
        return disinvoiceAmt;
    }

    public void setDisinvoiceAmt(String[] disinvoiceAmt) {
        this.disinvoiceAmt = disinvoiceAmt;
    }

    public String[] getDisTaxvalue() {
        return disTaxvalue;
    }

    public void setDisTaxvalue(String[] disTaxvalue) {
        this.disTaxvalue = disTaxvalue;
    }

    public String[] getDisPerTax() {
        return disPerTax;
    }

    public void setDisPerTax(String[] disPerTax) {
        this.disPerTax = disPerTax;
    }

    public String getSubLrId() {
        return subLrId;
    }

    public void setSubLrId(String subLrId) {
        this.subLrId = subLrId;
    }

    public String getSubLr() {
        return subLr;
    }

    public void setSubLr(String subLr) {
        this.subLr = subLr;
    }

    public String[] getStockIds() {
        return stockIds;
    }

    public void setStockIds(String[] stockIds) {
        this.stockIds = stockIds;
    }

    public String[] getItemIds() {
        return itemIds;
    }

    public void setItemIds(String[] itemIds) {
        this.itemIds = itemIds;
    }

    public String[] getSerialIds() {
        return serialIds;
    }

    public void setSerialIds(String[] serialIds) {
        this.serialIds = serialIds;
    }

    public String getInvoiceAmounts() {
        return invoiceAmounts;
    }

    public void setInvoiceAmounts(String invoiceAmounts) {
        this.invoiceAmounts = invoiceAmounts;
    }

    public String getPretaxValues() {
        return pretaxValues;
    }

    public void setPretaxValues(String pretaxValues) {
        this.pretaxValues = pretaxValues;
    }

    public String getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public String[] getSupplyId() {
        return supplyId;
    }

    public void setSupplyId(String[] supplyId) {
        this.supplyId = supplyId;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getVol() {
        return vol;
    }

    public void setVol(String vol) {
        this.vol = vol;
    }

    public String getTaxVol() {
        return taxVol;
    }

    public void setTaxVol(String taxVol) {
        this.taxVol = taxVol;
    }

    public String getWgt() {
        return wgt;
    }

    public void setWgt(String wgt) {
        this.wgt = wgt;
    }

    public String getInvoiceNos() {
        return invoiceNos;
    }

    public void setInvoiceNos(String invoiceNos) {
        this.invoiceNos = invoiceNos;
    }

    public String getTotQty() {
        return totQty;
    }

    public void setTotQty(String totQty) {
        this.totQty = totQty;
    }

    public String getTotVol() {
        return totVol;
    }

    public void setTotVol(String totVol) {
        this.totVol = totVol;
    }

    public String getTotWgt() {
        return totWgt;
    }

    public void setTotWgt(String totWgt) {
        this.totWgt = totWgt;
    }

    public String getTotTaxVol() {
        return totTaxVol;
    }

    public void setTotTaxVol(String totTaxVol) {
        this.totTaxVol = totTaxVol;
    }

    public String[] getTaxValE() {
        return taxValE;
    }

    public void setTaxValE(String[] taxValE) {
        this.taxValE = taxValE;
    }

    public String[] getVolE() {
        return volE;
    }

    public void setVolE(String[] volE) {
        this.volE = volE;
    }

    public String[] getWgtE() {
        return wgtE;
    }

    public void setWgtE(String[] wgtE) {
        this.wgtE = wgtE;
    }

    public String[] getCustomerIdE() {
        return customerIdE;
    }

    public void setCustomerIdE(String[] customerIdE) {
        this.customerIdE = customerIdE;
    }

    public String[] getInvoiceNoE() {
        return invoiceNoE;
    }

    public void setInvoiceNoE(String[] invoiceNoE) {
        this.invoiceNoE = invoiceNoE;
    }

    public String[] getQtyE() {
        return qtyE;
    }

    public void setQtyE(String[] qtyE) {
        this.qtyE = qtyE;
    }

    public String[] getOrderIdE() {
        return orderIdE;
    }

    public void setOrderIdE(String[] orderIdE) {
        this.orderIdE = orderIdE;
    }

    public String getPlannedDate() {
        return plannedDate;
    }

    public void setPlannedDate(String plannedDate) {
        this.plannedDate = plannedDate;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSuppId() {
        return suppId;
    }

    public void setSuppId(String suppId) {
        this.suppId = suppId;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getSuppName() {
        return suppName;
    }

    public void setSuppName(String suppName) {
        this.suppName = suppName;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getPlantCode() {
        return plantCode;
    }

    public void setPlantCode(String plantCode) {
        this.plantCode = plantCode;
    }

    public String getShipToPartycode() {
        return shipToPartycode;
    }

    public void setShipToPartycode(String shipToPartycode) {
        this.shipToPartycode = shipToPartycode;
    }

    public String getShipToPartyName() {
        return shipToPartyName;
    }

    public void setShipToPartyName(String shipToPartyName) {
        this.shipToPartyName = shipToPartyName;
    }

    public String getMaterialCode() {
        return materialCode;
    }

    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    public String getBilledQuantity() {
        return billedQuantity;
    }

    public void setBilledQuantity(String billedQuantity) {
        this.billedQuantity = billedQuantity;
    }

    public String getBasicPrice() {
        return basicPrice;
    }

    public void setBasicPrice(String basicPrice) {
        this.basicPrice = basicPrice;
    }

    public String getmRP() {
        return mRP;
    }

    public void setmRP(String mRP) {
        this.mRP = mRP;
    }

    public String getPretaxValue() {
        return pretaxValue;
    }

    public void setPretaxValue(String pretaxValue) {
        this.pretaxValue = pretaxValue;
    }

    public String getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(String taxValue) {
        this.taxValue = taxValue;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getPurchaseOrderNo() {
        return purchaseOrderNo;
    }

    public void setPurchaseOrderNo(String purchaseOrderNo) {
        this.purchaseOrderNo = purchaseOrderNo;
    }

    public String getPurchaseOrderDate() {
        return purchaseOrderDate;
    }

    public void setPurchaseOrderDate(String purchaseOrderDate) {
        this.purchaseOrderDate = purchaseOrderDate;
    }

    public String getSaleOrderNo() {
        return saleOrderNo;
    }

    public void setSaleOrderNo(String saleOrderNo) {
        this.saleOrderNo = saleOrderNo;
    }

    public String getSaleOrderDate() {
        return saleOrderDate;
    }

    public void setSaleOrderDate(String saleOrderDate) {
        this.saleOrderDate = saleOrderDate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String gethSNCode() {
        return hSNCode;
    }

    public void sethSNCode(String hSNCode) {
        this.hSNCode = hSNCode;
    }

    public String getcGSTAmount() {
        return cGSTAmount;
    }

    public void setcGSTAmount(String cGSTAmount) {
        this.cGSTAmount = cGSTAmount;
    }

    public String getsGSTAmount() {
        return sGSTAmount;
    }

    public void setsGSTAmount(String sGSTAmount) {
        this.sGSTAmount = sGSTAmount;
    }

    public String getiGSTAmount() {
        return iGSTAmount;
    }

    public void setiGSTAmount(String iGSTAmount) {
        this.iGSTAmount = iGSTAmount;
    }

    public String getDeliveryNumber() {
        return deliveryNumber;
    }

    public void setDeliveryNumber(String deliveryNumber) {
        this.deliveryNumber = deliveryNumber;
    }

    public String getMatGrptext() {
        return matGrptext;
    }

    public void setMatGrptext(String matGrptext) {
        this.matGrptext = matGrptext;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getVechileRegNo() {
        return vechileRegNo;
    }

    public void setVechileRegNo(String vechileRegNo) {
        this.vechileRegNo = vechileRegNo;
    }

    public String getDispatchNo() {
        return dispatchNo;
    }

    public void setDispatchNo(String dispatchNo) {
        this.dispatchNo = dispatchNo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTatId() {
        return tatId;
    }

    public void setTatId(String tatId) {
        this.tatId = tatId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getDispatchid() {
        return dispatchid;
    }

    public void setDispatchid(String dispatchid) {
        this.dispatchid = dispatchid;
    }

    public String getTansferid() {
        return Tansferid;
    }

    public void setTansferid(String Tansferid) {
        this.Tansferid = Tansferid;
    }

    public String getLoadingStart() {
        return loadingStart;
    }

    public void setLoadingStart(String loadingStart) {
        this.loadingStart = loadingStart;
    }

    public String getPendingReason() {
        return pendingReason;
    }

    public void setPendingReason(String pendingReason) {
        this.pendingReason = pendingReason;
    }

    public String getLoadingEnd() {
        return loadingEnd;
    }

    public void setLoadingEnd(String loadingEnd) {
        this.loadingEnd = loadingEnd;
    }

    public String[] getInvoiceIds() {
        return invoiceIds;
    }

    public void setInvoiceIds(String[] invoiceIds) {
        this.invoiceIds = invoiceIds;
    }

    public String[] getCustomerIds() {
        return customerIds;
    }

    public void setCustomerIds(String[] customerIds) {
        this.customerIds = customerIds;
    }

    public String[] getQuantitys() {
        return quantitys;
    }

    public void setQuantitys(String[] quantitys) {
        this.quantitys = quantitys;
    }

    public String[] getPendingReasons() {
        return pendingReasons;
    }

    public void setPendingReasons(String[] pendingReasons) {
        this.pendingReasons = pendingReasons;
    }

    public String[] getDcQty() {
        return dcQty;
    }

    public void setDcQty(String[] dcQty) {
        this.dcQty = dcQty;
    }

    public String getLrdate() {
        return lrdate;
    }

    public void setLrdate(String lrdate) {
        this.lrdate = lrdate;
    }

    public String getLrtime() {
        return lrtime;
    }

    public void setLrtime(String lrtime) {
        this.lrtime = lrtime;
    }

    public String getLrNumber() {
        return lrNumber;
    }

    public void setLrNumber(String lrNumber) {
        this.lrNumber = lrNumber;
    }

    public String getSublrNumber() {
        return sublrNumber;
    }

    public void setSublrNumber(String sublrNumber) {
        this.sublrNumber = sublrNumber;
    }

    public String getConsinorName() {
        return consinorName;
    }

    public void setConsinorName(String consinorName) {
        this.consinorName = consinorName;
    }

    public String getConsinorAddress() {
        return consinorAddress;
    }

    public void setConsinorAddress(String consinorAddress) {
        this.consinorAddress = consinorAddress;
    }

    public String getConsinorCity() {
        return consinorCity;
    }

    public void setConsinorCity(String consinorCity) {
        this.consinorCity = consinorCity;
    }

    public String getConsinorPhone() {
        return consinorPhone;
    }

    public void setConsinorPhone(String consinorPhone) {
        this.consinorPhone = consinorPhone;
    }

    public String getConsinorGst() {
        return consinorGst;
    }

    public void setConsinorGst(String consinorGst) {
        this.consinorGst = consinorGst;
    }

    public String getBoxQty() {
        return boxQty;
    }

    public void setBoxQty(String boxQty) {
        this.boxQty = boxQty;
    }

    public String getCft() {
        return cft;
    }

    public void setCft(String cft) {
        this.cft = cft;
    }

    public String getPackQTy() {
        return packQTy;
    }

    public void setPackQTy(String packQTy) {
        this.packQTy = packQTy;
    }

    public String getActualwgt() {
        return actualwgt;
    }

    public void setActualwgt(String actualwgt) {
        this.actualwgt = actualwgt;
    }

    public String[] getItemsides() {
        return itemsides;
    }

    public void setItemsides(String[] itemsides) {
        this.itemsides = itemsides;
    }

    public String[] getPendingQty() {
        return pendingQty;
    }

    public void setPendingQty(String[] pendingQty) {
        this.pendingQty = pendingQty;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getRackId() {
        return rackId;
    }

    public void setRackId(String rackId) {
        this.rackId = rackId;
    }

    public String getRackName() {
        return rackName;
    }

    public void setRackName(String rackName) {
        this.rackName = rackName;
    }

    public String getStorageWhid() {
        return storageWhid;
    }

    public void setStorageWhid(String storageWhid) {
        this.storageWhid = storageWhid;
    }

    public String getGrnQty() {
        return grnQty;
    }

    public void setGrnQty(String grnQty) {
        this.grnQty = grnQty;
    }

    public String[] getLocatioId() {
        return locatioId;
    }

    public void setLocatioId(String[] locatioId) {
        this.locatioId = locatioId;
    }

    public String[] getRackIdE() {
        return rackIdE;
    }

    public void setRackIdE(String[] rackIdE) {
        this.rackIdE = rackIdE;
    }

    public String getSerialId() {
        return serialId;
    }

    public void setSerialId(String serialId) {
        this.serialId = serialId;
    }

    public String getGateInwardDate() {
        return gateInwardDate;
    }

    public void setGateInwardDate(String gateInwardDate) {
        this.gateInwardDate = gateInwardDate;
    }

    public String getGateInwardNo() {
        return gateInwardNo;
    }

    public void setGateInwardNo(String gateInwardNo) {
        this.gateInwardNo = gateInwardNo;
    }

    public String getProcessType() {
        return processType;
    }

    public void setProcessType(String processType) {
        this.processType = processType;
    }

    public String getModeofTransport() {
        return modeofTransport;
    }

    public void setModeofTransport(String modeofTransport) {
        this.modeofTransport = modeofTransport;
    }

    public String getTransporterCode() {
        return transporterCode;
    }

    public void setTransporterCode(String transporterCode) {
        this.transporterCode = transporterCode;
    }

    public String getTransporterName() {
        return transporterName;
    }

    public void setTransporterName(String transporterName) {
        this.transporterName = transporterName;
    }

    public String getGiQuantity() {
        return giQuantity;
    }

    public void setGiQuantity(String giQuantity) {
        this.giQuantity = giQuantity;
    }

    public String getBillNum() {
        return billNum;
    }

    public void setBillNum(String billNum) {
        this.billNum = billNum;
    }

    public String getMatDocNo() {
        return matDocNo;
    }

    public void setMatDocNo(String matDocNo) {
        this.matDocNo = matDocNo;
    }

    public String getMatDocDate() {
        return matDocDate;
    }

    public void setMatDocDate(String matDocDate) {
        this.matDocDate = matDocDate;
    }

    public String getMatDocYear() {
        return matDocYear;
    }

    public void setMatDocYear(String matDocYear) {
        this.matDocYear = matDocYear;
    }

    public String getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(String postingDate) {
        this.postingDate = postingDate;
    }

    public String getSuppPlantCode() {
        return suppPlantCode;
    }

    public void setSuppPlantCode(String suppPlantCode) {
        this.suppPlantCode = suppPlantCode;
    }

    public String getSuppPlantDesc() {
        return suppPlantDesc;
    }

    public void setSuppPlantDesc(String suppPlantDesc) {
        this.suppPlantDesc = suppPlantDesc;
    }

    public String getRecPlantCode() {
        return recPlantCode;
    }

    public void setRecPlantCode(String recPlantCode) {
        this.recPlantCode = recPlantCode;
    }

    public String getRecPlantDesc() {
        return recPlantDesc;
    }

    public void setRecPlantDesc(String recPlantDesc) {
        this.recPlantDesc = recPlantDesc;
    }

    public String getGrQty() {
        return grQty;
    }

    public void setGrQty(String grQty) {
        this.grQty = grQty;
    }

    public String getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation;
    }

    public String getDeliveryNo() {
        return deliveryNo;
    }

    public void setDeliveryNo(String deliveryNo) {
        this.deliveryNo = deliveryNo;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCancelledNo() {
        return cancelledNo;
    }

    public void setCancelledNo(String cancelledNo) {
        this.cancelledNo = cancelledNo;
    }

    public String getGrnID() {
        return grnID;
    }

    public void setGrnID(String grnID) {
        this.grnID = grnID;
    }

    public String getCancelledNumber() {
        return cancelledNumber;
    }

    public void setCancelledNumber(String cancelledNumber) {
        this.cancelledNumber = cancelledNumber;
    }

    public String getRecplantDes() {
        return recplantDes;
    }

    public void setRecplantDes(String recplantDes) {
        this.recplantDes = recplantDes;
    }

    public String getRecplantCode() {
        return recplantCode;
    }

    public void setRecplantCode(String recplantCode) {
        this.recplantCode = recplantCode;
    }

    public String getSupPlantDes() {
        return supPlantDes;
    }

    public void setSupPlantDes(String supPlantDes) {
        this.supPlantDes = supPlantDes;
    }

    public String getSupPlantCode() {
        return supPlantCode;
    }

    public void setSupPlantCode(String supPlantCode) {
        this.supPlantCode = supPlantCode;
    }

    public String getMaterialDes() {
        return materialDes;
    }

    public void setMaterialDes(String materialDes) {
        this.materialDes = materialDes;
    }

    public String getVehicleNO() {
        return vehicleNO;
    }

    public void setVehicleNO(String vehicleNO) {
        this.vehicleNO = vehicleNO;
    }

    public String getCycleId() {
        return cycleId;
    }

    public void setCycleId(String cycleId) {
        this.cycleId = cycleId;
    }

    public String getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(String productTypeId) {
        this.productTypeId = productTypeId;
    }

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    public String getLrDate() {
        return lrDate;
    }

    public void setLrDate(String lrDate) {
        this.lrDate = lrDate;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getInbound() {
        return inbound;
    }

    public void setInbound(String inbound) {
        this.inbound = inbound;
    }

    public String getPrexo() {
        return prexo;
    }

    public void setPrexo(String prexo) {
        this.prexo = prexo;
    }

    public String getRvp() {
        return rvp;
    }

    public void setRvp(String rvp) {
        this.rvp = rvp;
    }

    public String[] getShipmentTypes() {
        return shipmentTypes;
    }

    public void setShipmentTypes(String[] shipmentTypes) {
        this.shipmentTypes = shipmentTypes;
    }

    public String[] getReconcileIds() {
        return reconcileIds;
    }

    public void setReconcileIds(String[] reconcileIds) {
        this.reconcileIds = reconcileIds;
    }

    public String getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(String depositAmount) {
        this.depositAmount = depositAmount;
    }

    public String getActiveType() {
        return activeType;
    }

    public void setActiveType(String activeType) {
        this.activeType = activeType;
    }

    public String getDepositType() {
        return depositType;
    }

    public void setDepositType(String depositType) {
        this.depositType = depositType;
    }

    public String getDepositCode() {
        return depositCode;
    }

    public void setDepositCode(String depositCode) {
        this.depositCode = depositCode;
    }

    public String getDepositId() {
        return depositId;
    }

    public void setDepositId(String depositId) {
        this.depositId = depositId;
    }

    public String[] getDeCods() {
        return deCods;
    }

    public void setDeCods(String[] deCods) {
        this.deCods = deCods;
    }

    public String[] getCardAmounts() {
        return cardAmounts;
    }

    public void setCardAmounts(String[] cardAmounts) {
        this.cardAmounts = cardAmounts;
    }

    public String[] getPrexoAmounts() {
        return prexoAmounts;
    }

    public void setPrexoAmounts(String[] prexoAmounts) {
        this.prexoAmounts = prexoAmounts;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPodId() {
        return podId;
    }

    public void setPodId(String podId) {
        this.podId = podId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getPodStatus() {
        return podStatus;
    }

    public void setPodStatus(String podStatus) {
        this.podStatus = podStatus;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getReconcileId() {
        return reconcileId;
    }

    public void setReconcileId(String reconcileId) {
        this.reconcileId = reconcileId;
    }

    public String getRunsheetId() {
        return runsheetId;
    }

    public void setRunsheetId(String runsheetId) {
        this.runsheetId = runsheetId;
    }

    public String getReconcileCode() {
        return reconcileCode;
    }

    public void setReconcileCode(String reconcileCode) {
        this.reconcileCode = reconcileCode;
    }

    public String getTripStatusId() {
        return tripStatusId;
    }

    public void setTripStatusId(String tripStatusId) {
        this.tripStatusId = tripStatusId;
    }

    public String getTotalCod() {
        return totalCod;
    }

    public void setTotalCod(String totalCod) {
        this.totalCod = totalCod;
    }

    public String getTotalDeCod() {
        return totalDeCod;
    }

    public void setTotalDeCod(String totalDeCod) {
        this.totalDeCod = totalDeCod;
    }

    public String getTotalCardAmount() {
        return totalCardAmount;
    }

    public void setTotalCardAmount(String totalCardAmount) {
        this.totalCardAmount = totalCardAmount;
    }

    public String getTotalPrexoAmount() {
        return totalPrexoAmount;
    }

    public void setTotalPrexoAmount(String totalPrexoAmount) {
        this.totalPrexoAmount = totalPrexoAmount;
    }

    public String getCardAmount() {
        return cardAmount;
    }

    public void setCardAmount(String cardAmount) {
        this.cardAmount = cardAmount;
    }

    public String getPrexoAmount() {
        return prexoAmount;
    }

    public void setPrexoAmount(String prexoAmount) {
        this.prexoAmount = prexoAmount;
    }

    public String getDeCod() {
        return deCod;
    }

    public void setDeCod(String deCod) {
        this.deCod = deCod;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getRejectionId() {
        return rejectionId;
    }

    public void setRejectionId(String rejectionId) {
        this.rejectionId = rejectionId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDispatchId() {
        return dispatchId;
    }

    public void setDispatchId(String dispatchId) {
        this.dispatchId = dispatchId;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOnHoldByOpsDate() {
        return onHoldByOpsDate;
    }

    public void setOnHoldByOpsDate(String onHoldByOpsDate) {
        this.onHoldByOpsDate = onHoldByOpsDate;
    }

    public String getBagId() {
        return bagId;
    }

    public void setBagId(String bagId) {
        this.bagId = bagId;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getDeliveryHub() {
        return deliveryHub;
    }

    public void setDeliveryHub(String deliveryHub) {
        this.deliveryHub = deliveryHub;
    }

    public String getAssignedExecutive() {
        return assignedExecutive;
    }

    public void setAssignedExecutive(String assignedExecutive) {
        this.assignedExecutive = assignedExecutive;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getCurrentHub() {
        return currentHub;
    }

    public void setCurrentHub(String currentHub) {
        this.currentHub = currentHub;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getLastReceived() {
        return lastReceived;
    }

    public void setLastReceived(String lastReceived) {
        this.lastReceived = lastReceived;
    }

    public String getShipmentTierType() {
        return shipmentTierType;
    }

    public void setShipmentTierType(String shipmentTierType) {
        this.shipmentTierType = shipmentTierType;
    }

    public String getReverseShipmentId() {
        return reverseShipmentId;
    }

    public void setReverseShipmentId(String reverseShipmentId) {
        this.reverseShipmentId = reverseShipmentId;
    }

    public String getBagStatus() {
        return bagStatus;
    }

    public void setBagStatus(String bagStatus) {
        this.bagStatus = bagStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLatestStatus() {
        return latestStatus;
    }

    public void setLatestStatus(String latestStatus) {
        this.latestStatus = latestStatus;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getLatestUpdateDateTime() {
        return latestUpdateDateTime;
    }

    public void setLatestUpdateDateTime(String latestUpdateDateTime) {
        this.latestUpdateDateTime = latestUpdateDateTime;
    }

    public String getFirstReceivedHub() {
        return firstReceivedHub;
    }

    public void setFirstReceivedHub(String firstReceivedHub) {
        this.firstReceivedHub = firstReceivedHub;
    }

    public String getFirstReceiveDateTime() {
        return firstReceiveDateTime;
    }

    public void setFirstReceiveDateTime(String firstReceiveDateTime) {
        this.firstReceiveDateTime = firstReceiveDateTime;
    }

    public String getHubNotes() {
        return hubNotes;
    }

    public void setHubNotes(String hubNotes) {
        this.hubNotes = hubNotes;
    }

    public String getCsNotes() {
        return csNotes;
    }

    public void setCsNotes(String csNotes) {
        this.csNotes = csNotes;
    }

    public String getNumberofAttempts() {
        return numberofAttempts;
    }

    public void setNumberofAttempts(String numberofAttempts) {
        this.numberofAttempts = numberofAttempts;
    }

    public String getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(String consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String getCustomerPromiseDate() {
        return customerPromiseDate;
    }

    public void setCustomerPromiseDate(String customerPromiseDate) {
        this.customerPromiseDate = customerPromiseDate;
    }

    public String getLogisticsPromiseDate() {
        return logisticsPromiseDate;
    }

    public void setLogisticsPromiseDate(String logisticsPromiseDate) {
        this.logisticsPromiseDate = logisticsPromiseDate;
    }

    public String getOnHoldByOpsReason() {
        return onHoldByOpsReason;
    }

    public void setOnHoldByOpsReason(String onHoldByOpsReason) {
        this.onHoldByOpsReason = onHoldByOpsReason;
    }

    public String getFirstAssignedHubName() {
        return firstAssignedHubName;
    }

    public void setFirstAssignedHubName(String firstAssignedHubName) {
        this.firstAssignedHubName = firstAssignedHubName;
    }

    public String getDeliveryPinCode() {
        return deliveryPinCode;
    }

    public void setDeliveryPinCode(String deliveryPinCode) {
        this.deliveryPinCode = deliveryPinCode;
    }

    public String getLastReceivedDateTime() {
        return lastReceivedDateTime;
    }

    public void setLastReceivedDateTime(String lastReceivedDateTime) {
        this.lastReceivedDateTime = lastReceivedDateTime;
    }

    public String getPickupaddress() {
        return pickupaddress;
    }

    public void setPickupaddress(String pickupaddress) {
        this.pickupaddress = pickupaddress;
    }

    public String getItemNos() {
        return itemNos;
    }

    public void setItemNos(String itemNos) {
        this.itemNos = itemNos;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getLastStatus() {
        return lastStatus;
    }

    public void setLastStatus(String lastStatus) {
        this.lastStatus = lastStatus;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getLrId() {
        return lrId;
    }

    public void setLrId(String lrId) {
        this.lrId = lrId;
    }

    public String getLrNo() {
        return lrNo;
    }

    public void setLrNo(String lrNo) {
        this.lrNo = lrNo;
    }

    public String getEwayBillNo() {
        return ewayBillNo;
    }

    public void setEwayBillNo(String ewayBillNo) {
        this.ewayBillNo = ewayBillNo;
    }

    public String getEwayExpiry() {
        return ewayExpiry;
    }

    public void setEwayExpiry(String ewayExpiry) {
        this.ewayExpiry = ewayExpiry;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getBillDoc() {
        return billDoc;
    }

    public void setBillDoc(String billDoc) {
        this.billDoc = billDoc;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    public void setPurchaseOrderNumber(String purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }

    public String getMatlGroup() {
        return matlGroup;
    }

    public void setMatlGroup(String matlGroup) {
        this.matlGroup = matlGroup;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getMaterialDescription() {
        return materialDescription;
    }

    public void setMaterialDescription(String materialDescription) {
        this.materialDescription = materialDescription;
    }

    public String getBillQty() {
        return billQty;
    }

    public void setBillQty(String billQty) {
        this.billQty = billQty;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getShipTo() {
        return shipTo;
    }

    public void setShipTo(String shipTo) {
        this.shipTo = shipTo;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getGstinNumber() {
        return gstinNumber;
    }

    public void setGstinNumber(String gstinNumber) {
        this.gstinNumber = gstinNumber;
    }

    public String getGrossValue() {
        return grossValue;
    }

    public void setGrossValue(String grossValue) {
        this.grossValue = grossValue;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getRunSheetId() {
        return runSheetId;
    }

    public void setRunSheetId(String runSheetId) {
        this.runSheetId = runSheetId;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public String getCodAmount() {
        return codAmount;
    }

    public void setCodAmount(String codAmount) {
        this.codAmount = codAmount;
    }

    public String getAssignedUser() {
        return assignedUser;
    }

    public void setAssignedUser(String assignedUser) {
        this.assignedUser = assignedUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getVertical() {
        return vertical;
    }

    public void setVertical(String vertical) {
        this.vertical = vertical;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(String productDetails) {
        this.productDetails = productDetails;
    }

    public String getAsnid() {
        return asnid;
    }

    public void setAsnid(String asnid) {
        this.asnid = asnid;
    }

    public String getLoanid() {
        return loanid;
    }

    public void setLoanid(String loanid) {
        this.loanid = loanid;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStockId() {
        return stockId;
    }

    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getPoDate() {
        return poDate;
    }

    public void setPoDate(String poDate) {
        this.poDate = poDate;
    }

    public String getPoRequestId() {
        return PoRequestId;
    }

    public void setPoRequestId(String PoRequestId) {
        this.PoRequestId = PoRequestId;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String Quantity) {
        this.Quantity = Quantity;
    }

    public String getWarehouseToId() {
        return warehouseToId;
    }

    public void setWarehouseToId(String warehouseToId) {
        this.warehouseToId = warehouseToId;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getCancelled1() {
        return cancelled1;
    }

    public void setCancelled1(String cancelled1) {
        this.cancelled1 = cancelled1;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getTripQty() {
        return tripQty;
    }

    public void setTripQty(String tripQty) {
        this.tripQty = tripQty;
    }

    public String getOfd() {
        return ofd;
    }

    public void setOfd(String ofd) {
        this.ofd = ofd;
    }

    public String getDelivered() {
        return delivered;
    }

    public void setDelivered(String delivered) {
        this.delivered = delivered;
    }

    public String getReturned() {
        return returned;
    }

    public void setReturned(String returned) {
        this.returned = returned;
    }

    public String getCancelled() {
        return cancelled;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getGrnId1() {
        return grnId1;
    }

    public void setGrnId1(String grnId1) {
        this.grnId1 = grnId1;
    }

    public String getSerials() {
        return serials;
    }

    public void setSerials(String serials) {
        this.serials = serials;
    }

    public String getTempIds() {
        return tempIds;
    }

    public void setTempIds(String tempIds) {
        this.tempIds = tempIds;
    }

    public String getUoms() {
        return uoms;
    }

    public void setUoms(String uoms) {
        this.uoms = uoms;
    }

    public String getBins() {
        return bins;
    }

    public void setBins(String bins) {
        this.bins = bins;
    }

    public String getProCons() {
        return proCons;
    }

    public void setProCons(String proCons) {
        this.proCons = proCons;
    }

    public String getPrevPoQuantity() {
        return prevPoQuantity;
    }

    public void setPrevPoQuantity(String prevPOQuantity) {
        this.prevPoQuantity = prevPOQuantity;
    }

    public Double getAsnQty() {
        return asnQty;
    }

    public void setAsnQty(Double asnQty) {
        this.asnQty = asnQty;
    }

    public String getSerialNoTemp1() {
        return serialNoTemp1;
    }

    public void setSerialNoTemp1(String serialNoTemp1) {
        this.serialNoTemp1 = serialNoTemp1;
    }

    public String getBinIdTemp1() {
        return binIdTemp1;
    }

    public void setBinIdTemp1(String binIdTemp1) {
        this.binIdTemp1 = binIdTemp1;
    }

    public String getProConTemp1() {
        return proConTemp1;
    }

    public void setProConTemp1(String proConTemp1) {
        this.proConTemp1 = proConTemp1;
    }

    public String getSerialNoTemp() {
        return serialNoTemp;
    }

    public void setSerialNoTemp(String serialNoTemp) {
        this.serialNoTemp = serialNoTemp;
    }

    public String getBinIdTemp() {
        return binIdTemp;
    }

    public void setBinIdTemp(String binIdTemp) {
        this.binIdTemp = binIdTemp;
    }

    public String getProConTemp() {
        return proConTemp;
    }

    public void setProConTemp(String proConTemp) {
        this.proConTemp = proConTemp;
    }

    public String[] getBinIds() {
        return binIds;
    }

    public void setBinIds(String[] binIds) {
        this.binIds = binIds;
    }

    public String getBinFlag() {
        return binFlag;
    }

    public void setBinFlag(String binFlag) {
        this.binFlag = binFlag;
    }

    public String getExcess() {
        return excess;
    }

    public void setExcess(String excess) {
        this.excess = excess;
    }

    public String getDamaged() {
        return damaged;
    }

    public void setDamaged(String damaged) {
        this.damaged = damaged;
    }

    public String getShortage() {
        return shortage;
    }

    public void setShortage(String shortage) {
        this.shortage = shortage;
    }

    public String getEmpUserId() {
        return empUserId;
    }

    public void setEmpUserId(String empUserId) {
        this.empUserId = empUserId;
    }

    public String getGtnDetId() {
        return gtnDetId;
    }

    public void setGtnDetId(String gtnDetId) {
        this.gtnDetId = gtnDetId;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getPoQuantity() {
        return poQuantity;
    }

    public void setPoQuantity(String poQuantity) {
        this.poQuantity = poQuantity;
    }

    public String getGtnNo() {
        return gtnNo;
    }

    public void setGtnNo(String gtnNo) {
        this.gtnNo = gtnNo;
    }

    public String getGtnId() {
        return gtnId;
    }

    public void setGtnId(String gtnId) {
        this.gtnId = gtnId;
    }

    public String[] getPoNos() {
        return poNos;
    }

    public void setPoNos(String[] poNos) {
        this.poNos = poNos;
    }

    public String[] getPoQty() {
        return poQty;
    }

    public void setPoQty(String[] poQty) {
        this.poQty = poQty;
    }

    public String getGtnTime() {
        return gtnTime;
    }

    public void setGtnTime(String gtnTime) {
        this.gtnTime = gtnTime;
    }

    public String getDock() {
        return dock;
    }

    public void setDock(String dock) {
        this.dock = dock;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCustId() {
        return CustId;
    }

    public void setCustId(String CustId) {
        this.CustId = CustId;
    }

    public String getPoName() {
        return poName;
    }

    public void setPoName(String poName) {
        this.poName = poName;
    }

    public String getGtnDate() {
        return gtnDate;
    }

    public void setGtnDate(String gtnDate) {
        this.gtnDate = gtnDate;
    }

    public String getProCode() {
        return proCode;
    }

    public void setProCode(String proCode) {
        this.proCode = proCode;
    }

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getUom1() {
        return uom1;
    }

    public void setUom1(String uom1) {
        this.uom1 = uom1;
    }

    public String getInpQty() {
        return inpQty;
    }

    public void setInpQty(String inpQty) {
        this.inpQty = inpQty;
    }

    public String[] getIpQty() {
        return ipQty;
    }

    public void setIpQty(String[] ipQty) {
        this.ipQty = ipQty;
    }

    public String[] getUom() {
        return uom;
    }

    public void setUom(String[] uom) {
        this.uom = uom;
    }

    public String[] getProCond() {
        return proCond;
    }

    public void setProCond(String[] proCond) {
        this.proCond = proCond;
    }

    public String getProCon() {
        return proCon;
    }

    public void setProCon(String proCon) {
        this.proCon = proCon;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String CustomerName) {
        this.CustomerName = CustomerName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getAsnId() {
        return asnId;
    }

    public void setAsnId(String asnId) {
        this.asnId = asnId;
    }

    public String getAsnNo() {
        return asnNo;
    }

    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    public String getAsnDate() {
        return asnDate;
    }

    public void setAsnDate(String asnDate) {
        this.asnDate = asnDate;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getAsnPath() {
        return asnPath;
    }

    public void setAsnPath(String asnPath) {
        this.asnPath = asnPath;
    }

    public String getAsnRemarks() {
        return asnRemarks;
    }

    public void setAsnRemarks(String asnRemarks) {
        this.asnRemarks = asnRemarks;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public String getActualQuantity() {
        return actualQuantity;
    }

    public void setActualQuantity(String actualQuantity) {
        this.actualQuantity = actualQuantity;
    }

    public String getAsnDetId() {
        return asnDetId;
    }

    public void setAsnDetId(String asnDetId) {
        this.asnDetId = asnDetId;
    }

    public String getDelRequestId() {
        return delRequestId;
    }

    public void setDelRequestId(String delRequestId) {
        this.delRequestId = delRequestId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getReMarks() {
        return reMarks;
    }

    public void setReMarks(String reMarks) {
        this.reMarks = reMarks;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getLoanProposalId() {
        return loanProposalId;
    }

    public void setLoanProposalId(String loanProposalId) {
        this.loanProposalId = loanProposalId;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getBatchDate() {
        return batchDate;
    }

    public void setBatchDate(String batchDate) {
        this.batchDate = batchDate;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public double getUnusedQty() {
        return unusedQty;
    }

    public void setUnusedQty(double unusedQty) {
        this.unusedQty = unusedQty;
    }

    public double getActQty() {
        return actQty;
    }

    public void setActQty(double actQty) {
        this.actQty = actQty;
    }

    public double getReqQty() {
        return reqQty;
    }

    public void setReqQty(double reqQty) {
        this.reqQty = reqQty;
    }

    public String getBinId() {
        return binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public String getBinName() {
        return binName;
    }

    public void setBinName(String binName) {
        this.binName = binName;
    }

    public String getWhId() {
        return whId;
    }

    public void setWhId(String whId) {
        this.whId = whId;
    }

    public String getEwaybillNo() {
        return ewaybillNo;
    }

    public void setEwaybillNo(String ewaybillNo) {
        this.ewaybillNo = ewaybillNo;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getGrnId() {
        return grnId;
    }

    public void setGrnId(String grnId) {
        this.grnId = grnId;
    }

    public String getRequestQty() {
        return requestQty;
    }

    public void setRequestQty(String requestQty) {
        this.requestQty = requestQty;
    }

    public String[] getSerialNos() {
        return serialNos;
    }

    public void setSerialNos(String[] serialNos) {
        this.serialNos = serialNos;
    }

    public String[] getWhIds() {
        return whIds;
    }

    public void setWhIds(String[] whIds) {
        this.whIds = whIds;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getGrnNo() {
        return grnNo;
    }

    public void setGrnNo(String grnNo) {
        this.grnNo = grnNo;
    }

    public String getGrnDate() {
        return grnDate;
    }

    public void setGrnDate(String grnDate) {
        this.grnDate = grnDate;
    }

    public String getAsnReqId() {
        return asnReqId;
    }

    public void setAsnReqId(String asnReqId) {
        this.asnReqId = asnReqId;
    }

    public String getExcessQty() {
        return excessQty;
    }

    public void setExcessQty(String excessQty) {
        this.excessQty = excessQty;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    public String getLrnNo() {
        return lrnNo;
    }

    public void setLrnNo(String lrnNo) {
        this.lrnNo = lrnNo;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getPreTaxValue() {
        return preTaxValue;
    }

    public void setPreTaxValue(String preTaxValue) {
        this.preTaxValue = preTaxValue;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDispatchDetailId() {
        return dispatchDetailId;
    }

    public void setDispatchDetailId(String dispatchDetailId) {
        this.dispatchDetailId = dispatchDetailId;
    }

    public String getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(String freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getStatusType() {
        return statusType;
    }

    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

    public String getOtherAmt() {
        return otherAmt;
    }

    public void setOtherAmt(String otherAmt) {
        this.otherAmt = otherAmt;
    }

    public String getUnloadingAmt() {
        return unloadingAmt;
    }

    public void setUnloadingAmt(String unloadingAmt) {
        this.unloadingAmt = unloadingAmt;
    }

    public String getHaltingAmt() {
        return haltingAmt;
    }

    public void setHaltingAmt(String haltingAmt) {
        this.haltingAmt = haltingAmt;
    }

    public String getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(String totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public String getPlannedTime() {
        return plannedTime;
    }

    public void setPlannedTime(String plannedTime) {
        this.plannedTime = plannedTime;
    }

    public String getPodName1() {
        return podName1;
    }

    public void setPodName1(String podName1) {
        this.podName1 = podName1;
    }

    public String getPodName2() {
        return podName2;
    }

    public void setPodName2(String podName2) {
        this.podName2 = podName2;
    }

    public String getPodName3() {
        return podName3;
    }

    public void setPodName3(String podName3) {
        this.podName3 = podName3;
    }

    public String getRemarks1() {
        return remarks1;
    }

    public void setRemarks1(String remarks1) {
        this.remarks1 = remarks1;
    }

    public String getRemarks2() {
        return remarks2;
    }

    public void setRemarks2(String remarks2) {
        this.remarks2 = remarks2;
    }

    public String getRemarks3() {
        return remarks3;
    }

    public void setRemarks3(String remarks3) {
        this.remarks3 = remarks3;
    }

    public String getStartKm() {
        return startKm;
    }

    public void setStartKm(String startKm) {
        this.startKm = startKm;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getWareHouseAddress() {
        return wareHouseAddress;
    }

    public void setWareHouseAddress(String wareHouseAddress) {
        this.wareHouseAddress = wareHouseAddress;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getTotalCrates() {
        return totalCrates;
    }

    public void setTotalCrates(String totalCrates) {
        this.totalCrates = totalCrates;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }

    public String getApprovedUser() {
        return approvedUser;
    }

    public void setApprovedUser(String approvedUser) {
        this.approvedUser = approvedUser;
    }

    public String getAuthUser() {
        return authUser;
    }

    public void setAuthUser(String authUser) {
        this.authUser = authUser;
    }

    public String getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(String approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getAuthDate() {
        return authDate;
    }

    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public String getHouseKeeping() {
        return houseKeeping;
    }

    public void setHouseKeeping(String houseKeeping) {
        this.houseKeeping = houseKeeping;
    }

    public String getSecurity() {
        return security;
    }

    public void setSecurity(String security) {
        this.security = security;
    }

    public String getDeliveryExecutive() {
        return deliveryExecutive;
    }

    public void setDeliveryExecutive(String deliveryExecutive) {
        this.deliveryExecutive = deliveryExecutive;
    }

    public String getDataEntryOperator() {
        return dataEntryOperator;
    }

    public void setDataEntryOperator(String dataEntryOperator) {
        this.dataEntryOperator = dataEntryOperator;
    }

    public String getExecutive() {
        return executive;
    }

    public void setExecutive(String executive) {
        this.executive = executive;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public String getTotStaff() {
        return totStaff;
    }

    public void setTotStaff(String totStaff) {
        this.totStaff = totStaff;
    }

    public String getTotHouseKeeping() {
        return totHouseKeeping;
    }

    public void setTotHouseKeeping(String totHouseKeeping) {
        this.totHouseKeeping = totHouseKeeping;
    }

    public String getTotSecurity() {
        return totSecurity;
    }

    public void setTotSecurity(String totSecurity) {
        this.totSecurity = totSecurity;
    }

    public String getTotDeliveryExecutive() {
        return totDeliveryExecutive;
    }

    public void setTotDeliveryExecutive(String totDeliveryExecutive) {
        this.totDeliveryExecutive = totDeliveryExecutive;
    }

    public String getTotDataEntryOperator() {
        return totDataEntryOperator;
    }

    public void setTotDataEntryOperator(String totDataEntryOperator) {
        this.totDataEntryOperator = totDataEntryOperator;
    }

    public String getTotExecutive() {
        return totExecutive;
    }

    public void setTotExecutive(String totExecutive) {
        this.totExecutive = totExecutive;
    }

    public String getTotOthers() {
        return totOthers;
    }

    public void setTotOthers(String totOthers) {
        this.totOthers = totOthers;
    }

    public String getAttendanceDate() {
        return attendanceDate;
    }

    public void setAttendanceDate(String attendanceDate) {
        this.attendanceDate = attendanceDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(String returnTime) {
        this.returnTime = returnTime;
    }

    public String getReturnQty() {
        return returnQty;
    }

    public void setReturnQty(String returnQty) {
        this.returnQty = returnQty;
    }

}
