package ets.domain.wms.data;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import ets.domain.util.ThrottleConstants;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.wms.business.WmsTO;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Arrays;

/**
 *
 * @author hp
 */
public class WmsDAO extends SqlMapClientDaoSupport {

    private final static String CLASS = "WmsDAO";
    public int lim = 0;
    private String agreedFuelPrice;

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    //asnMasterList
    public ArrayList getviewDeliveryRequest(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList asnMasterList = new ArrayList();
        try {
            map.put("userId", wmsTo.getUserId());
            asnMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getviewDeliveryRequest", map);
            System.out.println("asnMasterList.size()-----" + asnMasterList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return asnMasterList;

    }

    public ArrayList getDeliveryRequestUpdate(WmsTO wmsTO) {
        Map map = new HashMap();
        map.put("orderId", wmsTO.getOrderId());
        System.out.println("map = " + map);
        ArrayList getDeliveryRequestUpdate = new ArrayList();
        try {
            getDeliveryRequestUpdate = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDeliveryRequestUpdate", map);
            System.out.println("getDeliveryRequestUpdate size=" + getDeliveryRequestUpdate.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDeliveryRequestUpdate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDeliveryRequestUpdate List", sqlException);
        }

        return getDeliveryRequestUpdate;
    }

    public int getDeliveryRequestFinalUpdate(WmsTO wmsTO, int userId, SqlMapClient session) {
        synchronized (this) {
            Map map = new HashMap();

            int status = 0;

            try {

                map.put("delRequestId", wmsTO.getDelRequestId());
                map.put("loanProposalId", wmsTO.getLoanProposalId());
                map.put("productId", wmsTO.getProductId());
                map.put("ItemId", wmsTO.getItemId());
                map.put("warehouseId", wmsTO.getWarehouseId());
                map.put("serialNumber", wmsTO.getSerialNumber());
                map.put("orderId", wmsTO.getOrderId());
                // map.put("qty", Integer.parseInt(wmsTO.getQty()) - 1);

                System.out.println(" getDeliveryRequestFinalUpdate : " + map);

                status = (Integer) session.update("wms.updateWmsStock", map);
                System.out.println(" updateWmsStock : " + status);

                status = (Integer) session.update("wms.updateSerialNo", map);
                System.out.println(" updateSerialNo : " + status);

                status = (Integer) session.update("wms.insertPostApi", map);
                System.out.println(" insertPostApi : " + status);

                status = (Integer) session.update("wms.updateDeliveryStatus", map);
                System.out.println(" updateDeliveryStatus : " + status);

            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
            }
            return status;
        }
    }

    public ArrayList getAsnMasterList(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList asnMasterList = new ArrayList();
        try {
            map.put("userId", wmsTo.getUserId());
            asnMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getAsnMasterList", map);
            System.out.println("asnMasterList.size()-----" + asnMasterList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return asnMasterList;

    }

    //asnMasterList
    public ArrayList getAsnDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList asnDetails = new ArrayList();
        try {
            map.put("asnId", wmsTo.getAsnId());
            asnDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getAsnDetails", map);
            System.out.println("asnDetails.size()-----" + asnDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return asnDetails;

    }

    public int saveSerialTempDetails(WmsTO wmsTo, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            if (wmsTo.getSerialNo() != null) {
                map.put("serialNo", wmsTo.getSerialNo());
                map.put("asnId", wmsTo.getAsnId());
                map.put("itemId", wmsTo.getItemId());
                map.put("qty", "1");
                map.put("userId", userId);
                map.put("proCon", wmsTo.getProCon());
                map.put("uom", wmsTo.getUom());
                status = (Integer) getSqlMapClientTemplate().update("wms.saveSerialTempDetails", map);
                System.out.println("saveSerialTempDetails-----" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveSerialTempDetails", sqlException);
        }
        return status;

    }

    public int saveGrnSerialDetails(WmsTO wmsTo, int userId, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int status = 0;
        try {
            String accYear = ThrottleConstants.accountYear;
            map.put("userId", userId);
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            map.put("custId", wmsTo.getCustId());
            map.put("empUserId", wmsTo.getEmpUserId());
            map.put("ipQty", wmsTo.getIpQty().length);
            map.put("gtnId", wmsTo.getGtnId());
            System.out.println("empUserId=========" + wmsTo.getEmpUserId());
            int GRNNo = 0;
            GRNNo = (Integer) session.queryForObject("wms.getGRNCode", map);
            System.out.println("grnNo-----" + GRNNo);

            String GRNNoSequence = "" + GRNNo;

            if (GRNNoSequence == null) {
                GRNNoSequence = "00001";
            } else if (GRNNoSequence.length() == 1) {
                GRNNoSequence = "0000" + GRNNoSequence;
            } else if (GRNNoSequence.length() == 2) {
                GRNNoSequence = "000" + GRNNoSequence;
            } else if (GRNNoSequence.length() == 3) {
                GRNNoSequence = "00" + GRNNoSequence;
            } else if (GRNNoSequence.length() == 4) {
                GRNNoSequence = "0" + GRNNoSequence;
            } else if (GRNNoSequence.length() == 5) {
                GRNNoSequence = "" + GRNNoSequence;
            }

            String grnNo = "GRN/"+accYear+"/" + GRNNoSequence;
            map.put("grnNo", grnNo);
            System.out.println("grnNo---" + grnNo);

            int grnId = (Integer) session.insert("wms.saveGrnMaster", map);
            map.put("grnId", grnId);
            System.out.println("grnId-----" + grnId);

            map.put("reqQty", wmsTo.getActQty());
            map.put("actQty", wmsTo.getActQty());
            map.put("unusedQty", wmsTo.getUnusedQty());

            status = (Integer) session.update("wms.saveGrnDetails", map);
            System.out.println("status-----" + status);

            String checkQty = (String) session.queryForObject("wms.getGRNReceivedQty", map);
            System.out.println("checkQty-----" + checkQty);

            if (checkQty != null) {
                String[] checkQtys = checkQty.split("~");

                double poQty = Double.parseDouble(checkQtys[0]);
                double grnQty = Double.parseDouble(checkQtys[1]);

                if (grnQty >= poQty) {
                    status = (Integer) session.update("wms.updateGrnStatusInAsnMaster", map);
                    System.out.println("saveGrnDetails-----" + status);
                }
            }

            status = (Integer) session.update("wms.updateGRNTempStatus", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return status;

    }

    public ArrayList getSerialTempDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList serialTempDetails = new ArrayList();
        try {
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            map.put("grnId", wmsTo.getGrnId());
            System.out.println("MAP----" + map);
            serialTempDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSerialTempDetails", map);
            System.out.println("serialTempDetails.size()-----" + serialTempDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return serialTempDetails;

    }

    public ArrayList getSerialTempDetailsUnsed(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList serialTempDetails = new ArrayList();
        try {
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            System.out.println("MAP----" + map);
            serialTempDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSerialTempDetailsUnsed", map);
            System.out.println("serialTempDetails.size()-----" + serialTempDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return serialTempDetails;

    }

    public String serialNoAlreadyExists(String SerialNo) {
        Map map = new HashMap();
        String status = "";
        int serialNoAlreadyExistsGrn = 0;
        int serialNoAlreadyExistsTemp = 0;
        try {
            System.out.println("serialnoexistence" + SerialNo);
            map.put("serialNo", SerialNo);
            System.out.println("MAP----" + map);

            serialNoAlreadyExistsGrn = (Integer) getSqlMapClientTemplate().queryForObject("wms.serialNoAlreadyExistsGrn", map);
            System.out.println("serialNoAlreadyExistsGrn-----" + serialNoAlreadyExistsGrn);

            if (serialNoAlreadyExistsGrn == 0) {
                serialNoAlreadyExistsTemp = (Integer) getSqlMapClientTemplate().queryForObject("wms.serialNoAlreadyExistsTemp", map);
                System.out.println("serialNoAlreadyExistsTemp-----" + serialNoAlreadyExistsTemp);
                if (serialNoAlreadyExistsTemp > 0) {
                    status = "2";
                } else {
                    status = "3";
                }
            } else {
                status = "1";
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return status;

    }

    public String checkExistSkuCode(String skuCode) {
        Map map = new HashMap();
        String status = "";
        int checkExistSkuCode = 0;
//        int checkExistSapCodeNoTemp = 0;
        try {
            System.out.println("sapCodeexistence" + skuCode);
            map.put("skuCode", skuCode);
            System.out.println("MAP----" + map);

            checkExistSkuCode = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkExistSkuCode", map);
            System.out.println("checkExistSapCodeAlan-----" + checkExistSkuCode);

            if (checkExistSkuCode == 0) {
                System.out.println("checkExistSapCodealan-----" + checkExistSkuCode);
                status = "0";

            } else {
                status = "1";
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return status;

    }

    public String checkExistSapCode(String sapCode) {
        Map map = new HashMap();
        String status = "";
        int checkExistSapCode = 0;
//        int checkExistSapCodeNoTemp = 0;
        try {
            System.out.println("sapCodeexistence" + sapCode);
            map.put("sapCode", sapCode);
            System.out.println("MAP----" + map);

            checkExistSapCode = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkExistSapCode", map);
            System.out.println("checkExistSapCodeAlan-----" + checkExistSapCode);

            if (checkExistSapCode == 0) {
                System.out.println("checkExistSapCodealan-----" + checkExistSapCode);
                status = "0";

            } else {
                status = "1";
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return status;

    }

    public String checkRpExistSerialNo(String SerialNo) {
        Map map = new HashMap();
        String status = "";
        int checkRpExistSerialNo = 0;
        int checkRpExistSerialNoTemp = 0;
        try {
            System.out.println("serialnoexistence" + SerialNo);
            map.put("serialNo", SerialNo);
            System.out.println("MAP----" + map);

            checkRpExistSerialNo = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkRpExistSerialNo", map);
            System.out.println("checkRpExistSerialNo-----" + checkRpExistSerialNo);

            if (checkRpExistSerialNo == 0) {
                checkRpExistSerialNoTemp = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkRpExistSerialNoTemp", map);
                System.out.println("checkRpExistSerialNoTemp-----" + checkRpExistSerialNoTemp);
                if (checkRpExistSerialNoTemp > 0) {
                    status = "2";
                } else {
                    status = "3";
                }
            } else {
                status = "1";
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return status;

    }

    public String rpSelectSerialId(WmsTO wms) {
        Map map = new HashMap();
        String status = "";
        int serialId = 0;
        try {
            System.out.println("serialnoexistence" + wms.getSerialNo());
            map.put("serialNo", wms.getSerialNo());
            map.put("productId", wms.getProductId());
            System.out.println("MAP----" + map);
            serialId = (Integer) getSqlMapClientTemplate().queryForObject("wms.rpSelectSerialId", map);
            System.out.println("checkRpExistSerialNo-----" + serialId);
            if (serialId == 0) {
                status = "0";
            } else {
                status = serialId + "";
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return status;

    }

    public String getRpExistSerialNo(WmsTO wms) {
        Map map = new HashMap();
        String getRpExistSerialNo = "";
        try {
            map.put("serialNo", wms.getSerialNo());
            map.put("productId", wms.getProductId());
            map.put("whId", wms.getWhId());
            map.put("stockId", wms.getStockId());
            System.out.println("MAP----" + map);

            getRpExistSerialNo = (String) getSqlMapClientTemplate().queryForObject("wms.getRpExistSerialNo", map);
//            status = getRpExistSerialNo+"";
            System.out.println("checkRpExistSerialNo-----" + getRpExistSerialNo);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return getRpExistSerialNo;

    }

    public String checkEwayBillNo(String ewayNo) {
        Map map = new HashMap();
        String status = "";
        int checkEwayBillNo = 0;
        int checkEwayBillNoOut = 0;
        try {
            System.out.println("checkEwayBillNo" + ewayNo);
            map.put("ewayNo", ewayNo);
            System.out.println("MAP----" + map);

            checkEwayBillNo = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkEwayBillNo", map);
            System.out.println("checkEwayBillNo-----" + checkEwayBillNo);

            if (checkEwayBillNo == 0) {
                checkEwayBillNoOut = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkEwayBillNoOut", map);
                System.out.println("checkEwayBillNo-----" + checkEwayBillNoOut);
                if (checkEwayBillNoOut > 0) {
                    status = "1";
                } else {
                    status = "2";
                }
            } else {
                status = "1";
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return status;

    }

    public String checkRouteCode(String routeCode) {
        Map map = new HashMap();
        String status = "";
        int routeCodeExist = 0;
        try {
            System.out.println("routeCodeExistence" + routeCode);
            map.put("routeCode", routeCode);
            System.out.println("MAP----" + map);

            routeCodeExist = (Integer) getSqlMapClientTemplate().queryForObject("wms.routeCodeExist", map);

            if (routeCodeExist > 0) {
                status = "1";
            } else {
                status = "2";
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return status;

    }

    public String InvoiceNoAlreadyExists(String invoiceNo) {
        Map map = new HashMap();
        String status = "";
        int InvoiceNoAlreadyExists = 0;
        try {
            System.out.println("invoiceNoExistence" + invoiceNo);
            map.put("invoiceNo", invoiceNo);
            System.out.println("MAP----" + map);

            InvoiceNoAlreadyExists = (Integer) getSqlMapClientTemplate().queryForObject("wms.invoiceNoAlreadyExists", map);

            if (InvoiceNoAlreadyExists == 0) {
                status = "2";
            } else {
                status = "1";
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return status;

    }

    public int insertConnectBillUpload(WmsTO wmsTo) {
        Map map = new HashMap();
        int insertConnectBillUpload = 0;
        try {
            map.put("userId", wmsTo.getUserId());
//            Double igst = 0.00;
//            Double sgst = 0.00;
//            Double cgst = 0.00;
//            if ("".equals(wmsTo.getiGSTAmount()) && wmsTo.getiGSTAmount() == null) {
//                igst = Double.parseDouble(wmsTo.getiGSTAmount());
//            }
//            if ("".equals(wmsTo.getcGSTAmount()) && wmsTo.getcGSTAmount() == null) {
//                cgst = Double.parseDouble(wmsTo.getcGSTAmount());
//            }
//            if ("".equals(wmsTo.getsGSTAmount()) && wmsTo.getsGSTAmount() == null) {
//                sgst = Double.parseDouble(wmsTo.getsGSTAmount());
//            }
//            Double GST = igst + sgst + cgst;
            map.put("billId", wmsTo.getBillId());
            map.put("billNo", wmsTo.getBillNo());
            map.put("lrNo", wmsTo.getLrNo());
            if ("".equals(wmsTo.getFreightAmount()) || wmsTo.getFreightAmount() == null) {
                map.put("freightAmount", "0");
            } else {
                map.put("freightAmount", wmsTo.getFreightAmount());
            }
            if ("".equals(wmsTo.getUnloadingAmt()) || wmsTo.getUnloadingAmt() == null) {
                map.put("unloadingAmt", "0");
            } else {
                map.put("unloadingAmt", wmsTo.getUnloadingAmt());
            }
            if ("".equals(wmsTo.getHaltingAmt()) || wmsTo.getHaltingAmt() == null) {
                map.put("haltingAmt", "0");
            } else {
                map.put("haltingAmt", wmsTo.getHaltingAmt());
            }
            if ("".equals(wmsTo.getOtherAmt()) || wmsTo.getOtherAmt() == null) {
                map.put("otherAmt", "0");
            } else {
                map.put("otherAmt", wmsTo.getOtherAmt());
            }
            if ("".equals(wmsTo.getGstAmt()) || wmsTo.getGstAmt() == null) {
                map.put("gstAmt", "0");
            } else {
                map.put("gstAmt", wmsTo.getGstAmt());
            }
            if ("".equals(wmsTo.getTotalAmt()) || wmsTo.getTotalAmt() == null) {
                map.put("billAmt", "0");
            } else {
                map.put("billAmt", wmsTo.getTotalAmt());
            }
            int checkLrNo = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkConnectSubLrNo", map);
            if (checkLrNo > 0) {
                map.put("status", "0");
            } else {
                map.put("status", "1");
            }

            insertConnectBillUpload = (Integer) getSqlMapClientTemplate().update("wms.insertConnectBillUpload", map);
            System.out.println("getConnectBillUpload.size()-----" + insertConnectBillUpload);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return insertConnectBillUpload;

    }

    public int clearConnectBillMappingTemp(int userId) {
        Map map = new HashMap();
        int clearConnectBillMappingTemp = 0;
        try {
            map.put("userId", userId);
            clearConnectBillMappingTemp = (Integer) getSqlMapClientTemplate().delete("wms.clearConnectBillMappingTemp", map);
            System.out.println("clearConnectBillMappingTemp.size()-----" + clearConnectBillMappingTemp);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return clearConnectBillMappingTemp;

    }

    public int saveConnectBillMapping(WmsTO wms) {
        Map map = new HashMap();
        int saveConnectBillMapping = 0;
        try {
            WmsTO wmsTO = new WmsTO();
            map.put("billId", wms.getBillId());
            int userId = Integer.parseInt(wms.getUserId());
            map.put("userId", userId);
            ArrayList getConnectBillUpload = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectBillUpload", map);
            Iterator itr = getConnectBillUpload.iterator();
            while (itr.hasNext()) {
                wmsTO = (WmsTO) itr.next();
                map.put("freightAmount", wmsTO.getFreightAmount());
                map.put("lrNo", wmsTO.getLrNo());
                map.put("unloadingAmount", wmsTO.getUnloadingAmt());
                map.put("haltingAmount", wmsTO.getHaltingAmt());
                map.put("otherAmount", wmsTO.getOtherAmt());
                map.put("gstAmount", wmsTO.getGstAmt());
                map.put("billAmount", wmsTO.getBillAmt());
                map.put("billNo", wmsTO.getBillNo());
                map.put("billId", wmsTO.getBillId());
                if ("0".equals(wmsTO.getStatus())) {
                    saveConnectBillMapping = (Integer) getSqlMapClientTemplate().insert("wms.insertConnectBillUploadDetails", map);
                }
            }
            saveConnectBillMapping = (Integer) getSqlMapClientTemplate().update("wms.updateConnectBillStatus", map);
            clearConnectBillMappingTemp(userId);
            System.out.println("saveConnectBillMapping.size()-----" + saveConnectBillMapping);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return saveConnectBillMapping;

    }

    public int updateRpWorkOrderStatus(WmsTO wmsTo) {
        Map map = new HashMap();
        int updateRpWorkOrderStatus = 0;

        try {
            map.put("status", wmsTo.getStatus());
            map.put("woId", wmsTo.getWorkOrderId());

            updateRpWorkOrderStatus = (Integer) getSqlMapClientTemplate().update("wms.updateRpWorkOrderStatus", map);
            System.out.println("updateRpWorkOrderStatus.size()-----" + updateRpWorkOrderStatus);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return updateRpWorkOrderStatus;

    }

    public int setRpWorkOrderDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        int woId = 0;
        try {
            int qty = 0;
            for (int i = 0; i < wmsTo.getQuantitys().length; i++) {
                qty = qty + Integer.parseInt(wmsTo.getQuantitys()[i]);
            }
            Date date = new Date();
            String s = new SimpleDateFormat("yyyy-MM-dd").format(date);
            String woSequence = "RPWO" + s.split("-")[0] + s.split("-")[1];
            map.put("woSequence", woSequence);
            int getRpWoSequence = (Integer) getSqlMapClientTemplate().queryForObject("wms.getRpWoSequence", map);
            String seqTemp = getRpWoSequence + "";
            if (seqTemp.length() == 1) {
                woSequence = woSequence + "000" + seqTemp;
            } else if (seqTemp.length() == 2) {
                woSequence = woSequence + "00" + seqTemp;
            } else if (seqTemp.length() == 3) {
                woSequence = woSequence + "0" + seqTemp;
            } else if (seqTemp.length() == 4) {
                woSequence = woSequence + seqTemp;
            }
            map.put("userId", wmsTo.getUserId());
            map.put("qty", qty);
            map.put("woNo", woSequence);
            map.put("warehouse", wmsTo.getWarehouseId());
            map.put("whId", wmsTo.getWhId());
            map.put("workOrderDate", wmsTo.getWorkOrderDate());
            map.put("expectedDate", wmsTo.getExpectedDate());
            woId = (Integer) getSqlMapClientTemplate().insert("wms.setRpWorkOrderMaster", map);
            System.out.println("setRpWorkOrderDetails.size()-----" + woId);
            map.put("woId", woId);
            for (int i = 0; i < wmsTo.getQuantitys().length; i++) {
                map.put("quantity", wmsTo.getQuantitys()[i]);
                map.put("productId", wmsTo.getItemIds()[i]);
                int insertRpWorkOrderDetails = (Integer) getSqlMapClientTemplate().insert("wms.setRpWorkOrderDetails", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return woId;

    }

    public ArrayList getRpWoProductList(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getRpWoProductList = new ArrayList();
        try {
            map.put("userId", wmsTo.getUserId());
            getRpWoProductList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getRpWoProductList", map);
            System.out.println("getRpWoProductList.size(oiiii)-----" + getRpWoProductList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return getRpWoProductList;

    }

    public ArrayList getrpWorkOrderDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getrpWorkOrderDetails = new ArrayList();
        try {
            map.put("userId", wmsTo.getUserId());
            getrpWorkOrderDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getrpWorkOrderDetails", map);
            System.out.println("getrpWorkOrderDetails.size(oiiii)-----" + getrpWorkOrderDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return getrpWorkOrderDetails;

    }

    public ArrayList rpWorkOrderDetailsView(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList rpWorkOrderDetailsView = new ArrayList();
        try {
            map.put("woId", wmsTo.getWorkOrderId());
            rpWorkOrderDetailsView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.rpWorkOrderDetailsView", map);
            System.out.println("rpWorkOrderDetailsView-----" + rpWorkOrderDetailsView.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return rpWorkOrderDetailsView;
    }

    public ArrayList getConnectBillUpload(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getConnectBillUpload = new ArrayList();
        try {
            map.put("userId", wmsTo.getUserId());
            map.put("billId", wmsTo.getBillId());
            getConnectBillUpload = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectBillUpload", map);
            System.out.println("getConnectBillUpload.size(oiiii)-----" + getConnectBillUpload.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return getConnectBillUpload;

    }

    public ArrayList getDockInList(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getDockInList = new ArrayList();
        try {
            map.put("userId", wmsTo.getUserId());
            getDockInList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDockInList", map);
            System.out.println("getDockInList.size()-----" + getDockInList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return getDockInList;

    }

    //asnMasterList
    public ArrayList getDockInDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getDockInDetails = new ArrayList();
        try {
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            map.put("grnId", wmsTo.getGrnId());
            map.put("gtnId", wmsTo.getGtnId());
            getDockInDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDockInDetails", map);
            System.out.println("getDockInDetails.size()-----" + getDockInDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return getDockInDetails;

    }

    public int saveGrnSerialStockDetails(WmsTO wmsTo, String path, int userId, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int status = 0;
        int stockId = 0;
        int update = 0;
        FileInputStream fis = null;

        try {
            map.put("userId", userId);
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            map.put("custId", wmsTo.getCustId());

            map.put("grnId", wmsTo.getGrnId());
            map.put("whId", wmsTo.getWhId());
            map.put("inpQty", wmsTo.getInpQty());
            map.put("fileName", path);
            map.put("userId", userId);
            System.out.println("asnid grnid itemid" + wmsTo.getAsnId() + "  " + wmsTo.getItemId() + "  " + wmsTo.getGrnId());
            int excessQty = (Integer) session.queryForObject("wms.getExcessQty", map);
            int damagedQty = (Integer) session.queryForObject("wms.getDamagedQty", map);
            System.out.println("excessQty======" + excessQty);
            map.put("excessQty", excessQty);
            map.put("damagedQty", damagedQty);
            String stockIds = (String) session.queryForObject("wms.getStockId", map);

            if (stockIds == null) {
                System.out.println("stock map......" + map);
                stockId = (Integer) session.insert("wms.insertStockDetails", map);
                System.out.println("insertStockDetails-----" + status);
            } else {
                System.out.println("stock map......" + map);
                stockId = (Integer) session.update("wms.updateStockDetails", map);
                System.out.println("updateStockDetails-----" + status);
                stockId = Integer.parseInt(stockIds);
            }

            map.put("stockId", stockId);
            ArrayList serialGood = new ArrayList();
            serialGood = (ArrayList) session.queryForList("wms.getSerialGood", map);
            if (serialGood.size() > 0) {
                Iterator itr = serialGood.iterator();
                while (itr.hasNext()) {
                    wmsTo = (WmsTO) itr.next();
                    map.put("serialNo", wmsTo.getSerialNoTemp());
                    map.put("binId", wmsTo.getBinIdTemp());
                    update = (Integer) session.update("wms.saveGoodGrnSerialDetails", map);
                    System.out.println("saveGrnSerialDetails-----" + update);
                }
            }
            ArrayList serialOthers = new ArrayList();
            serialOthers = (ArrayList) session.queryForList("wms.getSerialOthers", map);
            if (serialOthers.size() > 0) {
                Iterator itr = serialOthers.iterator();
                while (itr.hasNext()) {
                    wmsTo = (WmsTO) itr.next();
                    map.put("serialNo", wmsTo.getSerialNoTemp1());
                    map.put("binId", wmsTo.getBinIdTemp1());
                    map.put("proConTemp", wmsTo.getProConTemp1());
                    update = (Integer) session.update("wms.saveBadGrnSerialDetails", map);
                    System.out.println("saveBadSerialDetails-----" + update);
                }
            }
            status = (Integer) session.update("wms.updateDockInStatus", map);
            System.out.println("updateDockInStatus-----" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return status;

    }

    public String grnImageFileUpload(WmsTO wmsTo, String actualFilePath, String fileSaved, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int status = 0;
        String path = "";
        FileInputStream fis = null;

        try {

            map.put("invoiceNo", wmsTo.getInvoiceNo());
            map.put("invoiceDate", wmsTo.getInvoiceDate());
            map.put("eWaybillNo", wmsTo.getEwaybillNo());
            map.put("grnId", wmsTo.getGrnId());
            map.put("fileName", fileSaved);

            System.out.println("actualFilePath = " + actualFilePath);
            if (actualFilePath != null) {
//                File file = new File(actualFilePath);
//                System.out.println("file = " + file);
//                fis = new FileInputStream(file);
//                System.out.println("fis = " + fis);
//                byte[] podFile = new byte[(int) file.length()];
//                System.out.println("podFile = " + podFile);
//                fis.read(podFile);
//                fis.close();
//                map.put("podFile", podFile);

                try {

//                    //final String URLLink = "http://localhost:8080/HSServiceAPI/api/v1/bfil/updateGRNImage";
//                    final String URLLink = "http://52.66.244.61//HSServiceAPI/api/v1/bfil/updateGRNImage";
//
//                    String payload = "[{\"folder\":\"GRN\", \"filename\":\"" + fileSaved + "\" ,\"grnid\":\"0\"}]";
//                    System.out.println("payload " + payload);
//
//                    URL myurl = new URL(URLLink);
//                    HttpURLConnection connection = null;
//                    StringBuffer jsonString = new StringBuffer();
//                    String line;
//
//                    connection = (HttpURLConnection) myurl.openConnection();
//                    connection.setDoOutput(true);
//                    connection.setRequestMethod("PUT");
//                    connection.setRequestProperty("Accept", "application/json");
//                    connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
//                    writer.write(payload);
//                    writer.close();
//
//                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//                    while ((line = br.readLine()) != null) {
//                        jsonString.append(line);
//                    }
//                    br.close();
//                    connection.disconnect();
//                    System.out.println("jsonString :" + jsonString);
//
//                    path = jsonString.toString();
                    map.put("actualFilePath", fileSaved);
                    System.out.println("path========" + fileSaved);

                    System.out.println("map updateGrnMaster----- -----" + map);
                    status = (Integer) session.update("wms.updateGrnMaster", map);
                    System.out.println("updateGrnMaster-----" + status);

                } catch (Exception e) {
                    // e.printStackTrace();
                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return path;

    }

    public String grnEwayImageFileUpload(WmsTO wmsTo, String actualFilePath, String fileSaved, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int status = 0;
        String path = "";
        FileInputStream fis = null;

        try {

            map.put("grnId", wmsTo.getGrnId());
            map.put("fileName", fileSaved);
            System.out.println("actualFilePath = " + actualFilePath);
            if (actualFilePath != null) {
//                File file = new File(actualFilePath);
//                System.out.println("file = " + file);
//                fis = new FileInputStream(file);
//                System.out.println("fis = " + fis);
//                byte[] podFile = new byte[(int) file.length()];
//                System.out.println("podFile = " + podFile);
//                fis.read(podFile);
//                fis.close();
//                map.put("podFile", podFile);

                try {

//                    //final String URLLink = "http://localhost:8080/HSServiceAPI/api/v1/bfil/updateGRNImage";
//                    final String URLLink = "http://52.66.244.61//HSServiceAPI/api/v1/bfil/updateGRNImage";
//
//                    String payload = "[{\"folder\":\"GRN\", \"filename\":\"" + fileSaved + "\" ,\"grnid\":\"0\"}]";
//                    System.out.println("payload " + payload);
//
//                    URL myurl = new URL(URLLink);
//                    HttpURLConnection connection = null;
//                    StringBuffer jsonString = new StringBuffer();
//                    String line;
//
//                    connection = (HttpURLConnection) myurl.openConnection();
//                    connection.setDoOutput(true);
//                    connection.setRequestMethod("PUT");
//                    connection.setRequestProperty("Accept", "application/json");
//                    connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
//                    writer.write(payload);
//                    writer.close();
//
//                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//                    while ((line = br.readLine()) != null) {
//                        jsonString.append(line);
//                    }
//                    br.close();
//                    connection.disconnect();
//                    System.out.println("jsonString :" + jsonString);
//
//                    path = jsonString.toString();
                    map.put("actualFilePath", fileSaved);
                    System.out.println("path========" + fileSaved);

                    System.out.println("map updateGrnEwayPath----- -----" + map);
                    status = (Integer) session.update("wms.updateGrnEwayPath", map);
                    System.out.println("updateGrnEwayPath-----" + status);

                } catch (Exception e) {
                    // e.printStackTrace();
                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return path;

    }

    public ArrayList getBinList(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getBinList = new ArrayList();
        try {
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            System.out.println("MAP----" + map);
            getBinList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getBinList", map);
            System.out.println("getBinList.size()-----" + getBinList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getBinList", sqlException);
        }
        return getBinList;

    }

    public String getBarcodeMaster() {
        Map map = new HashMap();
        int BarcodeMaster = 0;
        String getBarcodeMaster = "";
        try {
            BarcodeMaster = (Integer) getSqlMapClientTemplate().queryForObject("wms.getBarcodeMaster", map);
            String barSequence = "" + BarcodeMaster;

            if (barSequence == null) {
                barSequence = "00001";
            } else if (barSequence.length() == 1) {
                barSequence = "0000" + barSequence;
            } else if (barSequence.length() == 2) {
                barSequence = "000" + barSequence;
            } else if (barSequence.length() == 3) {
                barSequence = "00" + barSequence;
            } else if (barSequence.length() == 4) {
                barSequence = "0" + barSequence;
            } else if (barSequence.length() == 5) {
                barSequence = "" + barSequence;
            }
            getBarcodeMaster = barSequence;
            System.out.println("getBarcodeMaster-----" + getBarcodeMaster);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getBinList", sqlException);
        }
        return getBarcodeMaster;

    }

    public int generateBarcode(WmsTO wmsTo, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            String barcode = "";
            int barSequence = Integer.parseInt(wmsTo.getBarcode());
            String accYear = ThrottleConstants.accountYear;
            System.out.println("BarcodeNo===" + barSequence);
            map.put("itemId", wmsTo.getItemId());
            map.put("custId", wmsTo.getCustId());
            map.put("userId", wmsTo.getUserId());
            int i = 0;
            int Batch = 0;
            Batch = (Integer) getSqlMapClientTemplate().queryForObject("wms.getBarcodeBatch", map);
            System.out.println("batch========" + Batch);
            System.out.println("barCode========" + wmsTo.getBarcode());
            System.out.println("count========" + wmsTo.getCount());
            map.put("batch", Batch + 1);
            for (i = 1; i <= wmsTo.getCount(); i++) {
                if (barSequence == 0) {
                    barcode = "BAR/"+ accYear +"/" + "00001";
                } else if (String.valueOf(barSequence).length() == 1) {
                    barcode = "BAR/"+ accYear +"/" + "0000" + barSequence;
                } else if (String.valueOf(barSequence).length() == 2) {
                    barcode = "BAR/"+ accYear +"/" + "000" + barSequence;
                } else if (String.valueOf(barSequence).length() == 3) {
                    barcode = "BAR/"+ accYear +"/" + "00" + barSequence;
                } else if (String.valueOf(barSequence).length() == 4) {
                    barcode = "BAR/"+ accYear +"/" + "0" + barSequence;
                } else if (String.valueOf(barSequence).length() == 5) {
                    barcode = "BAR/"+ accYear +"/" + "" + barSequence;
                }
                map.put("count", i);
                map.put("barcode", barcode);
                System.out.println("barCodeSeq===" + barcode);
                barSequence++;
                status = (Integer) getSqlMapClientTemplate().update("wms.generateBarcode", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveSerialTempDetails", sqlException);
        }
        return status;

    }

    public ArrayList grnViewDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList grnViewDetails = new ArrayList();
        try {
            map.put("userId", wmsTo.getUserId());
            map.put("fromDate", wmsTo.getFromDate());
            map.put("toDate", wmsTo.getToDate());
            map.put("whId", wmsTo.getWhId());
//            map.put("itemId", wmsTo.getItemId());
            System.out.println("MAP----" + map);
            grnViewDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.grnViewDetails", map);
            System.out.println("grnViewDetails.size()-----" + grnViewDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("grnViewDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "grnViewDetails", sqlException);
        }
        return grnViewDetails;

    }

    public ArrayList viewSerialNumberDetails(String grnId) {
        Map map = new HashMap();
        ArrayList viewSerialNumberDetails = new ArrayList();
        try {
            map.put("grnId", grnId);
//            map.put("itemId", wmsTo.getItemId());
            System.out.println("MAP----" + map);
            viewSerialNumberDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.viewSerialNumberDetails", map);
            System.out.println("grnViewDetails.size()-----" + viewSerialNumberDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewSerialNumberDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "viewSerialNumberDetails", sqlException);
        }
        return viewSerialNumberDetails;

    }

    public ArrayList getOrderDetailsList(WmsTO wmsTo) {
        Map map = new HashMap();
        map.put("fromDate", wmsTo.getFromDate());
        map.put("toDate", wmsTo.getToDate());
        map.put("whId", wmsTo.getWhId());
        System.out.println("map = " + map);
        ArrayList ordersList = new ArrayList();
        try {
            if (wmsTo.getWhId() != null) {
                ordersList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getOrderDetailsList", map);
            }
            System.out.println("getOrderDetailsList size=" + ordersList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderDetailsList List", sqlException);
        }

        return ordersList;
    }

    public ArrayList getWareHouseList() {
        Map map = new HashMap();
//        map.put("fromDate", wmsTo.getFromDate());
//        map.put("toDate", tripTO.getToDate());
//        System.out.println("map = " + map);
        ArrayList getWareHouseList = new ArrayList();
        try {
            getWareHouseList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getWareHouseList", map);
            System.out.println("getWareHouseList size=" + getWareHouseList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWareHouseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWareHouseList List", sqlException);
        }

        return getWareHouseList;
    }

    public int saveTempGrnDetails(WmsTO wmsTo, int userId) {
        Map map = new HashMap();
        int status = 0;

        try {
            map.put("userId", userId);
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            map.put("empId", wmsTo.getEmpId());
            map.put("gtnDetId", wmsTo.getGtnDetId());

            System.out.println("serial length......" + wmsTo.getSerialNos().length);

            System.out.println("uom......" + wmsTo.getIpQty().length);

            int flag = (Integer) getSqlMapClientTemplate().update("wms.grnFlagClose", map);
            for (int i = 0; i < wmsTo.getSerialNos().length; i++) {
                if (wmsTo.getSerialNos()[i] != null) {
                    int uom;
                    int procond;
                    if (wmsTo.getUom()[i].equals("Pack")) {
                        uom = 1;
                    } else {
                        uom = 2;
                    }
                    if (wmsTo.getProCond()[i].equals("Good")) {
                        procond = 1;
                    } else if (wmsTo.getProCond()[i].equals("Bad")) {
                        procond = 2;
                    } else {
                        procond = 3;
                    }
                    map.put("serialNo", wmsTo.getSerialNos()[i]);
                    map.put("binId", wmsTo.getBinIds()[i]);
                    map.put("uom", uom);
                    map.put("proCond", procond);
                    status = (Integer) getSqlMapClientTemplate().update("wms.saveTempGrnDetails", map);
                    System.out.println("saveGrnSerialDetails-----" + status);

                }

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return status;

    }

    public int updateSerialTempDetails(WmsTO wmsTo, int userId) {
        Map map = new HashMap();
        int status = 0;

        try {
            map.put("userId", userId);
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            System.out.println("itemidinupdateserial" + wmsTo.getItemId());
            map.put("gtnDetId", wmsTo.getGtnDetId());
            map.put("grnId", wmsTo.getGrnId());
            System.out.println("grnid in udpate serialtemp" + wmsTo.getGrnId());
            System.out.println("serial length......" + wmsTo.getSerialNos().length);

            System.out.println("uom......" + wmsTo.getIpQty().length);

            int flag = (Integer) getSqlMapClientTemplate().update("wms.grnFlagClose", map);
            for (int i = 0; i < wmsTo.getSerialNos().length; i++) {
                if (wmsTo.getSerialNos()[i] != null) {
                    int uom;
                    int procond;
                    if (wmsTo.getUom()[i].equals("Pack")) {
                        uom = 1;
                    } else {
                        uom = 2;
                    }
                    if (wmsTo.getProCond()[i].equals("Good")) {
                        procond = 1;
                    } else if (wmsTo.getProCond()[i].equals("Damaged")) {
                        procond = 2;
                    } else {
                        procond = 3;
                    }
                    map.put("serialNo", wmsTo.getSerialNos()[i]);
                    map.put("binId", wmsTo.getBinIds()[i]);
                    map.put("uom", uom);
                    map.put("proCond", procond);
                    status = (Integer) getSqlMapClientTemplate().update("wms.updateSerialTempDetails", map);
                    System.out.println("updateSerialTempDetails-----" + status);

                }

            }
            map.put("qty", wmsTo.getSerialNos().length);
            int updateGrnMasterQty = (Integer) getSqlMapClientTemplate().update("wms.updateGrnMasterQty", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return status;

    }

    public int delGrnTempSerial(String tempId, String grnId) {
        Map map = new HashMap();
        int status = 0;

        try {
            map.put("tempId", tempId);
            map.put("grnId", grnId);
            System.out.println("tempId====" + tempId);
            status = (Integer) getSqlMapClientTemplate().update("wms.delGrnTempSerial", map);
            int reduceGrnQty = (Integer) getSqlMapClientTemplate().update("wms.reduceGrnQty", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return status;

    }

    public ArrayList getPoCode(WmsTO wmsTO, int userId) {
        Map map = new HashMap();
        ArrayList productList = new ArrayList();
        try {
            map.put("userId", userId);
            productList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getPoCode", map);
            System.out.println(" getVehicleList =" + productList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getProductList", sqlException);
        }

        return productList;
    }

    public ArrayList getCustList(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList custList = new ArrayList();
        try {
            custList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getCustList", map);
            System.out.println(" getCustList =" + custList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleList", sqlException);
        }

        return custList;
    }

    public ArrayList getItemList(int userId, String custId) {
        Map map = new HashMap();
        ArrayList getItemList = new ArrayList();
        try {
            map.put("userId", userId);
            map.put("custId", custId);
            getItemList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getItemList", map);
            System.out.println(" getItemList =" + getItemList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleList", sqlException);
        }

        return getItemList;
    }

    public int saveGtnDetails(WmsTO wmsTO, int userId) {
        Map map = new HashMap();
        map.put("userId", userId);
        String poNo = "";
        SqlMapClient session = getSqlMapClient();
        int status = 0;
        int updateStatus = 0;
        try {
            int GTNNo = 0;
            GTNNo = (Integer) session.queryForObject("wms.getGTNCode", map);

            System.out.println("gtnNo-----" + GTNNo);

            String GTNNoSequence = "" + GTNNo;

            if (GTNNoSequence == null) {
                GTNNoSequence = "00001";
            } else if (GTNNoSequence.length() == 1) {
                GTNNoSequence = "0000" + GTNNoSequence;
            } else if (GTNNoSequence.length() == 2) {
                GTNNoSequence = "000" + GTNNoSequence;
            } else if (GTNNoSequence.length() == 3) {
                GTNNoSequence = "00" + GTNNoSequence;
            } else if (GTNNoSequence.length() == 4) {
                GTNNoSequence = "0" + GTNNoSequence;
            } else if (GTNNoSequence.length() == 5) {
                GTNNoSequence = "" + GTNNoSequence;
            }

            String accYear = ThrottleConstants.accountYear;
            String gtnNo = "GTN/"+ accYear +"/" + GTNNoSequence;
            map.put("gtnNo", gtnNo);
            System.out.println("gtnNo---" + gtnNo);
            map.put("invoiceNo", wmsTO.getInvoiceNo());
            map.put("invoiceDate", wmsTO.getInvoiceDate());
            map.put("gtnDate", wmsTO.getGtnDate());
            map.put("gtnTime", wmsTO.getGtnTime());
            map.put("custId", wmsTO.getCustId());
            map.put("dock", wmsTO.getDock());
            map.put("vehicleType", wmsTO.getVehicleType());
            map.put("vehicleNo", wmsTO.getVehicleNo());
            map.put("driverName", wmsTO.getDriverName());
            map.put("remarks", wmsTO.getRemarks());
            int gtnId = (Integer) session.insert("wms.saveGtnMaster", map);
            map.put("gtnId", gtnId);
            System.out.println("gtnId-----" + gtnId);
            for (int i = 0; i < wmsTO.getPoNos().length; i++) {
                if (i == 0) {
                    map.put("invoiceNo1", wmsTO.getInvoiceNo());
                } else {
                    map.put("invoiceNo1", wmsTO.getInvoiceNo() + "-" + i);
                }
                poNo = wmsTO.getPoNos()[i];
                if (poNo != null) {
                    String[] poNo1 = poNo.split("-");
                    System.out.println("asnId in DAO" + poNo1[0]);
                    String asnId = poNo1[0];
                    String itemId = poNo1[1];
                    map.put("asnId", asnId);
                    map.put("itemId", itemId);
                    map.put("poQty", wmsTO.getPoQty()[i]);
                    status = (Integer) getSqlMapClientTemplate().update("wms.saveGtnDetails", map);
                    int totalAsnQty = (Integer) session.queryForObject("wms.totalAsnQty", map);
                    int totalGtnQty = (Integer) session.queryForObject("wms.totalGtnQty", map);
                    System.out.println("totalasn and item qty  " + asnId + "   " + itemId + "   " + totalAsnQty + "   " + totalGtnQty);
                    if (totalAsnQty == totalGtnQty && totalAsnQty != 0) {
                        updateStatus = (Integer) getSqlMapClientTemplate().update("wms.updateAsnStatus", map);
                    }
                }
                System.out.println("status---" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getGtnDetails", sqlException);
        }

        return status;
    }

    public ArrayList getReceivedGateIn(int userId) {
        Map map = new HashMap();
        ArrayList getReceivedGateIn = new ArrayList();
        try {
            map.put("userId", userId);
            System.out.println("userId==========" + userId);
            getReceivedGateIn = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getReceivedGateIn", map);
            System.out.println("getReceivedGateIn.size()-----" + getReceivedGateIn.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getReceivedGateIn;

    }

    public ArrayList getGtnDetails(WmsTO wmsTo, int userId) {
        Map map = new HashMap();
        ArrayList gtnDetails = new ArrayList();
        try {
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            map.put("gtnId", wmsTo.getGtnId());
            map.put("userId", userId);
            System.out.println("asnId======----" + wmsTo.getAsnId());
            System.out.println("itemId======----" + wmsTo.getItemId());
            System.out.println("gtnId======----" + wmsTo.getGtnId());
            gtnDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getGtnDetails", map);
            System.out.println("asnDetails.size()-----" + gtnDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return gtnDetails;

    }

    public ArrayList getUserDetails(int userId) {
        Map map = new HashMap();
        ArrayList getUserDetails = new ArrayList();
        try {
            map.put("userId", userId);
            getUserDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getUserDetails", map);
            System.out.println("asnDetails.size()-----" + getUserDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getUserDetails;

    }

    public ArrayList getBinDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getBinDetails = new ArrayList();
        try {
            map.put("itemId", wmsTo.getItemId());
            getBinDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getBinDetails", map);
            System.out.println("getBinDetails.size()-----" + getBinDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getBinDetails;

    }

    public ArrayList getImPOList(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getImPOList = new ArrayList();
        try {
            map.put("materialId", wmsTo.getMaterialId());
            map.put("whId", wmsTo.getWhId());
            System.out.println("itemId=+++++" + wmsTo.getItemId());
            getImPOList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImPOList", map);
            System.out.println("getImPOList.size()-----" + getImPOList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getImPOList;

    }

    public ArrayList rpBinDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getBinDetails = new ArrayList();
        try {
            getBinDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.rpBinDetails", map);
            System.out.println("getBinDetails.size()-----" + getBinDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getBinDetails;

    }

    public ArrayList rpStorageDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList rpStorageDetails = new ArrayList();
        try {
            rpStorageDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.rpStorageDetails", map);
            System.out.println("rpStorageDetails.size()-----" + rpStorageDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return rpStorageDetails;
    }

    public ArrayList getProList(int userId, WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getProList = new ArrayList();
        try {
            map.put("gtnId", wmsTo.getGtnId());
            map.put("userId", userId);
            getProList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getProList", map);
            System.out.println("proListDetails.size()-----" + getProList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getProList;

    }

    public ArrayList proListDetails(int userId, WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList proListDetails = new ArrayList();
        try {
            map.put("gtnId", wmsTo.getGtnId());
            map.put("itemId", wmsTo.getItemId());
            map.put("asnId", wmsTo.getAsnId());
            map.put("userId", userId);
            proListDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.proListDetails", map);
            System.out.println("proListDetails.size()-----" + proListDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return proListDetails;

    }

    public ArrayList getSerialDet(String gtnId, String grnId, String itemId, String asnId) {
        Map map = new HashMap();
        ArrayList getSerialDet = new ArrayList();
        try {
            map.put("gtnId", gtnId);
            map.put("itemId", itemId);
            map.put("asnId", asnId);
            getSerialDet = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSerialDet", map);
            System.out.println("getBinDetails.size()-----" + getSerialDet.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getSerialDet;

    }

    public ArrayList getStateList() {
        Map map = new HashMap();
        ArrayList getStateList = new ArrayList();
        try {
            getStateList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getStateList", map);
            System.out.println("stateListSize" + getStateList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getStateList;

    }

    public ArrayList getWhList() {
        Map map = new HashMap();
        ArrayList getWhList = new ArrayList();
        try {
            getWhList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getWhList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getWhList;

    }

    public ArrayList AddwhList() {
        Map map = new HashMap();
        ArrayList AddwhList = new ArrayList();
        try {
            AddwhList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.AddwhList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return AddwhList;

    }

    public ArrayList warehouseOrderReport(WmsTO wms) {
        Map map = new HashMap();
        ArrayList warehouseOrderReport = new ArrayList();
        try {
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("stateId", wms.getStateId());
            map.put("whId", wms.getWhId());
            System.out.println("map in warehouseOrderReport========" + map);
            warehouseOrderReport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.warehouseOrderReport", map);
            System.out.println("warehouseOrderReport========" + warehouseOrderReport);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return warehouseOrderReport;

    }

    public int insertAsnDetails(WmsTO wmsTO) {

        Map map = new HashMap();

        int status1 = 0;
        int status2 = 0;
        int status = 0;

        try {
            map.put("remark", wmsTO.getRemark());

            map.put("PoRequestId", wmsTO.getPoRequestId());
            map.put("poNumber", wmsTO.getPoNumber());
            map.put("vendorId", wmsTO.getVendorId());
            map.put("poDate", wmsTO.getPoDate());
            map.put("warehouseId", wmsTO.getWarehouseId());
            map.put("status", "1");

            System.out.println("mapppppp==========" + map);
            // map.put("qty", Integer.parseInt(wmsTO.getQty()) - 1);

            status1 = (Integer) getSqlMapClientTemplate().update("wms.updateAnsdetailsOne", map);

            map.put("Quantity", wmsTO.getQuantity());
            map.put("PoRequestId", wmsTO.getPoRequestId());
            map.put("price", "0.00");
            map.put("active", "Y");

            map.put("itemId", wmsTO.getItemId());

            System.out.println("mappp222=====" + map);
            status2 = (Integer) getSqlMapClientTemplate().update("wms.updateAnsdetailsTwo", map);

            status = status1 + status2;

            System.out.println(" updateDeliveryStatus : " + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
        }
        return status;
    }

    public ArrayList getItemLists(int userId) {
        Map map = new HashMap();
        ArrayList getItemList = new ArrayList();
        try {
            map.put("userId", userId);

            getItemList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getItemLists", map);
            System.out.println(" getItemList =" + getItemList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleList", sqlException);
        }

        return getItemList;
    }

    public ArrayList processGetSerialNumber(String itemId, String userWhId) {
        Map map = new HashMap();
        ArrayList getSerialNumber = new ArrayList();
        try {

            map.put("itemId", itemId);
            map.put("userWhId", userWhId);
            map.put("status", "0");
            System.out.println("itemmm============DAO" + map);

            getSerialNumber = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSerialNumber", map);
            System.out.println(" getSerialNumber =" + getSerialNumber.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleList", sqlException);
        }

        return getSerialNumber;
    }

    public ArrayList getWareToHouseList(String userWhId) {
        Map map = new HashMap();
//        map.put("fromDate", wmsTo.getFromDate());
//        map.put("toDate", tripTO.getToDate());
//        System.out.println("map = " + map);
        ArrayList getWareHouseList = new ArrayList();
        try {
            map.put("userWhId", userWhId);
            getWareHouseList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getWareToHouseList", map);
            System.out.println("getWareHouseList size=" + getWareHouseList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWareHouseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWareHouseList List", sqlException);
        }

        return getWareHouseList;
    }

    public int insertTransferQty(WmsTO wmsTO) {

        Map map = new HashMap();
        int status = 0;
        try {
//            map.put("qty", wmsTO.getQuantity());
            map.put("userId", wmsTO.getUserId());
            map.put("warehouseToId", wmsTO.getWarehouseToId());
            map.put("warehouseId", wmsTO.getWarehouseId());
            map.put("itemId", wmsTO.getItemId());
            status = (Integer) getSqlMapClientTemplate().insert("wms.transferQty", map);
            String toItemId = (String) getSqlMapClientTemplate().queryForObject("wms.getToItemId", map);
            map.put("toItemId", toItemId);
            if (toItemId != null && toItemId != "") {
                String toStockId = (String) getSqlMapClientTemplate().queryForObject("wms.getToStockId", map);
                if (toStockId != null && toStockId != "") {
                    map.put("stockId", toStockId);
                    System.out.println("loop entered");
                    int qty = 0;
                    for (int i = 0; i < wmsTO.getSelectedIndex().length; i++) {
                        if ("1".equals(wmsTO.getSelectedIndex()[i])) {
                            map.put("serialNo", wmsTO.getSerialNos()[i]);
                            System.out.println("Map   === " + map);
                            status = (Integer) getSqlMapClientTemplate().update("wms.updateItemSerialNo", map);
                            int apiId = (Integer) getSqlMapClientTemplate().update("wms.insertStockTransferApiDet", map);
                            qty++;
                        }
                    }
                    map.put("qty", qty);
                    int reduceItemStockQty = (Integer) getSqlMapClientTemplate().update("wms.reduceItemStockQty", map);
                    System.out.println("qty " + qty);
                    System.out.println("reduceItemStockQty " + reduceItemStockQty);
                    int increaseItemStockQty = (Integer) getSqlMapClientTemplate().update("wms.increaseItemStockQty", map);
                } else {
                    status = 405;
                }
            } else {
                status = 404;
            }
            System.out.println(" updateDeliveryStatus : " + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
        }
        return status;
    }

    public ArrayList ProcessVendorList() {

        Map map = new HashMap();
        ArrayList vendorList = new ArrayList();

        try {

            vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getVendorList", map);
            System.out.println("vendorList======" + vendorList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return vendorList;
    }

    public ArrayList getConnectVendorList(WmsTO wms) {

        Map map = new HashMap();
        ArrayList getConnectVendorList = new ArrayList();
        try {
            getConnectVendorList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectVendorList", map);
            System.out.println("getConnectVendorList======" + getConnectVendorList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getConnectVendorList", sqlException);
        }
        return getConnectVendorList;
    }

    public ArrayList getVendorList() {
        Map map = new HashMap();
        ArrayList vendorList = new ArrayList();
        try {
            vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVendorList", map);
            System.out.println("vendorList.size() = " + vendorList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStatusList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStatusList List", sqlException);
        }
        return vendorList;
    }

    public ArrayList barCodeDetails(WmsTO wmsTO) {

        Map map = new HashMap();
        ArrayList barCodeDetails = new ArrayList();

        try {
            map.put("custId", wmsTO.getCustId());
            map.put("itemId", wmsTO.getItemId());
            barCodeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.barCodeDetails", map);
            System.out.println("vendorList======" + barCodeDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return barCodeDetails;
    }

    public ArrayList processGetItemList(String whId) {
        Map map = new HashMap();
        ArrayList getItemLists = new ArrayList();
        try {

            map.put("whId", whId);
            System.out.println("itemmm============DAO" + map);

            getItemLists = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getItemListsDetails", map);
            System.out.println(" getItemList =" + getItemLists.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleList", sqlException);
        }

        return getItemLists;
    }

    public ArrayList getFkRunsheetDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getFkRunsheetDetails = new ArrayList();
        try {
            map.put("shipmentId", wms.getShipmentId());
            getFkRunsheetDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getFkRunsheetDetails", map);
            System.out.println(" getItemList =" + getFkRunsheetDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleList", sqlException);
        }

        return getFkRunsheetDetails;
    }

    public String getUserWarehouse(int userId) {
        Map map = new HashMap();
        String getUserWarehouse = "";
        try {
            map.put("userId", userId);
            getUserWarehouse = (String) getSqlMapClientTemplate().queryForObject("wms.getUserWarehouse", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getBinList", sqlException);
        }
        return getUserWarehouse;
    }

    public ArrayList getserialNumberDetails(WmsTO wmsTO) {
        Map map = new HashMap();
//        map.put("fromDate", wmsTo.getFromDate());
//        map.put("toDate", tripTO.getToDate());
//        System.out.println("map = " + map);
        ArrayList serialNumberDetails = new ArrayList();
        try {
            map.put("serialNumber", wmsTO.getSerialNo());
            serialNumberDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSerialNumberDetails", map);
            System.out.println("getWareHouseList size=" + serialNumberDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWareHouseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWareHouseList List", sqlException);
        }

        return serialNumberDetails;
    }

    public int saveRunsheetUpload(WmsTO WmsTO, int userId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int updateStatus = 0;
        ArrayList getRunsheetUpload = new ArrayList();
        WmsTO wmsTO = new WmsTO();
        try {
            map.put("userId", WmsTO.getUserId());
            System.out.println("user Id   " + WmsTO.getUserId());
            System.out.println("--1111--");
            getRunsheetUpload = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getRunsheetUpload", map);
            int getFkBatch = (Integer) getSqlMapClientTemplate().queryForObject("wms.getFkBatch", map);
            map.put("batch", getFkBatch);
            System.out.println("getContractUpload---" + getRunsheetUpload.size());
            Iterator itr = getRunsheetUpload.iterator();
            while (itr.hasNext()) {
                WmsTO = (WmsTO) itr.next();
                map.put("runSheetId", WmsTO.getRunSheetId());
                map.put("trackingId", WmsTO.getTrackingId());
                map.put("mobileNo", WmsTO.getMobileNo());
                map.put("name", WmsTO.getName());
                map.put("address", WmsTO.getAddress());
                map.put("shipmentType", WmsTO.getShipmentType());
                map.put("codAmount", WmsTO.getCodAmount());
                map.put("assignedUser", WmsTO.getAssignedUser());
                map.put("status", WmsTO.getStatus());
                map.put("date", WmsTO.getDate());
                map.put("amountPaid", WmsTO.getAmountPaid());
                map.put("receiverName", WmsTO.getReceiverName());
                map.put("relation", WmsTO.getRelation());
                map.put("receiver", WmsTO.getReceiver());
                map.put("mobile", WmsTO.getMobile());
                map.put("signature", WmsTO.getSignature());
                map.put("vertical", WmsTO.getVertical());
                map.put("orderNo", WmsTO.getOrderNo());
                map.put("productDetails", WmsTO.getProductDetails());
                System.out.println("map = " + map);
                if ("0".equals(WmsTO.getError())) {
                    int saveRunsheetUpload = (Integer) getSqlMapClientTemplate().update("wms.saveRunsheetUpload", map);
                    int updateShipmentMaster = (Integer) getSqlMapClientTemplate().update("wms.updateShipmentMaster", map);
                }

            }
            int clearPreviousData = (Integer) getSqlMapClientTemplate().update("wms.deleteRunsheetTemp", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public int uploadRunsheet(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int uploadRunsheet = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", wmsTO.getUserId());
        map.put("runSheetId", wmsTO.getRunSheetId());
        map.put("trackingId", wmsTO.getTrackingId());
        map.put("mobileNo", wmsTO.getMobileNo());
        map.put("name", wmsTO.getName());
        map.put("address", wmsTO.getAddress());

        map.put("shipmentType", wmsTO.getShipmentType());
        map.put("codAmount", wmsTO.getCodAmount());
        map.put("assignedUser", wmsTO.getAssignedUser());
        map.put("status", wmsTO.getStatus());
        map.put("date", wmsTO.getDate());
        map.put("amountPaid", wmsTO.getAmountPaid());
        map.put("receiverName", wmsTO.getReceiverName());
        map.put("relation", wmsTO.getRelation());
        map.put("receiver", wmsTO.getReceiver());
        map.put("mobile", wmsTO.getMobile());
        map.put("signature", wmsTO.getSignature());
        map.put("vertical", wmsTO.getVertical());
        map.put("orderNo", wmsTO.getOrderNo());
        map.put("productDetails", wmsTO.getProductDetails());

        try {
            int batch = (Integer) getSqlMapClientTemplate().queryForObject("wms.getFkBatch", map);
            map.put("batch", batch);
            int trackingId = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkTrackingId", map);
            System.out.println("trackingId = " + trackingId);
            if (trackingId > 0) {
                int trackingIdTemp = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkTrackingIdTemp", map);
                if (trackingIdTemp == 0) {
                    int trackingIdRunsheet = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkTrackingIdRunsheet", map);
                    if (trackingIdRunsheet == 0) {
                        map.put("error", "0");
                    } else {
                        map.put("error", "3");
                    }
                } else {
                    map.put("error", "2");
                }
            } else {
                map.put("error", "1");
            }

            System.out.println("map map map = " + map);
            uploadRunsheet = (Integer) getSqlMapClientTemplate().insert("wms.uploadRunsheetTemp", map);
            System.out.println("uploadShipment size=====" + uploadRunsheet);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadShipment Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadShipment", sqlException);
        }
        return uploadRunsheet;
    }

    public ArrayList getRunsheetUpload(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getRunsheetUpload = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            getRunsheetUpload = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getRunsheetUpload", map);
            System.out.println("getRunsheetUpload========" + getRunsheetUpload);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getRunsheetUpload", sqlException);
        }
        return getRunsheetUpload;

    }

    public int ifbOrderUpload(WmsTO WmsTO, int userId, String whId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int lrNo = 0;
        int checkInvoice = 0;
        int pinWhCheck = 0;
        ArrayList getIfbOrderUpload = new ArrayList();
        WmsTO wmsTO = new WmsTO();
        try {
            String accYear = ThrottleConstants.accountYear;
            System.out.println("--1111--");
            map.put("userId", userId);
            map.put("whId", whId);
            String hubCode = (String) getSqlMapClientTemplate().queryForObject("wms.getHubCode", map);
            String date = "";
            String date1 = "";
            int ifbBatch = (Integer) getSqlMapClientTemplate().queryForObject("wms.getIfbBatch", map);
            map.put("batch", ifbBatch);
            map.put("status", "0");
            getIfbOrderUpload = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getIfbOrderUpload", map);
            System.out.println("getIfbOrderUpload---" + getIfbOrderUpload.size());
            Iterator itr = getIfbOrderUpload.iterator();
            while (itr.hasNext()) {
                WmsTO = (WmsTO) itr.next();
                map.put("billDoc", WmsTO.getBillDoc());
                date = WmsTO.getBillDate();
                if (date != null && date != "") {
                    System.out.println("date   " + date);
                    date = date.replace(".", "-");
                    date = date.replace("/", "-");
                    if (date.split("-").length > 1) {
                        System.out.println("date.split(\"-\")[2].length()   " + date.split("-").length);
                        if (date.split("-")[2].length() == 4) {
                            date1 = date.split("-")[2] + "-" + date.split("-")[1] + "-" + date.split("-")[0];
                        } else {
                            date1 = date.split("-")[0] + "-" + date.split("-")[1] + "-" + date.split("-")[2];
                        }
                    } else {
                        date1 = date;
                        System.out.println("date  " + date1.split(".").length);
                    }
                }
                map.put("billDate", date1);
                map.put("purchaseOrderNumber", WmsTO.getPurchaseOrderNumber());
                map.put("matlGroup", WmsTO.getMatlGroup());
                map.put("material", WmsTO.getMaterial());
                map.put("materialDescription", WmsTO.getMaterialDescription());
                map.put("billQty", WmsTO.getBillQty());
                map.put("city", WmsTO.getCity());
                map.put("shipTo", WmsTO.getShipTo());
                map.put("partyName", WmsTO.getPartyName());
                map.put("gstinNumber", WmsTO.getGstinNumber());
                map.put("grossValue", WmsTO.getGrossValue());
                map.put("pincode", WmsTO.getPincode());
                map.put("flag", WmsTO.getFlag());
                if ("YES".equalsIgnoreCase(WmsTO.getExchange())) {
                    map.put("exchange", "2");
                } else {
                    map.put("exchange", "1");
                }
                System.out.println("map = " + map);
                checkInvoice = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkIfbInvoice", map);
                pinWhCheck = (Integer) getSqlMapClientTemplate().queryForObject("wms.pinWhCheck", map);
                if ((whId).equals(pinWhCheck + "")) {
                    map.put("hubFlag", "0");
                    map.put("invoiceStatus", "0");
                } else {
                    map.put("hubFlag", "1");
                    map.put("invoiceStatus", "5");
                }
                if (checkInvoice == 0) {
                    int getIfbOrderUploads = (Integer) getSqlMapClientTemplate().update("wms.insertIFBOrder", map);
                }
            }
            ArrayList invoiceNos = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getIfbInvoiceNos", map);
            Iterator itr1 = invoiceNos.iterator();
            while (itr1.hasNext()) {
                WmsTO = (WmsTO) itr1.next();
                map.put("invoiceNo", WmsTO.getInvoiceNo().split("~")[0]);
                Double gross = Double.parseDouble(WmsTO.getInvoiceNo().split("~")[1]);
                System.out.println("invoiceNo==" + WmsTO.getInvoiceNo().split("~")[0]);
                lrNo = (Integer) getSqlMapClientTemplate().queryForObject("wms.getIfbLr", map);
                String LRNoSequence = "" + lrNo;

                if (LRNoSequence == null) {
                    LRNoSequence = "00001";
                } else if (LRNoSequence.length() == 1) {
                    LRNoSequence = "0000" + LRNoSequence;
                } else if (LRNoSequence.length() == 2) {
                    LRNoSequence = "000" + LRNoSequence;
                } else if (LRNoSequence.length() == 3) {
                    LRNoSequence = "00" + LRNoSequence;
                } else if (LRNoSequence.length() == 4) {
                    LRNoSequence = "0" + LRNoSequence;
                } else if (LRNoSequence.length() == 5) {
                    LRNoSequence = "" + LRNoSequence;
                }

                if (gross < 50000) {
                    map.put("ewayBillNo", "0");
                    map.put("ewayBillExpiry", "0");
                    System.out.println("gross  ==" + gross);
                    int updateOrder = (Integer) getSqlMapClientTemplate().update("wms.updateIfbInvoiceStatus", map);
                } else {
                    map.put("ewayBillNo", "");
                    map.put("ewayBillExpiry", "");
                }
                String lr = "HS" + hubCode + "/" + accYear + "/" + LRNoSequence;
                map.put("lrNo", lr);
                System.out.println("lrNo---" + lr);

                int lrId = (Integer) getSqlMapClientTemplate().insert("wms.saveIfbLr", map);
                map.put("lrId", lrId);
                insertStatus = (Integer) getSqlMapClientTemplate().update("wms.updateLrId", map);
                System.out.println("lrId-----" + lrId);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public int saveConnectSrn(WmsTO WmsTO, int userId, String whId) {
        Map map = new HashMap();
        int insertStatus = 0;
        ArrayList getConnectSrn = new ArrayList();
        WmsTO wmsTO = new WmsTO();
        try {
            System.out.println("--1111--");
            map.put("userId", userId);
            map.put("whId", whId);
            getConnectSrn = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectSrnTemp", map);
            System.out.println("getIfbOrderUpload---" + getConnectSrn.size());
            Iterator itr = getConnectSrn.iterator();
            while (itr.hasNext()) {
                wmsTO = (WmsTO) itr.next();
                map.put("partyName", wmsTO.getPartyName());
                map.put("soldToPartyCode", wmsTO.getSoldTo());
                map.put("shipToPartycode", wmsTO.getShipTo());
                map.put("partyDcNos", wmsTO.getPartyDcNos());
                map.put("dcDate", wmsTO.getDcDate());
                map.put("materialCode", wmsTO.getMaterialCode());
                map.put("materialDescription", wmsTO.getMaterialDescription());
                map.put("qty", wmsTO.getQty());
                map.put("receivedOn", wmsTO.getDate());
                map.put("vendorName", wmsTO.getVendorName());
                map.put("orderNo", wmsTO.getOrderNo());
                map.put("orderDate", wmsTO.getOrderDate());
                map.put("remarks", wmsTO.getRemarks());
                map.put("status", "0");
                if (Integer.parseInt(wmsTO.getStatus()) == 0) {
                    insertStatus = (Integer) getSqlMapClientTemplate().update("wms.insertConnectSrn", map);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public int deleteIfbUploadTemp(int userId) {
        Map map = new HashMap();
        int deleteStatus = 0;
        try {
            map.put("userId", userId);
            deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deleteIfbUploadTemp", map);
            System.out.println("deleteUploadTemp---" + deleteStatus);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return deleteStatus;
    }

    public int deleteConnectSrnTemp(int userId) {
        Map map = new HashMap();
        int deleteStatus = 0;
        try {
            map.put("userId", userId);
            deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deleteConnectSrnTemp", map);
            System.out.println("deleteConnectSrnTemp---" + deleteStatus);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return deleteStatus;
    }

    public int saveIfbLr(String[] lrId, String[] ewayBillNo, String[] ewayExpiry, String[] selected) {
        Map map = new HashMap();
        int saveIfbLr = 0;
        try {
            for (int i = 0; i < selected.length; i++) {
                if ("1".equals(selected[i])) {
                    map.put("lrId", lrId[i]);
                    map.put("ewayBillNo", ewayBillNo[i]);
                    map.put("ewayExpiry", ewayExpiry[i]);
                    saveIfbLr = (Integer) getSqlMapClientTemplate().update("wms.saveIfbEway", map);
                    int updateIfbOrderStatus = (Integer) getSqlMapClientTemplate().update("wms.updateIfbOrderStatus", map);
                    System.out.println("saveIfbLr---" + saveIfbLr);
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return saveIfbLr;
    }

    public int uploadIfbOrder(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int uploadIfbOrder = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        int pincode = 0;
        map.put("whId", wmsTO.getWhId());
        map.put("billDoc", wmsTO.getBillDoc());
        map.put("billDate", wmsTO.getBillDate());
        map.put("purchaseOrderNumber", wmsTO.getPurchaseOrderNumber());
        map.put("matlGroup", wmsTO.getMatlGroup());
        map.put("material", wmsTO.getMaterial());
        map.put("materialDescription", wmsTO.getMaterialDescription());
        map.put("billQty", wmsTO.getBillQty());
        map.put("city", wmsTO.getCity());
        map.put("shipTo", wmsTO.getShipTo());
        map.put("partyName", wmsTO.getPartyName());
        map.put("gstinNumber", wmsTO.getGstinNumber());
        map.put("grossValue", wmsTO.getGrossValue());
        map.put("userId", wmsTO.getUserId());
        map.put("exchange", wmsTO.getExchange());
        System.out.println("map    " + map);
        try {
            int dealerPincode = 0;
            int batch = (Integer) getSqlMapClientTemplate().queryForObject("wms.getIfbBatch", map);
            map.put("batch", batch);
            if ("".equals(wmsTO.getPincode())) {
                dealerPincode = (Integer) getSqlMapClientTemplate().queryForObject("wms.getDealerPincodeCount", map);
                if (dealerPincode > 0) {
                    pincode = (Integer) getSqlMapClientTemplate().queryForObject("wms.getDealerPincode", map);
                }
            } else {
                pincode = Integer.parseInt(wmsTO.getPincode());
                dealerPincode = 1;
            }
            map.put("pincode", pincode);
            int pincheck = (Integer) getSqlMapClientTemplate().queryForObject("secondaryOperation.pinExistsMaster", map);
            int checkInvoice = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkIfbInvoice", map);
            if (pincheck > 0) {
                if (checkInvoice == 0) {
                    if (dealerPincode > 0) {
                        map.put("status", "0");
                    } else {
                        map.put("status", "3");
                    }
                } else {
                    map.put("status", "2");
                }
            } else {
                map.put("status", "1");
            }
            System.out.println("map map map = " + map);
            uploadIfbOrder = (Integer) getSqlMapClientTemplate().insert("wms.insertIFBOrderTemp", map);
            System.out.println("uploadIfbOrder size=====" + uploadIfbOrder);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadNormalOrder", sqlException);
        }
        return uploadIfbOrder;
    }

    public int uploadConnectSrn(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int uploadIfbOrder = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", wmsTO.getUserId());
        map.put("whId", wmsTO.getWhId());
        map.put("partyName", wmsTO.getPartyName());
        map.put("soldToPartyCode", wmsTO.getSoldTo());
        map.put("shipToPartycode", wmsTO.getShipTo());
        map.put("partyDcNos", wmsTO.getPartyDcNos());
        map.put("dcDate", wmsTO.getDcDate());
        map.put("materialCode", wmsTO.getMaterialCode());
        map.put("materialDescription", wmsTO.getMaterialDescription());
        map.put("qty", wmsTO.getQty());
        map.put("receivedOn", wmsTO.getDate());
        map.put("vendorName", wmsTO.getVendorName());
        map.put("orderNo", wmsTO.getOrderNo());
        map.put("orderDate", wmsTO.getInvoiceDate());
        map.put("remarks", wmsTO.getRemarks());
        System.out.println("map    " + map);
        try {
            int checkPartyCode = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkPartyCode", map);
            int checkMaterial = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkMaterialCode", map);
            if (checkPartyCode == 0) {
                map.put("status", "1");
            } else if (checkMaterial == 0) {
                map.put("status", "2");
            } else {
                map.put("status", "0");
            }
            System.out.println("map map map = " + map);
            uploadIfbOrder = (Integer) getSqlMapClientTemplate().insert("wms.insertConnectSrnTemp", map);
            System.out.println("insertConnectSrn size=====" + uploadIfbOrder);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConnectSrn Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertConnectSrn", sqlException);
        }
        return uploadIfbOrder;
    }

    public ArrayList getIFBList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getIFBList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            getIFBList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getIfbOrderUpload", map);
//            WmsTO wms1 = new WmsTO();
//            if (getIFBList.size() > 0) {
//                for (int i = 0; i < getIFBList.size(); i++) {
//                    wms1 = (WmsTO) getIFBList.get(i);
//                    System.out.println("idsishd   "+wms1.getBillDoc());
//                }
//            }
//            System.out.println(getIFBList.get(0));
            System.out.println("getIFBList========" + getIFBList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getIFBList;

    }

    public ArrayList ifbLRList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList ifbLRList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            ifbLRList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.ifbLRList", map);
            System.out.println("ifbLRList========" + ifbLRList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return ifbLRList;

    }

    public ArrayList getIfbRunsheetList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getIfbRunsheetList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            getIfbRunsheetList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getIfbRunsheetList", map);
            System.out.println("getIfbRunsheetList========" + getIfbRunsheetList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getIfbRunsheetList;

    }

    public ArrayList getIfbReturnOrders(WmsTO wms, int userId) {
        Map map = new HashMap();
        ArrayList getIfbReturnOrders = new ArrayList();
        try {
            map.put("invoiceNo", wms.getInvoiceNo());
            System.out.println("map    " + map);
            getIfbReturnOrders = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getIfbReturnOrders", map);
            System.out.println("getIfbReturnOrders========" + getIfbReturnOrders.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getIfbReturnOrders;

    }

    public ArrayList getFkRunsheetList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getFkRunsheetList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            if (wms.getRunsheetId() != null || wms.getRunsheetId() != "") {
                map.put("runsheetId", wms.getRunsheetId());
            }
            System.out.println("map    " + map);
            if (wms.getFromDate() != null && wms.getFromDate() != "") {
                if (wms.getFromDate().split("-")[2].length() != 2) {
                    map.put("fromDate", wms.getFromDate().split("-")[2] + "-" + wms.getFromDate().split("-")[1] + "-" + wms.getFromDate().split("-")[0]);
                    map.put("toDate", wms.getToDate().split("-")[2] + "-" + wms.getToDate().split("-")[1] + "-" + wms.getToDate().split("-")[0]);
                } else {
                    map.put("fromDate", wms.getFromDate());
                    map.put("toDate", wms.getToDate());
                }
            }
            getFkRunsheetList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getFkRunsheetList", map);
            System.out.println("getFkRunsheetList========" + getFkRunsheetList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getFkRunsheetList;

    }

    public ArrayList fkFinanceApproval(WmsTO wms) {
        Map map = new HashMap();
        ArrayList fkFinanceApproval = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            fkFinanceApproval = (ArrayList) getSqlMapClientTemplate().queryForList("wms.fkFinanceApproval", map);
            System.out.println("fkFinanceApproval========" + fkFinanceApproval.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return fkFinanceApproval;

    }

    public ArrayList fkManagerApproval(WmsTO wms) {
        Map map = new HashMap();
        ArrayList fkManagerApproval = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            fkManagerApproval = (ArrayList) getSqlMapClientTemplate().queryForList("wms.fkManagerApproval", map);
            System.out.println("fkManagerApproval========" + fkManagerApproval.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return fkManagerApproval;

    }

    public ArrayList getwmsItemReport(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getwmsItemReport = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            getwmsItemReport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getwmsItemReport", map);
            System.out.println("getwmsItemReport========" + getwmsItemReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getwmsItemReport;

    }

    public ArrayList getwmsItemReportView(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getwmsItemReport = new ArrayList();
        try {
            map.put("itemId", wms.getItemId());
            System.out.println("map    " + map);
            getwmsItemReport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getwmsItemReportView", map);
            System.out.println("getwmsItemReport========" + getwmsItemReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getwmsItemReport;

    }

    public int UpdateSku(WmsTO wmsTO) {
        Map map = new HashMap();
        int status = 0;

        try {
            String serialNos[] = wmsTO.getSerialNos();
            String selectedStatus[] = wmsTO.getSelectedIndex();
            map.put("itemId", wmsTO.getItemId());
            map.put("oldItemId", wmsTO.getProductId());
            for (int i = 0; i < serialNos.length; i++) {
                if ("1".equals(selectedStatus[i])) {
                    map.put("serialNumber", serialNos[i]);
                    int decrease = (Integer) getSqlMapClientTemplate().update("wms.reduceItemStock", map);
                    int increase = (Integer) getSqlMapClientTemplate().update("wms.increaseItemStock", map);
                    status = (Integer) getSqlMapClientTemplate().update("wms.updateSku", map);
                    if (status > 0) {
                        status = (Integer) getSqlMapClientTemplate().update("wms.insertSkuUpdateApiDet", map);
                    }
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
        }
        return status;
    }

    public ArrayList hubOutList(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList hubOutList = new ArrayList();
        try {
            map.put("userId", wmsTO.getUserId());
            System.out.println("map    in hubout    " + map);
            hubOutList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.hubOutList", map);
            System.out.println("hubOutList========" + hubOutList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return hubOutList;

    }

    public ArrayList hubOutListView(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList hubOutListView = new ArrayList();
        try {
            map.put("userId", wmsTO.getUserId());
            map.put("lrId", wmsTO.getLrId());
            System.out.println("map    in hubout    " + map);
            hubOutListView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.hubOutListView", map);
            System.out.println("hubOutList========" + hubOutListView.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return hubOutListView;
    }

    public ArrayList hubOutPrintDetails(String lrId, String compId) {
        Map map = new HashMap();
        ArrayList hubOutPrintDetails = new ArrayList();
        try {
            map.put("lrId", lrId);
            map.put("compId", compId);
            System.out.println("map    in hubout    " + map);
            hubOutPrintDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.hubOutPrintDetails", map);
            System.out.println("hubOutList========" + hubOutPrintDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return hubOutPrintDetails;
    }

    public ArrayList getIfbOrderDetails(String lrId) {
        Map map = new HashMap();
        ArrayList getIfbOrderDetails = new ArrayList();
        try {
            map.put("lrId", lrId);
            System.out.println("map    in hubout    " + map);
            getIfbOrderDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getIfbOrderDetails", map);
            System.out.println("getIfbOrderDetails========" + getIfbOrderDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getIfbOrderDetails;

    }

    public ArrayList getIfbOrderRelease(int userId, WmsTO wms) {
        Map map = new HashMap();
        ArrayList getIfbOrderRelease = new ArrayList();
        try {
            map.put("userId", userId);
            map.put("roleId", wms.getRoleId());
            String date = "";
            date = wms.getFromDate();
            String date1 = "";
            date1 = wms.getToDate();
            String date2 = "";
            String date3 = "";
            if (date != null && date != "") {
                if (date.split("-").length > 1) {
                    if (date.split("-")[2].length() == 4) {
                        date2 = date.split("-")[2] + "-" + date.split("-")[1] + "-" + date.split("-")[0];
                    } else {
                        date2 = date;
                    }
                }
            }
            if (date1 != null && date1 != "") {
                if (date1.split("-").length > 1) {
                    if (date1.split("-")[2].length() == 4) {
                        date3 = date1.split("-")[2] + "-" + date1.split("-")[1] + "-" + date1.split("-")[0];
                    } else {
                        date3 = date1;
                    }
                }
            }
            map.put("fromDate", date2);
            map.put("toDate", date3);
            map.put("rejectionId", wms.getRejectionId());
            System.out.println("map    in getIfbOrderRelease    " + map);
            getIfbOrderRelease = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getIfbOrderRelease", map);
            System.out.println("getIfbOrderRelease========" + getIfbOrderRelease.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getIfbOrderRelease;

    }

    public int ifbReturnOrderUpload(WmsTO wms, int userId, String whId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int lrNo = 0;
        int checkInvoice = 0;
        int pinWhCheck = 0;
        ArrayList getIfbOrderUpload = new ArrayList();
        WmsTO wmsTO = new WmsTO();
        try {
            String accYear = ThrottleConstants.accountYear;
            int checkMaterial = 0;
            int getLrId = 0;
            System.out.println("--1111--");
            map.put("userId", userId);
            map.put("whId", whId);
            String hubCode = (String) getSqlMapClientTemplate().queryForObject("wms.getHubCode", map);
            int ifbBatch = (Integer) getSqlMapClientTemplate().queryForObject("wms.getIfbBatch", map);
            map.put("batch", ifbBatch);
            map.put("invoiceNo", wms.getInvoiceNo());
            map.put("billDoc", wms.getInvoiceNo());
            map.put("invoiceDate", wms.getInvoiceDate());
            map.put("purchaseOrderNumber", wms.getPurchaseOrderNumber());
            map.put("matlGroup", wms.getMatlGroup());
            map.put("serialNo", wms.getSerialNo());
            map.put("materialDescription", wms.getMaterialDescription());
            map.put("qty", wms.getQty());
            map.put("city", wms.getCity());
            map.put("customerName", wms.getCustomerName());
            map.put("shipTo", wms.getDealerCode());
            map.put("gstinNumber", wms.getGstinNumber());
            map.put("grossValue", wms.getGrossValue());
            map.put("pincode", wms.getPincode());
            System.out.println("map = " + map);
            checkInvoice = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkIfbInvoice", map);
//            pinWhCheck = (Integer) getSqlMapClientTemplate().queryForObject("wms.pinWhCheck", map);
//            if ((whId).equals(pinWhCheck + "")) {
            map.put("hubFlag", "0");
            map.put("invoiceStatus", "1");
            map.put("statusId", "1");
//            } else {
//                map.put("hubFlag", "1");
//                map.put("invoiceStatus", "5");
//                map.put("statusId", "5");
//            }
            if (checkInvoice == 0) {
                int insertIfbReturnOrder = (Integer) getSqlMapClientTemplate().update("wms.insertReturnIFBOrder", map);
                map.put("orderId", insertIfbReturnOrder);
                int status = (Integer) getSqlMapClientTemplate().update("trip.postIfbOrderStatusDetail", map);
            } else {
                map.put("material", wms.getSerialNo());
                checkMaterial = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkIfbMaterial", map);
                getLrId = (Integer) getSqlMapClientTemplate().queryForObject("wms.getLrId", map);
            }
            if (checkInvoice != 0 && checkMaterial == 0) {
                int insertIfbReturnOrder = (Integer) getSqlMapClientTemplate().update("wms.insertReturnIFBOrder", map);
                map.put("orderId", insertIfbReturnOrder);
                map.put("lrId", getLrId);
            } else if (checkInvoice == 0) {
                map.put("invoiceNo", wms.getInvoiceNo());
                lrNo = (Integer) getSqlMapClientTemplate().queryForObject("wms.getIfbLr", map);
                String LRNoSequence = "" + lrNo;
                if (LRNoSequence == null) {
                    LRNoSequence = "00001";
                } else if (LRNoSequence.length() == 1) {
                    LRNoSequence = "0000" + LRNoSequence;
                } else if (LRNoSequence.length() == 2) {
                    LRNoSequence = "000" + LRNoSequence;
                } else if (LRNoSequence.length() == 3) {
                    LRNoSequence = "00" + LRNoSequence;
                } else if (LRNoSequence.length() == 4) {
                    LRNoSequence = "0" + LRNoSequence;
                } else if (LRNoSequence.length() == 5) {
                    LRNoSequence = "" + LRNoSequence;
                }
                map.put("ewayBillNo", "0");
                map.put("ewayBillExpiry", "0");
                int updateOrder = (Integer) getSqlMapClientTemplate().update("wms.updateIfbInvoiceStatus", map);
                String lr = "HS" + hubCode + "/" + accYear + "/" + LRNoSequence;
                map.put("lrNo", lr);
                System.out.println("lrNo---" + lr);

                int lrId = (Integer) getSqlMapClientTemplate().insert("wms.saveIfbLr", map);
                map.put("lrId", lrId);
                System.out.println("lrId-----" + lrId);
            }
            insertStatus = (Integer) getSqlMapClientTemplate().update("wms.updateLrId", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getRejectionMaster(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getRejectionMaster = new ArrayList();
        try {
            System.out.println("map    in getRejectionMaster    " + map);
            getRejectionMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getRejectionMaster", map);
            System.out.println("getRejectionMaster========" + getRejectionMaster.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getRejectionMaster;

    }

    public int updateIfbHubout(String[] ewayBillNo, String[] ewayExpiry, String[] orderIds, String[] selectedStatus, String[] hubId, String vehicleNo, String driverName, String driverMobile, String date, String vendorId) {
        Map map = new HashMap();
        int updateIfbHubout = 0;
        int updateIfbLrHubout = 0;
        try {
            System.out.println("map    " + map);
            map.put("date", date);
            int sequence = (Integer) getSqlMapClientTemplate().queryForObject("wms.dispatchIdSequence", map);
            String x = (sequence + 1) + "";
            if (x.length() == 1) {
                map.put("dispatchId", date + "00" + x);
            } else if (x.length() == 2) {
                map.put("dispatchId", date + "0" + x);
            } else if (x.length() == 3) {
                map.put("dispatchId", date + x);
            }
            map.put("vendorId", vendorId);
            map.put("vehicleNo", vehicleNo);
            map.put("driverName", driverName);
            map.put("driverMobile", driverMobile);
            for (int i = 0; i < orderIds.length; i++) {
                if ("1".equals(selectedStatus[i])) {
                    map.put("orderId", orderIds[i]);
                    map.put("hubId", hubId[i]);
                    map.put("ewayNo", ewayBillNo[i]);
                    map.put("ewayExpiry", ewayExpiry[i]);
                    System.out.println("map   " + map);
                    updateIfbHubout = (Integer) getSqlMapClientTemplate().update("wms.updateIfbHubout", map);
                    updateIfbLrHubout = (Integer) getSqlMapClientTemplate().update("wms.updateIfbLrHubout", map);
                    System.out.println("updateIfbHubout---" + updateIfbHubout);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return updateIfbHubout;
    }

    public int updateHubIn(String[] orderIds, String[] selectedStatus) {
        Map map = new HashMap();
        int updateHubIn = 0;
        int updateHubInTime = 0;
        try {
            System.out.println("map    " + map);
            for (int i = 0; i < orderIds.length; i++) {
                if ("1".equals(selectedStatus[i])) {
                    map.put("orderId", orderIds[i]);
                    Double gross = (Double) getSqlMapClientTemplate().queryForObject("wms.getIfbGrossValue", map);
                    if (gross < 50000) {
                        map.put("status", "1");
                    } else {
                        map.put("status", "0");
                    }
                    System.out.println("map   " + map);
                    updateHubIn = (Integer) getSqlMapClientTemplate().update("wms.updateHubIn", map);
                    updateHubInTime = (Integer) getSqlMapClientTemplate().update("wms.updateHubInTime", map);
                    System.out.println("updateHubIn---" + updateHubIn);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return updateHubIn;
    }

    public int saveReturnIfbOrder(String[] orderIds, String[] selectedStatus) {
        Map map = new HashMap();
        int saveReturnIfbOrder = 0;
        try {
            System.out.println("map    " + map);
            for (int i = 0; i < orderIds.length; i++) {
                if ("1".equals(selectedStatus[i])) {
                    map.put("orderId", orderIds[i]);
                    System.out.println("map   " + map);
                    saveReturnIfbOrder = (Integer) getSqlMapClientTemplate().update("wms.saveReturnIfbOrder", map);
                    System.out.println("saveReturnIfbOrder---" + saveReturnIfbOrder);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return saveReturnIfbOrder;
    }

    public int updateIfbQty(String[] orderIds, String[] qty, String[] index, String[] cust) {
        Map map = new HashMap();
        int updateIfbQty = 0;
        try {
            System.out.println("map    " + map);
            for (int i = 0; i < orderIds.length; i++) {
                if ("1".equals(index[i])) {
                    map.put("orderId", orderIds[i]);
                    map.put("custName", cust[i]);
                    map.put("qty", qty[i]);
                    System.out.println("map   " + map);
                    updateIfbQty = (Integer) getSqlMapClientTemplate().update("wms.updateIfbQty", map);
                    System.out.println("updateIfbQty---" + updateIfbQty);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return updateIfbQty;
    }

    public int updateReleaseIfbOrder(WmsTO wms, int userId) {
        Map map = new HashMap();
        int updateReleaseIfbOrder = 0;
        try {
            map.put("orderId", wms.getOrderId());
            System.out.println("map    " + map);
            updateReleaseIfbOrder = (Integer) getSqlMapClientTemplate().update("wms.updateReleaseIfbOrder", map);
            int status = (Integer) getSqlMapClientTemplate().update("wms.updateReleaseIfbOrderTrip", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return updateReleaseIfbOrder;
    }

    public int updateFinanceapproval(WmsTO wms, int userId) {
        Map map = new HashMap();
        int updateFinanceapproval = 0;
        try {
            map.put("depositId", wms.getDepositId());
            map.put("remark", wms.getRemarks());
            map.put("userId", userId);
            System.out.println("map==========================   " + map);
            updateFinanceapproval = (Integer) getSqlMapClientTemplate().update("wms.updateFinanceapproval", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return updateFinanceapproval;
    }

    public int rejectFinanceapproval(WmsTO wms, int userId) {
        Map map = new HashMap();
        int rejectFinanceapproval = 0;
        try {
            map.put("depositId", wms.getDepositId());
            map.put("remarks", wms.getRemarks());
            map.put("userId", userId);
            System.out.println("map==========================reject   " + map);
            rejectFinanceapproval = (Integer) getSqlMapClientTemplate().update("wms.rejectFinanceApproval", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return rejectFinanceapproval;
    }

    public int updateManagerapproval(WmsTO wms, int userId) {
        Map map = new HashMap();
        int updateManagerapproval = 0;
        try {
            map.put("depositId", wms.getDepositId());
            map.put("status", wms.getStatus());
            map.put("reMarks", wms.getRemarks());
            map.put("podStatus", wms.getPodStatus());
            map.put("userId", userId);
            System.out.println("map==========================   " + map);
            updateManagerapproval = (Integer) getSqlMapClientTemplate().update("wms.updateManagerapproval", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return updateManagerapproval;
    }

    public ArrayList ifbHubinList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList ifbHubinList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            ifbHubinList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.ifbHubinList", map);
            System.out.println("ifbHubinList========" + ifbHubinList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return ifbHubinList;

    }

    public ArrayList getCustomerContract(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList getContractList = new ArrayList();
        try {
            getContractList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getNormalOrderTemp", map);
            System.out.println("getContractList========" + getContractList);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getContractList", sqlException);
        }
        return getContractList;

    }

    public int saveNormalOrderTemp(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int saveNormalOrderTemp = 0;
        int deleteCustomerContract = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("shipmentId", wmsTO.getShipmentId());
        map.put("customerName", wmsTO.getCustomerName());
        map.put("city", wmsTO.getCity());
        map.put("pinCode", wmsTO.getPinCode());
        map.put("pincode", wmsTO.getPinCode());
        map.put("deliveryHub", wmsTO.getDeliveryHub());
        if (wmsTO.getNote().equals("")) {
            map.put("note", "-");
        } else {
            map.put("note", wmsTO.getNote());
        }
        if (wmsTO.getShipmentId().equals("")) {
            map.put("shipmentId", "-");
        } else {
            map.put("shipmentId", wmsTO.getShipmentId());
        }
        if (wmsTO.getAssignedExecutive().equals("")) {
            map.put("assignedExecutive", "-");
        } else {
            map.put("assignedExecutive", wmsTO.getNote());
        }
        if (wmsTO.getOrigin().equals("")) {
            map.put("origin", "-");
        } else {
            map.put("origin", wmsTO.getNote());
        }
        if (wmsTO.getLastModified().equals("")) {
            map.put("lastModified", "-");
        } else {
            map.put("lastModified", wmsTO.getNote());
        }
        if (wmsTO.getLastReceived().equals("")) {
            map.put("lastReceived", "-");
        } else {
            map.put("lastReceived", wmsTO.getNote());
        }

        map.put("assignedExecutive", wmsTO.getAssignedExecutive());
        map.put("origin", wmsTO.getOrigin());
        map.put("currentHub", wmsTO.getCurrentHub());
        map.put("status", wmsTO.getStatus());
        map.put("note", wmsTO.getNote());
        map.put("amount", wmsTO.getAmount());
        map.put("weight", wmsTO.getWeight());
        map.put("lastModified", wmsTO.getLastModified());
        map.put("lastReceived", wmsTO.getLastReceived());
        map.put("shipmentType", wmsTO.getShipmentType());
        map.put("orderType", "1");

        try {

            int checkShipmentId = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkShipmentId", map);
            if (checkShipmentId > 0) {
                map.put("error", "2");
            } else {
                int checkHub = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkDeliveryHub", map);
                if (checkHub == 0) {
                    map.put("error", "3");
                } else {
                    map.put("error", "0");
                }
            }

            System.out.println("map map map = " + map);
            saveNormalOrderTemp = (Integer) getSqlMapClientTemplate().insert("wms.saveNormalOrderTemp", map);
            System.out.println("saveNormalOrderTemp size=====" + saveNormalOrderTemp);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveNormalOrderTemp Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveNormalOrderTemp", sqlException);
        }
        return saveNormalOrderTemp;
    }

    public int SaveOrderApproval(WmsTO wmsTO, int userId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int updateStatus = 0;
        int deleteStatus = 0;
        ArrayList getSaveOrderApproval = new ArrayList();
        WmsTO WmsTO = new WmsTO();
        try {
            map.put("whId", wmsTO.getWhId());
            map.put("userId", userId);
            System.out.println("RRRRR---%%%--");
            getSaveOrderApproval = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getNormalOrderTemp", map);
            System.out.println("getContractUpload---" + getSaveOrderApproval.size());
            Iterator itr = getSaveOrderApproval.iterator();
            while (itr.hasNext()) {
                WmsTO = (WmsTO) itr.next();
                map.put("shipmentId", WmsTO.getShipmentId());
                map.put("customerName", WmsTO.getCustomerName());
                map.put("city", WmsTO.getCity());
                map.put("pinCode", WmsTO.getPinCode());
                map.put("deliveryHub", WmsTO.getDeliveryHub());
                map.put("assignedExecutive", WmsTO.getAssignedExecutive());
                map.put("origin", WmsTO.getOrigin());
                map.put("currentHub", WmsTO.getCurrentHub());
                map.put("status", WmsTO.getStatus());
                map.put("note", WmsTO.getNote());
                map.put("amount", WmsTO.getAmount());
                map.put("weight", WmsTO.getWeight());
                map.put("lastModified", WmsTO.getLastModified());
                map.put("lastReceived", WmsTO.getLastReceived());
                map.put("shipmentType", WmsTO.getShipmentType());
                System.out.println("map = " + map);
                if ("0".equals(WmsTO.getError())) {
                    updateStatus = (Integer) getSqlMapClientTemplate().insert("wms.saveDeliveryShipmentMaster", map);
                }
                deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deleteUploadTemp", map);

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getreplacementList(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList getreplacementList = new ArrayList();
        try {
            int update = 0;

            getreplacementList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getreplacementList", map);
            System.out.println("getreplacementList========" + getreplacementList);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getContractList", sqlException);
        }
        return getreplacementList;

    }

    public int uploadReplacementOrder(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int uploadReplacementOrder = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("shipmentId", wmsTO.getShipmentId());
        map.put("shipmentType", wmsTO.getShipmentType());
        map.put("shipmentTierType", wmsTO.getShipmentTierType());
        map.put("reverseShipmentId", wmsTO.getReverseShipmentId());
        map.put("bagStatus", wmsTO.getBagStatus());

        map.put("type", wmsTO.getType());
        map.put("price", wmsTO.getPrice());
        map.put("latestStatus", wmsTO.getLatestStatus());
        map.put("deliveryHub", wmsTO.getDeliveryHub());
        map.put("currentLocation", wmsTO.getCurrentLocation());
        map.put("latestUpdateDateTime", wmsTO.getLatestUpdateDateTime());
        map.put("firstReceivedHub", wmsTO.getFirstReceivedHub());
        map.put("firstReceiveDateTime", wmsTO.getFirstReceiveDateTime());
        map.put("hubNotes", wmsTO.getHubNotes());
        map.put("csNotes", wmsTO.getCsNotes());
        map.put("numberofAttempts", wmsTO.getNumberofAttempts());
        map.put("bagId", wmsTO.getBagId());
        map.put("consignmentId", wmsTO.getConsignmentId());
        map.put("customerPromiseDate", wmsTO.getCustomerPromiseDate());
        map.put("logisticsPromiseDate", wmsTO.getLogisticsPromiseDate());
        map.put("onHoldByOpsReason", wmsTO.getOnHoldByOpsReason());
        map.put("onHoldByOpsDate", wmsTO.getOnHoldByOpsDate());
        map.put("firstAssignedHubName", wmsTO.getFirstAssignedHubName());
        map.put("deliveryPinCode", wmsTO.getDeliveryPinCode());
        map.put("pincode", wmsTO.getDeliveryPinCode());
        map.put("lastReceivedDateTime", wmsTO.getLastReceivedDateTime());
        map.put("orderType", "2");

        try {
            if (wmsTO.getReverseShipmentId() != null && wmsTO.getReverseShipmentId() != "" && !"N/A".equalsIgnoreCase(wmsTO.getReverseShipmentId())) {
                int checkShipmentId = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkShipmentId", map);
                if (checkShipmentId > 0) {
                    int checkOrderType = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkFkOrderType", map);
                    if (checkOrderType > 0) {
                        map.put("error", "4");
                    } else {
                        map.put("error", "2");
                    }
                } else {
                    int checkHub = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkDeliveryHub", map);
                    if (checkHub == 0) {
                        map.put("error", "3");
                    } else {
                        map.put("error", "0");
                    }
                }
            } else {
                map.put("error", "1");
            }
            System.out.println("map map map = " + map);
            uploadReplacementOrder = (Integer) getSqlMapClientTemplate().insert("wms.uploadReplacementOrder", map);
            System.out.println("uploadNormalOrder size=====" + uploadReplacementOrder);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadNormalOrder", sqlException);
        }
        return uploadReplacementOrder;
    }

    public int saveReplacementOrderUpload(WmsTO wmsTO, int userId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int updateStatus = 0;
        int deleteStatus = 0;
        ArrayList getShipmentUpload = new ArrayList();
        WmsTO WmsTO = new WmsTO();
        try {
            System.out.println("RRRRR---%%%--");
            getShipmentUpload = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getreplacementList", map);
            System.out.println("getShipmentUpload---" + getShipmentUpload.size());
            Iterator itr = getShipmentUpload.iterator();
            while (itr.hasNext()) {
                WmsTO = (WmsTO) itr.next();
                map.put("shipmentId", WmsTO.getShipmentId());
                map.put("shipmentType", WmsTO.getShipmentType());
                map.put("shipmentTierType", WmsTO.getShipmentTierType());
                map.put("reverseShipmentId", WmsTO.getReverseShipmentId());
                map.put("bagStatus", WmsTO.getBagStatus());
                map.put("type", WmsTO.getType());
                map.put("price", WmsTO.getPrice());
                map.put("latestStatus", WmsTO.getLatestStatus());
                map.put("deliveryHub", WmsTO.getDeliveryHub());
                map.put("currentLocation", WmsTO.getCurrentLocation());
                map.put("latestUpdateDateTime", WmsTO.getLatestUpdateDateTime());
                map.put("firstReceivedHub", WmsTO.getFirstReceivedHub());
                map.put("firstReceiveDateTime", WmsTO.getFirstReceiveDateTime());
                map.put("hubNotes", WmsTO.getHubNotes());
                map.put("csNotes", WmsTO.getCsNotes());
                map.put("numberofAttempts", WmsTO.getNumberofAttempts());
                map.put("bagId", WmsTO.getBagId());
                map.put("consignmentId", WmsTO.getConsignmentId());
                map.put("customerPromiseDate", WmsTO.getCustomerPromiseDate());
                map.put("logisticsPromiseDate", WmsTO.getLogisticsPromiseDate());
                map.put("onHoldByOpsReason", WmsTO.getOnHoldByOpsReason());
                map.put("onHoldByOpsDate", WmsTO.getOnHoldByOpsDate());
                map.put("firstAssignedHubName", WmsTO.getFirstAssignedHubName());
                map.put("deliveryPinCode", WmsTO.getDeliveryPinCode());
                map.put("lastReceivedDateTime", WmsTO.getLastReceivedDateTime());
                map.put("whId", wmsTO.getWhId());
                map.put("userId", userId);
                System.out.println("map = " + map);
                if ("0".equals(WmsTO.getError())) {
                    updateStatus = (Integer) getSqlMapClientTemplate().update("wms.saveReplacementOrders", map);
                } else if ("4".equals(WmsTO.getError())) {
                    updateStatus = (Integer) getSqlMapClientTemplate().update("wms.updateReplacementOrder", map);
                }
                deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deletereplacementTemp", map);

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getreturnList(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList getreturnList = new ArrayList();
        try {
            int update = 0;

            getreturnList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getreturnlistupload", map);
            System.out.println("getList========" + getreturnList);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getContractList", sqlException);
        }
        return getreturnList;

    }

    public int uploadReturnordertemp(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int uploadReplacementOrder = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("trackingId", wmsTO.getTrackingId());
        map.put("shipmentId", wmsTO.getTrackingId());
        map.put("pickupaddress", wmsTO.getPickupaddress());
        map.put("pinCode", wmsTO.getPinCode());
        map.put("pincode", wmsTO.getPinCode());
        map.put("itemName", wmsTO.getItemName());
        map.put("itemNos", wmsTO.getItemNos());
        map.put("createdDate", wmsTO.getCreatedDate());
        map.put("pickupDate", wmsTO.getPickupDate());
        map.put("Type", wmsTO.getType());
        map.put("lastStatus", wmsTO.getLastStatus());
        map.put("remarks", wmsTO.getRemarks());
        map.put("numberofAttempts", wmsTO.getNumberofAttempts());

        try {
            map.put("orderType", "3");
            int checkShipmentId = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkShipmentId", map);
            if (checkShipmentId == 0) {
                map.put("error", "0");
            } else {
                map.put("error", "2");
            }
            System.out.println("map map map = " + map);
            uploadReplacementOrder = (Integer) getSqlMapClientTemplate().insert("wms.uploadReturnOrdertemp", map);
            System.out.println("uploadNormalOrder size=====" + uploadReplacementOrder);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadNormalOrder", sqlException);
        }
        return uploadReplacementOrder;
    }

    public int updateFkReconcile(WmsTO wmsTO, String[] orderIds) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int updateFkReconcile = 0;
        int reconcileSequence = 0;
        /*
         * set the parameters in the map for sending to ORM
         */

        try {
            if ((wmsTO.getTotalCod()) == "") {
                map.put("totalCod", "0");
            } else {
                map.put("totalCod", wmsTO.getTotalCod());
            }
            if ((wmsTO.getTotalPrexoAmount()) == "") {
                map.put("totalCod", "0");
            } else {
                map.put("totalPrexoAmount", wmsTO.getTotalPrexoAmount());
            }
            if ((wmsTO.getTotalCardAmount()) == "") {
                map.put("totalCod", "0");
            } else {
                map.put("totalCardAmount", wmsTO.getTotalCardAmount());
            }
            if ((wmsTO.getTotalDeCod()) == "") {
                map.put("totalDeCod", "0");
            } else {
                map.put("totalDeCod", wmsTO.getTotalDeCod());;
            }

            map.put("tripId", wmsTO.getTripId());
            map.put("userId", wmsTO.getUserId());
            map.put("whId", wmsTO.getWhId());
            map.put("statusId", "14");

            reconcileSequence = (Integer) getSqlMapClientTemplate().queryForObject("wms.getReconcileCode", map);
            System.out.println("reconcileSequence-----" + reconcileSequence);

            String RecoSequence = "" + reconcileSequence;

            if (RecoSequence == null) {
                RecoSequence = "00001";
            } else if (RecoSequence.length() == 1) {
                RecoSequence = "0000" + RecoSequence;
            } else if (RecoSequence.length() == 2) {
                RecoSequence = "000" + RecoSequence;
            } else if (RecoSequence.length() == 3) {
                RecoSequence = "00" + RecoSequence;
            } else if (RecoSequence.length() == 4) {
                RecoSequence = "0" + RecoSequence;
            } else if (RecoSequence.length() == 5) {
                RecoSequence = "" + RecoSequence;
            }

            String accYear = ThrottleConstants.accountYear;
            String reconcileCode = "RC/"+ accYear +"/" + RecoSequence;
            map.put("reconcileCode", reconcileCode);
            System.out.println("map map map = " + map);
            updateFkReconcile = (Integer) getSqlMapClientTemplate().insert("wms.updateFkReconcile", map);
            map.put("reconcileId", updateFkReconcile);
            int updateReconcileId = 0;
            for (int i = 0; i < orderIds.length; i++) {
                map.put("deCods", wmsTO.getDeCods()[i]);
                map.put("cardAmounts", wmsTO.getCardAmounts()[i]);
                map.put("prexoAmounts", wmsTO.getPrexoAmounts()[i]);
                map.put("orderId", orderIds[i]);
                if (!"PP".equals(wmsTO.getShipmentTypes()[i])) {
                    map.put("shipmentType", wmsTO.getShipmentTypes()[i]);
                    map.put("reconcileId", updateFkReconcile);
                    updateReconcileId = (Integer) getSqlMapClientTemplate().update("wms.updateFkReconcileId", map);
                } else {
                    map.put("shipmentType", wmsTO.getShipmentTypes()[i]);
                    map.put("reconcileId", "");

                    updateReconcileId = (Integer) getSqlMapClientTemplate().update("wms.updateFkReconcileId", map);
                    String shipMentId = (String) getSqlMapClientTemplate().queryForObject("wms.getShipmentId", map);
                    map.put("shipMentId", shipMentId);
                    System.out.println("mapp=====" + map);
                    updateReconcileId = (Integer) getSqlMapClientTemplate().update("wms.updateFkrunSheet", map);

                    System.out.println("updateReconcileId==" + updateReconcileId);

                }
            }
            System.out.println("map======" + map);
            int updateTripStatusId = (Integer) getSqlMapClientTemplate().update("wms.updateFkTripStatusId", map);
            System.out.println("updateFkReconcile size=====" + updateFkReconcile);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateFkReconcile Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateFkReconcile", sqlException);
        }
        return updateFkReconcile;
    }

    public int returnContractUpload(WmsTO wmsTO, int userId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int updateStatus = 0;
        int deleteStatus = 0;
        ArrayList getShipmentUpload = new ArrayList();
        WmsTO WmsTO = new WmsTO();
        try {
            System.out.println("RRRRR---%%%--");
            getShipmentUpload = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getreturnlistupload", map);
            System.out.println("getShipmentUpload---" + getShipmentUpload.size());
            Iterator itr = getShipmentUpload.iterator();
            while (itr.hasNext()) {
                WmsTO = (WmsTO) itr.next();
                map.put("trackingId", WmsTO.getTrackingId());
                map.put("pickupaddress", WmsTO.getPickupaddress());
                map.put("pinCode", WmsTO.getPinCode());
                map.put("pincode", WmsTO.getPinCode());
                map.put("itemName", WmsTO.getItemName());
                map.put("itemNos", WmsTO.getItemNos());
                map.put("createdDate", WmsTO.getCreatedDate());
                map.put("pickupDate", WmsTO.getPickupDate());
                map.put("Type", WmsTO.getType());
                map.put("lastStatus", WmsTO.getLastStatus());
                map.put("remarks", WmsTO.getRemarks());
                map.put("numberofAttempts", WmsTO.getNumberofAttempts());
                map.put("whId", wmsTO.getWhId());
                map.put("userId", userId);
                System.out.println("map = " + map);
                if ("0".equals(WmsTO.getError())) {
                    updateStatus = (Integer) getSqlMapClientTemplate().insert("wms.saveReturnOrdersupload", map);
                }
                deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deletereturnTemp", map);

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public int deleteNormalOrderTemp(int userId) {
        Map map = new HashMap();
        int deleteStatus = 0;
        try {
            map.put("userId", userId);
            deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deleteUploadTemp", map);
            System.out.println("deleteUploadTemp---" + deleteStatus);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return deleteStatus;
    }

    public int deleteReplacementOrderTemp(int userId) {
        Map map = new HashMap();
        int deleteStatus = 0;
        try {
            map.put("userId", userId);
            deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deletereplacementTemp", map);
            System.out.println("deletereplacementTemp---" + deleteStatus);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return deleteStatus;
    }

    public int deleteReturnOrderTemp(int userId) {
        Map map = new HashMap();
        int deleteStatus = 0;
        try {
            map.put("userId", userId);
            deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deletereturnTemp", map);
            System.out.println("deletereturnTemp---" + deleteStatus);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return deleteStatus;
    }

    public int deleteRunsheetTemp(int userId) {
        Map map = new HashMap();
        int deleteStatus = 0;
        try {
            map.put("userId", userId);
            deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deleteRunsheetTemp", map);
            System.out.println("deleteRunsheetTemp---" + deleteStatus);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return deleteStatus;
    }

    public ArrayList ifbLRUploadList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList ifbLRUploadList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("roleId", wms.getRoleId());
            if (wms.getFromDate() != null) {
                map.put("fromDate", wms.getFromDate());
            } else {
                map.put("fromDate", "");
            }
            if (wms.getToDate() != null) {
                map.put("toDate", wms.getToDate());
            } else {
                map.put("toDate", "");
            }
            System.out.println("map    " + map);
            ifbLRUploadList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.ifbLRUploadList", map);
            System.out.println("ifbLRList========" + ifbLRUploadList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return ifbLRUploadList;

    }

    public ArrayList ifbLRPrintlist(String[] lrIds, String[] selected) {
        Map map = new HashMap();
        ArrayList ifbLRPrintlist = new ArrayList();
        try {
            List lrId = new ArrayList(lrIds.length);
            for (int i = 0; i < lrIds.length; i++) {
                if ("1".equals(selected[i])) {
                    lrId.add(lrIds[i]);
                }
            }
            map.put("lrId", lrId);
            ifbLRPrintlist = (ArrayList) getSqlMapClientTemplate().queryForList("wms.ifbLRPrintlist", map);
            System.out.println("ifbLRPrintlist---" + ifbLRPrintlist.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return ifbLRPrintlist;
    }

    public ArrayList getDealerMaster(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getDealerMaster = new ArrayList();
        try {
            getDealerMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDealerMaster", map);
            System.out.println("ifbLRPrintlist---" + getDealerMaster.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getDealerMaster;
    }

    public ArrayList getFkReconcile(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getFkReconcile = new ArrayList();
        try {
            map.put("tripId", wms.getTripId());
            System.out.println("map   " + map);
            getFkReconcile = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getFkReconcileOrderDetails", map);
            System.out.println("getFkReconcile---" + getFkReconcile.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getFkReconcile;
    }

    public ArrayList getFkReconcileMaster(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getFkReconcileMaster = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            getFkReconcileMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getFkReconcileMaster", map);
            System.out.println("getFkReconcileMaster---" + getFkReconcileMaster.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getFkReconcileMaster;
    }

    public ArrayList ifbPodDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList ifbPodDetails = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("roleId", wms.getRoleId());
            if (wms.getFromDate() != null && wms.getFromDate() != "") {
                if (wms.getFromDate().split("-")[2].length() != 2) {
                    map.put("fromDate", wms.getFromDate().split("-")[2] + "-" + wms.getFromDate().split("-")[1] + "-" + wms.getFromDate().split("-")[0]);
                    map.put("toDate", wms.getToDate().split("-")[2] + "-" + wms.getToDate().split("-")[1] + "-" + wms.getToDate().split("-")[0]);
                } else {
                    map.put("fromDate", wms.getFromDate());
                    map.put("toDate", wms.getToDate());
                }
            }
            ifbPodDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.ifbPodDetails", map);
            System.out.println("ifbPodDetails---" + ifbPodDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return ifbPodDetails;
    }

    public ArrayList ifbPodDetailsDownload(String[] podIds) {
        Map map = new HashMap();
        ArrayList ifbPodDetails = new ArrayList();
        try {
            List podId = new ArrayList(podIds.length);
            for (int i = 0; i < podIds.length; i++) {
                System.out.println("podId  " + podIds[i]);
                podId.add(podIds[i]);
            }
            map.put("podId", podId);
            ifbPodDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.ifbPodDetailsDownload", map);
            System.out.println("ifbPodDetails---" + ifbPodDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return ifbPodDetails;
    }

    public ArrayList getFkCashDeposit(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getFkCashDeposit = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            getFkCashDeposit = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getFkCashDeposit", map);
            System.out.println("getFkCashDeposit---" + getFkCashDeposit.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getFkCashDeposit;
    }

    public int saveIfbPodFile(String podRemarks, String podId, String file, String fileName) {
        Map map = new HashMap();
        int saveIfbPodFile = 0;
        try {
            map.put("podId", podId);
            map.put("filePath", file);
            map.put("podRemarks", podRemarks);
            map.put("fileName", fileName);
            saveIfbPodFile = (Integer) getSqlMapClientTemplate().update("wms.saveIfbPodFile", map);
            System.out.println("saveIfbPodFile---" + saveIfbPodFile);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return saveIfbPodFile;
    }

    public int approveIfbPod(String podId) {
        Map map = new HashMap();
        int approveIfbPod = 0;
        try {
            map.put("podId", podId);
            approveIfbPod = (Integer) getSqlMapClientTemplate().update("wms.approveIfbPod", map);
            System.out.println("approveIfbPod---" + approveIfbPod);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return approveIfbPod;
    }

    public int updateCashDeposit(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int updateFkReconcile = 0;
        int reconcileSequence = 0;
        /*
         * set the parameters in the map for sending to ORM
         */

        try {
            String accYear = ThrottleConstants.accountYear;
            String[] reconcileIds = wmsTO.getReconcileIds();
            String[] selected = wmsTO.getSelectedIndex();
            map.put("depositAmount", wmsTO.getDepositAmount());
            map.put("depositType", wmsTO.getDepositType());
            map.put("fromDate", wmsTO.getFromDate());
            map.put("toDate", wmsTO.getToDate());
            map.put("whId", wmsTO.getWhId());
            map.put("userId", wmsTO.getUserId());

            reconcileSequence = (Integer) getSqlMapClientTemplate().queryForObject("wms.getDepositCode", map);
            System.out.println("reconcileSequence-----" + reconcileSequence);

            String RecoSequence = "" + reconcileSequence;

            if (RecoSequence == null) {
                RecoSequence = "00001";
            } else if (RecoSequence.length() == 1) {
                RecoSequence = "0000" + RecoSequence;
            } else if (RecoSequence.length() == 2) {
                RecoSequence = "000" + RecoSequence;
            } else if (RecoSequence.length() == 3) {
                RecoSequence = "00" + RecoSequence;
            } else if (RecoSequence.length() == 4) {
                RecoSequence = "0" + RecoSequence;
            } else if (RecoSequence.length() == 5) {
                RecoSequence = "" + RecoSequence;
            }

            String reconcileCode = "DC/"+ accYear +"/" + RecoSequence;
            map.put("depositCode", reconcileCode);
            System.out.println("map map map = " + map);
            updateFkReconcile = (Integer) getSqlMapClientTemplate().insert("wms.updatecashDeposit", map);
            map.put("depositId", updateFkReconcile);
            for (int i = 0; i < selected.length; i++) {
                if ("1".equals(selected[i])) {
                    map.put("reconcileId", reconcileIds[i]);
                    int updateDepositId = (Integer) getSqlMapClientTemplate().update("wms.updateFkdepositId", map);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateFkReconcile Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateFkReconcile", sqlException);
        }
        return updateFkReconcile;
    }

    public ArrayList fkCashDepositDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList fkCashDepositDetails = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            fkCashDepositDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.fkCashDepositDetails", map);
            System.out.println("fkCashDepositDetails---" + fkCashDepositDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return fkCashDepositDetails;
    }

    public int approveFkDeposit(String depositId) {
        Map map = new HashMap();
        int approveFkDeposit = 0;
        try {
            map.put("depositId", depositId);
            System.out.println("map map map = " + map);
            approveFkDeposit = (Integer) getSqlMapClientTemplate().update("wms.approveFkDeposit", map);
            System.out.println("approveIfbPod---" + approveFkDeposit);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return approveFkDeposit;
    }

    public int addFkRvpRunsheet(String[] shipmentId, String[] name, String[] address, String[] codAmount, String[] status, String[] date, String[] itemName, int userId) {
        Map map = new HashMap();
        int addFkRvpRunsheet = 0;
        try {
            map.put("userId", userId);
            map.put("orderType", "3");
            int getFkBatch = (Integer) getSqlMapClientTemplate().queryForObject("wms.getFkBatch", map);
            map.put("batch", getFkBatch);
            for (int i = 0; i < shipmentId.length; i++) {
                map.put("trackingId", shipmentId[i]);
                map.put("name", name[i]);
                map.put("address", address[i]);
                map.put("codAmount", codAmount[i]);
                map.put("status", status[i]);
                map.put("date", date[i]);
                map.put("runSheetId", "");
                map.put("productDetails", itemName[i]);
                System.out.println("map map map = " + map);
                addFkRvpRunsheet = (Integer) getSqlMapClientTemplate().update("wms.saveRunsheetUpload", map);
                int updateShipmentMaster = (Integer) getSqlMapClientTemplate().update("wms.updateShipmentMasterType", map);
                System.out.println("approveIfbPod---" + addFkRvpRunsheet);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return addFkRvpRunsheet;
    }

    public int saveFkDepositFile(String podRemarks, String depositId, String file, String fileName) {
        Map map = new HashMap();
        int saveFkDepositFile = 0;
        try {
            map.put("depositId", depositId);
            map.put("filePath", file);
            map.put("podRemarks", podRemarks);
            map.put("fileName", fileName);
            saveFkDepositFile = (Integer) getSqlMapClientTemplate().update("wms.saveFkDepositFile", map);
            System.out.println("saveFkDepositFile---" + saveFkDepositFile);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFkDepositFile", sqlException);
        }
        return saveFkDepositFile;
    }

    public ArrayList showReconcile(WmsTO wms) {
        Map map = new HashMap();
        ArrayList ifbPodDetails = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("depositId", wms.getDepositId());
            ifbPodDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.showReconcile", map);
            System.out.println("ifbPodDetails---" + ifbPodDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return ifbPodDetails;
    }

    public ArrayList getShipmentDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getFkReconcile = new ArrayList();
        try {
            map.put("reconcileId", wms.getReconcileId());
            getFkReconcile = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getShipmentDetails", map);
            System.out.println("getFkReconcile---" + getFkReconcile.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getFkReconcile;
    }

    public ArrayList ifbRunsheetReport(WmsTO wms) {
        Map map = new HashMap();
        ArrayList ifbRunsheetReport = new ArrayList();
        try {
            if (wms.getFromDate() != null && wms.getFromDate() != "") {
                if (wms.getFromDate().split("-")[2].length() != 2) {
                    map.put("fromDate", wms.getFromDate().split("-")[2] + "-" + wms.getFromDate().split("-")[1] + "-" + wms.getFromDate().split("-")[0]);
                    map.put("toDate", wms.getToDate().split("-")[2] + "-" + wms.getToDate().split("-")[1] + "-" + wms.getToDate().split("-")[0]);
                } else {
                    map.put("fromDate", wms.getFromDate());
                    map.put("toDate", wms.getToDate());
                }
            }
            map.put("vehicleNo", wms.getVehicleNo());
            map.put("empId", wms.getEmpId());
            map.put("orderType", wms.getOrderType());
            map.put("userId", wms.getUserId());
            map.put("roleId", wms.getRoleId());
            ifbRunsheetReport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.ifbRunsheetReport", map);
            System.out.println("ifbRunsheetReport---" + ifbRunsheetReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return ifbRunsheetReport;
    }

    public ArrayList fkRunsheetReport(WmsTO wms) {
        Map map = new HashMap();
        ArrayList fkRunsheetReport = new ArrayList();
        try {
            if (wms.getFromDate() != null && wms.getFromDate() != "") {
                if (wms.getFromDate().split("-")[2].length() != 2) {
                    map.put("fromDate", wms.getFromDate().split("-")[2] + "-" + wms.getFromDate().split("-")[1] + "-" + wms.getFromDate().split("-")[0]);
                    map.put("toDate", wms.getToDate().split("-")[2] + "-" + wms.getToDate().split("-")[1] + "-" + wms.getToDate().split("-")[0]);
                } else {
                    map.put("fromDate", wms.getFromDate());
                    map.put("toDate", wms.getToDate());
                }
            }
            map.put("empId", wms.getEmpId());
            map.put("orderType", wms.getOrderType());
            map.put("whId", wms.getWhId());
            map.put("userId", wms.getUserId());
            map.put("roleId", wms.getRoleId());
            fkRunsheetReport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.fkRunsheetReport", map);
            System.out.println("fkRunsheetReport---" + fkRunsheetReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return fkRunsheetReport;
    }

    public ArrayList ifbShipmentReport(WmsTO wms) {
        Map map = new HashMap();
        ArrayList ifbShipmentReport = new ArrayList();
        try {
            if (wms.getFromDate() != null && wms.getFromDate() != "") {
                if (wms.getFromDate().split("-")[2].length() != 2) {
                    map.put("fromDate", wms.getFromDate().split("-")[2] + "-" + wms.getFromDate().split("-")[1] + "-" + wms.getFromDate().split("-")[0]);
                    map.put("toDate", wms.getToDate().split("-")[2] + "-" + wms.getToDate().split("-")[1] + "-" + wms.getToDate().split("-")[0]);
                } else {
                    map.put("fromDate", wms.getFromDate());
                    map.put("toDate", wms.getToDate());
                }
            }
            map.put("orderType", wms.getOrderType());
            map.put("userId", wms.getUserId());
            map.put("roleId", wms.getRoleId());
            map.put("invoiceStatus", wms.getInvoiceStatus());
            ifbShipmentReport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.ifbShipmentReport", map);
            System.out.println("ifbShipmentReport---" + ifbShipmentReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return ifbShipmentReport;
    }

    public ArrayList fkReconcileView(WmsTO wms) {
        Map map = new HashMap();
        ArrayList fkReconcileView = new ArrayList();
        try {
            if (wms.getFromDate() != null && wms.getFromDate() != "") {
                if (wms.getFromDate().split("-")[2].length() != 2) {
                    map.put("fromDate", wms.getFromDate().split("-")[2] + "-" + wms.getFromDate().split("-")[1] + "-" + wms.getFromDate().split("-")[0]);
                    map.put("toDate", wms.getToDate().split("-")[2] + "-" + wms.getToDate().split("-")[1] + "-" + wms.getToDate().split("-")[0]);
                } else {
                    map.put("fromDate", wms.getFromDate());
                    map.put("toDate", wms.getToDate());
                }
            }
            map.put("userId", wms.getUserId());
            fkReconcileView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.fkReconcileView", map);
            System.out.println("fkReconcileView---" + fkReconcileView.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return fkReconcileView;
    }

    public ArrayList getProductTypeList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getProductTypeList = new ArrayList();
        try {

            map.put("custId", wms.getCustId());
            map.put("empId", wms.getEmpId());
            map.put("day", wms.getDate());
            System.out.println("map  in " + map);
            getProductTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getProductTypeList", map);
            System.out.println("getProductTypeList---" + getProductTypeList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getProductTypeList;
    }

    public ArrayList getSelectedProductType(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getSelectedProductType = new ArrayList();
        try {
            map.put("custId", wms.getCustId());
            map.put("empId", wms.getEmpId());
            map.put("day", wms.getDate());
            getSelectedProductType = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSelectedProductType", map);
            System.out.println("getProductTypeList---" + getSelectedProductType.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getSelectedProductType;
    }

    public ArrayList getHubManagerList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getHubManagerList = new ArrayList();
        try {
            getHubManagerList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getHubManagerList", map);
            System.out.println("getHubManagerList---" + getHubManagerList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getHubManagerList;
    }

    public ArrayList fkOrderUploadView(WmsTO wms) {
        Map map = new HashMap();
        ArrayList fkOrderUploadView = new ArrayList();
        try {
            map.put("orderType", wms.getOrderType());
            map.put("userId", wms.getUserId());
            fkOrderUploadView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.fkOrderUploadView", map);
            System.out.println("fkOrderUploadView---" + fkOrderUploadView.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return fkOrderUploadView;
    }

    public int updateCycleCountAssign(String[] cycleIds, String[] activeInds) {
        Map map = new HashMap();
        int updateCycleCountAssign = 0;
        try {
            for (int i = 0; i < cycleIds.length; i++) {
                map.put("cycleId", cycleIds[i]);
                map.put("activeInd", activeInds[i]);
                updateCycleCountAssign = (Integer) getSqlMapClientTemplate().update("wms.updateCycleCountAssign", map);
                System.out.println("updateCycleCountAssign---" + updateCycleCountAssign);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return updateCycleCountAssign;
    }

    public int saveCycleCountAssign(String empId, String custId, String day, String[] productType, String[] productTypeIds, String[] selected) {
        Map map = new HashMap();
        int saveCycleCountAssign = 0;
        try {
            map.put("custId", custId);
            map.put("empId", empId);
            map.put("day", day);
            for (int i = 0; i < productType.length; i++) {
                if ("1".equals(selected[i])) {
                    map.put("productType", productType[i]);
                    map.put("productTypeId", productTypeIds[i]);
                    saveCycleCountAssign = (Integer) getSqlMapClientTemplate().insert("wms.saveCycleCountAssign", map);
                    System.out.println("saveCycleCountAssign---" + saveCycleCountAssign);
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return saveCycleCountAssign;
    }

    public int insertDispatchDetails(WmsTO wmsTO, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertDispatch = 0;
        /*
         * set the parameters in the map for sending to ORM
         */

        try {
            String[] invoiceNoE = wmsTO.getInvoiceNoE();
            String[] orderIdE = wmsTO.getOrderIdE();
            String[] qtyE = wmsTO.getQtyE();
            String[] customerIdE = wmsTO.getCustomerIdE();
            String[] itemIdE = wmsTO.getItemsides();
            String[] supplyIdE = wmsTO.getSupplyId();
            map.put("plannedDate", wmsTO.getPlannedDate());
            map.put("whId", wmsTO.getWhId());
            map.put("userId", wmsTO.getUserId());
            map.put("transferType", wmsTO.getTransferType());
            map.put("vendorId", wmsTO.getVendorId());
            map.put("supplierId", wmsTO.getSupplierId());
            map.put("vehicleTypeId", wmsTO.getVehicleTypeId());
            map.put("totQty", wmsTO.getTotalQty());
            if (wmsTO.getTranshipmentWhId() != null && !"".equals(wmsTO.getTranshipmentWhId())) {
                map.put("TranshipmentWhId", wmsTO.getTranshipmentWhId());
            } else {
                map.put("TranshipmentWhId", "0");
            }
            map.put("invoiceAmt", wmsTO.getInvoiceAmounts());
            map.put("perTaxValue", wmsTO.getPertaxvalue());
            String whCode = (String) session.queryForObject("wms.getHubCode", map);
            int length = whCode.length();
            length = length + 8;
            map.put("length", length);
            map.put("totTaxVol", wmsTO.getTaxvalue());
            Date date1 = new Date();
            String str = new SimpleDateFormat("yyyy-MM-dd").format(date1);
            String date = str.replace("-", "");
            String dispatchCode = whCode + date;
            map.put("dispatchCode", dispatchCode);
            int sequence = (Integer) session.queryForObject("wms.connectDispatchIdSequence", map);
            String x = sequence + "";
            if (x.length() == 1) {
                map.put("dispatchNo", whCode + date + "000" + x);
            } else if (x.length() == 2) {
                map.put("dispatchNo", whCode + date + "00" + x);
            } else if (x.length() == 3) {
                map.put("dispatchNo", whCode + date + "0" + x);
            } else if (x.length() == 4) {
                map.put("dispatchNo", whCode + date + x);
            }
            System.out.println("map map map = " + map);
            map.put("TranshipmentStatus", "0");
            if ("7".equals(wmsTO.getTransferType())) {
                map.put("TranshipmentStatus", "1");
            }
            insertDispatch = (Integer) session.insert("wms.insertDispatch", map);
            map.put("dispatchId", insertDispatch);
            int stockType = (Integer) session.queryForObject("wms.getConnectSupplierStockType", map);
            if (stockType == 0) {
                int status = (Integer) session.update("wms.updateConnectPicklist", map);
            }
            for (int i = 0; i < invoiceNoE.length; i++) {

                map.put("invoiceNoE", invoiceNoE[i]);
                map.put("orderIdE", orderIdE[i]);
                map.put("qtyE", qtyE[i]);
                map.put("customerIdE", customerIdE[i]);
                map.put("suppId", supplyIdE[i]);
                map.put("invoiceAmt", wmsTO.getDisinvoiceAmt()[i]);
                map.put("perTax", wmsTO.getDisPerTax()[i]);
                System.out.println("map map map = " + map);

                insertDispatch = (Integer) session.insert("wms.insertDispatchDetails", map);
                map.put("invoiceStatus", "1");
                insertDispatch = (Integer) session.update("wms.updateInvoiceStatus", map);

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertDispatch Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateDepositId", sqlException);
        }
        return insertDispatch;
    }

    public ArrayList getSupplierList() {
        Map map = new HashMap();
        ArrayList getWhList = new ArrayList();
        try {
            getWhList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSupplierList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getWhList;

    }

    public ArrayList getDealerList() {
        Map map = new HashMap();
        ArrayList getWhList = new ArrayList();
        try {
            getWhList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDealerList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getWhList;

    }

    public ArrayList getSuppwhList() {
        Map map = new HashMap();
        ArrayList getWhList = new ArrayList();
        try {
            getWhList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSuppwhList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getWhList;

    }

    public ArrayList getVehicleTypeList() {
        Map map = new HashMap();
        ArrayList getWhList = new ArrayList();
        try {
            getWhList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getVehicleTypeList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getWhList;

    }

    public ArrayList getCustInvoiceDetails(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList getCustInvoiceDetails = new ArrayList();
        try {
            map.put("suppId", wmsTO.getSupplierId());
//            map.put("suppId", wmsTO.getSuppId());
            map.put("fromDate", wmsTO.getFromDate());
            map.put("toDate", wmsTO.getToDate());
            map.put("userId", wmsTO.getUserId());
            map.put("whId", wmsTO.getWhId());
            System.out.println("map map map = " + map);
            getCustInvoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getCustInvoiceDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getCustInvoiceDetails;

    }

    public int insertInvoiceDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertDispatch = 0;
        int dispatchNo = 0;
        /*
         * set the parameters in the map for sending to ORM
         */

        try {
            String[] invoiceNoE = wmsTO.getInvoiceNoE();
            map.put("plannedDate", wmsTO.getPlannedDate());
            map.put("whId", wmsTO.getWhId());
            map.put("userId", wmsTO.getUserId());
            map.put("customerId", wmsTO.getCustomerId());
            map.put("totQty", wmsTO.getTotQty());
            map.put("totVol", wmsTO.getTotVol());
            map.put("totWgt", wmsTO.getTotWgt());
            map.put("totTaxVol", wmsTO.getTotTaxVol());

            dispatchNo = (Integer) getSqlMapClientTemplate().queryForObject("wms.getDispatchNo", map);
            System.out.println("dispatchNo-----" + dispatchNo);

            String RecoSequence = "" + dispatchNo;

            if (RecoSequence == null) {
                RecoSequence = "00001";
            } else if (RecoSequence.length() == 1) {
                RecoSequence = "0000" + RecoSequence;
            } else if (RecoSequence.length() == 2) {
                RecoSequence = "000" + RecoSequence;
            } else if (RecoSequence.length() == 3) {
                RecoSequence = "00" + RecoSequence;
            } else if (RecoSequence.length() == 4) {
                RecoSequence = "0" + RecoSequence;
            } else if (RecoSequence.length() == 5) {
                RecoSequence = "" + RecoSequence;
            }

            String reconcileCode = "DP/2021/" + RecoSequence;
            map.put("dispatchNo", reconcileCode);
            System.out.println("map map map = " + map);
            insertDispatch = (Integer) getSqlMapClientTemplate().insert("wms.insertDispatch", map);
            map.put("dispatchId", insertDispatch);
            System.out.println("insertDispatch " + insertDispatch);
            System.out.println("invoiceNoE.length" + invoiceNoE.length);
            System.out.println("invoiceNoE.length" + invoiceNoE.length);
            for (int i = 0; i < invoiceNoE.length; i++) {

                String Inv = invoiceNoE[i];

                String invoice = Inv.split("~")[0];
                String orderE = Inv.split("~")[1];
                String qtyE = Inv.split("~")[2];
                String wgt = Inv.split("~")[3];
                String vol = Inv.split("~")[4];
                String taxVal = Inv.split("~")[5];
                map.put("invoiceNoE", invoice);
                map.put("orderIdE", orderE);
                map.put("qtyE", qtyE);
                map.put("wgt", wgt);
                map.put("vol", vol);
                map.put("taxVal", taxVal);
                System.out.println("map map map = " + map);
                int updateDepositId = (Integer) getSqlMapClientTemplate().insert("wms.insertInvoiceDetails", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertDispatch Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateDepositId", sqlException);
        }
        return insertDispatch;
    }

    public int deleteInvoiceConnectTemp(int userId) {
        Map map = new HashMap();
        int deleteStatus = 0;
        try {
            map.put("userId", userId);
            deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deleteInvoiceConnectTemp", map);
            System.out.println("deleteUploadTemp---" + deleteStatus);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return deleteStatus;
    }

    public int connectUploadInvoice(WmsTO WmsTO, int userId, String whId, String supplyId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int lrNo = 0;
        int checkInvoice = 0;
        int pinWhCheck = 0;
        ArrayList getconnectInvoice = new ArrayList();
        WmsTO wmsTO = new WmsTO();
        try {
            System.out.println("--1111--");
            map.put("userId", userId);
            map.put("whId", whId);
            map.put("supplyId", supplyId);
            String hubCode = (String) getSqlMapClientTemplate().queryForObject("wms.getHubCode", map);
            String invoiceId = "";
            int ifbBatch = (Integer) getSqlMapClientTemplate().queryForObject("wms.getIfbBatch", map);
            map.put("batch", ifbBatch);

            WmsTO wms1 = new WmsTO();
            ArrayList getconnectInvoiceMasterTemp = new ArrayList();
            getconnectInvoiceMasterTemp = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getconnectInvoiceMasterTemp", map);
            Iterator itr1 = getconnectInvoiceMasterTemp.iterator();
            while (itr1.hasNext()) {
                wms1 = (WmsTO) itr1.next();
                map.put("invoiceNo", wms1.getInvoiceNo());
                map.put("invoiceDate", wms1.getInvoiceDate());
                map.put("invoiceTime", wms1.getInvoiceTime());
                map.put("plantCode", wms1.getPlantCode());
                map.put("partyName", wms1.getPartyName());
                map.put("partyCode", wms1.getPartyCode());
                map.put("qty", wms1.getQty());
                map.put("preTaxValue", wms1.getPretaxValue());
                map.put("invoiceAmount", wms1.getInvoiceAmount());
                int insertConnectInvoiceMaster = (Integer) getSqlMapClientTemplate().update("wms.insertConnectInvoiceMaster", map);
            }

            getconnectInvoice = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getconnectInvoice", map);
            System.out.println("getconnectInvoice---" + getconnectInvoice.size());
            Iterator itr = getconnectInvoice.iterator();
            while (itr.hasNext()) {
                WmsTO = (WmsTO) itr.next();
                map.put("plantCode", WmsTO.getPlantCode());
                map.put("shipToPartycode", WmsTO.getShipToPartycode());
                map.put("shipToPartyName", WmsTO.getShipToPartyName());
                map.put("invoiceDate", WmsTO.getInvoiceDate());
                map.put("invoiceNo", WmsTO.getInvoiceNo());
                map.put("materialCode", WmsTO.getMaterialCode());
                map.put("materialDescription", WmsTO.getMaterialDescription());
                map.put("billedQuantity", WmsTO.getBilledQuantity());
                map.put("basicPrice", WmsTO.getBasicPrice());
                map.put("mRP", WmsTO.getmRP());
                map.put("pretaxValue", WmsTO.getPretaxValue());
                map.put("supplierId", WmsTO.getSupplierId());
                map.put("invoiceTime", WmsTO.getInvoiceTime());
                map.put("taxValue", WmsTO.getTaxValue());
                map.put("invoiceAmount", WmsTO.getInvoiceAmount());
                map.put("purchaseOrderNo", WmsTO.getPurchaseOrderNo());
                map.put("purchaseOrderDate", WmsTO.getPurchaseOrderDate());
                map.put("saleOrderNo", WmsTO.getOrderNo());
                map.put("saleOrderDate", WmsTO.getSaleOrderDate());
                int status = Integer.parseInt(WmsTO.getStatus());
                invoiceId = (String) getSqlMapClientTemplate().queryForObject("wms.getConnectInvoiceId", map);
                map.put("invoiceId", invoiceId);
                int productId = (Integer) getSqlMapClientTemplate().queryForObject("wms.getproductId", map);
                map.put("productId", productId);

                System.out.println("map = " + map);
//                 checkInvoice = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkInvoice", map);

                if (status == 0) {
                    int getIfbOrderUploads = (Integer) getSqlMapClientTemplate().update("wms.connectUploadInvoice", map);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public int uploadconnectInvoice(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int uploadIfbOrder = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        int pincode = 0;
        map.put("plantCode", wmsTO.getPlantCode());
        map.put("shipToPartycode", wmsTO.getShipToPartycode());
        map.put("shipToPartyName", wmsTO.getShipToPartyName());
        map.put("invoiceDate", wmsTO.getInvoiceDate());
        map.put("invoiceNo", wmsTO.getInvoiceNo());
        map.put("materialCode", wmsTO.getMaterialCode());
        map.put("materialDescription", wmsTO.getMaterialDescription());
        map.put("billedQuantity", wmsTO.getBilledQuantity());
        map.put("basicPrice", wmsTO.getBasicPrice());
        map.put("mRP", wmsTO.getmRP());
        map.put("pretaxValue", wmsTO.getPretaxValue());
        map.put("taxValue", wmsTO.getTaxValue());
        map.put("invoiceAmount", wmsTO.getInvoiceAmount());
        map.put("purchaseOrderNo", wmsTO.getPurchaseOrderNo());
        map.put("purchaseOrderDate", wmsTO.getPurchaseOrderDate());
        map.put("saleOrderNo", wmsTO.getSaleOrderNo());
        map.put("saleOrderDate", wmsTO.getSaleOrderDate());
        map.put("currency", wmsTO.getCurrency());
        map.put("hSNCode", wmsTO.gethSNCode());
        map.put("cGSTAmount", wmsTO.getcGSTAmount());
        map.put("sGSTAmount", wmsTO.getsGSTAmount());
        map.put("iGSTAmount", wmsTO.getiGSTAmount());
        map.put("invoiceTime", wmsTO.getInvoiceTime());
        map.put("deliveryNumber", wmsTO.getDeliveryNumber());
        map.put("matGrptext", wmsTO.getMatGrptext());
        map.put("discountAmount", wmsTO.getDiscountAmount());
        map.put("invoiceID", wmsTO.getInvoiceId());
        map.put("supplierId", wmsTO.getSupplierId());
        map.put("userId", wmsTO.getUserId());

//        BGMAL start
//        BGMAL end
        System.out.println("mappint    " + map);
        try {
            int dealerPincode = 0;
            int batch = (Integer) getSqlMapClientTemplate().queryForObject("wms.getIfbBatch", map);
            map.put("batch", batch);

            map.put("pincode", pincode);
            int checkPartyCode = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkPartyCode", map);
            int checkInvoice = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkInvoice", map);
            int checkMaterial = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkMaterialCode", map);

            if (checkMaterial > 0) {
                if (checkInvoice == 0) {
                    if (checkPartyCode > 0) {
                        map.put("status", "0");
                    } else {
                        map.put("status", "3");
                    }
                } else {
                    map.put("status", "2");
                }
            } else {
                map.put("status", "1");
            }
            System.out.println("map map map = " + map);
            uploadIfbOrder = (Integer) getSqlMapClientTemplate().insert("wms.insertconnectInvoice", map);
            System.out.println("uploadIfbOrder size=====" + uploadIfbOrder);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadNormalOrder", sqlException);
        }
        return uploadIfbOrder;
    }

    public ArrayList getconnectInvoice(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getconnectInvoice = new ArrayList();
        try {

            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            getconnectInvoice = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getconnectInvoice", map);
            System.out.println("getIFBList========" + getconnectInvoice.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getconnectInvoice;

    }

    public ArrayList getDispatchDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getDispatchDetails = new ArrayList();
        try {
            map.put("status", wms.getStatus());
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("whId", wms.getWhId());
            map.put("type", wms.getType());
            map.put("roleId", wms.getRoleId());
            System.out.println("map == " + map);
            getDispatchDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDispatchDetails", map);
            System.out.println("getDispatchDetails====" + getDispatchDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getDispatchDetails;

    }

    public ArrayList getWareHouse(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getWareHouse = new ArrayList();
        try {
            System.out.println("map == " + map);
            map.put("roleId", wms.getRoleId());
            getWareHouse = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getWareHouse", map);
            System.out.println("getWareHouse====" + getWareHouse.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getWareHouse;

    }

    public ArrayList getdispatchList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getdispatchList = new ArrayList();

        try {
            map.put("dispatchId", wms.getDispatchId());
            getdispatchList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getdispatchList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getdispatchList;

    }

    public ArrayList getdispatch(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getdispatch = new ArrayList();

        try {
            map.put("dispatchId", wms.getDispatchId());
            getdispatch = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getdispatch", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getdispatch;

    }

    public ArrayList getVechicleDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getVechicleDetails = new ArrayList();

        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("tatType", wms.getTatType());
            getVechicleDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getVehicleDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getVechicleDetails;

    }

    public int updateDcGenerate(WmsTO wmsTO, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int status = 0;
        int startId = 0;
        int lrNumbercheck = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        try {
            String accYear = ThrottleConstants.accountYear;
            String stockType = wmsTO.getStockType();
            String supplierId = wmsTO.getSupplierId();
            String[] picklistIds = wmsTO.getPicklistIds().split("~");
            map.put("dispatchId", wmsTO.getDispatchid());
            map.put("tripId", wmsTO.getDispatchid());
            map.put("loadingEnd", wmsTO.getLoadingEnd());
            map.put("loadingEndTime", wmsTO.getLoadingEndTime());
            map.put("loadingStart", wmsTO.getLoadingStart());
            map.put("loadingStartTime", wmsTO.getLoadingStartTime());
            map.put("tatId", wmsTO.getVehicleNumber());
            map.put("vehicleNo", wmsTO.getVehicleNo());
            map.put("transferid", wmsTO.getTansferid());
            map.put("vendorid", wmsTO.getVendorId());
            map.put("whId", wmsTO.getWhId());
            map.put("userId", wmsTO.getUserId());
            map.put("boxQty", wmsTO.getBoxQty());
            map.put("remarks", wmsTO.getRemarks());
            map.put("supervisorName", wmsTO.getSupervisorName());

            String[] invoiceId = wmsTO.getInvoiceIds();
            String[] invoiceDetailIds = wmsTO.getInvoiceDetailIds();
            String[] customerId = wmsTO.getCustomerIds();
            String[] quantity = wmsTO.getQuantitys();
            String[] itemId = wmsTO.getItemIds();
            String[] pendingReason = wmsTO.getPendingReasons();
            String[] dcQty = wmsTO.getDcQty();
            String[] pendingQty = wmsTO.getPendingQty();
            String hubCode = (String) session.queryForObject("wms.getHubCode", map);
            if (hubCode == null || "".equals(hubCode)) {
                hubCode = "HSC";
            }
            Date date = new Date();
            String str = new SimpleDateFormat("yyyy-MM-dd").format(date);
            String[] date1 = str.split("-");
            map.put("month", date1[1]);
            String seq = "HSC" + accYear + hubCode + date1[1];
            map.put("lrnew", seq);
            int length = seq.length();
            map.put("length", length);
            int lrNumberCount = (Integer) session.queryForObject("wms.getConnectLrCount", map);
            lrNumbercheck = lrNumberCount;
            System.out.println("dispatchNo-----" + lrNumbercheck);

            String lrSequence = "" + lrNumbercheck;

            if (lrSequence == null) {
                lrSequence = "0001";
            } else if (lrSequence.length() == 1) {
                lrSequence = "000" + lrSequence;
            } else if (lrSequence.length() == 2) {
                lrSequence = "00" + lrSequence;
            } else if (lrSequence.length() == 3) {
                lrSequence = "0" + lrSequence;
            } else if (lrSequence.length() == 4) {
                lrSequence = "" + lrSequence;
            }
            String lrNumber = "HSC" + accYear + hubCode + date1[1] + lrSequence;
            map.put("lrNumbercheck", lrNumber);
            map.put("lrdate", str);

            System.out.println("map map map = " + map);
            System.out.println("map map map = " + lrNumber);

            status = (Integer) session.update("wms.updateDispatchmaster", map);
            map.put("statusId", "2");
            map.put("tripCode", wmsTO.getDispatchNo());
            map.put("invoiceNo", wmsTO.getDispatchNo());
            System.out.println("map value is:" + map);
            String src = (String) session.queryForObject("wms.getWhLatLong", map);
            map.put("src", src);
            map.put("dest", src);
            String srcName = (String) session.queryForObject("trip.getTripSourceName", map);
            map.put("srcName", srcName);
            map.put("destName", srcName);
            map.put("custId", "4");
            String device = wmsTO.getDeviceId();
            if (!"".equals(device) && device != null) {
                String deviceId = device.split("-")[0];
                String deviceNo = device.split("-")[1];
                map.put("deviceId", deviceId);
                map.put("deviceNo", deviceNo);
                startId = (Integer) session.insert("trip.insertTripStartApiMaster", map);
                map.put("startId", startId);
            }

            int a = 1;
            for (int i = 0; i < invoiceId.length; i++) {
                String sublrNumber = lrNumber + "-" + a;
                map.put("invoiceId", invoiceId[i]);
                map.put("pendingReason", pendingReason[i]);
                map.put("dcQty", dcQty[i]);
                map.put("customerId", customerId[i]);
                map.put("itemId", itemId[i]);
                int k = 0;

                int sublrId = (Integer) session.queryForObject("wms.getsubLrIdincrement", map);
                k = sublrId + 1;
                int getCountOfSubLr = (Integer) session.queryForObject("wms.getCountOfSubLr", map);
                String existlr = "";
                String existlrId = "";
                if (getCountOfSubLr > 0) {
                    existlr = (String) session.queryForObject("wms.getsubLrNumber", map);
                    existlrId = (String) session.queryForObject("wms.getsubLrId", map);
                }

                System.out.println("existlr" + existlr);
                if (existlr == null || "".equals(existlr)) {
                    System.out.println("1");
                    map.put("sublrNumber", sublrNumber);
                    map.put("sublrId", k);
                    a++;
                } else {
                    System.out.println("2");
                    map.put("sublrId", existlrId);
                    map.put("sublrNumber", existlr);
                }

                map.put("dispatchStatus", "2");
                status = (Integer) session.update("wms.updatesublrDispatchdetails", map);
                map.put("pending", pendingQty[i]);
                map.put("invoiceDetailId", invoiceDetailIds[i]);
                System.out.println("map====" + map);
                status = (Integer) session.update("wms.updatePendingQty", map);
                String pincode = (String) session.queryForObject("wms.getConnectOrderPincode", map);
                if (pincode != null && startId != 0) {
                    map.put("latitude", "");
                    map.put("longitude", "");
                    map.put("clientName", pincode);
                    int insertTripStartApiDetails = (Integer) session.insert("trip.insertTripStartApiDetails", map);
                }
                if (Double.parseDouble(pendingQty[i]) > 0) {
                    map.put("invoiceStatus", "0");
                    System.out.println("invoice status is 0");
                    status = (Integer) session.update("wms.updateConnectInvoiceStatus", map);
                } else {
                    map.put("invoiceStatus", "2");
                    System.out.println("invoice status is 2");
                    status = (Integer) session.update("wms.updateConnectInvoiceStatus", map);
                }
                System.out.println("Invoice Status   " + status);
            }
            map.put("status", "2");
            status = (Integer) session.update("wms.updateConnectDispatchStatus", map);
            if (!"0".equals(stockType)) {
                for (int j = 0; j < picklistIds.length; j++) {
                    map.put("picklistId", picklistIds[j]);
                    System.out.println("picklist   " + picklistIds[j]);
                    status = (Integer) session.update("wms.updateConnectPicklistStatus", map);
                }
                String serialIds = (String) session.queryForObject("wms.getConnectPicklistSerialId", map);
                String[] serialIdArr = serialIds.split(",");
                for (int k = 0; k < serialIdArr.length; k++) {
                    map.put("serialId", serialIdArr[k].split("~")[0]);
                    if ("Y".equals(serialIdArr[k].split("~")[1])) {
                        map.put("usedStatus", "2");
                        status = (Integer) session.update("wms.updateConnectSerialUsedStatus", map);
                    } else {
                        map.put("qty", serialIdArr[k].split("~")[2]);
                        map.put("usedStatus", "1");
                        status = (Integer) session.update("wms.updateConnectSerialStock", map);
                    }
                    status = (Integer) session.update("wms.updateConnectStockDetails", map);
                }
            } else {
                String[] invoiceIdNew = wmsTO.getInvoiceIdE();
                String[] invoiceNoNew = wmsTO.getInvoiceNoE();
                String[] invoiceDetailIdNew = wmsTO.getInvoiceDetailIdE();
                String[] serialNos = wmsTO.getSerialNos();
                String[] qtyNew = wmsTO.getQtyE();
                String[] productIdNew = wmsTO.getProductIds();
                String[] custIdNew = wmsTO.getCustomerIdE();
                String[] uomNew = wmsTO.getUom();
                if (serialNos != null && serialNos.length != 0) {
                    for (int i = 0; i < serialNos.length; i++) {
                        map.put("invoiceId", invoiceIdNew[i]);
                        map.put("invoiceNo", invoiceNoNew[i]);
                        map.put("invoiceDetailId", invoiceDetailIdNew[i]);
                        map.put("serialNo", serialNos[i]);
                        map.put("serialNumber", serialNos[i]);
                        map.put("ipQty", qtyNew[i]);
                        map.put("grnId", "0");
                        map.put("locationIdE", "1");
                        map.put("proCond", "1");
                        map.put("rackIdE", "1");
                        map.put("binId", "1");
                        map.put("remarks", "dc generate");
                        map.put("productId", productIdNew[i]);
                        map.put("itemId", productIdNew[i]);
                        map.put("custId", custIdNew[i]);
                        map.put("uom", uomNew[i]);
                        map.put("qtyAdd", "0");
                        int checkconnectStockId = 0;
                        int checkconnectStockIdcount = (Integer) session.queryForObject("wms.checkconnectStockId", map);
                        if (checkconnectStockIdcount > 0) {
                            checkconnectStockId = (Integer) session.queryForObject("wms.getconnectStockId", map);
                            map.put("stockId", checkconnectStockId);
                            int updateStockqty = (Integer) session.update("wms.updateConnectStock", map);
                        } else {
                            checkconnectStockId = (Integer) session.insert("wms.insertConnectStock", map);
                        }
                        map.put("stockId", checkconnectStockId);
                        int insertSerial = (Integer) session.insert("wms.saveSubmitserial", map);
                        map.put("serialId", insertSerial);
                        int insertPicklist = (Integer) session.insert("wms.insertConnectPicklistMasterNew", map);
                        map.put("usedStatus", "2");
                        int updateConnectSerialUsedStatus = (Integer) session.update("wms.updateConnectSerialUsedStatus", map);
                    }
                }
            }
            if (status > 0) {
                status = (Integer) session.update("wms.updateConnectVehicleOutTime", map);
            }
            String getTranshipmentStatus = (String) session.queryForObject("wms.getTranshipmentStatus", map);
            String[] result = getTranshipmentStatus.split("~");
            String transStatus = result[1];
            String transWhId = result[0];
            if ("1".equals(transStatus)) {
                status = (Integer) session.update("wms.updateDispatchmaster", map);
                String str1 = new SimpleDateFormat("yyyy-MM-dd").format(date);
                String[] date2 = str1.split("-");
                map.put("month", date2[1]);
                String seq1 = "HSC" + accYear + hubCode + date2[1];
                map.put("lrnew1", seq1);
                int length1 = seq1.length();
                map.put("length1", length1);
                int lrNumberCount1 = (Integer) session.queryForObject("wms.getConnectLrCount1", map);
                int lrNumbercheck1 = lrNumberCount1;
                System.out.println("dispatchNo-----" + lrNumbercheck1);

                String lrSequence1 = "" + lrNumbercheck1;

                if (lrSequence1 == null) {
                    lrSequence1 = "0001";
                } else if (lrSequence1.length() == 1) {
                    lrSequence1 = "000" + lrSequence1;
                } else if (lrSequence1.length() == 2) {
                    lrSequence1 = "00" + lrSequence1;
                } else if (lrSequence1.length() == 3) {
                    lrSequence1 = "0" + lrSequence1;
                } else if (lrSequence1.length() == 4) {
                    lrSequence1 = "" + lrSequence1;
                }
                String lrNumber1 = "HSC" + accYear + hubCode + date2[1] + lrSequence1;
                map.put("lrNumbercheck1", lrNumber1);

                int x = 0;
                int b = 1;
                int addRowInDispatchMaster = (Integer) session.insert("wms.addRowInDispatchMaster", map);
                ArrayList getConnectDispatchDetails = (ArrayList) session.queryForList("wms.getConnectDispatchDetails", map);
                Iterator itr = getConnectDispatchDetails.iterator();
                WmsTO wms1 = new WmsTO();
                while (itr.hasNext()) {
                    wms1 = (WmsTO) itr.next();
                    map.put("dispatchId", addRowInDispatchMaster);
                    map.put("customerIdE", wms1.getDealerId());
                    map.put("orderIdE", wms1.getInvoiceId());
                    map.put("suppId", wms1.getSuppId());
                    map.put("qtyE", wms1.getQty());
                    map.put("userId", wms1.getUserId());
                    map.put("activeInd", wms1.getActiveInd());
                    map.put("Created_By", wms1.getCreatedBy());
                    map.put("Created_on", wms1.getCreatedOn());
                    map.put("Modified_By", wms1.getModifiedBy());
                    map.put("Modified_on", wms1.getModifiedOn());
                    map.put("invoiceNoE", wms1.getInvoiceNo());
                    map.put("itemId", wms1.getItemIds());
                    map.put("taxvalue", wms1.getTacVal());
                    map.put("perTax", wms1.getPreTaxValue());
                    map.put("dcQty", wms1.getDcQty());
                    map.put("invoiceAmt", wms1.getInvoiceAmount());

                    int sublrId = (Integer) session.queryForObject("wms.getsubLrIdincrement", map);
                    x = sublrId + 1;
                    int getCountOfSubLr = (Integer) session.queryForObject("wms.getCountOfSubLr", map);
                    String existlr = "";
                    String existlrId = "";
                    if (getCountOfSubLr > 0) {
                        existlr = (String) session.queryForObject("wms.getsubLrNumber", map);
                        existlrId = (String) session.queryForObject("wms.getsubLrId", map);
                    }

                    String sublrNumber = lrNumber1 + "-" + b;
                    if (existlr == null || "".equals(existlr)) {
                        map.put("subLr", sublrNumber);
                        map.put("sublrId", x);
                        b++;
                    } else {
                        map.put("sublrId", existlrId);
                        map.put("", existlr);
                    }

                    int insertDispatchDetails = (Integer) session.insert("wms.insertDispatchDetailsTranshipment", map);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertDispatch Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateDepositId", sqlException);
        }
        return status;
    }

    public ArrayList getconnectlrnumber(String dispatchid) {
        Map map = new HashMap();
        ArrayList getconnectlrnumber = new ArrayList();

        try {
            map.put("dispatchId", dispatchid);
            getconnectlrnumber = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getconnectlrnumber", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getconnectlrnumber;

    }

    public ArrayList getImLrDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImLrDetails = new ArrayList();

        try {
            map.put("lrId", wms.getLrId());
            getImLrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImLrDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getImLrDetails;

    }

    public int updateConnectTranshipment(WmsTO wms) {
        Map map = new HashMap();
        int status = 0;
        int startId = 0;
        int lrNumbercheck = 0;
        try {
            String accYear = ThrottleConstants.accountYear;
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("dispatchId", wms.getDispatchId());
            String getTranshipmentStatus = (String) getSqlMapClientTemplate().queryForObject("wms.getTranshipmentStatus", map);
            String[] result = getTranshipmentStatus.split("~");
            String transStatus = result[1];
            String transWhId = result[0];
            if ("1".equals(transStatus)) {
                String hubCode = (String) getSqlMapClientTemplate().queryForObject("wms.getHubCode", map);
                if (hubCode == null || "".equals(hubCode)) {
                    hubCode = "HSC";
                }
                Date date = new Date();
                String str1 = new SimpleDateFormat("yyyy-MM-dd").format(date);
                String[] date2 = str1.split("-");
                map.put("month", date2[1]);
                String seq1 = "HSC" + accYear + hubCode + date2[1];
                map.put("lrnew1", seq1);
                int length1 = seq1.length();
                map.put("length1", length1);
                int lrNumberCount1 = (Integer) getSqlMapClientTemplate().queryForObject("wms.getConnectLrCount1", map);
                int lrNumbercheck1 = lrNumberCount1;
                System.out.println("dispatchNo-----" + lrNumbercheck1);

                String lrSequence1 = "" + lrNumbercheck1;

                if (lrSequence1 == null) {
                    lrSequence1 = "0001";
                } else if (lrSequence1.length() == 1) {
                    lrSequence1 = "000" + lrSequence1;
                } else if (lrSequence1.length() == 2) {
                    lrSequence1 = "00" + lrSequence1;
                } else if (lrSequence1.length() == 3) {
                    lrSequence1 = "0" + lrSequence1;
                } else if (lrSequence1.length() == 4) {
                    lrSequence1 = "" + lrSequence1;
                }
                String lrNumber1 = "HSC" + accYear + hubCode + date2[1] + lrSequence1;
                map.put("lrNumbercheck1", lrNumber1);

                int x = 0;
                int b = 1;
                int addRowInDispatchMaster = (Integer) getSqlMapClientTemplate().insert("wms.addRowInDispatchMaster", map);
                ArrayList getConnectDispatchDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectDispatchDetails", map);
                Iterator itr = getConnectDispatchDetails.iterator();
                WmsTO wms1 = new WmsTO();
                while (itr.hasNext()) {
                    wms1 = (WmsTO) itr.next();
                    map.put("dispatchId", addRowInDispatchMaster);
                    map.put("customerIdE", wms1.getDealerId());
                    map.put("customerId", wms1.getDealerId());
                    map.put("orderIdE", wms1.getInvoiceId());
                    map.put("suppId", wms1.getSuppId());
                    map.put("qtyE", wms1.getQty());
                    map.put("userId", wms1.getUserId());
                    map.put("activeInd", wms1.getActiveInd());
                    map.put("Created_By", wms1.getCreatedBy());
                    map.put("Created_on", wms1.getCreatedOn());
                    map.put("Modified_By", wms1.getModifiedBy());
                    map.put("Modified_on", wms1.getModifiedOn());
                    map.put("invoiceNoE", wms1.getInvoiceNo());
                    map.put("itemId", wms1.getItemIds());
                    map.put("taxvalue", wms1.getTacVal());
                    map.put("perTax", wms1.getPreTaxValue());
                    map.put("dcQty", wms1.getQuantity());
                    map.put("invoiceAmt", wms1.getInvoiceAmount());

                    int sublrId = (Integer) getSqlMapClientTemplate().queryForObject("wms.getsubLrIdincrement", map);
                    x = sublrId + 1;
                    int getCountOfSubLr = (Integer) getSqlMapClientTemplate().queryForObject("wms.getCountOfSubLr", map);
                    String existlr = "";
                    String existlrId = "";
                    if (getCountOfSubLr > 0) {
                        existlr = (String) getSqlMapClientTemplate().queryForObject("wms.getsubLrNumber", map);
                        existlrId = (String) getSqlMapClientTemplate().queryForObject("wms.getsubLrId", map);
                    }

                    String sublrNumber = lrNumber1 + "-" + b;
                    if (existlr == null || "".equals(existlr)) {
                        map.put("subLr", sublrNumber);
                        map.put("sublrId", x);
                        b++;
                    } else {
                        map.put("sublrId", existlrId);
                        map.put("", existlr);
                    }

                    int insertDispatchDetails = (Integer) getSqlMapClientTemplate().insert("wms.insertDispatchDetailsTranshipment", map);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return status;

    }

    public int saveImGrnInbound(WmsTO wms, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            String accYear = ThrottleConstants.accountYear;
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            String hubCode = (String) session.queryForObject("wms.getHubCode", map);
            if (hubCode == null || "".equals(hubCode)) {
                hubCode = "HSC";
            }
            Date date = new Date();
            String str1 = new SimpleDateFormat("yyyy-MM-dd").format(date);
            String[] date2 = str1.split("-");
            map.put("month", date2[1]);
            String seq1 = "GRN" + accYear + "CHNI" + date2[1];
            map.put("grnCode", seq1);
            int length1 = seq1.length();
            map.put("length", length1);
            int grNumCheck = (Integer) session.queryForObject("wms.checkImGrnNo", map);
            System.out.println("grNumCheck-----" + grNumCheck);
            String grnSequence = "" + grNumCheck;
            if (grnSequence == null) {
                grnSequence = "0001";
            } else if (grnSequence.length() == 1) {
                grnSequence = "000" + grnSequence;
            } else if (grnSequence.length() == 2) {
                grnSequence = "00" + grnSequence;
            } else if (grnSequence.length() == 3) {
                grnSequence = "0" + grnSequence;
            } else if (grnSequence.length() == 4) {
                grnSequence = "" + grnSequence;
            }
            String grnNo = "GRN" + accYear + "CHNI" + date2[1] + grnSequence;
            map.put("grnNo", grnNo);
            map.put("vendorId", wms.getVendorId());
            map.put("vendorName", wms.getVendorName());
            map.put("grnDate", wms.getGrnDate());
            map.put("grnTime", wms.getGrnTime());
            map.put("vehicleNo", wms.getVehicleNo());
            map.put("totKgQty", wms.getTotKgQty());
            map.put("totPcQty", wms.getTotPcQty());
            map.put("totPacketQty", wms.getTotPacketQty());
            map.put("invoiceNo", wms.getInvoiceNo());
            map.put("invoiceDate", wms.getInvoiceDate());
            map.put("receivedBy", wms.getReceivedBy());
            int grnId = 0;
            if (wms.getItemIds() != null && wms.getItemIds().length > 0) {
                grnId = (Integer) session.insert("wms.insertImGrnInbound", map);
            }
            if (grnId > 0) {
                for (int i = 0; i < wms.getItemIds().length; i++) {
                    map.put("skuId", wms.getItemIds()[i]);
                    map.put("skuDescription", wms.getProductDescriptions()[i]);
                    map.put("uom", wms.getUom()[i]);
                    map.put("qty", wms.getQuantitys()[i]);
                    status = (Integer) session.insert("wms.insertImGrnInboundDetails", map);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return status;

    }

    public ArrayList imCratesReport(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList cratesReport = new ArrayList();
        try {
            map.put("whId", wmsTo.getWhId());
            map.put("fromDate", wmsTo.getFromDate());
            map.put("toDate", wmsTo.getToDate());
            cratesReport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imCratesReport", map);
            System.out.println("cratesReport()-----" + cratesReport.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return cratesReport;
    }

    public ArrayList imCratesReportView(WmsTO wms) {
        Map map = new HashMap();
        ArrayList cratesReportView = new ArrayList();
        try {
            map.put("sublrId", wms.getSubLrId());
            System.out.println("map===" + map);
            cratesReportView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imCratesReportView", map);
            System.out.println("cratesReportView()-----" + cratesReportView.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return cratesReportView;
    }

    public int saveImPackagingDetails(WmsTO wms, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("vendorName", wms.getVendorName());
            map.put("empName", wms.getEmpName());
            map.put("machineId", wms.getMachineId());
            map.put("packDate", wms.getDate());
            map.put("packTime", wms.getTime());
            map.put("startTime", wms.getLoadingStartTime());
            map.put("endTime", wms.getLoadingEndTime());
            int packId = 0;
            if (wms.getItemIds() != null && wms.getItemIds().length > 0) {
                packId = (Integer) session.insert("wms.saveImPackagingMaster", map);
            }
            map.put("packId", packId);
            if (packId > 0) {
                for (int i = 0; i < wms.getItemIds().length; i++) {
                    map.put("skuId", wms.getItemIds()[i]);
                    map.put("skuDescription", wms.getProductDescriptions()[i]);
                    map.put("qty", wms.getQuantitys()[i]);
                    status = (Integer) session.insert("wms.saveImPackagingDetails", map);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return status;

    }

    public ArrayList getImGrnInbound(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImGrnInbound = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("grnId", wms.getGrnId());
            getImGrnInbound = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImGrnInbound", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getImGrnInbound;

    }

    public ArrayList getImGrnInboundDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImGrnInboundDetails = new ArrayList();

        try {
            map.put("grnId", wms.getGrnId());
            getImGrnInboundDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImGrnInboundDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getImGrnInboundDetails;

    }

    public ArrayList getImPackagingMaster(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImPackagingMaster = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("packId", wms.getPackId());
            getImPackagingMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImPackagingMaster", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getImPackagingMaster;

    }

    public ArrayList getImPackagingDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImPackagingDetails = new ArrayList();

        try {
            map.put("packId", wms.getPackId());
            getImPackagingDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImPackagingDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getImPackagingDetails;

    }

    public ArrayList getImSubLrDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImSubLrDetails = new ArrayList();

        try {
            map.put("lrId", wms.getLrId());
            getImSubLrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImSubLrDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getImSubLrDetails;

    }

    public ArrayList invoiceMaster(WmsTO wms) {
        Map map = new HashMap();
        ArrayList invoiceMaster = new ArrayList();

        try {
            map.put("lrId", wms.getLrId());
            map.put("whId", wms.getWhId());
            System.out.println("map----------" + map);
            invoiceMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzInvoiceMasterPick", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return invoiceMaster;

    }

    public ArrayList getconnectsublrnumber(String dispatchid) {
        Map map = new HashMap();
        ArrayList getconnectsublrnumber = new ArrayList();

        try {
            map.put("dispatchId", dispatchid);
            System.out.println("wms.getDispatchId()" + dispatchid);
            getconnectsublrnumber = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getconnectsublrnumber", map);
            System.out.println("getconnectsublrnumber" + getconnectsublrnumber.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getconnectsublrnumber;

    }

    public ArrayList getCellList(String supplyId) {
        Map map = new HashMap();
        ArrayList getWhList = new ArrayList();
        try {
            map.put("supplyId", supplyId);
            System.out.println("map map map = " + map);
            getWhList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getCellList", map);
            System.out.println("getCellList==" + getWhList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getWhList;

    }

    public ArrayList getRackDetails() {
        Map map = new HashMap();
        ArrayList getrackDetails = new ArrayList();

        try {

            getrackDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getrackDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getrackDetails;

    }

    public ArrayList getBinDetails() {
        Map map = new HashMap();
        ArrayList getbinDetails = new ArrayList();

        try {

            getbinDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getbinDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getbinDetails;

    }

    public int insertStoragewareHouse(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStoragewareHouse = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        int pincode = 0;
        map.put("rackid", wmsTO.getRackId());
        map.put("storageid", wmsTO.getStorageId());
        map.put("binid", wmsTO.getBinId());

        map.put("status", wmsTO.getStatus());
        map.put("storafeWhId", wmsTO.getStorageWhid());

        System.out.println("mappint    " + map);
        try {

            System.out.println("map map map = " + map);
            insertStoragewareHouse = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkStorage", map);
            if (insertStoragewareHouse == 0) {

                if ((wmsTO.getStorageWhid() != null && wmsTO.getStorageWhid() != "")) {
                    System.out.println("enterupdate");
                    insertStoragewareHouse = (Integer) getSqlMapClientTemplate().update("wms.upadteStorageWareHouse", map);
                    insertStoragewareHouse = 2;

                } else {
                    System.out.println("enterNormal");

                    insertStoragewareHouse = (Integer) getSqlMapClientTemplate().insert("wms.insertStorageWareHouse", map);
                    System.out.println("insertStoragewareHouse size=====" + insertStoragewareHouse);
                    insertStoragewareHouse = 3;
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadNormalOrder", sqlException);
        }
        return insertStoragewareHouse;
    }

    public ArrayList getStorageWareHouse() {
        Map map = new HashMap();
        ArrayList getStorageWareHouse = new ArrayList();

        try {

            getStorageWareHouse = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getStorageWareHouse", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getStorageWareHouse;

    }

    public ArrayList getReceivedgrn(int userId) {
        Map map = new HashMap();
        ArrayList getReceivedgrn = new ArrayList();
        try {
            map.put("userId", userId);
            System.out.println("userId==========" + userId);
            getReceivedgrn = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getReceivedgrn", map);
            System.out.println("getReceivedGateIn.size()-----" + getReceivedgrn.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getReceivedgrn;

    }

    public ArrayList getGrnDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getGrnDetails = new ArrayList();
        try {
            map.put("grnId", wmsTo.getGrnId());
            map.put("userId", wmsTo.getUserId());
            map.put("whid", wmsTo.getWhId());
            map.put("productId", wmsTo.getProductID());
            System.out.println("map=======" + map);

            getGrnDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getGrnDetails", map);
            System.out.println("getBinDetails.size()-----" + getGrnDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getGrnDetails;

    }

    public ArrayList getGrnproductDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getGrnproductDetails = new ArrayList();
        try {
            map.put("grnId", wmsTo.getGrnId());
            System.out.println("grnId===" + wmsTo.getGrnId());
            getGrnproductDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getGrnproductDetails", map);
            System.out.println("getGrnproductDetails.size()-----" + getGrnproductDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getGrnproductDetails;

    }

    public String checkconnectSerialNo(String SerialNo, String grnId) {
        Map map = new HashMap();
        String status = "";
        int checkconnectSerialNo = 0;

        try {
            System.out.println("connectSerialNo" + SerialNo);
            map.put("serialNo", SerialNo);
            map.put("grnId", grnId);

            System.out.println("MAP----" + map);

            checkconnectSerialNo = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkConnectSerialNoMaster", map);
            System.out.println("connectSerialNo-----" + checkconnectSerialNo);

            if (checkconnectSerialNo > 0) {
                status = "1";

            } else {
                if (!"".equals(grnId) && grnId != null) {
                    int serialMasterStatus = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkconnectSerialNo", map);
                    if (serialMasterStatus > 0) {
                        status = "2";
                    } else {
                        status = "0";
                    }
                } else {
                    status = "0";
                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return status;

    }

    public int inserconnectSerialNumber(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertSerialNumber = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        int pincode = 0;
        map.put("rackid", wmsTO.getRackId());
        map.put("storageid", wmsTO.getStorageId());
        map.put("binid", wmsTO.getBinId());
        String[] serialNo = wmsTO.getSerialNos();
        map.put("grnId", wmsTO.getGrnId1());
        map.put("whId", wmsTO.getWhId());
        map.put("Qty", wmsTO.getQty());
        map.put("productId", wmsTO.getProductID());
        map.put("userId", wmsTO.getUserId());

        try {

            System.out.println("map map map = " + map);
            if (serialNo != null) {
                for (int i = 0; i < serialNo.length; i++) {
                    map.put("qtyAdd", serialNo.length);
                    if (wmsTO.getSerialNos()[i] != null) {
                        int uom;
                        int procond;
                        String ipQty;
                        if (wmsTO.getUom()[i].equals("Pack")) {
                            uom = 1;
                        } else {
                            uom = 2;
                        }
                        if (wmsTO.getUom()[i].equals("Pack")) {
                            ipQty = wmsTO.getIpQty()[i];
                        } else {
                            ipQty = "1";
                        }
                        if (wmsTO.getProCond()[i].equals("Good")) {
                            System.out.println("wmsTO.getProCond()[i]" + wmsTO.getProCond()[i]);
                            procond = 1;
                        } else if (wmsTO.getProCond()[i].equals("Damaged")) {
                            procond = 2;

                        } else {
                            procond = 3;

                        }
                        map.put("serialNo", wmsTO.getSerialNos()[i]);
                        map.put("binId", wmsTO.getBinIds()[i]);
                        map.put("locationIdE", wmsTO.getLocatioId()[i]);
                        map.put("rackIdE", wmsTO.getRackIdE()[i]);
                        map.put("uom", uom);
                        map.put("ipQty", ipQty);
                        map.put("proCond", procond);
//                    map.put("proCond", procond);
                        System.out.println("map===" + procond + "" + map);

                        map.put("status", "1");

                        insertSerialNumber = (Integer) getSqlMapClientTemplate().insert("wms.insertConnectSerilaNo", map);

                    }

                }

                int getGrnQty = (Integer) getSqlMapClientTemplate().queryForObject("wms.getGrnQty", map);
                System.out.println("grn count==" + getGrnQty);
                int serialNumbercount = (Integer) getSqlMapClientTemplate().queryForObject("wms.getserialCount", map);
                System.out.println("grn serial==" + serialNumbercount);

                int totalSerialNumber = serialNumbercount;
                System.out.println("grn total==" + serialNumbercount);

                if (totalSerialNumber == getGrnQty) {
                    map.put("status", "2");
                    int updateSerialStatus = (Integer) getSqlMapClientTemplate().update("wms.updateSerialStatus", map);
                    map.put("grnStatus", "1");
                    int updategrnStatus = (Integer) getSqlMapClientTemplate().update("wms.updateGrnStatus", map);

                }

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadNormalOrder", sqlException);
        }
        return insertSerialNumber;
    }

    public int insertNonScanSerialNumber(String uom, String storageId, String rackId, String binId, String quantity, WmsTO wms) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertSerialNumber = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("whId", wms.getWhId());

        try {
            if (uom.equals("Pack")) {
                uom = "1";
            } else {
                uom = "2";
            }
            map.put("locationIdE", storageId);
            map.put("rackIdE", rackId);
            map.put("binId", binId);
            map.put("userId", wms.getUserId());
            map.put("serialNo", "Non Scannable");
            map.put("ipQty", quantity);
            map.put("proCond", "1");
            map.put("uom", uom);
            map.put("productId", wms.getProductId());
            map.put("grnId", wms.getGrnId());

            insertSerialNumber = (Integer) getSqlMapClientTemplate().insert("wms.insertConnectSerilaNo", map);
            int getGrnQty = (Integer) getSqlMapClientTemplate().queryForObject("wms.getGrnQty", map);
            System.out.println("grn count==" + getGrnQty);
            int serialNumbercount = (Integer) getSqlMapClientTemplate().queryForObject("wms.getserialCount", map);
            System.out.println("grn serial==" + serialNumbercount);

            if (serialNumbercount == getGrnQty) {
                map.put("status", "2");
                int updateSerialSatus = (Integer) getSqlMapClientTemplate().update("wms.updateSerialStatus", map);
                map.put("grnStatus", "1");
                int updategrnStatus = (Integer) getSqlMapClientTemplate().update("wms.updateGrnStatus", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadNormalOrder", sqlException);
        }
        return insertSerialNumber;
    }

    public ArrayList getConnectSerialNumberDetails(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList getConnectSerialDetails = new ArrayList();

        try {
            map.put("grnId", wmsTO.getGrnId1());
            map.put("productId", wmsTO.getProductID());
            System.out.println("map in getConnectSerialDetails  " + map);
            getConnectSerialDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectSerialNumber", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getConnectSerialDetails;

    }

    public int delconnectSerialno(String serialNo, String grnId, String itemId) {
        Map map = new HashMap();
        int status = 0;

        try {
            map.put("SerialId", serialNo);
            map.put("grnId", grnId);
            map.put("itemId", itemId);
            System.out.println("tempId====" + serialNo);
//            int checkconnectStockId = (Integer) getSqlMapClientTemplate().queryForObject("wms.getdropQtyconnectStockId", map);
            status = (Integer) getSqlMapClientTemplate().update("wms.delConnectSerialNo", map);
//            map.put("stockId",checkconnectStockId);
//         int upadteStockqty = (Integer) getSqlMapClientTemplate().update("wms.updatedropConnectStock", map);

//            status = (Integer) getSqlMapClientTemplate().update("wms.delConnectSerialNo", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return status;

    }

    public int deleteConnectgrnTemp(int userId) {
        Map map = new HashMap();
        int deleteStatus = 0;
        try {
            map.put("userId", userId);
            deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deleteConnectgrnTemp", map);
            System.out.println("deleteUploadTemp---" + deleteStatus);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return deleteStatus;
    }

    public int uploadconnectGrn(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int uploadConnectGrn = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        int pincode = 0;
        map.put("userId", wmsTO.getUserId());
        map.put("gateInDate", wmsTO.getGateInwardDate());
        map.put("gateInNo", wmsTO.getGateInwardNo());
        map.put("processType", wmsTO.getProcessType());
        map.put("modeOftrans", wmsTO.getModeofTransport());
        map.put("vehicleNo", wmsTO.getVehicleNo());
        map.put("transporterCode", wmsTO.getTransporterCode());
        map.put("transPorterName", wmsTO.getTransporterName());
        map.put("serialNo", wmsTO.getSerials());
        map.put("giQty", wmsTO.getGiQuantity());
        map.put("uoms", wmsTO.getUoms());
        map.put("billNum", wmsTO.getBillNum());
        map.put("billDate", wmsTO.getBillDate());
        map.put("matDocNo", wmsTO.getMatDocNo());
        map.put("matDocDate", wmsTO.getMatDocDate());
        map.put("matDocYear", wmsTO.getMatDocYear());
        map.put("postingDate", wmsTO.getPostingDate());
        map.put("plantCode", wmsTO.getSuppPlantCode());
        map.put("plantDesc", wmsTO.getSuppPlantDesc());
        map.put("resplantCode", wmsTO.getRecPlantCode());
        map.put("recPlantDesc", wmsTO.getRecPlantDesc());
        map.put("grQty", wmsTO.getGrQty());
        map.put("storageLoc", wmsTO.getStorageLocation());
        map.put("purchaseOrderNo", wmsTO.getPurchaseOrderNo());
        map.put("deliveryNo", wmsTO.getDeliveryNumber());
        map.put("movementType", wmsTO.getMovementType());
        map.put("create", wmsTO.getCreatedBy());
        map.put("cancelledno", wmsTO.getCancelledNo());
        map.put("materialCode", wmsTO.getMaterialCode());
        map.put("materialdes", wmsTO.getMaterialDescription());
        map.put("invoiceNumber", wmsTO.getInvoiceNo());
        map.put("invoiceDate", wmsTO.getInvoiceDate());

        System.out.println("mappint    " + map);
        try {
            int dealerPincode = 0;

            int checkMaterial = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkMaterialCode", map);

            System.out.println("checkMaterial" + checkMaterial);

            if (checkMaterial > 0) {

                map.put("status", "0");

            } else {
                map.put("status", "1");
            }
            System.out.println("map map map = " + map);
            uploadConnectGrn = (Integer) getSqlMapClientTemplate().insert("wms.insertGrnUploadTemp", map);
            System.out.println("uploadIfbOrder size=====" + uploadConnectGrn);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadNormalOrder", sqlException);
        }
        return uploadConnectGrn;
    }

    public ArrayList getConnectgrnDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getConnectgrnDetails = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            getConnectgrnDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getconnectgrnDetails", map);
            System.out.println("getIFBList========" + getConnectgrnDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getConnectgrnDetails;

    }

    public int connectGrnUpload(WmsTO WmsTO, int userId, String whId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int lrNo = 0;
        int checkInvoice = 0;
        int pinWhCheck = 0;
        ArrayList getconnectInvoice = new ArrayList();
        WmsTO wmsTO = new WmsTO();
        try {
            System.out.println("--1111--");
            map.put("userId", userId);
            map.put("whId", whId);
            map.put("tatId", WmsTO.getTatId());
            System.out.println("tatt===" + WmsTO.getTatId());

            ArrayList getConnectgrnDetails = new ArrayList();
            getConnectgrnDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getconnectgrnDetails", map);
            System.out.println("getconnectInvoice---" + getconnectInvoice.size());
            int checkGrnNumberCount = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkConnectGrnNumberCount", map);;
            int grnNumbercheck = 1;
            if (checkGrnNumberCount > 0) {
                grnNumbercheck = (Integer) getSqlMapClientTemplate().queryForObject("wms.getConnectGrnNumber", map);
            }
            String grnSequence = "" + grnNumbercheck;

            if (grnSequence == null) {
                grnSequence = "00001";
            } else if (grnSequence.length() == 1) {
                grnSequence = "0000" + grnSequence;
            } else if (grnSequence.length() == 2) {
                grnSequence = "000" + grnSequence;
            } else if (grnSequence.length() == 3) {
                grnSequence = "00" + grnSequence;
            } else if (grnSequence.length() == 4) {
                grnSequence = "0" + grnSequence;
            } else if (grnSequence.length() == 5) {
                grnSequence = "" + grnSequence;
            }
            Date date = new Date();
            String str = new SimpleDateFormat("yyyy-MM-dd").format(date);
            String[] date1 = str.split("-");

            String grnCode = "GRN/" + date1[0] + "/" + grnSequence;

            Iterator itr = getConnectgrnDetails.iterator();
            while (itr.hasNext()) {
                WmsTO = (WmsTO) itr.next();

                map.put("gateInDate", WmsTO.getGateInwardDate());

                map.put("gateInNo", WmsTO.getGateInwardNo());

                map.put("processType", WmsTO.getProcessType());

                map.put("modeOftrans", WmsTO.getModeofTransport());

                map.put("vehicleNo", WmsTO.getVehicleNo());

                map.put("transporterCode", WmsTO.getTransporterCode());

                map.put("transPorterName", WmsTO.getTransporterName());

                map.put("serialNo", WmsTO.getSerials());

                map.put("giQty", WmsTO.getGiQuantity());

                map.put("uoms", WmsTO.getUoms());

                map.put("billNum", WmsTO.getBillNum());

                map.put("billDate", WmsTO.getBillDate());

                map.put("matDocNo", WmsTO.getMatDocNo());

                map.put("matDocDate", WmsTO.getMatDocDate());

                map.put("matDocYear", WmsTO.getMatDocYear());

                map.put("postingDate", WmsTO.getPostingDate());

                map.put("plantCode", WmsTO.getSuppPlantCode());

                map.put("plantDesc", WmsTO.getSuppPlantDesc());

                map.put("resplantCode", WmsTO.getRecPlantCode());

                map.put("recPlantDesc", WmsTO.getRecPlantDesc());

                map.put("grQty", WmsTO.getGrQty());

                map.put("storageLoc", WmsTO.getStorageLocation());

                map.put("purchaseOrderNo", WmsTO.getPurchaseOrderNo());

                map.put("deliveryNo", WmsTO.getDeliveryNumber());

                map.put("movementType", WmsTO.getMovementType());

                map.put("create", WmsTO.getCreatedBy());

                map.put("cancelledno", WmsTO.getCancelledNo());

                map.put("materialCode", WmsTO.getMaterialCode());

                map.put("materialdes", WmsTO.getMaterialDescription());

                map.put("invoiceNumber", WmsTO.getInvoiceNo());

                map.put("invoiceDate", WmsTO.getInvoiceDate());

                map.put("status", WmsTO.getStatus());

                map.put("grnId", grnNumbercheck);

                map.put("grnCode", grnCode);

                System.out.println("WmsTO.getMaterialCode()==="
                        + WmsTO.getMaterialCode());

                int status = Integer.parseInt(WmsTO.getStatus());

                System.out.println("map = " + map);

                if (status == 0) {
                    int productId = (Integer) getSqlMapClientTemplate().queryForObject("wms.getproductId", map);
                    map.put("productId", productId);
                    insertStatus = (Integer) getSqlMapClientTemplate().insert("wms.connectinsertGrn", map);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getgrnView(int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getgrnView = new ArrayList();
        try {
            map.put("userId", userId);
            getgrnView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getgrnView", map);
            System.out.println("getConnectGrnReport---" + getgrnView.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ProductWarehouseMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "ProductWarehouseMasterList", sqlException);
        }
        return getgrnView;
    }

    public ArrayList getConnectGrnReport(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getgrnView = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            getgrnView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectGrnReport", map);
            System.out.println("getConnectGrnReport---" + getgrnView.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ProductWarehouseMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "ProductWarehouseMasterList", sqlException);
        }
        return getgrnView;
    }

    public ArrayList getRpOrderDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getRpOrderDetails = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("orderId", wms.getOrderId());
            getRpOrderDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getRpOrderDetails", map);
            System.out.println("getRpOrderDetails---" + getRpOrderDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ProductWarehouseMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "ProductWarehouseMasterList", sqlException);
        }
        return getRpOrderDetails;
    }

    public ArrayList getRpOrderMaster(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getRpOrderDetails = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("status", wms.getStatus());
            getRpOrderDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getRpOrderMaster", map);
            System.out.println("getRpOrderDetails---" + getRpOrderDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ProductWarehouseMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "ProductWarehouseMasterList", sqlException);
        }
        return getRpOrderDetails;
    }

    public ArrayList rpTripPlanningDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getRpOrderDetails = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            String[] orderId = wms.getOrderIdE();
            String[] selected = wms.getSelectedIndex();
            List orderIds = new ArrayList(orderId.length);
            for (int i = 0; i < orderId.length; i++) {
                if ("1".equals(selected[i])) {
                    orderIds.add(orderId[i]);
                }
            }
            map.put("orderId", orderIds);
            getRpOrderDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.rpTripPlanningDetails", map);
            System.out.println("getRpOrderDetails---" + getRpOrderDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ProductWarehouseMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "ProductWarehouseMasterList", sqlException);
        }
        return getRpOrderDetails;
    }

    public int saveGrnView(WmsTO WmsTO, int userId) {
        Map map = new HashMap();
        int saveGrnView = 0;
        int user = userId;
        try {
            map.put("userId", user);
            map.put("cancelledNumber", WmsTO.getCancelledNumber());
            map.put("deliveryNo", WmsTO.getDeliveryNo());
            map.put("purchaseOrderNo", WmsTO.getPurchaseOrderNo());
            map.put("storageLocation", WmsTO.getStorageLocation());
            map.put("storageLocation", WmsTO.getStorageLocation());
            map.put("recplantDes", WmsTO.getRecplantDes());
            map.put("recplantCode", WmsTO.getRecplantCode());
            map.put("supPlantDes", WmsTO.getSupPlantDes());
            map.put("supPlantCode", WmsTO.getSupPlantCode());
            map.put("giQuantity", WmsTO.getGiQuantity());
            map.put("materialDes", WmsTO.getMaterialDes());
            map.put("materialCode", WmsTO.getMaterialCode());
            map.put("vehicleNO", WmsTO.getVehicleNO());
            map.put("gateInwardNo", WmsTO.getGateInwardNo());
            map.put("grnID", WmsTO.getGrnID());

            System.out.println("get grn  Value map=====" + map);
            System.out.println("saveProductMasters=====" + saveGrnView);
            if (WmsTO.getGrnID() != null && WmsTO.getGrnID() != "") {
                saveGrnView = (Integer) getSqlMapClientTemplate().update("wms.updategrnView", map);
                System.out.println("updategrnView====" + saveGrnView);
                System.out.println("if in");
            } else {

                int grnNumbercheck = (Integer) getSqlMapClientTemplate().queryForObject("wms.getGrnNumber", map);

                String grnSequence = "" + grnNumbercheck;

                if (grnSequence == null) {
                    grnSequence = "00001";
                } else if (grnSequence.length() == 1) {
                    grnSequence = "0000" + grnSequence;
                } else if (grnSequence.length() == 2) {
                    grnSequence = "000" + grnSequence;
                } else if (grnSequence.length() == 3) {
                    grnSequence = "00" + grnSequence;
                } else if (grnSequence.length() == 4) {
                    grnSequence = "0" + grnSequence;
                } else if (grnSequence.length() == 5) {
                    grnSequence = "" + grnSequence;
                }
                Date date = new Date();
                String str = new SimpleDateFormat("yyyy-MM-dd").format(date);
                String[] date1 = str.split("-");

                String grnCode = "GRN/" + date1[0] + "/" + grnSequence;
                map.put("grnCode", grnCode);

                saveGrnView = (Integer) getSqlMapClientTemplate().insert("wms.savegrnView", map);
                System.out.println("savegrnView====" + saveGrnView);
                System.out.println("if out");
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveproductWarehouseMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveproductWarehouseMaster ", sqlException);
        }
        return saveGrnView;
    }

    public ArrayList getRackList(String locationId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getgrnView = new ArrayList();
        try {
            map.put("locationId", locationId);
            getgrnView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getRackList", map);
            System.out.println("getproductWarehouseMaster---" + getgrnView.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ProductWarehouseMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "ProductWarehouseMasterList", sqlException);
        }
        return getgrnView;
    }

    public ArrayList getbinList(String binId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getgrnView = new ArrayList();
        try {
            map.put("binId", binId);
            getgrnView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getBinupdateList", map);
            System.out.println("getproductWarehouseMaster---" + getgrnView.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ProductWarehouseMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "ProductWarehouseMasterList", sqlException);
        }
        return getgrnView;
    }

    public int editconnectSerialNumber(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertSerialNumber = 0;
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("rackid", wmsTO.getRackId());
        map.put("storageid", wmsTO.getLocationId());
        map.put("binid", wmsTO.getBinIdTemp1());

        map.put("serialId", wmsTO.getSerialId());

        map.put("grnId", wmsTO.getGrnId());

        try {

            System.out.println("map map map = " + map);
            int uom;
            int procond;
            if (wmsTO.getProCons().equals("Good")) {
                procond = 1;

            } else if (wmsTO.getProCons().equals("Damaged")) {
                procond = 2;

            } else {
                procond = 3;

            }
            if (wmsTO.getUoms().equals("Pack")) {
                uom = 1;
            } else {
                uom = 2;
            }

            map.put("uom", uom);
            map.put("proCond", procond);

            System.out.println("map===" + map);

            insertSerialNumber = (Integer) getSqlMapClientTemplate().update("wms.updateConnectSerial", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadNormalOrder", sqlException);
        }
        return insertSerialNumber;
    }

    public ArrayList getConnectsubmitSerialNumber(int userId) {
        Map map = new HashMap();
        ArrayList serialNumber = new ArrayList();
        try {
            map.put("userId", userId);
            System.out.println("userId==========" + userId);
            serialNumber = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectsubmitSerialNumber", map);
            System.out.println("getReceivedGateIn.size()-----" + serialNumber.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return serialNumber;

    }

    public ArrayList getConnectPickList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getConnectPickList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("dispatchId", wms.getDispatchId());
            getConnectPickList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectPickList", map);
            System.out.println("getConnectPickList.size()-----" + getConnectPickList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getConnectPickList;

    }

    public ArrayList getConnectSerialList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getConnectSerialList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("dispatchId", wms.getDispatchId());
            map.put("productId", wms.getProductId());
            map.put("qty", wms.getQty());
            System.out.println("map  +   " + map);
            getConnectSerialList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectSerialList", map);
            System.out.println("getConnectSerialList.size()-----" + getConnectSerialList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getConnectSerialList;

    }

    public ArrayList getConnectDispatchViewDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getConnectDispatchViewDetails = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("dispatchId", wms.getDispatchId());
            System.out.println("map  +   " + map);
            getConnectDispatchViewDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectDispatchViewDetails", map);
            System.out.println("getConnectDispatchViewDetails.size()-----" + getConnectDispatchViewDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getConnectDispatchViewDetails;
    }

    public int insertConnectPicklistMaster(WmsTO wms) {
        Map map = new HashMap();
        int insertConnectPicklistMaster = 0;
        try {

            String serial[] = wms.getSerialNos();
            String itemId[] = wms.getItemIds();
            String stockId[] = wms.getStockIds();

            for (int i = 0; i < serial.length; i++) {

                map.put("itemId", itemId[i]);
                map.put("serialNo", serial[i]);
                map.put("stockId", stockId[i]);
                map.put("whId", wms.getWhId());
                map.put("dispatchId", wms.getDispatchId());
                System.out.println("map  +   " + map);
                insertConnectPicklistMaster = (Integer) getSqlMapClientTemplate().insert("wms.insertConnectPicklistMaster", map);
                System.out.println("insertConnectPicklistMaster.size()-----" + insertConnectPicklistMaster);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return insertConnectPicklistMaster;

    }

    public int insertSerialNumber(WmsTO WmsTO, int userId, String whId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int lrNo = 0;
        int checkInvoice = 0;
        int pinWhCheck = 0;
        ArrayList getconnectInvoice = new ArrayList();
        WmsTO wmsTO = new WmsTO();
        try {
            System.out.println("--1111--");
            map.put("userId", userId);
            map.put("whId", whId);
            map.put("grnId", WmsTO.getGrnId1());
            map.put("productId", WmsTO.getProductID());
            map.put("remarks", WmsTO.getRemarks());
            System.out.println("map====" + map);

            ArrayList getConnectSerialNumberTemp = new ArrayList();
            getConnectSerialNumberTemp = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectSerialNumberTemp", map);
            System.out.println("getconnectInvoice---" + getConnectSerialNumberTemp.size());
            int stockQty = (Integer) getSqlMapClientTemplate().queryForObject("wms.getConnectSerialNumberQty", map);
            map.put("qtyAdd", stockQty);
            int checkconnectStockId = 0;
            int checkconnectStockIdcount = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkconnectStockId", map);

            if (checkconnectStockIdcount > 0) {
                checkconnectStockId = (Integer) getSqlMapClientTemplate().queryForObject("wms.getconnectStockId", map);
                map.put("stockId", checkconnectStockId);
                int upadteStockqty = (Integer) getSqlMapClientTemplate().update("wms.updateConnectStock", map);

            } else {
                checkconnectStockId = (Integer) getSqlMapClientTemplate().insert("wms.insertConnectStock", map);
            }
            map.put("stockId", checkconnectStockId);
            Iterator itr = getConnectSerialNumberTemp.iterator();
            while (itr.hasNext()) {
                WmsTO = (WmsTO) itr.next();

                map.put("productId", WmsTO.getProductID());
                map.put("serialNumber", WmsTO.getSerialNumber());
                map.put("locationIdE", WmsTO.getLocationId());

                map.put("rackIdE", WmsTO.getRackId());

                map.put("binId", WmsTO.getBinIdTemp());

                map.put("Qty", WmsTO.getQty());
                map.put("ipQty", WmsTO.getInpQty());

                map.put("uom", WmsTO.getUom1());
                map.put("proCond", WmsTO.getProCons());

                System.out.println("map = " + map);

                insertStatus = (Integer) getSqlMapClientTemplate().insert("wms.saveSubmitserial", map);

            }
            map.put("grnStatus", "2");
            int updategrnStatus = (Integer) getSqlMapClientTemplate().update("wms.updateGrnStatus", map);
            WmsTO wms1 = new WmsTO();
            String grnNo = (String) getSqlMapClientTemplate().queryForObject("wms.getConnectGrnNo", map);
            String mailTemplate = "";
            String grnDate = "";
            String gateInDate = "";
            String billNo = "";
            String billDate = "";
            String vehicleNo = "";
            String vendorName = "";
            String sapRef = "";
            String getHubMailId = "";
            getHubMailId = (String) getSqlMapClientTemplate().queryForObject("wms.getHubMailId", map);
            String sendMail = "sivanesap@entitlesolutions.com,sasikannan@hssupplychain.com";
            if (getHubMailId != null && !"".equals(getHubMailId) && !"s@gmail.com".equals(getHubMailId)) {
                sendMail = sendMail + "," + getHubMailId;
            }
            String driverName = "";
            mailTemplate = getMailTemplate("1", null);
            map.put("grnNo", grnNo);
            int grnNonUpdatedCount = (Integer) getSqlMapClientTemplate().queryForObject("wms.getGrnNonUpdatedCount", map);
            System.out.println("grnNonUpdated Count  " + grnNonUpdatedCount);
            if (grnNonUpdatedCount == 0) {
                ArrayList connectGrnMailDet = new ArrayList();
                connectGrnMailDet = (ArrayList) getSqlMapClientTemplate().queryForList("wms.connectGrnMailDet", map);
                System.out.println("connectGrnMailDet " + connectGrnMailDet.size());
                Iterator itr1 = connectGrnMailDet.iterator();
                while (itr1.hasNext()) {
                    wms1 = (WmsTO) itr1.next();
                    grnDate = wms1.getGrnDate();
                    gateInDate = wms1.getGateInwardDate();
                    billNo = wms1.getBillNo();
                    billDate = wms1.getBillDate();
                    vehicleNo = wms1.getVehicleNo();
                    vendorName = wms1.getVendorName();
                    driverName = wms1.getDriverName();
                    sapRef = wms1.getSapRefNo();
                    mailTemplate = mailTemplate.replaceAll("grnNo", wms1.getGrnNo());
                    mailTemplate = mailTemplate.replaceAll("grnDate", grnDate);
                    mailTemplate = mailTemplate.replaceAll("gateInTime", gateInDate);
                    mailTemplate = mailTemplate.replaceAll("refInvoice", billNo);
                    mailTemplate = mailTemplate.replaceAll("referenceInvoiceDate", billDate);
                    mailTemplate = mailTemplate.replaceAll("sapGrn", sapRef);
                    mailTemplate = mailTemplate.replaceAll("vehicleNo", vehicleNo);
                    mailTemplate = mailTemplate.replaceAll("driverName", driverName);
                    mailTemplate = mailTemplate.replaceAll("vendorName", vendorName);
                    String rows = "";
                    ArrayList connectGrnMailDetails = new ArrayList();
                    connectGrnMailDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.connectGrnMailDetails", map);
                    System.out.println("connectGrnMailDetails " + connectGrnMailDetails.size());
                    Iterator itr2 = connectGrnMailDetails.iterator();
                    WmsTO wms2 = new WmsTO();
                    int count1 = 1;
                    while (itr2.hasNext()) {
                        wms2 = (WmsTO) itr2.next();
                        rows = rows + "<tr align='center'><td>" + count1 + "</td><td>"
                                + wms2.getMaterialCode() + "</td><td>"
                                + wms2.getMaterialDescription() + "</td><td>"
                                + wms2.getCategoryName() + "</td><td>"
                                + wms2.getGiQuantity() + "</td><td>"
                                + wms2.getGrQty() + "</td><td>"
                                + wms2.getQty() + "</td><td>"
                                + wms2.getDamaged() + "</td><td>"
                                + (Integer.parseInt(wms2.getDamaged()) + Integer.parseInt(wms2.getQty()))
                                + "</td></tr>";
                        count1++;
                    }
                    mailTemplate = mailTemplate.replaceAll("newRows", rows);

                    String subject = "GRN Post Report";
                    String content = mailTemplate;
                    map.put("mailTypeId", "2");
                    map.put("mailSubjectTo", subject);
                    map.put("mailSubjectCc", subject);
                    map.put("mailSubjectBcc", subject);
                    map.put("mailContentTo", content);
                    map.put("mailContentCc", content);
                    map.put("mailContentBcc", content);
                    map.put("mailTo", sendMail);
                    map.put("mailCc", "");
                    map.put("mailBcc", "");
                    map.put("userId", userId);
                    System.out.println("map = " + map);

                    int advancemail = (Integer) getSqlMapClientTemplate().insert("wms.insertMailDetails", map);
                    System.out.println("advancemail = " + advancemail);
                    if (advancemail > 0) {
                        int updateMailStatusInGRN = (Integer) getSqlMapClientTemplate().update("wms.updateConnectGrnMailStatus", map);
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public String getMailTemplate(String eventId, SqlMapClient session) {
        Map map = new HashMap();
        String template = "";
        map.put("eventId", eventId);
        try {
            System.out.println("eventId  " + eventId);
            template = (String) getSqlMapClientTemplate().queryForObject("wms.getMailTemplate", map);
            System.out.println("template  " + template);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSMSTemplate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSMSTemplate", sqlException);
        }

        return template;
    }

    public ArrayList getVehicleTatList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getVehicleTatList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("tatType", wms.getTatType());
            System.out.println("map    " + map);
            getVehicleTatList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getVehicleTatList", map);
            System.out.println("getIFBList========" + getVehicleTatList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getVehicleTatList;

    }

    public ArrayList getSerialNumber(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getdispatch = new ArrayList();

        try {

            getdispatch = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSerialList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getdispatch;

    }

    public int updateSerialNumber(WmsTO WmsTO) {
        Map map = new HashMap();
        int insertStatus = 0;

        WmsTO wmsTO = new WmsTO();
        try {
            System.out.println("--1111--");

            String[] serialIde = WmsTO.getSerialNos();
            map.put("customerId", WmsTO.getCustomerId());
            map.put("invoiceId", WmsTO.getInvoiceId());
            System.out.println("map====dao" + map);

            for (int i = 0; i < serialIde.length; i++) {
                map.put("serialId", serialIde[i]);
                System.out.println("map====dao" + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("wms.updateInvoice", map);

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getItemQty(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getItemQty = new ArrayList();

        try {

            getItemQty = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getItemQty", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getItemQty;

    }

    public ArrayList getConnectPickedSerial(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getConnectPickedSerial = new ArrayList();

        try {
            map.put("dispatchId", wms.getDispatchId());
            map.put("productId", wms.getProductId());
            getConnectPickedSerial = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectPickedSerial", map);
            System.out.println("getConnectPickedSerial=== " + getConnectPickedSerial.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getConnectPickedSerial;

    }

    public ArrayList transferTypeList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList transferTypeList = new ArrayList();

        try {
            transferTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.transferTypeList", map);
            System.out.println("transferTypeList=== " + transferTypeList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return transferTypeList;

    }

    public ArrayList flurencoPickslipList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList flurencoPickslipList = new ArrayList();

        try {
            map.put("userId", wms.getUserId());
            map.put("shipmentType", wms.getShipmentType());
            if (wms.getFromDate() != null && wms.getFromDate() != "") {
                if (wms.getFromDate().split("-")[2].length() != 2) {
                    map.put("fromDate", wms.getFromDate().split("-")[2] + "-" + wms.getFromDate().split("-")[1] + "-" + wms.getFromDate().split("-")[0]);
                    map.put("toDate", wms.getToDate().split("-")[2] + "-" + wms.getToDate().split("-")[1] + "-" + wms.getToDate().split("-")[0]);
                } else {
                    map.put("fromDate", wms.getFromDate());
                    map.put("toDate", wms.getToDate());
                }
            }
            flurencoPickslipList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.flurencoPickslipList", map);
            System.out.println("flurencoPickslipList=== " + flurencoPickslipList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return flurencoPickslipList;

    }

    public ArrayList flurencoPickslipDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList flurencoPickslipDetails = new ArrayList();

        try {
            map.put("userId", wms.getUserId());

            String[] selectedIndex = wms.getSelectedIndex();
            String[] orderIds = wms.getOrderIdE();
            List orders = new ArrayList(orderIds.length);
            for (int i = 0; i < orderIds.length; i++) {
                if ("1".equals(selectedIndex[i])) {
                    orders.add(orderIds[i]);
                }
            }
            map.put("orders", orders);
            System.out.println("map  " + map);
            flurencoPickslipDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.flurencoPickslipDetails", map);
            System.out.println("flurencoPickslipDetails=== " + flurencoPickslipDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return flurencoPickslipDetails;

    }

    public int updatePicklist(WmsTO WmsTO) {
        Map map = new HashMap();
        int insertStatus = 0;

        WmsTO wmsTO = new WmsTO();
        try {
            System.out.println("--1111--");

            String[] serialIde = WmsTO.getSerialIds();
            String[] customerId = WmsTO.getCustomerIdE();
            String[] invoiceId = WmsTO.getInvoiceIds();
            System.out.println("map====dao" + map);

            for (int i = 0; i < serialIde.length; i++) {
                map.put("serialId", serialIde[i]);
                map.put("customerId", customerId[i]);
                map.put("invoiceId", invoiceId[i]);

                System.out.println("map====dao" + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("wms.updateInvoice", map);

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public int clearConnectPicklist(WmsTO wms) {
        Map map = new HashMap();
        int status = 0;

        try {
            map.put("dispatchId", wms.getDispatchId());
            System.out.println("map====dao" + map);
            status = (Integer) getSqlMapClientTemplate().delete("wms.clearConnectPicklist", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return status;
    }

    public ArrayList getDispatchList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getDispatchList = new ArrayList();
        try {

            getDispatchList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDispatchList", map);
            System.out.println("getDispatchList---" + getDispatchList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getDispatchList;
    }

    public ArrayList getSubLrList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getSubLrList = new ArrayList();
        try {
            map.put("dipatchId", wms.getDispatchId());

            getSubLrList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSubLrDetails", map);
            System.out.println("getDispatchList---" + getSubLrList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getSubLrList;
    }

    public int saveConnectPodFile(String podRemarks, String dispatchId, String file, String fileName, String subLr, String subLrId) {
        Map map = new HashMap();
        int saveConnectPodFile = 0;
        try {
            map.put("depositId", dispatchId);
            map.put("filePath", file);
            map.put("podRemarks", podRemarks);
            map.put("fileName", fileName);
            map.put("subLr", subLr);
            map.put("subLrId", subLrId);
            String dealerId = (String) getSqlMapClientTemplate().queryForObject("wms.getConnectDealerId", map);

            map.put("dealerId", dealerId);
            saveConnectPodFile = (Integer) getSqlMapClientTemplate().update("wms.saveConnectPodFile", map);
            System.out.println("saveConnectPodFile---" + saveConnectPodFile);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFkDepositFile", sqlException);
        }
        return saveConnectPodFile;
    }

    public ArrayList getPodList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getPodList = new ArrayList();
        try {
            map.put("roleId", wms.getRoleId());
            map.put("userId", wms.getUserId());
            map.put("toDate", wms.getToDate());
            map.put("viewStatus", wms.getStatusName());
            map.put("fromDate", wms.getFromDate());
            map.put("whId", wms.getWhId());
            if (!"".equals(wms.getInvoiceNo()) && wms.getInvoiceNo() != null) {
                map.put("invoiceNo", "%" + wms.getInvoiceNo() + "%");
            }
            map.put("status", wms.getStatus());
            System.out.println("connectPOD List   " + map);
            getPodList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getPodList", map);
            System.out.println("getPodList---" + getPodList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getPodList;
    }

    public ArrayList getimPodList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getPodList = new ArrayList();
        try {
            map.put("roleId", wms.getRoleId());
            map.put("userId", wms.getUserId());
            map.put("toDate", wms.getToDate());
            map.put("fromDate", wms.getFromDate());
            map.put("viewStatus", wms.getStatusName());
            map.put("whId", wms.getWhId());
            map.put("status", wms.getStatus());
            System.out.println("connectPOD List----------------------------" + map);
            getPodList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getimPodList", map);
            System.out.println("getPodList---" + getPodList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getPodList;
    }
    
    public ArrayList viewConnectStockDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList viewConnectStockDetails = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            viewConnectStockDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.viewConnectStockDetails", map);
            System.out.println("viewConnectStockDetails---" + viewConnectStockDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return viewConnectStockDetails;
    }

    public ArrayList viewConnectSerialNumberDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList viewConnectSerialNumberDetails = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("stockId", wms.getStockId());
            viewConnectSerialNumberDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.viewConnectSerialNumberDetails", map);
            System.out.println("viewConnectSerialNumberDetails---" + viewConnectSerialNumberDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return viewConnectSerialNumberDetails;
    }

    public int checkConnectSerialNoExist(WmsTO wms) {
        Map map = new HashMap();
        int checkConnectSerialNoExist = 0;
        try {
            map.put("productId", wms.getProductId());
            map.put("whId", wms.getWhId());
            map.put("serialNo", wms.getSerialNo());
            checkConnectSerialNoExist = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkConnectSerialNoExist", map);
            System.out.println("checkConnectSerialNoExist---" + checkConnectSerialNoExist);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return checkConnectSerialNoExist;
    }

    public int checkConnectStockDetail(WmsTO wms) {
        Map map = new HashMap();
        int checkConnectStockDetail = 0;
        int getConnectStockDetail = 0;
        try {
            map.put("productId", wms.getProductId());
            map.put("whId", wms.getWhId());
            map.put("serialNo", wms.getSerialNo());
            System.out.println("map   ++ " + map);
            checkConnectStockDetail = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkConnectStockDetail", map);
            if (checkConnectStockDetail > 0) {
                getConnectStockDetail = (Integer) getSqlMapClientTemplate().queryForObject("wms.getConnectStockDetail", map);
            } else {
                getConnectStockDetail = 0;
            }
            System.out.println("checkConnectStockDetail---" + checkConnectStockDetail);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getConnectStockDetail;
    }

    public ArrayList checkConnectSerialNo(WmsTO wms) {
        Map map = new HashMap();
        ArrayList checkConnectSerialNo = new ArrayList();
        try {
            map.put("inpQty", wms.getInpQty());
            map.put("productId", wms.getProductId());
            map.put("invoiceId", wms.getInvoiceId());
            map.put("whId", wms.getWhId());
            map.put("serialNo", "%" + wms.getSerialNo() + "%");
            System.out.println("map   " + map);
            checkConnectSerialNo = (ArrayList) getSqlMapClientTemplate().queryForList("wms.checkConnectSerialNo", map);
            System.out.println("checkConnectSerialNo---" + checkConnectSerialNo.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return checkConnectSerialNo;
    }

    public ArrayList getConnectSerialNoDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getConnectSerialNoDetails = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("serialNo", wms.getSerialNo());
            getConnectSerialNoDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectSerialNoDetails", map);
            System.out.println("getConnectSerialNoDetails---" + getConnectSerialNoDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getConnectSerialNoDetails;
    }

    public int updateConnectPodApproval(String podId, String status, WmsTO wms) {
        Map map = new HashMap();
        int saveConnectPodFile = 0;
        try {
            map.put("podId", podId);
            map.put("status", status);
            map.put("userId", wms.getUserId());

            saveConnectPodFile = (Integer) getSqlMapClientTemplate().update("wms.updatePodStatus", map);
            System.out.println("saveConnectPodFile---" + saveConnectPodFile);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFkDepositFile", sqlException);
        }
        return saveConnectPodFile;
    }

    public int updateConnectPicklist(WmsTO wms) {
        Map map = new HashMap();
        int updateConnectPicklist = 0;
        try {
            map.put("dispatchId", wms.getDispatchId());
            updateConnectPicklist = (Integer) getSqlMapClientTemplate().update("wms.updateConnectPicklist", map);
            System.out.println("updateConnectPicklist---" + updateConnectPicklist);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFkDepositFile", sqlException);
        }
        return updateConnectPicklist;
    }

    public int deleteConnectDispatchInvoice(WmsTO wms) {
        Map map = new HashMap();
        int deleteConnectDispatchInvoice = 0;
        try {
            map.put("dispatchId", wms.getDispatchId());
            map.put("invoiceNo", wms.getInvoiceNo());
            deleteConnectDispatchInvoice = (Integer) getSqlMapClientTemplate().delete("wms.deleteConnectDispatchInvoice", map);
            deleteConnectDispatchInvoice = (Integer) getSqlMapClientTemplate().update("wms.updateConnectDispatchInvoiceStatus", map);
            System.out.println("deleteConnectDispatchInvoice---" + deleteConnectDispatchInvoice);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFkDepositFile", sqlException);
        }
        return deleteConnectDispatchInvoice;
    }

    public int updateFlurencoPickUpDetails(WmsTO wms) {
        Map map = new HashMap();
        int updateFlurencoPickUpDetails = 0;
        try {
            map.put("vehicleNo", wms.getVehicleNo());
            map.put("driverName", wms.getDriverName());
            map.put("driverMobile", wms.getMobile());
            map.put("pickupDate", wms.getPickupDate());
            map.put("pickupTime", wms.getPickupTime());
            updateFlurencoPickUpDetails = (Integer) getSqlMapClientTemplate().update("wms.updateFlurencoPickUpDetails", map);
            System.out.println("updateFlurencoPickUpDetails---" + updateFlurencoPickUpDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFkDepositFile", sqlException);
        }
        return updateFlurencoPickUpDetails;
    }

    public int connectOpenPackSerialSave(WmsTO wms) {
        Map map = new HashMap();
        int connectOpenPackSerialSave = 0;
        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("productId", wms.getProductId());
            map.put("grnId", wms.getGrnId());
            map.put("stockId", wms.getStockId());
            String[] serialNos = wms.getSerialNos();
            String[] uoms = wms.getUom();
            String[] proCond = wms.getProCond();
            String[] binIds = wms.getBinIds();
            String[] rackIds = wms.getRackIdE();
            String[] ipQty = wms.getIpQty();
            String[] storageIds = wms.getStorageIds();
            for (int i = 0; i < serialNos.length; i++) {
                map.put("serialNo", serialNos[i]);
                map.put("storageId", storageIds[i]);
                map.put("rackId", rackIds[i]);
                map.put("binId", binIds[i]);
                map.put("ipQty", ipQty[i]);
                if ("Good".equalsIgnoreCase(proCond[i])) {
                    map.put("proCond", "1");
                } else if ("Damaged".equalsIgnoreCase(proCond[i])) {
                    map.put("proCond", "2");
                } else {
                    map.put("proCond", "3");
                }
                if ("Pack".equalsIgnoreCase(uoms[i])) {
                    map.put("uom", "1");
                } else {
                    map.put("uom", "2");
                }
                connectOpenPackSerialSave = (Integer) getSqlMapClientTemplate().update("wms.connectOpenPackSerialSave", map);
            }
            map.put("usedStatus", "2");
            map.put("serialId", wms.getSerialId());
            int updateConnectSerialStatus = (Integer) getSqlMapClientTemplate().update("wms.updateConnectSerialStatus", map);
            System.out.println("connectOpenPackSerialSave---" + connectOpenPackSerialSave);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFkDepositFile", sqlException);
        }
        return connectOpenPackSerialSave;
    }

    public int insertConnectPicklistDetails(WmsTO wms) {
        Map map = new HashMap();
        int insertConnectPicklistDetails = 0;
        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("productId", wms.getProductId());
            map.put("stockId", wms.getStockId());
            map.put("serialId", wms.getSerialId());
            map.put("serialNo", wms.getSerialNo());
            map.put("dispatchId", wms.getDispatchId());
            map.put("custId", wms.getCustId());
            map.put("qty", wms.getQty());
            map.put("invoiceId", wms.getInvoiceId());
            map.put("invoiceDetailId", wms.getOrderDetailId());
            map.put("invoiceNo", wms.getInvoiceNo());
            insertConnectPicklistDetails = (Integer) getSqlMapClientTemplate().insert("wms.insertConnectPicklistDetails", map);
            System.out.println("insertConnectPicklistDetails---" + insertConnectPicklistDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFkDepositFile", sqlException);
        }
        return insertConnectPicklistDetails;
    }

    public ArrayList dcFreightDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList dcFreightDetails = new ArrayList();

        try {
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("whId", wms.getWhId());
            if (wms.getStatusType() == null) {
                map.put("statusType", "");
            } else {
                map.put("statusType", wms.getStatusType());
            }
            System.out.println("map  " + map);
            dcFreightDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.dcFreightDetails", map);
            System.out.println("dcFreightDetails=== " + dcFreightDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "dcFreightDetails", sqlException);
        }
        return dcFreightDetails;

    }

    public ArrayList docketNoUpdateDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList docketNoUpdateDetails = new ArrayList();

        try {
            map.put("roleId", wms.getRoleId());
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("whId", wms.getWhId());
            if (wms.getStatusType() == null) {
                map.put("statusType", "");
            } else {
                map.put("statusType", wms.getStatusType());
            }
            System.out.println("map  " + map);
            docketNoUpdateDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.docketNoUpdateDetails", map);
            System.out.println("docketNoUpdateDetails=== " + docketNoUpdateDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "docketNoUpdateDetails", sqlException);
        }
        return docketNoUpdateDetails;

    }

    public ArrayList connectLrView(WmsTO wms) {
        Map map = new HashMap();
        ArrayList connectLrView = new ArrayList();

        try {
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            connectLrView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.connectLrView", map);
            System.out.println("connectLrView=== " + connectLrView.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "docketNoUpdateDetails", sqlException);
        }
        return connectLrView;

    }

    public ArrayList podUploadDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList podUploadDetails = new ArrayList();

        try {
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            if (wms.getStatusType() == null) {
                map.put("statusType", "0");
            } else {
                map.put("statusType", wms.getStatusType());
            }
            if (wms.getWarehouseId() != null && "".equals(wms.getWarehouseId())) {
                map.put("whId", wms.getWarehouseId());
            } else {
                map.put("whId", wms.getWhId());
            }
            System.out.println("map  " + map);
            podUploadDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.podUploadDetails", map);
            System.out.println("podUploadDetails=== " + podUploadDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return podUploadDetails;

    }

    public int updateConnectLrNo(WmsTO wms) {
        Map map = new HashMap();
        int updateConnectLrNo = 0;
        try {
            map.put("whId", wms.getWhId());
            map.put("subLr", wms.getLrnNo());
            map.put("deliveryDate", wms.getDeliveryDate());
            updateConnectLrNo = (Integer) getSqlMapClientTemplate().update("wms.updateConnectLrNo", map);
            String getConnectInvoiceIds = (String) getSqlMapClientTemplate().queryForObject("wms.getConnectInvoiceIds", map);
            System.out.println("map in Lr no update " + map);
            String[] invoiceNos = getConnectInvoiceIds.split("~");
            if (updateConnectLrNo > 0) {
                for (int i = 0; i < invoiceNos.length; i++) {
                    map.put("invoiceNo", invoiceNos[i]);
                    int updateConnectInvoiceDetailsStatus = (Integer) getSqlMapClientTemplate().update("wms.updateConnectInvoiceDetailsStatus", map);
                }
            }
            System.out.println("updateConnectLrNo   " + updateConnectLrNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return updateConnectLrNo;

    }

    public int insertImMaterialMaster(WmsTO wms) {
        Map map = new HashMap();
        int insertImMaterialMaster = 0;
        try {
            map.put("whId", wms.getWhId());
            map.put("userId", wms.getUserId());
            map.put("materialName", wms.getMaterial());
            map.put("materialDescription", wms.getMaterialDescription());
            map.put("materialCode", wms.getMaterialCode());
            map.put("size", wms.getSize());
            map.put("unit", wms.getUnit().split("~")[0]);
            map.put("qty", wms.getQty());
            map.put("activeInd", wms.getActiveType());
            System.out.println("map  " + map);
            if (wms.getMaterialId() != null && !"".equals(wms.getMaterialId())) {
                map.put("materialId", wms.getMaterialId());
                insertImMaterialMaster = (Integer) getSqlMapClientTemplate().update("wms.updateImMaterialMaster", map);
            } else {
                insertImMaterialMaster = (Integer) getSqlMapClientTemplate().update("wms.insertImMaterialMaster", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return insertImMaterialMaster;

    }

    public int insertImMaterialCustomerMaster(WmsTO wms) {
        Map map = new HashMap();
        int insertImMaterialCustomerMaster = 0;
        try {
            map.put("whId", wms.getWhId());
            map.put("userId", wms.getUserId());
            map.put("customerName", wms.getCustName());
            map.put("customerCode", wms.getCustCode());
            map.put("phone", wms.getMobileNo());
            map.put("phone2", wms.getPhoneNumber());
            map.put("address", wms.getAddress());
            map.put("address1", wms.getAddress1());
            map.put("city", wms.getCity());
            map.put("gstNo", wms.getGstinNumber());
            map.put("state", wms.getStateId());
            map.put("activeInd", wms.getActiveType());
            map.put("pincode", wms.getPincode());
            System.out.println("map  " + map);
            if (wms.getCustId() != null && !"".equals(wms.getCustId())) {
                map.put("customerId", wms.getCustId());
                insertImMaterialCustomerMaster = (Integer) getSqlMapClientTemplate().update("wms.updateImMaterialCustomerMaster", map);
            } else {
                insertImMaterialCustomerMaster = (Integer) getSqlMapClientTemplate().update("wms.insertImMaterialCustomerMaster", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return insertImMaterialCustomerMaster;

    }

    public ArrayList getConnectSrnTemp(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getConnectSrn = new ArrayList();

        try {
            map.put("userId", wms.getUserId());
            System.out.println("map  " + map);
            getConnectSrn = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectSrnTemp", map);
            System.out.println("getConnectSrn=== " + getConnectSrn.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return getConnectSrn;

    }

    public ArrayList getConnectSrn(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getConnectSrn = new ArrayList();

        try {
            map.put("userId", wms.getUserId());
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            System.out.println("map  " + map);
            getConnectSrn = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectSrn", map);
            System.out.println("getConnectSrn=== " + getConnectSrn.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return getConnectSrn;

    }

    public ArrayList getConnectInvoiceReport(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getConnectInvoiceReport = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("userId", wms.getUserId());
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("status", wms.getStatus());
            System.out.println("map  " + map);
            getConnectInvoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectInvoiceReport", map);
            System.out.println("getConnectInvoiceReport=== " + getConnectInvoiceReport.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return getConnectInvoiceReport;

    }

    public ArrayList getConnectInvoiceProductDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getConnectInvoiceProductDetails = new ArrayList();

        try {
            map.put("invoiceId", wms.getInvoiceId());
            map.put("wh", wms.getWhId());
            System.out.println("map  " + map);
            getConnectInvoiceProductDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectInvoiceProductDetails", map);
            System.out.println("getConnectInvoiceProductDetails=== " + getConnectInvoiceProductDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return getConnectInvoiceProductDetails;

    }

    public ArrayList connectGetBillReceiving(WmsTO wms) {
        Map map = new HashMap();
        ArrayList connectGetBillReceiving = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("status", wms.getStatus());
            map.put("billId", wms.getBillId());
            System.out.println("map  " + map);
            connectGetBillReceiving = (ArrayList) getSqlMapClientTemplate().queryForList("wms.connectGetBillReceiving", map);
            System.out.println("connectGetBillReceiving=== " + connectGetBillReceiving.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return connectGetBillReceiving;

    }

    public ArrayList rpStockDetailsView(WmsTO wms) {
        Map map = new HashMap();
        ArrayList rpStockDetailsView = new ArrayList();

        try {
            map.put("stockId", wms.getStockId());
            rpStockDetailsView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.rpStockDetailsView", map);
            System.out.println("rpStockDetailsView=== " + rpStockDetailsView.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return rpStockDetailsView;

    }

    public ArrayList rpStockDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList rpStockDetails = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("indentId", wms.getIndentId());
            map.put("status", wms.getStatus());
            System.out.println("map  " + map);
            rpStockDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.rpStockDetails", map);
            System.out.println("rpStockDetails=== " + rpStockDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return rpStockDetails;

    }

    public ArrayList rpStockTransferView(WmsTO wms) {
        Map map = new HashMap();
        ArrayList rpStockTransferView = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("transferId", wms.getTransferId());
            rpStockTransferView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.rpStockTransferView", map);
            System.out.println("rpStockTransferView=== " + rpStockTransferView.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return rpStockTransferView;

    }

    public ArrayList rpStockTransfer(WmsTO wms) {
        Map map = new HashMap();
        ArrayList rpStockTransfer = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            rpStockTransfer = (ArrayList) getSqlMapClientTemplate().queryForList("wms.rpStockTransfer", map);
            System.out.println("rpStockTransfer=== " + rpStockTransfer.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return rpStockTransfer;

    }

    public ArrayList rpStockReceive(WmsTO wms) {
        Map map = new HashMap();
        ArrayList rpStockTransfer = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("transferId", wms.getTransferId());
            map.put("status", wms.getStatus());
            rpStockTransfer = (ArrayList) getSqlMapClientTemplate().queryForList("wms.rpStockReceive", map);
            System.out.println("rpStockReceiveConfirm=== " + rpStockTransfer.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return rpStockTransfer;

    }

    public ArrayList connectIndentRequest(WmsTO wms) {
        Map map = new HashMap();
        ArrayList connectIndentRequest = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("indentId", wms.getIndentId());
            map.put("status", wms.getStatus());
            System.out.println("map  " + map);
            connectIndentRequest = (ArrayList) getSqlMapClientTemplate().queryForList("wms.connectIndentRequest", map);
            System.out.println("connectIndentRequest=== " + connectIndentRequest.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return connectIndentRequest;

    }

    public ArrayList connectIndentRequestDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList connectIndentRequestDetails = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("indentId", wms.getIndentId());
            map.put("indentStatus", wms.getIndentStatus());
            map.put("type", wms.getType());
            System.out.println("map  " + map);
            connectIndentRequestDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.connectIndentRequestDetails", map);
            System.out.println("connectIndentRequestDetailswm=== " + connectIndentRequestDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return connectIndentRequestDetails;

    }

    public ArrayList getRpProductList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getRpProductList = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            System.out.println("map  " + map);
            getRpProductList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getRpProductList", map);
            System.out.println("getRpProductList=== " + getRpProductList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return getRpProductList;
    }

    public ArrayList imMaterialMasterDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList imMaterialMasterDetails = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("status", wms.getStatus());
            System.out.println("map  " + map);
            imMaterialMasterDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imMaterialMasterDetails", map);
            System.out.println("imMaterialMasterDetails=== " + imMaterialMasterDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return imMaterialMasterDetails;
    }

    public ArrayList imMaterialGrnMaster(WmsTO wms) {
        Map map = new HashMap();
        ArrayList imMaterialGrnMaster = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("status", wms.getStatus());
            System.out.println("map  " + map);
            imMaterialGrnMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imMaterialGrnMaster", map);
            System.out.println("imMaterialGrnMaster=== " + imMaterialGrnMaster.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return imMaterialGrnMaster;
    }

    public ArrayList imMaterialGrnDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList imMaterialGrnDetails = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("grnId", wms.getGrnId());
            System.out.println("map  " + map);
            imMaterialGrnDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imMaterialGrnDetails", map);
            System.out.println("imMaterialGrnDetails=== " + imMaterialGrnDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return imMaterialGrnDetails;
    }

    public ArrayList imMaterialCustomerMaster(WmsTO wms) {
        Map map = new HashMap();
        ArrayList imMaterialCustomerMaster = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            System.out.println("map  " + map);
            imMaterialCustomerMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imMaterialCustomerMaster", map);
            System.out.println("imMaterialCustomerMaster=== " + imMaterialCustomerMaster.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return imMaterialCustomerMaster;
    }

    public ArrayList imMaterialIndent(WmsTO wms) {
        Map map = new HashMap();
        ArrayList imMaterialIndent = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("whName", wms.getName());
            imMaterialIndent = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imMaterialIndent", map);
            System.out.println("imMaterialIndent=== " + imMaterialIndent.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return imMaterialIndent;
    }

    public ArrayList getImMaterialCustomerMaster(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImMaterialCustomerMaster = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            System.out.println("map  " + map);
            getImMaterialCustomerMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImMaterialCustomerMaster", map);
            System.out.println("getImMaterialCustomerMaster=== " + getImMaterialCustomerMaster.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return getImMaterialCustomerMaster;
    }

    public ArrayList getConnectInvoiceExcelTemplate() {
        Map map = new HashMap();
        ArrayList getConnectInvoiceExcelTemplate = new ArrayList();

        try {
            getConnectInvoiceExcelTemplate = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getConnectInvoiceExcelTemplate", map);
            System.out.println("getConnectInvoiceExcelTemplate=== " + getConnectInvoiceExcelTemplate.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return getConnectInvoiceExcelTemplate;
    }

    public ArrayList imMaterialPOView(WmsTO wms) {
        Map map = new HashMap();
        ArrayList imMaterialPOView = new ArrayList();

        try {
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("whId", wms.getWhId());
            map.put("userId", wms.getUserId());
            imMaterialPOView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imMaterialPOView", map);
            System.out.println(imMaterialPOView);
            for (int i = 0; i < imMaterialPOView.size(); i++) {
            }
            System.out.println("imMaterialPOView=== " + imMaterialPOView.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return imMaterialPOView;
    }

    public ArrayList getImMaterialPOViewDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList imMaterialPOView = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("poId", wms.getPoId());
            imMaterialPOView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImMaterialPOViewDetails", map);
            System.out.println(imMaterialPOView);
            for (int i = 0; i < imMaterialPOView.size(); i++) {
            }
            System.out.println("imMaterialPOView=== " + imMaterialPOView.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return imMaterialPOView;
    }

    public ArrayList getImPOMaterialList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImPOMaterialList = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("status", wms.getStatus());
            map.put("poId", wms.getPoId());
            getImPOMaterialList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImPOMaterialList", map);
            System.out.println(getImPOMaterialList);
            for (int i = 0; i < getImPOMaterialList.size(); i++) {
            }
            System.out.println("getImPOMaterialList=== " + getImPOMaterialList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return getImPOMaterialList;
    }

    public int updateConnectInvoiceUpdate(WmsTO wms) {
        Map map = new HashMap();
        int updateConnectInvoiceUpdate = 0;

        try {
            map.put("wh", wms.getWhId());
            map.put("userId", wms.getUserId());
            map.put("appointmentDate", wms.getAppointmentDate());
            map.put("invoiceId", wms.getInvoiceId());
            System.out.println("map  " + map);
            updateConnectInvoiceUpdate = (Integer) getSqlMapClientTemplate().update("wms.updateConnectInvoiceUpdate", map);
            System.out.println("updateConnectInvoiceUpdate=== " + updateConnectInvoiceUpdate);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return updateConnectInvoiceUpdate;
    }

    public int saveStockReceive(WmsTO wms, String[] detailId) {
        Map map = new HashMap();
        int saveStockReceive = 0;

        try {
            String stockId = "";
            map.put("whId", wms.getToWhId());
            map.put("userId", wms.getUserId());
            map.put("toWhId", wms.getToWhId());
            map.put("transferId", wms.getTransferId());
            for (int i = 0; i < wms.getSerialNos().length; i++) {
                map.put("serialNo", wms.getSerialNos()[i]);
                map.put("serialId", wms.getSerialIds()[i]);
                map.put("transferDetailId", detailId[i]);
                map.put("productId", wms.getItemIds()[i]);
                System.out.println("map  " + map);
                saveStockReceive = (Integer) getSqlMapClientTemplate().update("wms.saveStockReceiveDetail", map);
                int rpStockIdCount = (Integer) getSqlMapClientTemplate().queryForObject("wms.rpStockIdCount", map);
                if (rpStockIdCount > 0) {
                    stockId = rpStockIdCount + "";
                } else {
                    map.put("quantity", "0");
                    map.put("excess", "0");
                    map.put("damaged", "0");
                    int insertStockDetails = (Integer) getSqlMapClientTemplate().insert("wms.insertRpStockDetails", map);
                    stockId = insertStockDetails + "";
                }
                map.put("stockId", stockId);
                map.put("qty", "1");
                saveStockReceive = (Integer) getSqlMapClientTemplate().update("wms.saveStockReceiveSerial", map);
                saveStockReceive = (Integer) getSqlMapClientTemplate().update("wms.updateRpStockQty", map);
                System.out.println("saveStockReceive=== " + saveStockReceive);
            }
            if (saveStockReceive > 0) {
                map.put("status", "1");
                int status = (Integer) getSqlMapClientTemplate().update("wms.updateStockTransferStatus", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return saveStockReceive;
    }

    public int updateConnectIndentRequestDetails(WmsTO wms, String[] tatId, String[] indentDetailId, String[] inDate, String[] inTime, String[] selectedStatus) {
        Map map = new HashMap();
        int updateConnectIndentDetails = 0;

        try {
            map.put("whId", wms.getWhId());
            map.put("userId", wms.getUserId());
            map.put("indentId", wms.getIndentId());
            for (int i = 0; i < tatId.length; i++) {
                map.put("tatId", tatId[i]);
                map.put("indentDetailId", indentDetailId[i]);
                map.put("inDate", inDate[i]);
                map.put("inTime", inTime[i]);
                if ("1".equals(selectedStatus[i]) && !"".equals(inTime[i]) && !"".equals(inDate[i]) && inDate[i] != null && inTime[i] != null) {
                    System.out.println("map  " + map);
                    updateConnectIndentDetails = (Integer) getSqlMapClientTemplate().update("wms.updateConnectIndentDetails", map);
                    updateConnectIndentDetails = (Integer) getSqlMapClientTemplate().update("wms.updateConnectIndentInTime", map);
                    System.out.println("updateConnectIndentDetails=== " + updateConnectIndentDetails);
                }
            }
            int getConnectIndentUpdatedCount = (Integer) getSqlMapClientTemplate().queryForObject("wms.getConnectIndentUpdatedCount", map);
            if (getConnectIndentUpdatedCount == 0) {
                map.put("status", "3");
                int status = (Integer) getSqlMapClientTemplate().update("wms.updateConnectIndentStatus", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return updateConnectIndentDetails;

    }

    public int insertConnectIndentDetails(WmsTO wms) {
        Map map = new HashMap();
        int insertConnectIndentDetails = 0;

        try {
            map.put("wh", wms.getWhId());
            map.put("userId", wms.getUserId());
            map.put("indentId", wms.getIndentId());
            map.put("vehicleType", wms.getVehicleType());
            map.put("fromLocation", wms.getFromLocation());
            map.put("plant", wms.getPlantCode());
            for (int i = 0; i < wms.getVehicleNos().length; i++) {
                map.put("vehicleNo", wms.getVehicleNos()[i]);
                map.put("driverName", wms.getDriverNames()[i]);
                map.put("driverMobile", wms.getDriverMobileNos()[i]);
                map.put("tattype", "1");
                System.out.println("map  " + map);
                insertConnectIndentDetails = (Integer) getSqlMapClientTemplate().insert("wms.insertConnectIndentDetails", map);
                int tatId = (Integer) getSqlMapClientTemplate().insert("wms.insertConnectVehicleTat", map);
                map.put("indentDetailId", insertConnectIndentDetails);
                map.put("tatId", tatId);
                int updateConnectIndentTatId = (Integer) getSqlMapClientTemplate().update("wms.updateConnectIndentTatId", map);
                System.out.println("insertConnectIndentDetails=== " + insertConnectIndentDetails);
            }
            int getConnectIndentDetailStatus = (Integer) getSqlMapClientTemplate().queryForObject("wms.getConnectIndentDetailStatus", map);
            map.put("status", getConnectIndentDetailStatus);
            int updateConnectIndentStatus = (Integer) getSqlMapClientTemplate().update("wms.updateConnectIndentStatus", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return insertConnectIndentDetails;

    }

    public int updateConnectDeliveryDate(String[] lrnNo, String[] deliveryDate, String[] remarks, WmsTO wmsTO) {
        Map map = new HashMap();
        int updateConnectDeliveryDate = 0;

        try {
            if (lrnNo != null) {
                for (int i = 0; i < lrnNo.length; i++) {
                    if (lrnNo != null && !"".equals(lrnNo[i]) && deliveryDate != null && !"".equals(deliveryDate[i])) {
                        map.put("subLr", lrnNo[i]);
                        map.put("deliveryDate", (deliveryDate[i].replace("/", "-")).replace(".", "-"));
                        map.put("remarks", remarks[i]);
                        updateConnectDeliveryDate = (Integer) getSqlMapClientTemplate().update("wms.updateConnectDeliveryDate", map);
                    }
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return updateConnectDeliveryDate;

    }

    public int cancelConnectInvoice(WmsTO wms) {
        Map map = new HashMap();
        int cancelConnectInvoice = 0;

        try {
            map.put("userId", wms.getUserId());
            map.put("invoiceId", wms.getInvoiceId());
            System.out.println("map  " + map);
            cancelConnectInvoice = (Integer) getSqlMapClientTemplate().update("wms.cancelConnectInvoice", map);
            cancelConnectInvoice = (Integer) getSqlMapClientTemplate().update("wms.cancelConnectInvoiceDispatch", map);
            System.out.println("cancelConnectInvoice=== " + cancelConnectInvoice);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return cancelConnectInvoice;

    }

    public int connectSaveBillReceiving(WmsTO wms) {
        Map map = new HashMap();
        int connectSaveBillReceiving = 0;

        try {
            String accYear = ThrottleConstants.accountYear;
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("billNo", wms.getBillNo());
            map.put("billId", wms.getBillId());
            map.put("billDate", wms.getBillDate());
            map.put("refNo", wms.getReferenceNo());
            map.put("freightAmount", wms.getFreightAmount());
            map.put("unloadingCharge", wms.getUnloadingAmt());
            map.put("haltingCharge", wms.getHaltingAmt());
            map.put("otherCharge", wms.getOtherAmt());
            map.put("gstCharge", wms.getGstAmt());
            map.put("billAmount", wms.getBillAmt());
            map.put("vendorId", wms.getVendorId());
            map.put("periodFrom", wms.getPeriodFrom());
            map.put("periodTo", wms.getPeriodTo());
            map.put("dateOfReceived", wms.getDateOfReceived());
            map.put("dateOfEntry", wms.getDateOfEntry());
            map.put("receivedBy", wms.getReceivedBy());
            System.out.println("map  " + map);
            if ("".equals(wms.getBillId()) || wms.getBillId() == null) {
                String sequence = "HSC/" + accYear + "/";
                map.put("sequence", sequence);
                int refSequence = (Integer) getSqlMapClientTemplate().queryForObject("wms.connectBillRefNo", map);
                String refNo = "" + refSequence;
                if (refNo == null) {
                    refNo = "00001";
                } else if (refNo.length() == 1) {
                    refNo = "0000" + refNo;
                } else if (refNo.length() == 2) {
                    refNo = "000" + refNo;
                } else if (refNo.length() == 3) {
                    refNo = "00" + refNo;
                } else if (refNo.length() == 4) {
                    refNo = "0" + refNo;
                } else if (refNo.length() == 5) {
                    refNo = "" + refNo;
                }
                map.put("refNo", sequence + refNo);
                connectSaveBillReceiving = (Integer) getSqlMapClientTemplate().update("wms.connectSaveBillReceiving", map);
            } else {
                connectSaveBillReceiving = (Integer) getSqlMapClientTemplate().update("wms.connectUpdateBillReceiving", map);
            }
            System.out.println("connectSaveBillReceiving=== " + connectSaveBillReceiving);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return connectSaveBillReceiving;

    }

    public String getConnectDeliveryDate(WmsTO wms) {
        Map map = new HashMap();
        String getConnectDeliveryDate = "";

        try {
            map.put("subLr", wms.getLrnNo());
            System.out.println("map  " + map);
            getConnectDeliveryDate = (String) getSqlMapClientTemplate().queryForObject("wms.getConnectDeliveryDate", map);
            System.out.println("getConnectDeliveryDate=== " + getConnectDeliveryDate);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return getConnectDeliveryDate;

    }

    public int updateFreightAmount(int userId, String[] check, String[] lrnNo, String[] freightAmt, String[] haltingAmt, String[] unloadingAmt, String[] otherAmt, String[] totalAmt, String[] remarks) {
        Map map = new HashMap();
        int updateFreightAmount = 0;
        try {
            map.put("userId", userId);
            for (int i = 0; i < check.length; i++) {
                if ("1".equals(check[i])) {
                    map.put("remarks", remarks[i]);
                    map.put("freightAmt", freightAmt[i]);
                    map.put("haltingAmt", haltingAmt[i]);
                    map.put("unloadingAmt", unloadingAmt[i]);
                    map.put("otherAmt", otherAmt[i]);
                    map.put("totalAmt", totalAmt[i]);
                    map.put("subLr", lrnNo[i]);
                    Double total = Double.parseDouble(freightAmt[i]) + Double.parseDouble(haltingAmt[i]) + Double.parseDouble(unloadingAmt[i]) + Double.parseDouble(otherAmt[i]);
                    System.out.println("map   " + map);
                    if (total > 0) {
                        map.put("totalAmt", total);
                        updateFreightAmount = (Integer) getSqlMapClientTemplate().update("wms.updateFreightAmount", map);
                    }
                    System.out.println("updateFreightAmount---" + updateFreightAmount);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateFreightAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "updateFreightAmount", sqlException);
        }
        return updateFreightAmount;
    }

    public int updateDocketnumber(String[] check, String[] lrnNo, String[] docketNo) {
        Map map = new HashMap();
        int updateDocketnumber = 0;
        try {
            for (int i = 0; i < check.length; i++) {
                if ("1".equals(check[i])) {
                    map.put("docketNo", docketNo[i]);

                    map.put("subLr", lrnNo[i]);
                    updateDocketnumber = (Integer) getSqlMapClientTemplate().update("wms.updateDocketnumber", map);
                    System.out.println("updateDocketnumber---" + updateDocketnumber);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateDocketnumber Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "updateDocketnumber", sqlException);
        }
        return updateDocketnumber;
    }

    public int uploadPOD(String file, String saveFile, String check, String lrnNo) {
        Map map = new HashMap();
        int uploadPOD = 0;
        try {

            map.put("subLr", lrnNo);
            map.put("file", saveFile);
            map.put("path", file);
            System.out.println("map==" + map);

            uploadPOD = (Integer) getSqlMapClientTemplate().update("wms.uploadPOD", map);
            System.out.println("uploadPOD---" + uploadPOD);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadPOD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "uploadPOD", sqlException);
        }
        return uploadPOD;
    }

    public int checkConnectExistingVehicleTat(WmsTO wms) {
        Map map = new HashMap();
        int checkConnectExistingVehicleTat = 0;
        try {
            map.put("vehicleNo", "%" + wms.getVehicleNo().replaceAll(" ", "").replaceAll("-", "") + "%");
            map.put("status", wms.getStatus());
            System.out.println("map==" + map);
            checkConnectExistingVehicleTat = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkConnectExistingVehicleTat", map);
            System.out.println("checkConnectExistingVehicleTat---" + checkConnectExistingVehicleTat);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadPOD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "uploadPOD", sqlException);
        }
        return checkConnectExistingVehicleTat;
    }

    public int saveRpStockInward(WmsTO wms) {
        Map map = new HashMap();
        int saveRpStockInward = 0;
        try {
            String stockId = "";
            map.put("fromWhId", wms.getWhId());
            map.put("toWhId", wms.getWhId());
            map.put("whId", wms.getWhId());
            map.put("woId", "0");
            map.put("remark", "Primary Warehouse Update");
            map.put("userId", wms.getUserId());
            map.put("productId", wms.getProductId());
            map.put("qty", wms.getQty());
            int rpStockIdCount = (Integer) getSqlMapClientTemplate().queryForObject("wms.rpStockIdCount", map);
            if (rpStockIdCount > 0) {
                stockId = rpStockIdCount + "";
            } else {
                map.put("quantity", "0");
                map.put("excess", "0");
                map.put("damaged", "0");
                int insertStockDetails = (Integer) getSqlMapClientTemplate().insert("wms.insertRpStockDetails", map);
                stockId = insertStockDetails + "";
            }
            map.put("stockId", stockId);
            int inwardId = (Integer) getSqlMapClientTemplate().insert("wms.insertRpInwardMaster", map);
            for (int i = 0; i < wms.getSerialNos().length; i++) {
                map.put("serialNo", wms.getSerialNos()[i]);
                map.put("uom", wms.getUom()[i]);
                map.put("proCond", wms.getProCond()[i]);
                map.put("ipQty", wms.getIpQty()[i]);
                map.put("binId", wms.getBinIds()[i]);
                map.put("storageId", wms.getStorageIds()[i]);
                saveRpStockInward = (Integer) getSqlMapClientTemplate().insert("wms.insertRpSerialMaster", map);
                System.out.println("map  by siva" + map);
            }
            saveRpStockInward = (Integer) getSqlMapClientTemplate().update("wms.updateRpStockQty", map);
            System.out.println("saveRpStockInward---" + saveRpStockInward);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadPOD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "uploadPOD", sqlException);
        }
        return saveRpStockInward;
    }

    public int insertRpStockTransferDetails(WmsTO wms) {
        Map map = new HashMap();
        int status = 0;
        try {
            String stockId = "";
            map.put("fromWhId", wms.getWhId());
            map.put("whId", wms.getWhId());
            map.put("userId", wms.getUserId());
            map.put("toWhId", wms.getWarehouseId());
            map.put("vehicleNo", wms.getVehicleNo());
            map.put("driverMobile", wms.getMobileNo());
            map.put("driverName", wms.getDriverName());
            map.put("qty", wms.getSerialNos().length);
            String getSequence = "";
            Date date = new Date();
            String sd = new SimpleDateFormat("yyyy-MM-dd").format(date);
            getSequence = "STN" + sd.split("-")[0] + "RP";
            map.put("sequence", getSequence);
            int getLastRPStockTransferSeq = (Integer) getSqlMapClientTemplate().queryForObject("wms.getLastRPStockTransferSeq", map);
            String seqTemp = getLastRPStockTransferSeq + "";
            if (seqTemp.length() == 1) {
                getSequence = getSequence + "0000" + seqTemp;
            } else if (seqTemp.length() == 2) {
                getSequence = getSequence + "000" + seqTemp;
            } else if (seqTemp.length() == 3) {
                getSequence = getSequence + "00" + seqTemp;
            } else if (seqTemp.length() == 4) {
                getSequence = getSequence + "0" + seqTemp;
            } else if (seqTemp.length() == 5) {
                getSequence = getSequence + seqTemp;
            }
            map.put("transferNo", getSequence);
            int transferId = (Integer) getSqlMapClientTemplate().insert("wms.insertRpStockTransferMaster", map);
            map.put("transferId", transferId);
            for (int i = 0; i < wms.getSerialNos().length; i++) {
                map.put("serialId", wms.getSerialIds()[i]);
                map.put("productId", wms.getItemIds()[i]);
                map.put("serialNo", wms.getSerialNos()[i]);
                map.put("stockId", wms.getStockIds()[i]);
                int insertRpStockTransferDetails = (Integer) getSqlMapClientTemplate().insert("wms.insertRpStockTransferDetails", map);
                status = (Integer) getSqlMapClientTemplate().update("wms.updateRpSerialDetails", map);
                status = (Integer) getSqlMapClientTemplate().update("wms.reduceRpStockQty", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadPOD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "uploadPOD", sqlException);
        }
        return status;
    }

    public int uploadPODFiles(String file, String saveFile, String lrnNo, String subLrId, String dispatchid, String plannedTime) {
        Map map = new HashMap();
        int uploadPOD = 0;
        try {

            map.put("subLr", lrnNo);
            map.put("subLrId", subLrId);
            map.put("dispatchid", dispatchid);
            map.put("plannedTime", plannedTime);
            map.put("file", saveFile);
            map.put("path", file);
            System.out.println("map==" + map);

            uploadPOD = (Integer) getSqlMapClientTemplate().update("wms.uploadPODFiles", map);
            System.out.println("uploadPOD---" + uploadPOD);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadPOD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "uploadPOD", sqlException);
        }
        return uploadPOD;
    }

    public int saveIndentRequest(WmsTO wms) {
        Map map = new HashMap();
        int saveIndentRequest = 0;
        try {

            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("indentDate", wms.getIndentDate());
            map.put("boxQty", wms.getBoxQty());
            map.put("indentId", wms.getIndentId());
            map.put("fromLocation", wms.getFromLocation());
            map.put("toLocation", wms.getToLocation());
            map.put("deliveryDate", wms.getDeliveryDate());
            map.put("appointmentDate", wms.getAppointmentDate());
            map.put("deliveryType", wms.getDeliveryType());
            map.put("partyName", wms.getPartyName());
            map.put("vehicleType", wms.getVehicleType());
            map.put("qty", wms.getQty());
            System.out.println("map==" + map);
            if ("".equals(wms.getIndentId()) || wms.getIndentId() == null) {
                saveIndentRequest = (Integer) getSqlMapClientTemplate().update("wms.saveIndentRequest", map);
            } else {
                saveIndentRequest = (Integer) getSqlMapClientTemplate().update("wms.updateIndentRequest", map);
            }
            System.out.println("saveIndentRequest---" + saveIndentRequest);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadPOD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "uploadPOD", sqlException);
        }
        return saveIndentRequest;
    }

    public int insertImPoOrder(WmsTO wms, String[] materialName, String[] materialDescription, String[] poQty, String[] pending, String[] id, SqlMapClient session, String[] uom, String[] unitPrice, String[] price) {
        Map map = new HashMap();
        System.out.println("here mw");
        int insertImPoOrder = 0;
        try {
            String accYear = ThrottleConstants.accountYear;
            map.put("custId", wms.getCustId());
            map.put("userId", wms.getUserId());
            map.put("deliveryDate", wms.getDeliveryDate());
            map.put("poDate", wms.getPoDate());
            map.put("whId", wms.getWhId());
            map.put("totQty", wms.getTotQty());
            map.put("remarks", wms.getRemarks());
            map.put("tax", wms.getTaxVol());
            String poNo = "";
            int poSequence = (Integer) session.queryForObject("wms.getImPoSequence", map);
            String poSeq = poSequence + "";
            if (poSeq.length() == 0) {
                poNo = "IMPO/" + accYear + "/00001";
            } else if (poSeq.length() == 1) {
                poNo = "IMPO/" + accYear + "/0000" + poSeq;
            } else if (poSeq.length() == 2) {
                poNo = "IMPO/" + accYear + "/000" + poSeq;
            } else if (poSeq.length() == 3) {
                poNo = "IMPO/" + accYear + "/00" + poSeq;
            } else if (poSeq.length() == 4) {
                poNo = "IMPO/" + accYear + "/0" + poSeq;
            } else {
                poNo = "IMPO/" + accYear + "/" + poSeq;
            }
            map.put("poNo", poNo);

            insertImPoOrder = (Integer) session.insert("wms.insertImPOMaster", map);
            map.put("poId", insertImPoOrder);

            if (materialName != null && materialName.length > 0) {

                for (int i = 0; i < materialName.length; i++) {
                    map.put("materialName", materialName[i]);
                    map.put("materialDescription", materialDescription[i]);
                    map.put("poQty", poQty[i]);
                    map.put("materialId", wms.getItemIds()[i]);
                    map.put("uom", uom[i]);
                    map.put("pending", pending[i]);
                    map.put("unitPrice", unitPrice[i]);
                    map.put("price", price[i]);
                    map.put("id", id[i]);
                    int insertIntoImPoDetails = (Integer) session.insert("wms.insertImPODetails", map);
                    if (insertIntoImPoDetails > 0) {
                        int update = (Integer) session.update("wms.updateImIndentPendingQty", map);
//
                    }
                }
            }
            System.out.println("insertImPoOrder---" + insertImPoOrder);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadPOD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "uploadPOD", sqlException);
        }
        return insertImPoOrder;
    }

    public int insertImMaterialGrn(WmsTO wms, String[] poQty, String[] qty, String[] poStatus, String[] poId, String[] id, String[] good, String[] damage, String[] shortage, String[] balance, SqlMapClient session) {
        Map map = new HashMap();
        int insertImMaterialGrn = 0;
        int status = 0;
        try {
            String accYear = ThrottleConstants.accountYear;
            String[] materialId = wms.getItemIds();
            map.put("userId", wms.getUserId());
            map.put("grnDate", wms.getGrnDate());
            map.put("grnTime", wms.getGrnTime());
            map.put("invoiceNo", wms.getInvoiceNo());
            map.put("invoiceDate", wms.getInvoiceDate());
            map.put("whId", wms.getWhId());
            map.put("totQty", wms.getTotQty());
            map.put("remarks", wms.getRemarks());
            String grnNo = "";
            int grnSequence = (Integer) session.queryForObject("wms.getImGrnSequence", map);
            String grnSeq = grnSequence + "";
            if (grnSeq.length() == 0) {
                grnNo = "IMGRN/" + accYear + "/00001";
            } else if (grnSeq.length() == 1) {
                grnNo = "IMGRN/" + accYear + "/0000" + grnSeq;
            } else if (grnSeq.length() == 2) {
                grnNo = "IMGRN/" + accYear + "/000" + grnSeq;
            } else if (grnSeq.length() == 3) {
                grnNo = "IMGRN/" + accYear + "/00" + grnSeq;
            } else if (grnSeq.length() == 4) {
                grnNo = "IMGRN/" + accYear + "/0" + grnSeq;
            } else {
                grnNo = "IMGRN/" + accYear + "/" + grnSeq;
            }
            map.put("grnNo", grnNo);
            if (materialId != null && materialId.length > 0) {
                insertImMaterialGrn = (Integer) session.insert("wms.insertImMaterialGrnMaster", map);
                map.put("grnId", insertImMaterialGrn);
                for (int i = 0; i < materialId.length; i++) {
                    if (materialId[i].split("-").length > 0) {
                        map.put("materialId", materialId[i].split("-")[0]);
                        if (!"".equals(poId[i]) && poId[i] != null) {
                            map.put("poId", poId[i]);
                            map.put("poStatus", poStatus[i]);
                            map.put("poQty", poQty[i]);
                            map.put("good", good[i]);
                            map.put("damage", damage[i]);
                            map.put("shortage", shortage[i]);
                            map.put("balance", balance[i]);
                            map.put("id", id[i]);
                            map.put("rec", qty[i]);
                            status = (Integer) session.update("wms.updateImMaterialPOMasterStatus", map);
                            status = (Integer) session.update("wms.updateImMaterialPODetailStatus", map);
                            int getMaterialStatus = (Integer) session.queryForObject("wms.getImMaterialPoDetailStatus", map);
                            if (getMaterialStatus > 0) {
                                map.put("status", getMaterialStatus);
                                status = (Integer) session.update("wms.updateImMaterialPODetailStatus", map);
                                int masterStatus = (Integer) session.queryForObject("wms.getImMaterialPoMasterStatus", map);
                                if (masterStatus == 0) {
                                    map.put("status", "2");
                                    status = (Integer) session.update("wms.updateImMaterialPOMasterStatus", map);
                                }
                            }
                        }
                        map.put("grnQty", qty[i]);
                        int checkStockId = (Integer) session.queryForObject("wms.checkImMaterialStockId", map);
                        if (checkStockId > 0) {
                            map.put("stockId", checkStockId);
                            int updateImMaterialStockQty = (Integer) session.update("wms.updateImMaterialStockQty", map);
                        } else {
                            int stockId = (Integer) session.insert("wms.insertImMaterialStock", map);
                            map.put("stockId", stockId);
                            int updateImMaterialStockQty = (Integer) session.update("wms.updateImMaterialStockQty", map);
                        }
                        int insertImGrnDetails = (Integer) session.insert("wms.insertImMaterialGrnDetails", map);
                        if (insertImGrnDetails > 0) {
                            status = (Integer) session.update("wms.updateImMaterialPODetailPending", map);
                        }
                    }
                }
            }
            System.out.println("insertImGrnMaster---" + insertImMaterialGrn);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadPOD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "uploadPOD", sqlException);
        }
        return insertImMaterialGrn;
    }

    public int processInsertPODDetails(WmsTO wmsTO, int UserId, String[] actualFilePath, String[] fileSaved, String[] remarks) {

        Map map = new HashMap();
        FileInputStream fis = null;
        int tripId = 0;
        int vendorId = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        try {
            for (int x = 0; x < 3; x++) {
                if (fileSaved[x] != null) {
                    map.put("fileName" + x, fileSaved[x]);
                    File file = new File(actualFilePath[x]);
                    map.put("podfiles" + x, file.toString());
                } else {
                    map.put("fileName" + x, null);
                    map.put("podfiles" + x, null);
                }
            }

            System.out.println("Tesiting = ");
            map.put("userId", UserId);
            map.put("podname0", wmsTO.getPodName1());
            map.put("podname1", wmsTO.getPodName2());
            map.put("podname2", wmsTO.getPodName3());
            map.put("remarks0", wmsTO.getRemarks1());
            map.put("remarks1", wmsTO.getRemarks2());
            map.put("remarks2", wmsTO.getRemarks3());
            map.put("dispatchDetailId", wmsTO.getDispatchDetailId());
            map.put("Dispatchid", wmsTO.getDispatchid());
            map.put("PlannedTime", wmsTO.getPlannedTime() + "00:00:00");
            map.put("PlannedDate", wmsTO.getPlannedDate());
            map.put("LrnNo", wmsTO.getLrnNo());
            map.put("SubLrId", wmsTO.getSubLrId());
            map.put("dealerId", wmsTO.getDealerId());
            int status = 0;
            int mfrstatus = 0;

            String code = "";
            String[] temp;
            int vehicle_id = 0;
            int VendorLedgerId = 0;
            String getDealerCode = (String) getSqlMapClientTemplate().queryForObject("wms.getDealerCodeForIns", map);
            map.put("dealerCode", getDealerCode);

            int podId = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkConnectPod", map);
            if (podId == 0) {
                status = (Integer) getSqlMapClientTemplate().insert("wms.insertPOD", map);
            } else {
                map.put("podId", podId);
                status = (Integer) getSqlMapClientTemplate().update("wms.updateConnectPod", map);
            }

            if (status > 0) {
                status = (Integer) getSqlMapClientTemplate().update("wms.updatePodStatusn", map);
                System.out.println("statusstatus" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return vendorId;

    }

    public int updateConnectLrVendor(WmsTO wms) {
        Map map = new HashMap();
        int updateVendorName = 0;

        try {

            map.put("dispatchId", wms.getDispatchId());
            map.put("vendorId", wms.getVendorId());
            System.out.println("Mappp  " + map);
            //saveStockReceive = (Integer) getSqlMapClientTemplate().update("wms.saveSerialTempDetails", map);
            updateVendorName = (Integer) getSqlMapClientTemplate().update("wms.updateConnectLrVendor", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return updateVendorName;
    }

    public int insertDeTat(WmsTO wms) {
        Map map = new HashMap();
        int insertDeTat = 0;

        try {
            map.put("empName", wms.getEmpName());
            map.put("empId", wms.getEmpId());
            map.put("inTime", wms.getInTime());
            map.put("date", wms.getDate());
            map.put("userId", wms.getUserId());
            map.put("desigId", wms.getDesigId());
            map.put("whId", wms.getWhId());

            insertDeTat = (Integer) getSqlMapClientTemplate().update("wms.insertDeTat", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return insertDeTat;
    }

    public int updateDeTat(WmsTO wms) {
        Map map = new HashMap();
        int updateDeTat = 0;

        try {
            map.put("tatId", wms.getTatId());
            map.put("outTime", wms.getOutTime());
            map.put("status", "1");
            System.out.println("map in updateDe tat  " + map);
            updateDeTat = (Integer) getSqlMapClientTemplate().update("wms.updateDeTat", map);
            System.out.println("updateDe tat  " + updateDeTat);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return updateDeTat;
    }

    public ArrayList deliveryExecutiveDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList deliveryExecutiveDetails = new ArrayList();
        try {
            map.put("whId", wms.getWhId());
            System.out.println("map == " + map);
            deliveryExecutiveDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.deliveryExecutiveDetails", map);
            System.out.println("deliveryExecutiveDetails====" + deliveryExecutiveDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return deliveryExecutiveDetails;

    }

    public int updateStorageMaster(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int updateStorageMaster = 0;

        map.put("rackName", wmsTO.getRackName());
        map.put("storageName", wmsTO.getStorageName());
        map.put("binName", wmsTO.getBinName());
        map.put("binId", wmsTO.getBinId());
        map.put("rackId", wmsTO.getRackId());
        map.put("storageId", wmsTO.getStorageId());

        System.out.println("map" + map);

        try {
            if (updateStorageMaster == 0) {

                if ((wmsTO.getStorageName() != null && wmsTO.getStorageName() != "")) {
                    updateStorageMaster = (Integer) getSqlMapClientTemplate().update("wms.updateStorageName", map);
                }
                if ((wmsTO.getRackName() != null && wmsTO.getRackName() != "")) {
                    updateStorageMaster = (Integer) getSqlMapClientTemplate().update("wms.updateRackName", map);
                }
                if ((wmsTO.getBinName() != null && wmsTO.getBinName() != "")) {
                    updateStorageMaster = (Integer) getSqlMapClientTemplate().update("wms.updateBinName", map);
                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadNormalOrder", sqlException);
        }
        return updateStorageMaster;
    }

    public int insertStorageMaster(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStorageMaster = 0;
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("rackName", wmsTO.getRackName());
        map.put("storageName", wmsTO.getStorageName());
        map.put("binName", wmsTO.getBinName());

        System.out.println(map.put("storageName", wmsTO.getStorageName()));

        try {
            if (insertStorageMaster == 0) {

                if ((wmsTO.getStorageName() != null && wmsTO.getStorageName() != "")) {
                    insertStorageMaster = (Integer) getSqlMapClientTemplate().insert("wms.insertStorageName", map);
                }

                if ((wmsTO.getRackName() != null && wmsTO.getRackName() != "")) {
                    insertStorageMaster = (Integer) getSqlMapClientTemplate().insert("wms.insertRackName", map);
                }

                if ((wmsTO.getBinName() != null && wmsTO.getBinName() != "")) {
                    insertStorageMaster = (Integer) getSqlMapClientTemplate().insert("wms.insertBinName", map);
                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadNormalOrder", sqlException);
        }
        return insertStorageMaster;
    }

    public ArrayList getStorageDetails() {
        Map map = new HashMap();
        ArrayList getStorageDetails = new ArrayList();

        try {

            getStorageDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getStorageDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getStorageDetails;

    }

    public ArrayList getRackDetails2() {
        Map map = new HashMap();
        ArrayList getRackDetails2 = new ArrayList();

        try {

            getRackDetails2 = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getRackDetails2", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getRackDetails2;

    }

    public ArrayList getBinkDetails2() {
        Map map = new HashMap();
        ArrayList getBinkDetails2 = new ArrayList();

        try {

            getBinkDetails2 = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getBinkDetails2", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getBinkDetails2;

    }

    public String binNameAlreadyExists(String binName) {
        Map map = new HashMap();
        String status = "";
        int checkExistBinCode = 0;

        try {
            System.out.println("binCodeexistence" + binName);
            map.put("binName", binName);
            System.out.println("MAP----" + map);

            checkExistBinCode = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkExistBinName", map);
            System.out.println("checkExistBinName-----" + checkExistBinCode);

            if (checkExistBinCode == 0) {
                System.out.println("checkExistBinCode-----" + checkExistBinCode);
                status = "0";

            } else {
                status = "1";
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return status;

    }

    public String rackNameAlreadyExists(String rackName) {
        Map map = new HashMap();
        String status = "";
        int checkExistRackCode = 0;
//        int checkExistSapCodeNoTemp = 0;
        try {
            System.out.println("rackNameexistence" + rackName);
            map.put("rackName", rackName);
            System.out.println("MAP----" + map);

            checkExistRackCode = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkExistRackName", map);
            System.out.println("checkExistRackCode-----" + checkExistRackCode);

            if (checkExistRackCode == 0) {
                System.out.println("checkExistRackCode-----" + checkExistRackCode);
                status = "0";

            } else {
                status = "1";
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return status;

    }

    public String storageNameAlreadyExists(String storageName) {
        Map map = new HashMap();
        String status = "";
        int checkExistStorageCode = 0;
//        int checkExistSapCodeNoTemp = 0;
        try {
            System.out.println("storageNameexistence" + storageName);
            map.put("storageName", storageName);
            System.out.println("MAP----" + map);

            checkExistStorageCode = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkExistStorageName", map);
            System.out.println("checkExistStorageCode-----" + checkExistStorageCode);

            if (checkExistStorageCode == 0) {
                System.out.println("checkExistStorageCode-----" + checkExistStorageCode);
                status = "0";

            } else {
                status = "1";
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return status;

    }

    public ArrayList getCityList() {
        Map map = new HashMap();
        ArrayList getCityList = new ArrayList();
        try {
            getCityList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getCityList", map);
            System.out.println("getCityList size=" + getCityList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWareHouseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWareHouseList List", sqlException);
        }

        return getCityList;
    }

    public ArrayList getImMaterialIssueMaster(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImMaterialIssueMaster = new ArrayList();
        try {
            map.put("issueId", wms.getIssueId());
            map.put("whId", wms.getWhId());
            System.out.println("map  " + map);
            getImMaterialIssueMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImMaterialIssueMaster", map);
            System.out.println("getImMaterialIssueMaster size=" + getImMaterialIssueMaster.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWareHouseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWareHouseList List", sqlException);
        }

        return getImMaterialIssueMaster;
    }

    public ArrayList getImMaterialGroup(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImMaterialGroup = new ArrayList();
        try {
            System.out.println("map  " + map);
            getImMaterialGroup = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImMaterialGroup", map);
            System.out.println("getImMaterialGroup size=" + getImMaterialGroup.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWareHouseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWareHouseList List", sqlException);
        }

        return getImMaterialGroup;
    }

    public ArrayList getImMaterialUom(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImMaterialUom = new ArrayList();
        try {
            System.out.println("map  " + map);
            getImMaterialUom = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImMaterialUom", map);
            System.out.println("getImMaterialUom size=" + getImMaterialUom.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWareHouseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWareHouseList List", sqlException);
        }

        return getImMaterialUom;
    }

    public ArrayList imMaterialStockView(WmsTO wms) {
        Map map = new HashMap();
        ArrayList imMaterialStockView = new ArrayList();
        try {
            map.put("whId", wms.getWhId());
            System.out.println("map  " + map);
            imMaterialStockView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imMaterialStockView", map);
            System.out.println("imMaterialStockView size=" + imMaterialStockView.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("imMaterialStockView Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "imMaterialStockView List", sqlException);
        }

        return imMaterialStockView;
    }

    public int updateImGrnIssue(String[] floorQty, String[] usedQty, String[] excess, String[] shortage, String[] status, String[] detailId, String[] floorPackQty, String[] materialIds, WmsTO wms, int userId) {
        Map map = new HashMap();
        int updateImGrnIssue = 0;
        try {
            System.out.println("map  " + map);
            for (int i = 0; i < floorQty.length; i++) {
                map.put("floorQty", floorQty[i]);
                map.put("usedQty", usedQty[i]);
                map.put("excess", excess[i]);
                map.put("shortage", shortage[i]);
                map.put("detailId", detailId[i]);
                map.put("status", status[i]);
                map.put("userId", userId);
                updateImGrnIssue = (Integer) getSqlMapClientTemplate().update("wms.updateImGrnIssue", map);
                if ("1".equals(status[i])) {
                    map.put("whId", wms.getWhId());
                    map.put("materialId", materialIds[i]);
                    int stockId = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkImMaterialStockId", map);
                    map.put("stockId", stockId);
                    if (stockId != 0) {
                        map.put("grnQty", floorPackQty[i]);
                        int materialReference = (Integer) getSqlMapClientTemplate().update("wms.updateMaterialReference", map);
//                        int update = (Integer) getSqlMapClientTemplate().update("wms.updateImMaterialStockQty", map);
                    } else {
                        stockId = (Integer) getSqlMapClientTemplate().update("wms.insertImMaterialStock", map);
//                        map.put("stockId", stockId);
//                        int update = (Integer) getSqlMapClientTemplate().update("wms.updateImMaterialStockQty", map);
                    }
                }
                System.out.println("updateImGrnIssue size=" + updateImGrnIssue);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWareHouseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWareHouseList List", sqlException);
        }

        return updateImGrnIssue;
    }

    public ArrayList getImMaterialIssueDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImMaterialIssueDetails = new ArrayList();
        try {
            map.put("issueId", wms.getIssueId());
            map.put("whId", wms.getWhId());
            getImMaterialIssueDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImMaterialIssueDetails", map);
            System.out.println("getImMaterialIssueDetails size=" + getImMaterialIssueDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWareHouseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWareHouseList List", sqlException);
        }

        return getImMaterialIssueDetails;
    }

    public int insertImMaterialGrnIssue(WmsTO wms, String[] skuCode, String[] uom, String[] stockQty, String[] issueQty, SqlMapClient session) {
        Map map = new HashMap();
        int issueId = 0;
        int status = 0;
        try {
            String accYear = ThrottleConstants.accountYear;
            String[] materialId = wms.getItemIds();
            map.put("userId", wms.getUserId());
            map.put("issueDate", wms.getIssueDate());
            map.put("issueTime", wms.getIssueTime());
            map.put("whId", wms.getWhId());
            map.put("totQty", wms.getTotQty());
            map.put("remarks", wms.getRemarks());
            map.put("receivedBy", wms.getReceivedBy());

            String issueNo = "";
            int issueSequence = (Integer) session.queryForObject("wms.imIssueSequence", map);
            String issueSeq = issueSequence + "";
            if (issueSeq.length() == 0) {
                issueNo = "GISID/" + accYear + "/00001";
            } else if (issueSeq.length() == 1) {
                issueNo = "GISID/" + accYear + "/0000" + issueSeq;
            } else if (issueSeq.length() == 2) {
                issueNo = "GISID/" + accYear + "/000" + issueSeq;
            } else if (issueSeq.length() == 3) {
                issueNo = "GISID/" + accYear + "/00" + issueSeq;
            } else if (issueSeq.length() == 4) {
                issueNo = "GISID/" + accYear + "/0" + issueSeq;
            } else {
                issueNo = "GISID/" + accYear + "/" + issueSeq;
            }
            map.put("issueNo", issueNo);

            issueId = (Integer) session.insert("wms.insertImMaterialIssueMaster", map);
            map.put("issueId", issueId);
            for (int i = 0; i < materialId.length; i++) {
                map.put("materialId", materialId[i].split("~")[0]);
                map.put("skuCode", skuCode[i]);
                map.put("uom", uom[i]);
                map.put("issueQty", issueQty[i]);
                map.put("stockQty", stockQty[i]);
                map.put("status", "0");
                Double packQty = (Double) session.queryForObject("wms.getImMaterialPackQty", map);
                Double tot = packQty * Double.parseDouble(issueQty[i]);
                map.put("floorQty", tot);
                int stockId = (Integer) session.queryForObject("wms.checkImMaterialStockId", map);
                int insertImIssueDetails = (Integer) session.insert("wms.insertImMaterialIssue", map);
                if (stockId > 0) {
                    map.put("stockId", stockId);
                    int reduceImMaterialStock = (Integer) session.update("wms.reduceImMaterialStockQty", map);
                }
            }
            System.out.println("issueId---" + issueId);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadPOD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "uploadPOD", sqlException);
        }
        return issueId;
    }

    public int getInstaMart(WmsTO wmsTO) {
        Map map = new HashMap();
        int getInstaMart = 0;
        try {
//            String materialId= "";
            map.put("productName", wmsTO.getProductName());
            map.put("skuCode", wmsTO.getSkuCode());
            map.put("productDescription", wmsTO.getProductDescription());
            map.put("minimumStockQty", wmsTO.getMinimumStockQty());
            map.put("uom", wmsTO.getUom2());
            map.put("temperature", wmsTO.getTemperature());
            map.put("storage", wmsTO.getStorage());
            map.put("activeInd", wmsTO.getActiveInd());
            map.put("productId", wmsTO.getProductId());

            System.out.println("MAP---" + map);
            if (wmsTO.getProductId() != "" && wmsTO.getProductId() != null) {
                getInstaMart = (Integer) getSqlMapClientTemplate().update("wms.updateInstaMartProductMaster", map);
            } else {
                getInstaMart = (Integer) getSqlMapClientTemplate().update("wms.inserInstaMartProductMaster", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getInstaMart;

    }

    public ArrayList getInstaMartProduct(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList getInstaMartProduct = new ArrayList();
        try {

            getInstaMartProduct = (ArrayList) getSqlMapClientTemplate().queryForList("wms.instaMartProductMaster", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return getInstaMartProduct;

    }

    public int insertImProductMaster(WmsTO wms) {
        Map map = new HashMap();

        int insertDealerList = 0;
        try {
            map.put("userId", wms.getUserId());
            map.put("productName", wms.getProductName());
            map.put("productDescription", wms.getProductDescription());
            map.put("uom", wms.getUom2());
            map.put("storage", wms.getStorage());
            map.put("skuCode", wms.getSkuCode());
            map.put("minimumStockQty", wms.getMinimumStockQty());
            map.put("temperature", wms.getTemperature());
            map.put("activeInd", wms.getActiveInd());
            map.put("status", wms.getStatus());

            System.out.println("map    " + map);

            try {
//                
                int code = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkExistImSkuCodeTemp", map);
                if (code == 0) {
                    int code1 = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkExistImSkuCodeMain", map);
                    if (code1 == 0) {
                        map.put("status", "0");
                    } else {
                        map.put("status", "2");
                    }
                } else {
                    map.put("status", "1");
                }
                insertDealerList = (Integer) getSqlMapClientTemplate().insert("wms.insertImProductMasterTemp", map);
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-SYS-01", CLASS,
                        "uploadNormalOrder", sqlException);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return insertDealerList;

    }

    public ArrayList getImProductMasterTemp(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImProductMasterTemp = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            getImProductMasterTemp = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImProductMasterTemp", map);
//            getDealerList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getIfbOrderUpload", map);
//            WmsTO wms1 = new WmsTO();
//            if (getIFBList.size() > 0) {
//                for (int i = 0; i < getIFBList.size(); i++) {
//                    wms1 = (WmsTO) getIFBList.get(i);
//                    System.out.println("idsishd   "+wms1.getBillDoc());
//                }
//            }
//            System.out.println(getIFBList.get(0));
            System.out.println("getImProductMasterTemp========" + getImProductMasterTemp.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getImProductMasterTemp", sqlException);
        }
        return getImProductMasterTemp;

    }

    public int imProductMasterToMainTable(WmsTO wms, int userId, String whId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int lrNo = 0;
        String checkSkuCode = "";
        int deleteStatus = 0;

        ArrayList imProductMasterToMainTable = new ArrayList();
        WmsTO wmsTO = new WmsTO();
        try {
            map.put("userId", userId);
            imProductMasterToMainTable = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImProductMasterTemp", map);
            Iterator itr = imProductMasterToMainTable.iterator();
            while (itr.hasNext()) {
                wmsTO = (WmsTO) itr.next();
                map.put("productName", wmsTO.getProductName());
                map.put("skuCode", wmsTO.getSkuCode());
                map.put("productDescription", wmsTO.getProductDescription());
                map.put("minimumStockQty", wmsTO.getMinimumStockQty());
                map.put("uom", wmsTO.getUom2());
                map.put("temperature", wmsTO.getTemperature());
                map.put("storage", wmsTO.getStorage());
                map.put("activeInd", wmsTO.getActiveInd());
                map.put("status", wmsTO.getStatus());

                checkSkuCode = wmsTO.getStatus();

                if ("0".equals(checkSkuCode)) {
                    int imProductMasterToMain = (Integer) getSqlMapClientTemplate().update("wms.inserInstaMartProductMaster", map);
                }

            }

            map.put("userId", userId);
            deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.imProductMasterDeleteTemp", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public ArrayList vehicleUtilizationCust(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList vehicleUtilizationCust = new ArrayList();
        try {
            map.put("userId", wms.getUserId());

            vehicleUtilizationCust = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getCustVehicleUtilization", map);
            System.out.println("vehicleUtilizationCust---" + vehicleUtilizationCust.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleUtilizationCust Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "ProductWarehouseMasterList", sqlException);
        }
        return vehicleUtilizationCust;
    }

    public ArrayList getTripDetailsReport(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getTripDetailsReport = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("custId", wms.getCustId());
            map.put("customer", wms.getCustId());
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("state", wms.getStateId());
            map.put("city", wms.getCity());
            map.put("whId", wms.getWhId());

            System.out.println("map    " + map);

            getTripDetailsReport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getTripDetailsReport", map);
            System.out.println("getTripDetailsReport---" + getTripDetailsReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleUtilizationCust Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "ProductWarehouseMasterList", sqlException);
        }
        return getTripDetailsReport;
    }

    public ArrayList designationName(WmsTO wms) {
        Map map = new HashMap();
        ArrayList designationName = new ArrayList();
        try {

            System.out.println("map == " + map);
            designationName = (ArrayList) getSqlMapClientTemplate().queryForList("wms.designationNames", map);
            System.out.println("designationName====" + designationName.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return designationName;

    }

    public ArrayList getEmployeeId(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getEmployeeId = new ArrayList();
        try {
            map.put("designationId", wmsTo.getDesignationId());
            System.out.println("itemId=+++++" + wmsTo.getItemId());
            getEmployeeId = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getEmployeeId", map);
            System.out.println("getEmployeeId.size()-----" + getEmployeeId.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getEmployeeId;

    }

    public ArrayList getImVendorList(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getImVendorList = new ArrayList();
        try {
            map.put("whId", wmsTo.getWhId());
            getImVendorList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImVendorList", map);
            System.out.println("getImVendorList()-----" + getImVendorList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getImVendorList;
    }

    public ArrayList getImSkuDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getImSkuDetails = new ArrayList();
        try {
            map.put("whId", wmsTo.getWhId());
            getImSkuDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImSkuDetails", map);
            System.out.println("getImSkuDetails()-----" + getImSkuDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getImSkuDetails;
    }

    public int getSaveEditPage(WmsTO wms) {
        Map map = new HashMap();
        int getInstaMart = 0;
        try {
//            String materialId= "";

            map.put("productName", wms.getProductName());
            map.put("productVariantId", wms.getProductVariantId());
            map.put("optionName", wms.getOptionName());
            map.put("optionValue", wms.getOptionValue());
            map.put("barCode", wms.getBarCode());
            map.put("hsnCode", wms.getHsnCode());
            map.put("categoryName", wms.getCategoryName());
            map.put("subCategoryName", wms.getSubCategoryName());

            map.put("productId1", wms.getProductId());

            System.out.println("productId    " + map);

            System.out.println("MAP---" + map);

            getInstaMart = (Integer) getSqlMapClientTemplate().update("wms.getSaveEditPage", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getInstaMart;

    }

    public ArrayList getImChildSkuDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getImChildSkuDetails = new ArrayList();
        try {
            map.put("whId", wmsTo.getWhId());
            getImChildSkuDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImChildSkuDetails", map);
            System.out.println("getImChildSkuDetails()-----" + getImChildSkuDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getImChildSkuDetails;
    }

    public ArrayList getdzProductUpload(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getdzProductUploadTemp = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            getdzProductUploadTemp = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getdzProductUpload", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getdzProductUploadTemp", sqlException);
        }
        return getdzProductUploadTemp;

    }

    public ArrayList getImDealerList(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getImDealerList = new ArrayList();
        try {
            
            map.put("whId", wmsTo.getWhId());
            getImDealerList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImDealerList", map);
            System.out.println("getImDealerList()-----" + getImDealerList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getImDealerList;
    }

    public ArrayList getImInvoiceMaster(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getImInvoiceMaster = new ArrayList();
        try {
//            Date date = new Date();
//            SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
//            String endDate = sd.format(date);
//            String startDate = "01-" + endDate.split("-")[1] + endDate.split("-")[2];
            if (wmsTo.getFromDate() != null && !"".equals(wmsTo.getFromDate())) {
                map.put("fromDate", wmsTo.getFromDate());
            } else {
                map.put("fromDate", null);
            }

            if (wmsTo.getToDate() != null && !"".equals(wmsTo.getToDate())) {
                map.put("toDate", wmsTo.getToDate());
            } else {
                map.put("toDate", null);
            }
            map.put("whId", wmsTo.getWhId());
            System.out.println("map---------" + map);
            getImInvoiceMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImInvoiceMaster", map);
            System.out.println("getImInvoiceMaster()-----" + getImInvoiceMaster.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getImInvoiceMaster;
    }

    public ArrayList getImGetCrateScanned(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getImGetCrateScanned = new ArrayList();
        try {
            map.put("whId", wmsTo.getWhId());
            map.put("dealerId", wmsTo.getDealerId());
            getImGetCrateScanned = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImGetCrateScanned", map);
            System.out.println("getImGetCrateScanner()-----" + getImGetCrateScanned.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getImGetCrateScanned;
    }

    public int deleteImCrateScanned(WmsTO wmsTo) {
        Map map = new HashMap();
        int deleteImCrateScanned = 0;
        try {
            map.put("whId", wmsTo.getWhId());
            map.put("scanId", wmsTo.getScanId());
            map.put("status", "0");
            deleteImCrateScanned = (Integer) getSqlMapClientTemplate().update("wms.updateImCrateStatus", map);
            deleteImCrateScanned = (Integer) getSqlMapClientTemplate().delete("wms.deleteImCrateScanned", map);
            System.out.println("deleteImCrateScanned()-----" + deleteImCrateScanned);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return deleteImCrateScanned;
    }

    public ArrayList getImInvoiceDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getImInvoiceDetails = new ArrayList();
        try {
            map.put("whId", wmsTo.getWhId());
            map.put("lrId", wmsTo.getLrId());
            getImInvoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImInvoiceDetails", map);
            System.out.println("getImInvoiceDetails()-----" + getImInvoiceDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getImInvoiceDetails;
    }

    public int imInvoiceUpload(WmsTO wmsTO, int userId) {
        Map map = new HashMap();
        map.put("userId", userId);
        map.put("whId", wmsTO.getWhId());
        int lrNo = 0;
        SqlMapClient session = getSqlMapClient();
        int status = 0;
        int updateStatus = 0;
        try {
            int lrNumbercheck = 0;
            String hubCode = (String) session.queryForObject("wms.getHubCode", map);
            if (hubCode == null || "".equals(hubCode)) {
                hubCode = "HSC";
            }
            String accYear = ThrottleConstants.accountYear;
            Date date = new Date();
            String str = new SimpleDateFormat("yyyy-MM-dd").format(date);
            String[] date1 = str.split("-");
            map.put("month", date1[1]);
            String seq = "HSC" + accYear + hubCode + date1[1];
            map.put("lrnew", seq);
            int length = seq.length();
            map.put("length", length);
            int lrNumberCount = (Integer) session.queryForObject("wms.getImLrSequence", map);
            lrNumbercheck = lrNumberCount;
            System.out.println("lrNo-----" + lrNumbercheck);

            String lrSequence = "" + lrNumbercheck;

            if (lrSequence == null) {
                lrSequence = "0001";
            } else if (lrSequence.length() == 1) {
                lrSequence = "000" + lrSequence;
            } else if (lrSequence.length() == 2) {
                lrSequence = "00" + lrSequence;
            } else if (lrSequence.length() == 3) {
                lrSequence = "0" + lrSequence;
            } else if (lrSequence.length() == 4) {
                lrSequence = "" + lrSequence;
            }
            String lrNumber = "HSC" + accYear + hubCode + date1[1] + lrSequence;
            map.put("lrNo", lrNumber);
            map.put("lrDate", str);
            map.put("supplierId", wmsTO.getSupplierId());
            map.put("vendorId", wmsTO.getVendorId());
            if (!"".equals(wmsTO.getDeviceId()) && wmsTO.getDeviceId() != null) {
                map.put("deviceId", wmsTO.getDeviceId().split("-")[0]);
            }
            map.put("vehicleNo", wmsTO.getVehicleNo());
            map.put("driverName", wmsTO.getDriverName());
            map.put("driverMobile", wmsTO.getDriverMobileNo());
            map.put("dispatchDate", wmsTO.getDispatchDate());
            map.put("tripStartTime", wmsTO.getTripStartTime());
            map.put("remarks", wmsTO.getRemarks());
            map.put("noOfCrates", wmsTO.getBoxQty());
            map.put("lastResponse", wmsTO.getReason());
            map.put("lastDeviceId", wmsTO.getMachineId());
            map.put("vehicleType", wmsTO.getVehicleType());

            int lrId = (Integer) session.insert("wms.insertImInvoiceMaster", map);
            int lrDetailId = 0;

            map.put("lrId", lrId);
            map.put("tripId", lrId);
            System.out.println("lrId-----" + lrId);
            for (int i = 0; i < wmsTO.getInvoiceNoE().length; i++) {
                map.put("invoiceNo", wmsTO.getInvoiceNoE()[i]);
                map.put("invoiceDate", wmsTO.getInvoiceDates()[i]);
                map.put("customerId", wmsTO.getCustomerIds()[i]);
                map.put("qty", wmsTO.getQtyE()[i]);
                map.put("dispatchQty", wmsTO.getQuantitys()[i]);
                map.put("noOfCrates1", wmsTO.getNoOfCrates1()[i]);
                map.put("invoiceAmountL", wmsTO.getInvoiceAmountL()[i]);
                lrDetailId = (Integer) session.insert("wms.insertImInvoiceDetails", map);
            }
            String getImCustomerListLr = (String) session.queryForObject("wms.getImCustomerListLr", map);
            String[] custIds = getImCustomerListLr.split(",");
            System.out.println("custIds" + getImCustomerListLr);
            for (int i = 0; i < custIds.length; i++) {
                String sublr = lrNumber + "-" + (i + 1);
                map.put("sublr", sublr);
                map.put("customerId", custIds[i]);
                int sublrId = (Integer) session.insert("wms.insertImSublrId", map);
                map.put("sublrId", sublrId);
                int update = (Integer) session.update("wms.updateImSubLr", map);
                map.put("status", "2");
                update = (Integer) session.update("wms.updateImCrateScanned", map);
            }
            status = lrId;

            int startId = 0;
            String src = (String) session.queryForObject("wms.getWhLatLong", map);
            map.put("src", src);
            map.put("dest", src);
            String srcName = (String) session.queryForObject("trip.getTripSourceName", map);
            map.put("srcName", srcName);
            map.put("destName", srcName);
            map.put("custId", "5");
            String device = wmsTO.getDeviceId();
            if (!"".equals(device) && device != null) {

                String deviceId = device.split("-")[0];
                String deviceNo = device.split("-")[1];
                map.put("deviceId", deviceId);
                map.put("deviceNo", deviceNo);
                startId = (Integer) session.insert("trip.insertTripStartApiMaster", map);
                map.put("startId", startId);

                /////////////////////
                for (int i = 0; i < wmsTO.getInvoiceNoE().length; i++) {

                    String pincode = (String) session.queryForObject("wms.getInstaMartPincode", map);
                    String latLong = (String) session.queryForObject("wms.getInstaMartLatLong", map);
                    if (!"".equals(latLong) && latLong != null && latLong.split("-").length == 2) {
                        map.put("latitude", latLong.split("-")[0]);
                        map.put("longitude", latLong.split("-")[1]);
                        map.put("clientName", pincode.split("~")[1]);
                        int insertTripStartApiDetails = (Integer) session.insert("trip.insertTripStartApiDetails", map);
                    } else if (pincode != null && startId != 0) {
                        map.put("latitude", "");
                        map.put("longitude", "");
                        map.put("clientName", pincode.split("~")[0]);
                        int insertTripStartApiDetails = (Integer) session.insert("trip.insertTripStartApiDetails", map);
                    }

                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getGtnDetails", sqlException);
        }

        return status;
    }

    public int insertImDealerMaster(WmsTO wmsTO) {
        Map map = new HashMap();
        int insertImDealerMaster = 0;

        try {
            map.put("dealerNamee", "%" + wmsTO.getDealerName() + "%");
            map.put("dealerCodee", "%" + wmsTO.getDealerCode() + "%");
            map.put("userId", wmsTO.getUserId());
            map.put("whId", wmsTO.getWhId());
            map.put("dealerName", wmsTO.getDealerName());
            map.put("dealerCode", wmsTO.getDealerCode());
            map.put("industries", wmsTO.getIndustries());
            map.put("district", wmsTO.getDistrict());
            map.put("street", wmsTO.getStreet());
            map.put("address", wmsTO.getAddress());
            map.put("address2", wmsTO.getAddress1());
            map.put("city", wmsTO.getCity());
            map.put("pincode", wmsTO.getPincode());
            map.put("phone1", wmsTO.getMobileNo());
            map.put("phone2", wmsTO.getPhoneNumber());
            map.put("gstNo", wmsTO.getGstNo());
            map.put("customerName", wmsTO.getCustomerName());
            map.put("customerPhone", wmsTO.getMobile());
            map.put("activeInd", wmsTO.getActiveInd());
            map.put("dealerId", wmsTO.getDealerId());

            System.out.println("MAP---" + map);
            if (!"".equals(wmsTO.getDealerId()) && wmsTO.getDealerId() != null) {
                int updateImDealerMaster = (Integer) getSqlMapClientTemplate().update("wms.updateImDealerMaster", map);
                insertImDealerMaster = 10;
            } else {
                int getDealerName = (Integer) getSqlMapClientTemplate().queryForObject("wms.getDealerName", map);
                System.out.println("getDealerName" + getDealerName);
                int getDealerCode = (Integer) getSqlMapClientTemplate().queryForObject("wms.getDealerCode", map);
                System.out.println("getDealerCode" + getDealerCode);
                if (getDealerName > 0) {
                    insertImDealerMaster = 1000;
                }
                if (getDealerCode > 0) {
                    insertImDealerMaster = 1001;
                }
                if (getDealerCode == 0 && getDealerName == 0) {
                    int insertDealerMaster = (Integer) getSqlMapClientTemplate().update("wms.insertImDealerMaster", map);
                    insertImDealerMaster = 11;
                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return insertImDealerMaster;

    }

    public ArrayList getImDealerMaster(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList getImDealerMaster = new ArrayList();
        try {
            map.put("whId", wmsTO.getWhId());
            getImDealerMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imDealerMaster", map);
            System.out.println("getImDealerMaster   " + getImDealerMaster.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return getImDealerMaster;

    }

    public int insertImVendorMaster(WmsTO wms) {
        Map map = new HashMap();
        int insertImVendorMaster = 0;
        try {
            map.put("whId", wms.getWhId());
            map.put("userId", wms.getUserId());
            map.put("vendorId", wms.getVendorId());
            map.put("vendorName", wms.getVendorName());
            map.put("vendorCode", wms.getVendorCode());
            map.put("city", wms.getCity());
            map.put("state", wms.getState());
            map.put("address", wms.getAddress());
            map.put("phoneNumber", wms.getPhoneNumber());

            map.put("emailId", wms.getEmailId());
            map.put("contactPerson", wms.getContactPerson());
            map.put("gstNo", wms.getGstNo());

            System.out.println("map  " + map);

            if (wms.getVendorId() != null && !"".equals(wms.getVendorId())) {
                map.put("vendorId", wms.getVendorId());
                insertImVendorMaster = (Integer) getSqlMapClientTemplate().update("wms.updateImVendorMaster", map);
            } else {
                insertImVendorMaster = (Integer) getSqlMapClientTemplate().update("wms.insertImVendorMaster", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return insertImVendorMaster;

    }

    public ArrayList imVendorMasterDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList imVendorMasterDetails = new ArrayList();

        try {

            map.put("whId", wms.getWhId());
            map.put("status", wms.getStatus());
            System.out.println("map  " + map);
            imVendorMasterDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imVendorMasterDetails", map);
            System.out.println("imVendorMasterDetails=== " + imVendorMasterDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return imVendorMasterDetails;
    }

    public int updateImGrnInboundDetails(WmsTO wms) {
        Map map = new HashMap();
        int updateImGrnInboundDetails = 0;
        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("status", wms.getStatus());
            if (wms.getSelectedIndex() != null) {
                for (int i = 0; i < wms.getSelectedIndex().length; i++) {
                    if ("1".equals(wms.getSelectedIndex()[i])) {
                        map.put("updatedDate", wms.getUpdatedDate()[i]);
                        map.put("updatedTime", wms.getUpdatedTime()[i]);
                        map.put("acceptedQty", wms.getAcceptedQty()[i]);
                        map.put("rejectedQty", wms.getRejectedQty()[i]);
                        map.put("detailId", wms.getDetailIds()[i]);
                        int skuId = (Integer) getSqlMapClientTemplate().queryForObject("wms.getImGrnSkuId", map);
                        map.put("skuId", skuId);
                        if (skuId != 0) {
                            int stockId = (Integer) getSqlMapClientTemplate().queryForObject("wms.getImSkuStockId", map);
                            if (stockId != 0) {
                                map.put("stockId", stockId);
                            } else {
                                stockId = (Integer) getSqlMapClientTemplate().insert("wms.insertImSkuStock", map);
                                map.put("stockId", stockId);
                            }
                            map.put("stockQty", wms.getAcceptedQty()[i]);
                            int update = (Integer) getSqlMapClientTemplate().update("wms.updateImSkuStock", map);
                        }
                        System.out.println("map  " + map);
                        updateImGrnInboundDetails = (Integer) getSqlMapClientTemplate().update("wms.updateImGrnInboundDetails", map);
                        System.out.println("updateImGrnInboundDetails=== " + updateImGrnInboundDetails);
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return updateImGrnInboundDetails;
    }

    public ArrayList getImMachineList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImMachineList = new ArrayList();
        try {
            map.put("whId", wms.getWhId());
            System.out.println("map  " + map);
            getImMachineList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImMachineList", map);
            System.out.println("getImMachineList=== " + getImMachineList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return getImMachineList;
    }

    public int updateFreightAmountNew(int userId, String[] check, String[] lrnNo, String[] lrNo, String[] vendorName, String[] customerName, String[] dispatchQuantity, String[] billQuantity, String[] freightAmt, String[] otherAmt, String[] totalAmt) {
        Map map = new HashMap();
        int updateFreightAmountNew = 0;
        try {
            map.put("userId", userId);
            map.put("check", check);
            for (int i = 0; i < check.length; i++) {
                if ("1".equals(check[i])) {
                    if (Integer.parseInt(freightAmt[i]) > 0 && Integer.parseInt(totalAmt[i]) > 0) {
                        map.put("freightAmt", freightAmt[i]);
                        map.put("otherAmt", otherAmt[i]);
                        map.put("totalAmt", totalAmt[i]);
                        map.put("subLr", lrnNo[i]);
                        updateFreightAmountNew = (Integer) getSqlMapClientTemplate().update("wms.updateFreightAmountNew", map);
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateFreightAmountNew Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "updateFreightAmountNew", sqlException);
        }
        return updateFreightAmountNew;
    }

    public ArrayList imFreightDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList imFreightDetails = new ArrayList();

        try {
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("whId", wms.getWhId());
            if (wms.getStatus() == null) {
                map.put("status", "");
            } else {
                map.put("status", wms.getStatus());
            }
            System.out.println("map  " + map);
            imFreightDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imFreightDetails", map);
            System.out.println("imFreightDetails=== " + imFreightDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "dcFreightDetails", sqlException);
        }
        return imFreightDetails;

    }

    public int dzProductUpload(WmsTO wms) {
        Map map = new HashMap();

        int insertDealerList = 0;
        map.put("userId", wms.getUserId());
        map.put("productId", wms.getProductId());
        map.put("productVariantId", wms.getProductVariantId());
        map.put("productName", wms.getProductName());
        map.put("optionName", wms.getOptionName());
        map.put("optionValue", wms.getOptionValue());
        map.put("barCode", wms.getBarCode());
        map.put("hsnCode", wms.getHsnCode());
        map.put("categoryName", wms.getCategoryName());
        map.put("subCategoryName", wms.getSubCategoryName());

        System.out.println("map    " + map);

        try {
//                
            int code = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkExistDzProductUploadTemp", map);
            if (code == 0) {
                int code1 = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkExistDzProductUploadMain", map);
                if (code1 == 0) {
                    map.put("status", "0");
                } else {
                    map.put("status", "2");
                }
            } else {
                map.put("status", "1");
            }
            insertDealerList = (Integer) getSqlMapClientTemplate().insert("wms.insertDzProductUploadTemp", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return insertDealerList;

    }

    public ArrayList getdzProductUploadTemp(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getdzProductUploadTemp = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            getdzProductUploadTemp = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getdzProductUploadTemp", map);
            System.out.println("getdzProductUploadTemp========" + getdzProductUploadTemp.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getdzProductUploadTemp", sqlException);
        }
        return getdzProductUploadTemp;

    }

    public int delDzProductUploadTemp(WmsTO wms) {
        Map map = new HashMap();
        int delDzProductUploadTemp = 0;

        try {
            map.put("userId", wms.getUserId());
            delDzProductUploadTemp = (Integer) getSqlMapClientTemplate().update("wms.delDzProductUploadTemp", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return delDzProductUploadTemp;

    }

    public int dzInsertIntoProductMaster(WmsTO wms) {
        Map map = new HashMap();
        int insertStatus = 0;

        ArrayList getdzProductUploadTemp = new ArrayList();
        WmsTO wmsTO = new WmsTO();
        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            getdzProductUploadTemp = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getdzProductUploadTemp", map);
            Iterator itr = getdzProductUploadTemp.iterator();
            while (itr.hasNext()) {
                wmsTO = (WmsTO) itr.next();
                map.put("productId", wmsTO.getProductId());
                map.put("productVariantId", wmsTO.getProductVariantId());
                map.put("productName", wmsTO.getProductName());
                map.put("optionName", wmsTO.getOptionName());
                map.put("optionValue", wmsTO.getOptionValue());
                map.put("barCode", wmsTO.getBarCode());
                map.put("hsnCode", wmsTO.getHsnCode());
                map.put("categoryName", wmsTO.getCategoryName());
                map.put("subCategoryName", wmsTO.getSubCategoryName());
                map.put("status", wms.getStatus());
                String checkProductVariantId = wmsTO.getStatus();
                if ("0".equals(checkProductVariantId)) {
                    insertStatus = (Integer) getSqlMapClientTemplate().update("wms.dzInsertIntoProductMaster", map);
                }

            }

            map.put("userId", wms.getUserId());
            int delDzProductUploadTemp = (Integer) getSqlMapClientTemplate().update("wms.delDzProductUploadTemp", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public int delDzGrnDetailsTemp(WmsTO wms) {
        Map map = new HashMap();
        int delDzGrnDetailsTemp = 0;
        try {
            map.put("userId", wms.getUserId());
            delDzGrnDetailsTemp = (Integer) getSqlMapClientTemplate().update("wms.delDzGrnDetailsTemp", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "delDzGrnDetailsTemp", sqlException);
        }
        return delDzGrnDetailsTemp;

    }

    public int dzGrnUpload(WmsTO wms) {
        int insert = 0;
        Map map = new HashMap();
        map.put("userId", wms.getUserId());
        map.put("productLineEntryId", wms.getProductLineEntryId());
        map.put("productVariantId", wms.getProductVariantId());
        map.put("categoryName", wms.getCategoryName());
        map.put("productId", wms.getProductId());
        map.put("productFullName", wms.getName());
        map.put("orderedQty", wms.getOrderedQty());
        map.put("receivedQty", wms.getReceivedQty());
        map.put("diffQty", wms.getDiffQty());
        map.put("price", wms.getPrice());
        map.put("taxValue", wms.getTaxValue());
        map.put("poNo", wms.getPoNo());
        map.put("totalOrderedQty", wms.getTotalOrderedQty());
        map.put("totalReceivedQty", wms.getTotalReceivedQty());
        map.put("totalDiffQty", wms.getTotalDiffQty());
        map.put("taxVol", wms.getTaxVol());
        map.put("billToLocationId", wms.getBillToLocationId());
        map.put("shipToLocationId", wms.getShipToLocationId());
        map.put("billToLocationName", wms.getBillToLocationName());
        map.put("shipToLocationName", wms.getShipToLocationName());
        map.put("poDate", wms.getPoDate());
        map.put("poReferenceNumber", wms.getReferenceNo());
        map.put("poSupplierId", wms.getSupplierId());
        map.put("poSupplierName", wms.getSupplierName());
        map.put("poOrderedQty", wms.getOrderedQty1());
        map.put("poReceivedQty", wms.getReceivedQty1());
        map.put("status", wms.getStatus());

        System.out.println("map    " + map);

        if ("".equals(wms.getOrderedQty())) {
            map.put("orderedQty", 0);
        }

        if ("".equals(wms.getReceivedQty())) {
            map.put("receivedQty", 0);
        }

        if ("".equals(wms.getDiffQty())) {
            map.put("diffQty", 0);
        }

        if ("".equals(wms.getPrice())) {
            map.put("price", 0);
        }

        if ("".equals(wms.getTaxValue())) {
            map.put("taxValue", 0);
        }

        if ("".equals(wms.getTotalOrderedQty())) {
            map.put("totalOrderedQty", 0);
        }

        if ("".equals(wms.getTotalReceivedQty())) {
            map.put("totalReceivedQty", 0);
        }

        if ("".equals(wms.getTotalDiffQty())) {
            map.put("totalDiffQty", 0);
        }

        if ("".equals(wms.getTaxVol())) {
            map.put("taxVol", 0);
        }

        if ("".equals(wms.getSupplierName())) {
            map.put("poSupplierName", null);
        }

        if ("".equals(wms.getOrderedQty())) {
            map.put("poOrderedQty", "0");
        }

        if ("".equals(wms.getReceivedQty())) {
            map.put("poReceivedQty", "0");
        }

        try {
            if (wms.getReceivedQty() != "0") {

                int code = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkDzProductCode", map);
                if (code > 0) {
                    int code1 = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkExistDzPoNoMain", map);
                    if (code1 == 0) {
                        map.put("status", "0");
                    } else {
                        map.put("status", "2");
                    }
                } else {
                    map.put("status", "1");
                }

            } else {
                map.put("status", "3");
            }

            if (!"".equals(wms.getPoNo())) {
                insert = (Integer) getSqlMapClientTemplate().insert("wms.insertDzGrnDetailsTemp", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return insert;

    }

    public int delDzInvoiceDetailsTemp(WmsTO wms) {
        Map map = new HashMap();
        int delDzInvoiceDetailsTemp = 0;
        try {
            map.put("userId", wms.getUserId());
            delDzInvoiceDetailsTemp = (Integer) getSqlMapClientTemplate().update("wms.delDzInvoiceDetailsTemp", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "delDzGrnDetailsTemp", sqlException);
        }
        return delDzInvoiceDetailsTemp;

    }

    public int dzInvoiceUploadExcel(WmsTO wms) {
        int insert = 0;
        Map map = new HashMap();
        map.put("userId", wms.getUserId());
        map.put("productLineEntryId", wms.getProductLineEntryId());
        map.put("productVariantId", wms.getProductVariantId());
        map.put("categoryName", wms.getCategoryName());
//        map.put("productId", wms.getProductId());
        map.put("productCode", wms.getProductCode());
        map.put("productFullName", wms.getName());
        map.put("sendQty", wms.getOrderedQty());
        map.put("receivedQty", wms.getReceivedQty());
        map.put("diffQty", wms.getDiffQty());
        map.put("price", wms.getPrice());
        map.put("sendAmount", wms.getSendAmt());
        map.put("receivedAmount", wms.getReceivedAmt());
        map.put("toNo", wms.getInvoiceNo());
        map.put("toSendQty", wms.getTotalOrderedQty());
        map.put("toReceivedQty", wms.getTotalReceivedQty());
        map.put("toSendAmount", wms.getTotalSendAmt());
        map.put("toReceivedAmount", wms.getTotalReceivedAmt());
        map.put("fromLocationId", wms.getFromLocation());
        map.put("fromLocationName", wms.getFromWhName());
        map.put("toLocationId", wms.getToLocation());
        map.put("toLocationName", wms.getToWhName());
        map.put("whId", wms.getWhId());

        System.out.println("map    " + map);

        if ("".equals(wms.getOrderedQty())) {
            map.put("orderedQty", 0);
        }

        if ("".equals(wms.getReceivedQty())) {
            map.put("receivedQty", 0);
        }

        if ("".equals(wms.getSendAmt())) {
            map.put("sendAmount", 0);
        }

        if ("".equals(wms.getReceivedAmt())) {
            map.put("receivedAmount", 0);
        }

        if ("".equals(wms.getTotalOrderedQty())) {
            map.put("toSendQty", 0);
        }

        if ("".equals(wms.getTotalReceivedQty())) {
            map.put("toReceivedQty", 0);
        }

        if ("".equals(wms.getTotalSendAmt())) {
            map.put("toSendAmount", 0);
        }

        if ("".equals(wms.getTotalReceivedAmt())) {
            map.put("toReceivedAmount", 0);
        }

        if ("".equals(wms.getDiffQty())) {
            map.put("diffQty", 0);
        }

        if ("".equals(wms.getPrice())) {
            map.put("price", 0);
        }

        try {
            int code = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkDzProductCode", map);
            if (code > 0) {
                int code1 = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkExistDzToNo", map);
                if (code1 == 0) {
                    int checkDzDealerCode = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkDzDealerCode", map);
                    if (checkDzDealerCode > 0) {
                        int checkStockAvailable = (Integer) getSqlMapClientTemplate().queryForObject("wms.getDzCheckStock", map);

                        if (checkStockAvailable == 0) {
                            map.put("status", "0");
                        } else {
                            map.put("status", "4");
                        }

                    } else {
                        map.put("status", "3");
                    }
                } else {
                    map.put("status", "2");
                }
            } else {
                map.put("status", "1");
            }

            if (!"".equals(wms.getInvoiceNo())) {
                insert = (Integer) getSqlMapClientTemplate().insert("wms.insertDzInvoiceTemp", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return insert;

    }

    public ArrayList getDzInvoiceTemp(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getDzInvoiceTemp = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            getDzInvoiceTemp = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzInvoiceTemp", map);

            System.out.println("getDzInvoiceTemp========" + getDzInvoiceTemp.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getdzProductUploadTemp", sqlException);
        }
        return getDzInvoiceTemp;

    }

    public ArrayList getdzGrnDetailsTemp(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getdzProductUploadTemp = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            getdzProductUploadTemp = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzGrnDetailsTemp", map);

            System.out.println("getdzProductUploadTemp========" + getdzProductUploadTemp.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getdzProductUploadTemp", sqlException);
        }
        return getdzProductUploadTemp;

    }

    public int insertDzGrnUpload(WmsTO wmsTO1, SqlMapClient session) {
        Map map = new HashMap();
        int insertStatus = 0;

        ArrayList getdzProductUploadTemp = new ArrayList();
        ArrayList getdzProductUploadTempData = new ArrayList();
        map.put("userId", wmsTO1.getUserId());
        map.put("whId", wmsTO1.getWhId());
        WmsTO wmsTO = new WmsTO();
        try {

            getdzProductUploadTemp = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzGrnDetailsTemp1", map);
            Iterator itr = getdzProductUploadTemp.iterator();
            while (itr.hasNext()) {
                wmsTO = (WmsTO) itr.next();
                map.put("orderedQty", wmsTO.getOrderedQty());
                map.put("receivedQty", wmsTO.getReceivedQty());
                map.put("diffQty", wmsTO.getDiffQty());
                map.put("price", wmsTO.getPrice());
                map.put("taxValue", wmsTO.getTaxValue());
                map.put("poNo", wmsTO.getPoNo());
                map.put("totalOrderedQty", wmsTO.getTotalOrderedQty());
                map.put("totalReceivedQty", wmsTO.getTotalReceivedQty());
                map.put("totalDiffQty", wmsTO.getTotalDiffQty());

                System.out.println(wmsTO.getUserId() + "this value is to check userId");

                int GRNNo = 0;
                GRNNo = (Integer) session.queryForObject("wms.getDzGRNCode", map);
                System.out.println("grnNo-----" + GRNNo);

                String GRNNoSequence = "" + GRNNo;

                if (GRNNoSequence == null) {
                    GRNNoSequence = "00001";
                } else if (GRNNoSequence.length() == 1) {
                    GRNNoSequence = "0000" + GRNNoSequence;
                } else if (GRNNoSequence.length() == 2) {
                    GRNNoSequence = "000" + GRNNoSequence;
                } else if (GRNNoSequence.length() == 3) {
                    GRNNoSequence = "00" + GRNNoSequence;
                } else if (GRNNoSequence.length() == 4) {
                    GRNNoSequence = "0" + GRNNoSequence;
                } else if (GRNNoSequence.length() == 5) {
                    GRNNoSequence = "" + GRNNoSequence;
                }

                String grnNo = "GRN/2021/" + GRNNoSequence;
                map.put("grnNo", grnNo);
                System.out.println("grnNo---" + grnNo);
                System.out.println(map + "map");
                System.out.println("totalOrderedQty" + wmsTO.getTotalOrderedQty());
                System.out.println(getdzProductUploadTemp.size() + "size of value");
                insertStatus = (Integer) getSqlMapClientTemplate().update("wms.dzInsertGrnMaster", map);

            }
            getdzProductUploadTempData = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzGrnDetailsTemp", map);
            Iterator itr1 = getdzProductUploadTempData.iterator();
            WmsTO wms = new WmsTO();
            while (itr1.hasNext()) {
                wms = (WmsTO) itr1.next();
                map.put("productLineEntryId", wms.getProductLineEntryId());
                map.put("productVariantId", wms.getProductVariantId());
                map.put("categoryName", wms.getCategoryName());
                map.put("productId", wms.getProductId());
                map.put("productFullName", wms.getName());
                map.put("orderedQty", wms.getOrderedQty());
                map.put("receivedQty", wms.getReceivedQty());
                map.put("diffQty", wms.getDiffQty());
                map.put("price", wms.getPrice());
                map.put("taxValue", wms.getTaxValue());
                map.put("poNo", wms.getPoNo());
                map.put("totalOrderedQty", wms.getTotalOrderedQty());
                map.put("totalReceivedQty", wms.getTotalReceivedQty());
                map.put("totalDiffQty", wms.getTotalDiffQty());
                map.put("taxVol", wms.getTaxVol());
                map.put("billToLocationId", wms.getBillToLocationId());
                map.put("shipToLocationId", wms.getShipToLocationId());
                map.put("billToLocationName", wms.getBillToLocationName());
                map.put("shipToLocationName", wms.getShipToLocationName());
                map.put("poDate", wms.getPoDate());
                map.put("poReferenceNumber", wms.getReferenceNo());
                map.put("poSupplierId", wms.getSupplierId());
                map.put("poSupplierName", wms.getSupplierName());
                map.put("poOrderedQty", wms.getOrderedQty1());
                map.put("poReceivedQty", wms.getReceivedQty1());
                map.put("status", wms.getStatus());

                System.out.println(wms.getStatus() + " this value is for status " + wms.getPoNo());
                if ("0".equals(wms.getStatus())) {
                    int GRNID = 0;
                    GRNID = (Integer) session.queryForObject("wms.getDzGrnId", map);
                    map.put("grnId", GRNID);
                    System.out.println(GRNID + "this value is for grnId");

                    insertStatus = (Integer) getSqlMapClientTemplate().update("wms.insertDzGrnDetails", map);
                }

            }
            map.put("userId", wmsTO1.getUserId());
            int delDzProductUploadTemp = (Integer) getSqlMapClientTemplate().update("wms.delDzGrnDetailsTemp", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public int dzInvoiceUpload(WmsTO wmsTO, SqlMapClient session) {
        Map map = new HashMap();
        int insertStatus = 0;

        ArrayList getDzInvoiceTemp = new ArrayList();
        ArrayList getdzInvoiceUploadTemp = new ArrayList();

        try {
            map.put("userId", wmsTO.getUserId());
            map.put("whId", wmsTO.getWhId());

            WmsTO wmsTO2 = new WmsTO();
            WmsTO wms = new WmsTO();
            getdzInvoiceUploadTemp = (ArrayList) session.queryForList("wms.getDzInvoiceDetailsTemp", map);
            Iterator itr = getdzInvoiceUploadTemp.iterator();
            while (itr.hasNext()) {
                int checkDzInvoiceIdExists = 0;
                wmsTO2 = (WmsTO) itr.next();
                map.put("invoiceNo", wmsTO2.getInvoiceNo());
                map.put("toLocation", wmsTO2.getToLocation());
                map.put("toWhName", wmsTO2.getToWhName());
                map.put("status", 0);
                checkDzInvoiceIdExists = (Integer) session.queryForObject("wms.checkDzInvoiceIdExists", map);
                System.out.println("my code   " + map);
                if (checkDzInvoiceIdExists == 0) {
                    System.out.println("my code   success");
                    int PICKNO = 0;
                    PICKNO = (Integer) session.queryForObject("wms.getDzPicklistCode", map);
                    String PICKNoSequence = "" + PICKNO;

                    if (PICKNoSequence == null) {
                        PICKNoSequence = "00001";
                    } else if (PICKNoSequence.length() == 1) {
                        PICKNoSequence = "0000" + PICKNoSequence;
                    } else if (PICKNoSequence.length() == 2) {
                        PICKNoSequence = "000" + PICKNoSequence;
                    } else if (PICKNoSequence.length() == 3) {
                        PICKNoSequence = "00" + PICKNoSequence;
                    } else if (PICKNoSequence.length() == 4) {
                        PICKNoSequence = "0" + PICKNoSequence;
                    } else if (PICKNoSequence.length() == 5) {
                        PICKNoSequence = "" + PICKNoSequence;
                    }

                    String pickNo = "HSINV/2021/" + PICKNoSequence;
                    map.put("pickNo", pickNo);
                    insertStatus = (Integer) session.update("wms.insertDzInvoiceMaster", map);
                }
            }
            map.put("checkStatus", "10");

            getDzInvoiceTemp = (ArrayList) session.queryForList("wms.getDzInvoiceTemp", map);

            Iterator itr1 = getDzInvoiceTemp.iterator();
            while (itr1.hasNext()) {
                wms = (WmsTO) itr1.next();
                map.put("productLineEntryId", wms.getProductLineEntryId());
                map.put("productVariantId", wms.getProductVariantId());
                map.put("categoryName", wms.getCategoryName());
                map.put("productCode", wms.getProductCode());
                map.put("productId", wms.getProductId());
                map.put("productFullName", wms.getProductName());
                map.put("sendQty", wms.getOrderedQty());
                map.put("receivedQty", wms.getReceivedQty());
                map.put("diffQty", wms.getDiffQty());
                map.put("price", wms.getPrice());
                map.put("sendAmount", wms.getSendAmt());
                map.put("receivedAmount", wms.getReceivedAmt());
                map.put("toNo", wms.getInvoiceNo());
                map.put("toSendQty", wms.getTotalOrderedQty());
                map.put("toReceivedQty", wms.getTotalReceivedQty());
                map.put("toSendAmount", wms.getTotalSendAmt());
                map.put("toReceivedAmount", wms.getTotalReceivedAmt());
                map.put("fromLocationId", wms.getFromLocation());
                map.put("fromLocationName", wms.getFromWhName());
                map.put("toLocationId", wms.getToLocation());
                map.put("toLocationName", wms.getToWhName());

                if ("".equals(wms.getOrderedQty())) {
                    map.put("orderedQty", 0);
                }

                if ("".equals(wms.getReceivedQty())) {
                    map.put("receivedQty", 0);
                }

                if ("".equals(wms.getSendAmt())) {
                    map.put("sendAmount", 0);
                }

                if ("".equals(wms.getReceivedAmt())) {
                    map.put("receivedAmount", 0);
                }
                if ("".equals(wms.getTotalOrderedQty())) {
                    map.put("toSendQty", 0);
                }
                if ("".equals(wms.getTotalReceivedQty())) {
                    map.put("toReceivedQty", 0);
                }
                if ("".equals(wms.getTotalSendAmt())) {
                    map.put("toSendAmount", 0);
                }
                if ("".equals(wms.getTotalReceivedAmt())) {
                    map.put("toReceivedAmount", 0);
                }
                if ("".equals(wms.getDiffQty())) {
                    map.put("diffQty", 0);
                }
                if ("".equals(wms.getPrice())) {
                    map.put("price", 0);
                }
                int invoiceId = 0;
                System.out.println("map   check  " + map);
                System.out.println("status   check  " + wms.getStatus());
                invoiceId = (Integer) session.queryForObject("wms.getDzInvoiceNo", map);
                map.put("invoiceId", invoiceId);

                System.out.println(" Check Invoice Number " + invoiceId);
                insertStatus = (Integer) session.insert("wms.insertDzInvoiceDetails", map);

                wms.setDetailId(insertStatus + "");
                updateDzPicklist(wms, session, wmsTO);
            }

            map.put("userId", wmsTO.getUserId());
            int delDzInvoiceDetailsTemp = (Integer) session.update("wms.delDzInvoiceDetailsTemp", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public int updateDzPicklist(WmsTO wms, SqlMapClient session, WmsTO wms2) {
        int update = 0;
        HashMap map = new HashMap();
        try {
            String productId = wms.getProductId();
            Double sendQty = Double.parseDouble(wms.getOrderedQty());
            Double tempQty = Double.parseDouble(wms.getOrderedQty());
            Double getQty = 0.00;
            map.put("invoiceDetailId", wms.getDetailId());
            map.put("productId", wms.getItemId());
            map.put("productCode", wms.getProductVariantId());
            map.put("whId", wms2.getWhId());

            System.out.println(" ------> productId =  " + map);
            WmsTO wms1 = new WmsTO();
            while (tempQty > 0) {
                System.out.println("tempQTy == " + tempQty);
                ArrayList getDzBinData = (ArrayList) session.queryForList("wms.getDzBinData", map);
                Iterator itr = getDzBinData.iterator();
                if (getDzBinData.size() > 0) {
                    while (itr.hasNext()) {
                        wms1 = (WmsTO) itr.next();
                        System.out.println("mapp  " + map);
                        System.out.println("first test" + "-----> " + (tempQty - Double.parseDouble(wms1.getStockQty())));
                        Double stockQty = 0.00;

                        if ((tempQty - Double.parseDouble(wms1.getStockQty())) >= 0) {
                            System.out.println("second test");
                            stockQty = Double.parseDouble(wms1.getStockQty());
                            tempQty = tempQty - Double.parseDouble(wms1.getStockQty());
                            map.put("id", wms1.getId());
                            map.put("balance", 0);
                            map.put("usedQty", stockQty);
                            map.put("status", 2);
                            map.put("scannedCode", wms1.getScannedCode());
                            int insertProductBinDetails = (Integer) session.update("wms.dzUpdateProductBin", map);
                            //insert into picklist
                            int insertPickList = (Integer) session.update("wms.dzPicklistUpdate", map);

                            System.out.println("tempqty==Arun Insert--- >" + tempQty);
//                            if (tempQty == 0) {
                            map.put("status", 1);
                            int insertDzInvoiceStatus = (Integer) session.update("wms.dzInvoiceDetailsStatus", map);
//                            }
                            System.out.println("null " + null);
                        } else {
                            System.out.println("third test");
                            stockQty = tempQty;
                            Double remainQty = Double.parseDouble(wms1.getStockQty()) - tempQty;
                            tempQty = 0.00;
                            map.put("id", wms1.getId());
                            map.put("scannedCode", wms1.getScannedCode());
                            map.put("balance", remainQty);
                            map.put("usedQty", stockQty);
                            map.put("status", 1);
                            int insertProductBinDetails = (Integer) session.update("wms.dzUpdateProductBin", map);
                            int insertPickList = (Integer) session.update("wms.dzPicklistUpdate", map);
                            //insert into picklist

                            System.out.println("tempqtyArun==" + tempQty);
                            map.put("status", 1);
                            if (tempQty == 0) {
                                int insertDzInvoiceStatus = (Integer) session.update("wms.dzInvoiceDetailsStatus", map);
                            }
                        }

                        System.out.println("tempQTyfinal == " + tempQty);
                    }
                } else {
                    tempQty = 0.00;
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return update;
    }

    public ArrayList getdzGrnUpload(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getdzGrnUpload = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("grnId", wms.getGrnId());
            getdzGrnUpload = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getdzGrnUploadView", map);
            System.out.println("getdzGrnUpload========" + getdzGrnUpload.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getdzGrnUpload", sqlException);
        }
        return getdzGrnUpload;

    }

    public ArrayList getdzInvoiceUpload(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getdzInvoiceUpload = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());

            getdzInvoiceUpload = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzInvoice", map);

            System.out.println("getdzInvoiceUpload========" + getdzInvoiceUpload.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getdzInvoiceUpload", sqlException);
        }
        return getdzInvoiceUpload;

    }

    public ArrayList getDzGrnDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getDzGrnDetails = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("grnId", wms.getGrnId());
            
            System.out.println("id---------"+wms.getGrnId());

            getDzGrnDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzGrnDetails", map);

            System.out.println("getDzGrnDetails========" + getDzGrnDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getdzInvoiceUpload", sqlException);
        }
        return getDzGrnDetails;

    }

    public ArrayList getDzStockDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getStockDetails = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            System.out.println("==================================<><><>" + wms.getFromDate());
            System.out.println("==================================<><><>" + wms.getToDate());

            getStockDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzStockDetails", map);
            System.out.println("getStockDetails========" + getStockDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getStockDetails", sqlException);
        }
        return getStockDetails;

    }

    public int insertDailyOutward(WmsTO wmsTO) {
        Map map = new HashMap();
        int insertDailyOutward = 0;
        try {
//            String materialId= "";
            map.put("whId", wmsTO.getWhId());
            map.put("date", wmsTO.getDate());
            map.put("packets", wmsTO.getPackets());
            map.put("tons", wmsTO.getTons());
            map.put("milk", wmsTO.getMilk());
            map.put("fandv", wmsTO.getFandv());
            map.put("userId", wmsTO.getUserId());

            System.out.println("MAP---" + map);

            insertDailyOutward = (Integer) getSqlMapClientTemplate().update("wms.insertDailyOutward", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return insertDailyOutward;

    }

    public ArrayList getDailyOutWard(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList getDailyOutWard = new ArrayList();
        try {

            getDailyOutWard = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDailyOutWard", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return getDailyOutWard;

    }

    public ArrayList imDailyLUDetails(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList getDailyOutWard = new ArrayList();
        try {

            getDailyOutWard = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDailyLottingAndUnloadingDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return getDailyOutWard;

    }

    public int insertimDailyLUDetails(WmsTO wmsTO) {
        Map map = new HashMap();
        int insertimDailyLUDetails = 0;
        try {
//            String materialId= "";
            map.put("type", wmsTO.getType());
            map.put("date", wmsTO.getDate());
            map.put("remarks", wmsTO.getRemarks());
            map.put("grnQty", wmsTO.getGrnQty());
            map.put("userId", wmsTO.getUserId());

            System.out.println("MAP---" + map);

            insertimDailyLUDetails = (Integer) getSqlMapClientTemplate().update("wms.insertimDailyLUDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return insertimDailyLUDetails;

    }

    public ArrayList dzPickListMaster(WmsTO wms) {
        Map map = new HashMap();
        ArrayList dzPickListMaster = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            dzPickListMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzInvoiceMasterPick", map);

            System.out.println("dzPickListMaster========" + dzPickListMaster.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "dzPickListMaster", sqlException);
        }
        return dzPickListMaster;

    }

    public ArrayList dzPickListMasterPick(WmsTO wms) {
        Map map = new HashMap();
        ArrayList dzPickListMaster = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            dzPickListMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzInvoiceMasterPicks", map);

            System.out.println("dzPickListMaster========" + dzPickListMaster.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "dzPickListMaster", sqlException);
        }
        return dzPickListMaster;

    }

    public ArrayList dzPickListMasterView(WmsTO wms) {
        Map map = new HashMap();
        ArrayList dzPickListMaster = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("invoiceId", wms.getInvoiceId());
            System.out.println("inVoice + ===" + wms.getInvoiceId());
            dzPickListMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzInvoiceDetailsPick", map);
            System.out.println("getDzInvoiceDetailsPick ========" + dzPickListMaster.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "dzPickListMaster", sqlException);
        }
        return dzPickListMaster;

    }

    public ArrayList getDzLrInvoiceDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getDzLrInvoiceDetails = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("invoiceId", wms.getInvoiceId());
            System.out.println("getDzLrInvoiceDetails map ===" + map);
            getDzLrInvoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzLrInvoiceDetails", map);
            System.out.println("getDzLrInvoiceDetails ========" + getDzLrInvoiceDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "dzPickListMaster", sqlException);
        }
        return getDzLrInvoiceDetails;

    }

    public ArrayList getAllDzGrnDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList dzPickListMaster = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("invoiceId", wms.getInvoiceId());
            System.out.println("inVoice + ===" + wms.getInvoiceId());

            dzPickListMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getAllDzGrnDetails", map);

            System.out.println("getAllDzGrnDetails ========" + dzPickListMaster.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "dzPickListMaster", sqlException);
        }
        return dzPickListMaster;

    }

    public ArrayList getCratesReportDealerBased(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getCratesReportDealerBased = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("invoiceId", wms.getInvoiceId());
            System.out.println("inVoice + ===" + wms.getInvoiceId());

            getCratesReportDealerBased = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getCratesReportDealerBased", map);

            System.out.println("getCratesReportDealerBased ========" + getCratesReportDealerBased.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "dzPickListMaster", sqlException);
        }
        return getCratesReportDealerBased;

    }

    public int insertDZLrStatus(WmsTO wmsTO, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int uploadConnectGrn = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
//        int pincode = 0;
        map.put("userId", wmsTO.getUserId());
        map.put("whId", wmsTO.getWhId());

        try {

            int lrNumbercheck = 0;
            String hubCode = (String) session.queryForObject("wms.getHubCode", map);
            if (hubCode == null || "".equals(hubCode)) {
                hubCode = "HSC";
            }
            Date date = new Date();
            String str = new SimpleDateFormat("yyyy-MM-dd").format(date);
            String[] date1 = str.split("-");
            map.put("month", date1[1]);
            String accYear = ThrottleConstants.accountYear;
            String seq = "HSC" + accYear + hubCode + date1[1];
            map.put("lrnew", seq);
            int length = seq.length();
            map.put("length", length);
            int lrNumberCount = (Integer) session.queryForObject("wms.getDzLrSequence", map);
            lrNumbercheck = lrNumberCount;
            System.out.println("lrNo-----" + lrNumbercheck);

            String lrSequence = "" + lrNumbercheck;

            if (lrSequence == null) {
                lrSequence = "0001";
            } else if (lrSequence.length() == 1) {
                lrSequence = "000" + lrSequence;
            } else if (lrSequence.length() == 2) {
                lrSequence = "00" + lrSequence;
            } else if (lrSequence.length() == 3) {
                lrSequence = "0" + lrSequence;
            } else if (lrSequence.length() == 4) {
                lrSequence = "" + lrSequence;
            }
            String lrNumber = "HSC" + accYear + hubCode + date1[1] + lrSequence;
            map.put("lrNo", lrNumber);
            map.put("lrDate", str);
            map.put("supplierId", wmsTO.getSupplierId());
            map.put("vendorId", wmsTO.getVendorId());
            map.put("deviceId", wmsTO.getDeviceId());
            map.put("vehicleNo", wmsTO.getVehicleNo());
            map.put("noOfCrates", wmsTO.getNoOfCrates());
            map.put("vehicleType", wmsTO.getVehicleType());
            map.put("driverName", wmsTO.getDriverName());
            map.put("driverMobile", wmsTO.getMobile());
            map.put("dispatchDate", wmsTO.getDate());
            map.put("tripStartTime", wmsTO.getTime());
            map.put("remarks", wmsTO.getRemarks());
            map.put("startKm", wmsTO.getStartKm());

            System.out.println("mappint    " + map);
//
            int lrId = (Integer) session.insert("wms.insertDzLrMaster", map);
            int lrDetailId = 0;
            ArrayList main1 = new ArrayList();
            ArrayList toNoList = new ArrayList();
            ArrayList idList = new ArrayList();

            String num = wmsTO.getPicklistIds();
            System.out.println("num   " + num);
            String str1[] = num.split(",");
            List<String> al = new ArrayList<String>();
            al = Arrays.asList(str1);
            for (String s : al) {
                main1.add(s);
            }

            for (int i = 0; i < main1.size(); i++) {

                String num2 = main1.get(i) + "";
                String str2[] = num2.split("~");

                toNoList.add(str2[0]);
                idList.add(str2[1]);

            }
            int sublrSequence = 0;
            for (int i = 0; i < toNoList.size(); i++) {
                map.put("lrId", lrId);
                map.put("tripId", lrId);
                map.put("toNo", toNoList.get(i));
                map.put("id", idList.get(i));

                ArrayList getDzInvoiceDetailsLr = (ArrayList) session.queryForList("wms.getDzInvoiceDetailsLr", map);

                Iterator itr1 = getDzInvoiceDetailsLr.iterator();
                while (itr1.hasNext()) {
                    wmsTO = (WmsTO) itr1.next();
                    map.put("invoiceId", wmsTO.getInvoiceId());
                    map.put("invoiceDate", wmsTO.getCreatedOn());
                    map.put("dealerId", wmsTO.getDealerId());
                    map.put("qty", wmsTO.getQty());
                    map.put("dispatchQty", wmsTO.getQty());
                    map.put("noOfCrates", wmsTO.getNoOfCrates());
                    map.put("invoiceAmount", wmsTO.getAmount());
                    int checkDzSublrId = (Integer) session.queryForObject("wms.checkDzSublrId", map);
                    if (checkDzSublrId == 0) {
                        sublrSequence++;
                        String sublr = lrNumber + "-" + sublrSequence;
                        map.put("sublr", sublr);
                        int sublrId = (Integer) session.insert("wms.insertDzSublrId", map);
                        map.put("sublrId", sublrId);
                    } else {
                        String getDzSubLrCode = (String) session.queryForObject("wms.getDzSubLrCode", map);
                        map.put("sublr", getDzSubLrCode);
                        map.put("sublrId", checkDzSublrId);
                    }
                    int insertDzLrDetails = (Integer) session.insert("wms.insertDzLrDetails", map);
                    int updatePickDetailsStatus = (Integer) session.update("wms.updatePickDetailsStatus", map);
                    int updatePickDetailsStock = (Integer) session.update("wms.updatePickDetailsStock", map);
                    int updatePickDetailsProductBinDetails = (Integer) session.update("wms.updatePickDetailsProductBinDetails", map);
                    int updateDzPickListStatus = (Integer) session.update("wms.updatePickDetailsProductBinDetailsPickStatus1", map);
                    updateDzPickListStatus = (Integer) session.update("wms.updateDzPickListStatus", map);
//                    int 

                }

            }
            System.out.println("lrId-----" + lrId);

            //rohan
            int status = lrId;

            int startId = 0;
            String src = (String) session.queryForObject("wms.getWhLatLong", map);
            map.put("src", src);
            map.put("dest", src);
            String srcName = (String) session.queryForObject("trip.getTripSourceName", map);
            map.put("srcName", srcName);
            map.put("destName", srcName);
            map.put("custId", "6");
            String device = wmsTO.getDeviceId();
            if (!"".equals(device) && device != null) {

                String deviceId = device.split("-")[0];
                String deviceNo = device.split("-")[1];
                map.put("deviceId", deviceId);
                map.put("deviceNo", deviceNo);
                startId = (Integer) session.insert("trip.insertTripStartApiMaster", map);
                map.put("startId", startId);

                /////////////////////
                for (int i = 0; i < wmsTO.getInvoiceNoE().length; i++) {

                    String pincode = (String) session.queryForObject("wms.getInstaMartPincode", map);
                    String latLong = (String) session.queryForObject("wms.getInstaMartLatLong", map);
                    if (!"".equals(latLong) && latLong != null && latLong.split("-").length == 2) {
                        map.put("latitude", latLong.split("-")[0]);
                        map.put("longitude", latLong.split("-")[1]);
                        map.put("clientName", pincode.split("~")[1]);
                        int insertTripStartApiDetails = (Integer) session.insert("trip.insertTripStartApiDetails", map);
                    } else if (pincode != null && startId != 0) {
                        map.put("latitude", "");
                        map.put("longitude", "");
                        map.put("clientName", pincode.split("~")[0]);
                        int insertTripStartApiDetails = (Integer) session.insert("trip.insertTripStartApiDetails", map);
                    }

                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadNormalOrder", sqlException);
        }
        return uploadConnectGrn;
    }

    public ArrayList getDzLrMaster(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getDzLrMaster = new ArrayList();
        try {
            map.put("whId", wmsTo.getWhId());
            map.put("lrId", wmsTo.getLrId());
            map.put("fromDate", wmsTo.getFromDate());
            map.put("toDate", wmsTo.getToDate());
            map.put("whId", wmsTo.getWhId());
            System.out.println(wmsTo.getLrId() + "testing === >" + map);
            getDzLrMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzLrMaster", map);
            System.out.println("getDzLrMaster()-----" + getDzLrMaster.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getDzLrMaster;
    }

    public ArrayList getDzInvoiceLrDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getDzInvoiceLrDetails = new ArrayList();
        try {
            map.put("whId", wmsTo.getWhId());
            map.put("lrId", wmsTo.getLrId());
            getDzInvoiceLrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzInvoiceLrDetails", map);
            System.out.println("getDzInvoiceLrDetails()-----" + getDzInvoiceLrDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getDzInvoiceLrDetails;
    }

    public ArrayList getDzLrDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getDzLrDetails = new ArrayList();

        try {
            map.put("lrId", wms.getLrId());
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("whId", wms.getWhId());
            System.out.println(wms.getLrId() + "testing === >" + map);
            getDzLrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzLrDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getDzLrDetails;

    }

    public ArrayList dzSubLrDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList dzSubLrDetails = new ArrayList();

        try {
            map.put("lrId", wms.getLrId());
            dzSubLrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.dzSubLrDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return dzSubLrDetails;

    }

    public ArrayList dzCratesReportView(WmsTO wms) {
        Map map = new HashMap();
        ArrayList dzCratesReportView = new ArrayList();
        try {
            map.put("sublrId", wms.getSubLrId());
            System.out.println("map===" + map);
            dzCratesReportView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.dzCratesReportView", map);
            System.out.println("cratesReportView()-----" + dzCratesReportView.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return dzCratesReportView;
    }

    public ArrayList dzCratesReport(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList cratesReport = new ArrayList();
        try {
            map.put("whId", wmsTo.getWhId());
            map.put("fromDate", wmsTo.getFromDate());
            map.put("toDate", wmsTo.getToDate());
            cratesReport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.dzCratesReport", map);
            System.out.println("cratesReport()-----" + cratesReport.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return cratesReport;
    }

    public ArrayList getImPodDeatails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImPodDeatails = new ArrayList();

        try {
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
//              if (wms.getStatusType() == null) {
//                  map.put("statusType", "0");
//              } else {
//                  map.put("statusType", wms.getStatusType());
//              }
//              if (wms.getWarehouseId() != null && "".equals(wms.getWarehouseId())) {
//                  map.put("whId", wms.getWarehouseId());
//              } else {
//                  map.put("whId", wms.getWhId());
//              }
            System.out.println("map  " + map);
            getImPodDeatails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImPodDeatails", map);
            System.out.println("podUploadDetails=== " + getImPodDeatails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return getImPodDeatails;

    }

    public ArrayList dzFreightDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList dzFreightDetails = new ArrayList();

        try {
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("whId", wms.getWhId());
            if (wms.getStatus() == null) {
                map.put("status", "");
            } else {
                map.put("status", wms.getStatus());
            }
            System.out.println("map  " + map);
            dzFreightDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.dzFreightDetails", map);
            System.out.println("imFreightDetails=== " + dzFreightDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "dcFreightDetails", sqlException);
        }
        return dzFreightDetails;

    }

    public int dzUpdateFreightAmountNew(int userId, String[] check, String[] lrnNo, String[] lrNo, String[] vendorName, String[] customerName, String[] dispatchQuantity, String[] billQuantity, String[] freightAmt, String[] otherAmt, String[] totalAmt) {
        Map map = new HashMap();
        int dzUpdateFreightAmountNew = 0;
        try {
            map.put("userId", userId);
            map.put("check", check);
            for (int i = 0; i < check.length; i++) {
                if ("1".equals(check[i])) {
                    map.put("freightAmt", freightAmt[i]);
                    map.put("otherAmt", otherAmt[i]);
                    map.put("totalAmt", totalAmt[i]);
                    map.put("subLr", lrnNo[i]);
                    dzUpdateFreightAmountNew = (Integer) getSqlMapClientTemplate().update("wms.dzUpdateFreightAmountNew", map);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateFreightAmountNew Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "updateFreightAmountNew", sqlException);
        }
        return dzUpdateFreightAmountNew;
    }

    public ArrayList imPodUploadDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList imPodUploadDetails = new ArrayList();

        try {
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("podStatus", wms.getPodStatus());
            if (wms.getStatusType() == null) {
                map.put("statusType", "0");
            } else {
                map.put("statusType", wms.getStatusType());
            }

            map.put("whId", wms.getWhId());

            System.out.println("map  " + map);
            imPodUploadDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imPodUploadDetails", map);
            System.out.println("imPodUploadDetails=== " + imPodUploadDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "imPodUploadDetails", sqlException);
        }
        return imPodUploadDetails;

    }

    public int processInsertImPODDetails(WmsTO wmsTO, int UserId, String[] actualFilePath, String[] fileSaved, String[] remarks) {

        Map map = new HashMap();
        FileInputStream fis = null;
        int tripId = 0;
        int vendorId = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        try {

//            System.out.println("actualFilePath[0] = " + actualFilePath[0]);
//            System.out.println("actualFilePath.length = " + actualFilePath.length);
            for (int x = 0; x < 3; x++) {
//                System.out.println("fileSaved[x] = " + fileSaved[x]);
                if (fileSaved[x] != null) {
                    map.put("fileName" + x, fileSaved[x]);
//                    System.out.println("fileName map = " + map);
//                    System.out.println("actualFilePath = " + actualFilePath[x]);
                    File file = new File(actualFilePath[x]);
//                    System.out.println("file = " + file.toString());
                    map.put("podfiles" + x, file.toString());
//                    System.out.println("podfile map = " + map);
//                    System.out.println("the saveTripPodDetails123455" + map);
                } else {
                    map.put("fileName" + x, null);
                    map.put("podfiles" + x, null);
                }
            }

//            System.out.println("Tesiting = ");
            map.put("whId", wmsTO.getWhId());
            map.put("userId", UserId);
            map.put("podname0", wmsTO.getPodName1());
            map.put("podname1", wmsTO.getPodName2());
            map.put("podname2", wmsTO.getPodName3());
            map.put("remarks0", wmsTO.getRemarks1());
            map.put("remarks1", wmsTO.getRemarks2());
            map.put("remarks2", wmsTO.getRemarks3());
            map.put("dispatchDetailId", wmsTO.getDispatchDetailId());
            map.put("Dispatchid", wmsTO.getDispatchid());
            map.put("PlannedTime", wmsTO.getPlannedTime() + "00:00:00");
            map.put("PlannedDate", wmsTO.getDeliveryDate());
            map.put("LrnNo", wmsTO.getLrnNo());
            map.put("SubLrId", wmsTO.getSubLrId());
            System.out.println("maprr>>>>>>>>>>>>>>>=>=>=>= > = >-) - = - > - = - >" + map);

            int status = 0;
            int mfrstatus = 0;

            String code = "";
            String[] temp;
            int vehicle_id = 0;
            int VendorLedgerId = 0;
            int podId = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkImPod", map);
            if (podId == 0) {
                status = (Integer) getSqlMapClientTemplate().insert("wms.insertImPOD", map);
            } else {
                map.put("podId", podId);
                status = (Integer) getSqlMapClientTemplate().update("wms.updateImPod", map);
            }

            if (status > 0) {
                int completeStatus = (Integer) getSqlMapClientTemplate().update("wms.imPodCompleteStatus", map);
                System.out.println("statusstatus" + status + "id" + wmsTO.getSubLrId());
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return vendorId;

    }

    public ArrayList getDzInvoiceTempCount(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getDzInvoiceTempCount = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            getDzInvoiceTempCount = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzInvoiceTempCount", map);

            System.out.println("getDzInvoiceTempCount========" + getDzInvoiceTempCount.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getdzProductUploadTemp", sqlException);
        }
        return getDzInvoiceTempCount;

    }

    public ArrayList crateInventoryCount(WmsTO wms) {
        Map map = new HashMap();
        ArrayList crateInventoryCount = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            crateInventoryCount = (ArrayList) getSqlMapClientTemplate().queryForList("wms.crateInventoryCount", map);

            System.out.println("crateInventoryCount========" + crateInventoryCount.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getdzProductUploadTemp", sqlException);
        }
        return crateInventoryCount;

    }

    public ArrayList crateInventoryGet(WmsTO wms) {
        Map map = new HashMap();
        ArrayList crateInventoryGet = new ArrayList();
        int status = 0;
        try {
            map.put("userId", wms.getUserId());
            map.put("barCode", wms.getBarCode());
            map.put("date", wms.getDate());

            map.put("endTime", wms.getEndTime());
            map.put("whId", wms.getWhId());

            int getStartTimeCheck = (Integer) getSqlMapClientTemplate().queryForObject("wms.getStartTimeCheck", map);

            if (getStartTimeCheck == 0) {
                map.put("date", wms.getDate());
            } else {
                String startTime = (String) getSqlMapClientTemplate().queryForObject("wms.getCrateStartTime", map);
//                String day = (String) getSqlMapClientTemplate().queryForObject("wms.getCrateDate", map);

                map.put("startTime", startTime);
                System.out.println(wms.getStartTime() + "xxxxxxxxxxxx");
//                map.put("date", day);
                map.put("date", wms.getDate());

            }

            int checkCrateBarCode = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkCrateBarCode", map);
            System.out.println(checkCrateBarCode + "sagfshagshaf");

            if (checkCrateBarCode > 0) {

                int checkCrateBarCodeStatus = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkCrateBarCodeStatus", map);

                if (checkCrateBarCodeStatus == 0) {
                    status = 3;

                } else {
                    int getCrateStatus = (Integer) getSqlMapClientTemplate().queryForObject("wms.getCrateStatus", map);
                    System.out.println(getCrateStatus + "gggggggg");
                    if (getCrateStatus == 1) {
                        status = 1;

                    } else if (getCrateStatus == 2) {
                        status = 2;

                    } else {
                        status = 3;
//                        status = 0 ;

                    }

                }
            } else {
                status = 0;
            }
            map.put("status", status);

            int checkCrateBarCodeExInTemp = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkCrateBarCodeExInTemp", map);
            if (checkCrateBarCodeExInTemp == 0) {
                int insertTempCrateInventoryDetails = (Integer) getSqlMapClientTemplate().update("wms.insertTempCrateInventoryDetails", map);

                wms.setStatus("2");

            } else {
                wms.setStatus("1");

            }

            crateInventoryGet = (ArrayList) getSqlMapClientTemplate().queryForList("wms.crateInventoryGet", map);

            System.out.println("crateInventoryGet========" + crateInventoryGet.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "crateInventoryGet", sqlException);
        }
        return crateInventoryGet;

    }

    public ArrayList imIndentGet(WmsTO wms) {
        Map map = new HashMap();
        ArrayList imIndentDetails = new ArrayList();
        int status = 0;
        try {
            map.put("userId", wms.getUserId());
            map.put("indentId", wms.getInId());

            imIndentDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imIndentDetails", map);

            System.out.println("imIndentDetails========" + imIndentDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "imIndentDetails", sqlException);
        }
        return imIndentDetails;

    }

    public ArrayList getTempCurrentDataCrate(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getTempCurrentDataCrate = new ArrayList();
        try {
            map.put("whId", wmsTo.getWhId());
            map.put("userId", wmsTo.getUserId());
            map.put("endTime", wmsTo.getEndTime());
            map.put("date", wmsTo.getDate());
            System.out.println(wmsTo.getDate() + "dfghjkkuytre");
            System.out.println(wmsTo.getEndTime() + "dfghjkkuytre");

            getTempCurrentDataCrate = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getTempCurrentDataCrate", map);
            System.out.println("getTempCurrentDataCrate()-----" + getTempCurrentDataCrate.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getTempCurrentDataCrate", sqlException);
        }
        return getTempCurrentDataCrate;
    }

    public int insertCrateInventoryMaster(WmsTO wms) {
        Map map = new HashMap();
        int crateInsertMaster = 0;

        ArrayList getCrateInventoryTempMaster = new ArrayList();
        int crateId = 0;

        WmsTO wmsTO = new WmsTO();
        try {
            System.out.println("Testing by roger");
            map.put("userId", wms.getUserId());
            map.put("whId", wms.getWhId());
            map.put("endTime", wms.getEndTime());
            map.put("date", wms.getDate());

            getCrateInventoryTempMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getTempCurrentDataCrates", map);
            Iterator itr = getCrateInventoryTempMaster.iterator();
            while (itr.hasNext()) {
                wmsTO = (WmsTO) itr.next();
                map.put("startTime", wmsTO.getStartTime());
                map.put("date", wmsTO.getDate());

                int CRATECODE = 0;
                CRATECODE = (Integer) getSqlMapClientTemplate().queryForObject("wms.getCrateCode", map);
                System.out.println("CRATECODE-----" + CRATECODE);

                String CRATECODESequence = "" + CRATECODE;

                if (CRATECODESequence == null) {
                    CRATECODESequence = "00001";
                } else if (CRATECODESequence.length() == 1) {
                    CRATECODESequence = "0000" + CRATECODESequence;
                } else if (CRATECODESequence.length() == 2) {
                    CRATECODESequence = "000" + CRATECODESequence;
                } else if (CRATECODESequence.length() == 3) {
                    CRATECODESequence = "00" + CRATECODESequence;
                } else if (CRATECODESequence.length() == 4) {
                    CRATECODESequence = "0" + CRATECODESequence;
                } else if (CRATECODESequence.length() == 5) {
                    CRATECODESequence = "" + CRATECODESequence;
                }

                String crateCode = "CRATE/2021/" + CRATECODESequence;
                map.put("crateCode", crateCode);
                System.out.println("crateCode---" + crateCode);

                crateId = (Integer) getSqlMapClientTemplate().insert("wms.crateInsertMaster", map);
            }

            System.out.println("map  what the ---> " + map);
            ArrayList getTempCrate = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getTempCrate", map);
            System.out.println(getTempCrate.size() + "wat");
            Iterator itr1 = getTempCrate.iterator();
            while (itr1.hasNext()) {
                wms = (WmsTO) itr1.next();
                map.put("barCode", wms.getBarCode());
                map.put("status", wms.getStatus());
                System.out.println(map + "mapped");
                System.out.println(wms.getStatus() + " this value is for status " + wmsTO.getBarCode());
                int CRAID = 0;
                CRAID = (Integer) getSqlMapClientTemplate().queryForObject("wms.getCrateId", map);
                map.put("crateId", CRAID);
                System.out.println(CRAID + "this value is for CRAID");

                crateInsertMaster = (Integer) getSqlMapClientTemplate().update("wms.insertCrateDetails", map);
                System.out.println("detailsssssss");
            }

            int delDzProductUploadTemp = (Integer) getSqlMapClientTemplate().delete("wms.delCrateInventoryTemp", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return crateId;
    }

    public ArrayList getCrateDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getCrateDetails = new ArrayList();

        try {
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("whId", wms.getWhId());
            map.put("userId", wms.getUserId());
            System.out.println("fromDate testing the date " + wms.getFromDate() + "this toDate " + wms.getToDate());
            if (wms.getStatus() == null) {
                map.put("status", "");
            } else {
                map.put("status", wms.getStatus());
            }
            System.out.println("map  " + map);
            getCrateDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getCrateDetails", map);
            System.out.println("getCrateDetails=== " + getCrateDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "dcFreightDetails", sqlException);
        }
        return getCrateDetails;

    }

    public ArrayList getCrateDetailsReport(WmsTO wms, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList getCrateDetailsReport = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("userId", wms.getUserId());

            map.put("fromDate", fromDate);
            map.put("toDate", toDate);

            System.out.println("map  " + map);
            getCrateDetailsReport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getCrateDetailsReport", map);
            System.out.println("getCrateDetailsReport=== " + getCrateDetailsReport.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "dcFreightDetails", sqlException);
        }
        return getCrateDetailsReport;

    }

    public ArrayList getCrateDetailsReportPer(WmsTO wms, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList getCrateDetailsReportPer = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("userId", wms.getUserId());

            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            System.out.println("map  " + map);
            getCrateDetailsReportPer = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getCrateDetailsReportPer", map);
            System.out.println("getCrateDetailsReportPer=== " + getCrateDetailsReportPer.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "dcFreightDetails", sqlException);
        }
        return getCrateDetailsReportPer;

    }

    public ArrayList getImPodList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImPodList = new ArrayList();
        try {
            map.put("roleId", wms.getRoleId());
            map.put("userId", wms.getUserId());
            map.put("toDate", wms.getToDate());
            map.put("viewStatus", wms.getStatusName());
            map.put("fromDate", wms.getFromDate());
            map.put("whId", wms.getWhId());
            map.put("status", wms.getStatus());
            System.out.println("imPODList   " + map);
            getImPodList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImPodList", map);
            System.out.println("getImPodList---" + getImPodList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getImPodList;
    }

    public ArrayList getInventoryExportData(WmsTO wmsTo) {

        Map map = new HashMap();
        ArrayList getTempCurrentDataCrate = new ArrayList();
        try {
            map.put("whId", wmsTo.getWhId());
            map.put("userId", wmsTo.getUserId());
            map.put("crateId", wmsTo.getCrateId());
            getTempCurrentDataCrate = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getInventoryExportData", map);
            System.out.println("getExportData()-----" + getTempCurrentDataCrate.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getTempCurrentDataCrate", sqlException);
        }
        return getTempCurrentDataCrate;
    }

    public ArrayList getDzInvoiceErro(WmsTO wmsTo) {

        Map map = new HashMap();
        ArrayList getDzInvoiceErro = new ArrayList();
        try {
            map.put("whId", wmsTo.getWhId());
            map.put("userId", wmsTo.getUserId());
            map.put("crateId", wmsTo.getCrateId());
            getDzInvoiceErro = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzInvoiceErro", map);
            System.out.println("getDzInvoiceErro()-----" + getDzInvoiceErro.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getTempCurrentDataCrate", sqlException);
        }
        return getDzInvoiceErro;
    }

    public ArrayList getDzGrnDetailsExport(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getDzGrnDetailsExport = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("poNo", wms.getPoNo());

            getDzGrnDetailsExport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzGrnDetailsExport", map);

            System.out.println("getDzGrnDetailsExport========" + getDzGrnDetailsExport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getdzInvoiceUpload", sqlException);
        }
        return getDzGrnDetailsExport;

    }

    public int insertImIndentOrder(WmsTO wms, String[] materialName, String[] materialDescription, String[] uom, String[] poQty, SqlMapClient session) {
        Map map = new HashMap();
        int insertImIndentMaster = 0;
        try {
//            map.put("custId", wms.getCustId());
            String accYear = ThrottleConstants.accountYear;
            map.put("userId", wms.getUserId());
            map.put("deliveryDate", wms.getDeliveryDate());
            map.put("indentDate", wms.getPoDate());
            map.put("whId", wms.getWhId());
            map.put("totQty", wms.getTotQty());
            map.put("remarks", wms.getRemarks());
            String poNo = "";
            int poSequence = (Integer) session.queryForObject("wms.getImIndentSequence", map);
            String poSeq = poSequence + "";
            if (poSeq.length() == 0) {
                poNo = "IMIN/" + accYear + "/00001";
            } else if (poSeq.length() == 1) {
                poNo = "IMIN/" + accYear + "/0000" + poSeq;
            } else if (poSeq.length() == 2) {
                poNo = "IMIN/" + accYear + "/000" + poSeq;
            } else if (poSeq.length() == 3) {
                poNo = "IMIN/" + accYear + "/00" + poSeq;
            } else if (poSeq.length() == 4) {
                poNo = "IMIN/" + accYear + "/0" + poSeq;
            } else {
                poNo = "IMIN/" + accYear + "/" + poSeq;
            }
            map.put("indentNo", poNo);

            if (materialName != null && materialName.length > 0) {
                insertImIndentMaster = (Integer) session.insert("wms.insertImIndentMaster", map);
                map.put("indentId", insertImIndentMaster);
                for (int i = 0; i < materialName.length; i++) {
                    map.put("materialName", materialName[i]);
                    map.put("materialDescription", materialDescription[i]);
                    map.put("indentQty", poQty[i]);
                    map.put("materialId", wms.getItemIds()[i]);
                    map.put("uom", uom[i]);
                    int insertIntoImPoDetails = (Integer) session.insert("wms.insertImIndentDetails", map);
                }
            }
            System.out.println("insertImInDetails---" + insertImIndentMaster);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadPOD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "uploadPOD", sqlException);
        }
        return insertImIndentMaster;
    }

    public ArrayList imMaterialPo(WmsTO wms) {
        Map map = new HashMap();
        ArrayList imMaterialPo = new ArrayList();

        try {
            map.put("whId", wms.getWhId());
            map.put("whName", wms.getName());
            System.out.println("map  " + map);
            imMaterialPo = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imMaterialPo", map);
            System.out.println("imMaterialPo=== " + imMaterialPo.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return imMaterialPo;
    }

    public ArrayList imPoMaterialForGrn(WmsTO wms) {
        Map map = new HashMap();
        ArrayList imPoMaterialForGrn = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("poNo", wms.getPoNo());
            map.put("poId", wms.getPoId());

            imPoMaterialForGrn = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imPoMaterialForGrn", map);

            System.out.println("imPoMaterialForGrn ========" + imPoMaterialForGrn.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "imPoMaterialForGrn", sqlException);
        }
        return imPoMaterialForGrn;

    }

    public ArrayList towerLRDetails(WmsTO wmsTo) {

        Map map = new HashMap();
        ArrayList towerLRDetails = new ArrayList();
        try {
            map.put("whId", wmsTo.getWhId());
            map.put("userId", wmsTo.getUserId());
            map.put("fromDate", wmsTo.getFromDate());
            map.put("toDate", wmsTo.getToDate());
            map.put("date", wmsTo.getDate());

            towerLRDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.towerLRDetails", map);
            System.out.println("towerLRDetails()-----" + towerLRDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getTempCurrentDataCrate", sqlException);
        }
        return towerLRDetails;
    }

    public ArrayList towerLRDetails2(WmsTO wmsTo) {

        Map map = new HashMap();
        ArrayList towerLRDetails2 = new ArrayList();
        try {
            map.put("whId", wmsTo.getWhId());
            map.put("userId", wmsTo.getUserId());
            map.put("fromDate", wmsTo.getFromDate());
            map.put("toDate", wmsTo.getToDate());
            map.put("date", wmsTo.getDate());
            towerLRDetails2 = (ArrayList) getSqlMapClientTemplate().queryForList("wms.towerLRDetails2", map);
            System.out.println("towerLRDetails2()-----" + towerLRDetails2.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getTempCurrentDataCrate", sqlException);
        }
        return towerLRDetails2;
    }

    public ArrayList dzPodUploadDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList dzPodUploadDetails = new ArrayList();

        try {
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("podStatus", wms.getPodStatus());
            if (wms.getStatusType() == null) {
                map.put("statusType", "0");
            } else {
                map.put("statusType", wms.getStatusType());
            }
            if (wms.getWarehouseId() != null && "".equals(wms.getWarehouseId())) {
                map.put("whId", wms.getWarehouseId());
            } else {
                map.put("whId", wms.getWhId());
            }
            System.out.println("map  " + map);
            dzPodUploadDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.dzPodUploadDetails", map);
            System.out.println("dzPodUploadDetails=== " + dzPodUploadDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "imPodUploadDetails", sqlException);
        }
        return dzPodUploadDetails;

    }

    public ArrayList getWareHouseListForTR(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getWareHouseListForTR = new ArrayList();
        try {
            System.out.println("map == " + map);
            map.put("roleId", wms.getRoleId());
            getWareHouseListForTR = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getWareHouseListForTR", map);
            System.out.println("getWareHouseListForTR====" + getWareHouseListForTR.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getWareHouseListForTR;

    }

    public ArrayList getIndentView(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getIndentView = new ArrayList();

        try {
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("whId", wms.getWhId());
            map.put("userId", wms.getUserId());
            System.out.println("fromDate testing the date " + wms.getFromDate() + "this toDate " + wms.getToDate());
            if (wms.getStatus() == null) {
                map.put("status", "");
            } else {
                map.put("status", wms.getStatus());
            }
            System.out.println("map  " + map);
            getIndentView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getIndentView", map);
            System.out.println("getIndentView=== " + getIndentView.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "dcFreightDetails", sqlException);
        }
        return getIndentView;

    }

    public int instaSendMail(WmsTO WmsTO, int userId, String whId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int lrNo = 0;
        int checkInvoice = 0;
        int pinWhCheck = 0;
        ArrayList getconnectInvoice = new ArrayList();

        WmsTO wmsTO = new WmsTO();
        try {
            map.put("userId", userId);
            map.put("whId", whId);
            map.put("grnId", WmsTO.getGrnId1());
            map.put("productId", WmsTO.getProductID());
            map.put("remarks", WmsTO.getRemarks());

            String mailTemplate = "";
            String userName = "";
            String totalQty = "";
            String indentNo = "";
            String indentDate = "";
            String whName = "";
            String requirement = "";
            String materialCode = "";
            String material = "";
            String UOM = "";
            String materialDescription = "";
            String indentQty = "";
            String getHubMailId = "";
            String currentStock = "";
            String id = "";
            String rows = "";
            String subject = "";

            getHubMailId = (String) getSqlMapClientTemplate().queryForObject("wms.getHubMailId", map);
            String sendMail = "sivanesap@entitlesolutions.com,sasikannan@hssupplychain.com,puneet.p@hssupplychain.com ,venkat@hssupplychain.com,jagan.p@hssupplychain.com";
            if (getHubMailId != null && !"".equals(getHubMailId) && !"s@gmail.com".equals(getHubMailId)) {
                sendMail = sendMail + "," + getHubMailId;
            }

            mailTemplate = getMailTemplate("4", null);

            ArrayList imIndentDetailsMail1 = new ArrayList();
            imIndentDetailsMail1 = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imIndentDetailsMail1", map);
            System.out.println("imIndentDetailsMail1 " + imIndentDetailsMail1.size());

            Iterator itr2 = imIndentDetailsMail1.iterator();
            while (itr2.hasNext()) {
                WmsTO wms1 = (WmsTO) itr2.next();
                indentDate = wms1.getDate();
                requirement = wms1.getExpectedDate();
                indentNo = wms1.getInvoiceNo();
                totalQty = wms1.getStockQty();
                whName = wms1.getWhName();
                userName = wms1.getName();
                materialCode = wms1.getMaterial();
                materialDescription = wms1.getMaterialDescription();
                indentQty = wms1.getInQty();
                currentStock = wms1.getStockQty();
                mailTemplate = mailTemplate.replaceAll("whName", whName);
                mailTemplate = mailTemplate.replaceAll("indentDate", indentDate);
                mailTemplate = mailTemplate.replaceAll("indentNo", indentNo);
                mailTemplate = mailTemplate.replaceAll("userName", userName);
                mailTemplate = mailTemplate.replaceAll("whName", whName);
                mailTemplate = mailTemplate.replaceAll("requirement", requirement);
            }

            ArrayList imIndentDetailsMail = new ArrayList();
            imIndentDetailsMail = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imIndentDetailsMail", map);
            System.out.println("imIndentDetailsMail " + imIndentDetailsMail.size());
            int count1 = 1;
            Iterator itr1 = imIndentDetailsMail.iterator();
            while (itr1.hasNext()) {
                WmsTO wms1 = (WmsTO) itr1.next();
                indentDate = wms1.getDate();
                requirement = wms1.getExpectedDate();
                indentNo = wms1.getMaterialCode();
                totalQty = wms1.getStockQty();
                whName = wms1.getWhName();
                userName = wms1.getName();
                materialCode = wms1.getMaterial();
                material = wms1.getMaterial();
                materialDescription = wms1.getMaterialDescription();
                indentQty = wms1.getInQty();
                currentStock = wms1.getStockQty();
                UOM = wms1.getUom2();
                id = wms1.getId();

                subject = "Indent Request " + wms1.getWhName();

                rows = rows + "<tr>\n"
                        + "  <td>" + count1 + "</td>\n"
                        + "  <td>" + material + "</td>\n"
                        + "  <td>" + materialDescription + "</td>\n"
                        + "  <td>" + indentQty + "</td>\n"
                        + "  <td>" + UOM + "</td>\n"
                        + "  <td>" + currentStock + "</td>\n"
                        + "</tr>";

                count1++;

            }

            mailTemplate = mailTemplate.replaceAll("newRows", rows);

//            subject = "Indent Request";
            String content = mailTemplate;
            map.put("mailTypeId", "4");
            map.put("mailSubjectTo", subject);
            map.put("mailSubjectCc", subject);
            map.put("mailSubjectBcc", subject);
            map.put("mailContentTo", content);
            map.put("mailContentCc", content);
            map.put("mailContentBcc", content);
            map.put("mailTo", sendMail);
            map.put("mailCc", "");
            map.put("mailBcc", "");
            map.put("userId", userId);
            map.put("id", id);
            System.out.println("start" + id);
//            System.out.println("map = " + map);

            int advancemail = (Integer) getSqlMapClientTemplate().insert("wms.insertMailDetails", map);
//            System.out.println("advancemail = " + advancemail);
            if (advancemail > 0) {
                int updateMailStatusInGRN = (Integer) getSqlMapClientTemplate().update("wms.updateConnectImIndentMailStatus", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public int instaPoSendMail(WmsTO WmsTO, int userId, String whId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int lrNo = 0;
        int checkInvoice = 0;
        int pinWhCheck = 0;
        ArrayList getconnectInvoice = new ArrayList();

        WmsTO wmsTO = new WmsTO();
        try {
            map.put("userId", userId);
            map.put("whId", whId);
            map.put("grnId", WmsTO.getGrnId1());
            map.put("productId", WmsTO.getProductID());
            map.put("remarks", WmsTO.getRemarks());
            map.put("poId", WmsTO.getPoId());

            String mailTemplate = "";
            String poNo = "";
            String poDate = "";
            String whName = "";
            String indentNo = "";
            String userName = "";
            String requirement = "";
            String cust = "";
            String tax = "";
            String material = "";
            String uom = "";
            String unitPrice = "";
            String poQty = "";
            String price = "";
            String stock = "";
            String getHubMailId = "";
            String currentStock = "";
            String id = "";
            String rows = "";
            String subject = "";

            getHubMailId = (String) getSqlMapClientTemplate().queryForObject("wms.getHubMailId", map);
            String sendMail = "sivanesap@entitlesolutions.com,sasikannan@hssupplychain.com";
            if (getHubMailId != null && !"".equals(getHubMailId) && !"s@gmail.com".equals(getHubMailId)) {
                sendMail = sendMail + "," + getHubMailId;
            }

            mailTemplate = getMailTemplate("5", null);

            ArrayList imPoDetailsMail = new ArrayList();
            imPoDetailsMail = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imPoDetailsMail", map);
            System.out.println("imPoDetailsMail " + imPoDetailsMail.size());

            Iterator itr2 = imPoDetailsMail.iterator();
            while (itr2.hasNext()) {

                WmsTO wms1 = (WmsTO) itr2.next();
                poNo = wms1.getPoNo();
                poDate = wms1.getPoDate();
                whName = wms1.getWhName();
                indentNo = wms1.getInNo();
                userName = wms1.getName();
                requirement = wms1.getExpectedDate();
                cust = wms1.getCustName();
                tax = wms1.getTaxValue() + "%";

                mailTemplate = mailTemplate.replaceAll("poNo", poNo);
                mailTemplate = mailTemplate.replaceAll("poDate", poDate);
                mailTemplate = mailTemplate.replaceAll("whName", whName);
                mailTemplate = mailTemplate.replaceAll("indentNo", indentNo);
                mailTemplate = mailTemplate.replaceAll("userName", userName);
                mailTemplate = mailTemplate.replaceAll("requirement", requirement);
                mailTemplate = mailTemplate.replaceAll("cust", cust);
                mailTemplate = mailTemplate.replaceAll("taz", tax);
                subject = "PO " + whName;

//                mailTemplate = mailTemplate.replaceAll("totalQty", totalQty);
            }

            ArrayList imPoDetailsMail1 = new ArrayList();
            imPoDetailsMail1 = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imPoDetailsMail1", map);
            System.out.println("imPoDetailsMail1 " + imPoDetailsMail1.size());
            int count1 = 1;
            Iterator itr1 = imPoDetailsMail1.iterator();
            while (itr1.hasNext()) {
                WmsTO wms1 = (WmsTO) itr1.next();
                material = wms1.getMaterial();
                uom = wms1.getUom2();
                unitPrice = wms1.getUnit();
                poQty = wms1.getPoQty1();
                price = wms1.getPrice();
                stock = wms1.getStockQty();
                id = wms1.getId();

                rows = rows + "<tr>\n"
                        + "  <td>" + count1 + "</td>\n"
                        + "  <td>" + material + "</td>\n"
                        + "  <td>" + uom + "</td>\n"
                        + "  <td>" + unitPrice + "</td>\n"
                        + "  <td>" + poQty + "</td>\n"
                        + "  <td>" + price + "</td>\n"
                        + "  <td>" + stock + "</td>\n"
                        + "</tr>";

                count1++;

            }

            mailTemplate = mailTemplate.replaceAll("newRows", rows);

            String content = mailTemplate;
            map.put("mailTypeId", "5");
            map.put("mailSubjectTo", subject);
            map.put("mailSubjectCc", subject);
            map.put("mailSubjectBcc", subject);
            map.put("mailContentTo", content);
            map.put("mailContentCc", content);
            map.put("mailContentBcc", content);
            map.put("mailTo", sendMail);
            map.put("mailCc", "");
            map.put("mailBcc", "");
            map.put("userId", userId);
            map.put("id", id);
            System.out.println("start" + id);
//            System.out.println("map = " + map);

            int advancemail = (Integer) getSqlMapClientTemplate().insert("wms.insertMailDetails", map);
//            System.out.println("advancemail = " + advancemail);
            if (advancemail > 0) {
                int updateMailStatusInGRN = (Integer) getSqlMapClientTemplate().update("wms.updateImPoMailStatus", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public int instaGrnSendMail(WmsTO WmsTO, int userId, String whId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int lrNo = 0;
        int checkInvoice = 0;
        int pinWhCheck = 0;
        ArrayList getconnectInvoice = new ArrayList();

        WmsTO wmsTO = new WmsTO();
        try {
            map.put("userId", userId);
            map.put("whId", whId);
            map.put("grnId", WmsTO.getGrnId1());
            map.put("productId", WmsTO.getProductID());
            map.put("remarks", WmsTO.getRemarks());

            String mailTemplate = "";
            String grnNo = "";
            String grnDate = "";
            String poNo = "";
            String whName = "";
            String userName = "";
            String invoiceNo = "";
            String invoiceDate = "";
            String material = "";
            String receivedQty = "";
            String goodQty = "";
            String shortageQty = "";
            String damageQty = "";
            String getHubMailId = "";
            String UOM = "";
            String currentStock = "";
            String id = "";
            String rows = "";
            String subject = "";

            getHubMailId = (String) getSqlMapClientTemplate().queryForObject("wms.getHubMailId", map);
            String sendMail = "sivanesap@entitlesolutions.com,sasikannan@hssupplychain.com";
            if (getHubMailId != null && !"".equals(getHubMailId) && !"s@gmail.com".equals(getHubMailId)) {
                sendMail = sendMail + "," + getHubMailId;
            }

            mailTemplate = getMailTemplate("6", null);

            ArrayList imIndentDetailsMail1 = new ArrayList();
            imIndentDetailsMail1 = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imGrnDetailsMail", map);
            System.out.println("imIndentDetailsMail1 " + imIndentDetailsMail1.size());

            Iterator itr2 = imIndentDetailsMail1.iterator();
            while (itr2.hasNext()) {
                WmsTO wms1 = (WmsTO) itr2.next();

                grnNo = wms1.getGrnNo();
                grnDate = wms1.getGrnDate();
                poNo = wms1.getPoNo();
                whName = wms1.getWhName();
                userName = wms1.getName();
                invoiceNo = wms1.getInvoiceNo();
                invoiceDate = wms1.getInvoiceDate();

                mailTemplate = mailTemplate.replaceAll("grnNo", grnNo);
                mailTemplate = mailTemplate.replaceAll("grnDate", grnDate);
                mailTemplate = mailTemplate.replaceAll("poNo", poNo);
                mailTemplate = mailTemplate.replaceAll("whName", whName);
                mailTemplate = mailTemplate.replaceAll("userName", userName);
                mailTemplate = mailTemplate.replaceAll("invoiceNo", invoiceNo);
                mailTemplate = mailTemplate.replaceAll("invoiceDate", invoiceDate);
                subject = "GRN " + whName;
//                mailTemplate = mailTemplate.replaceAll("totalQty", totalQty);

            }

            ArrayList imGrnDetailsMail1 = new ArrayList();
            imGrnDetailsMail1 = (ArrayList) getSqlMapClientTemplate().queryForList("wms.imGrnDetailsMail1", map);
            System.out.println("imGrnDetailsMail1 " + imGrnDetailsMail1.size());
            int count1 = 1;
            Iterator itr1 = imGrnDetailsMail1.iterator();
            while (itr1.hasNext()) {
                WmsTO wms1 = (WmsTO) itr1.next();
                material = wms1.getMaterial();
                receivedQty = wms1.getReceivedQty();
                goodQty = wms1.getGood();
                UOM = wms1.getUom2();
                goodQty = wms1.getGood();
                shortageQty = wms1.getShortage();
                damageQty = wms1.getDamage();

                currentStock = wms1.getStockQty();
                id = wms1.getId();

                rows = rows + "<tr>\n"
                        + "  <td>" + count1 + "</td>\n"
                        + "  <td>" + material + "</td>\n"
                        + "  <td>" + receivedQty + "</td>\n"
                        + "  <td>" + goodQty + "</td>\n"
                        + "  <td>" + UOM + "</td>\n"
                        + "  <td>" + shortageQty + "</td>\n"
                        + "  <td>" + damageQty + "</td>\n"
                        + "</tr>";

                count1++;

            }

            mailTemplate = mailTemplate.replaceAll("newRows", rows);

            String content = mailTemplate;
            map.put("mailTypeId", "6");
            map.put("mailSubjectTo", subject);
            map.put("mailSubjectCc", subject);
            map.put("mailSubjectBcc", subject);
            map.put("mailContentTo", content);
            map.put("mailContentCc", content);
            map.put("mailContentBcc", content);
            map.put("mailTo", sendMail);
            map.put("mailCc", "");
            map.put("mailBcc", "");
            map.put("userId", userId);
            map.put("id", id);
            int advancemail = (Integer) getSqlMapClientTemplate().insert("wms.insertMailDetails", map);
//            System.out.println("advancemail = " + advancemail);
            if (advancemail > 0) {
                int updateMailStatusInGRN = (Integer) getSqlMapClientTemplate().update("wms.updateImGrnMailStatus", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public ArrayList employeeHireDetails(WmsTO wmsTo) {

        Map map = new HashMap();
        ArrayList employeeHireDetails = new ArrayList();
        try {
            map.put("whId", wmsTo.getWhId());
            map.put("userId", wmsTo.getUserId());
            map.put("fromDate", wmsTo.getFromDate());
            map.put("toDate", wmsTo.getToDate());
            map.put("date", wmsTo.getDate());

            employeeHireDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.employeeHireDetailsTemp", map);
            System.out.println("employeeHireDetails()-----" + employeeHireDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getTempCurrentDataCrate", sqlException);
        }
        return employeeHireDetails;
    }

    public ArrayList employeeHireDetailsTemp(WmsTO wms) {
        Map map = new HashMap();
        ArrayList employeeHireDetailsTemp = new ArrayList();
        try {
            map.put("userId", wms.getUserId());

            employeeHireDetailsTemp = (ArrayList) getSqlMapClientTemplate().queryForList("wms.employeeHireDetailsTemp", map);

            System.out.println("employeeHireDetailsTemp========" + employeeHireDetailsTemp);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getdzProductUploadTemp", sqlException);
        }
        return employeeHireDetailsTemp;

    }

    public int insertDzHireUpload(WmsTO wmsTO1, SqlMapClient session) {
        Map map = new HashMap();
        int insertStatus = 0;

        ArrayList employeeHireDetailsTemp = new ArrayList();
        ArrayList getdzProductUploadTempData = new ArrayList();
        map.put("userId", wmsTO1.getUserId());
        map.put("whId", wmsTO1.getWhId());
        WmsTO wmsTO = new WmsTO();
        try {

            employeeHireDetailsTemp = (ArrayList) getSqlMapClientTemplate().queryForList("wms.employeeHireDetailsTemp", map);
            Iterator itr = employeeHireDetailsTemp.iterator();
            while (itr.hasNext()) {
                wmsTO = (WmsTO) itr.next();
                map.put("name", wmsTO.getName());
                map.put("id", wmsTO.getId());
                map.put("ctc", wmsTO.getCtc());
                map.put("designation", wmsTO.getDesignation());
                map.put("client", wmsTO.getClient());
                map.put("location", wmsTO.getLocation());
                map.put("date", wmsTO.getDate());
                map.put("leavingDate", wmsTO.getLeavingDate());
                map.put("type", wmsTO.getType());
                map.put("category", wmsTO.getCategory());
                map.put("status", wmsTO.getStatus());

                System.out.println(wmsTO.getUserId() + "this value is to check userId");

                int HireNo = 0;
                HireNo = (Integer) session.queryForObject("wms.getEmpHireCode", map);
                System.out.println("hireNo-----" + HireNo);

                String HireNoSequence = "" + HireNo;

                if (HireNoSequence == null) {
                    HireNoSequence = "00001";
                } else if (HireNoSequence.length() == 1) {
                    HireNoSequence = "0000" + HireNoSequence;
                } else if (HireNoSequence.length() == 2) {
                    HireNoSequence = "000" + HireNoSequence;
                } else if (HireNoSequence.length() == 3) {
                    HireNoSequence = "00" + HireNoSequence;
                } else if (HireNoSequence.length() == 4) {
                    HireNoSequence = "0" + HireNoSequence;
                } else if (HireNoSequence.length() == 5) {
                    HireNoSequence = "" + HireNoSequence;
                }

                String hireCode = "EPH/2021/" + HireNoSequence;
                map.put("hireCode", hireCode);
                System.out.println("hireCode---" + hireCode);
                System.out.println(map + "map");
                System.out.println(employeeHireDetailsTemp.size() + "size of value");
                insertStatus = (Integer) getSqlMapClientTemplate().update("wms.insertEmpHireMaster", map);

            }
            getdzProductUploadTempData = (ArrayList) getSqlMapClientTemplate().queryForList("wms.employeeHireDetailsTemp", map);
            Iterator itr1 = getdzProductUploadTempData.iterator();
            WmsTO wms = new WmsTO();
            while (itr1.hasNext()) {
                wms = (WmsTO) itr1.next();
                map.put("name", wmsTO.getName());
                map.put("id", wmsTO.getId());
                map.put("ctc", wmsTO.getCtc());
                map.put("designation", wmsTO.getDesignation());
                map.put("client", wmsTO.getClient());
                map.put("location", wmsTO.getLocation());
                map.put("date", wmsTO.getDate());
                map.put("leavingDate", wmsTO.getLeavingDate());
                map.put("type", wmsTO.getType());
                map.put("category", wmsTO.getCategory());
                map.put("status", wmsTO.getStatus());

                if ("0".equals(wms.getStatus())) {
                    int HIREID = 0;
                    HIREID = (Integer) session.queryForObject("wms.getDzGrnId", map);
                    map.put("grnId", HIREID);
                    System.out.println(HIREID + "this value is for grnId");

                    insertStatus = (Integer) getSqlMapClientTemplate().update("wms.insertDzGrnDetails", map);
                }

            }
            map.put("userId", wmsTO1.getUserId());
            int delDzProductUploadTemp = (Integer) getSqlMapClientTemplate().update("wms.delDzGrnDetailsTemp", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public int empHireUpload(WmsTO wms) {
        int insert = 0;
        System.out.println("on here");
        Map map = new HashMap();
        map.put("userId", wms.getUserId());
        map.put("whId", wms.getWhId());
        map.put("name", wms.getName());
        map.put("id", wms.getId());
        map.put("ctc", wms.getCtc());
        map.put("designation", wms.getDesignation());
        map.put("client", wms.getClient());
        map.put("location", wms.getLocation());
        map.put("DOJ", wms.getDate());
        map.put("DOL", wms.getLeavingDate());
        map.put("type", wms.getType());
        map.put("category", wms.getCategory());

        System.out.println("'''''''''''======================'''''''''''''" + wms.getName());

        System.out.println("map    " + map);
//        if ("".equals(wms.getReceivedQty())) {
//            map.put("poReceivedQty", "0");
//        }

        try {
            int code = (Integer) getSqlMapClientTemplate().queryForObject("wms.empHireEmpIdCheck", map);
            System.out.println("----------------------------" + code);
            if (code == 0) {
                map.put("status", "0");

            } else {
                map.put("status", "1");
            }

            insert = (Integer) getSqlMapClientTemplate().insert("wms.insertEmpHireTemp", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return insert;

    }

    public int delEmpHireTemp(WmsTO wms) {
        Map map = new HashMap();
        int delEmpHireTemp = 0;
        try {
            map.put("userId", wms.getUserId());
            delEmpHireTemp = (Integer) getSqlMapClientTemplate().update("wms.delEmpHireTemp", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "delDzGrnDetailsTemp", sqlException);
        }
        return delEmpHireTemp;

    }

    public int updateImPodApproval(String podId, String status, WmsTO wms) {
        Map map = new HashMap();
        int saveImPodFile = 0;
        try {
            map.put("podId", podId);
            map.put("status", status);
            map.put("userId", wms.getUserId());

            System.out.println(podId + "podId" + status + "" + "the testing");

            saveImPodFile = (Integer) getSqlMapClientTemplate().update("wms.updateImPodStatus", map);

//            if (status == "3"){
//               int statusOnSublrMaster = (Integer) getSqlMapClientTemplate().update("wms.updateImPodStatus", map);
//
//            }
            System.out.println("saveImPodFile---" + saveImPodFile);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFkDepositFile", sqlException);
        }
        return saveImPodFile;
    }

    public int updateApproveStatus(WmsTO wms, int userId) {
        Map map = new HashMap();
        int updateApproveStatus = 0;
        try {
            map.put("podId", wms.getPoId());
            map.put("userId", userId);
            System.out.println("map-----------" + map);
            updateApproveStatus = (Integer) getSqlMapClientTemplate().update("wms.updateApproveStatus", map);
            System.out.println("updateApproveStatus---" + updateApproveStatus);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFkDepositFile", sqlException);
        }
        return updateApproveStatus;
    }

    public int updateAuthStatus(WmsTO wms, int userId) {
        Map map = new HashMap();
        int updateAuthStatus = 0;
        try {
            map.put("podId", wms.getPoId());
            map.put("userId", userId);
            System.out.println("map-----------" + map);
            updateAuthStatus = (Integer) getSqlMapClientTemplate().update("wms.updateAuthStatus", map);
            System.out.println("updateAuthStatus---" + updateAuthStatus);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFkDepositFile", sqlException);
        }
        return updateAuthStatus;
    }

    public String getAuthStatus(WmsTO wms) {
        Map map = new HashMap();
        String getAuthStatus = " ";
        try {
            map.put("podId", wms.getPoId());

            System.out.println("map-----------" + map);
            getAuthStatus = (String) getSqlMapClientTemplate().queryForObject("wms.getAuthStatus", map);
            System.out.println("updateAuthStatus---" + getAuthStatus);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFkDepositFile", sqlException);
        }
        return getAuthStatus;
    }

    public ArrayList staffAttendanceReport(WmsTO wms) {
        Map map = new HashMap();
        ArrayList staffAttendanceReport = new ArrayList();
        try {

            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("whId", wms.getWhId());
            System.out.println("map----------------" + map);
            staffAttendanceReport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.staffAttendanceReport", map);
            System.out.println("updateAuthStatus---" + staffAttendanceReport);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFkDepositFile", sqlException);
        }
        return staffAttendanceReport;
    }

    public String insertImMaterialGrnReturn(WmsTO wms, String[] skuCode, String[] uom, String[] stockQty, String[] returnQty, SqlMapClient session) {
        Map map = new HashMap();
        int returnId = 0;
        int status = 0;
        String returnNo = "";
        try {
            String accYear = ThrottleConstants.accountYear;
            String[] materialId = wms.getItemIds();
            map.put("userId", wms.getUserId());
            map.put("returnDate", wms.getReturnDate());
            map.put("returnTime", wms.getReturnTime());
            map.put("whId", wms.getWhId());
            map.put("totQty", wms.getTotQty());
            map.put("remarks", wms.getRemarks());
            map.put("receivedBy", wms.getReceivedBy());

            int returnSequence = (Integer) session.queryForObject("wms.imReturnSequence", map);
            String returnSeq = returnSequence + "";
            if (returnSeq.length() == 0) {
                returnNo = "GRS/" + accYear + "/00001";
            } else if (returnSeq.length() == 1) {
                returnNo = "GRS/" + accYear + "/0000" + returnSeq;
            } else if (returnSeq.length() == 2) {
                returnNo = "GRS/" + accYear + "/000" + returnSeq;
            } else if (returnSeq.length() == 3) {
                returnNo = "GRS/" + accYear + "/00" + returnSeq;
            } else if (returnSeq.length() == 4) {
                returnNo = "GRS/" + accYear + "/0" + returnSeq;
            } else {
                returnNo = "GRS/" + accYear + "/" + returnSeq;
            }
            map.put("returnNo", returnNo);

            returnId = (Integer) session.insert("wms.insertImMaterialReturnMaster", map);
            map.put("returnId", returnId);
            for (int i = 0; i < materialId.length; i++) {
                map.put("materialId", materialId[i].split("~")[0]);
                map.put("skuCode", skuCode[i]);
                map.put("uom", uom[i]);
                map.put("returnQty", returnQty[i]);
                map.put("stockQty", stockQty[i]);
                map.put("status", "0");
                Double packQty = (Double) session.queryForObject("wms.getImMaterialPackQty", map);
                Double tot = packQty * Double.parseDouble(returnQty[i]);
                map.put("floorQty", "0.00");
                int stockId = (Integer) session.queryForObject("wms.checkImMaterialStockId", map);
                int insertImReturnDetails = (Integer) session.insert("wms.insertImMaterialReturn", map);
                if (stockId > 0) {
                    map.put("stockId", stockId);
                    int addImMaterialStock = (Integer) session.update("wms.addImMaterialStockQty", map);
                }
            }
            System.out.println("returnId---" + returnId);
            System.out.println("returnNo---" + returnNo);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadPOD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "uploadPOD", sqlException);
        }
        return returnNo;
    }

    public ArrayList getImMaterialReturnMaster(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImMaterialReturnMaster = new ArrayList();
        try {
            map.put("returnId", wms.getReturnId());
            map.put("whId", wms.getWhId());
            System.out.println("map  " + map);
            getImMaterialReturnMaster = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImMaterialReturnMaster", map);
            System.out.println("getImMaterialReturnMaster size=" + getImMaterialReturnMaster.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWareHouseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWareHouseList List", sqlException);
        }

        return getImMaterialReturnMaster;
    }

    public ArrayList getImMaterialReturnDetails(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getImMaterialReturnDetails = new ArrayList();
        try {
            map.put("returnId", wms.getReturnId());
            map.put("whId", wms.getWhId());
            getImMaterialReturnDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getImMaterialReturnDetails", map);
            System.out.println("getImMaterialReturnDetails size=" + getImMaterialReturnDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWareHouseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWareHouseList List", sqlException);
        }

        return getImMaterialReturnDetails;
    }

    public int updateImGrnReturn(String[] floorQty, String[] usedQty, String[] excess, String[] shortage, String[] status, String[] detailId, String[] floorPackQty, String[] materialIds, WmsTO wms, int userId) {
        Map map = new HashMap();
        int updateImGrnReturn = 0;
        try {
            System.out.println("map  " + map);
            for (int i = 0; i < floorQty.length; i++) {
                map.put("floorQty", floorQty[i]);
                map.put("usedQty", usedQty[i]);
                map.put("excess", excess[i]);
                map.put("shortage", shortage[i]);
                map.put("detailId", detailId[i]);
                map.put("status", status[i]);
                map.put("userId", userId);
                updateImGrnReturn = (Integer) getSqlMapClientTemplate().update("wms.updateImGrnReturn", map);
                if ("1".equals(status[i])) {
                    map.put("whId", wms.getWhId());
                    map.put("materialId", materialIds[i]);
                    int stockId = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkImMaterialStockId", map);
                    map.put("stockId", stockId);
                    if (stockId != 0) {
                        map.put("grnQty", floorPackQty[i]);
                        int materialReference = (Integer) getSqlMapClientTemplate().update("wms.updateReturnMaterialReference", map);
//                        int update = (Integer) getSqlMapClientTemplate().update("wms.updateImMaterialStockQty", map);
                    } else {
                        stockId = (Integer) getSqlMapClientTemplate().update("wms.insertImMaterialStock", map);
//                        map.put("stockId", stockId);
//                        int update = (Integer) getSqlMapClientTemplate().update("wms.updateImMaterialStockQty", map);
                    }
                }
                System.out.println("updateImGrnReturn size=" + updateImGrnReturn);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWareHouseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWareHouseList List", sqlException);
        }

        return updateImGrnReturn;
    }
    
    public ArrayList getDzPodList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getDzPodList = new ArrayList();
        try {
            map.put("roleId", wms.getRoleId());
            map.put("userId", wms.getUserId());
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("viewStatus", wms.getStatusName());
            map.put("whId", wms.getWhId());
            map.put("status", wms.getStatus());
            System.out.println("connectPOD List----------------------------" + map);
            getDzPodList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDzPodList", map);
            System.out.println("getPodList---" + getDzPodList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return getDzPodList;
    }
    
      public int updateDzPodApproval(String podId, String status, WmsTO wms) {
        Map map = new HashMap();
        int saveDzPodFile = 0;
        try {
            map.put("podId", podId);
            map.put("status", status);
            map.put("userId", wms.getUserId());

            System.out.println(podId + "podId" + status + "" + "the testing");

            saveDzPodFile = (Integer) getSqlMapClientTemplate().update("wms.updateDzPodStatus", map);

//            if (status == "3"){
//               int statusOnSublrMaster = (Integer) getSqlMapClientTemplate().update("wms.updateImPodStatus", map);
//
//            }
            System.out.println("saveImPodFile---" + saveDzPodFile);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFkDepositFile", sqlException);
        }
        return saveDzPodFile;
    }
  public String getImDeliveryDate(WmsTO wms) {
        Map map = new HashMap();
        String getImDeliveryDate = "";

        try {
            map.put("subLr", wms.getLrnNo());
            System.out.println("map  " + map);
            getImDeliveryDate = (String) getSqlMapClientTemplate().queryForObject("wms.getImDeliveryDate", map);
            System.out.println("getImDeliveryDate=== " + getImDeliveryDate);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return getImDeliveryDate;

    }
 public String getDzDeliveryDate(WmsTO wms) {
        Map map = new HashMap();
        String getDzDeliveryDate = "";

        try {
            map.put("subLr", wms.getLrnNo());
            System.out.println("map  " + map);
            getDzDeliveryDate = (String) getSqlMapClientTemplate().queryForObject("wms.getDzDeliveryDate", map);
            System.out.println("getDzDeliveryDate=== " + getDzDeliveryDate);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "podUploadDetails", sqlException);
        }
        return getDzDeliveryDate;

    }

    

}
