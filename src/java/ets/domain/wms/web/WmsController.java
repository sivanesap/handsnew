package ets.domain.wms.web;

import ets.arch.web.BaseController;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.wms.business.WmsBP;
import ets.domain.wms.business.WmsTO;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.*;
import java.util.*;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONObject;
import jxl.Workbook;
import jxl.*;
import jxl.WorkbookSettings;
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import ets.domain.ops.business.OpsTO;
import ets.domain.ops.business.OpsBP;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WmsController extends BaseController {

    WmsCommand wmsCommand;
    WmsBP wmsBP;
    OpsBP opsBP;

    public OpsBP getOpsBP() {
        return opsBP;
    }

    public void setOpsBP(OpsBP opsBP) {
        this.opsBP = opsBP;
    }

    public WmsCommand getWmsCommand() {
        return wmsCommand;
    }

    public void setWmsCommand(WmsCommand wmsCommand) {
        this.wmsCommand = wmsCommand;
    }

    public WmsBP getWmsBP() {
        return wmsBP;
    }

    public void setWmsBP(WmsBP wmsBP) {
        this.wmsBP = wmsBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        //   int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        System.out.println("userId" + userId);
        //  System.out.println("loginRecordId" + loginRecordId);
        String getUserFunctionsActive = "";
        String uri = request.getRequestURI();
//        getUserFunctionsActive = loginBP.getUserFunctionsActive(userId, uri);
        if (getUserFunctionsActive != "" && getUserFunctionsActive != null) {
            String temp[] = getUserFunctionsActive.split("~");
            System.out.println("function Id" + temp[0]);
            System.out.println("function Name  " + temp[0]);
            int status1 = 0;
            int functionId = Integer.parseInt(temp[0]);
            String functionName = temp[1];
//            status1 = loginBP.insertUserFunctionAccessDetails(functionId, functionName, userId, loginRecordId);
        }
        binder.closeNoCatch();
        initialize(request);
    }

    /**
     * This method used to View Vendor Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView viewDeliveryRequest(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Manage Vendor  >>  View ";
        path = "content/wms/deliveryRequestMaster.jsp";
        String pageTitle = "viewDeliveryRequest";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "viewDeliveryRequest";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            wmsTo.setUserId(userId + "");
            ArrayList deliveryRequestList = new ArrayList();
            deliveryRequestList = wmsBP.getviewDeliveryRequest(wmsTo);
            request.setAttribute("deliveryRequestList", deliveryRequestList);

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView connectDispatchView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        WmsTO wmsTO = new WmsTO();
        try {
            String param = "";
            wmsTO.setWhId(whId);
            param = request.getParameter("param");
            String dispatchId = request.getParameter("dispatchId");
            request.setAttribute("dispatchId", dispatchId);
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            wmsTO.setDispatchId(dispatchId);
            if ("view".equals(param)) {
                ArrayList getConnectDispatchViewDetails = new ArrayList();
                getConnectDispatchViewDetails = wmsBP.getConnectDispatchViewDetails(wmsTO);
                request.setAttribute("connectDispatchViewDetails", getConnectDispatchViewDetails);
                path = "content/wms/connectDispatchViewDetails.jsp";
            } else if ("delete".equals(param)) {
                String invoiceNo = request.getParameter("invoiceNo");
                wmsTO.setInvoiceNo(invoiceNo);
                int deleteConnectDispatchInvoice = wmsBP.deleteConnectDispatchInvoice(wmsTO);
                ArrayList getConnectDispatchViewDetails = new ArrayList();
                getConnectDispatchViewDetails = wmsBP.getConnectDispatchViewDetails(wmsTO);
                request.setAttribute("connectDispatchViewDetails", getConnectDispatchViewDetails);
                path = "content/wms/connectDispatchViewDetails.jsp";
            } else {
                path = "content/wms/ConnectDispatchView.jsp";
            }
            wmsTO.setStatus("5");
            ArrayList getDispatchDetails = new ArrayList();
            getDispatchDetails = wmsBP.getDispatchDetails(wmsTO);
            request.setAttribute("getDispatchDetails", getDispatchDetails);
            System.out.println("getDispatchDetails.size()  " + getDispatchDetails.size());
            request.setAttribute("dispatchSize", getDispatchDetails.size());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView getDeliveryRequestUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();

        String pageTitle = "View Order Details";
        menuPath = "Operation >>  Order Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        Date dNow = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(dNow);
        cal.add(Calendar.DATE, 0);
        dNow = cal.getTime();

        try {
            path = "content/trip/deliveryRequestUpdate.jsp";
            String orderId = request.getParameter("orderId");

            wmsTO.setOrderId(orderId);
            ArrayList getDeliveryRequestUpdate = new ArrayList();
            request.setAttribute("orderId", orderId);
            getDeliveryRequestUpdate = wmsBP.getDeliveryRequestUpdate(wmsTO);
            System.out.println("getDeliveryRequestUpdate " + getDeliveryRequestUpdate.size());
            request.setAttribute("getDeliveryRequestUpdate", getDeliveryRequestUpdate);
            WmsTO wmsTO1 = new WmsTO();
            if (getDeliveryRequestUpdate.size() > 0) {
                Iterator itr = getDeliveryRequestUpdate.iterator();
                while (itr.hasNext()) {
                    wmsTO1 = (WmsTO) itr.next();
                    request.setAttribute("delRequestId", wmsTO1.getDelRequestId());
                    request.setAttribute("loanProposalId", wmsTO1.getLoanProposalId());
                    request.setAttribute("createdDate", wmsTO1.getCreatedDate());
                    request.setAttribute("productId", wmsTO1.getProductId());
                    request.setAttribute("serialNumber", wmsTO1.getSerialNumber());
                    request.setAttribute("orderId", wmsTO1.getOrderId());
                    request.setAttribute("qty", wmsTO1.getQty());
                    request.setAttribute("customerName", wmsTO1.getCustomerName());
                    request.setAttribute("model", wmsTO1.getModel());
                    request.setAttribute("branch", wmsTO1.getBranchName());
                    request.setAttribute("orderDate", wmsTO1.getBatchDate());
                    request.setAttribute("itemId", wmsTO1.getItemId());
                    request.setAttribute("warehouseId", wmsTO1.getWarehouseId());
                }
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*
         * run time exception has occurred. Directed to error page.
         */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateDeliveryStatus(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();

        String menuPath = "Operation  >>  View TripSheet ";
        String path = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            WmsTO wmsTO = new WmsTO();

            String delRequestId = request.getParameter("delRequestId");
            String loanProposalId = request.getParameter("loanProposalId");
            String createdDate = request.getParameter("createdDate");
            String productId = request.getParameter("productId");
            String serialNumber = request.getParameter("serialNumber");
            String orderId = request.getParameter("orderId");
            String qty = request.getParameter("qty");
            String itemId = request.getParameter("itemId");
            String warehouseId = request.getParameter("warehouseId");

            wmsTO.setDelRequestId(delRequestId);
            wmsTO.setLoanProposalId(loanProposalId);
            wmsTO.setCreatedDate(createdDate);
            wmsTO.setProductId(productId);
            wmsTO.setSerialNumber(serialNumber);
            wmsTO.setOrderId(orderId);
            wmsTO.setQty(qty);
            wmsTO.setItemId(itemId);
            wmsTO.setWarehouseId(warehouseId);
            wmsTO.setUserId(userId + "");

            int status = wmsBP.getDeliveryRequestFinalUpdate(wmsTO, userId);
            System.out.println("status :" + status);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Request Updated Successfully.");
            } else {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Request Not Updated.");
            }

            path = "content/wms/deliveryRequestMaster.jsp";

            ArrayList deliveryRequestList = new ArrayList();
            deliveryRequestList = wmsBP.getviewDeliveryRequest(wmsTO);
            request.setAttribute("deliveryRequestList", deliveryRequestList);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewAsnMasterList(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Manage Vendor  >>  View ";
        path = "content/wms/receivedAsnMaster.jsp";
        String pageTitle = "viewAsnMasterList";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "viewAsnMasterList";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList asnMasterList = new ArrayList();
            wmsTo.setUserId(userId + "");
            asnMasterList = wmsBP.getAsnMasterList(wmsTo);
            request.setAttribute("asnMasterList", asnMasterList);

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewReceivedGRN(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Manage Vendor  >>  View ";
        path = "content/wms/receivedAsnDetails.jsp";
        String pageTitle = "viewReceivedGRN";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "viewReceivedGRN";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String asnId = request.getParameter("asnId");
            wmsTo.setAsnId(asnId);
            ArrayList asnDetails = new ArrayList();
            asnDetails = wmsBP.getAsnDetails(wmsTo);
            request.setAttribute("asnDetails", asnDetails);
            System.out.println("asnDetails.size()----" + asnDetails.size());

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void saveSerialTempDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            String SerialNo = request.getParameter("serialNo");
            String serialNoAlreadyExists = "";
            serialNoAlreadyExists = wmsBP.serialNoAlreadyExists(SerialNo);
            System.out.println("serialnoexist==========" + serialNoAlreadyExists);
            if ("1".equals(serialNoAlreadyExists)) {
                existsStatus = "Already Used Serial No";
            } else if ("2".equals(serialNoAlreadyExists)) {
                existsStatus = "Already Scanned Serial No";
            } else if ("3".equals(serialNoAlreadyExists)) {
                existsStatus = ""; //new Serial No
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(existsStatus);
            writer.close();
            request.setAttribute("serialNoAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public void checkExistSku(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            String skuCode = request.getParameter("skuCode");
            String skuCodeAlreadyExists = "";
            skuCodeAlreadyExists = wmsBP.checkExistSkuCode(skuCode);
            System.out.println("serialnoexist==========" + skuCodeAlreadyExists);
            if ("0".equals(skuCodeAlreadyExists)) {

            } else if ("1".equals(skuCodeAlreadyExists)) {
                existsStatus = "Already Exists Sku Code";
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(existsStatus);
            writer.close();
            request.setAttribute("skuCodeAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public void checkExistSap(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            String sapCode = request.getParameter("sapCode");
            String sapCodeAlreadyExists = "";
            sapCodeAlreadyExists = wmsBP.checkExistSapCode(sapCode);
            System.out.println("serialnoexist==========" + sapCodeAlreadyExists);
            if ("0".equals(sapCodeAlreadyExists)) {

            } else if ("1".equals(sapCodeAlreadyExists)) {
                existsStatus = "Already Exists Sap Code";
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(existsStatus);
            writer.close();
            request.setAttribute("sapCodeAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public void checkRpExistSerialNo(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            String SerialNo = request.getParameter("serialNo");
            String serialNoAlreadyExists = "";
            serialNoAlreadyExists = wmsBP.checkRpExistSerialNo(SerialNo);
            System.out.println("serialnoexist==========" + serialNoAlreadyExists);
            if ("1".equals(serialNoAlreadyExists)) {
                existsStatus = "Already Used Serial No";
            } else if ("2".equals(serialNoAlreadyExists)) {
                existsStatus = "Already Scanned Serial No";
            } else if ("3".equals(serialNoAlreadyExists)) {
                existsStatus = ""; //new Serial No
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(existsStatus);
            writer.close();
            request.setAttribute("serialNoAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public void rpSelectSerialId(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            String serialNo = request.getParameter("serialNo");
            String productId = request.getParameter("productId");
            wmsTO.setSerialNo(serialNo);
            wmsTO.setProductId(productId);
            String rpSelectSerialId = "";
            rpSelectSerialId = wmsBP.rpSelectSerialId(wmsTO);
            System.out.println("rpSelectSerialId==========" + rpSelectSerialId);
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(rpSelectSerialId);
            writer.close();
            request.setAttribute("serialNoAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public void getRpExistSerialNo(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");

        WmsTO wmsTO = new WmsTO();
        try {
            String serialNo = request.getParameter("serialNo");
            String productId = request.getParameter("productId");
            wmsTO.setSerialNo(serialNo);
            wmsTO.setWhId(hubId);
            wmsTO.setUserId(userId + "");
            wmsTO.setProductId(productId);
            String getRpExistSerialNo = "";
            getRpExistSerialNo = wmsBP.getRpExistSerialNo(wmsTO);
            System.out.println("getRpExistSerialNo==========" + getRpExistSerialNo);
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(getRpExistSerialNo);
            writer.close();
            request.setAttribute("serialNoAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public void checkEwayBillNo(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            String ewayNo = request.getParameter("ewayBillNo");
            String checkEwayBillNo = "";
            checkEwayBillNo = wmsBP.checkEwayBillNo(ewayNo);
            System.out.println("serialnoexist==========" + checkEwayBillNo);
            if ("1".equals(checkEwayBillNo)) {
                existsStatus = "Already Used Eway Bill No";
            } else if ("2".equals(checkEwayBillNo)) {
                existsStatus = ""; //new Serial No
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(existsStatus);
            writer.close();
            request.setAttribute("ewayAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public void checkRouteCode(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            String routeCode = request.getParameter("routeCode");
            String checkRouteCode = "";
            checkRouteCode = wmsBP.checkRouteCode(routeCode);
            if ("1".equals(checkRouteCode)) {
                existsStatus = "Route Code Exists";
            } else if ("2".equals(checkRouteCode)) {
                existsStatus = ""; //new Serial No
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(existsStatus);
            writer.close();
            request.setAttribute("serialNoAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public void checkInvoiceExist(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            String InvoiceNo = request.getParameter("invoiceNo");
            String invoiceNoAlreadyExists = "";
            invoiceNoAlreadyExists = wmsBP.invoiceNoAlreadyExists(InvoiceNo);
            System.out.println("serialnoexist==========" + invoiceNoAlreadyExists);
            if ("1".equals(invoiceNoAlreadyExists)) {
                existsStatus = "Already Used InvoiceNo";
            } else if ("2".equals(invoiceNoAlreadyExists)) {
                existsStatus = ""; //new Serial No
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(existsStatus);
            writer.close();
            request.setAttribute("invoiceNoAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public ModelAndView saveGrnSerialDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            wmsTO.setAsnId(request.getParameter("asnId"));
            wmsTO.setSerialNos(request.getParameterValues("serialNos"));
            wmsTO.setUom(request.getParameterValues("uom"));
            wmsTO.setProCond(request.getParameterValues("proCond"));
            wmsTO.setIpQty(request.getParameterValues("ipQty"));
            if (wmsCommand.getItemId() != null && wmsCommand.getItemId() != "") {
                wmsTO.setItemId(wmsCommand.getItemId());
            }
            wmsTO.setQty(request.getParameter("qty"));
            wmsTO.setBinIds(request.getParameterValues("bins"));
            wmsTO.setEmpUserId(request.getParameter("empUserId"));
            wmsTO.setGtnDetId(request.getParameter("gtnDetId"));
            wmsTO.setCustId(request.getParameter("custId"));
            wmsTO.setGtnId(request.getParameter("gtnId"));

            int tempGrnDetails = 0;

            ArrayList serialTempDetails = new ArrayList();
            tempGrnDetails = wmsBP.saveTempGrnDetails(wmsTO, userId);
            if (tempGrnDetails > 0) {
                serialTempDetails = wmsBP.getSerialTempDetailsUnsed(wmsTO);
                request.setAttribute("serialTempDetails", serialTempDetails);
                double reqQty = Double.parseDouble(request.getParameter("reqQuantity"));
                double actQty = serialTempDetails.size();
                wmsTO.setReqQty(reqQty);
                wmsTO.setActQty(actQty);

                double unusedQty = 0;
                System.out.println("unusedQty-----" + unusedQty);
                wmsTO.setUnusedQty(unusedQty);

                int status = 0;
                status = wmsBP.saveGrnSerialDetails(wmsTO, userId);
                System.out.println("saveSerialTempDetails---" + status);

                ArrayList asnMasterList = new ArrayList();
                wmsTO.setUserId(userId + "");
                asnMasterList = wmsBP.getAsnMasterList(wmsTO);
                request.setAttribute("asnMasterList", asnMasterList);

                System.out.println("asnMasterList......" + asnMasterList.size());
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "GRN Created Successfully.");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "GRN Created Failed");

                }
            }
            ArrayList receivedGateIn = new ArrayList();
            receivedGateIn = wmsBP.getReceivedGateIn(userId);
            request.setAttribute("receivedGateIn", receivedGateIn);
            path = "content/wms/receivedGateIn.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewDockInList(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Manage Vendor  >>  View ";
        String pageTitle = "viewDockInList";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "viewDocketList";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            wmsTo.setUserId(userId + "");
            ArrayList getDockInList = new ArrayList();
            getDockInList = wmsBP.getDockInList(wmsTo);
            request.setAttribute("getDockInList", getDockInList);

            path = "content/wms/viewDockInList.jsp";

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewDockInDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "viewGRNDetails  >>  View ";
        path = "content/wms/viewDockInDetails.jsp";
        String pageTitle = "viewGRNDetails";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "viewGRNDetails";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String asnId = request.getParameter("asnId");
            String grnId = request.getParameter("grnId");
            String itemId = request.getParameter("itemId");
            String gtnId = request.getParameter("gtnId");
            wmsTo.setAsnId(asnId);
            wmsTo.setItemId(itemId);
            wmsTo.setGrnId(grnId);
            wmsTo.setGtnId(gtnId);
            System.out.println("itemId----" + itemId);
            ArrayList getDockInDetails = new ArrayList();
            getDockInDetails = wmsBP.getDockInDetails(wmsTo);
            request.setAttribute("getDockInDetails", getDockInDetails);
            System.out.println("getDockInDetails.size()----" + getDockInDetails.size());

            ArrayList serialTempDetails = new ArrayList();
            serialTempDetails = wmsBP.getSerialTempDetails(wmsTo);
            request.setAttribute("serialTempDetails", serialTempDetails);
            int serialTempSize = serialTempDetails.size();
            request.setAttribute("serialTempSize", serialTempSize);
            ArrayList getBinList = new ArrayList();
            getBinList = wmsBP.getBinList(wmsTo);
            request.setAttribute("getBinList", getBinList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveGrnSerialStockDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        wmsCommand = command;
        String menuPath = "DockIn Details";
        int insertStatus = 0;
        ModelAndView mv = null;
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int j = 0;
        int n = 0;
        int p = 0;
        int q = 0;
        int r = 0;
        int s = 0;
        int t = 0;
        String[] fileSaved = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        String[] serialNos = new String[500];
        String[] binIds = new String[500];
        String itemId = "";
        String asnId = "";
        String inpQty = "";
        String invoiceNo = "";
        String invoiceDate = "";
        String ewaybillNo = "";
        String whId = "";
        String grnId = "";
        String binId = "";
        String custId = "";
        try {

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Dockin Details";
            request.setAttribute("pageTitle", pageTitle);

            WmsTO wmsTO = new WmsTO();

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                        // actualServerFilePath = "D:\\HS\\Images";
                        System.out.println("Server Path == " + actualServerFilePath);
//                        tempServerFilePath = actualServerFilePath.replace("//", "////");
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        Date now = new Date();
                        String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                        String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                        fPart = (FilePart) partObj;
                        uploadedFileName[j] = fPart.getFileName();

                        if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {

                            String[] splitFileName = uploadedFileName[j].split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath[j] = tempServerFilePath + "//" + fileSaved[j];
                            actualFilePath = actualServerFilePath + "//" + tempFilePath;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath[j]));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
//                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
//                            String part1 = parts.replace("\\", "");
                        }

                        System.out.println("fileName..." + fileName);
                        j++;
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("itemId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            itemId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("asnId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            asnId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("inpQty")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            inpQty = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("custId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            custId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("invoiceNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            invoiceNo = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("invoiceDate")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            invoiceDate = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("ewaybillNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            ewaybillNo = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("whId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            whId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("grnId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            grnId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("binId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            binId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("serialNos")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            serialNos[s] = paramPart.getStringValue();
                            System.out.println("serialNos " + serialNos);
                            s++;
                        }
                        if (partObj.getName().equals("binIds")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            binIds[t] = paramPart.getStringValue();
                            System.out.println("serialNos " + serialNos);
                            t++;
                        }

                    }
                }
            }

//             wmsTO.setSerialNos(request.getParameterValues("serialNos"));
            String file = "";
            String file1 = "";
            String saveFile = "";
            String saveFile1 = "";
            int status = 0;

            wmsTO.setItemId(itemId);
            wmsTO.setAsnId(asnId);
            wmsTO.setInvoiceNo(invoiceNo);
            wmsTO.setInvoiceDate(invoiceDate);
            wmsTO.setEwaybillNo(ewaybillNo);
            wmsTO.setWhId(whId);
            wmsTO.setGrnId(grnId);
            wmsTO.setBinId(binId);
            wmsTO.setRequestQty(inpQty);
            wmsTO.setSerialNos(serialNos);
            wmsTO.setBinIds(binIds);
            wmsTO.setCustId(custId);
            wmsTO.setInpQty(inpQty);
            System.out.println("value of j" + j);
            file = tempFilePath[0];
            file1 = tempFilePath[1];
            System.out.println("file :" + file);
            System.out.println("file :" + file1);
            saveFile = fileSaved[0];
            saveFile1 = fileSaved[1];
            System.out.println("saveFile :" + saveFile);
            System.out.println("saveFile :" + saveFile1);

            status = wmsBP.saveGrnSerialStockDetails(wmsTO, file, file1, saveFile, saveFile1, userId);

            System.out.println("saveGrnSerialStockDetails---" + status);

            wmsTO.setUserId(userId + "");
            ArrayList getDockInList = new ArrayList();
            getDockInList = wmsBP.getDockInList(wmsTO);
            request.setAttribute("getDockInList", getDockInList);

            System.out.println("getDockInList......" + getDockInList.size());
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "GRN Stock Created Successfully.");
            } else {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "GRN Stock Failed");

            }
            path = "content/wms/viewDockInList.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView grnView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Operation  >>  View Trip Sheet Report";
        String statusId1 = request.getParameter("statusId");
        System.out.println("pass value:" + request.getParameter("successMessage"));
        request.setAttribute("errorMessage", request.getParameter("msg"));

        try {

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Grn View Repor";
            request.setAttribute("pageTitle", pageTitle);
            WmsTO wmsTO = new WmsTO();
            int userId = (Integer) session.getAttribute("userId");
            String userIds = String.valueOf(userId);
            wmsTO.setUserId(userIds);

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));

            String fromDate = "";
            String toDate = "";

            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");

            if (fromDate != null) {
                request.setAttribute("fromDate", fromDate);
                wmsTO.setFromDate(fromDate);
            } else {
                fromDate = "";
            }

            if (toDate != null) {
                wmsTO.setToDate(toDate);
                request.setAttribute("toDate", toDate);
            } else {
                toDate = "";
            }

            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            String whId = request.getParameter("fleetCenterId");
            wmsTO.setWhId(whId);

            String cityId = request.getParameter("cityList");
            wmsTO.setId(cityId);

            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseList();
            System.out.println("getWareHouseList " + getWareHouseList.size());
            request.setAttribute("getWareHouseList", getWareHouseList);

            ArrayList getCityList = new ArrayList();
            getCityList = wmsBP.getCityList();
            System.out.println("getCityList " + getCityList.size());
            request.setAttribute("getCityList", getCityList);

            ArrayList grnViewDetails = new ArrayList();
            grnViewDetails = wmsBP.grnViewDetails(wmsTO);
            request.setAttribute("grnViewDetails", grnViewDetails);

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                path = "content/report/grnViewReportExcel.jsp";
            } else {
                path = "content/wms/grnViewReport.jsp";
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView rpStockInward(HttpServletRequest request, HttpServletResponse response) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        String userIds = String.valueOf(userId);
        wmsTO.setUserId(userIds);
        WmsTO wms = new WmsTO();

        try {
            String whId = (String) session.getAttribute("hubId");
            wmsTO.setUserId(userId + "");
            wmsTO.setWhId(whId);
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                String productId = request.getParameter("itemId");
                String qty = request.getParameter("qty");
                String[] serialNos = request.getParameterValues("serialNos");
                String[] uom = request.getParameterValues("uom");
                String[] proCond = request.getParameterValues("proCond");
                String[] binIds = request.getParameterValues("bins");
                String[] storageIds = request.getParameterValues("storages");
                String[] ipQty = request.getParameterValues("ipQty");
                wmsTO.setBinIds(binIds);
                wmsTO.setProCond(proCond);
                wmsTO.setUom(uom);
                wmsTO.setSerialNos(serialNos);
                wmsTO.setQty(qty);
                wmsTO.setIpQty(ipQty);
                wmsTO.setStorageIds(storageIds);
                wmsTO.setProductId(productId);
                int insertStatus = wmsBP.saveRpStockInward(wmsTO);
                if (insertStatus > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated Successfully.");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Update Failed.");
                }
            } else if ("upload".equals(param)) {

                String newFileName = "", actualFilePath = "";
                String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
                boolean isMultipart = false;
                Part partObj = null;
                FilePart fPart = null;
                String[] fileSaved = new String[10];
                String uploadedFileName = "";
                String fileFormat = "";
                String tempFilePath = "";
                isMultipart = ServletFileUpload.isMultipartContent(request);
                if (isMultipart) {
                    MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                    while ((partObj = parser.readNextPart()) != null) {
                        System.out.println("part Name:" + partObj.getName());
                        if (partObj.isFile()) {
                            actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                            System.out.println("Server Path == " + actualServerFilePath);
                            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                            System.out.println("Server Path After Replace== " + tempServerFilePath);
                            fPart = (FilePart) partObj;
                            uploadedFileName = fPart.getFileName();
                            System.out.println("uploadedFileName====" + uploadedFileName);
                            String[] splitFileNames = uploadedFileName.split("\\.");
                            int len = splitFileNames.length;
                            fileFormat = splitFileNames[len - 1];
                            System.out.println("fileFormat==" + fileFormat);
                            if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                                System.out.println("Ses");
                                if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                    String[] splitFileName = uploadedFileName.split("\\.");
                                    fileSavedAs = uploadedFileName;
                                    System.out.println("splitFileName[0]=" + splitFileName[0]);
                                    System.out.println("splitFileName[0]=" + splitFileName[1]);

                                    fileName = fileSavedAs;
                                    tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                    actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                    System.out.println("tempPath..." + tempFilePath);
                                    System.out.println("actPath..." + actualFilePath);
                                    long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                    System.out.println("fileSize..." + fileSize);
                                    File f1 = new File(actualFilePath);
                                    System.out.println("check " + f1.isFile());
                                    f1.renameTo(new File(tempFilePath));
                                    System.out.println("tempPath = " + tempFilePath);
                                    int j = 1;
                                    int sno = 0;
                                    String qty = "0";
                                    DataFormatter formatter = new DataFormatter(Locale.US);
                                    if ("xlsx".equals(fileFormat)) {
                                        FileInputStream fis = new FileInputStream(tempFilePath);
                                        XSSFWorkbook wb = new XSSFWorkbook(fis);
                                        XSSFSheet sheet = wb.getSheetAt(0);
                                        int value = sheet.getLastRowNum();
                                        System.out.println("value  " + value);

                                        String productId = request.getParameter("productId");
                                        qty = request.getParameter("qty");
                                        String binId = request.getParameter("binId1");
                                        String storageId = request.getParameter("storageId1");
                                        String[] serialNos = new String[value];
                                        String[] uom = new String[value];
                                        String[] proCond = new String[value];
                                        String[] binIds = new String[value];
                                        String[] ipQty = new String[value];
                                        String[] storageIds = new String[value];
                                        String exists = "";
                                        String serialNo = "";
                                        Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                        while (itr.hasNext()) {
                                            Row row = itr.next();
                                            if (sno > 0 && sno <= value) {
                                                System.out.println("sno  " + sno);
                                                System.out.println("j  " + j);
                                                serialNo = formatter.formatCellValue(row.getCell(0)) + "";
                                                exists = wmsBP.checkRpExistSerialNo(serialNo);
                                                if ("3".equals(exists) && !"".equals(serialNo)) {
                                                    System.out.println("cell " + row.getCell(0));
                                                    serialNos[j - 1] = serialNo;
                                                    proCond[j - 1] = "1";
                                                    uom[j - 1] = "2";
                                                    binIds[j - 1] = binId;
                                                    ipQty[j - 1] = "1";
                                                    storageIds[j - 1] = storageId;
                                                    j++;
                                                }
                                            }
                                            serialNo = "";
                                            sno++;
                                        }
                                        wmsTO.setBinIds(binIds);
                                        wmsTO.setProCond(proCond);
                                        wmsTO.setUom(uom);
                                        wmsTO.setSerialNos(serialNos);
                                        wmsTO.setQty(qty);
                                        wmsTO.setIpQty(ipQty);
                                        wmsTO.setProductId(productId);
                                        wmsTO.setStorageIds(storageIds);
                                        System.out.println("hi  +  " + serialNos.length);
                                    } else if ("xls".equals(fileFormat)) {
                                        WorkbookSettings ws = new WorkbookSettings();
                                        ws.setLocale(new Locale("en", "EN"));
//                                        HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(new File(actualFilePath)));
//                                        HSSFSheet s = wb.getSheetAt(0);
                                        Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                        Sheet s = workbook.getSheet(0);

                                        int value = s.getRows() - 1;
                                        System.out.println("value  " + value);
                                        String productId = request.getParameter("productId");
                                        qty = request.getParameter("qty");
                                        String binId = request.getParameter("binId1");
                                        String storageId = request.getParameter("storageId1");
                                        String[] serialNos = new String[value];
                                        String[] uom = new String[value];
                                        String[] proCond = new String[value];
                                        String[] binIds = new String[value];
                                        String[] ipQty = new String[value];
                                        String[] storageIds = new String[value];
                                        String exists = "";
                                        for (int i = 1; i < s.getRows(); i++) {
                                            System.out.println("rows  " + s.getCell(0, i).getContents());
                                            exists = wmsBP.checkRpExistSerialNo(s.getCell(0, i).getContents());
                                            if ("3".equals(exists)) {
                                                serialNos[i - 1] = s.getCell(0, i).getContents();
                                                proCond[j - 1] = "1";
                                                uom[j - 1] = "2";
                                                binIds[j - 1] = binId;
                                                ipQty[j - 1] = "1";
                                                storageIds[j - 1] = storageId;
                                                j++;
                                            }
                                        }
                                        wmsTO.setBinIds(binIds);
                                        wmsTO.setProCond(proCond);
                                        wmsTO.setUom(uom);
                                        wmsTO.setSerialNos(serialNos);
                                        wmsTO.setQty(qty);
                                        wmsTO.setIpQty(ipQty);
                                        wmsTO.setProductId(productId);
                                        wmsTO.setStorageIds(storageIds);
                                    }
                                    int insertStatus = wmsBP.saveRpStockInward(wmsTO);
                                    if (insertStatus > 0) {
                                        if (j > 1) {
                                            if (j == Integer.parseInt(qty)) {
                                                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated Successfully.");
                                            } else {
                                                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Only " + qty + " Updated Successfully.");
                                            }
                                        } else {
                                            request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Update Failed.");
                                        }
                                    } else {
                                        request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Update Failed.");
                                    }
                                }
                            } else {
                                request.setAttribute("errorMessage", "File Upload Failed:");
                            }
                        }

                    }
                }
            }
            ArrayList getBinDetails = new ArrayList();
            getBinDetails = wmsBP.rpBinDetails(wmsTO);
            request.setAttribute("binDetails", getBinDetails);
            ArrayList getStorageDetails = new ArrayList();
            getStorageDetails = wmsBP.rpStorageDetails(wmsTO);
            request.setAttribute("storageDetails", getStorageDetails);
            path = "content/wms/rpStockInward.jsp";
            System.out.println("start correct point");

            ArrayList getRpWoProductList = new ArrayList();
            getRpWoProductList = wmsBP.getRpWoProductList(wmsTO);
            request.setAttribute("productList", getRpWoProductList);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }

        return new ModelAndView(path);
    }

    public ModelAndView rpWorkOrder(HttpServletRequest request, HttpServletResponse response) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        String userIds = String.valueOf(userId);
        wmsTO.setUserId(userIds);
        WmsTO wms = new WmsTO();

        try {
            String whId = (String) session.getAttribute("hubId");
            wmsTO.setUserId(userId + "");
            wmsTO.setWhId(whId);
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                String warehouse = request.getParameter("warehouse");
                wmsTO.setWarehouseId(warehouse);

                String[] qty = request.getParameterValues("qty");
                wmsTO.setQuantitys(qty);

                String[] itemIds = request.getParameterValues("itemId");
                wmsTO.setItemIds(itemIds);

                String workOrderDate = request.getParameter("workOrderDate");
                wmsTO.setWorkOrderDate(workOrderDate);

                String expectedDate = request.getParameter("expectedDate");
                wmsTO.setExpectedDate(expectedDate);

                wmsTO.setWarehouseId(warehouse);
                wmsTO.setWorkOrderDate(workOrderDate);
                wmsTO.setExpectedDate(expectedDate);

                int insertStatus = wmsBP.setRpWorkOrderDetails(wmsTO);
                if (insertStatus > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated Successfully.");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Update Failed.");
                }
            }
            System.out.println("start point");
            ArrayList getrpWorkOrderDetails = new ArrayList();
            getrpWorkOrderDetails = wmsBP.getrpWorkOrderDetails(wmsTO);
            request.setAttribute("getrpWorkOrderDetails", getrpWorkOrderDetails);

            ArrayList getRpWoProductList = new ArrayList();
            getRpWoProductList = wmsBP.getRpWoProductList(wmsTO);
            request.setAttribute("productList", getRpWoProductList);

            path = "content/wms/rpWorkOrder.jsp";
            System.out.println("start correct point");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }

        return new ModelAndView(path);
    }

    public ModelAndView rpWorkOrderView(HttpServletRequest request, HttpServletResponse response) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        String userIds = String.valueOf(userId);
        wmsTO.setUserId(userIds);
        WmsTO wms = new WmsTO();

        try {
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                String woId = request.getParameter("woId");
                wmsTO.setWorkOrderId(woId);
                wmsTO.setStatus("1");
                int updateRpWorkOrderStatus = wmsBP.updateRpWorkOrderStatus(wmsTO);
                path = "content/wms/rpWorkOrderView.jsp";
            } else if ("view".equals(param)) {
                String woId = request.getParameter("woId");
                wmsTO.setWorkOrderId(woId);
                ArrayList rpWorkOrderDetailsView = new ArrayList();
                rpWorkOrderDetailsView = wmsBP.rpWorkOrderDetailsView(wmsTO);
                request.setAttribute("orderDetails", rpWorkOrderDetailsView);
                path = "content/wms/rpWorkOrderDetailsView.jsp";
            } else {
                path = "content/wms/rpWorkOrderView.jsp";
            }
            ArrayList getrpWorkOrderDetails = new ArrayList();
            getrpWorkOrderDetails = wmsBP.getrpWorkOrderDetails(wmsTO);
            request.setAttribute("getrpWorkOrderDetails", getrpWorkOrderDetails);

            System.out.println("start correct point");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView viewSerialNumberDetails(HttpServletRequest request, HttpServletResponse response) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Vehicle  >>  View Vehicle Detail";
//        reportCommand = command;
//        ReportTO report = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        int UserId = (Integer) session.getAttribute("userId");
        try {
            //            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-Add")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            pageTitle = "Alter Vehicle";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            request.setAttribute("pageTitle", pageTitle);
            String param = request.getParameter("param");
            System.out.println("param==" + param);
            if (param.equals("ExportExcel")) {
                request.setAttribute("grnNo", (request.getParameter("grnNo")));
                request.setAttribute("asnNo", (request.getParameter("asnNo")));
                request.setAttribute("asnDate", (request.getParameter("asnDate")).split(" ")[0]);
                request.setAttribute("grnDate", (request.getParameter("grnDate")));
                path = "content/report/grnViewReportExcel.jsp";
            } else {
                path = "content/wms/viewSerialNumberDetails.jsp";
            }
            String grnId = request.getParameter("grnId");
            request.setAttribute("grnId", grnId);
            System.out.println("grnId==" + grnId);
//            report.setItemId(itemId);

            ArrayList viewSerialNumberDetails = new ArrayList();
            viewSerialNumberDetails = wmsBP.viewSerialNumberDetails(grnId);
            System.out.println("viewSerialNumberDetails.size() = " + viewSerialNumberDetails.size());
            request.setAttribute("viewSerialNumberDetails", viewSerialNumberDetails);
            // }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve viewStockQtyDetails data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewOrderDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();

        String pageTitle = "View Order Details";
        menuPath = "Operation >>  Order Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            String param = request.getParameter("param");
            System.out.println("param vale is:" + param);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            if (param != null && param.equals("exportExcel")) {
                path = "content/wms/shipmentViewReportExcel.jsp";
            } else {
                path = "content/wms/shipmentViewReport.jsp";
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));

            String fromDate = "";
            String toDate = "";

            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");

            if (fromDate != null) {
                request.setAttribute("fromDate", fromDate);
                wmsTO.setFromDate(fromDate);
            } else {
                fromDate = "";
            }

            if (toDate != null) {
                wmsTO.setToDate(toDate);
                request.setAttribute("toDate", toDate);
            } else {
                toDate = "";
            }

            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            String whId = request.getParameter("fleetCenterId");
            wmsTO.setWhId(whId);

            request.setAttribute("whId", whId);
            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseList();
            System.out.println("getWareHouseList " + getWareHouseList.size());
            request.setAttribute("getWareHouseList", getWareHouseList);

            ArrayList ordersList = new ArrayList();
            ordersList = wmsBP.getOrderDetailsList(wmsTO);
            System.out.println("ordersList " + ordersList.size());
            request.setAttribute("ordersList", ordersList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView inboundGateIn(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        wmsCommand = command;
        String menuPath = "GATE IN  >>  Insert ";

        String pageTitle = "GTN Details";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTO = new WmsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "GTN Details";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (wmsCommand.getInvoiceNo() != null && wmsCommand.getInvoiceNo() != "") {
                wmsTO.setInvoiceNo(wmsCommand.getInvoiceNo());
            }
            if (wmsCommand.getInvoiceDate() != null && wmsCommand.getInvoiceDate() != "") {
                wmsTO.setInvoiceDate(wmsCommand.getInvoiceDate());
            }
            if (wmsCommand.getGtnDate() != null && wmsCommand.getGtnDate() != "") {
                wmsTO.setGtnDate(wmsCommand.getGtnDate());
            }
            if (wmsCommand.getGtnTime() != null && wmsCommand.getGtnTime() != "") {
                wmsTO.setGtnTime(wmsCommand.getGtnTime());
            }
            if (wmsCommand.getCustId() != null && wmsCommand.getCustId() != "") {
                wmsTO.setCustId(wmsCommand.getCustId());
            }
            if (wmsCommand.getDock() != null && wmsCommand.getDock() != "") {
                wmsTO.setDock(wmsCommand.getDock());
            }
            if (wmsCommand.getVehicleType() != null && wmsCommand.getVehicleType() != "") {
                wmsTO.setVehicleType(wmsCommand.getVehicleType());
            }
            if (wmsCommand.getVehicleNo() != null && wmsCommand.getVehicleNo() != "") {
                wmsTO.setVehicleNo(wmsCommand.getVehicleNo());
            }
            if (wmsCommand.getDriverName() != null && wmsCommand.getDriverName() != "") {
                wmsTO.setDriverName(wmsCommand.getDriverName());
            }
            if (wmsCommand.getRemarks() != null && wmsCommand.getRemarks() != "") {
                wmsTO.setRemarks(wmsCommand.getRemarks());
            }

            String param = request.getParameter("param");
            System.out.println("param===" + param);
            if (param != null && param.equals("Save")) {
                System.out.println("param.invoiceno===" + wmsCommand.getInvoiceNo());
                System.out.println("param.date===" + wmsCommand.getInvoiceDate());
                System.out.println("param.gtndate===" + wmsCommand.getGtnDate());
                System.out.println("param.gtntime===" + wmsCommand.getGtnTime());
                System.out.println("param.Remarks===" + wmsCommand.getRemarks());
                System.out.println("param.dock===" + wmsCommand.getDock());
                System.out.println("param.equals===" + wmsCommand.getVehicleNo());
                System.out.println("param.equals===" + wmsCommand.getVehicleType());

                wmsTO.setPoNos(request.getParameterValues("poNo"));
                wmsTO.setPoQty(request.getParameterValues("poQty"));
                int saveGtnDetails = 0;
                saveGtnDetails = wmsBP.saveGtnDetails(wmsTO, userId);
                path = "content/wms/"
                        + "";
                if (saveGtnDetails != 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "GTN Stock Created Successfully.");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "GTN Stock Failed");
                }
            } else {
                path = "content/wms/inboundGateIn.jsp";
            }
//           
            ArrayList poCode = new ArrayList();
            poCode = wmsBP.getPoCode(wmsTO, userId);
            request.setAttribute("poCode", poCode);
            ArrayList custList = new ArrayList();
            custList = wmsBP.getCustList(wmsTO);
            request.setAttribute("custList", custList);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String invoiceDate = "";
            String gtnDate = "";
            invoiceDate = request.getParameter("invoiceDate");
            gtnDate = request.getParameter("gtnDate");

            if (invoiceDate != null) {
                request.setAttribute("invoiceDate", invoiceDate);
                wmsTO.setInvoiceDate(invoiceDate);
            } else {
                invoiceDate = "";
            }

            if (gtnDate != null) {
                wmsTO.setGtnDate(gtnDate);
                request.setAttribute("gtnDate", gtnDate);
            } else {
                gtnDate = "";
            }

            String endDate = dateFormat.format(curDate);

            if ("".equals(gtnDate)) {
                gtnDate = endDate;
                wmsTO.setToDate(gtnDate);
            }

            request.setAttribute("gtnDate", gtnDate);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView receivedGateIn(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Received GateIn  >>  View ";

        String pageTitle = "receivedGateIn";
        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTo = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "receivedGateIn";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList receivedGateIn = new ArrayList();
            receivedGateIn = wmsBP.getReceivedGateIn(userId);
            request.setAttribute("receivedGateIn", receivedGateIn);
            path = "content/wms/receivedGateIn.jsp";
            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewReceivedGTN(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Manage Vendor  >>  View ";
        String pageTitle = "viewReceivedGTN";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "viewReceivedGTN";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String gtnId = request.getParameter("gtnId");
            wmsTo.setGtnId(gtnId);
            request.setAttribute("gtnId", gtnId);
            ArrayList proList = new ArrayList();
            proList = wmsBP.getProList(userId, wmsTo);
            request.setAttribute("gtnDetails", proList);
            String itemId = request.getParameter("itemId");
            String asnId = request.getParameter("asnId");
            if (asnId != "" && itemId != "") {
                wmsTo.setItemId("itemId");
                wmsTo.setAsnId("asnId");
//            ArrayList serialDetails=new ArrayList();
//            serialDetails = wmsBP.getSerialDetails();
//            request.setAttribute("serialDetails",serialDetails);
//            ArrayList gtnDetails = new ArrayList();
//            gtnDetails = wmsBP.getGtnDetails(wmsTo, userId);
                ArrayList userDetails = new ArrayList();
                userDetails = wmsBP.getUserDetails(userId);
//            ArrayList binDetails=new ArrayList();
//            binDetails=wmsBP.getBinDetails(wmsTo);
//            request.setAttribute("binDetails",binDetails);
                request.setAttribute("userDetails", userDetails);
//            request.setAttribute("gtnDetails", gtnDetails);
//            System.out.println("gtnDetails.size()----"+gtnDetails.size());
                path = "content/wms/receivedGtnDetails.jsp";
            } else {
                path = "content/wms/receivedGtnDetails.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getBinDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();

        ArrayList getBinDetails = new ArrayList();
        PrintWriter pw = response.getWriter();
        try {
            String itemId = "";

            response.setContentType("text/html");
            itemId = request.getParameter("item");
            wmsTO.setItemId(itemId);
            System.out.println("itemId=+++++" + itemId);
            getBinDetails = wmsBP.getBinDetails(wmsTO);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getBinDetails.iterator();
            int cntr = 0;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                wmsTO = (WmsTO) itr.next();
                jsonObject.put("Id", wmsTO.getBinId());
                jsonObject.put("Name", wmsTO.getBinName());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getImPOList(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();

        ArrayList getImPOList = new ArrayList();
        PrintWriter pw = response.getWriter();
        try {
            String materialId = "";
            String hubId = (String) session.getAttribute("hubId");
            wmsTO.setWhId(hubId);
            response.setContentType("text/html");
            materialId = request.getParameter("materialId");
            wmsTO.setMaterialId(materialId);
            System.out.println("materialId=+++++" + materialId);
            getImPOList = wmsBP.getImPOList(wmsTO);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getImPOList.iterator();
            int cntr = 0;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                wmsTO = (WmsTO) itr.next();
                jsonObject.put("poId", wmsTO.getPoId());
                jsonObject.put("poNo", wmsTO.getPoNo());
                jsonObject.put("poQty", wmsTO.getQty());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getImPOMaterialList(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();

        ArrayList getImPOMaterialList = new ArrayList();
        PrintWriter pw = response.getWriter();
        try {
            String poId = "";
            String hubId = (String) session.getAttribute("hubId");
            wmsTO.setWhId(hubId);
            response.setContentType("text/html");
            poId = request.getParameter("poId");
            wmsTO.setPoId(poId);
            System.out.println("poId=+++++" + poId);
            getImPOMaterialList = wmsBP.getImPOMaterialList(wmsTO);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getImPOMaterialList.iterator();
            int cntr = 0;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                wmsTO = (WmsTO) itr.next();
                jsonObject.put("materialId", wmsTO.getMaterialId());
                jsonObject.put("materialName", wmsTO.getMaterial());
                jsonObject.put("poQty", wmsTO.getQty());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void checkConnectSerialNo(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();

        ArrayList checkConnectSerialNo = new ArrayList();
        PrintWriter pw = response.getWriter();
        try {
            String serialNo = request.getParameter("serialNo");
            String invoiceId = request.getParameter("invoiceId");
            String productId = request.getParameter("productId");
            String inpQty = request.getParameter("inpQty");
            String hubId = request.getParameter("whId");
            String scannable = request.getParameter("scannable");
            response.setContentType("text/html");
            wmsTO.setProductId(productId);
            wmsTO.setInvoiceId(invoiceId);
            wmsTO.setScannable(scannable);
            wmsTO.setInpQty(inpQty);
            wmsTO.setSerialNo(serialNo);
            wmsTO.setWhId(hubId);
            JSONArray jsonArray = new JSONArray();
            if ("Y".equals(scannable)) {
                int checkConnectSerialNoExist = wmsBP.checkConnectSerialNoExist(wmsTO);
                if (checkConnectSerialNoExist > 0) {
                    checkConnectSerialNo = wmsBP.checkConnectSerialNo(wmsTO);
                    Iterator itr = checkConnectSerialNo.iterator();
                    int cntr = 0;
                    while (itr.hasNext()) {
                        JSONObject jsonObject = new JSONObject();
                        wmsTO = (WmsTO) itr.next();
                        jsonObject.put("serialId", wmsTO.getSerialId());
                        jsonObject.put("serialNo", wmsTO.getSerialNo());
                        jsonObject.put("rackName", wmsTO.getRackName());
                        jsonObject.put("binName", wmsTO.getBinName());
                        jsonObject.put("storageName", wmsTO.getStorageName());
                        jsonObject.put("stockId", wmsTO.getStockId());
                        jsonObject.put("uom", wmsTO.getUom());
                        jsonObject.put("packQty", wmsTO.getPackQty());
                        jsonObject.put("productName", wmsTO.getProductName());
                        jsonObject.put("qty", wmsTO.getQty());
                        jsonObject.put("status", "Y");
                        System.out.println("jsonObject = " + jsonObject);
                        jsonArray.put(jsonObject);
                        cntr++;
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "N");
                    jsonArray.put(jsonObject);
                }
            } else {
                int checkConnectStockDetail = wmsBP.checkConnectStockDetail(wmsTO);
                if (checkConnectStockDetail > 0) {
                    checkConnectSerialNo = wmsBP.checkConnectSerialNo(wmsTO);
                    Iterator itr = checkConnectSerialNo.iterator();
                    int cntr = 0;
                    while (itr.hasNext()) {
                        JSONObject jsonObject = new JSONObject();
                        wmsTO = (WmsTO) itr.next();
                        jsonObject.put("serialId", wmsTO.getSerialId());
                        jsonObject.put("serialNo", wmsTO.getSerialNo());
                        jsonObject.put("rackName", wmsTO.getRackName());
                        jsonObject.put("binName", wmsTO.getBinName());
                        jsonObject.put("storageName", wmsTO.getStorageName());
                        jsonObject.put("stockId", wmsTO.getStockId());
                        jsonObject.put("uom", wmsTO.getUom());
                        jsonObject.put("packQty", wmsTO.getPackQty());
                        jsonObject.put("productName", wmsTO.getProductName());
                        jsonObject.put("qty", wmsTO.getQty());
                        jsonObject.put("status", "Y");
                        System.out.println("jsonObject = " + jsonObject);
                        jsonArray.put(jsonObject);
                        cntr++;
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "N");
                    jsonArray.put(jsonObject);
                }
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getFkRunsheetDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();

        ArrayList getFkRunsheetDetails = new ArrayList();
        PrintWriter pw = response.getWriter();
        try {
            String shipmentId = "";

            response.setContentType("text/html");
            shipmentId = request.getParameter("id");
            wmsTO.setShipmentId(shipmentId);
            getFkRunsheetDetails = wmsBP.getFkRunsheetDetails(wmsTO);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getFkRunsheetDetails.iterator();
            int cntr = 0;
            if (getFkRunsheetDetails.size() > 0) {
                while (itr.hasNext()) {
                    wmsTO = (WmsTO) itr.next();
                    JSONObject jsonObject = new JSONObject();
                    if ("3".equals(wmsTO.getOrderType())) {
                        jsonObject.put("shipmentId", wmsTO.getShipmentId());
                        jsonObject.put("name", wmsTO.getCustomerName());
                        jsonObject.put("address", wmsTO.getPickupaddress());
                        jsonObject.put("itemName", wmsTO.getItemName());
                        jsonObject.put("orderType", wmsTO.getOrderType());
                        jsonObject.put("date", wmsTO.getPickupDate());
                        jsonObject.put("status", wmsTO.getStatus());
                        jsonObject.put("city", wmsTO.getCity());
                        jsonObject.put("count", wmsTO.getCount());
                        jsonObject.put("stat", "0");
                        jsonArray.put(jsonObject);
                        cntr++;
                    } else {
                        jsonObject.put("stat", "1");
                        jsonArray.put(jsonObject);
                    }
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("stat", "2");
                jsonArray.put(jsonObject);

            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getItemList(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();

        ArrayList getItemList = new ArrayList();
        PrintWriter pw = response.getWriter();
        try {
            String custId = "";
            int userId = (Integer) session.getAttribute("userId");
            custId = request.getParameter("custId");
            getItemList = wmsBP.getItemList(userId, custId);
            response.setContentType("text/html");
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getItemList.iterator();
            int cntr = 0;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                wmsTO = (WmsTO) itr.next();
                jsonObject.put("Id", wmsTO.getItemId());
                jsonObject.put("Name", wmsTO.getItemName());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void checkConnectExistingVehicleTat(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();

        ArrayList getItemList = new ArrayList();
        PrintWriter pw = response.getWriter();
        try {
            String vehicleNo = "";
            int userId = (Integer) session.getAttribute("userId");
            vehicleNo = request.getParameter("vehicleNo");
            String status = request.getParameter("status");
            wmsTO.setVehicleNo(vehicleNo);
            wmsTO.setStatus(status);
            int checkVehicleCount = wmsBP.checkConnectExistingVehicleTat(wmsTO);
            response.setContentType("text/html");
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("count", checkVehicleCount);
            jsonObject.put("status", "1");
            jsonArray.put(jsonObject);
            pw.print(jsonObject);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView editSerialDetail(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Edit GRN  >>  Edit ";
        path = "content/wms/editSerialDetail.jsp";
        String pageTitle = "EditGRN";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "EditGRN";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String itemId = "";
            ArrayList serialList = new ArrayList();
            ArrayList getBinDetails = new ArrayList();
            String gtnId = request.getParameter("gtnId");
            wmsTo.setGtnId(gtnId);
            String grnId = request.getParameter("grnId");
            wmsTo.setGrnId(grnId);
            ArrayList userDetails = new ArrayList();
            userDetails = wmsBP.getUserDetails(userId);
            request.setAttribute("userDetails", userDetails);
            request.setAttribute("gtnId", gtnId);
            ArrayList proList = new ArrayList();
            proList = wmsBP.getProList(userId, wmsTo);
            request.setAttribute("proList", proList);
            itemId = request.getParameter("itemId");
            request.setAttribute("grnId", grnId);

            int delGrnTempSerial = 0;
            String tempId = request.getParameter("tempId");
            if (tempId != "" && tempId != null) {
                delGrnTempSerial = wmsBP.delGrnTempSerial(tempId, grnId);
            }

            if (itemId != "" && itemId != null) {
                String asnId = request.getParameter("asnId");
                wmsTo.setAsnId(request.getParameter("asnId"));
                request.setAttribute("asnId", request.getParameter("asnId"));
                wmsTo.setItemId(itemId);
                ArrayList proListDetails = new ArrayList();
                proListDetails = wmsBP.proListDetails(userId, wmsTo);
                request.setAttribute("proListDet", proListDetails);
                request.setAttribute("itemId", itemId);
                System.out.println("itemId=+++++" + itemId);
                getBinDetails = wmsBP.getBinDetails(wmsTo);
                request.setAttribute("binDetails", getBinDetails);
                System.out.println("binListSize" + getBinDetails.size());
                serialList = wmsBP.getSerialDets(gtnId, grnId, itemId, asnId);
                request.setAttribute("serialDet", serialList);
                request.setAttribute("size", serialList.size());
                System.out.println("getSerialDet.size() = " + serialList.size());

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView editGrnSerialDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            wmsTO.setAsnId(request.getParameter("asnId"));
            wmsTO.setSerialNos(request.getParameterValues("serialNos"));
            wmsTO.setUom(request.getParameterValues("uom"));
            wmsTO.setProCond(request.getParameterValues("proCond"));
            wmsTO.setIpQty(request.getParameterValues("ipQty"));
            wmsTO.setQty(request.getParameter("qty"));
            wmsTO.setBinIds(request.getParameterValues("bins"));
            wmsTO.setEmpUserId(request.getParameter("empUserId"));
            wmsTO.setGtnDetId(request.getParameter("gtnDetId"));
            wmsTO.setGtnId(request.getParameter("gtnId"));
            wmsTO.setGrnId(request.getParameter("grnId1"));
            wmsTO.setItemId(request.getParameter("itemId1"));

            int updateSerialTempDetails = 0;
            updateSerialTempDetails = wmsBP.updateSerialTempDetails(wmsTO, userId);
            if (updateSerialTempDetails > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "GRN Created Successfully.");
            } else {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "GRN Created Failed");

            }
            ArrayList receivedGateIn = new ArrayList();
            receivedGateIn = wmsBP.getReceivedGateIn(userId);
            request.setAttribute("receivedGateIn", receivedGateIn);
            path = "content/wms/receivedGateIn.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView addBarcodeMaster(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("ProductCategory...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String barcodeMaster = "";
        WmsTO wmsTO = new WmsTO();
        String menuPath = "";
        int generateBarcode = 0;
        menuPath = "Operation  >> handleBarcodeMaster ";
        String pageTitle = "addBarcodeMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            ArrayList custList = new ArrayList();
            ArrayList barCodeDetails = new ArrayList();
            custList = wmsBP.getCustList(wmsTO);
            request.setAttribute("custList", custList);

            barcodeMaster = wmsBP.getBarcodeMaster();
            request.setAttribute("barcodeMaster", barcodeMaster);

            String param = request.getParameter("param");
            if ("Generate".equals(param)) {
                wmsTO.setBarcode(request.getParameter("barCodeNo"));
                wmsTO.setCount(Integer.parseInt(request.getParameter("count")));
                wmsTO.setCustId(request.getParameter("custId"));
                wmsTO.setItemId(request.getParameter("itemId"));
                generateBarcode = wmsBP.generateBarcode(wmsTO, userId);
                if (generateBarcode > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Barcode Generated Successfully.");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Barcode Generation Failed");

                }
                barcodeMaster = wmsBP.getBarcodeMaster();
                request.setAttribute("barcodeMaster", barcodeMaster);
                path = "content/wms/addBarcodeMaster.jsp";
            } else if ("Search".equals(param)) {
                wmsTO.setCustId(request.getParameter("custId"));
                wmsTO.setItemId(request.getParameter("itemId"));
                request.setAttribute("custId", request.getParameter("custId"));
                request.setAttribute("itemId", request.getParameter("itemId"));
                barCodeDetails = wmsBP.barCodeDetails(wmsTO);
                request.setAttribute("barCodeDetails", barCodeDetails);

                path = "content/wms/addBarcodeMaster.jsp";
            } else {

                path = "content/wms/addBarcodeMaster.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView warehouseOrderReport(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Report >>  Hub Report";
        wmsCommand = command;
        WmsTO wms = new WmsTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        int userId = (Integer) session.getAttribute("userId");
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            request.setAttribute("pageTitle", pageTitle);
            String param = request.getParameter("param");

            ArrayList stateList = new ArrayList();
            stateList = wmsBP.getStateList();
            request.setAttribute("stateList", stateList);

            ArrayList whList = new ArrayList();
            whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);

            if ("search".equals(param)) {

                String fromDate = request.getParameter("fromDate");
                String toDate = request.getParameter("toDate");
                wms.setFromDate(fromDate);
                wms.setToDate(toDate);

                String whId = request.getParameter("whId");
                wms.setWhId(whId);
                String stateId = request.getParameter("stateId");
                wms.setStateId(stateId);
                ArrayList warehouseOrderReport = new ArrayList();
                warehouseOrderReport = wmsBP.warehouseOrderReport(wms);
                request.setAttribute("warehouseOrderReport", warehouseOrderReport);
                path = "content/wms/warehouseOrderReport.jsp";
            } else {
                path = "content/wms/warehouseOrderReport.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to get Hub data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewAsnMaster(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String Path = "";
        String menuPath = "GATE IN  >>  Insert ";

        String pageTitle = "GTN Details";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String custId = "";
        custId = request.getParameter("custId");
        System.out.println("custtt=========" + custId);
        WmsTO wmsTO = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "GTN Details";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            Path = "content/wms/addAsnRecords.jsp";
            ArrayList getItemList = new ArrayList();
            getItemList = wmsBP.getItemLists(userId);
            request.setAttribute("getItemList", getItemList);
            System.out.println("ControllerSizegetList=====" + getItemList.size());
            ArrayList vendorList = new ArrayList();
            vendorList = wmsBP.processVendorList();
            System.out.println("ControllerSize=====" + vendorList.size());
            request.setAttribute("vendorList", vendorList);

            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseList();
            System.out.println("getWareHouseList " + getWareHouseList.size());
            request.setAttribute("getWareHouseList", getWareHouseList);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(Path);

    }

    public ModelAndView updateAnsRecord(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();

        String menuPath = "Operation  >>  View TripSheet ";
        String path = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            WmsTO wmsTO = new WmsTO();

            String remark = request.getParameter("remark");
            String Quantity = request.getParameter("Quantity");
            String warehouseId = request.getParameter("warehouseId");
            String itemId = request.getParameter("itemId");
            String vendorId = request.getParameter("vendorId");
            String poDate = request.getParameter("poDate");
            String PoRequestId = request.getParameter("PoRequestId");
            String poNumber = request.getParameter("poNumber");

            System.out.println("remark");
            System.out.println("Quantity" + Quantity);
            System.out.println("remark" + poDate);
            System.out.println("remark" + PoRequestId);

            wmsTO.setRemark(remark);
            wmsTO.setQuantity(Quantity);
            wmsTO.setPoRequestId(PoRequestId);
            wmsTO.setPoNumber(poNumber);
            wmsTO.setVendorId(vendorId);
            wmsTO.setPoDate(poDate);
            wmsTO.setWarehouseId(warehouseId);
            wmsTO.setItemId(itemId);

            int status = wmsBP.insertAsnDetails(wmsTO);
            System.out.println("status :" + status);
            path = "content/wms/addAsnRecords.jsp";
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Request added Successfully.");
            } else {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Request Not Updated.");
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewStockDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String Path = "";
        String menuPath = "GATE IN  >>  Insert ";

        String pageTitle = "GTN Details";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String custId = "";
        custId = request.getParameter("custId");
        System.out.println("custtt=========" + custId);
        WmsTO wmsTO = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "GTN Details";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            String param = request.getParameter("param");
            if ("search".equals(param)) {
                System.out.println("param==" + param);
                String itemId = request.getParameter("itemId");
                String userWhId = request.getParameter("userWhId");
                String itemid = request.getParameter("itemId");
                System.out.println("itemid=====" + itemid);
                request.setAttribute("item1", itemid);
                String Quantity = request.getParameter("Quantity");
                request.setAttribute("Quantity", Quantity);
                String TransferQty = request.getParameter("TransferQty");
                request.setAttribute("TransferQty", TransferQty);
                String item = itemid.split("-")[0];
                String userName = itemid.split("-")[1];
                String userName2 = itemid.split("-")[2];

                ArrayList getSerialNumber = new ArrayList();
                getSerialNumber = wmsBP.getSerialNumber(item, userWhId);
                request.setAttribute("getSerialNumber", getSerialNumber);
                System.out.println("getSerialNumberController====" + getSerialNumber.size());
            }
            Path = "content/wms/addStockDetails.jsp";
            ArrayList getItemList = new ArrayList();
            getItemList = wmsBP.getItemLists(userId);
            request.setAttribute("getItemList", getItemList);
            System.out.println("ControllerSizegetList=====" + getItemList.size());
            ArrayList vendorList = new ArrayList();
            vendorList = wmsBP.processVendorList();
            System.out.println("ControllerSize=====" + vendorList.size());
            request.setAttribute("vendorList", vendorList);
            String getUserWarehouse = wmsBP.getUserWarehouse(userId);
            String userWhId = getUserWarehouse.split("-")[0];
            String userWhName = getUserWarehouse.split("-")[1];
            request.setAttribute("userWhId", userWhId);
            request.setAttribute("userWhName", userWhName);
            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareToHouseList(userWhId);
            System.out.println("getWareHouseList " + getWareHouseList.size());
            request.setAttribute("getWareHouseList", getWareHouseList);
            ArrayList getItemDetails = new ArrayList();
            getItemDetails = wmsBP.processGetItemList(userWhId);
            request.setAttribute("getItemDetails", getItemDetails);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(Path);

    }

    public ModelAndView saveTransferDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();

        String menuPath = "Operation  >>  View TripSheet ";
        String path = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            WmsTO wmsTO = new WmsTO();
            ArrayList getItemList = new ArrayList();
            getItemList = wmsBP.getItemLists(userId);
            request.setAttribute("getItemList", getItemList);
            String transfer = request.getParameter("TransferQty");
            System.out.println("transferrr===" + transfer);
            String[] serialNos = request.getParameterValues("serNo");
            System.out.println("seialllno======" + serialNos);
            String[] selectedIndex = request.getParameterValues("selectedIndex");
            wmsTO.setSelectedIndex(selectedIndex);
            wmsTO.setSerialNos(serialNos);
            wmsTO.setUserId(userId + "");
            String warehouseId = request.getParameter("userWhId");

            String getUserWarehouse = wmsBP.getUserWarehouse(userId);
            String userWhId = getUserWarehouse.split("-")[0];
            String userWhName = getUserWarehouse.split("-")[1];
            request.setAttribute("userWhId", userWhId);
            request.setAttribute("userWhName", userWhName);
            String itemid = request.getParameter("itemId");
            String item = itemid.split("-")[0];
            System.out.println("itemId====" + item);
            System.out.println("itemId====" + itemid);
            String warehouseToId = request.getParameter("warehouseToId");

            wmsTO.setQuantity(transfer);
            wmsTO.setWarehouseToId(warehouseToId);
            wmsTO.setWarehouseId(warehouseId);
            wmsTO.setItemId(item);

            int status = wmsBP.insertTransferQty(wmsTO);
            System.out.println("status :" + status);
            path = "content/wms/addStockDetails.jsp";
            if (status > 0) {
                if (status == 404) {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "SKU Id Not updated for the selected To warehouse.");
                } else {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Request added Successfully.");
                }
            } else {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Request Not Updated.");
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewFindSerialNumber(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String Path = "";
        String menuPath = "GATE IN  >>  Insert ";

        String pageTitle = "GTN Details";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String custId = "";
        custId = request.getParameter("custId");
        System.out.println("custtt=========" + custId);
        WmsTO wmsTO = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "GTN Details";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            String param = request.getParameter("param");
            if ("search".equals(param)) {

                String Serial = request.getParameter("serial");
                request.setAttribute("serialNumber", Serial);

                wmsTO.setSerialNo(Serial);
                System.out.println("Serial=====" + Serial);
                ArrayList serialNumberDetails = new ArrayList();
                serialNumberDetails = wmsBP.serialNumberDetails(wmsTO);
                request.setAttribute("serialNumberDetails", serialNumberDetails);

                Path = "content/wms/findSerialNumber.jsp";
            } else {
                Path = "content/wms/findSerialNumber.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(Path);

    }

    public ModelAndView runsheetUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "Runsheet Upload";
        menuPath = "wms >>  Runsheet Upload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        wmsTO.setUserId(userId + "");
        try {
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                int saveRunsheetUpload = wmsBP.saveRunsheetUpload(wmsTO, userId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Runsheet Uploaded Successfully");
                String runsheetId = request.getParameter("runsheetId");
                wmsTO.setRunsheetId(runsheetId);
                path = "content/wms/fkRunsheet.jsp";
                ArrayList getFkRunsheetList = new ArrayList();
                getFkRunsheetList = wmsBP.getFkRunsheetList(wmsTO);
                System.out.println("here in controller");
                request.setAttribute("fkRunsheetList", getFkRunsheetList);
                int TripSheetSize = getFkRunsheetList.size();
                request.setAttribute("runsheetSize", TripSheetSize);
            } else if ("add".equals(param)) {
                String[] shipmentId = request.getParameterValues("shipmentId");
                String[] name = request.getParameterValues("name");
                String[] address = request.getParameterValues("address");
                String[] codAmount = request.getParameterValues("codAmount");
                String[] status = request.getParameterValues("status");
                String[] date = request.getParameterValues("date");
                String[] itemName = request.getParameterValues("itemName");
                int addRunsheet = wmsBP.addFkRvpRunsheet(shipmentId, name, address, codAmount, status, date, itemName, userId);
                if (addRunsheet > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Runsheet Added Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Failed To Add Runsheet");
                }
                path = "content/wms/fkRunsheet.jsp";
                ArrayList getFkRunsheetList = new ArrayList();
                getFkRunsheetList = wmsBP.getFkRunsheetList(wmsTO);
                System.out.println("here in controller");
                request.setAttribute("fkRunsheetList", getFkRunsheetList);
                int TripSheetSize = getFkRunsheetList.size();
                request.setAttribute("runsheetSize", TripSheetSize);
            } else {
                int status = wmsBP.deleteRunsheetTemp(userId);
                path = "content/wms/runsheetUpload.jsp";
            }
//            System.out.println("status11111111111111111111111111===="+shipmentUpload);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView saveRunsheetUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
//        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = 0;
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList getContractUpload = new ArrayList();
        String runSheetId = "";
        String trackingId = "";
        String mobileNo = "";
        String name = "";
        String address = "";
        String shipmentType = "";
        String codAmount = "";
        String assignedUser = "";
        String status = "";
        String date = "";
        String amountPaid = "";
        String receiverName = "";
        String relation = "";
        String receiver = "";
        String mobile = "";
        String signature = "";
        String vertical = "";
        String orderNo = "";
        String productDetails = "";

//        int status = 0;
        int status1 = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            String custId = request.getParameter("custId");
            String contractId = request.getParameter("contractId");
            System.out.println("custId----" + custId);
            System.out.println("contractId----" + contractId);
            request.setAttribute("custId", custId);
            request.setAttribute("contractId", contractId);
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

//            int deleteUploadTemp = wBP.deleteUploadTemp();
//            System.out.println("deleteUploadTemp----"+deleteUploadTemp);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        int len = splitFileNames.length;
                        fileFormat = splitFileNames[len - 1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = uploadedFileName;
                                System.out.println("splitFileName[0]=" + splitFileName[0]);
                                System.out.println("splitFileName[0]=" + splitFileName[1]);

                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                System.out.println("tempPath..." + tempFilePath);
                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath));
                                System.out.println("tempPath = " + tempFilePath);
//                                System.out.println("actPath = " + actualFilePath);
//                                String parts = actualFilePath.substring(actualFilePath.lastIndexOf("//"));
//                                String part1 = parts.replace("//", "");
                                if ("xlsx".equals(fileFormat)) {
                                    FileInputStream fis = new FileInputStream(tempFilePath);
                                    XSSFWorkbook wb = new XSSFWorkbook(fis);
                                    XSSFSheet sheet = wb.getSheetAt(0);
                                    int value = sheet.getLastRowNum();
                                    System.out.println("value  " + value);

                                    int sno = 0;
                                    Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                    while (itr.hasNext()) {
                                        Row row = itr.next();
                                        if (sno > 0) {
                                            WmsTO wmsTo = new WmsTO();

                                            wmsTo.setUserId(userId + "");

                                            if (row.getCell(1) != null && !"".equals(row.getCell(1))) {
                                                row.getCell(1).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                runSheetId = row.getCell(1).getStringCellValue();
                                                System.out.println("99999 " + runSheetId);
                                                wmsTo.setRunSheetId(runSheetId);
                                            }

                                            if (row.getCell(2) != null && !"".equals(row.getCell(2))) {
                                                row.getCell(2).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                trackingId = row.getCell(2).getStringCellValue();
                                                wmsTo.setTrackingId(trackingId);
                                            }

                                            if (row.getCell(3) != null && !"".equals(row.getCell(3))) {
                                                row.getCell(3).setCellType(row.getCell(3).CELL_TYPE_STRING);
                                                mobileNo = row.getCell(3).getStringCellValue();
                                                wmsTo.setMobileNo(mobileNo);
                                            }

                                            if (row.getCell(4) != null && !"".equals(row.getCell(4))) {
                                                row.getCell(4).setCellType(row.getCell(4).CELL_TYPE_STRING);
                                                name = row.getCell(4).getStringCellValue();
                                                wmsTo.setName(name);
                                            }

                                            if (row.getCell(5) != null && !"".equals(row.getCell(5))) {
                                                row.getCell(5).setCellType(row.getCell(5).CELL_TYPE_STRING);
                                                address = row.getCell(5).getStringCellValue();
                                                wmsTo.setAddress(address);
                                            }

                                            if (row.getCell(6) != null && !"".equals(row.getCell(6))) {
                                                row.getCell(6).setCellType(row.getCell(6).CELL_TYPE_STRING);
                                                shipmentType = row.getCell(6).getStringCellValue();
                                                wmsTo.setShipmentType(shipmentType);
                                            }

                                            if (row.getCell(7) != null && !"".equals(row.getCell(7))) {
                                                row.getCell(7).setCellType(row.getCell(7).CELL_TYPE_STRING);
                                                codAmount = row.getCell(7).getStringCellValue();
                                                wmsTo.setCodAmount(codAmount);
                                            }

                                            if (row.getCell(8) != null && !"".equals(row.getCell(8))) {
                                                row.getCell(8).setCellType(row.getCell(8).CELL_TYPE_STRING);
                                                assignedUser = row.getCell(8).getStringCellValue();
                                                wmsTo.setAssignedUser(assignedUser);
                                            }

                                            if (row.getCell(9) != null && !"".equals(row.getCell(9))) {
                                                row.getCell(9).setCellType(row.getCell(9).CELL_TYPE_STRING);
                                                status = row.getCell(9).getStringCellValue();
                                                wmsTo.setStatus(status);
                                            }

                                            if (row.getCell(10) != null && !"".equals(row.getCell(10))) {
                                                row.getCell(10).setCellType(row.getCell(10).CELL_TYPE_STRING);
                                                date = row.getCell(10).getStringCellValue();
                                                System.out.println(date + " ref date");
                                                wmsTo.setDate(date);
                                            }

                                            if (row.getCell(11) != null && !"".equals(row.getCell(11))) {
                                                row.getCell(11).setCellType(row.getCell(11).CELL_TYPE_STRING);
                                                amountPaid = row.getCell(11).getStringCellValue();
                                                wmsTo.setAmountPaid(amountPaid);
                                            }

                                            if (row.getCell(12) != null && !"".equals(row.getCell(12))) {
                                                row.getCell(12).setCellType(row.getCell(12).CELL_TYPE_STRING);
                                                receiverName = row.getCell(12).getStringCellValue();
                                                wmsTo.setReceiverName(receiverName);
                                            }

                                            if (row.getCell(13) != null && !"".equals(row.getCell(13))) {
                                                row.getCell(13).setCellType(row.getCell(13).CELL_TYPE_STRING);
                                                relation = row.getCell(13).getStringCellValue();;
                                                wmsTo.setRelation(relation);
                                                System.out.println("55555555" + relation);
                                            }

                                            if (row.getCell(14) != null && !"".equals(row.getCell(14))) {
                                                row.getCell(14).setCellType(row.getCell(14).CELL_TYPE_STRING);
                                                receiver = row.getCell(14).getStringCellValue();
                                                wmsTo.setReceiver(receiver);
                                                System.out.println("66666666" + receiver);
                                            }

                                            if (row.getCell(15) != null && !"".equals(row.getCell(15))) {
                                                row.getCell(15).setCellType(row.getCell(15).CELL_TYPE_STRING);
                                                mobile = row.getCell(15).getStringCellValue();
                                                wmsTo.setMobile(mobile);
                                            }

                                            if (row.getCell(16) != null && !"".equals(row.getCell(16))) {
                                                row.getCell(16).setCellType(row.getCell(16).CELL_TYPE_STRING);
                                                signature = row.getCell(16).getStringCellValue();
                                                wmsTo.setSignature(signature);
                                            }

                                            if (row.getCell(17) != null && !"".equals(row.getCell(17))) {
                                                row.getCell(17).setCellType(row.getCell(17).CELL_TYPE_STRING);
                                                vertical = row.getCell(17).getStringCellValue();
                                                wmsTo.setVertical(vertical);
                                            }

                                            if (row.getCell(18) != null && !"".equals(row.getCell(18))) {
                                                row.getCell(18).setCellType(row.getCell(18).CELL_TYPE_STRING);
                                                orderNo = row.getCell(18).getStringCellValue();
                                                wmsTo.setOrderNo(orderNo);
                                            }

                                            if (row.getCell(19) != null && !"".equals(row.getCell(19))) {
                                                row.getCell(19).setCellType(row.getCell(19).CELL_TYPE_STRING);
                                                productDetails = row.getCell(19).getStringCellValue();
                                                wmsTo.setProductDetails(productDetails);
                                            }

                                            status1 = wmsBP.uploadRunsheet(wmsTo);
                                            System.out.println("status====" + status);
                                        }
                                        sno++;

                                    }
                                } else if ("xls".equals(fileFormat)) {

                                    int count = 0;
                                    WorkbookSettings ws = new WorkbookSettings();
                                    ws.setLocale(new Locale("en", "EN"));
                                    Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                    Sheet s = workbook.getSheet(0);
                                    System.out.println("rows" + userId + s.getRows());

                                    for (int i = 1; i < s.getRows(); i++) {
                                        System.out.println("88888888");
                                        WmsTO wmsTO = new WmsTO();
                                        wmsTO.setUserId(userId + "");
                                        count++;

                                        runSheetId = s.getCell(1, i).getContents();
                                        System.out.println("99999 " + runSheetId);
                                        wmsTO.setRunSheetId(runSheetId);

                                        trackingId = s.getCell(2, i).getContents();
                                        wmsTO.setTrackingId(trackingId);

                                        mobileNo = s.getCell(3, i).getContents();
                                        wmsTO.setMobileNo(mobileNo);

                                        name = s.getCell(4, i).getContents();
                                        wmsTO.setName(name);

                                        address = s.getCell(5, i).getContents();
                                        wmsTO.setAddress(address);

                                        shipmentType = s.getCell(6, i).getContents();
                                        wmsTO.setShipmentType(shipmentType);

                                        codAmount = s.getCell(7, i).getContents();
                                        wmsTO.setCodAmount(codAmount);

                                        assignedUser = s.getCell(8, i).getContents();
                                        wmsTO.setAssignedUser(assignedUser);

                                        status = s.getCell(9, i).getContents();
                                        wmsTO.setStatus(status);

                                        date = s.getCell(10, i).getContents();
                                        wmsTO.setDate(date);

                                        amountPaid = s.getCell(11, i).getContents();
                                        wmsTO.setAmountPaid(amountPaid);

                                        receiverName = s.getCell(12, i).getContents();
                                        wmsTO.setReceiverName(receiverName);

                                        relation = s.getCell(13, i).getContents();
                                        wmsTO.setRelation(relation);
                                        System.out.println("55555555" + relation);

                                        receiver = s.getCell(14, i).getContents();
                                        wmsTO.setReceiver(receiver);
                                        System.out.println("66666666" + receiver);

                                        mobile = s.getCell(15, i).getContents().toString();
                                        wmsTO.setMobile(mobile);

                                        signature = s.getCell(16, i).getContents().toString();
                                        wmsTO.setSignature(signature);

                                        vertical = s.getCell(17, i).getContents().toString();
                                        wmsTO.setVertical(vertical);

                                        orderNo = s.getCell(18, i).getContents().toString();
                                        wmsTO.setOrderNo(orderNo);

                                        productDetails = s.getCell(19, i).getContents().toString();
                                        wmsTO.setProductDetails(productDetails);

                                        status1 = wmsBP.uploadRunsheet(wmsTO);
                                        System.out.println("status====" + status);

                                    }

                                }
                            }
                        } else {
                            request.setAttribute("errorMessage", "Contract Updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            wms.setUserId(userId + "");
            ArrayList getRunsheetUpload = new ArrayList();
            getRunsheetUpload = wmsBP.getRunsheetUpload(wms);
            request.setAttribute("getContractList", getRunsheetUpload);
            path = "content/wms/runsheetUpload.jsp";

            if (getRunsheetUpload.size() > 0) {
//                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Contract Updated Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView ifbOrderUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "ifbOrderUpload";
        menuPath = "wms >>  ifbOrderUpload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        try {
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                wmsTO.setWhId(whId);
                System.out.println("whId    " + whId);
                int ifbOrderUpload = wmsBP.ifbOrderUpload(wmsTO, userId, whId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "LR Updated Successfully");
                int clearData = wmsBP.deleteIfbUploadTemp(userId);
                path = "content/wms/ifbOrderUpload.jsp";
            } else {
                int clearData = wmsBP.deleteIfbUploadTemp(userId);
                path = "content/wms/ifbOrderUpload.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView ifbReturnOrderUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "ifbOrderUpload";
        menuPath = "wms >>  ifbOrderUpload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        try {
            String param = request.getParameter("param");
            ArrayList dealerMaster = wmsBP.getDealerMaster(wmsTO);
            request.setAttribute("dealerMaster", dealerMaster);
            if ("save".equals(param)) {
                wmsTO.setWhId(whId);
                System.out.println("whId    " + whId);
                String invoiceNo = request.getParameter("invoiceNo");
                String invoiceDate = request.getParameter("invoiceDate");
                String itemDesc = request.getParameter("itemDescription");
                String serialNo = request.getParameter("serialNo");
                String matlGroup = request.getParameter("matlGroup");
                String purchaseOrderNumber = request.getParameter("purchaseNo");
                String customerName = request.getParameter("dealerName");
                String qty = request.getParameter("qty");
                String pincode = request.getParameter("pincode");
                String city = request.getParameter("city");
                String grossValue = request.getParameter("grossValue");
                String gst = request.getParameter("gst");
                String dealerCode = request.getParameter("dealerCode");
                wmsTO.setInvoiceNo(invoiceNo);
                wmsTO.setInvoiceDate(invoiceDate);
                wmsTO.setMaterialDescription(itemDesc);
                wmsTO.setSerialNo(serialNo);
                wmsTO.setMatlGroup(matlGroup);
                wmsTO.setPurchaseOrderNumber(purchaseOrderNumber);
                wmsTO.setCustomerName(customerName);
                wmsTO.setPincode(pincode);
                wmsTO.setCity(city);
                wmsTO.setQty(qty);
                wmsTO.setGrossValue(grossValue);
                wmsTO.setGstinNumber(gst);
                wmsTO.setDealerCode(dealerCode);
                int ifbOrderUpdate = wmsBP.ifbReturnOrderUpload(wmsTO, userId, whId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Order Updated Successfully");
                path = "content/wms/ifbReturnOrderUpdate.jsp";
            } else if ("update".equals(param)) {
                String[] orderId = request.getParameterValues("orderId");
                String[] index = request.getParameterValues("index");
                int saveReturnIfbOrder = wmsBP.saveReturnIfbOrder(orderId, index);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Order Updated Successfully");
            } else if ("search".equals(param)) {
                String invoiceNo = request.getParameter("invoiceNo1");
                wmsTO.setInvoiceNo(invoiceNo);
                ArrayList getIfbReturnOrders = wmsBP.getIfbReturnOrders(wmsTO, userId);
                request.setAttribute("ifbReturnOrders", getIfbReturnOrders);
            }
            path = "content/wms/ifbReturnOrderUpdate.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView ifbOrderRelease(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "ifbOrderRelease";
        menuPath = "wms >>  ifbOrderRelease";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int roleId = (Integer) session.getAttribute("RoleId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        try {
            String param = request.getParameter("param");
            wmsTO.setRoleId(roleId + "");
            ArrayList rejectionMaster = wmsBP.getRejectionMaster(wmsTO);
            request.setAttribute("rejectionMaster", rejectionMaster);
            String rejectionId = request.getParameter("rejectionId");
            request.setAttribute("rejectionId", rejectionId);
            String orderId = "";
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);
            wmsTO.setRejectionId(rejectionId);
            if ("update".equals(param)) {
                orderId = request.getParameter("orderId");
                wmsTO.setOrderId(orderId);
                int ifbOrderUpdate = wmsBP.updateReleaseIfbOrder(wmsTO, userId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Order Released Successfully");
                path = "content/wms/ifbReleaseOrder.jsp";
            } else if ("excel".equals(param)) {
                path = "content/wms/ifbReleaseOrderExcel.jsp";
            } else {
                path = "content/wms/ifbReleaseOrder.jsp";
            }
            ArrayList getIfbOrderRelease = wmsBP.getIfbOrderRelease(userId, wmsTO);
            request.setAttribute("ifbOrderRelease", getIfbOrderRelease);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView uploadIfbOrder(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
//        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = 0;
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        String billDoc = "";
        String billDate = "";
        String purchaseOrderNumber = "";
        String matlGroup = "";
        String material = "";
        String materialDescription = "";
        String billQty = "";
        String city = "";
        String shipTo = "";
        String partyName = "";
        String gstinNumber = "";
        String grossValue = "";
        String pincode = "";
        String exchange = "";
        int status1 = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            String hubId = (String) session.getAttribute("hubId");
            userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

//            int deleteUploadTemp = wBP.deleteUploadTemp();
//            System.out.println("deleteUploadTemp----"+deleteUploadTemp);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadedxls/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        int len = splitFileNames.length;
                        fileFormat = splitFileNames[len - 1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = uploadedFileName;
                                System.out.println("splitFileName[0]=" + splitFileName[0]);
                                System.out.println("splitFileName[0]=" + splitFileName[1]);

                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                System.out.println("tempPath..." + tempFilePath);
                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath));
                                System.out.println("tempPath = " + tempFilePath);
//                                System.out.println("actPath = " + actualFilePath);
//                                String parts = actualFilePath.substring(actualFilePath.lastIndexOf("//"));
//                                String part1 = parts.replace("//", "");
                                System.out.println("11111111111111111111111111");
                                if ("xlsx".equals(fileFormat)) {
                                    FileInputStream fis = new FileInputStream(tempFilePath);
                                    XSSFWorkbook wb = new XSSFWorkbook(fis);
                                    XSSFSheet sheet = wb.getSheetAt(0);
                                    int value = sheet.getLastRowNum();
                                    System.out.println("value  " + value);

                                    int sno = 0;
                                    Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                    while (itr.hasNext()) {
                                        Row row = itr.next();
                                        if (sno > 0) {
                                            System.out.println("cellyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy " + row.getCell(1));
                                            WmsTO wmsTO = new WmsTO();

                                            wmsTO.setUserId(userId + "");

                                            if (row.getCell(0) != null && !"".equals(row.getCell(0))) {
                                                row.getCell(0).setCellType(row.getCell(0).CELL_TYPE_STRING);
                                                billDoc = row.getCell(0).getStringCellValue();
                                                System.out.println("AAAAAAAAAAAAAAAA " + billDoc);
                                                wmsTO.setBillDoc(billDoc);
                                            }

                                            if (row.getCell(1) != null && !"".equals(row.getCell(1))) {
                                                row.getCell(1).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                billDate = row.getCell(1).getStringCellValue();
                                                wmsTO.setBillDate(billDate);
                                            }

                                            if (row.getCell(2) != null && !"".equals(row.getCell(2))) {
                                                row.getCell(2).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                purchaseOrderNumber = row.getCell(2).getStringCellValue();
                                                wmsTO.setPurchaseOrderNumber(purchaseOrderNumber);
                                            }

                                            if (row.getCell(3) != null && !"".equals(row.getCell(3))) {
                                                row.getCell(3).setCellType(row.getCell(3).CELL_TYPE_STRING);
                                                matlGroup = row.getCell(3).getStringCellValue();
                                                wmsTO.setMatlGroup(matlGroup);
                                            }

                                            if (row.getCell(4) != null && !"".equals(row.getCell(4))) {
                                                row.getCell(4).setCellType(row.getCell(4).CELL_TYPE_STRING);
                                                material = row.getCell(4).getStringCellValue();
                                                wmsTO.setMaterial(material);
                                            }

                                            if (row.getCell(5) != null && !"".equals(row.getCell(5))) {
                                                row.getCell(5).setCellType(row.getCell(5).CELL_TYPE_STRING);
                                                materialDescription = row.getCell(5).getStringCellValue();
                                                wmsTO.setMaterialDescription(materialDescription);
                                            }

                                            if (row.getCell(6) != null && !"".equals(row.getCell(6))) {
                                                row.getCell(6).setCellType(row.getCell(6).CELL_TYPE_STRING);
                                                billQty = row.getCell(6).getStringCellValue();
                                                wmsTO.setBillQty(billQty);
                                            }

                                            if (row.getCell(7) != null && !"".equals(row.getCell(7))) {
                                                row.getCell(7).setCellType(row.getCell(7).CELL_TYPE_STRING);
                                                city = row.getCell(7).getStringCellValue();
                                                wmsTO.setCity(city);
                                            }

                                            if (row.getCell(8) != null && !"".equals(row.getCell(8))) {
                                                row.getCell(8).setCellType(row.getCell(8).CELL_TYPE_STRING);
                                                shipTo = row.getCell(8).getStringCellValue();
                                                wmsTO.setShipTo(shipTo);
                                            }

                                            if (row.getCell(9) != null && !"".equals(row.getCell(9))) {
                                                row.getCell(9).setCellType(row.getCell(9).CELL_TYPE_STRING);
                                                partyName = row.getCell(9).getStringCellValue();
                                                wmsTO.setPartyName(partyName);
                                            }

                                            if (row.getCell(10) != null && !"".equals(row.getCell(10))) {
                                                row.getCell(10).setCellType(row.getCell(10).CELL_TYPE_STRING);
                                                gstinNumber = row.getCell(10).getStringCellValue();
                                                wmsTO.setGstinNumber(gstinNumber);
                                            }

                                            if (row.getCell(11) != null && !"".equals(row.getCell(11))) {
                                                row.getCell(11).setCellType(row.getCell(11).CELL_TYPE_STRING);
                                                grossValue = row.getCell(11).getStringCellValue();
                                                wmsTO.setGrossValue(grossValue.replace(",", ""));
                                            }

                                            if (row.getCell(12) != null && !"".equals(row.getCell(12))) {
                                                row.getCell(12).setCellType(row.getCell(12).CELL_TYPE_STRING);
                                                pincode = row.getCell(12).getStringCellValue();
                                                wmsTO.setPincode(pincode);
                                            }

                                            if (row.getCell(13) != null && !"".equals(row.getCell(13))) {
                                                row.getCell(13).setCellType(row.getCell(13).CELL_TYPE_STRING);
                                                exchange = row.getCell(13).getStringCellValue();
                                                wmsTO.setExchange(exchange);
                                            }

                                            wmsTO.setWhId(hubId);
                                            status1 = wmsBP.uploadIfbOrder(wmsTO);
                                            System.out.println("status====" + status1);

                                        }
                                        sno++;

                                    }
                                } else if ("xls".equals(fileFormat)) {

                                    int count = 0;
                                    WorkbookSettings ws = new WorkbookSettings();
                                    ws.setLocale(new Locale("en", "EN"));
                                    Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                    Sheet s = workbook.getSheet(0);
                                    System.out.println("rows" + userId + s.getRows());

                                    for (int i = 1; i < s.getRows(); i++) {
                                        System.out.println("11111111111111111111");
                                        WmsTO wmsTO = new WmsTO();
                                        count++;

                                        wmsTO.setUserId(userId + "");
                                        billDoc = s.getCell(0, i).getContents();
                                        System.out.println("AAAAAAAAAAAAAAAA " + billDoc);
                                        wmsTO.setBillDoc(billDoc);

                                        billDate = s.getCell(1, i).getContents();
                                        wmsTO.setBillDate(billDate);

                                        purchaseOrderNumber = s.getCell(2, i).getContents();
                                        wmsTO.setPurchaseOrderNumber(purchaseOrderNumber);

                                        matlGroup = s.getCell(3, i).getContents();
                                        wmsTO.setMatlGroup(matlGroup);

                                        material = s.getCell(4, i).getContents();
                                        wmsTO.setMaterial(material);

                                        materialDescription = s.getCell(5, i).getContents();
                                        wmsTO.setMaterialDescription(materialDescription);

                                        billQty = s.getCell(6, i).getContents();
                                        wmsTO.setBillQty(billQty);

                                        city = s.getCell(7, i).getContents();
                                        wmsTO.setCity(city);

                                        shipTo = s.getCell(8, i).getContents();
                                        wmsTO.setShipTo(shipTo);

                                        partyName = s.getCell(9, i).getContents();
                                        wmsTO.setPartyName(partyName);

                                        gstinNumber = s.getCell(10, i).getContents();
                                        wmsTO.setGstinNumber(gstinNumber);

                                        grossValue = s.getCell(11, i).getContents();
                                        wmsTO.setGrossValue(grossValue.replace(",", ""));

                                        pincode = s.getCell(12, i).getContents();
                                        wmsTO.setPincode(pincode);

                                        exchange = s.getCell(13, i).getContents();
                                        wmsTO.setExchange(exchange);

                                        wmsTO.setWhId(hubId);
                                        status1 = wmsBP.uploadIfbOrder(wmsTO);
                                        System.out.println("status====" + status1);
//                                    getContractUpload = wmsBP.getContractUpload(wmsTO);

                                    }

                                }
                            }
                        } else {
                            request.setAttribute("errorMessage", "IFB order updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            wms.setUserId(userId + "");
            ArrayList getIFBList = new ArrayList();
            getIFBList = wmsBP.getIFBList(wms);
            request.setAttribute("getContractList", getIFBList);
            path = "content/wms/ifbOrderUpload.jsp";

            if (getIFBList.size() > 0) {
                System.out.println("getContractList====check================status" + getIFBList);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "IFB Updated Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView connectSrnUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "connectSrnUpload";
        menuPath = "wms >>  connectSrnUpload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        try {
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                wmsTO.setWhId(whId);
                System.out.println("whId    " + whId);
                int saveConnectSrn = wmsBP.saveConnectSrn(wmsTO, userId, whId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "SRN Saved Successfully");
                int clearData = wmsBP.deleteConnectSrnTemp(userId);
                path = "content/wms/connectSrnUpload.jsp";
            } else {
                int clearData = wmsBP.deleteConnectSrnTemp(userId);
                path = "content/wms/connectSrnUpload.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView connectSrnView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "connect SRN View";
        menuPath = "wms >>  connect SRN View";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        try {
            wmsTO.setUserId(userId + "");
            String param = request.getParameter("param");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            ArrayList getConnectSrn = new ArrayList();
            getConnectSrn = wmsBP.getConnectSrn(wmsTO);
            request.setAttribute("connectSrn", getConnectSrn);
            path = "content/wms/connectSrnView.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView connectSaveSrnUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
//        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = 0;
        wmsCommand = command;
        String pageTitle = "connect Srn Upload";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        String partyName = "";
        String soldTo = "";
        String shipTo = "";
        String partyDCNos = "";
        String dcDate = "";
        String materialCode = "";
        String materialDescription = "";
        String qty = "";
        String receivedOn = "";
        String transporterName = "";
        String internalOrderNo = "";
        String internalOrderDate = "";
        String remarks = "";
//        int status = 0;
        int status1 = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            String hubId = (String) session.getAttribute("hubId");
            userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

//            int deleteUploadTemp = wBP.deleteUploadTemp();
//            System.out.println("deleteUploadTemp----"+deleteUploadTemp);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadedxls/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        int len = splitFileNames.length;
                        fileFormat = splitFileNames[len - 1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = uploadedFileName;
                                System.out.println("splitFileName[0]=" + splitFileName[0]);
                                System.out.println("splitFileName[0]=" + splitFileName[1]);

                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                System.out.println("tempPath..." + tempFilePath);
                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath));
                                System.out.println("tempPath = " + tempFilePath);
//                                System.out.println("actPath = " + actualFilePath);
//                                String parts = actualFilePath.substring(actualFilePath.lastIndexOf("//"));
//                                String part1 = parts.replace("//", "");
                                System.out.println("11111111111111111111111111");
                                if ("xlsx".equals(fileFormat)) {
                                    FileInputStream fis = new FileInputStream(tempFilePath);
                                    XSSFWorkbook wb = new XSSFWorkbook(fis);
                                    XSSFSheet sheet = wb.getSheetAt(0);
                                    int value = sheet.getLastRowNum();
                                    System.out.println("value  " + value);

                                    int sno = 0;
                                    Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                    while (itr.hasNext()) {
                                        Row row = itr.next();
                                        if (sno > 0) {
                                            System.out.println("cellyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy " + row.getCell(1));
                                            WmsTO wmsTo = new WmsTO();

                                            wmsTo.setUserId(userId + "");

                                            if (row.getCell(0) != null && !"".equals(row.getCell(0))) {
                                                row.getCell(0).setCellType(row.getCell(0).CELL_TYPE_STRING);
                                                partyName = row.getCell(0).getStringCellValue();
                                                System.out.println("srn  " + partyName);
                                                wmsTo.setPartyName(partyName);
                                            }

                                            if (row.getCell(1) != null && !"".equals(row.getCell(1))) {
                                                row.getCell(1).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                soldTo = row.getCell(1).getStringCellValue();
                                                wmsTo.setSoldTo(soldTo);
                                            }

                                            if (row.getCell(2) != null && !"".equals(row.getCell(2))) {
                                                row.getCell(2).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                shipTo = row.getCell(2).getStringCellValue();
                                                wmsTo.setShipTo(shipTo);
                                            }

                                            if (row.getCell(3) != null && !"".equals(row.getCell(3))) {
                                                row.getCell(3).setCellType(row.getCell(3).CELL_TYPE_STRING);
                                                partyDCNos = row.getCell(3).getStringCellValue();
                                                wmsTo.setPartyDcNos(partyDCNos);
                                            }

                                            if (row.getCell(4) != null && !"".equals(row.getCell(4))) {
                                                row.getCell(4).setCellType(row.getCell(4).CELL_TYPE_STRING);
                                                dcDate = row.getCell(4).getStringCellValue();
                                                wmsTo.setDcDate(dcDate);
                                            }

                                            if (row.getCell(5) != null && !"".equals(row.getCell(5))) {
                                                row.getCell(5).setCellType(row.getCell(5).CELL_TYPE_STRING);
                                                materialCode = row.getCell(5).getStringCellValue();
                                                wmsTo.setMaterialCode(materialCode);
                                            }

                                            if (row.getCell(6) != null && !"".equals(row.getCell(6))) {
                                                row.getCell(6).setCellType(row.getCell(6).CELL_TYPE_STRING);
                                                materialDescription = row.getCell(6).getStringCellValue();
                                                wmsTo.setMaterialDescription(materialDescription);
                                            }

                                            if (row.getCell(7) != null && !"".equals(row.getCell(7))) {
                                                row.getCell(7).setCellType(row.getCell(7).CELL_TYPE_STRING);
                                                qty = row.getCell(7).getStringCellValue();
                                                wmsTo.setQty(qty);
                                            }

                                            if (row.getCell(8) != null && !"".equals(row.getCell(8))) {
                                                row.getCell(8).setCellType(row.getCell(8).CELL_TYPE_STRING);
                                                receivedOn = row.getCell(8).getStringCellValue();
                                                wmsTo.setDate(receivedOn);
                                            }

                                            if (row.getCell(9) != null && !"".equals(row.getCell(9))) {
                                                row.getCell(9).setCellType(row.getCell(9).CELL_TYPE_STRING);
                                                transporterName = row.getCell(9).getStringCellValue();
                                                wmsTo.setVendorName(transporterName);
                                            }

                                            if (row.getCell(10) != null && !"".equals(row.getCell(10))) {
                                                row.getCell(10).setCellType(row.getCell(10).CELL_TYPE_STRING);
                                                internalOrderNo = row.getCell(10).getStringCellValue();
                                                wmsTo.setOrderNo(internalOrderNo);
                                            }

                                            if (row.getCell(11) != null && !"".equals(row.getCell(11))) {
                                                row.getCell(11).setCellType(row.getCell(11).CELL_TYPE_STRING);
                                                internalOrderDate = row.getCell(11).getStringCellValue();
                                                wmsTo.setInvoiceDate(internalOrderDate);
                                            }

                                            if (row.getCell(12) != null && !"".equals(row.getCell(12))) {
                                                row.getCell(12).setCellType(row.getCell(12).CELL_TYPE_STRING);
                                                remarks = row.getCell(12).getStringCellValue();
                                                wmsTo.setRemarks(remarks);
                                            }

                                            wmsTo.setWhId(hubId);
                                            status1 = wmsBP.uploadConnectSrn(wmsTo);
                                            System.out.println("status====" + status1);
//                                    getContractUpload = wmsBP.getContractUpload(wmsTo);
                                        }
                                        sno++;

                                    }
                                } else if ("xls".equals(fileFormat)) {

                                    int count = 0;
                                    WorkbookSettings ws = new WorkbookSettings();
                                    ws.setLocale(new Locale("en", "EN"));
                                    Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                    Sheet s = workbook.getSheet(0);
                                    System.out.println("rows" + userId + s.getRows());

                                    for (int i = 1; i < s.getRows(); i++) {
                                        System.out.println("11111111111111111111");
                                        WmsTO wmsTO = new WmsTO();
                                        count++;

                                        wmsTO.setUserId(userId + "");
                                        partyName = s.getCell(0, i).getContents();
                                        System.out.println("srn  " + partyName);
                                        wmsTO.setPartyName(partyName);

                                        soldTo = s.getCell(1, i).getContents();
                                        wmsTO.setSoldTo(soldTo);

                                        shipTo = s.getCell(2, i).getContents();
                                        wmsTO.setShipTo(shipTo);

                                        partyDCNos = s.getCell(3, i).getContents();
                                        wmsTO.setPartyDcNos(partyDCNos);

                                        dcDate = s.getCell(4, i).getContents();
                                        wmsTO.setDcDate(dcDate);

                                        materialCode = s.getCell(5, i).getContents();
                                        wmsTO.setMaterialCode(materialCode);

                                        materialDescription = s.getCell(6, i).getContents();
                                        wmsTO.setMaterialDescription(materialDescription);

                                        qty = s.getCell(7, i).getContents();
                                        wmsTO.setQty(qty);

                                        receivedOn = s.getCell(8, i).getContents();
                                        wmsTO.setDate(receivedOn);

                                        transporterName = s.getCell(9, i).getContents();
                                        wmsTO.setVendorName(transporterName);

                                        internalOrderNo = s.getCell(10, i).getContents();
                                        wmsTO.setOrderNo(internalOrderNo);

                                        internalOrderDate = s.getCell(11, i).getContents();
                                        wmsTO.setInvoiceDate(internalOrderDate);

                                        remarks = s.getCell(12, i).getContents();
                                        wmsTO.setRemarks(remarks);

                                        wmsTO.setWhId(hubId);
                                        status1 = wmsBP.uploadConnectSrn(wmsTO);
                                        System.out.println("status====" + status1);
//                                    getContractUpload = wmsBP.getContractUpload(wmsTO);

                                    }

                                }
                            }
                        } else {
                            request.setAttribute("errorMessage", "SRN Upload Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            wms.setUserId(userId + "");
            ArrayList getConnectSrn = new ArrayList();
            getConnectSrn = wmsBP.getConnectSrnTemp(wms);
            request.setAttribute("getConnectSrn", getConnectSrn);
            path = "content/wms/connectSrnUpload.jsp";

            if (getConnectSrn.size() > 0) {
                System.out.println("getConnectSrn====check================status" + getConnectSrn);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Srn Updated Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView ifbLRUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "ifbLRUpdate";
        menuPath = "wms >>  ifbLRUpdate";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int saveIfbLr = 0;
        wmsTO.setUserId(userId + "");
        try {
            String invoiceNo = "";
            String[] lrId = request.getParameterValues("lrId");
            String[] ewayBillNo = request.getParameterValues("ewayBillNoTemp");
            String[] ewayExpiry = request.getParameterValues("ewayBillExpiryTemp");
            String[] selected = request.getParameterValues("selectedStatus");
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                saveIfbLr = wmsBP.saveIfbLr(lrId, ewayBillNo, ewayExpiry, selected);
            }
            ArrayList ifbLRList = wmsBP.ifbLRList(wmsTO);
            request.setAttribute("ifbLRList", ifbLRList);
            path = "content/wms/ifbLRUpdate.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView ifbRunsheet(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "Ifb Runsheet";
        menuPath = "wms >>  Ifb Runsheet";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int saveIfbLr = 0;
        wmsTO.setUserId(userId + "");
        try {

            String param = request.getParameter("param");
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));

            String fromDate = "";
            String toDate = "";

            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");

            if (fromDate != null) {
                request.setAttribute("fromDate", fromDate);
                wmsTO.setFromDate(fromDate);
            } else {
                fromDate = "";
            }

            if (toDate != null) {
                wmsTO.setToDate(toDate);
                request.setAttribute("toDate", toDate);
            } else {
                toDate = "";
            }
            wmsTO.setUserId(userId + "");

            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            if ("Excel".equals(param)) {
                System.out.println("param==" + param);
                path = "content/wms/ifbRunsheetExcel.jsp";
            } else {
                System.out.println("param==" + param);
                path = "content/wms/ifbRunsheet.jsp";
            }
            ArrayList getIfbRunsheetList = new ArrayList();
            getIfbRunsheetList = wmsBP.getIfbRunsheetList(wmsTO);
            System.out.println("here in controller");
            request.setAttribute("ifbRunsheetList", getIfbRunsheetList);
            int TripSheetSize = getIfbRunsheetList.size();
            request.setAttribute("runsheetSize", TripSheetSize);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView flipkartRunsheet(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "Flipkart Runsheet";
        menuPath = "wms >>  Flipkart Runsheet";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int saveIfbLr = 0;
        wmsTO.setUserId(userId + "");
        try {

            String param = request.getParameter("param");
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));

            String fromDate = "";
            String toDate = "";

            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");
            if (!"".equals(fromDate)) {
                wmsTO.setFromDate(fromDate);
            }
            if (!"".equals(toDate)) {
                wmsTO.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            if ("Excel".equals(param)) {
                System.out.println("param==" + param);
                path = "content/wms/fkRunsheetExcel.jsp";
            } else {
                System.out.println("param==" + param);
                path = "content/wms/fkRunsheet.jsp";
            }
            ArrayList getFkRunsheetList = new ArrayList();
            getFkRunsheetList = wmsBP.getFkRunsheetList(wmsTO);
            System.out.println("here in controller");
            request.setAttribute("fkRunsheetList", getFkRunsheetList);
            int TripSheetSize = getFkRunsheetList.size();
            request.setAttribute("runsheetSize", TripSheetSize);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView viewFinanceApproval(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "Flipkart Finance Approval";
        menuPath = "wms >>  Flipkart Finance Approval";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        wmsTO.setUserId(userId + "");
        try {
            String param = request.getParameter("param");
            String remark = request.getParameter("remarks");
            wmsTO.setRemarks(remark);
            System.out.println(remark);
            String depositId = "";
            if ("approve".equals(param)) {
                depositId = request.getParameter("depositId");
                wmsTO.setDepositId(depositId);
                int updateStatus = wmsBP.updateFinanceapproval(wmsTO, userId);
                System.out.println("updateStatus" + updateStatus);
                request.setAttribute("updateStatus", updateStatus);
                if (updateStatus > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Approved Successfully");
                }
            } else if ("reject".equals(param)) {
                depositId = request.getParameter("depositId");
                wmsTO.setDepositId(depositId);
                int updateStatus = wmsBP.rejectFinanceApproval(wmsTO, userId);
                System.out.println("updateStatus" + updateStatus);
                request.setAttribute("updateStatus", updateStatus);
                if (updateStatus > 0) {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Rejected Successfully");
                }
            }
            ArrayList fkFinanceApproval = new ArrayList();
            fkFinanceApproval = wmsBP.fkFinanceApproval(wmsTO);
            System.out.println("financeapproval" + fkFinanceApproval.size());
            request.setAttribute("fkFinanceApproval", fkFinanceApproval);

            path = "content/wms/viewfinanceApproval.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView viewManagerApproval(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "Flipkart Manager Approval";
        menuPath = "Flipkart >> Flipkart Manager Approval";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        wmsTO.setUserId(userId + "");
        try {
            String param = request.getParameter("param");
            String remark = request.getParameter("remarks");
            System.out.println("remark");
            wmsTO.setRemarks(remark);

            String depositId = "";
            if ("approve".equals(param)) {
                depositId = request.getParameter("depositId");
                wmsTO.setDepositId(depositId);
                wmsTO.setStatus("2");
                int updateStatus = wmsBP.updateManagerapproval(wmsTO, userId);
                System.out.println("updateStatus" + updateStatus);
                request.setAttribute("updateStatus", updateStatus);
                if (updateStatus > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Approved Successfully");
                }
            } else if ("reject".equals(param)) {
                depositId = request.getParameter("depositId");
                wmsTO.setDepositId(depositId);
                wmsTO.setPodStatus("1");
                wmsTO.setStatus("0");
                int updateStatus = wmsBP.updateManagerapproval(wmsTO, userId);
                System.out.println("updateStatus" + updateStatus);
                request.setAttribute("updateStatus", updateStatus);
                if (updateStatus > 0) {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Rejected Successfully");
                }
            }
            ArrayList fkManagerApproval = new ArrayList();
            fkManagerApproval = wmsBP.fkManagerApproval(wmsTO);
            System.out.println("fkManagerApproval" + fkManagerApproval.size());
            request.setAttribute("fkManagerApproval", fkManagerApproval);

            path = "content/wms/viewManagerApproval.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView skuUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        ArrayList getwmsItemReport = new ArrayList();
        ArrayList getPickDetails = new ArrayList();
        String param = "";
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;

        PrintWriter pw = response.getWriter();
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        wmsTO.setUserId(userId + "");
        String menuPath = "";
        String action = "";
        String pageTitle = "Picklist Master";
        menuPath = "Masters  >> View Picklist Master ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            getwmsItemReport = wmsBP.getwmsItemReport(wmsTO);
            request.setAttribute("picklistMaster", getwmsItemReport);
            param = request.getParameter("param");
            path = "content/wms/skuupdate.jsp";

            if (getwmsItemReport.size() == 0) {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "No records found");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewPincodeMaster --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView skuViewUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        ArrayList getwmsItemReport = new ArrayList();
        ArrayList getPickDetails = new ArrayList();
        String param = "";
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;

        PrintWriter pw = response.getWriter();
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        wmsTO.setUserId(userId + "");
        String menuPath = "";
        String pageTitle = "Picklist Master";
        menuPath = "Masters  >> View Picklist Master ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String itemId = request.getParameter("itemId");
            request.setAttribute("itemId", itemId);
            wmsTO.setItemId(itemId);
            ArrayList getItemList = new ArrayList();
            getItemList = wmsBP.getItemLists(userId);
            request.setAttribute("getItemList", getItemList);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            getwmsItemReport = wmsBP.getwmsItemReportView(wmsTO);
            request.setAttribute("picklistMaster", getwmsItemReport);
            param = request.getParameter("param");
            path = "content/wms/skuupdateview.jsp";

            if (getwmsItemReport.size() == 0) {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "No records found");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewPincodeMaster --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView handleUpdateSku(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();

        String menuPath = "Operation  >>  View TripSheet ";
        String path = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            WmsTO wmsTO = new WmsTO();

            String itemId = request.getParameter("itemId");
            wmsTO.setItemId(itemId);
            request.setAttribute("itemId", itemId);
            String[] serialNumber = request.getParameterValues("serialNumber");
            String[] selectedStatus = request.getParameterValues("selectedStatus");
            wmsTO.setSerialNos(serialNumber);
            wmsTO.setSelectedIndex(selectedStatus);
            String oldItemId = request.getParameter("oldItemId");
            wmsTO.setProductId(oldItemId);

            int status = wmsBP.UpdateSku(wmsTO);
            System.out.println("status :" + status);
            path = "content/wms/skuupdateview.jsp";
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Request added Successfully.");
            } else {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Request added Successfully.");
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView ifbHubOut(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        wmsTO.setUserId(userId + "");
        String menuPath = "";
        String param = "";
        String pageTitle = "ifbHubOut";
        menuPath = "Masters  >> ifbHubOut ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            param = request.getParameter("param");
            String[] orderIds = request.getParameterValues("orderId");
            String[] selectedStatus = request.getParameterValues("selectedStatus");
            String[] hubId = request.getParameterValues("hubId");
            String[] ewayBillNo = request.getParameterValues("ewayBillNoTemp");
            String[] ewayExpiry = request.getParameterValues("ewayBillExpiryTemp");
            String vehicleNo = request.getParameter("vehicleNo");
            String vendorId = request.getParameter("vendorId");
            String driverName = request.getParameter("driverName");
            String driverMobile = request.getParameter("driverMobile");
            ArrayList vendorList = wmsBP.getVendorList();
            request.setAttribute("vendorList", vendorList);
            int updateIfbHubout = 0;
            Date date = new Date();
            ArrayList whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);
            String str = new SimpleDateFormat("yyyy-MM-dd").format(date);
            String date1 = str.replace("-", "");
            if ("update".equals(param)) {
                System.out.println("reached hub out update   " + orderIds.length);
                updateIfbHubout = wmsBP.updateIfbHubout(ewayBillNo, ewayExpiry, orderIds, selectedStatus, hubId, vehicleNo, driverName, driverMobile, date1, vendorId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Hub out done Successfully.");
                path = "content/wms/ifbHubOut.jsp";
            } else if ("serial".equals(param)) {
                String qte = request.getParameter("qte");
                if ("no".equals(qte)) {
                    request.setAttribute("qte", "1");
                    System.out.println("qte 2 ===" + qte);
                } else {
                    request.setAttribute("qte", "0");
                    System.out.println("qte 1 ===" + qte);
                }
                String lrId = request.getParameter("lrId");
                request.setAttribute("lrId", lrId);
                String type = request.getParameter("type");
                if ("update".equals(type)) {
                    String[] orderId = request.getParameterValues("orderId");
                    String[] custName = request.getParameterValues("customerName");
                    String[] qty = request.getParameterValues("qty");
                    String[] index = request.getParameterValues("index");
                    int updateIfbQty = wmsBP.updateIfbQty(orderId, qty, index, custName);
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Quantity Updated Successfully.");
                }
                ArrayList ifbDetails = wmsBP.getIfbOrderDetails(lrId);
                request.setAttribute("ifbDetails", ifbDetails);
                path = "content/wms/ifbOrderDetails.jsp";
            } else {
                path = "content/wms/ifbHubOut.jsp";
            }
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            ArrayList hubOutList = wmsBP.hubOutList(wmsTO);
            request.setAttribute("hubOutList", hubOutList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to addPincodeMaster --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView ifbHubOutView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        wmsTO.setUserId(userId + "");
        String companyId = (String) session.getAttribute("companyId");
        String menuPath = "";
        String param = "";
        String pageTitle = "ifbHubOutView";
        menuPath = "Masters  >> ifbHubOutView ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            param = request.getParameter("param");
            if ("print".equals(param)) {
                String lrId = request.getParameter("lrId");
                request.setAttribute("lrId", lrId);
                wmsTO.setLrId(lrId);
                request.setAttribute("vehicleNo", request.getParameter("vehicleNo"));
                request.setAttribute("driverName", request.getParameter("driverName"));
                request.setAttribute("driverMobile", request.getParameter("driverMobile"));
                ArrayList hubOutPrintDetails = wmsBP.hubOutPrintDetails(lrId, companyId);
                WmsTO wmsTO1 = new WmsTO();
                Iterator itr = hubOutPrintDetails.iterator();
                while (itr.hasNext()) {
                    wmsTO1 = (WmsTO) itr.next();
                    request.setAttribute("fromWhName", (wmsTO1.getCurrentHub()).split("~")[0]);
                    request.setAttribute("fromWhAddress", (wmsTO1.getCurrentHub()).split("~")[1]);
                    request.setAttribute("fromWhPhone", (wmsTO1.getCurrentHub()).split("~")[2]);
                    request.setAttribute("fromWhGst", (wmsTO1.getCurrentHub()).split("~")[3]);
                    request.setAttribute("toWhName", (wmsTO1.getDeliveryHub()).split("~")[0]);
                    request.setAttribute("toWhAddress", (wmsTO1.getDeliveryHub()).split("~")[1]);
                    request.setAttribute("toWhPhone", (wmsTO1.getDeliveryHub()).split("~")[2]);
                    request.setAttribute("toWhGst", (wmsTO1.getDeliveryHub()).split("~")[3]);
                    request.setAttribute("date", wmsTO1.getCreatedDate());
                    request.setAttribute("dispatchId", wmsTO1.getDispatchId());
                }
                path = "content/wms/hubOutPrint.jsp";
            } else {
                path = "content/wms/ifbHubOutView.jsp";
            }
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            ArrayList hubOutList = wmsBP.hubOutListView(wmsTO);
            request.setAttribute("hubOutList", hubOutList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to addPincodeMaster --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView ifbHubIn(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "ifbLRUpdate";
        menuPath = "wms >>  ifbLRUpdate";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int updateHubIn = 0;
        wmsTO.setUserId(userId + "");
        try {
            String param = request.getParameter("param");
            String[] orderIds = request.getParameterValues("orderId");
            String[] selectedStatus = request.getParameterValues("selectedStatus");
            if ("update".equals(param)) {
                updateHubIn = wmsBP.updateHubIn(orderIds, selectedStatus);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Hub In done Successfully.");
            }

            ArrayList ifbHubinList = wmsBP.ifbHubinList(wmsTO);
            request.setAttribute("ifbHubinList", ifbHubinList);
            path = "content/wms/ifbHubIn.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView normalOrderUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "normalOrderApproval";
        menuPath = "wms >>  normalOrderApproval";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            String param = request.getParameter("param");
            int status = wmsBP.deleteNormalOrderTemp(userId);
            path = "content/wms/normalOrderApproval.jsp";
//            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "shipment Uploaded Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView saveNormalOrderUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
//        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = 0;
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList getContractUpload = new ArrayList();
        ArrayList custContractUpload = new ArrayList();
        String shipmentId = "";
        String customerName = "";
        String city = "";
        String pinCode = "";
        String deliveryHub = "";
        String assignedExecutive = "";
        String origin = "";
        String currentHub = "";
        String status = "";
        String note = "";
        String amount = "";
        String weight = "";
        String lastModified = "";
        String lastReceived = "";
        String shipmentType = "";

//        int status = 0;
        int status1 = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            String custId = request.getParameter("custId");
            String contractId = request.getParameter("contractId");
            System.out.println("custId----" + custId);
            System.out.println("contractId----" + contractId);
            request.setAttribute("custId", custId);
            request.setAttribute("contractId", contractId);
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

//            int deleteUploadTemp = wBP.deleteUploadTemp();
//            System.out.println("deleteUploadTemp----"+deleteUploadTemp);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        int len = splitFileNames.length;
                        fileFormat = splitFileNames[len - 1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = uploadedFileName;
                                System.out.println("splitFileName[0]=" + splitFileName[0]);
                                System.out.println("splitFileName[0]=" + splitFileName[1]);

                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                System.out.println("tempPath..." + tempFilePath);
                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath));
                                System.out.println("tempPath = " + tempFilePath);
//                                System.out.println("actPath = " + actualFilePath);
//                                String parts = actualFilePath.substring(actualFilePath.lastIndexOf("//"));
//                                String part1 = parts.replace("//", "");
                                System.out.println("11111111111111111111111111");
                                if ("xlsx".equals(fileFormat)) {
                                    FileInputStream fis = new FileInputStream(tempFilePath);
                                    XSSFWorkbook wb = new XSSFWorkbook(fis);
                                    XSSFSheet sheet = wb.getSheetAt(0);
                                    int value = sheet.getLastRowNum();
                                    System.out.println("value  " + value);

                                    int sno = 0;
                                    Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                    while (itr.hasNext()) {
                                        Row row = itr.next();
                                        if (sno > 0) {
                                            System.out.println("cellyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy " + row.getCell(1));
                                            WmsTO wmsTO = new WmsTO();

                                            wmsTO.setUserId(userId + "");

                                            if (row.getCell(0) != null && !"".equals(row.getCell(0))) {
                                                row.getCell(0).setCellType(row.getCell(0).CELL_TYPE_STRING);
                                                shipmentId = row.getCell(0).getStringCellValue();
                                                System.out.println("AAAAAAAAAAAAAAAA " + shipmentId);
                                                wmsTO.setShipmentId(shipmentId);
                                            }

                                            if (row.getCell(1) != null && !"".equals(row.getCell(1))) {
                                                row.getCell(1).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                customerName = row.getCell(1).getStringCellValue();
                                                wmsTO.setCustomerName(customerName);
                                            }

                                            if (row.getCell(2) != null && !"".equals(row.getCell(2))) {
                                                row.getCell(2).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                city = row.getCell(2).getStringCellValue();
                                                wmsTO.setCity(city);
                                            }

                                            if (row.getCell(3) != null && !"".equals(row.getCell(3))) {
                                                row.getCell(3).setCellType(row.getCell(3).CELL_TYPE_STRING);
                                                pinCode = row.getCell(3).getStringCellValue();
                                                wmsTO.setPinCode(pinCode);
                                            }

                                            if (row.getCell(4) != null && !"".equals(row.getCell(4))) {
                                                row.getCell(4).setCellType(row.getCell(4).CELL_TYPE_STRING);
                                                deliveryHub = row.getCell(4).getStringCellValue();
                                                wmsTO.setDeliveryHub(deliveryHub);
                                            }

                                            if (row.getCell(5) != null && !"".equals(row.getCell(5))) {
                                                row.getCell(5).setCellType(row.getCell(5).CELL_TYPE_STRING);
                                                assignedExecutive = row.getCell(5).getStringCellValue();
                                                wmsTO.setAssignedExecutive(assignedExecutive);
                                            }

                                            if (row.getCell(6) != null && !"".equals(row.getCell(6))) {
                                                row.getCell(6).setCellType(row.getCell(6).CELL_TYPE_STRING);
                                                origin = row.getCell(6).getStringCellValue();
                                                wmsTO.setOrigin(origin);
                                            }

                                            if (row.getCell(7) != null && !"".equals(row.getCell(7))) {
                                                row.getCell(7).setCellType(row.getCell(7).CELL_TYPE_STRING);
                                                currentHub = row.getCell(7).getStringCellValue();
                                                wmsTO.setCurrentHub(currentHub);
                                            }

                                            if (row.getCell(8) != null && !"".equals(row.getCell(8))) {
                                                row.getCell(8).setCellType(row.getCell(8).CELL_TYPE_STRING);
                                                status = row.getCell(8).getStringCellValue();
                                                wmsTO.setStatus(status);
                                            }

                                            if (row.getCell(9) != null && !"".equals(row.getCell(9))) {
                                                row.getCell(9).setCellType(row.getCell(9).CELL_TYPE_STRING);
                                                note = row.getCell(9).getStringCellValue();
                                                wmsTO.setNote(note);
                                            }

                                            if (row.getCell(10) != null && !"".equals(row.getCell(10))) {
                                                row.getCell(10).setCellType(row.getCell(10).CELL_TYPE_STRING);
                                                amount = row.getCell(10).getStringCellValue();
                                                wmsTO.setAmount(amount);
                                            }

                                            if (row.getCell(11) != null && !"".equals(row.getCell(11))) {
                                                row.getCell(11).setCellType(row.getCell(11).CELL_TYPE_STRING);
                                                weight = row.getCell(11).getStringCellValue();
                                                wmsTO.setWeight(weight);
                                            }

                                            if (row.getCell(12) != null && !"".equals(row.getCell(12))) {
                                                row.getCell(12).setCellType(row.getCell(12).CELL_TYPE_STRING);
                                                lastModified = row.getCell(12).getStringCellValue();
                                                wmsTO.setLastModified(lastModified);
                                            }

                                            if (row.getCell(13) != null && !"".equals(row.getCell(3))) {
                                                row.getCell(13).setCellType(row.getCell(13).CELL_TYPE_STRING);
                                                lastReceived = row.getCell(13).getStringCellValue();
                                                wmsTO.setLastReceived(lastReceived);
                                            }

                                            row.getCell(14).setCellType(row.getCell(14).CELL_TYPE_STRING);
                                            shipmentType = row.getCell(14).getStringCellValue();
                                            wmsTO.setShipmentType(shipmentType);

                                            custContractUpload.add(wmsTO);

                                            status1 = wmsBP.saveNormalOrderTemp(wmsTO);
                                            System.out.println("status====" + status);
//                                    getContractUpload = wmsBP.getContractUpload(wmsTO);

                                        }
                                        sno++;

                                    }
                                } else if ("xls".equals(fileFormat)) {
                                    int count = 0;
                                    WorkbookSettings ws = new WorkbookSettings();
                                    ws.setLocale(new Locale("en", "EN"));
                                    Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                    Sheet s = workbook.getSheet(0);
                                    System.out.println("rows" + userId + s.getRows());

                                    for (int i = 1; i < s.getRows(); i++) {
                                        System.out.println("11111111111111111111111111");
                                        WmsTO wmsTO = new WmsTO();
                                        count++;

                                        shipmentId = s.getCell(0, i).getContents();
                                        System.out.println("AAAAAAAAAAAAAAAA " + shipmentId);
                                        wmsTO.setShipmentId(shipmentId);

                                        customerName = s.getCell(1, i).getContents();
                                        wmsTO.setCustomerName(customerName);

                                        city = s.getCell(2, i).getContents();
                                        wmsTO.setCity(city);

                                        pinCode = s.getCell(3, i).getContents();
                                        wmsTO.setPinCode(pinCode);

                                        deliveryHub = s.getCell(4, i).getContents();
                                        wmsTO.setDeliveryHub(deliveryHub);

                                        assignedExecutive = s.getCell(5, i).getContents();
                                        wmsTO.setAssignedExecutive(assignedExecutive);

                                        origin = s.getCell(6, i).getContents();
                                        wmsTO.setOrigin(origin);

                                        currentHub = s.getCell(7, i).getContents();
                                        wmsTO.setCurrentHub(currentHub);

                                        status = s.getCell(8, i).getContents();
                                        wmsTO.setStatus(status);

                                        note = s.getCell(9, i).getContents();
                                        wmsTO.setNote(note);

                                        amount = s.getCell(10, i).getContents();
                                        wmsTO.setAmount(amount);

                                        weight = s.getCell(11, i).getContents();
                                        wmsTO.setWeight(weight);

                                        lastModified = s.getCell(12, i).getContents();
                                        wmsTO.setLastModified(lastModified);
                                        System.out.println("vvvvvvvvvvvvvvvvvvvvvvvvv" + lastModified);

                                        lastReceived = s.getCell(13, i).getContents();
                                        wmsTO.setLastReceived(lastReceived);
                                        System.out.println("mmmmmmmmmmmmmmmmmmmmmmm" + lastReceived);

                                        shipmentType = s.getCell(14, i).getContents().toString();
                                        wmsTO.setShipmentType(shipmentType);

                                        custContractUpload.add(wmsTO);

                                        status1 = wmsBP.saveNormalOrderTemp(wmsTO);
                                        System.out.println("status====" + status);
//                                    getContractUpload = wmsBP.getContractUpload(wmsTO);

                                    }

                                }
                            }
                        } else {
                            request.setAttribute("errorMessage", "Order Upload Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            ArrayList getContractList = new ArrayList();
            getContractList = wmsBP.getCustomerContract(wms);
            request.setAttribute("getContractList", getContractList);
            path = "content/wms/normalOrderApproval.jsp";

            if (getContractList.size() > 0) {
                System.out.println("status====check================status" + status);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Order Uploaded Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView SaveOrderApproval(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "SaveOrderApproval";
        menuPath = "wms >>  SaveOrderApproval";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            String hubId = (String) session.getAttribute("hubId");
            System.out.println("hubId" + hubId);
            wmsTO.setWhId(hubId);
            int status = wmsBP.SaveOrderApproval(wmsTO, userId);
            System.out.println("status====" + status);
            path = "content/wms/normalOrderApproval.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Order Uploaded Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView replacementOrderUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "replacementOrderUpload";
        menuPath = "wms >>  replacementOrderUpload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            String param = request.getParameter("param");
            int status = wmsBP.deleteReplacementOrderTemp(userId);
//            System.out.println("status11111111111111111111111111===="+replacementOrderUpload);
            path = "content/wms/replacementOrderUpload.jsp";
//            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "shipment Uploaded Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView uploadReplacementOrder(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
//        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = 0;
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList getContractUpload = new ArrayList();
        ArrayList custContractUpload = new ArrayList();
        String shipmentId = "";
        String shipmentType = "";
        String shipmentTierType = "";
        String reverseShipmentId = "";
        String bagStatus = "";
        String type = "";
        String price = "";
        String latestStatus = "";
        String deliveryHub = "";
        String currentLocation = "";
        String latestUpdateDateTime = "";
        String firstReceivedHub = "";
        String firstReceiveDateTime = "";
        String hubNotes = "";
        String csNotes = "";
        String numberofAttempts = "";
        String bagId = "";
        String consignmentId = "";
        String customerPromiseDate = "";
        String logisticsPromiseDate = "";
        String onHoldByOpsReason = "";
        String onHoldByOpsDate = "";
        String firstAssignedHubName = "";
        String deliveryPinCode = "";
        String lastReceivedDateTime = "";

//        int status = 0;
        int status1 = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            String custId = request.getParameter("custId");
            String contractId = request.getParameter("contractId");
            System.out.println("custId----" + custId);
            System.out.println("contractId----" + contractId);
            request.setAttribute("custId", custId);
            request.setAttribute("contractId", contractId);
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        int len = splitFileNames.length;
                        fileFormat = splitFileNames[len - 1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = uploadedFileName;
                                System.out.println("splitFileName[0]=" + splitFileName[0]);
                                System.out.println("splitFileName[0]=" + splitFileName[1]);

                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                System.out.println("tempPath..." + tempFilePath);
                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath));
                                System.out.println("tempPath = " + tempFilePath);
//                                System.out.println("actPath = " + actualFilePath);
//                                String parts = actualFilePath.substring(actualFilePath.lastIndexOf("//"));
//                                String part1 = parts.replace("//", "");
                                System.out.println("11111111111111111111111111");
                                if ("xlsx".equals(fileFormat)) {
                                    FileInputStream fis = new FileInputStream(tempFilePath);
                                    XSSFWorkbook wb = new XSSFWorkbook(fis);
                                    XSSFSheet sheet = wb.getSheetAt(0);
                                    int value = sheet.getLastRowNum();
                                    System.out.println("value  " + value);

                                    int sno = 0;
                                    Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                    while (itr.hasNext()) {
                                        Row row = itr.next();
                                        if (sno > 0) {
                                            System.out.println("cellyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy " + row.getCell(1));
                                            WmsTO wmsTO = new WmsTO();

                                            wmsTO.setUserId(userId + "");

                                            if (row.getCell(0) != null && !"".equals(row.getCell(0))) {
                                                row.getCell(0).setCellType(row.getCell(0).CELL_TYPE_STRING);
                                                shipmentId = row.getCell(0).getStringCellValue();
                                                System.out.println("AAAAAAAAAAAAAAAA " + shipmentId);
                                                wmsTO.setShipmentId(shipmentId);
                                            }

                                            if (row.getCell(1) != null && !"".equals(row.getCell(1))) {
                                                row.getCell(1).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                shipmentType = row.getCell(1).getStringCellValue();
                                                wmsTO.setShipmentType(shipmentType);
                                            }

                                            if (row.getCell(2) != null && !"".equals(row.getCell(2))) {
                                                row.getCell(2).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                shipmentTierType = row.getCell(2).getStringCellValue();
                                                wmsTO.setShipmentTierType(shipmentTierType);
                                            }

                                            if (row.getCell(3) != null && !"".equals(row.getCell(3))) {
                                                row.getCell(3).setCellType(row.getCell(3).CELL_TYPE_STRING);
                                                reverseShipmentId = row.getCell(3).getStringCellValue();
                                                wmsTO.setReverseShipmentId(reverseShipmentId);
                                            }

                                            if (row.getCell(4) != null && !"".equals(row.getCell(4))) {
                                                row.getCell(4).setCellType(row.getCell(4).CELL_TYPE_STRING);
                                                bagStatus = row.getCell(4).getStringCellValue();
                                                wmsTO.setBagStatus(bagStatus);
                                            }

                                            if (row.getCell(5) != null && !"".equals(row.getCell(5))) {
                                                row.getCell(5).setCellType(row.getCell(5).CELL_TYPE_STRING);
                                                type = row.getCell(5).getStringCellValue();
                                                wmsTO.setType(type);
                                            }

                                            if (row.getCell(6) != null && !"".equals(row.getCell(6))) {
                                                row.getCell(6).setCellType(row.getCell(6).CELL_TYPE_STRING);
                                                price = row.getCell(6).getStringCellValue();
                                                wmsTO.setPrice(price);
                                            }

                                            if (row.getCell(7) != null && !"".equals(row.getCell(7))) {
                                                row.getCell(7).setCellType(row.getCell(7).CELL_TYPE_STRING);
                                                latestStatus = row.getCell(7).getStringCellValue();
                                                wmsTO.setLatestStatus(latestStatus);
                                            }

                                            if (row.getCell(8) != null && !"".equals(row.getCell(8))) {
                                                row.getCell(8).setCellType(row.getCell(8).CELL_TYPE_STRING);
                                                deliveryHub = row.getCell(8).getStringCellValue();
                                                wmsTO.setDeliveryHub(deliveryHub);
                                            }

                                            if (row.getCell(9) != null && !"".equals(row.getCell(9))) {
                                                row.getCell(9).setCellType(row.getCell(9).CELL_TYPE_STRING);
                                                currentLocation = row.getCell(9).getStringCellValue();
                                                wmsTO.setCurrentLocation(currentLocation);
                                            }

                                            if (row.getCell(10) != null && !"".equals(row.getCell(10))) {
                                                row.getCell(10).setCellType(row.getCell(10).CELL_TYPE_STRING);
                                                latestUpdateDateTime = row.getCell(10).getStringCellValue();
                                                wmsTO.setLatestUpdateDateTime(latestUpdateDateTime);
                                            }

                                            if (row.getCell(11) != null && !"".equals(row.getCell(11))) {
                                                row.getCell(11).setCellType(row.getCell(11).CELL_TYPE_STRING);
                                                firstReceivedHub = row.getCell(11).getStringCellValue();
                                                wmsTO.setFirstReceivedHub(firstReceivedHub);
                                            }

                                            if (row.getCell(12) != null && !"".equals(row.getCell(12))) {
                                                row.getCell(12).setCellType(row.getCell(12).CELL_TYPE_STRING);
                                                firstReceiveDateTime = row.getCell(12).getStringCellValue();
                                                wmsTO.setFirstReceiveDateTime(firstReceiveDateTime);
                                                System.out.println("vvvvvvvvvvvvvvvvvvvvvvvvv" + firstReceiveDateTime);
                                            }

                                            if (row.getCell(13) != null && !"".equals(row.getCell(13))) {
                                                row.getCell(13).setCellType(row.getCell(13).CELL_TYPE_STRING);
                                                hubNotes = row.getCell(13).getStringCellValue();
                                                wmsTO.setHubNotes(hubNotes);
                                                System.out.println("mmmmmmmmmmmmmmmmmmmmmmm" + hubNotes);
                                            }

                                            if (row.getCell(14) != null && !"".equals(row.getCell(14))) {
                                                row.getCell(14).setCellType(row.getCell(14).CELL_TYPE_STRING);
                                                csNotes = row.getCell(14).getStringCellValue();
                                                wmsTO.setCsNotes(csNotes);
                                            }

                                            if (row.getCell(15) != null && !"".equals(row.getCell(15))) {
                                                row.getCell(15).setCellType(row.getCell(15).CELL_TYPE_STRING);
                                                numberofAttempts = row.getCell(15).getStringCellValue();
                                                wmsTO.setNumberofAttempts(numberofAttempts);
                                            }

                                            if (row.getCell(16) != null && !"".equals(row.getCell(16))) {
                                                row.getCell(16).setCellType(row.getCell(16).CELL_TYPE_STRING);
                                                bagId = row.getCell(16).getStringCellValue();
                                                wmsTO.setBagId(bagId);
                                            }

                                            if (row.getCell(17) != null && !"".equals(row.getCell(17))) {
                                                row.getCell(17).setCellType(row.getCell(17).CELL_TYPE_STRING);
                                                consignmentId = row.getCell(17).getStringCellValue();
                                                wmsTO.setConsignmentId(consignmentId);
                                            }

                                            if (row.getCell(18) != null && !"".equals(row.getCell(18))) {
                                                row.getCell(18).setCellType(row.getCell(18).CELL_TYPE_STRING);
                                                customerPromiseDate = row.getCell(18).getStringCellValue();
                                                wmsTO.setCustomerPromiseDate(customerPromiseDate);
                                            }

                                            if (row.getCell(19) != null && !"".equals(row.getCell(19))) {
                                                row.getCell(19).setCellType(row.getCell(19).CELL_TYPE_STRING);
                                                logisticsPromiseDate = row.getCell(19).getStringCellValue();
                                                wmsTO.setLogisticsPromiseDate(logisticsPromiseDate);
                                            }

                                            if (row.getCell(20) != null && !"".equals(row.getCell(20))) {
                                                row.getCell(20).setCellType(row.getCell(20).CELL_TYPE_STRING);
                                                onHoldByOpsReason = row.getCell(20).getStringCellValue();
                                                wmsTO.setOnHoldByOpsReason(onHoldByOpsReason);
                                            }

                                            if (row.getCell(21) != null && !"".equals(row.getCell(21))) {
                                                row.getCell(21).setCellType(row.getCell(21).CELL_TYPE_STRING);
                                                onHoldByOpsDate = row.getCell(21).getStringCellValue();
                                                wmsTO.setOnHoldByOpsDate(onHoldByOpsDate);
                                            }

                                            if (row.getCell(22) != null && !"".equals(row.getCell(22))) {
                                                row.getCell(22).setCellType(row.getCell(22).CELL_TYPE_STRING);
                                                firstAssignedHubName = row.getCell(22).getStringCellValue();
                                                wmsTO.setFirstAssignedHubName(firstAssignedHubName);
                                            }

                                            if (row.getCell(23) != null && !"".equals(row.getCell(23))) {
                                                row.getCell(23).setCellType(row.getCell(23).CELL_TYPE_STRING);
                                                deliveryPinCode = row.getCell(23).getStringCellValue();
                                                wmsTO.setDeliveryPinCode(deliveryPinCode);
                                            }

                                            if (row.getCell(24) != null && !"".equals(row.getCell(24))) {
                                                row.getCell(24).setCellType(row.getCell(24).CELL_TYPE_STRING);
                                                lastReceivedDateTime = row.getCell(24).getStringCellValue();
                                                wmsTO.setLastReceivedDateTime(lastReceivedDateTime);
                                            }

                                            custContractUpload.add(wmsTO);

                                            status1 = wmsBP.uploadReplacementOrder(wmsTO);
                                            System.out.println("status====" + status1);
//                                    getContractUpload = wmsBP.getContractUpload(wmsTO);

                                        }
                                        sno++;

                                    }
                                } else if ("xls".equals(fileFormat)) {

                                    int count = 0;
                                    WorkbookSettings ws = new WorkbookSettings();
                                    ws.setLocale(new Locale("en", "EN"));
                                    Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                    Sheet s = workbook.getSheet(0);
                                    System.out.println("rows" + userId + s.getRows());

                                    for (int i = 1; i < s.getRows(); i++) {
                                        System.out.println("11111111111111111111111111");
                                        WmsTO wmsTO = new WmsTO();
                                        count++;

                                        shipmentId = s.getCell(0, i).getContents();
                                        System.out.println("AAAAAAAAAAAAAAAA " + shipmentId);
                                        wmsTO.setShipmentId(shipmentId);

                                        shipmentType = s.getCell(1, i).getContents();
                                        wmsTO.setShipmentType(shipmentType);

                                        shipmentTierType = s.getCell(2, i).getContents();
                                        wmsTO.setShipmentTierType(shipmentTierType);

                                        reverseShipmentId = s.getCell(3, i).getContents();
                                        wmsTO.setReverseShipmentId(reverseShipmentId);

                                        bagStatus = s.getCell(4, i).getContents();
                                        wmsTO.setBagStatus(bagStatus);

                                        type = s.getCell(5, i).getContents();
                                        wmsTO.setType(type);

                                        price = s.getCell(6, i).getContents();
                                        wmsTO.setPrice(price);

                                        latestStatus = s.getCell(7, i).getContents();
                                        wmsTO.setLatestStatus(latestStatus);

                                        deliveryHub = s.getCell(8, i).getContents();
                                        wmsTO.setDeliveryHub(deliveryHub);

                                        currentLocation = s.getCell(9, i).getContents();
                                        wmsTO.setCurrentLocation(currentLocation);

                                        latestUpdateDateTime = s.getCell(10, i).getContents();
                                        wmsTO.setLatestUpdateDateTime(latestUpdateDateTime);

                                        firstReceivedHub = s.getCell(11, i).getContents();
                                        wmsTO.setFirstReceivedHub(firstReceivedHub);

                                        firstReceiveDateTime = s.getCell(12, i).getContents();
                                        wmsTO.setFirstReceiveDateTime(firstReceiveDateTime);
                                        System.out.println("vvvvvvvvvvvvvvvvvvvvvvvvv" + firstReceiveDateTime);

                                        hubNotes = s.getCell(13, i).getContents();
                                        wmsTO.setHubNotes(hubNotes);
                                        System.out.println("mmmmmmmmmmmmmmmmmmmmmmm" + hubNotes);

                                        csNotes = s.getCell(14, i).getContents().toString();
                                        wmsTO.setCsNotes(csNotes);

                                        numberofAttempts = s.getCell(15, i).getContents().toString();
                                        wmsTO.setNumberofAttempts(numberofAttempts);

                                        bagId = s.getCell(16, i).getContents().toString();
                                        wmsTO.setBagId(bagId);

                                        consignmentId = s.getCell(17, i).getContents().toString();
                                        wmsTO.setConsignmentId(consignmentId);

                                        customerPromiseDate = s.getCell(18, i).getContents().toString();
                                        wmsTO.setCustomerPromiseDate(customerPromiseDate);

                                        logisticsPromiseDate = s.getCell(19, i).getContents().toString();
                                        wmsTO.setLogisticsPromiseDate(logisticsPromiseDate);

                                        onHoldByOpsReason = s.getCell(20, i).getContents().toString();
                                        wmsTO.setOnHoldByOpsReason(onHoldByOpsReason);

                                        onHoldByOpsDate = s.getCell(21, i).getContents().toString();
                                        wmsTO.setOnHoldByOpsDate(onHoldByOpsDate);

                                        firstAssignedHubName = s.getCell(22, i).getContents().toString();
                                        wmsTO.setFirstAssignedHubName(firstAssignedHubName);

                                        deliveryPinCode = s.getCell(23, i).getContents().toString();
                                        wmsTO.setDeliveryPinCode(deliveryPinCode);

                                        lastReceivedDateTime = s.getCell(24, i).getContents().toString();
                                        wmsTO.setLastReceivedDateTime(lastReceivedDateTime);

                                        custContractUpload.add(wmsTO);

                                        status1 = wmsBP.uploadReplacementOrder(wmsTO);
                                        System.out.println("status====" + status1);
//                                    getContractUpload = wmsBP.getContractUpload(wmsTO);

                                    }

                                }
                            }
                        } else {
                            request.setAttribute("errorMessage", "Contract Updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            ArrayList getreplacementList = new ArrayList();
            getreplacementList = wmsBP.getreplacementList(wms);
            request.setAttribute("getreplacementList", getreplacementList);
            path = "content/wms/replacementOrderUpload.jsp";

            if (getreplacementList.size() > 0) {
                System.out.println("getContractList====check================status" + getreplacementList);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Contract Updated Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            System.out.println("exception" + exception.toString().split(":")[1]);
            String ErrorConst = (exception.toString().split(":")[1]);
            request.setAttribute("errorConst", ErrorConst);
            System.out.println("err   " + ErrorConst);
            System.out.println("oiiiii " + exception);
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView saveReplacementOrderUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "SaveOrderUpload";
        menuPath = "wms >>  SaveOrderUpload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            String hubId = (String) session.getAttribute("hubId");
            wmsTO.setWhId(hubId);
            int status = wmsBP.saveReplacementOrderUpload(wmsTO, userId);
            System.out.println("status====" + status);
            path = "content/wms/replacementOrderUpload.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Order Uploaded Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView returnOrderUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "returnOrderUpload";
        menuPath = "wms >>  returnOrderUpload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            int status = wmsBP.deleteReturnOrderTemp(userId);
            String param = request.getParameter("param");
//            System.out.println("status11111111111111111111111111===="+returnOrderUpload);

            path = "content/wms/returnOrderUpload.jsp";
//            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "shipment Uploaded Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView savereturnOrder(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
//        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = 0;
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList getContractUpload = new ArrayList();
        ArrayList custContractUpload = new ArrayList();
        String trackingId = "";
        String pickupaddress = "";
        String pinCode = "";
        String itemName = "";
        String itemNos = "";
        String createdDate = "";
        String pickupDate = "";
        String Type = "";
        String lastStatus = "";
        String remarks = "";
        String numberofAttempts = "";

//        int status = 0;
        int status1 = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            String custId = request.getParameter("custId");
            String contractId = request.getParameter("contractId");
            System.out.println("custId----" + custId);
            System.out.println("contractId----" + contractId);
            request.setAttribute("custId", custId);
            request.setAttribute("contractId", contractId);
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

//            int deleteUploadTemp = wBP.deleteUploadTemp();
//            System.out.println("deleteUploadTemp----"+deleteUploadTemp);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        int len = splitFileNames.length;
                        fileFormat = splitFileNames[len - 1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = uploadedFileName;
                                System.out.println("splitFileName[0]=" + splitFileName[0]);
                                System.out.println("splitFileName[0]=" + splitFileName[1]);

                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                System.out.println("tempPath..." + tempFilePath);
                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath));
                                System.out.println("tempPath = " + tempFilePath);
//                                System.out.println("actPath = " + actualFilePath);
//                                String parts = actualFilePath.substring(actualFilePath.lastIndexOf("//"));
//                                String part1 = parts.replace("//", "");
                                System.out.println("11111111111111111111111111");
                                if ("xlsx".equals(fileFormat)) {
                                    FileInputStream fis = new FileInputStream(tempFilePath);
                                    XSSFWorkbook wb = new XSSFWorkbook(fis);
                                    XSSFSheet sheet = wb.getSheetAt(0);
                                    int value = sheet.getLastRowNum();
                                    System.out.println("value  " + value);

                                    int sno = 0;
                                    Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                    while (itr.hasNext()) {
                                        Row row = itr.next();
                                        if (sno > 0) {
                                            System.out.println("cellyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy " + row.getCell(1));
                                            WmsTO wmsTO = new WmsTO();

                                            if (row.getCell(0) != null && !"".equals(row.getCell(0))) {
                                                row.getCell(0).setCellType(row.getCell(0).CELL_TYPE_STRING);
                                                trackingId = row.getCell(0).getStringCellValue();
                                                System.out.println("AAAAAAAAAAAAAAAA " + trackingId);
                                                wmsTO.setTrackingId(trackingId);
                                            }

                                            if (row.getCell(1) != null && !"".equals(row.getCell(1))) {
                                                row.getCell(1).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                pickupaddress = row.getCell(1).getStringCellValue();
                                                wmsTO.setPickupaddress(pickupaddress);
                                            }

                                            if (row.getCell(2) != null && !"".equals(row.getCell(2))) {
                                                row.getCell(2).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                pinCode = row.getCell(2).getStringCellValue();
                                                wmsTO.setPinCode(pinCode);
                                            }

                                            if (row.getCell(3) != null && !"".equals(row.getCell(3))) {
                                                row.getCell(3).setCellType(row.getCell(3).CELL_TYPE_STRING);
                                                itemName = row.getCell(3).getStringCellValue();
                                                wmsTO.setItemName(itemName);
                                            }

                                            if (row.getCell(4) != null && !"".equals(row.getCell(4))) {
                                                row.getCell(4).setCellType(row.getCell(4).CELL_TYPE_STRING);
                                                itemNos = row.getCell(4).getStringCellValue();
                                                wmsTO.setItemNos(itemNos);
                                            }

                                            if (row.getCell(5) != null && !"".equals(row.getCell(5))) {
                                                row.getCell(5).setCellType(row.getCell(5).CELL_TYPE_STRING);
                                                createdDate = row.getCell(5).getStringCellValue();
                                                wmsTO.setCreatedDate(createdDate);
                                            }

                                            if (row.getCell(6) != null && !"".equals(row.getCell(6))) {
                                                row.getCell(6).setCellType(row.getCell(6).CELL_TYPE_STRING);
                                                pickupDate = row.getCell(6).getStringCellValue();
                                                wmsTO.setPickupDate(pickupDate);
                                            }

                                            if (row.getCell(7) != null && !"".equals(row.getCell(7))) {
                                                row.getCell(7).setCellType(row.getCell(7).CELL_TYPE_STRING);
                                                Type = row.getCell(7).getStringCellValue();
                                                wmsTO.setType(Type);
                                            }

                                            if (row.getCell(8) != null && !"".equals(row.getCell(8))) {
                                                row.getCell(8).setCellType(row.getCell(8).CELL_TYPE_STRING);
                                                lastStatus = row.getCell(8).getStringCellValue();
                                                wmsTO.setLastStatus(lastStatus);
                                            }

                                            if (row.getCell(9) != null && !"".equals(row.getCell(9))) {
                                                row.getCell(9).setCellType(row.getCell(9).CELL_TYPE_STRING);
                                                remarks = row.getCell(9).getStringCellValue();
                                                wmsTO.setRemarks(remarks);
                                            }

                                            if (row.getCell(10) != null && !"".equals(row.getCell(10))) {
                                                row.getCell(10).setCellType(row.getCell(10).CELL_TYPE_STRING);
                                                numberofAttempts = row.getCell(10).getStringCellValue();
                                                wmsTO.setNumberofAttempts(numberofAttempts);
                                            }

                                            custContractUpload.add(wmsTO);

                                            status1 = wmsBP.uploadReturnordertemp(wmsTO);
                                            System.out.println("status====" + status1);

                                        }
                                        sno++;

                                    }
                                } else if ("xls".equals(fileFormat)) {

                                    int count = 0;
                                    WorkbookSettings ws = new WorkbookSettings();
                                    ws.setLocale(new Locale("en", "EN"));
                                    Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                    Sheet s = workbook.getSheet(0);
                                    System.out.println("rows" + userId + s.getRows());

                                    for (int i = 1; i < s.getRows(); i++) {
                                        System.out.println("11111111111111111111111111");
                                        WmsTO wmsTO = new WmsTO();
                                        count++;

                                        trackingId = s.getCell(0, i).getContents();
                                        System.out.println("AAAAAAAAAAAAAAAA " + trackingId);
                                        wmsTO.setTrackingId(trackingId);

                                        pickupaddress = s.getCell(1, i).getContents();
                                        wmsTO.setPickupaddress(pickupaddress);

                                        pinCode = s.getCell(2, i).getContents();
                                        wmsTO.setPinCode(pinCode);

                                        itemName = s.getCell(3, i).getContents();
                                        wmsTO.setItemName(itemName);

                                        itemNos = s.getCell(4, i).getContents();
                                        wmsTO.setItemNos(itemNos);

                                        createdDate = s.getCell(5, i).getContents();
                                        wmsTO.setCreatedDate(createdDate);

                                        pickupDate = s.getCell(6, i).getContents();
                                        wmsTO.setPickupDate(pickupDate);

                                        Type = s.getCell(7, i).getContents();
                                        wmsTO.setType(Type);

                                        lastStatus = s.getCell(8, i).getContents();
                                        wmsTO.setLastStatus(lastStatus);

                                        remarks = s.getCell(9, i).getContents();
                                        wmsTO.setRemarks(remarks);

                                        numberofAttempts = s.getCell(10, i).getContents();
                                        wmsTO.setNumberofAttempts(numberofAttempts);

                                        custContractUpload.add(wmsTO);

                                        status1 = wmsBP.uploadReturnordertemp(wmsTO);
                                        System.out.println("status====" + status1);

                                    }

                                }
                            }
                        } else {
                            request.setAttribute("errorMessage", "Contract Updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            ArrayList getreplacementList = new ArrayList();
            getreplacementList = wmsBP.getreturnList(wms);
            request.setAttribute("getreplacementList", getreplacementList);
            path = "content/wms/returnOrderUpload.jsp";

            if (getreplacementList.size() > 0) {
                System.out.println("getContractList====check================status" + getreplacementList);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Contract Updated Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView saveReturnOrderUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "SaveOrderApproval";
        menuPath = "wms >>  SaveOrderApproval";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            String hubId = (String) session.getAttribute("hubId");
            wmsTO.setWhId(hubId);
            int status = wmsBP.returnContractUpload(wmsTO, userId);
            System.out.println("status====" + status);

            path = "content/wms/returnOrderUpload.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Order Updated Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView ifbLrUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "ifbLRUpdate";
        menuPath = "wms >>  ifbLRUpdate";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int roleId = (Integer) session.getAttribute("RoleId");
        int saveIfbLr = 0;
        wmsTO.setUserId(userId + "");
        wmsTO.setRoleId(roleId + "");
        try {
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String invoiceNo = "";
            String param = request.getParameter("param");
            wmsTO.setFromDate(fromDate + "");
            wmsTO.setToDate(toDate + "");
            ArrayList ifbLRUploadList = wmsBP.ifbLRUploadList(wmsTO);
            request.setAttribute("ifbLRUploadList", ifbLRUploadList);
            path = "content/wms/ifbLrUpload.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView ifbLrPrint(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "ifbLRUpdate";
        menuPath = "wms >>  ifbLRUpdate";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
//         int ewayBillNo = (Integer) session.getAttribute("ewayBillNoTemp");
//         System.out.println("ewayBillNo"+ ewayBillNo);
        int saveIfbLr = 0;
        wmsTO.setUserId(userId + "");
        try {
            String[] lrIds = request.getParameterValues("lrId");
            String[] selected = request.getParameterValues("selectedStatus");
            ArrayList ifbLRPrintlist = wmsBP.ifbLRPrintlist(lrIds, selected);
            WmsTO wmsTO1 = new WmsTO();
//            Iterator itr = ifbLRPrintlist.iterator();
//            if ("0".equals(type)) {
//                while (itr.hasNext()) {
//                    wmsTO1 = (WmsTO) itr.next();
//                    request.setAttribute("customerName", wmsTO1.getCustomerName());
//                    request.setAttribute("pincode", wmsTO1.getPincode());
//                    request.setAttribute("gstNo", wmsTO1.getGstinNumber());
//                    request.setAttribute("phoneNo", wmsTO1.getMobileNo());
//                    request.setAttribute("city", wmsTO1.getCity());
//                    request.setAttribute("custName", wmsTO1.getCustName().split("~")[0]);
//                    request.setAttribute("custAddress", wmsTO1.getCustName().split("~")[1]);
//                    request.setAttribute("custCity", wmsTO1.getCustName().split("~")[2]);
//                    request.setAttribute("custPincode", wmsTO1.getCustName().split("~")[3]);
//                    request.setAttribute("custPhone", wmsTO1.getCustName().split("~")[4]);
//                    request.setAttribute("ewayBillNo", wmsTO1.getEwayBillNo());
//                    request.setAttribute("ewayExpiry", wmsTO1.getEwayExpiry());
//                    request.setAttribute("lrNo", wmsTO1.getLrNo());
//                    request.setAttribute("invoiceNo", wmsTO1.getInvoiceNo());
//                    request.setAttribute("grossValue", wmsTO1.getGrossValue());
//                    request.setAttribute("itemDescription", wmsTO1.getMaterialDescription());
//                    request.setAttribute("qty", wmsTO1.getQty());
//                    request.setAttribute("pickupDate", wmsTO1.getLastModified().split(" ")[0]);
//                    request.setAttribute("pickupTime", wmsTO1.getLastModified().split(" ")[1]);
//                }
//            } else if ("1".equals(type)) {
//                while (itr.hasNext()) {
//                    wmsTO1 = (WmsTO) itr.next();
//                    request.setAttribute("custName", wmsTO1.getCustomerName());
//                    request.setAttribute("custPincode", wmsTO1.getPincode());
//                    request.setAttribute("custGstNo", wmsTO1.getGstinNumber());
//                    request.setAttribute("custPhone", wmsTO1.getMobileNo());
//                    request.setAttribute("custCity", wmsTO1.getCity());
//                    request.setAttribute("customerName", wmsTO1.getCustName().split("~")[0]);
//                    request.setAttribute("customerAddress", wmsTO1.getCustName().split("~")[1]);
//                    request.setAttribute("city", wmsTO1.getCustName().split("~")[2]);
//                    request.setAttribute("pincode", wmsTO1.getCustName().split("~")[3]);
//                    request.setAttribute("phoneNo", wmsTO1.getCustName().split("~")[4]);
//                    request.setAttribute("ewayBillNo", wmsTO1.getEwayBillNo());
//                    request.setAttribute("ewayExpiry", wmsTO1.getEwayExpiry());
//                    request.setAttribute("lrNo", wmsTO1.getLrNo());
//                    request.setAttribute("invoiceNo", wmsTO1.getInvoiceNo());
//                    request.setAttribute("grossValue", wmsTO1.getGrossValue());
//                    request.setAttribute("itemDescription", wmsTO1.getMaterialDescription());
//                    request.setAttribute("qty", wmsTO1.getQty());
//                    request.setAttribute("pickupDate", wmsTO1.getLastModified().split(" ")[0]);
//                    request.setAttribute("pickupTime", wmsTO1.getLastModified().split(" ")[1]);
//                }
//            }
            request.setAttribute("ifbLRPrintlist", ifbLRPrintlist);
            path = "content/wms/ifbLrPrint.jsp";

//            ArrayList ifbLRList = wmsBP.ifbLRList(wmsTO);
//            request.setAttribute("ifbLRList", ifbLRList);
//            path = "content/wms/ifbLrPrint.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView viewFkReconcile(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        int updateFkReconcile = 0;
        try {
            String param = request.getParameter("param");
            String tripId = request.getParameter("tripId");
            String statusId = request.getParameter("statusId");
            request.setAttribute("statusId", statusId);
            wmsTO.setUserId(userId + "");
            wmsTO.setTripId(tripId);
            wmsTO.setWhId(whId);
            request.setAttribute("tripId", tripId);
            if ("save".equals(param)) {
                String[] deCods = request.getParameterValues("deCod");
                String[] cardAmounts = request.getParameterValues("cardAmount");
                String[] prexoAmounts = request.getParameterValues("prexoAmount");
                String[] shipmentType = request.getParameterValues("shipmentType");
                wmsTO.setDeCods(deCods);
                wmsTO.setShipmentTypes(shipmentType);
                wmsTO.setCardAmounts(cardAmounts);
                wmsTO.setPrexoAmounts(prexoAmounts);
                String[] orderIds = request.getParameterValues("orderId");
                String totalCod = request.getParameter("totalCod");
                String totalDeCod = request.getParameter("totalDeCod");
                String totalCard = request.getParameter("totalCard");
                String totalPrexo = request.getParameter("totalPrexo");
                wmsTO.setTotalCod(totalCod);
                wmsTO.setTotalDeCod(totalDeCod);
                wmsTO.setTotalCardAmount(totalCard);
                wmsTO.setTotalPrexoAmount(totalPrexo);
                updateFkReconcile = wmsBP.updateFkReconcile(wmsTO, orderIds);
                if (updateFkReconcile > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Reconciliation Done Successfully");
                }
                path = "content/wms/viewFkReconcile.jsp";
            } else if ("view".equals(param)) {
                path = "content/wms/fkReconcileViewDetails.jsp";
            } else {
                path = "content/wms/viewFkReconcile.jsp";
            }
            ArrayList reconcileMaster = wmsBP.getFkReconcile(wmsTO);
            request.setAttribute("reconcileMaster", reconcileMaster);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView viewFkCashDeposit(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        try {
            int updateFkReconcile = 0;
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                wmsTO.setWhId(whId);
                wmsTO.setUserId(userId + "");
                String depositAmount = request.getParameter("depositAmount");
                String reconcileId[] = request.getParameterValues("reconcileId");
                String selectedStatus[] = request.getParameterValues("selectedStatus");
                String depositType = request.getParameter("depositType");
                String fromDate = request.getParameter("fromDate");
                String toDate = request.getParameter("toDate");
                wmsTO.setDepositAmount(depositAmount);
                wmsTO.setDepositType(depositType);
                wmsTO.setSelectedIndex(selectedStatus);
                wmsTO.setReconcileIds(reconcileId);
                wmsTO.setFromDate(fromDate);
                wmsTO.setToDate(toDate);
                updateFkReconcile = wmsBP.updateCashDeposit(wmsTO);
                if (updateFkReconcile > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Reconciliation Done Successfully");
                }
            }
            wmsTO.setUserId(userId + "");
            ArrayList reconcileMaster = wmsBP.getFkReconcileMaster(wmsTO);
            request.setAttribute("reconcileMaster", reconcileMaster);
            path = "content/wms/viewFkCashDeposit.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView fkPodUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            wmsTO.setUserId(userId + "");
            String param = request.getParameter("param");
            ArrayList cashDepositMaster = wmsBP.getFkCashDeposit(wmsTO);
            request.setAttribute("cashDeposit", cashDepositMaster);
            path = "content/wms/fkPodUpload.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView ifbPodApproval(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            int roleId = (Integer) session.getAttribute("RoleId");
            wmsTO.setRoleId(roleId + "");
            wmsTO.setUserId(userId + "");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            String param = request.getParameter("param");
            if ("upload".equals(param)) {
                String podId = request.getParameter("podId");
                String invoiceNo = request.getParameter("invoiceNo");
                request.setAttribute("podId", podId);
                request.setAttribute("invoiceNo", invoiceNo);
                path = "content/wms/ifbUploadPod.jsp";
            } else if ("approve".equals(param)) {
                String podId = request.getParameter("podId");
                int approveIfbPod = wmsBP.approveIfbPod(podId);
                ArrayList ifbPodDetails = wmsBP.ifbPodDetails(wmsTO);
                request.setAttribute("podDetails", ifbPodDetails);
                if (approveIfbPod > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Pod Approved Successfully.");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "POD Approval Failed");

                }
                path = "content/wms/ifbPodApproval.jsp";
            } else if ("download".equals(param)) {
                String[] Ids = request.getParameter("podCopys").split(",");
                ArrayList ifbPodDetails = wmsBP.ifbPodDetailsDownload(Ids);
                request.setAttribute("podDetails", ifbPodDetails);
                path = "content/wms/ifbPodDownload.jsp";
            } else {
                ArrayList ifbPodDetails = wmsBP.ifbPodDetails(wmsTO);
                request.setAttribute("podDetails", ifbPodDetails);
                path = "content/wms/ifbPodApproval.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView ifbUploadPod(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        wmsCommand = command;
        String menuPath = "DockIn Details";
        int insertStatus = 0;
        ModelAndView mv = null;
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int j = 0;
        int s = 0;
        int t = 0;
        String[] fileSaved = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        String[] serialNos = new String[500];
        String[] binIds = new String[500];
        String podId = "";
        String podRemarks = "";
        try {

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Dockin Details";
            request.setAttribute("pageTitle", pageTitle);

            WmsTO wmsTO = new WmsTO();

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/pod/");
                        // actualServerFilePath = "D:\\HS\\Images";
                        System.out.println("Server Path == " + actualServerFilePath);
//                        tempServerFilePath = actualServerFilePath.replace("//", "////");
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        Date now = new Date();
                        String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                        String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                        fPart = (FilePart) partObj;
                        uploadedFileName[j] = fPart.getFileName();

                        if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {

                            String[] splitFileName = uploadedFileName[j].split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath[j] = tempServerFilePath + "//" + fileSaved[j];
                            actualFilePath = actualServerFilePath + "//" + tempFilePath;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath[j]));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
//                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
//                            String part1 = parts.replace("\\", "");
                        }

                        System.out.println("fileName..." + fileName);
                        j++;
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("podId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            podId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("podRemarks")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            podRemarks = paramPart.getStringValue();
                        }
                    }
                }
            }

//             wmsTO.setSerialNos(request.getParameterValues("serialNos"));
            String file = "";
            String file1 = "";
            String saveFile = "";
            String saveFile1 = "";
            int status = 0;

            System.out.println("value of j" + j);
            file = tempFilePath[0];
            System.out.println("file :" + file);
            saveFile = fileSaved[0];
            System.out.println("saveFile :" + saveFile);

            status = wmsBP.saveIfbPodFile(podRemarks, podId, file, saveFile);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Pod Uploaded Successfully.");
            } else {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "POD Upload Failed");

            }
            path = "content/wms/ifbUploadPod.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView deliveryPodUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            wmsTO.setUserId(userId + "");
            String param = request.getParameter("param");
            if ("upload".equals(param)) {
                String depositId = request.getParameter("depositId");
                String depositCode = request.getParameter("depositCode");
                System.out.println("depositId = " + depositId);
                System.out.println("depositCode = " + depositCode);
                request.setAttribute("depositId", depositId);
                request.setAttribute("depositCode", depositCode);
                path = "content/wms/fkUploadDeposit.jsp";
            } else if ("approve".equals(param)) {
                String depositId = request.getParameter("depositId");
                System.out.println("depositId = " + depositId);
                int approveFkPod = wmsBP.approveFkDeposit(depositId);
                ArrayList fkPodDetails = wmsBP.fkCashDepositDetails(wmsTO);
                request.setAttribute("podDetails", fkPodDetails);
                if (approveFkPod > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Pod submitted Successfully.");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "POD submitted Failed");

                }
                path = "content/wms/fkDepositApproval.jsp";
            } else {
                ArrayList fkPodDetails = wmsBP.fkCashDepositDetails(wmsTO);
                request.setAttribute("podDetails", fkPodDetails);
                path = "content/wms/fkDepositApproval.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView saveDeliveryPodUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        wmsCommand = command;
        String menuPath = "DockIn Details";
        int insertStatus = 0;
        ModelAndView mv = null;
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int j = 0;
        int s = 0;
        int t = 0;
        String[] fileSaved = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        String[] serialNos = new String[500];
        String[] binIds = new String[500];
        String depositId = "";
        String podRemarks = "";
        try {

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Dockin Details";
            request.setAttribute("pageTitle", pageTitle);

            WmsTO wmsTO = new WmsTO();

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/pod/");
                        // actualServerFilePath = "D:\\HS\\Images";
                        System.out.println("Server Path == " + actualServerFilePath);
//                        tempServerFilePath = actualServerFilePath.replace("//", "////");
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        Date now = new Date();
                        String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                        String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                        fPart = (FilePart) partObj;
                        uploadedFileName[j] = fPart.getFileName();

                        String[] splitFileNames = uploadedFileName[j].split("\\.");
                        int len = splitFileNames.length;
                        if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {

                            String[] splitFileName = uploadedFileName[j].split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[len - 1];
                            fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[len - 1];
                            fileName = fileSavedAs;
                            tempFilePath[j] = tempServerFilePath + "//" + fileSaved[j];
                            actualFilePath = actualServerFilePath + "//" + tempFilePath;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath[j]));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
//                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
//                            String part1 = parts.replace("\\", "");
                        }

                        System.out.println("fileName..." + fileName);
                        j++;
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("depositId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            depositId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("podRemarks")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            podRemarks = paramPart.getStringValue();
                        }
                    }
                }
            }

            String file = "";
            String saveFile = "";
            int status = 0;

            System.out.println("value of j" + j);
            file = tempFilePath[0];
            System.out.println("file :" + file);
            saveFile = fileSaved[0];
            System.out.println("saveFile :" + saveFile);

            status = wmsBP.saveFkDepositFile(podRemarks, depositId, file, saveFile);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Pod Uploaded Successfully.");
            } else {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "POD Upload Failed");

            }
            path = "content/wms/fkUploadDeposit.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView showReconcile(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            wmsTO.setUserId(userId + "");
            String param = request.getParameter("param");
            if ("show".equals(param)) {
                String depositId = request.getParameter("depositId");
                System.out.println("depositId = " + depositId);
                wmsTO.setDepositId(depositId);
                ArrayList ifbPodDetails = wmsBP.showReconcile(wmsTO);
                request.setAttribute("podDetails", ifbPodDetails);
                path = "content/wms/showReconcile.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView showShipment(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            String param = request.getParameter("param");
            String reconcileId = request.getParameter("reconcileId");
            wmsTO.setUserId(userId + "");
            wmsTO.setReconcileId(reconcileId);
            ArrayList reconcileMaster = wmsBP.getShipmentDetails(wmsTO);
            request.setAttribute("reconcileMaster", reconcileMaster);

            path = "content/wms/showShipment.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView ifbShipmentReport(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int roleId = (Integer) session.getAttribute("RoleId");
        try {
            String param = request.getParameter("param");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String invoiceStatus = request.getParameter("invoiceStatus");
            String orderType = request.getParameter("orderType");

            wmsTO.setRoleId(roleId + "");
            wmsTO.setUserId(userId + "");
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);
            wmsTO.setOrderType(orderType);
            wmsTO.setInvoiceStatus(invoiceStatus);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("invoiceStatus", invoiceStatus);
            request.setAttribute("orderType", orderType);

            ArrayList ifbShipmentReport = wmsBP.ifbShipmentReport(wmsTO);
            request.setAttribute("ifbShipmentReport", ifbShipmentReport);
            if ("ExportExcel".equals(param)) {
                path = "content/wms/ifbShipmentReportExcel.jsp";
            } else {
                path = "content/wms/ifbShipmentReport.jsp";
            }
        } catch (FPRuntimeException exception) {
            exception.printStackTrace();
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView ifbRunsheetReport(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        OpsTO opsTO = new OpsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int roleId = (Integer) session.getAttribute("RoleId");
        try {
            String param = request.getParameter("param");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String empId = request.getParameter("empId");
            String vehicleNo = request.getParameter("vehicleNo");
            String orderType = request.getParameter("orderType");

            opsTO.setUserId(userId);
            wmsTO.setUserId(userId + "");
            wmsTO.setRoleId(roleId + "");
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);
            wmsTO.setVehicleNo(vehicleNo);
            wmsTO.setOrderType(orderType);
            wmsTO.setEmpId(empId);

            ArrayList deliveryExecutiveList = opsBP.deliveryExecutiveList(opsTO);
            System.out.println("deliveryExecutiveList.size() = " + deliveryExecutiveList.size());
            request.setAttribute("deliveryExecutiveList", deliveryExecutiveList);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("vehicleNo", vehicleNo);
            request.setAttribute("empId", empId);
            request.setAttribute("orderType", orderType);

            ArrayList ifbRunsheetReport = wmsBP.ifbRunsheetReport(wmsTO);
            request.setAttribute("ifbRunsheetReport", ifbRunsheetReport);
            if ("ExportExcel".equals(param)) {
                path = "content/wms/ifbRunsheetReportExcel.jsp";
            } else {
                path = "content/wms/ifbRunsheetReport.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView fkRunsheetReport(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        OpsTO opsTO = new OpsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int roleId = (Integer) session.getAttribute("RoleId");
        String userWhId = (String) session.getAttribute("hubId");
        request.setAttribute("RoleId", roleId);

        try {
            String param = request.getParameter("param");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String empId = request.getParameter("empId");
            String orderType = request.getParameter("orderType");
            String whId = request.getParameter("whId");
            wmsTO.setWhId(whId);
            opsTO.setUserId(userId);
            wmsTO.setUserId(userId + "");
            if (fromDate == null || "".equals(fromDate)) {
                Date date = new Date();
                DateFormat s = new SimpleDateFormat("dd-MM-yyyy");
                String date1 = s.format(date);
                date1 = "01-" + date1.split("-")[1] + "-" + date1.split("-")[2];
                fromDate = date1;
                wmsTO.setFromDate(fromDate);
            }
            if (toDate == null || "".equals(toDate)) {
                Date date = new Date();
                DateFormat s = new SimpleDateFormat("dd-MM-yyyy");
                String date1 = s.format(date);
                toDate = date1;
                wmsTO.setToDate(toDate);
            }
            wmsTO.setOrderType(orderType);
            wmsTO.setEmpId(empId);
            wmsTO.setRoleId(roleId + "");

            ArrayList deliveryExecutiveList = opsBP.deliveryExecutiveList(opsTO);
            System.out.println("deliveryExecutiveList.size() = " + deliveryExecutiveList.size());
            request.setAttribute("deliveryExecutiveList", deliveryExecutiveList);

            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareToHouseList(userWhId);
            request.setAttribute("getWareHouseList", getWareHouseList);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("empId", empId);
            request.setAttribute("orderType", orderType);

            ArrayList fkRunsheetReport = wmsBP.fkRunsheetReport(wmsTO);
            request.setAttribute("fkRunsheetReport", fkRunsheetReport);
            if ("ExportExcel".equals(param)) {
                path = "content/wms/fkRunsheetReportExcel.jsp";
            } else {
                path = "content/wms/fkRunsheetReport.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView fkReconcileView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            String param = request.getParameter("param");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");

            wmsTO.setUserId(userId + "");
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            ArrayList fkReconcileView = wmsBP.fkReconcileView(wmsTO);
            request.setAttribute("fkReconcileView", fkReconcileView);
            if ("ExportExcel".equals(param)) {
                path = "content/wms/fkReconcileViewExcel.jsp";
            } else {
                path = "content/wms/fkReconcileView.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView fkOrderUploadView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            String orderType = request.getParameter("orderType");
            request.setAttribute("orderType", orderType);
            wmsTO.setOrderType(orderType);
            wmsTO.setUserId(userId + "");
            ArrayList fkOrderUploadView = wmsBP.fkOrderUploadView(wmsTO);
            request.setAttribute("fkOrderUploadView", fkOrderUploadView);
            path = "content/wms/fkOrderUploadView.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView cycleCountAssign(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            int status = 0;
            String param = request.getParameter("param");
            wmsTO.setUserId(userId + "");

            ArrayList whList = new ArrayList();
            whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);

            ArrayList custList = new ArrayList();
            custList = wmsBP.getCustList(wmsTO);
            request.setAttribute("custList", custList);
            ArrayList empList = wmsBP.getHubManagerList(wmsTO);
            request.setAttribute("empList", empList);
            if ("search".equals(param)) {
                String whId = request.getParameter("whId");
                String custId = request.getParameter("custId");
                String empId = request.getParameter("empId");
                String weekDay = request.getParameter("weekDay");
                wmsTO.setCustId(custId);
                wmsTO.setWhId(whId);
                wmsTO.setEmpId(empId);
                wmsTO.setDate(weekDay);
                request.setAttribute("custId", custId);
                request.setAttribute("empId", empId);
                request.setAttribute("weekDay", weekDay);
                ArrayList getProductType = wmsBP.getProductTypeList(wmsTO);
                request.setAttribute("productTypeList", getProductType);
                ArrayList getSelectedProductType = wmsBP.getSelectedProductType(wmsTO);
                request.setAttribute("selectedProductType", getSelectedProductType);
                path = "content/wms/cycleCountAssign.jsp";
            } else if ("save".equals(param)) {
                String custId = request.getParameter("custId");
                String empId = request.getParameter("empId");
                String weekDay = request.getParameter("weekDay");
                String[] productType = request.getParameterValues("productType");
                String[] productTypeIds = request.getParameterValues("productTypeId");
                String[] selected = request.getParameterValues("selectedStatus");
                String[] cycleIds = request.getParameterValues("cycleIds");
                String[] activeInds = request.getParameterValues("activeInds");
                request.setAttribute("custId", custId);
                request.setAttribute("empId", empId);
                request.setAttribute("weekDay", weekDay);
                wmsTO.setCustId(custId);
                wmsTO.setEmpId(empId);
                wmsTO.setDate(weekDay);
                if (cycleIds != null) {
                    if (cycleIds.length > 0) {
                        status = wmsBP.updateCycleCountAssign(cycleIds, activeInds);
                    }
                }
                if (productType != null) {
                    if (productType.length > 0) {
                        status = wmsBP.saveCycleCountAssign(empId, custId, weekDay, productType, productTypeIds, selected);
                    }
                }
                ArrayList getProductType = wmsBP.getProductTypeList(wmsTO);
                request.setAttribute("productTypeList", getProductType);
                ArrayList getSelectedProductType = wmsBP.getSelectedProductType(wmsTO);
                request.setAttribute("selectedProductType", getSelectedProductType);
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Cycle Count Assigned Successfully.");
                }
                path = "content/wms/cycleCountAssign.jsp";
            } else {
                path = "content/wms/cycleCountAssign.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView connectDispatchPlanning(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        try {
            wmsTO.setWhId(hubId);
            int updateconnectInvoice = 0;
            String param = request.getParameter("param");
            String plannedDate = request.getParameter("plannedDate");
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String newDate = dateFormat.format(curDate);
            if (plannedDate == null || "".equals(plannedDate)) {
                plannedDate = newDate;
            }
            request.setAttribute("plannedDate", plannedDate);
            String supplierId = request.getParameter("suppId");
            request.setAttribute("suppId", supplierId);
            System.out.println("output====" + supplierId);
            wmsTO.setSupplierId(supplierId);
            if ("save".equals(param)) {
                wmsTO.setUserId(userId + "");
//                String whId = request.getParameter("whId");
                String vendorId = request.getParameter("vendorId");
                String[] suppId = request.getParameterValues("suplierIds");
                String vehicleTypeId = request.getParameter("vehicleTypeId");
                String[] customerIdE = request.getParameterValues("customerids");
                String[] qtyE = request.getParameterValues("qtys");
                String[] invoiceNoE = request.getParameterValues("invoiceNumbers");
                String[] orderIdE = request.getParameterValues("orderids");
                String[] itemIdE = request.getParameterValues("itemids");

                String[] invoiceAmt = request.getParameterValues("invoiceValue");
                String[] preTaxValue = request.getParameterValues("perTaxValue");
                String[] taxvalue = request.getParameterValues("taxValue");
                String supplierIdNew = request.getParameter("supplierIdNew");
                String totalInvoice = request.getParameter("totalInvoice");
                String totalTax = request.getParameter("totalTax");
                String totalPerTax = request.getParameter("totalPerTax");
                String totalQty = request.getParameter("totalQty");
                String transferType = request.getParameter("transferType");
//                String TranshipmentStatus = request.getParameter("TranshipmentStatus");
                String TranshipmentWhId = request.getParameter("TranshipmentWhId");
                System.out.println("hhhhhhhhhhhhhh" + TranshipmentWhId);

                wmsTO.setTransferType(transferType);
                wmsTO.setTotalQty(totalQty);
                wmsTO.setPertaxvalue(totalPerTax);
                wmsTO.setTaxvalue(totalTax);

                wmsTO.setInvoiceAmounts(totalInvoice);
                wmsTO.setDisinvoiceAmt(invoiceAmt);
                wmsTO.setDisPerTax(preTaxValue);
                wmsTO.setDisTaxvalue(taxvalue);

                wmsTO.setCustomerIdE(customerIdE);
                wmsTO.setQtyE(qtyE);
                wmsTO.setInvoiceNoE(invoiceNoE);
                wmsTO.setOrderIdE(orderIdE);
//                wmsTO.setSupplyId(suppId);
                wmsTO.setItemsides(itemIdE);
                wmsTO.setSupplyId(suppId);
                wmsTO.setSupplierId(supplierIdNew);
                wmsTO.setVendorId(vendorId);
                wmsTO.setVehicleTypeId(vehicleTypeId);
                wmsTO.setTranshipmentWhId(TranshipmentWhId);

                wmsTO.setPlannedDate(plannedDate);
                updateconnectInvoice = wmsBP.insertDispatchDetails(wmsTO);
                if (updateconnectInvoice > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Dispatch Details Inserted Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Dispatch Details Update Failed");
                }
            }
            ArrayList transferTypeList = new ArrayList();
            transferTypeList = wmsBP.transferTypeList(wmsTO);
            request.setAttribute("transferTypeList", transferTypeList);

            ArrayList whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);

            ArrayList getCustInvoiceDetails = new ArrayList();
            getCustInvoiceDetails = wmsBP.getCustInvoiceDetails(wmsTO);
            System.out.println("getCustInvoiceDetails=====" + getCustInvoiceDetails.size());
            request.setAttribute("getCustInvoiceDetails", getCustInvoiceDetails);

            ArrayList getSupplierList = new ArrayList();
            getSupplierList = wmsBP.getSupplierList();
            request.setAttribute("getSupplierList", getSupplierList);

            ArrayList vendorList = new ArrayList();
            vendorList = wmsBP.getConnectVendorList(wmsTO);
            System.out.println("ControllerSize=====" + vendorList.size());
            request.setAttribute("vendorList", vendorList);

            ArrayList getVehicleTypeList = new ArrayList();
            getVehicleTypeList = wmsBP.getVehicleTypeList();
            System.out.println("getVehicleTypeList=====" + getVehicleTypeList.size());
            request.setAttribute("getVehicleTypeList", getVehicleTypeList);

            path = "content/wms/addDispatchpage.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView connectUploadInvoice(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "connectUploadInvoice";
        menuPath = "wms >>  connectUploadInvoice";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        try {
            String param = request.getParameter("param");
            String supplyId = request.getParameter("supplyId");
            System.out.println("supplyIdoutput" + supplyId);
            ArrayList getSupplierList = new ArrayList();
//            getSupplierList = wmsBP.getSupplierList();
            getSupplierList = wmsBP.getConnectInvoiceExcelTemplate();
            request.setAttribute("getSupplierList", getSupplierList);
            if ("save".equals(param)) {
                wmsTO.setWhId(whId);
                System.out.println("whId    " + whId);
                int connectUploadInvoice = wmsBP.connectUploadInvoice(wmsTO, userId, whId, supplyId);

                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Invoice Updated Successfully");
                int clearData = wmsBP.deleteInvoiceConnectTemp(userId);
                path = "content/wms/connectInvoiceMaster.jsp";
            } else {
                int clearData = wmsBP.deleteInvoiceConnectTemp(userId);
                path = "content/wms/connectInvoiceMaster.jsp";
            }
//            System.out.println("status11111111111111111111111111===="+returnOrderUpload);
//            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "shipment Uploaded Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView uploadconnectInvoice(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
//        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = 0;
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        String supplyId = request.getParameter("supplyId");
        String supplierId = "";
        request.setAttribute("supplyId", supplyId);
//        common rows
        String plantCode = "";
        String shipToPartycode = "";
        String shipToPartyName = "";
        String invoiceDate = "";
        String invoiceNo = "";
        String materialCode = "";
        String materialDescription = "";
        String billedQuantity = "";
        String basicPrice = "";
        String mRP = "";
        String pretaxValue = "";
        String taxValue = "";
        String invoiceAmount = "";
        String invoiceID = "";
        String purchaseOrderNo = "";
        String purchaseOrderDate = "";
        String saleOrderNo = "";
        String saleOrderDate = "";
        String currency = "";
        String hSNCode = "";
        String cGSTAmount = "";
        String sGSTAmount = "";
        String iGSTAmount = "";
        String deliveryNumber = "";
        String matGrptext = "";
        String discountAmount = "";
        String vechileRegNo = "";
        String invoiceTime = "";

        String cellplantCode = "";
        String cellshipToPartycode = "";
        String cellshipToPartyName = "";
        String cellinvoiceDate = "";
        String cellinvoiceNo = "";
        String cellmaterialCode = "";
        String cellmaterialDescription = "";
        String cellbilledQuantity = "";
        String cellbasicPrice = "";
        String cellmRP = "";
        String cellpretaxValue = "";
        String cellInvoiceTime = "";
        String celltaxValue = "";
        String cellinvoiceAmount = "";
        String cellinvoiceID = "";
        String cellpurchaseOrderNo = "";
        String cellpurchaseOrderDate = "";
        String cellsaleOrderNo = "";
        String cellsaleOrderDate = "";
        String cellcurrency = "";
        String cellhSNCode = "";
        String cellcGSTAmount = "";
        String cellsGSTAmount = "";
        String celliGSTAmount = "";
        String celldeliveryNumber = "";
        String cellmatGrptext = "";
        String celldiscountAmount = "";
        String cellvechileRegNo = "";

//        common rows end
//        int status = 0;
        int status1 = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            String hubId = (String) session.getAttribute("hubId");
            userId = (Integer) session.getAttribute("userId");
            int clearData = wmsBP.deleteInvoiceConnectTemp(userId);
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

//            int deleteUploadTemp = wBP.deleteUploadTemp();
//            System.out.println("deleteUploadTemp----"+deleteUploadTemp);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadedxls/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        fileFormat = splitFileNames[1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                                System.out.println("splitFileName[0]=" + splitFileName[0]);
                                System.out.println("splitFileName[0]=" + splitFileName[1]);

                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                System.out.println("tempPath..." + tempFilePath);
                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath));
                                System.out.println("tempPath = " + tempFilePath);
//                                System.out.println("actPath = " + actualFilePath);
//                                String parts = actualFilePath.substring(actualFilePath.lastIndexOf("//"));
//                                String part1 = parts.replace("//", "");
                                System.out.println("11111111111111111111111111");

                                int count = 0;
                                WorkbookSettings ws = new WorkbookSettings();
                                ws.setLocale(new Locale("en", "EN"));
                                Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                Sheet s = workbook.getSheet(0);
                                System.out.println("rows" + userId + s.getRows());

                                ArrayList getCellList = new ArrayList();
                                getCellList = wmsBP.getCellList(supplyId);
                                System.out.println("getCellList" + getCellList.size());

                                WmsTO wmsTO1 = new WmsTO();
                                if (getCellList.size() > 0) {
                                    Iterator itr = getCellList.iterator();
                                    while (itr.hasNext()) {
                                        wmsTO1 = (WmsTO) itr.next();
                                        supplierId = wmsTO1.getSupplierId();
                                        cellplantCode = wmsTO1.getPlantCode();
                                        cellshipToPartycode = wmsTO1.getShipToPartycode();
                                        cellshipToPartyName = wmsTO1.getShipToPartyName();
                                        cellinvoiceDate = wmsTO1.getInvoiceDate();
                                        cellinvoiceNo = wmsTO1.getInvoiceNumber();
                                        cellInvoiceTime = wmsTO1.getInvoiceTime();
                                        cellmaterialCode = wmsTO1.getMaterialCode();
                                        cellmaterialDescription = wmsTO1.getMaterialDescription();
                                        cellbilledQuantity = wmsTO1.getBilledQuantity();
                                        cellbasicPrice = wmsTO1.getBasicPrice();
                                        cellmRP = wmsTO1.getmRP();
                                        cellpretaxValue = wmsTO1.getPretaxValue();
                                        celltaxValue = wmsTO1.getTaxValue();
                                        System.out.println("celltaxValue" + celltaxValue);
                                        cellinvoiceAmount = wmsTO1.getInvoiceAmount();
                                        cellinvoiceID = wmsTO1.getPlantCode();
                                        cellpurchaseOrderNo = wmsTO1.getPurchaseOrderNo();
                                        cellpurchaseOrderDate = wmsTO1.getPurchaseOrderDate();
                                        cellsaleOrderNo = wmsTO1.getSaleOrderNo();
                                        System.out.println("cellsaleOrderNo===" + cellsaleOrderNo);
                                        cellsaleOrderDate = wmsTO1.getSaleOrderDate();
                                        cellcurrency = wmsTO1.getCurrency();
                                        cellhSNCode = wmsTO1.gethSNCode();
                                        cellcGSTAmount = wmsTO1.getcGSTAmount();
                                        cellsGSTAmount = wmsTO1.getsGSTAmount();
                                        celliGSTAmount = wmsTO1.getiGSTAmount();
                                        celldeliveryNumber = wmsTO1.getDeliveryNumber();
                                        cellmatGrptext = wmsTO1.getMatGrptext();
                                        celldiscountAmount = wmsTO1.getDiscountAmount();
//                                        cellvechileRegNo = Integer.parseInt(wmsTO1.getPlantCode());

                                    }
                                }

                                for (int i = 1; i < s.getRows(); i++) {
                                    System.out.println("11111111111111111111");
                                    WmsTO wmsTO = new WmsTO();
                                    count++;

                                    wmsTO.setUserId(userId + "");

                                    if (!"".equals(cellplantCode) && cellplantCode != null) {
                                        plantCode = s.getCell(Integer.parseInt(cellplantCode), i).getContents();
                                        System.out.println("AAAAAAAAAAAAAAAA " + plantCode);
                                        wmsTO.setPlantCode(plantCode);
                                    }
                                    if (!"".equals(cellshipToPartycode) && cellshipToPartycode != null) {
                                        shipToPartycode = s.getCell(Integer.parseInt(cellshipToPartycode), i).getContents();
                                        wmsTO.setShipToPartycode(shipToPartycode);
                                    }
                                    if (!"".equals(cellshipToPartyName) && cellshipToPartyName != null) {
                                        shipToPartyName = s.getCell(Integer.parseInt(cellshipToPartyName), i).getContents();
                                        wmsTO.setShipToPartyName(shipToPartyName);
                                    }
                                    if (!"".equals(cellinvoiceDate) && cellinvoiceDate != null) {
                                        invoiceDate = s.getCell(Integer.parseInt(cellinvoiceDate), i).getContents();
                                        wmsTO.setInvoiceDate(invoiceDate);
                                    }
                                    if (!"".equals(cellinvoiceNo) && cellinvoiceNo != null) {
                                        invoiceNo = s.getCell(Integer.parseInt(cellinvoiceNo), i).getContents();
                                        wmsTO.setInvoiceNo(invoiceNo);
                                    }
                                    if (!"".equals(cellmaterialCode) && cellmaterialCode != null) {
                                        materialCode = s.getCell(Integer.parseInt(cellmaterialCode), i).getContents();
                                        wmsTO.setMaterialCode(materialCode);
                                    }
                                    if (!"".equals(cellmaterialDescription) && cellmaterialDescription != null) {
                                        materialDescription = s.getCell(Integer.parseInt(cellmaterialDescription), i).getContents();
                                        wmsTO.setMaterialDescription(materialDescription);
                                    }
                                    if (!"".equals(cellbilledQuantity) && cellbilledQuantity != null) {
                                        billedQuantity = s.getCell(Integer.parseInt(cellbilledQuantity), i).getContents();
                                        wmsTO.setBilledQuantity(billedQuantity);
                                    }
                                    System.out.println("cellbasicPrice=1=" + cellbasicPrice);
                                    if (!"".equals(cellbasicPrice) && (cellbasicPrice != null)) {
                                        System.out.println("cellbasicPrice=2=" + cellbasicPrice + "sdsdfdf");
                                        basicPrice = s.getCell(Integer.parseInt(cellbasicPrice), i).getContents();
                                        wmsTO.setBasicPrice(basicPrice);
                                    }
                                    if (!"".equals(cellmRP) && cellmRP != null) {
                                        mRP = s.getCell(Integer.parseInt(cellmRP), i).getContents();
                                        wmsTO.setmRP(mRP);
                                    }
                                    if (!"".equals(cellpretaxValue) && cellpretaxValue != null) {
                                        pretaxValue = s.getCell(Integer.parseInt(cellpretaxValue), i).getContents();
                                        wmsTO.setPretaxValue(pretaxValue);
                                    }
                                    if (!"".equals(celltaxValue) && celltaxValue != null) {
                                        taxValue = s.getCell(Integer.parseInt(celltaxValue), i).getContents();
                                        wmsTO.setTaxValue(taxValue);
                                    }
                                    if (!"".equals(cellinvoiceAmount) && cellinvoiceAmount != null) {
                                        invoiceAmount = s.getCell(Integer.parseInt(cellinvoiceAmount), i).getContents();
                                        wmsTO.setInvoiceAmount(invoiceAmount.replace(",", ""));
                                    }
                                    if (!"".equals(cellpurchaseOrderNo) && cellpurchaseOrderNo != null) {
                                        purchaseOrderNo = s.getCell(Integer.parseInt(cellpurchaseOrderNo), i).getContents();
                                        wmsTO.setPurchaseOrderNo(purchaseOrderNo);
                                    }
                                    if (!"".equals(cellpurchaseOrderDate) && cellpurchaseOrderDate != null) {
                                        purchaseOrderDate = s.getCell(Integer.parseInt(cellpurchaseOrderDate), i).getContents();
                                        wmsTO.setPurchaseOrderDate(purchaseOrderDate);
                                    }
//                                    vechileRegNo = s.getCell(cellvechileRegNo, i).getContents();
//                                    wmsTO.setVechileRegNo(vechileRegNo);
                                    if (!"".equals(cellsaleOrderDate) && cellsaleOrderDate != null) {
                                        saleOrderDate = s.getCell(Integer.parseInt(cellsaleOrderDate), i).getContents();
                                        wmsTO.setSaleOrderDate(saleOrderDate);
                                    }
                                    if (!"".equals(cellsaleOrderNo) && cellsaleOrderNo != null) {
                                        saleOrderNo = s.getCell(Integer.parseInt(cellsaleOrderNo), i).getContents();
                                        wmsTO.setSaleOrderNo(saleOrderNo);
                                    }
                                    if (!"".equals(cellcurrency) && cellcurrency != null) {
                                        currency = s.getCell(Integer.parseInt(cellcurrency), i).getContents();
                                        wmsTO.setCurrency(currency);
                                    }
                                    if (!"".equals(cellhSNCode) && cellhSNCode != null) {
                                        hSNCode = s.getCell(Integer.parseInt(cellhSNCode), i).getContents();
                                        wmsTO.sethSNCode(hSNCode);
                                    }
                                    if (!"".equals(cellcGSTAmount) && cellcGSTAmount != null) {
                                        cGSTAmount = s.getCell(Integer.parseInt(cellcGSTAmount), i).getContents();
                                        wmsTO.setcGSTAmount(cGSTAmount);
                                    }
                                    if (!"".equals(cellsGSTAmount) && cellsGSTAmount != null) {
                                        sGSTAmount = s.getCell(Integer.parseInt(cellsGSTAmount), i).getContents();
                                        wmsTO.setsGSTAmount(sGSTAmount);
                                    }
                                    if (!"".equals(celldiscountAmount) && celldiscountAmount != null) {
                                        discountAmount = s.getCell(Integer.parseInt(celldiscountAmount), i).getContents();
                                        wmsTO.setDiscountAmount(discountAmount);
                                    }
                                    if (!"".equals(cellInvoiceTime) && cellInvoiceTime != null) {
                                        invoiceTime = s.getCell(Integer.parseInt(cellInvoiceTime), i).getContents();
                                        wmsTO.setInvoiceTime(invoiceTime);
                                    }
                                    wmsTO.setWhId(hubId);
                                    wmsTO.setSupplierId(supplierId);
                                    System.out.println("supplierId" + supplierId);
                                    status1 = wmsBP.uploadconnectInvoice(wmsTO);
                                    System.out.println("status====" + status1);

                                }

                                System.out.println("celltaxValue-=====");
                                System.out.println("celltaxValue-=====" + celltaxValue);

                            }

                        } else {
                            request.setAttribute("errorMessage", "Invoice Master updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            wms.setUserId(userId + "");
            ArrayList getIFBList = new ArrayList();
            getIFBList = wmsBP.getconnectInvoice(wms);
            request.setAttribute("getContractList", getIFBList);

            ArrayList getSupplierList = new ArrayList();
            getSupplierList = wmsBP.getSupplierList();
            request.setAttribute("getSupplierList", getSupplierList);

            path = "content/wms/connectInvoiceMaster.jsp";

            if (getIFBList.size() > 0) {
                System.out.println("getContractList====check================status" + getIFBList);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Updated Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView viewDcGenerateDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";

        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        OpsTO opsTO = new OpsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        try {
            String dispatchId = request.getParameter("dispatchId");
            request.setAttribute("dispatchId", dispatchId);
            wmsTO.setUserId(userId + "");
            wmsTO.setWhId(whId);
            opsTO.setUserId(userId);
            opsTO.setWhId(whId);
            wmsTO.setTatType("1");
            String param = request.getParameter("param");
            if ("update".equals(param)) {
                String[] serialId = request.getParameterValues("serialIds");
                String[] customerId = request.getParameterValues("customerIds");
                String[] invoiceId = request.getParameterValues("invoiceIds");

                wmsTO.setInvoiceIds(invoiceId);
                wmsTO.setCustomerIdE(customerId);
                wmsTO.setSerialIds(serialId);

                int updatePicklist = wmsBP.updatePicklist(wmsTO);

            }
            wmsTO.setDispatchId(dispatchId);
            int clearConnectPicklist = wmsBP.clearConnectPicklist(wmsTO);
            ArrayList getdispatchList = new ArrayList();
            getdispatchList = wmsBP.getdispatchList(wmsTO);
            request.setAttribute("getdispatchList", getdispatchList);

            ArrayList getVechicleDetails = new ArrayList();
            getVechicleDetails = wmsBP.getVechicleDetails(wmsTO);
            request.setAttribute("getVechicleDetails", getVechicleDetails);

            ArrayList getdispatch = new ArrayList();
            getdispatch = wmsBP.getdispatch(wmsTO);
            request.setAttribute("getdispatch", getdispatch);

            ArrayList getItemQty = new ArrayList();
            getItemQty = wmsBP.getItemQty(wmsTO);
            request.setAttribute("getItemQty", getItemQty);

            ArrayList getDeviceIdList = opsBP.getDeviceIdList(opsTO);
            request.setAttribute("deviceList", getDeviceIdList);

            path = "content/wms/viewdcgenerate.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewDcGenerate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "DC Generate";
        menuPath = "wms >>  DC Generate";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        try {
            wmsTO.setWhId(whId);
            String param = request.getParameter("param");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            wmsTO.setStatus("1");
            ArrayList getDispatchDetails = new ArrayList();
            getDispatchDetails = wmsBP.getDispatchDetails(wmsTO);
            request.setAttribute("getDispatchDetails", getDispatchDetails);

            if ("excel".equals(param)) {
                path = "content/wms/dcGenerateExcel.jsp";
            } else {
                path = "content/wms/handelViewDcGenerate.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView saveDcGenerate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "DC Generate";
        menuPath = "wms >>  DC Generate";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        int updateDcGenerate = 0;
        try {
            wmsTO.setUserId(userId + "");
            String loadingEnd = request.getParameter("loadingEnd");
            String loadingEndTime = request.getParameter("loadingEndTime");
            String loadingStart = request.getParameter("loadingStart");
            String loadingStartTime = request.getParameter("loadingStartTime");
            String vehicleNumber = request.getParameter("vehicleNumber");
            String Tansferid = request.getParameter("Tansferid");
            String vendorId = request.getParameter("vendorId");
            String dispatchid = request.getParameter("dispatchid");
            String dispatchNo = request.getParameter("dispatchNo");
            String supervisorName = request.getParameter("supervisorName");
            String stockType = request.getParameter("stockType");
            String remarks = request.getParameter("remarks");
            String boxQty = request.getParameter("noOfBoxes");
            String supplierId = request.getParameter("supplierId");
            String deviceId = request.getParameter("deviceId");
            String vehicleNo = request.getParameter("vehicleNo");

            String picklistIds = request.getParameter("picklistIds");
            String[] invoiceId = request.getParameterValues("invoiceId");
            String[] invoiceDetailId = request.getParameterValues("invoiceDetailId");
            String[] customerId = request.getParameterValues("customerId");
            String[] quantity = request.getParameterValues("quantity");
            String[] pendingReason = request.getParameterValues("pendingReason");
            String[] dcQty = request.getParameterValues("dcQty");
            String[] pendingQty = request.getParameterValues("pendingQty");
            String[] itemIds = request.getParameterValues("itemId");
            if ("0".equals(stockType)) {
                String[] serialNoNew = request.getParameterValues("xSerialNo");
                String[] qtyNew = request.getParameterValues("xQty");
                String[] uomNew = request.getParameterValues("xUom");
                String[] productIdNew = request.getParameterValues("xProductId");
                String[] custIdNew = request.getParameterValues("xCustId");
                String[] invoiceNoNew = request.getParameterValues("xInvoiceNo");
                String[] invoiceIdNew = request.getParameterValues("xInvoiceId");
                String[] invoiceDetailIdNew = request.getParameterValues("xInvoiceDetailId");

                wmsTO.setSerialNos(serialNoNew);
                wmsTO.setUom(uomNew);
                wmsTO.setQtyE(qtyNew);
                wmsTO.setProductIds(productIdNew);
                wmsTO.setCustomerIdE(custIdNew);
                wmsTO.setInvoiceNoE(invoiceNoNew);
                wmsTO.setInvoiceIdE(invoiceIdNew);
                wmsTO.setInvoiceDetailIdE(invoiceDetailIdNew);
            }
            wmsTO.setItemIds(itemIds);
            wmsTO.setSupplierId(supplierId);
            wmsTO.setStockType(stockType);
            wmsTO.setSupervisorName(supervisorName);
            wmsTO.setPicklistIds(picklistIds);
            wmsTO.setInvoiceIds(invoiceId);
            wmsTO.setInvoiceDetailIds(invoiceDetailId);
            wmsTO.setLoadingStart(loadingStart);
            wmsTO.setLoadingStartTime(loadingStartTime);
            wmsTO.setLoadingEnd(loadingEnd);
            wmsTO.setLoadingEndTime(loadingEndTime);
            wmsTO.setPendingReasons(pendingReason);
            wmsTO.setTansferid(Tansferid);
            wmsTO.setDispatchid(dispatchid);
            wmsTO.setCustomerIds(customerId);
            wmsTO.setQuantitys(quantity);
            wmsTO.setDcQty(dcQty);
            wmsTO.setDispatchNo(dispatchNo);
            wmsTO.setVehicleNumber(vehicleNumber);
            wmsTO.setVehicleNo(vehicleNo);
            wmsTO.setVendorId(vendorId);
            wmsTO.setPendingQty(pendingQty);
            wmsTO.setRemarks(remarks);
            wmsTO.setBoxQty(boxQty);
            wmsTO.setDeviceId(deviceId);

            wmsTO.setWhId(whId);
            updateDcGenerate = wmsBP.updateDcGenerate(wmsTO);
            if (updateDcGenerate > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Dispatch Details Inserted Successfully");
            } else {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "DC Generate Failed");
            }

            ArrayList getconnectlrnumber = new ArrayList();
            getconnectlrnumber = wmsBP.getconnectlrnumber(dispatchid);
            request.setAttribute("getconnectlrnumber", getconnectlrnumber);
            ArrayList getconnectsublrnumber = new ArrayList();
            getconnectsublrnumber = wmsBP.getconnectsublrnumber(dispatchid);
            request.setAttribute("getconnectsublrnumber", getconnectsublrnumber);

            path = "content/wms/connectLrprint.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView connectLrPrint(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        int updateDcGenerate = 0;
        try {

            String dispatchid = request.getParameter("dispatchid");

            ArrayList getconnectlrnumber = new ArrayList();
            getconnectlrnumber = wmsBP.getconnectlrnumber(dispatchid);
            request.setAttribute("getconnectlrnumber", getconnectlrnumber);
            ArrayList getconnectsublrnumber = new ArrayList();
            getconnectsublrnumber = wmsBP.getconnectsublrnumber(dispatchid);
            request.setAttribute("getconnectsublrnumber", getconnectsublrnumber);

            path = "content/wms/connectLrprint.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView connectLrView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        int roleId = (Integer) session.getAttribute("RoleId");
        System.out.println("whId    " + whId);
        int updateDcGenerate = 0;
        try {
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            wmsTO.setFromDate(fromDate);
            wmsTO.setWhId(whId);
            wmsTO.setRoleId(roleId + "");
            wmsTO.setToDate(toDate);
            wmsTO.setStatus("2");
            wmsTO.setType("5");
            ArrayList getDispatchDetails = new ArrayList();
            getDispatchDetails = wmsBP.getDispatchDetails(wmsTO);
            request.setAttribute("getDispatchDetails", getDispatchDetails);

            ArrayList getWareHouse = new ArrayList();
            getWareHouse = wmsBP.getWareHouse(wmsTO);
            request.setAttribute("getWareHouse", getWareHouse);
            String param = request.getParameter("param");

            if ("Export".equals(param)) {
                path = "content/wms/connectLrViewExcel.jsp";
            } else {
                path = "content/wms/connectLrView.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView handlegrnUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "GrnUpload";
        menuPath = "wms >>  GrnUpload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        try {
            String param = request.getParameter("param");

            System.out.println("supplyIdoutput" + param);
            if ("save".equals(param)) {
                wmsTO.setWhId(whId);
                String tatId = request.getParameter("tatId");
                wmsTO.setTatId(tatId);
                System.out.println("whId    " + whId);
                System.out.println("whId===" + tatId);
                int connectUploadInvoice = wmsBP.connectGrnUpload(wmsTO, userId, whId);

                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "GRN Updated Successfully");
                int clearData = wmsBP.deleteConnectgrnTemp(userId);
                path = "content/wms/uploadGrn.jsp";
            } else {
                System.out.println("enter");
                int clearData = wmsBP.deleteConnectgrnTemp(userId);
                path = "content/wms/uploadGrn.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView connectStoragemaster(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "Storage Mater";
        menuPath = "wms >>  Storage Mater";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        try {
            String param = request.getParameter("param");
            String supplyId = request.getParameter("supplyID");
            System.out.println("supplyIdoutput" + supplyId);
            if ("save".equals(param)) {
                wmsTO.setWhId(whId);

                String storageId = request.getParameter("storageId");
                String binId = request.getParameter("binId");
                String rackId = request.getParameter("rackId");
                String status = request.getParameter("statusid");
                String storageWhId = request.getParameter("storageWareHouseId");
                wmsTO.setBinId(binId);
                wmsTO.setRackId(rackId);
                wmsTO.setStorageId(storageId);
                wmsTO.setStatus(status);
                wmsTO.setStorageWhid(storageWhId);

                int insertStoragewareHouse = wmsBP.insertStoragewareHouse(wmsTO);
                if (insertStoragewareHouse == 3) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Inserted Successfully");

                }
                if (insertStoragewareHouse == 2) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "updated Successfully");

                }

                if (insertStoragewareHouse == 1) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Master Already Exist");

                }

            }
            ArrayList getStorageWareHouse = new ArrayList();
            getStorageWareHouse = wmsBP.getStorageWareHouse();
            request.setAttribute("getStorageWareHouse", getStorageWareHouse);

            path = "content/wms/viewStoragemaster.jsp";
            ArrayList getStorageDetails = new ArrayList();
            getStorageDetails = wmsBP.getStorageDetails();
            request.setAttribute("getStorageDetails", getStorageDetails);
            ArrayList getbinDetails = new ArrayList();
            getbinDetails = wmsBP.getbinDetails();
            request.setAttribute("getbinDetails", getbinDetails);
            ArrayList getrackDetails = new ArrayList();
            getrackDetails = wmsBP.getrackDetails();
            request.setAttribute("getrackDetails", getrackDetails);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView viewSerialNumberUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = ">>  View ";

        String pageTitle = "serialNumberUpdate";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "serialNumberUpdate";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList getReceivedgrn = new ArrayList();
            getReceivedgrn = wmsBP.getReceivedgrn(userId);
            request.setAttribute("getReceivedgrn", getReceivedgrn);
            path = "content/wms/receviedgrn.jsp";
            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView editConnectSerialNumber(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Edit GRN  >>  Edit ";
        String pageTitle = "EditGRN";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "EditGRN";
        String whId = (String) session.getAttribute("hubId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String itemId = "";

            String user = "" + userId;
            wmsTo.setUserId(user);
            wmsTo.setWhId(whId);
            ArrayList getGrnDetails = new ArrayList();
            ArrayList getBinDetails = new ArrayList();
            String param = request.getParameter("param");
            String grnId = request.getParameter("grnId");
            request.setAttribute("grnId", grnId);
            wmsTo.setGrnId1(grnId);
            itemId = request.getParameter("itemId");
            wmsTo.setProductID(itemId);
            request.setAttribute("itemId", itemId);

            wmsTo.setGrnId(grnId);
            if ("remove".equals(param)) {

                System.out.println("removee=======");
                int delGrnTempSerial = 0;

                String serialNo = request.getParameter("serialid");

                if (serialNo != "" && serialNo != null) {
                    delGrnTempSerial = wmsBP.delConnectSerial(serialNo, grnId, itemId);
                }
                path = "content/wms/editconnectSerial.jsp";
            } else if ("save".equals(param)) {
                String scannable = request.getParameter("scanStatus");
                if ("Y".equalsIgnoreCase(scannable)) {
                    wmsTo.setSerialNos(request.getParameterValues("serialNos"));
                    wmsTo.setUom(request.getParameterValues("uom"));
                    wmsTo.setProCond(request.getParameterValues("proCond"));
                    wmsTo.setIpQty(request.getParameterValues("ipQty"));

                    wmsTo.setQty(request.getParameter("poQuantity"));
                    wmsTo.setBinIds(request.getParameterValues("bins"));
                    wmsTo.setRackIdE(request.getParameterValues("racks"));
                    wmsTo.setLocatioId(request.getParameterValues("stoLocation"));
                    wmsTo.setWhId(whId);
                    int connectSerialno = wmsBP.inserconnectSerialNumber(wmsTo);
                    if (connectSerialno >= 0) {
                        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Saved Successfully");
                    } else {
                        request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Failed to Save");
                    }

                    ArrayList getReceivedgrn = new ArrayList();
                    getReceivedgrn = wmsBP.getReceivedgrn(userId);
                    request.setAttribute("getReceivedgrn", getReceivedgrn);
                    path = "content/wms/receviedgrn.jsp";
                } else if ("N".equalsIgnoreCase(scannable)) {
                    String productId = request.getParameter("productId");
                    String uom = request.getParameter("Uom");
                    String storageId = request.getParameter("storageId");
                    String rackId = request.getParameter("rackId");
                    String binId = request.getParameter("binId");
                    String quantity = request.getParameter("quantity");
                    wmsTo.setProductId(productId);
                    int updateSerialNo = wmsBP.insertNonScanSerialNumber(uom, storageId, rackId, binId, quantity, wmsTo);
                    if (updateSerialNo >= 0) {
                        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Saved Successfully");
                    } else {
                        request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Failed to Save");
                    }

                    ArrayList getReceivedgrn = new ArrayList();
                    getReceivedgrn = wmsBP.getReceivedgrn(userId);
                    request.setAttribute("getReceivedgrn", getReceivedgrn);
                    path = "content/wms/receviedgrn.jsp";
                }
            } else if ("edit".equals(param)) {
                wmsTo.setSerialId(request.getParameter("serialId"));
                wmsTo.setUoms(request.getParameter("Uom"));
                wmsTo.setLocationId(request.getParameter("storageId"));
                wmsTo.setProCons(request.getParameter("proCon"));
                wmsTo.setBinIdTemp1(request.getParameter("binId"));
                wmsTo.setRackId(request.getParameter("rackId"));

                int connectSerialno = wmsBP.editconnectSerialNumber(wmsTo);
                path = "content/wms/editconnectSerial.jsp";
            } else {
                path = "content/wms/editconnectSerial.jsp";
            }

            getGrnDetails = wmsBP.getGrnDetails(wmsTo);
            request.setAttribute("getGrnDetails", getGrnDetails);
            ArrayList getGrnproductDetails = new ArrayList();
            getGrnproductDetails = wmsBP.getGrnproductDetails(wmsTo);
            request.setAttribute("getGrnproductDetails", getGrnproductDetails);

            ArrayList getStorageDetails = new ArrayList();
            getStorageDetails = wmsBP.getStorageDetails();
            request.setAttribute("getStorageDetails", getStorageDetails);
            ArrayList getbinDetails = new ArrayList();
            getbinDetails = wmsBP.getbinDetails();
            request.setAttribute("getbinDetails", getbinDetails);
            ArrayList getrackDetails = new ArrayList();
            getrackDetails = wmsBP.getrackDetails();
            request.setAttribute("getrackDetails", getrackDetails);
            ArrayList getConnectSerialNumberDetails = new ArrayList();
            getConnectSerialNumberDetails = wmsBP.getConnectSerialNumberDetails(wmsTo);
            request.setAttribute("getConnectSerialNumberDetails", getConnectSerialNumberDetails);
            request.setAttribute("size", getConnectSerialNumberDetails.size());

            path = "content/wms/editconnectSerial.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void checkconnectSerialNo(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            String SerialNo = request.getParameter("serialNo");
            String grnId = request.getParameter("grnId");
            System.out.println("SerialNo" + SerialNo);
            String checkconnectSerialNo = "";
            checkconnectSerialNo = wmsBP.checkconnectSerialNo(SerialNo, grnId);

            System.out.println("checkconnectSerialNo===" + checkconnectSerialNo);

            if ("0".equals(checkconnectSerialNo)) {
                existsStatus = "";
            } else if ("1".equals(checkconnectSerialNo)) {
                existsStatus = "Serial No Already Exists";
            } else {
                existsStatus = "Already Scanned Serial No";
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(existsStatus);
            writer.close();
            request.setAttribute("serialNoAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public void checkConnectSerialNoMaster(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            String SerialNo = request.getParameter("serialNo");
            String grnId = request.getParameter("grnId");
            System.out.println("SerialNo" + SerialNo);
            String checkconnectSerialNo = "";
            checkconnectSerialNo = wmsBP.checkconnectSerialNo(SerialNo, grnId);

            System.out.println("checkconnectSerialNo===" + checkconnectSerialNo);

            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(checkconnectSerialNo);
            writer.close();
            request.setAttribute("serialNoAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public ModelAndView connectUploadGrn(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
//        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = 0;
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        String supplyId = request.getParameter("supplyId");
        System.out.println("supplyIdsupplyId" + supplyId);
//        common rows
        String gateInwardDate = "";
        String gateInwardNo = "";
        String processType = "";
        String modeofTransport = "";
        String vehicleNo = "";
        String transporterCode = "";
        String transporterName = "";
        String SerialNo = "";
        String giQuantity = "";
        String uom = "";
        String billNum = "";
        String billDate = "";
        String matDocNo = "";
        String matDocDate = "";
        String matDocYear = "";
        String postingDate = "";
        String suppPlantCode = "";
        String suppPlantDesc = "";
        String recPlantCode = "";
        String recPlantDesc = "";
        String grQty = "";
        String storageLocation = "";
        String purchaseOrderNo = "";
        String deliveryNo = "";
        String movementType = "";
        String createdBy = "";
        String cancelledNo = "";
        String invoceNo = "";
        String invoiceDate = "";
        String materialCode = "";
        String materialDes = "";

//        common rows end
//        int status = 0;
        int status1 = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            String hubId = (String) session.getAttribute("hubId");
            userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadedxls/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        fileFormat = splitFileNames[1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = uploadedFileName;
                                System.out.println("splitFileName[0]=" + splitFileName[0]);
                                System.out.println("splitFileName[0]=" + splitFileName[1]);

                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                System.out.println("tempPath..." + tempFilePath);
                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath));
                                System.out.println("tempPath = " + tempFilePath);
//                                System.out.println("actPath = " + actualFilePath);
//                                String parts = actualFilePath.substring(actualFilePath.lastIndexOf("//"));
//                                String part1 = parts.replace("//", "");
                                System.out.println("11111111111111111111111111");
                                if ("xlsx".equals(fileFormat)) {
                                    FileInputStream fis = new FileInputStream(tempFilePath);
                                    XSSFWorkbook wb = new XSSFWorkbook(fis);
                                    XSSFSheet sheet = wb.getSheetAt(0);
                                    int value = sheet.getLastRowNum();
                                    System.out.println("value  " + value);

                                    int sno = 0;
                                    Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                    while (itr.hasNext()) {
                                        Row row = itr.next();
                                        if (sno > 0) {
                                            System.out.println("cellyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy " + row.getCell(1));
                                            WmsTO wmsTO = new WmsTO();

                                            wmsTO.setUserId(userId + "");

                                            if (row.getCell(0) != null && !"".equals(row.getCell(0))) {
                                                row.getCell(0).setCellType(row.getCell(0).CELL_TYPE_STRING);
                                                gateInwardDate = row.getCell(0).getStringCellValue();
                                                System.out.println("AAAAAAAAAAAAAAAA " + gateInwardDate);
                                                wmsTO.setGateInwardDate(gateInwardDate);
                                            }

                                            if (row.getCell(1) != null && !"".equals(row.getCell(1))) {
                                                row.getCell(1).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                gateInwardNo = row.getCell(1).getStringCellValue();
                                                wmsTO.setGateInwardNo(gateInwardNo);
                                            }

                                            if (row.getCell(2) != null && !"".equals(row.getCell(2))) {
                                                row.getCell(2).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                processType = row.getCell(2).getStringCellValue();
                                                wmsTO.setProcessType(processType);
                                            }

                                            if (row.getCell(3) != null && !"".equals(row.getCell(3))) {
                                                row.getCell(3).setCellType(row.getCell(3).CELL_TYPE_STRING);
                                                modeofTransport = row.getCell(3).getStringCellValue();
                                                wmsTO.setModeofTransport(modeofTransport);
                                            }

                                            if (row.getCell(4) != null && !"".equals(row.getCell(4))) {
                                                row.getCell(4).setCellType(row.getCell(4).CELL_TYPE_STRING);
                                                vehicleNo = row.getCell(4).getStringCellValue();
                                                wmsTO.setVehicleNo(vehicleNo);
                                            }

                                            if (row.getCell(5) != null && !"".equals(row.getCell(5))) {
                                                row.getCell(5).setCellType(row.getCell(5).CELL_TYPE_STRING);
                                                transporterCode = row.getCell(5).getStringCellValue();
                                                wmsTO.setTransporterCode(transporterCode);
                                            }

                                            if (row.getCell(6) != null && !"".equals(row.getCell(6))) {
                                                row.getCell(6).setCellType(row.getCell(6).CELL_TYPE_STRING);
                                                transporterName = row.getCell(6).getStringCellValue();
                                                wmsTO.setTransporterName(transporterName);
                                            }

                                            if (row.getCell(7) != null && !"".equals(row.getCell(7))) {
                                                row.getCell(7).setCellType(row.getCell(7).CELL_TYPE_STRING);
                                                SerialNo = row.getCell(7).getStringCellValue();
                                                wmsTO.setSerials(SerialNo);
                                            }

                                            if (row.getCell(8) != null && !"".equals(row.getCell(8))) {
                                                row.getCell(8).setCellType(row.getCell(8).CELL_TYPE_STRING);
                                                materialCode = row.getCell(8).getStringCellValue();
                                                wmsTO.setMaterialCode(materialCode);
                                            }

                                            if (row.getCell(9) != null && !"".equals(row.getCell(9))) {
                                                row.getCell(9).setCellType(row.getCell(9).CELL_TYPE_STRING);
                                                materialDes = row.getCell(9).getStringCellValue();
                                                wmsTO.setMaterialDescription(materialDes);
                                            }

                                            if (row.getCell(10) != null && !"".equals(row.getCell(10))) {
                                                row.getCell(10).setCellType(row.getCell(10).CELL_TYPE_STRING);
                                                giQuantity = row.getCell(10).getStringCellValue();
                                                wmsTO.setGiQuantity(giQuantity);
                                            }

                                            if (row.getCell(11) != null && !"".equals(row.getCell(11))) {
                                                row.getCell(11).setCellType(row.getCell(11).CELL_TYPE_STRING);
                                                uom = row.getCell(11).getStringCellValue();
                                                wmsTO.setUoms(uom);
                                            }

                                            if (row.getCell(12) != null && !"".equals(row.getCell(12))) {
                                                row.getCell(12).setCellType(row.getCell(12).CELL_TYPE_STRING);
                                                invoceNo = row.getCell(12).getStringCellValue();
                                                wmsTO.setInvoiceNo(invoceNo);
                                            }

                                            if (row.getCell(13) != null && !"".equals(row.getCell(13))) {
                                                row.getCell(13).setCellType(row.getCell(13).CELL_TYPE_STRING);
                                                invoiceDate = row.getCell(13).getStringCellValue();
                                                wmsTO.setInvoiceDate(invoiceDate);
                                            }

                                            if (row.getCell(14) != null && !"".equals(row.getCell(14))) {
                                                row.getCell(14).setCellType(row.getCell(14).CELL_TYPE_STRING);
                                                billNum = row.getCell(14).getStringCellValue();
                                                wmsTO.setBillNum(billNum);
                                            }

                                            if (row.getCell(15) != null && !"".equals(row.getCell(15))) {
                                                row.getCell(15).setCellType(row.getCell(15).CELL_TYPE_STRING);
                                                billDate = row.getCell(15).getStringCellValue();
                                                wmsTO.setBillDate(billDate);
                                            }

                                            if (row.getCell(16) != null && !"".equals(row.getCell(16))) {
                                                row.getCell(16).setCellType(row.getCell(16).CELL_TYPE_STRING);
                                                matDocNo = row.getCell(16).getStringCellValue();
                                                wmsTO.setMatDocNo(matDocNo);
                                            }

                                            if (row.getCell(17) != null && !"".equals(row.getCell(17))) {
                                                row.getCell(17).setCellType(row.getCell(17).CELL_TYPE_STRING);
                                                matDocDate = row.getCell(17).getStringCellValue();
                                                wmsTO.setMatDocDate(matDocDate);
                                            }

                                            if (row.getCell(18) != null && !"".equals(row.getCell(18))) {
                                                row.getCell(18).setCellType(row.getCell(18).CELL_TYPE_STRING);
                                                matDocYear = row.getCell(18).getStringCellValue();
                                                wmsTO.setMatDocYear(matDocYear);
                                            }

                                            if (row.getCell(19) != null && !"".equals(row.getCell(19))) {
                                                row.getCell(19).setCellType(row.getCell(19).CELL_TYPE_STRING);
                                                postingDate = row.getCell(19).getStringCellValue();
                                                wmsTO.setPostingDate(postingDate);
                                            }

                                            if (row.getCell(20) != null && !"".equals(row.getCell(20))) {
                                                row.getCell(20).setCellType(row.getCell(20).CELL_TYPE_STRING);
                                                suppPlantCode = row.getCell(20).getStringCellValue();
                                                wmsTO.setSuppPlantCode(suppPlantCode);
                                            }

                                            if (row.getCell(21) != null && !"".equals(row.getCell(21))) {
                                                row.getCell(21).setCellType(row.getCell(21).CELL_TYPE_STRING);
                                                suppPlantDesc = row.getCell(21).getStringCellValue();
                                                wmsTO.setSuppPlantDesc(suppPlantDesc);
                                            }

                                            if (row.getCell(22) != null && !"".equals(row.getCell(22))) {
                                                row.getCell(22).setCellType(row.getCell(22).CELL_TYPE_STRING);
                                                recPlantCode = row.getCell(22).getStringCellValue();
                                                wmsTO.setRecPlantCode(recPlantCode);
                                            }

                                            if (row.getCell(23) != null && !"".equals(row.getCell(23))) {
                                                row.getCell(23).setCellType(row.getCell(23).CELL_TYPE_STRING);
                                                recPlantDesc = row.getCell(23).getStringCellValue();
                                                wmsTO.setRecPlantDesc(recPlantDesc);
                                            }

                                            if (row.getCell(24) != null && !"".equals(row.getCell(24))) {
                                                row.getCell(24).setCellType(row.getCell(24).CELL_TYPE_STRING);
                                                grQty = row.getCell(24).getStringCellValue();
                                                wmsTO.setGrQty(grQty);
                                            }

                                            if (row.getCell(25) != null && !"".equals(row.getCell(25))) {
                                                row.getCell(25).setCellType(row.getCell(25).CELL_TYPE_STRING);
                                                storageLocation = row.getCell(25).getStringCellValue();
                                                wmsTO.setStorageLocation(storageLocation);
                                            }

                                            if (row.getCell(26) != null && !"".equals(row.getCell(26))) {
                                                row.getCell(26).setCellType(row.getCell(26).CELL_TYPE_STRING);
                                                purchaseOrderNo = row.getCell(26).getStringCellValue();
                                                wmsTO.setPurchaseOrderNo(purchaseOrderNo);
                                            }

                                            if (row.getCell(27) != null && !"".equals(row.getCell(27))) {
                                                row.getCell(27).setCellType(row.getCell(27).CELL_TYPE_STRING);
                                                deliveryNo = row.getCell(27).getStringCellValue();
                                                wmsTO.setDeliveryNumber(deliveryNo);
                                            }

                                            if (row.getCell(28) != null && !"".equals(row.getCell(28))) {
                                                row.getCell(28).setCellType(row.getCell(28).CELL_TYPE_STRING);
                                                movementType = row.getCell(28).getStringCellValue();
                                                wmsTO.setMovementType(movementType);
                                            }

                                            if (row.getCell(29) != null && !"".equals(row.getCell(29))) {
                                                row.getCell(29).setCellType(row.getCell(29).CELL_TYPE_STRING);
                                                createdBy = row.getCell(29).getStringCellValue();
                                                wmsTO.setCreatedBy(createdBy);
                                            }

                                            if (row.getCell(30) != null && !"".equals(row.getCell(30))) {
                                                row.getCell(30).setCellType(row.getCell(30).CELL_TYPE_STRING);
                                                cancelledNo = row.getCell(30).getStringCellValue();
                                                wmsTO.setCancelledNo(cancelledNo);
                                            }

                                            wmsTO.setWhId(hubId);
                                            status1 = wmsBP.uploadconnectGrn(wmsTO);
                                            System.out.println("status====" + status1);

                                        }
                                        sno++;

                                    }
                                } else if ("xls".equals(fileFormat)) {

                                    int count = 0;
                                    WorkbookSettings ws = new WorkbookSettings();
                                    ws.setLocale(new Locale("en", "EN"));
                                    Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                    Sheet s = workbook.getSheet(0);
                                    System.out.println("rows" + userId + s.getRows());

                                    for (int i = 1; i < s.getRows(); i++) {
                                        System.out.println("11111111111111111111");
                                        WmsTO wmsTO = new WmsTO();
                                        count++;

                                        wmsTO.setUserId(userId + "");

                                        gateInwardDate = s.getCell(0, i).getContents();
                                        System.out.println("AAAAAAAAAAAAAAAA " + gateInwardDate);
                                        wmsTO.setGateInwardDate(gateInwardDate);

                                        gateInwardNo = s.getCell(1, i).getContents();
                                        wmsTO.setGateInwardNo(gateInwardNo);

                                        processType = s.getCell(2, i).getContents();
                                        wmsTO.setProcessType(processType);

                                        modeofTransport = s.getCell(3, i).getContents();
                                        wmsTO.setModeofTransport(modeofTransport);

                                        vehicleNo = s.getCell(4, i).getContents();
                                        wmsTO.setVehicleNo(vehicleNo);

                                        transporterCode = s.getCell(5, i).getContents();
                                        wmsTO.setTransporterCode(transporterCode);

                                        transporterName = s.getCell(6, i).getContents();
                                        wmsTO.setTransporterName(transporterName);

                                        SerialNo = s.getCell(7, i).getContents();
                                        wmsTO.setSerials(SerialNo);

                                        materialCode = s.getCell(8, i).getContents();
                                        wmsTO.setMaterialCode(materialCode);

                                        materialDes = s.getCell(9, i).getContents();
                                        wmsTO.setMaterialDescription(materialDes);

                                        giQuantity = s.getCell(10, i).getContents();
                                        wmsTO.setGiQuantity(giQuantity);

                                        uom = s.getCell(11, i).getContents();
                                        wmsTO.setUoms(uom);

                                        invoceNo = s.getCell(12, i).getContents();
                                        wmsTO.setInvoiceNo(invoceNo);

                                        invoiceDate = s.getCell(13, i).getContents();
                                        wmsTO.setInvoiceDate(invoiceDate);

                                        billNum = s.getCell(14, i).getContents();
                                        wmsTO.setBillNum(billNum);

                                        billDate = s.getCell(15, i).getContents();
                                        wmsTO.setBillDate(billDate);

                                        matDocNo = s.getCell(16, i).getContents();
                                        wmsTO.setMatDocNo(matDocNo);

                                        matDocDate = s.getCell(17, i).getContents();
                                        wmsTO.setMatDocDate(matDocDate);

                                        matDocYear = s.getCell(18, i).getContents();
                                        wmsTO.setMatDocYear(matDocYear);

                                        postingDate = s.getCell(19, i).getContents();
                                        wmsTO.setPostingDate(postingDate);

                                        suppPlantCode = s.getCell(20, i).getContents();
                                        wmsTO.setSuppPlantCode(suppPlantCode);

                                        suppPlantDesc = s.getCell(21, i).getContents();
                                        wmsTO.setSuppPlantDesc(suppPlantDesc);

                                        recPlantCode = s.getCell(22, i).getContents();
                                        wmsTO.setRecPlantCode(recPlantCode);

                                        recPlantDesc = s.getCell(23, i).getContents();
                                        wmsTO.setRecPlantDesc(recPlantDesc);

                                        grQty = s.getCell(24, i).getContents();
                                        wmsTO.setGrQty(grQty);

                                        storageLocation = s.getCell(25, i).getContents();
                                        wmsTO.setStorageLocation(storageLocation);

                                        purchaseOrderNo = s.getCell(26, i).getContents();
                                        wmsTO.setPurchaseOrderNo(purchaseOrderNo);

                                        deliveryNo = s.getCell(27, i).getContents();
                                        wmsTO.setDeliveryNumber(deliveryNo);

                                        movementType = s.getCell(28, i).getContents();
                                        wmsTO.setMovementType(movementType);

                                        createdBy = s.getCell(29, i).getContents();
                                        wmsTO.setCreatedBy(createdBy);

                                        cancelledNo = s.getCell(30, i).getContents();
                                        wmsTO.setCancelledNo(cancelledNo);

                                        wmsTO.setWhId(hubId);
                                        status1 = wmsBP.uploadconnectGrn(wmsTO);
                                        System.out.println("status====" + status1);

                                    }

                                }

                            }
                        } else {
                            request.setAttribute("errorMessage", "Invoice Master updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            wms.setUserId(userId + "");
            ArrayList getConnectgrnDetails = new ArrayList();
            getConnectgrnDetails = wmsBP.getConnectgrnDetails(wms);
            request.setAttribute("getConnectgrnDetails", getConnectgrnDetails);
            wms.setTatType("2");
            ArrayList getVehicleTatList = wmsBP.getVehicleTatList(wms);
            request.setAttribute("getVehicleTatList", getVehicleTatList);

            System.out.println("getConnectgrnDetails" + getConnectgrnDetails.size());
            path = "content/wms/uploadGrn.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView connectGrnView(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        int productMaster = 0;
        WmsTO wms = new WmsTO();
        String menuPath = "";
        menuPath = "Operation  >> ProductMasters ";
        String pageTitle = "ProductMasters ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            ArrayList getProductMasterList = new ArrayList();
            wms.setUserId(userId + "");
            getProductMasterList = opsBP.getproductMasters(userId);
            String param = request.getParameter("param");
            request.setAttribute("getProductMasterList", getProductMasterList);
            if ("view".equals(param)) {
                request.setAttribute("param", param);
                ArrayList getGrnView = new ArrayList();
                getGrnView = wmsBP.getConnectGrnReport(wms);
                request.setAttribute("ProductMasters", getGrnView);
            } else {
                request.setAttribute("param", "edit");
                ArrayList getGrnView = new ArrayList();
                getGrnView = wmsBP.getgrnView(userId);
                request.setAttribute("ProductMasters", getGrnView);
            }
            path = "content/wms/grnview.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView savegrnData(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {

        System.out.println("saveProductMasters...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        int insertStatus = 0;
        int updateStatus = 0;
        WmsTO wmsTO = new WmsTO();
        String menuPath = "";
        menuPath = "Operation  >> saveProductMasters ";
        String pageTitle = "saveProductMasters ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            ArrayList getproductMasters = new ArrayList();

            String cancelledNumber = request.getParameter("cancelledNumber");
            String deliveryNo = request.getParameter("deliveryNo");
            String purchaseOrderNo = request.getParameter("purchaseOrderNo");
            String storageLocation = request.getParameter("storageLocation");
            String recplantDes = request.getParameter("recplantDes");
            String recplantCode = request.getParameter("recplantCode");
            String supPlantDes = request.getParameter("supPlantDes");
            String supPlantCode = request.getParameter("supPlantCode");
            String giQuantity = request.getParameter("giQuantity");
            String materialDes = request.getParameter("materialDes");
            String materialCode = request.getParameter("materialCode");
            String vehicleNO = request.getParameter("vehicleNO");
            String gateInwardNo = request.getParameter("gateInwardNo");
            String grnID = request.getParameter("grnID");

            wmsTO.setCancelledNumber(cancelledNumber);
            wmsTO.setDeliveryNo(deliveryNo);
            wmsTO.setPurchaseOrderNo(purchaseOrderNo);
            wmsTO.setStorageLocation(storageLocation);
            wmsTO.setRecplantDes(recplantDes);
            wmsTO.setRecplantCode(recplantCode);
            wmsTO.setSupPlantDes(supPlantDes);
            wmsTO.setSupPlantCode(supPlantCode);
            wmsTO.setGiQuantity(giQuantity);
            wmsTO.setMaterialDes(materialDes);
            wmsTO.setMaterialCode(materialCode);
            wmsTO.setVehicleNO(vehicleNO);
            wmsTO.setGateInwardNo(gateInwardNo);
            wmsTO.setGrnID(grnID);

            insertStatus = wmsBP.saveGrnView(wmsTO, userId);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated SucessFully");

            getproductMasters = wmsBP.getgrnView(userId);
            request.setAttribute("ProductMasters", getproductMasters);
            path = "content/wms/grnview.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void rackList(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();

        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            String locationId = request.getParameter("locationId");
            System.out.println("locationId==" + locationId);

            ArrayList getrackList = wmsBP.getRackList(locationId);
            request.setAttribute("driverList", getrackList);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getrackList.iterator();

            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                wmsTO = (WmsTO) itr.next();
                jsonObject.put("rackName", wmsTO.getRackName());
                jsonObject.put("rackId", wmsTO.getRackId());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void insertConnectPicklistDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();

        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            int userId = (Integer) session.getAttribute("userId");
            String hubId = (String) session.getAttribute("hubId");
            String serialNo = request.getParameter("serialNo");
            String serialId = request.getParameter("serialId");
            String invoiceId = request.getParameter("invoiceId");
            String invoiceNo = request.getParameter("invoiceNo");
            String productId = request.getParameter("productId");
            String stockId = request.getParameter("stockId");
            String qty = request.getParameter("qty");
            String custId = request.getParameter("custId");
            String dispatchId = request.getParameter("dispatchId");
            String invoiceDetailId = request.getParameter("invoiceDetailId");
            wmsTO.setWhId(hubId);
            wmsTO.setQty(qty);
            wmsTO.setDispatchId(dispatchId);
            wmsTO.setSerialId(serialId);
            wmsTO.setSerialNo(serialNo);
            wmsTO.setInvoiceNo(invoiceNo);
            wmsTO.setInvoiceId(invoiceId);
            wmsTO.setOrderDetailId(invoiceDetailId);
            wmsTO.setProductId(productId);
            wmsTO.setStockId(stockId);
            wmsTO.setCustId(custId);
            wmsTO.setUserId(userId + "");
            int insertStatus = wmsBP.insertConnectPicklistDetails(wmsTO);
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            if (insertStatus > 0) {
                jsonObject.put("id", insertStatus);
                jsonObject.put("qty", qty);
                jsonObject.put("status", "200");
            } else {
                jsonObject.put("status", "404");
            }
            jsonArray.put(jsonObject);
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void zipDownloadTest(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();
        FileOutputStream fos = null;
        ZipOutputStream zipOut = null;
        FileInputStream fis = null;
        PrintWriter pw = response.getWriter();
        String path = "";
        try {
            String[] Ids = request.getParameter("podCopys").split(",");
            List<String> files = new ArrayList<String>();
            ArrayList ifbPodDetails = wmsBP.ifbPodDetailsDownload(Ids);
            Iterator itr = ifbPodDetails.iterator();
            while (itr.hasNext()) {
                wmsTO = (WmsTO) itr.next();
                files.add(wmsTO.getFilePath());
            }
            fos = new FileOutputStream("E:\\Git-Projects\\handsnew\\build\\web\\content\\test.zip");
            zipOut = new ZipOutputStream(new BufferedOutputStream(fos));
            for (String fpath : files) {
                URL url = new URL(fpath);
                File input = new File(url.getFile());
                fis = new FileInputStream(input);
                ZipEntry ze = new ZipEntry(input.getName());
                System.out.println("Zipping the file: " + input.getName());
                zipOut.putNextEntry(ze);
                byte[] tmp = new byte[4 * 1024];
                int size = 0;
                while ((size = fis.read(tmp)) != -1) {
                    zipOut.write(tmp, 0, size);
                }
                zipOut.flush();
                fis.close();
            }
            zipOut.close();
            response.setContentType("text/zip");
            response.setHeader("Content-Disposition", "attachment; filename=POD.zip");
            String locationId = request.getParameter("locationId");
            System.out.println("locationId==" + locationId);
//            pw.print(zipOut);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void binList(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();

        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            String binId = request.getParameter("binId");
            System.out.println("binId==" + binId);

            ArrayList getbinList = wmsBP.getbinList(binId);
            request.setAttribute("driverList", getbinList);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getbinList.iterator();

            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                wmsTO = (WmsTO) itr.next();
                jsonObject.put("binName", wmsTO.getBinName());
                jsonObject.put("binId", wmsTO.getBinId());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

//    public void dzInvoiceDetailsPicked(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
//        HttpSession session = request.getSession();
//        wmsCommand = command;
//        WmsTO wmsTO = new WmsTO();
//
//        String path = "";
//        PrintWriter pw = response.getWriter();
//        try {
//            response.setContentType("text/html");
//            String binId = request.getParameter("binId");
//            System.out.println("binId==" + binId);
//
//            ArrayList getbinList = wmsBP.getbinList(binId);
//            request.setAttribute("driverList", getbinList);
//            JSONArray jsonArray = new JSONArray();
//            Iterator itr = getbinList.iterator();
//
//            while (itr.hasNext()) {
//                JSONObject jsonObject = new JSONObject();
//                wmsTO = (WmsTO) itr.next();
//                jsonObject.put("binName", wmsTO.getBinName());
//                jsonObject.put("binId", wmsTO.getBinId());
//                System.out.println("jsonObject = " + jsonObject);
//                jsonArray.put(jsonObject);
//            }
//            System.out.println("jsonArray = " + jsonArray);
//
//            pw.print(jsonArray);
//        } catch (FPRuntimeException excp) {
//            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
//        } catch (Exception exception) {
//            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
//            exception.printStackTrace();
//        }
//    }
    public ModelAndView connectPicklist(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {

        System.out.println("saveProductMasters...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        WmsTO wmsTO = new WmsTO();
        try {
            String param = "";
            wmsTO.setWhId(whId);
            param = request.getParameter("param");
            String dispatchId = request.getParameter("dispatchId");
            request.setAttribute("dispatchId", dispatchId);
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            wmsTO.setDispatchId(dispatchId);
            if ("generate".equals(param)) {
                ArrayList pickList = new ArrayList();
                pickList = wmsBP.getConnectPickList(wmsTO);
                request.setAttribute("pickList", pickList);
                path = "content/wms/connectPickListItems.jsp";
            } else if ("pick".equals(param)) {
                String productId = request.getParameter("productId");

                String qty = request.getParameter("qty");
                wmsTO.setProductId(productId);
                wmsTO.setQty(qty);
                ArrayList getConnectSerialList = wmsBP.getConnectSerialList(wmsTO);
                request.setAttribute("serialList", getConnectSerialList);
                request.setAttribute("qty", qty);

                request.setAttribute("dispatchId", dispatchId);

                ArrayList getConnectPickedSerial = wmsBP.getConnectPickedSerial(wmsTO);
                request.setAttribute("getConnectPickedSerial", getConnectPickedSerial);
                request.setAttribute("serialSize", getConnectPickedSerial.size());
                path = "content/wms/connectPickListSerial.jsp";
            } else if ("save".equals(param)) {
                wmsTO.setStatus("0");
                int updateConnectPicklist = wmsBP.updateConnectPicklist(wmsTO);
                if (updateConnectPicklist > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Update Failed");
                }
                path = "content/wms/connectPicklist.jsp";
            } else if ("excel".equalsIgnoreCase(param)) {
                wmsTO.setStatus("0");
                path = "content/wms/connectPicklistExcel.jsp";
            } else {
                wmsTO.setStatus("0");
                path = "content/wms/connectPicklist.jsp";
            }
            ArrayList getDispatchDetails = new ArrayList();
            getDispatchDetails = wmsBP.getDispatchDetails(wmsTO);
            request.setAttribute("getDispatchDetails", getDispatchDetails);
            System.out.println("getDispatchDetails.size()  " + getDispatchDetails.size());
            request.setAttribute("dispatchSize", getDispatchDetails.size());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewSerialNumber(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = ">>  View ";

        String pageTitle = "serialNumberUpdate";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "serialNumberUpdate";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList getConnectsubmitSerialNumber = new ArrayList();
            getConnectsubmitSerialNumber = wmsBP.getConnectsubmitSerialNumber(userId);
            request.setAttribute("getConnectsubmitSerialNumber", getConnectsubmitSerialNumber);
            path = "content/wms/connectserialNumberView.jsp";

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewSerialNumberSubmit(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = ">>  View ";

        String pageTitle = "serialNumberUpdate";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "serialNumberUpdate";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String whId = (String) session.getAttribute("hubId");
        try {
            String param = request.getParameter("param");
            String grnId = request.getParameter("grnId");
            request.setAttribute("grnId", grnId);
            wmsTo.setGrnId1(grnId);
            String itemId = request.getParameter("itemId");
            wmsTo.setProductID(itemId);
            request.setAttribute("itemId", itemId);
            wmsTo.setWhId(whId);
            System.out.println("itemId" + itemId);

            if ("save".equals(param)) {
                String remarks = request.getParameter("remarks");
                wmsTo.setRemarks(remarks);

                int insertSerialNumber = wmsBP.insertSerialNumber(wmsTo, userId, whId);
                if (insertSerialNumber >= 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Saved Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Failed to Save");
                }

                ArrayList getConnectsubmitSerialNumber = new ArrayList();
                getConnectsubmitSerialNumber = wmsBP.getConnectsubmitSerialNumber(userId);
                request.setAttribute("getConnectsubmitSerialNumber", getConnectsubmitSerialNumber);
                path = "content/wms/connectserialNumberView.jsp";

            } else {
                path = "content/wms/viewsubmitSerialNumber.jsp";
            }
            ArrayList getConnectSerialNumberDetails = new ArrayList();
            getConnectSerialNumberDetails = wmsBP.getConnectSerialNumberDetails(wmsTo);
            request.setAttribute("getConnectSerialNumberDetails", getConnectSerialNumberDetails);

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView connectinvoicUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        OpsTO opsTO = new OpsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            String dispatchId = request.getParameter("dispatchId");

            wmsTO.setDispatchId(dispatchId);
            String invocieId = request.getParameter("invoiceId");
            String invoiceNumber = request.getParameter("invoiceNumber");
            String custId = request.getParameter("custId");
            String customerName = request.getParameter("custName");

            request.setAttribute("invoiceId", invocieId);
            request.setAttribute("invoiceNumber", invoiceNumber);
            request.setAttribute("custId", custId);
            request.setAttribute("customerName", customerName);
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                String serialId = request.getParameter("serialIds");
                String[] serialIds = serialId.split("/");
                System.out.println("serialIds===" + serialIds.length);
                wmsTO.setSerialNos(serialIds);
                wmsTO.setCustomerId(custId);
                wmsTO.setInvoiceId(invocieId);

                int updateSerial = wmsBP.updateSerialNumber(wmsTO);

            }

            ArrayList getSerialNumber = new ArrayList();
            getSerialNumber = wmsBP.getSerialNumber(wmsTO);
            request.setAttribute("getSerialNumber", getSerialNumber);

            path = "content/wms/connectSerialNumber.jsp";
//            path = "content/wms/connectLrprint.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView connectPodApproval(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        int RoleId = (Integer) session.getAttribute("RoleId");
        try {
            wmsTO.setRoleId(RoleId + "");
            wmsTO.setUserId(userId + "");
            String param = request.getParameter("param");
            String whId = request.getParameter("whId");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            } else {
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            } else {
                wmsTO.setToDate(toDate);
            }
            if ("".equals(whId) || whId == null) {
                whId = hubId;
            }
            wmsTO.setWhId(whId);
            ArrayList whList = new ArrayList();
            whList = wmsBP.getWareHouse(wmsTO);
            request.setAttribute("whList", whList);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("whId", whId);
            System.out.println("fromDate == " + fromDate);
            if ("update".equals(param)) {

                String podId = request.getParameter("podId");
                String status = request.getParameter("status");

                int updateConnectPodApproval = wmsBP.updateConnectPodApproval(podId, status, wmsTO);

                if (updateConnectPodApproval > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " status Uploaded Successfully.");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "status Upload Failed");

                }
            }

            ArrayList getPodList = wmsBP.getPodList(wmsTO);
            request.setAttribute("getPodList", getPodList);
            path = "content/wms/viewConnectPodApproval.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView connectPodView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        int roleId = (Integer) session.getAttribute("RoleId");
        try {
            wmsTO.setUserId(userId + "");
            wmsTO.setRoleId(roleId + "");
            String param = request.getParameter("param");
            String invoiceNo = request.getParameter("invoiceNo");
            String whId = request.getParameter("whId");
            String status = request.getParameter("podStatus");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            } else {
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            } else {
                wmsTO.setToDate(toDate);
            }
            if ("".equals(whId) || whId == null) {
                whId = hubId;
            }
            wmsTO.setWhId(whId);
            wmsTO.setInvoiceNo(invoiceNo);
            wmsTO.setStatus(status);
            ArrayList whList = new ArrayList();
            whList = wmsBP.getWareHouse(wmsTO);
            request.setAttribute("whList", whList);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("whId", whId);
            wmsTO.setStatusName("2");
            System.out.println("fromDate == " + fromDate);

            ArrayList getPodList = wmsBP.getPodList(wmsTO);
            request.setAttribute("getPodList", getPodList);
            path = "content/wms/connectPodView.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView viewConnectStockDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            wmsTO.setUserId(userId + "");
            String param = request.getParameter("param");
            if ("view".equals(param)) {
                String stockId = request.getParameter("stockId");
                wmsTO.setStockId(stockId);
                ArrayList getSerialNumberDetails = new ArrayList();
                getSerialNumberDetails = wmsBP.viewConnectSerialNumberDetails(wmsTO);
                request.setAttribute("serialNumberDetails", getSerialNumberDetails);
                path = "content/wms/viewConnectSerialDetails.jsp";
            } else {
                path = "content/wms/viewConnectStockDetails.jsp";
            }
            ArrayList viewConnectStockDetails = wmsBP.viewConnectStockDetails(wmsTO);
            request.setAttribute("viewConnectStockDetails", viewConnectStockDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView connectOpenPack(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        try {
            wmsTO.setUserId(userId + "");
            wmsTO.setWhId(whId);
            String param = request.getParameter("param");
            int connectOpenPackSerialSave = 0;
            String packSerialNo = request.getParameter("packSerialNo");
            wmsTO.setSerialNo(packSerialNo);
            ArrayList getConnectSerialNoDetails = wmsBP.getConnectSerialNoDetails(wmsTO);
            request.setAttribute("connectSerialNoDetails", getConnectSerialNoDetails);
            request.setAttribute("size", getConnectSerialNoDetails.size());

            ArrayList getStorageDetails = new ArrayList();
            getStorageDetails = wmsBP.getStorageDetails();
            request.setAttribute("getStorageDetails", getStorageDetails);
            ArrayList getbinDetails = new ArrayList();
            path = "content/wms/connectOpenPack.jsp";
            getbinDetails = wmsBP.getbinDetails();
            request.setAttribute("getbinDetails", getbinDetails);
            ArrayList getrackDetails = new ArrayList();
            getrackDetails = wmsBP.getrackDetails();
            request.setAttribute("getrackDetails", getrackDetails);
            if ("save".equals(param)) {
                String grnId = request.getParameter("grnId");
                String serialId = request.getParameter("serialId");
                String stockId = request.getParameter("stockId");
                String productId = request.getParameter("productId");
                String[] serialNos = request.getParameterValues("serialNos");
                String[] storageIds = request.getParameterValues("storageIds");
                String[] uoms = request.getParameterValues("uom");
                String[] proCond = request.getParameterValues("proCond");
                String[] ipQty = request.getParameterValues("ipQty");
                String[] rackIds = request.getParameterValues("rackIds");
                String[] binIds = request.getParameterValues("binIds");
                wmsTO.setSerialNos(serialNos);
                wmsTO.setStorageIds(storageIds);
                wmsTO.setBinIds(binIds);
                wmsTO.setSerialId(serialId);
                wmsTO.setRackIdE(rackIds);
                wmsTO.setIpQty(ipQty);
                wmsTO.setProCond(proCond);
                wmsTO.setUom(uoms);
                wmsTO.setGrnId(grnId);
                wmsTO.setStockId(stockId);
                wmsTO.setProductId(productId);
                connectOpenPackSerialSave = wmsBP.connectOpenPackSerialSave(wmsTO);
                if (connectOpenPackSerialSave > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Serial Nos Updated Successfully.");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Update Failed");
                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView flurencoPickslip(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        OpsTO opsTO = new OpsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            wmsTO.setUserId(userId + "");
            opsTO.setUserId(userId);
            ArrayList deliveryExecutiveList = opsBP.deliveryExecutiveList(opsTO);
            System.out.println("deliveryExecutiveList.size() = " + deliveryExecutiveList.size());
            request.setAttribute("deliveryExecutiveList", deliveryExecutiveList);

            String param = request.getParameter("param");
            if ("pick".equals(param)) {
                String[] selectedStatus = request.getParameterValues("selectedStatus");
                String[] orderId = request.getParameterValues("orderId");
                wmsTO.setOrderIdE(orderId);
                wmsTO.setSelectedIndex(selectedStatus);
                ArrayList flurencoPickSlipList = new ArrayList();
                flurencoPickSlipList = wmsBP.flurencoPickslipDetails(wmsTO);
                request.setAttribute("flurencoPickslip", flurencoPickSlipList);
                path = "content/wms/flurencoPickslipDetails.jsp";
            } else if ("print".equals(param)) {
                path = "content/wms/flurencoPickslipPrint.jsp";
            } else if ("generate".equals(param)) {
                String[] selectedStatus = request.getParameterValues("selectedStatus");
                String[] orderId = request.getParameterValues("orderId");
                wmsTO.setOrderIdE(orderId);
                wmsTO.setSelectedIndex(selectedStatus);
                String vehicleNo = request.getParameter("vehicleNo");
                String driverName = request.getParameter("driverName");
                String driverMobile = request.getParameter("driverMobile");
                String pickupDate = request.getParameter("pickupDate");
                String pickupTime = request.getParameter("pickupTime");
                wmsTO.setVehicleNo(vehicleNo);
                wmsTO.setDriverName(driverName);
                wmsTO.setMobile(driverMobile);
                wmsTO.setPickupDate(pickupDate);
                wmsTO.setPickupTime(pickupTime);
                int updateFlurencoPickUpDetails = wmsBP.updateFlurencoPickUpDetails(wmsTO);
            } else {
                String fromDate = request.getParameter("fromDate");
                String toDate = request.getParameter("toDate");
                String shipmentType = request.getParameter("shipmentType");
                wmsTO.setFromDate(fromDate);
                wmsTO.setToDate(toDate);
                wmsTO.setShipmentType(shipmentType);
                ArrayList flurencoPickSlipList = new ArrayList();
                flurencoPickSlipList = wmsBP.flurencoPickslipList(wmsTO);
                request.setAttribute("flurencoPickslip", flurencoPickSlipList);
                path = "content/wms/flurencoPickslip.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView dcFreightUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        wmsCommand = command;
        String menuPath = "Connect Operation  >> DC Freight Update ";
        int updateFreight = 0;
        int userId = (Integer) session.getAttribute("userId");
        try {
            String whId = (String) session.getAttribute("hubId");
            String warehouseId = request.getParameter("whId");
            if (warehouseId == null || "".equals(warehouseId)) {
                warehouseId = whId;
            }
            wmsTO.setWhId(warehouseId);
            request.setAttribute("whId", warehouseId);
            String param = request.getParameter("param");
            System.out.println("parsm===" + param);
            if ("Submit".equals(param)) {
                String[] check = request.getParameterValues("selectedStatus");
                String[] lrnNo = request.getParameterValues("subLr");
                String[] freightAmt = request.getParameterValues("freightAmt");
                String[] haltingAmt = request.getParameterValues("haltingAmt");
                String[] unloadingAmt = request.getParameterValues("unloadingAmt");
                String[] otherAmt = request.getParameterValues("otherAmt");
                String[] totalAmt = request.getParameterValues("totalAmt");
                String[] remarks = request.getParameterValues("remarks");
                updateFreight = wmsBP.updateFreightAmount(userId, check, lrnNo, freightAmt, haltingAmt, unloadingAmt, otherAmt, totalAmt, remarks);

            } else if ("ExportExel".equals(param)) {
                System.out.println("ExportExel" + param);
                path = "content/wms/dcFreightUpdateExel.jsp";
            } else {
                path = "content/wms/dcFreightUpdate.jsp";
            }
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");

            String statusType = request.getParameter("statusType");
            wmsTO.setStatusType(statusType);
            request.setAttribute("statusType", statusType);

            ArrayList whList = wmsBP.getWareHouse(wmsTO);
            request.setAttribute("whList", whList);

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
            }
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            ArrayList dcFreightDetails = new ArrayList();
            dcFreightDetails = wmsBP.dcFreightDetails(wmsTO);
            request.setAttribute("dcFreightDetails", dcFreightDetails);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView connectFreightUploadExcel(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        wmsCommand = command;
        String menuPath = "Connect Operation  >> DC Freight Update ";
        String fromDate = "", toDate = "", tripCode = "";
        int updateFreight = 0;
        int userId = (Integer) session.getAttribute("userId");
        try {
            String param = request.getParameter("param");
            System.out.println("parsm===" + param);
            if ("upload".equals(param)) {
                String newFileName = "", actualFilePath = "";
                String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
                boolean isMultipart = false;
                Part partObj = null;
                FilePart fPart = null;
                String[] fileSaved = new String[10];
                String uploadedFileName = "";
                String fileFormat = "";
                String tempFilePath = "";
                isMultipart = ServletFileUpload.isMultipartContent(request);
                if (isMultipart) {
                    MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                    while ((partObj = parser.readNextPart()) != null) {
                        System.out.println("part Name:" + partObj.getName());
                        if (partObj.isFile()) {
                            actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                            System.out.println("Server Path == " + actualServerFilePath);
                            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                            System.out.println("Server Path After Replace== " + tempServerFilePath);
                            fPart = (FilePart) partObj;
                            uploadedFileName = fPart.getFileName();
                            System.out.println("uploadedFileName====" + uploadedFileName);
                            String[] splitFileNames = uploadedFileName.split("\\.");
                            int len = splitFileNames.length;
                            fileFormat = splitFileNames[len - 1];
                            System.out.println("fileFormat==" + fileFormat);
                            if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                                System.out.println("Ses");
                                if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                    String[] splitFileName = uploadedFileName.split("\\.");
                                    fileSavedAs = uploadedFileName;
                                    System.out.println("splitFileName[0]=" + splitFileName[0]);
                                    System.out.println("splitFileName[0]=" + splitFileName[1]);

                                    fileName = fileSavedAs;
                                    tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                    actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                    System.out.println("tempPath..." + tempFilePath);
                                    System.out.println("actPath..." + actualFilePath);
                                    long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                    System.out.println("fileSize..." + fileSize);
                                    File f1 = new File(actualFilePath);
                                    System.out.println("check " + f1.isFile());
                                    f1.renameTo(new File(tempFilePath));
                                    System.out.println("tempPath = " + tempFilePath);
                                    if ("xlsx".equals(fileFormat)) {
                                        FileInputStream fis = new FileInputStream(tempFilePath);
                                        XSSFWorkbook wb = new XSSFWorkbook(fis);
                                        XSSFSheet sheet = wb.getSheetAt(0);
                                        int value = sheet.getLastRowNum();
                                        System.out.println("value  " + value);

                                        String[] check = new String[value];
                                        String[] lrnNo = new String[value];
                                        String[] freightAmt = new String[value];
                                        String[] haltingAmt = new String[value];
                                        String[] unloadingAmt = new String[value];
                                        String[] otherAmt = new String[value];
                                        String[] totalAmt = new String[value];
                                        String[] remarks = new String[value];
                                        int sno = 0;
                                        Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                        while (itr.hasNext()) {
                                            if (sno > 0) {
                                                Row row = itr.next();
                                                System.out.println("cell " + row.getCell(1));
                                                check[sno - 1] = "1";
                                                lrnNo[sno - 1] = row.getCell(1).getStringCellValue();
                                                if (!"".equals(row.getCell(5).getStringCellValue())) {
                                                    freightAmt[sno - 1] = row.getCell(5).getStringCellValue();
                                                } else {
                                                    freightAmt[sno - 1] = "0.00";
                                                }
                                                if (!"".equals(row.getCell(6).getStringCellValue())) {
                                                    haltingAmt[sno - 1] = row.getCell(6).getStringCellValue();
                                                } else {
                                                    haltingAmt[sno - 1] = "0.00";
                                                }
                                                if (!"".equals(row.getCell(7).getStringCellValue())) {
                                                    unloadingAmt[sno - 1] = row.getCell(7).getStringCellValue();
                                                } else {
                                                    unloadingAmt[sno - 1] = "0.00";
                                                }
                                                if (!"".equals(row.getCell(8).getStringCellValue())) {
                                                    otherAmt[sno - 1] = row.getCell(8).getStringCellValue();
                                                } else {
                                                    otherAmt[sno - 1] = "0.00";
                                                }
                                                totalAmt[sno - 1] = row.getCell(9).getStringCellValue();
                                                remarks[sno - 1] = "Excel Upload";
                                            }
                                            sno++;

                                        }
                                        updateFreight = wmsBP.updateFreightAmount(userId, check, lrnNo, freightAmt, haltingAmt, unloadingAmt, otherAmt, totalAmt, remarks);
                                    } else if ("xls".equals(fileFormat)) {
                                        WorkbookSettings ws = new WorkbookSettings();
                                        ws.setLocale(new Locale("en", "EN"));
//                                        HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(new File(actualFilePath)));
//                                        HSSFSheet s = wb.getSheetAt(0);
                                        Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                        Sheet s = workbook.getSheet(0);

                                        int value = s.getRows() - 1;
                                        System.out.println("value  " + value);
                                        String[] check = new String[value];
                                        String[] lrnNo = new String[value];
                                        String[] freightAmt = new String[value];
                                        String[] haltingAmt = new String[value];
                                        String[] unloadingAmt = new String[value];
                                        String[] otherAmt = new String[value];
                                        String[] totalAmt = new String[value];
                                        String[] remarks = new String[value];
                                        for (int i = 1; i < s.getRows(); i++) {
                                            check[i - 1] = "1";
                                            lrnNo[i - 1] = s.getCell(1, i).getContents();
                                            if (!"".equals(s.getCell(5, i).getContents())) {
                                                freightAmt[i - 1] = s.getCell(5, i).getContents();
                                            } else {
                                                freightAmt[i - 1] = "0.00";
                                            }
                                            if (!"".equals(s.getCell(6, i).getContents())) {
                                                haltingAmt[i - 1] = s.getCell(6, i).getContents();
                                            } else {
                                                haltingAmt[i - 1] = "0.00";
                                            }
                                            if (!"".equals(s.getCell(7, i).getContents())) {
                                                unloadingAmt[i - 1] = s.getCell(7, i).getContents();
                                            } else {
                                                unloadingAmt[i - 1] = "0.00";
                                            }
                                            if (!"".equals(s.getCell(8, i).getContents())) {
                                                otherAmt[i - 1] = s.getCell(8, i).getContents();
                                            } else {
                                                otherAmt[i - 1] = "0.00";
                                            }
                                            totalAmt[i - 1] = s.getCell(9, i).getContents();
                                            remarks[i - 1] = "Excel Upload";
                                        }
                                        updateFreight = wmsBP.updateFreightAmount(userId, check, lrnNo, freightAmt, haltingAmt, unloadingAmt, otherAmt, totalAmt, remarks);
                                    }
                                }
                            } else {
                                request.setAttribute("errorMessage", "File Upload Failed:");
                            }
                        }
                    }
                }
            }
            path = "content/wms/dcFreightUpdate.jsp";
            if (wmsCommand.getFromDate() != null && wmsCommand.getFromDate() != "") {
                wmsTO.setFromDate(wmsCommand.getFromDate());
                fromDate = wmsCommand.getFromDate();
            }
            if (wmsCommand.getToDate() != null && wmsCommand.getToDate() != "") {
                wmsTO.setToDate(wmsCommand.getToDate());
                toDate = wmsCommand.getToDate();
            }
            String statusType = request.getParameter("statusType");
            wmsTO.setStatusType(statusType);
            request.setAttribute("statusType", statusType);

            ArrayList whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            ArrayList dcFreightDetails = new ArrayList();
            dcFreightDetails = wmsBP.dcFreightDetails(wmsTO);
            request.setAttribute("dcFreightDetails", dcFreightDetails);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView connectPodUploadExcel(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        wmsCommand = command;
        String fromDate = "", toDate = "", tripCode = "";
        int deliveryDateUpdate = 0;
        try {

            String param = request.getParameter("param");
            System.out.println("parsm===" + param);
            if ("upload".equals(param)) {
                String newFileName = "", actualFilePath = "";
                String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
                boolean isMultipart = false;
                Part partObj = null;
                FilePart fPart = null;
                String[] fileSaved = new String[10];
                String uploadedFileName = "";
                String fileFormat = "";
                String tempFilePath = "";
                isMultipart = ServletFileUpload.isMultipartContent(request);
                if (isMultipart) {
                    MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                    while ((partObj = parser.readNextPart()) != null) {
                        System.out.println("part Name:" + partObj.getName());
                        if (partObj.isFile()) {
                            actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                            System.out.println("Server Path == " + actualServerFilePath);
                            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                            System.out.println("Server Path After Replace== " + tempServerFilePath);
                            fPart = (FilePart) partObj;
                            int userId = (Integer) session.getAttribute("userId");
                            wmsTO.setUserId(userId + "");
                            uploadedFileName = fPart.getFileName();
                            System.out.println("uploadedFileName====" + uploadedFileName);
                            String[] splitFileNames = uploadedFileName.split("\\.");
                            int len = splitFileNames.length;
                            fileFormat = splitFileNames[len - 1];
                            System.out.println("fileFormat==" + fileFormat);
                            if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                                System.out.println("Ses");
                                if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                    String[] splitFileName = uploadedFileName.split("\\.");
                                    fileSavedAs = uploadedFileName;
                                    System.out.println("splitFileName[0]=" + splitFileName[0]);
                                    System.out.println("splitFileName[0]=" + splitFileName[1]);

                                    fileName = fileSavedAs;
                                    tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                    actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                    System.out.println("tempPath..." + tempFilePath);
                                    System.out.println("actPath..." + actualFilePath);
                                    long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                    System.out.println("fileSize..." + fileSize);
                                    File f1 = new File(actualFilePath);
                                    System.out.println("check " + f1.isFile());
                                    f1.renameTo(new File(tempFilePath));
                                    System.out.println("tempPath = " + tempFilePath);
                                    if ("xlsx".equals(fileFormat)) {
                                        FileInputStream fis = new FileInputStream(tempFilePath);
                                        XSSFWorkbook wb = new XSSFWorkbook(fis);
                                        XSSFSheet sheet = wb.getSheetAt(0);
                                        int value = sheet.getLastRowNum();
                                        System.out.println("value  " + value);

                                        String[] check = new String[value];
                                        String[] lrnNo = new String[value];
                                        String[] deliveryDate = new String[value];
                                        String[] remarks = new String[value];
                                        int sno = 0;
                                        Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                        while (itr.hasNext()) {
                                            if (sno > 0) {
                                                Row row = itr.next();
                                                System.out.println("cell " + row.getCell(1));
                                                check[sno - 1] = "1";
                                                lrnNo[sno - 1] = row.getCell(1).getStringCellValue();
                                                deliveryDate[sno - 1] = row.getCell(11).getStringCellValue();
                                                remarks[sno - 1] = row.getCell(12).getStringCellValue();
                                            }
                                            sno++;
                                        }
                                        deliveryDateUpdate = wmsBP.updateConnectDeliveryDate(lrnNo, deliveryDate, remarks, wmsTO);
                                    } else if ("xls".equals(fileFormat)) {
                                        WorkbookSettings ws = new WorkbookSettings();
                                        ws.setLocale(new Locale("en", "EN"));
//                                        HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(new File(actualFilePath)));
//                                        HSSFSheet s = wb.getSheetAt(0);
                                        Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                        Sheet s = workbook.getSheet(0);

                                        int value = s.getRows() - 1;
                                        System.out.println("value  " + value);
                                        String[] check = new String[value];
                                        String[] lrnNo = new String[value];
                                        String[] deliveryDate = new String[value];
                                        String[] remarks = new String[value];
                                        for (int i = 1; i < s.getRows(); i++) {
                                            System.out.println("rows  " + s.getCell(0, i).getContents());
                                            check[i - 1] = "1";
                                            lrnNo[i - 1] = s.getCell(1, i).getContents();
                                            deliveryDate[i - 1] = s.getCell(11, i).getContents();
                                            remarks[i - 1] = s.getCell(12, i).getContents();
                                        }
                                        deliveryDateUpdate = wmsBP.updateConnectDeliveryDate(lrnNo, deliveryDate, remarks, wmsTO);
                                    }
                                }
                            } else {
                                request.setAttribute("errorMessage", "File Upload Failed:");
                            }
                        }
//                        updateFreight = wmsBP.updateFreightAmount(check, lrnNo, freightAmt, haltingAmt, unloadingAmt, otherAmt, totalAmt, remarks);

                    }
                }
            }
            path = "content/wms/connectPodUpload.jsp";
            if (wmsCommand.getFromDate() != null && wmsCommand.getFromDate() != "") {
                wmsTO.setFromDate(wmsCommand.getFromDate());
                fromDate = wmsCommand.getFromDate();
            }
            if (wmsCommand.getToDate() != null && wmsCommand.getToDate() != "") {
                wmsTO.setToDate(wmsCommand.getToDate());
                toDate = wmsCommand.getToDate();
            }
            String statusType = request.getParameter("statusType");
            wmsTO.setStatusType(statusType);
            request.setAttribute("statusType", statusType);

            ArrayList whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);

            ArrayList podUploadDetails = new ArrayList();
            podUploadDetails = wmsBP.podUploadDetails(wmsTO);
            request.setAttribute("podUploadDetails", podUploadDetails);

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView docketNoUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        wmsCommand = command;
        String menuPath = "Connect Operation  >>Docket No. Update ";
        String fromDate = "", toDate = "";
        int updateDc = 0;
        String whId = (String) session.getAttribute("hubId");
        int roleId = (Integer) session.getAttribute("RoleId");
        try {
            wmsTO.setWhId(whId);
            wmsTO.setRoleId(roleId + "");
            String param = request.getParameter("param");
            System.out.println("parsm===" + param);
            ArrayList whList = wmsBP.getWareHouse(wmsTO);
            request.setAttribute("whList", whList);

            if (param.equals("Submit")) {
                String[] check = request.getParameterValues("selectedStatus");
                String[] lrnNo = request.getParameterValues("subLr");
                String[] docketNo = request.getParameterValues("docketNo");

                updateDc = wmsBP.updateDocketnumber(check, lrnNo, docketNo);

            } else if (param.equals("ExportExel")) {
                path = "content/wms/docketNoUpdateExel.jsp";
            } else {
                path = "content/wms/docketNoUpdate.jsp";
            }
            if (wmsCommand.getFromDate() != null && wmsCommand.getFromDate() != "") {
                wmsTO.setFromDate(wmsCommand.getFromDate());
                fromDate = wmsCommand.getFromDate();
            }
            if (wmsCommand.getToDate() != null && wmsCommand.getToDate() != "") {
                wmsTO.setToDate(wmsCommand.getToDate());
                toDate = wmsCommand.getToDate();
            }
            String statusType = request.getParameter("statusType");
            wmsTO.setStatusType(statusType);
            request.setAttribute("statusType", statusType);

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            ArrayList docketNoUpdateDetails = new ArrayList();
            docketNoUpdateDetails = wmsBP.docketNoUpdateDetails(wmsTO);
            request.setAttribute("docketNoUpdateDetails", docketNoUpdateDetails);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView connectPodUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        wmsCommand = command;
        String menuPath = "Connect Operation  >> POD Upload ";
        String fromDate = "", toDate = "";
        int updateFreight = 0;
        Part partObj = null;
        boolean isMultipart = false;
        try {
            String whId = (String) session.getAttribute("hubId");
            String param = request.getParameter("param");
            wmsTO.setWhId(whId);
            String hubId = request.getParameter("whId");
            wmsTO.setWarehouseId(hubId);
            System.out.println("parsm===" + param);

            path = "content/wms/connectPodUpload.jsp";
            if (wmsCommand.getFromDate() != null && wmsCommand.getFromDate() != "") {
                wmsTO.setFromDate(wmsCommand.getFromDate());
                fromDate = wmsCommand.getFromDate();
            }

            if (wmsCommand.getToDate() != null && wmsCommand.getToDate() != "") {
                wmsTO.setToDate(wmsCommand.getToDate());
                toDate = wmsCommand.getToDate();
            }

            String statusType = request.getParameter("statusType");
            wmsTO.setStatusType(statusType);

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }
            wmsTO.setStatusType("0");
            ArrayList whList = new ArrayList();
            whList = wmsBP.getWareHouse(wmsTO);
            request.setAttribute("whList", whList);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            ArrayList podUploadDetails = new ArrayList();
            podUploadDetails = wmsBP.podUploadDetails(wmsTO);
            request.setAttribute("podUploadDetails", podUploadDetails);
            if ("save".equals(param)) {
                String deliveryDate = request.getParameter("deliveryDate");
                String lrNo = request.getParameter("lrNo");
                wmsTO.setDeliveryDate(deliveryDate);
                wmsTO.setLrnNo(lrNo);
                int updateLrNo = wmsBP.updateConnectLrNo(wmsTO);
            } else if ("ExportExcel".equals(param)) {
                path = "content/wms/connectPodUploadExcel.jsp";
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveConnectPodUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        wmsCommand = command;
        String menuPath = "Operation  >>  Edit Start Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;
        int m = 0;
        int n = 0;
        int p = 0;
        int s = 0;
        String fromDate = "", toDate = "";
        String[] check = new String[100];
        String[] lrnNo = new String[100];

        String[] fileSaved = new String[100];
        String[] uploadedFileName = new String[100];
        String[] tempFilePath = new String[100];

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            WmsTO wmsTO = new WmsTO();

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 19 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        if (partObj.getName().equals("attachFile")) {
                            actualServerFilePath = getServletContext().getRealPath("/content/upload");
                            System.out.println("Server Path == " + actualServerFilePath);
                            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                            System.out.println("Server Path After Replace== " + tempServerFilePath);
                            Date now = new Date();
                            String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                            String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                            fPart = (FilePart) partObj;
                            uploadedFileName[j] = fPart.getFileName();

                            if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {

                                // for ubuntu
//                            String[] splitFileName = uploadedFileName[j].split(".");
                                // for windows
                                String[] splitFileName = uploadedFileName[j].split("\\.");
                                fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                                fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                                fileName = fileSavedAs;

                                tempFilePath[j] = tempServerFilePath + "/" + fileSaved[j];
                                actualFilePath = actualServerFilePath + "/" + tempFilePath;
//                            for windows machines
//                            tempFilePath[j] = tempServerFilePath + "\\" + fileSaved[j];
//                            actualFilePath = actualServerFilePath + "\\" + tempFilePath;
                                System.out.println("tempPath..." + tempFilePath);
                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath[j]));
                                System.out.println("tempPath = " + tempFilePath);
                                System.out.println("actPath = " + actualFilePath);
//                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
//                            String part1 = parts.replace("\\", "");
                            }

                            System.out.println("fileName..." + fileName);
                            j++;
                            System.out.println("value of j===" + j);
                        }
                    } else if (partObj.isParam()) {

                        System.out.println("Entering partObj.isParam:::");

                        if (partObj.getName().equals("subLr")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            lrnNo[m] = paramPart.getStringValue();
                            m++;
                            System.out.println("lrnNo===" + lrnNo[m]);
                            System.out.println("value of m===" + m);
                        }

                    }
                    System.out.println("NO ELSE:::");
                }
            }

            String file = "";

            String saveFile = "";
            int status = 0;
            System.out.println("j value---" + j);

            for (int x = 0; x < j; x++) {
                file = tempFilePath[x];

                saveFile = fileSaved[x];

                System.out.println("check==" + check[x]);
                System.out.println("lrN==" + lrnNo[x]);

                String check1 = check[x];
                String lrnNo1 = lrnNo[x];
                System.out.println("check1===" + check1);

                if ("1".equals(check1)) {
                    status = wmsBP.uploadPOD(file, saveFile, check1, lrnNo1);
                    System.out.println("savePodDetails1==" + status);

                    if (status > 0) {
                        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Pod Uploaded Successfully.");
                    } else {
                        request.setAttribute(ParveenErrorConstants.ERROR_KEY, "POD Upload Failed");

                    }

                }
            }

            if (wmsCommand.getFromDate() != null && wmsCommand.getFromDate() != "") {
                wmsTO.setFromDate(wmsCommand.getFromDate());
                fromDate = wmsCommand.getFromDate();
            }
            if (wmsCommand.getToDate() != null && wmsCommand.getToDate() != "") {
                wmsTO.setToDate(wmsCommand.getToDate());
                toDate = wmsCommand.getToDate();
            }
            String statusType = request.getParameter("statusType");
            wmsTO.setStatusType(statusType);
            request.setAttribute("statusType", statusType);

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }
            System.out.println("From date  " + fromDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            ArrayList podUploadDetails = new ArrayList();
            podUploadDetails = wmsBP.podUploadDetails(wmsTO);
            request.setAttribute("podUploadDetails", podUploadDetails);

            ArrayList whList = new ArrayList();
            whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);

            path = "content/wms/connectPodUpload.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
//        return mv;
    }

    public ModelAndView connectUploadPage(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        OpsTO opsTO = new OpsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        try {
            wmsTO.setWhId(hubId);
            int updateconnectInvoice = 0;
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            String param = request.getParameter("param");
            String subLrId = request.getParameter("subLrId");
            String dealerId = request.getParameter("dealerId");
            System.out.println("dealerId------------" + dealerId);
            request.setAttribute("dealerId", dealerId);
            request.setAttribute("subLrId", subLrId);
            String dispatchid = request.getParameter("dispatchid");
            request.setAttribute("dispatchid", dispatchid);
            String lrnNo = request.getParameter("lrnNo");
            wmsTO.setLrnNo(lrnNo);
            String deliveryDate = wmsBP.getConnectDeliveryDate(wmsTO);
            request.setAttribute("deliveryDate", deliveryDate);
            request.setAttribute("lrnNo", lrnNo);
            String dispatchDetailId = request.getParameter("dispatchDetailId");
            request.setAttribute("dispatchDetailId", dispatchDetailId);
            String lrdate = request.getParameter("lrdate");
            request.setAttribute("lrdate", lrdate);
            String invoiceNo = request.getParameter("invoiceNo");
            request.setAttribute("invoiceNo", invoiceNo);
            String invoiceDate = request.getParameter("invoiceDate");
            request.setAttribute("invoiceDate", invoiceDate);

            String CustomerName = request.getParameter("CustomerName");
            request.setAttribute("CustomerName", CustomerName);

            path = "content/wms/uploadPage.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView storageCreation(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        wmsTO.setUserId(userId + "");
        String menuPath = "";
        String param = "";
        String pageTitle = "ifbHubOut";
        menuPath = "Masters  >> ifbHubOut ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            param = request.getParameter("param");
            String[] orderIds = request.getParameterValues("orderId");
            String[] selectedStatus = request.getParameterValues("selectedStatus");
            String[] hubId = request.getParameterValues("hubId");
            String[] ewayBillNo = request.getParameterValues("ewayBillNoTemp");
            String[] ewayExpiry = request.getParameterValues("ewayBillExpiryTemp");
            String vehicleNo = request.getParameter("vehicleNo");
            String vendorId = request.getParameter("vendorId");
            String driverName = request.getParameter("driverName");
            String driverMobile = request.getParameter("driverMobile");
            ArrayList vendorList = wmsBP.getVendorList();
            request.setAttribute("vendorList", vendorList);
            int updateIfbHubout = 0;
            Date date = new Date();
            ArrayList whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);
            String str = new SimpleDateFormat("yyyy-MM-dd").format(date);
            String date1 = str.replace("-", "");
            if ("update".equals(param)) {
                System.out.println("reached hub out update   " + orderIds.length);
                updateIfbHubout = wmsBP.updateIfbHubout(ewayBillNo, ewayExpiry, orderIds, selectedStatus, hubId, vehicleNo, driverName, driverMobile, date1, vendorId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Hub out done Successfully.");
                path = "content/wms/ifbHubOut.jsp";
            } else if ("serial".equals(param)) {
                String qte = request.getParameter("qte");
                if ("no".equals(qte)) {
                    request.setAttribute("qte", "1");
                    System.out.println("qte 2 ===" + qte);
                } else {
                    request.setAttribute("qte", "0");
                    System.out.println("qte 1 ===" + qte);
                }
                String lrId = request.getParameter("lrId");
                request.setAttribute("lrId", lrId);
                String type = request.getParameter("type");
                if ("update".equals(type)) {
                    String[] orderId = request.getParameterValues("orderId");
                    String[] custName = request.getParameterValues("customerName");
                    String[] qty = request.getParameterValues("qty");
                    String[] index = request.getParameterValues("index");
                    int updateIfbQty = wmsBP.updateIfbQty(orderId, qty, index, custName);
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Quantity Updated Successfully.");
                }
                ArrayList ifbDetails = wmsBP.getIfbOrderDetails(lrId);
                request.setAttribute("ifbDetails", ifbDetails);
                path = "content/wms/storageCreation.jsp";
            } else {
                path = "content/wms/storageCreation.jsp";
            }
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            ArrayList hubOutList = wmsBP.hubOutList(wmsTO);
            request.setAttribute("hubOutList", hubOutList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to addPincodeMaster --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView connectBillMapping(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        OpsTO opsTO = new OpsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        try {
            wmsTO.setWhId(hubId);

            String newFileName = "", actualFilePath = "";
            String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
            boolean isMultipart = false;
            Part partObj = null;
            FilePart fPart = null;
            String uploadedFileName = "";
            String fileFormat = "";
            String tempFilePath = "";

            String lrNo = "";
            String freightAmount = "";
            String unloadingAmt = "";
            String haltingAmt = "";
            String otherAmt = "";
            String gstAmount = "";
            String billAmt = "";
            String totalAmount = "";
            String param = request.getParameter("param");
            String billId = request.getParameter("billId");
            String billNo = request.getParameter("billNo");
            request.setAttribute("billId", billId);
            wmsTO.setBillId(billId);
            wmsTO.setBillNo(billNo);
            System.out.println("billId  " + billId);
            if ("view".equals(param)) {
                path = "content/wms/connectBillMappingView.jsp";
            } else if ("upload".equals(param)) {
                isMultipart = ServletFileUpload.isMultipartContent(request);
                if (isMultipart) {
                    MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                    while ((partObj = parser.readNextPart()) != null) {
                        System.out.println("part Name:" + partObj.getName());
                        if (partObj.isFile()) {
                            actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                            System.out.println("Server Path == " + actualServerFilePath);
                            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                            System.out.println("Server Path After Replace== " + tempServerFilePath);
                            fPart = (FilePart) partObj;
                            uploadedFileName = fPart.getFileName();
                            System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                            String[] splitFileNames = uploadedFileName.split("\\.");
                            int len = splitFileNames.length;
                            fileFormat = splitFileNames[len - 1];
                            System.out.println("fileFormat==" + fileFormat);
                            if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                                System.out.println("Ses");
                                if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                    String[] splitFileName = uploadedFileName.split("\\.");
                                    fileSavedAs = uploadedFileName;
                                    System.out.println("splitFileName[0]=" + splitFileName[0]);
                                    System.out.println("splitFileName[0]=" + splitFileName[1]);

                                    fileName = fileSavedAs;
                                    tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                    actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                    System.out.println("tempPath..." + tempFilePath);
                                    System.out.println("actPath..." + actualFilePath);
                                    long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                    System.out.println("fileSize..." + fileSize);
                                    File f1 = new File(actualFilePath);
                                    System.out.println("check " + f1.isFile());
                                    f1.renameTo(new File(tempFilePath));
                                    System.out.println("tempPath = " + tempFilePath);
//                                System.out.println("actPath = " + actualFilePath);
//                                String parts = actualFilePath.substring(actualFilePath.lastIndexOf("//"));
//                                String part1 = parts.replace("//", "");
                                    System.out.println("11111111111111111111111111");
                                    if ("xlsx".equals(fileFormat)) {
                                        FileInputStream fis = new FileInputStream(tempFilePath);
                                        XSSFWorkbook wb = new XSSFWorkbook(fis);
                                        XSSFSheet sheet = wb.getSheetAt(0);
                                        int value = sheet.getLastRowNum();
                                        System.out.println("value  " + value);

                                        int sno = 0;
                                        Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                        while (itr.hasNext()) {
                                            Row row = itr.next();
                                            if (sno > 0) {
                                                System.out.println("cellyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy " + row.getCell(1));

                                                wmsTO.setUserId(userId + "");

                                                wmsTO.setUserId(userId + "");

                                                if (row.getCell(1) != null && !"".equals(row.getCell(1))) {
                                                    row.getCell(1).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                    lrNo = row.getCell(1).getStringCellValue();
                                                    wmsTO.setLrNo(lrNo);
                                                }

                                                if (row.getCell(2) != null && !"".equals(row.getCell(2))) {
                                                    row.getCell(2).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                    freightAmount = row.getCell(2).getStringCellValue();
                                                    wmsTO.setFreightAmount(freightAmount);
                                                }

                                                if (row.getCell(3) != null && !"".equals(row.getCell(3))) {
                                                    row.getCell(3).setCellType(row.getCell(3).CELL_TYPE_STRING);
                                                    unloadingAmt = row.getCell(3).getStringCellValue();
                                                    wmsTO.setUnloadingAmt(unloadingAmt);
                                                }

                                                if (row.getCell(4) != null && !"".equals(row.getCell(4))) {
                                                    row.getCell(4).setCellType(row.getCell(4).CELL_TYPE_STRING);
                                                    haltingAmt = row.getCell(4).getStringCellValue();
                                                    wmsTO.setHaltingAmt(haltingAmt);
                                                }

                                                if (row.getCell(5) != null && !"".equals(row.getCell(5))) {
                                                    row.getCell(5).setCellType(row.getCell(5).CELL_TYPE_STRING);
                                                    otherAmt = row.getCell(5).getStringCellValue();
                                                    wmsTO.setOtherAmt(otherAmt);
                                                }

                                                if (row.getCell(6) != null && !"".equals(row.getCell(6))) {
                                                    row.getCell(6).setCellType(row.getCell(6).CELL_TYPE_STRING);
                                                    gstAmount = row.getCell(6).getStringCellValue();
                                                    wmsTO.setGstAmt(gstAmount);
                                                }

                                                if (row.getCell(7) != null && !"".equals(row.getCell(7))) {
                                                    row.getCell(7).setCellType(row.getCell(7).CELL_TYPE_STRING);
                                                    totalAmount = row.getCell(7).getStringCellValue();
                                                    wmsTO.setTotalAmt(totalAmount);
                                                }

                                                int insertConnectBillTemp = wmsBP.insertConnectBillUpload(wmsTO);

                                            }
                                            sno++;

                                        }
                                    }
                                } else if ("xls".equals(fileFormat)) {

                                    int count = 0;
                                    WorkbookSettings ws = new WorkbookSettings();
                                    ws.setLocale(new Locale("en", "EN"));
                                    Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                    Sheet s = workbook.getSheet(0);
                                    System.out.println("rows" + userId + s.getRows());
                                    int clearConnectBillMappingTemp = wmsBP.clearConnectBillMappingTemp(userId);
                                    for (int i = 1; i < s.getRows(); i++) {
                                        System.out.println("88888888");

                                        wmsTO.setUserId(userId + "");
                                        count++;

                                        lrNo = s.getCell(1, i).getContents();
                                        wmsTO.setLrNo(lrNo);

                                        freightAmount = s.getCell(2, i).getContents();
                                        wmsTO.setFreightAmount(freightAmount);

                                        unloadingAmt = s.getCell(3, i).getContents();
                                        wmsTO.setUnloadingAmt(unloadingAmt);

                                        haltingAmt = s.getCell(4, i).getContents();
                                        wmsTO.setHaltingAmt(haltingAmt);

                                        otherAmt = s.getCell(5, i).getContents();
                                        wmsTO.setOtherAmt(otherAmt);

                                        gstAmount = s.getCell(6, i).getContents();
                                        wmsTO.setGstAmt(gstAmount);

                                        totalAmount = s.getCell(7, i).getContents();
                                        wmsTO.setTotalAmt(totalAmount);

                                        int insertConnectBillTemp = wmsBP.insertConnectBillUpload(wmsTO);
                                    }

                                }
                            } else {
                                request.setAttribute("errorMessage", "File Upload Failed:");
                            }
                        }
                    }
                    ArrayList getConnectBillUpload = new ArrayList();
                    getConnectBillUpload = wmsBP.getConnectBillUpload(wmsTO);
                    request.setAttribute("billTemp", getConnectBillUpload);
                    path = "content/wms/connectBillMappingView.jsp";
                    request.setAttribute("size", getConnectBillUpload.size());
                }
            } else if ("save".equals(param)) {
                wmsTO.setUserId(userId + "");
                int saveConnectBillMapping = wmsBP.saveConnectBillMapping(wmsTO);
                if (saveConnectBillMapping > 0) {
                } else {
                }
                path = "content/wms/connectBillMapping.jsp";
            } else {
                path = "content/wms/connectBillMapping.jsp";
            }
            ArrayList connectGetBillReceiving = wmsBP.connectGetBillReceiving(wmsTO);
            request.setAttribute("connectGetBillReceiving", connectGetBillReceiving);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlePODupload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        wmsCommand = command;
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        path = "content/vendor/manageVendor.jsp";
        String pageTitle = "Add Vendor";
        request.setAttribute("pageTitle", pageTitle);
        int vendorId = 0;

        String menuPath = "Vendor  >>  ManageVendor  >> Add";

        //file upload
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;
        int m = 0;
        int n = 0;
        int p = 0;
        int s = 0;
        String dealerName = "";
        String tripSheetId1 = "";
        String subLrId1 = "";
        String dispatchid1 = "";
        String lrnNo1 = "";
        String plannedTime1 = "";
        String vendorName1 = "";
        String dispatchDetailId1 = "";
        String plannedDate1 = "";
        String tinNo1 = "";

        //        String[] podRemarks1 = new String[10];
        String[] fileSaved = new String[10];
        String[] remarks = new String[10];
        //        String[] lrNumber1 = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        String[] city = new String[10];

        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            WmsTO wmsTO = new WmsTO();

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/POD");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        Date now = new Date();
                        String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                        String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                        fPart = (FilePart) partObj;
                        uploadedFileName[j] = fPart.getFileName();
                        System.out.println("fPart.getFileName() = " + fPart.getFileName());

                        if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {
                            System.out.println("partObj.getName() = " + partObj.getName());
                            String[] splitFileName = uploadedFileName[j].split("\\.");
                            fileSavedAs = uploadedFileName[j];
                            System.out.println("fileSavedAs = " + fileSavedAs);
                            fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath[j] = tempServerFilePath + "/" + fileSaved[j];
                            actualFilePath = actualServerFilePath + "/" + tempFilePath;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath[j]));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            //                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            //                            String part1 = parts.replace("\\", "");
                        }

                        System.out.println("fileName..." + fileName);
                        j++;
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("subLrId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            subLrId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("dispatchid")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            dispatchid1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("lrnNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            lrnNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("plannedTime")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            plannedTime1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("deliveryDate")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            plannedDate1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("dispatchDetailId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            dispatchDetailId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("dealerId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            dealerName = paramPart.getStringValue();
                        }

                    }
                }
            }
            String whId = (String) session.getAttribute("hubId");
            wmsTO.setWhId(whId);
            int index = j;
            String[] file = new String[index];
            System.out.println(" " + file);

            String[] saveFile = new String[index];;

            int status = 0;

            //file upload
            String podName1 = request.getParameter("podName1");
            wmsTO.setPodName1(podName1);
            String podName2 = request.getParameter("podName2");
            wmsTO.setPodName2(podName2);
            String podName3 = request.getParameter("podName3");
            wmsTO.setPodName3(podName3);
            String remarks1 = request.getParameter("remarks1");
            wmsTO.setRemarks1(remarks1);
            String remarks2 = request.getParameter("remarks2");
            wmsTO.setRemarks2(remarks2);
            String remarks3 = request.getParameter("remarks3");
            wmsTO.setRemarks3(remarks3);
            wmsTO.setLrnNo(lrnNo1);
            wmsTO.setDispatchDetailId(dispatchDetailId1);
            wmsTO.setPlannedDate(plannedDate1);
            wmsTO.setDeliveryDate(plannedDate1);
            wmsTO.setPlannedTime(plannedTime1);
            wmsTO.setDispatchid(dispatchid1);
            wmsTO.setSubLrId(subLrId1);
            wmsTO.setDealerId(dealerName);
            //             String[] mfrids = vendorCommand.getMfrids();
            //             String[] mfrids = vendorCommand.getMfrids();
            //             System.out.println("mfrids222"+mfrids);
            //  String[] selectedIndex = vendorCommand.getSelectedIndex();
            String selectedIndex = "0";
            //            System.out.println("selectedIndexpp"+selectedIndex);

            System.out.println("j = " + j);
            System.out.println("file = " + file.length);
            System.out.println("tempFilePath[0] = " + tempFilePath[0]);
            System.out.println("tempFilePath[1] = " + tempFilePath[1]);

            for (int x = 0; x < j; x++) {
                System.out.println("tempFilePath[x] = " + tempFilePath[x]);
                file[x] = tempFilePath[x];
                System.out.println("saveFile[x] = " + fileSaved[x]);
                saveFile[x] = fileSaved[x];
            }
            vendorId = wmsBP.processInsertPODDetails(wmsTO, selectedIndex, userId, file, saveFile, remarks);
            int updateConnectLrNo = wmsBP.updateConnectLrNo(wmsTO);
            request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));
            mv = connectPodUpload(request, response, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView connectInvoiceReport(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wms.setFromDate(fromDate);
            wms.setToDate(toDate);
            wms.setUserId(userId + "");
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            ArrayList whList = new ArrayList();
            whList = wmsBP.getWareHouse(wms);
            request.setAttribute("whList", whList);

            String status = request.getParameter("newStatus");
            wms.setStatus(status);

            if ("ExportExcel".equals(param)) {
                path = "content/wms/connectInvoiceReportExcel.jsp";
            } else if ("view".equals(param)) {
                String invoiceId = request.getParameter("invoiceId");
                wms.setInvoiceId(invoiceId);
                ArrayList getConnectInvoiceProductDetails = wmsBP.getConnectInvoiceProductDetails(wms);
                request.setAttribute("getConnectInvoiceProductDetails", getConnectInvoiceProductDetails);
                path = "content/wms/viewConnectProductDetails.jsp";
            } else if ("update".equals(param)) {
                String invoiceId = request.getParameter("invoiceId");
                wms.setInvoiceId(invoiceId);
                String appointmentDate = request.getParameter("appointment");
                wms.setAppointmentDate(appointmentDate);
                int updateConnectInvoiceUpdate = wmsBP.updateConnectInvoiceUpdate(wms);
                if (updateConnectInvoiceUpdate > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Appointment Date Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Appointment Date Update Failed");
                }
                path = "content/wms/connectInvoiceReport.jsp";
            } else if ("cancel".equals(param)) {
                String invoiceId = request.getParameter("invoiceId");
                wms.setInvoiceId(invoiceId);
                int cancelConnectInvoice = wmsBP.cancelConnectInvoice(wms);
                path = "content/wms/connectInvoiceReport.jsp";
            } else {
                path = "content/wms/connectInvoiceReport.jsp";
            }
            ArrayList connectInvoiceList = new ArrayList();
            connectInvoiceList = wmsBP.getConnectInvoiceReport(wms);
            request.setAttribute("connectInvoiceReport", connectInvoiceList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView connectBillReceiving(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            wms.setUserId(userId + "");
            wms.setWhId(hubId);

            ArrayList vendorList = new ArrayList();
            vendorList = wmsBP.getConnectVendorList(wms);
            System.out.println("ControllerSize=====" + vendorList.size());
            request.setAttribute("vendorList", vendorList);

            String param = request.getParameter("param");
            if ("save".equals(param)) {
                String billId = request.getParameter("billId");
                String billNo = request.getParameter("billNo");
                String billDate = request.getParameter("billDate");
                String refNo = request.getParameter("referenceNo");
                String freightAmount = request.getParameter("baseFreight");
                String unloadingCharge = request.getParameter("unloadingCharge");
                String haltingCharge = request.getParameter("haltingCharge");
                String otherCharges = request.getParameter("otherCharges");
                String gstCharge = request.getParameter("gstCharge");
                String billAmount = request.getParameter("billAmount");
                String vendorId = request.getParameter("vendorId");
                String periodFrom = request.getParameter("periodFrom");
                String periodTo = request.getParameter("periodTo");
                String dateOfReceived = request.getParameter("dateOfReceived");
                String dateOfEntry = request.getParameter("dateOfEntry");
                String receivedBy = request.getParameter("receivedBy");
                wms.setBillId(billId);
                wms.setBillNo(billNo);
                wms.setBillDate(billDate);
                wms.setReferenceNo(refNo);
                wms.setFreightAmount(freightAmount);
                wms.setUnloadingAmt(unloadingCharge);
                wms.setHaltingAmt(haltingCharge);
                wms.setOtherAmt(otherCharges);
                wms.setGstAmt(gstCharge);
                wms.setBillAmt(billAmount);
                wms.setVendorId(vendorId);
                wms.setPeriodFrom(periodFrom);
                wms.setPeriodTo(periodTo);
                wms.setDateOfReceived(dateOfReceived);
                wms.setDateOfEntry(dateOfEntry);
                wms.setReceivedBy(receivedBy);
                int connectSaveBillReceiving = wmsBP.connectSaveBillReceiving(wms);
                if (connectSaveBillReceiving > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Billing Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Billing Update Failed");
                }
            }
            ArrayList connectGetBillReceiving = wmsBP.connectGetBillReceiving(wms);
            request.setAttribute("connectGetBillReceiving", connectGetBillReceiving);
            path = "content/wms/connectBillReceiving.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView rpOrderDetails(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            if ("view".equals(param)) {
                String orderId = request.getParameter("orderId");
                wms.setOrderId(orderId);
                ArrayList getRpOrderDetails = wmsBP.getRpOrderDetails(wms);
                request.setAttribute("orderDetails", getRpOrderDetails);
                path = "content/wms/rpOrderDetails.jsp";
            } else {
                path = "content/wms/rpOrderMaster.jsp";
            }
            ArrayList getRpOrderMaster = wmsBP.getRpOrderMaster(wms);
            request.setAttribute("orderMaster", getRpOrderMaster);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView connectIndentRequest(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            wms.setStatus("0");
            if ("save".equals(param)) {
                String indentDate = request.getParameter("indentDate");
                String fromLocation = request.getParameter("fromLocation");
                String toLocation = request.getParameter("toLocation");
                String partyName = request.getParameter("partyName");
                String vehicleType = request.getParameter("vehicleType");
                String deliveryType = request.getParameter("deliveryType");
                String deliveryDate = request.getParameter("deliveryDate");
                String appointmentDate = request.getParameter("appointmentDate");
                String requiredQuantity = request.getParameter("requiredQty");
                String boxQty = request.getParameter("boxQty");
                String indentId = request.getParameter("indentId");
                wms.setIndentDate(indentDate);
                wms.setBoxQty(boxQty);
                wms.setFromLocation(fromLocation);
                wms.setToLocation(toLocation);
                wms.setPartyName(partyName);
                wms.setVehicleType(vehicleType);
                wms.setDeliveryType(deliveryType);
                wms.setAppointmentDate(appointmentDate);
                wms.setQty(requiredQuantity);
                wms.setDeliveryDate(deliveryDate);
                wms.setIndentId(indentId);
                int saveIndentRequest = wmsBP.saveIndentRequest(wms);
                if (saveIndentRequest > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Indent Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Indent Update Failed");
                }

            }
            ArrayList getVehicleTypeList = new ArrayList();
            getVehicleTypeList = wmsBP.getVehicleTypeList();
            System.out.println("getVehicleTypeList=====" + getVehicleTypeList.size());
            request.setAttribute("getVehicleTypeList", getVehicleTypeList);

            ArrayList whList = wmsBP.getWareHouse(wms);
            request.setAttribute("whList", whList);

            ArrayList connectIndentRequest = wmsBP.connectIndentRequest(wms);
            request.setAttribute("connectIndentRequest", connectIndentRequest);
            path = "content/wms/connectIndentRequest.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView connectIndentConfirm(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            wms.setStatus("5");
            if ("update".equals(param)) {
                String indentId = request.getParameter("indentId");
                wms.setIndentId(indentId);
                path = "content/wms/connectIndentConfirmDetails.jsp";
            } else if ("save".equals(param)) {
                String indentId = request.getParameter("indentId");
                wms.setIndentId(indentId);
                String plant = request.getParameter("plant");
                String vehicleType = request.getParameter("vehicleType");
                String fromLocation = request.getParameter("fromLocation");

                String[] vehicleNos = request.getParameterValues("vehicleNo");
                String[] driverMobile = request.getParameterValues("driverMobile");
                String[] driverName = request.getParameterValues("driverName");
                String[] deliveryDate = request.getParameterValues("deliveryDate");
                wms.setVehicleNos(vehicleNos);
                wms.setDriverMobileNos(driverMobile);
                wms.setDriverNames(driverName);
                wms.setDeliveryDates(deliveryDate);
                wms.setPlantCode(plant);
                wms.setFromLocation(fromLocation);
                wms.setVehicleType(vehicleType);
                wms.setType("1");
                int insertConnectIndentDetails = wmsBP.insertConnectIndentDetails(wms);
                if (insertConnectIndentDetails > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Indent Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Indent Update Failed");
                }
                path = "content/wms/connectIndentConfirm.jsp";
            } else {
                path = "content/wms/connectIndentConfirm.jsp";
            }
            ArrayList connectIndentRequest = wmsBP.connectIndentRequest(wms);
            request.setAttribute("connectIndentRequest", connectIndentRequest);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView connectIndentUpdate(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            String indentId = request.getParameter("indentId");
            wms.setIndentId(indentId);
            wms.setStatus("6");
            if ("update".equals(param)) {
                ArrayList connectIndentRequestDetails = new ArrayList();
                connectIndentRequestDetails = wmsBP.connectIndentRequestDetails(wms);
                request.setAttribute("connectIndentRequestDetails", connectIndentRequestDetails);
                path = "content/wms/connectIndentUpdateDetails.jsp";
            } else if ("save".equals(param)) {
                String[] tatId = request.getParameterValues("tatId");
                String[] indentDetailId = request.getParameterValues("indentDetailId");
                String[] inDate = request.getParameterValues("inDate");
                String[] inTime = request.getParameterValues("inTime");
                String[] selectedStatus = request.getParameterValues("selectedStatus");
                int updateConnectIndentRequestDetails = wmsBP.updateConnectIndentRequestDetails(wms, tatId, indentDetailId, inDate, inTime, selectedStatus);
                if (updateConnectIndentRequestDetails > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Indent Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Indent Update Failed");
                }
                path = "content/wms/connectIndentUpdate.jsp";
            } else {
                path = "content/wms/connectIndentUpdate.jsp";
            }
            ArrayList connectIndentRequest = wmsBP.connectIndentRequest(wms);
            request.setAttribute("connectIndentRequest", connectIndentRequest);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView rpStockDetails(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            String stockId = request.getParameter("stockId");
            wms.setStockId(stockId);
            if ("view".equals(param)) {
                ArrayList rpStockDetailsView = new ArrayList();
                rpStockDetailsView = wmsBP.rpStockDetailsView(wms);
                request.setAttribute("rpStockDetailsView", rpStockDetailsView);
                path = "content/wms/rpStockDetailsView.jsp";
            } else {
                path = "content/wms/rpStockDetails.jsp";
            }
            ArrayList rpStockDetails = wmsBP.rpStockDetails(wms);
            request.setAttribute("rpStockDetails", rpStockDetails);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView rpTripPlanning(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            wms.setStatus("0");
            String stockId = request.getParameter("stockId");
            wms.setStockId(stockId);
            ArrayList rpOrderMaster = wmsBP.getRpOrderMaster(wms);
            request.setAttribute("rpOrderMaster", rpOrderMaster);
            path = "content/wms/rpTripPlanning.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView imMaterialPO(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String whId = request.getParameter("whId");
        String hubId = (String) session.getAttribute("hubId");

        WmsTO wms = new WmsTO();

        try {
            String param = request.getParameter("param");
            if ("".equals(whId) || whId == null) {
                whId = hubId;
            }
            wms.setUserId(userId + "");
            wms.setWhId(whId);
            request.setAttribute("whId", whId);
            if ("save".equals(param)) {
                whId = request.getParameter("whId");
                wms.setWhId(whId);
                String custId = request.getParameter("custId");
                String poDate = request.getParameter("poDate");
                String expectedDeliveryDate = request.getParameter("deliveryDate");
                String totQty = request.getParameter("totQty");
                String remarks = request.getParameter("remarks");
                String[] materialId = request.getParameterValues("materialId");
//                String[] materialId = request.getParameterValues("materialIdTemp");
                String[] materialName = request.getParameterValues("materialName");
                String[] materialDescription = request.getParameterValues("materialDescription");
                String[] poQty = request.getParameterValues("poQty");
                String[] pending = request.getParameterValues("pending");
                String[] id = request.getParameterValues("idd");
                String[] uom = request.getParameterValues("uom");
                String[] unitPrice = request.getParameterValues("unitPrice");
                String[] price = request.getParameterValues("price");
                String tax = request.getParameter("tax");
                wms.setCustId(custId);
                wms.setPoDate(poDate);
                wms.setDeliveryDate(expectedDeliveryDate);
                wms.setRemarks(remarks);
                wms.setTotQty(totQty);
                wms.setItemIds(materialId);
                wms.setTaxVol(tax);

                int insertImPoOrder = wmsBP.insertImPoOrder(wms, materialName, materialDescription, poQty, pending, id, uom, unitPrice, price);
                wms.setPoId(insertImPoOrder + "");

                int instaPoSendMail = wmsBP.instaPoSendMail(wms, userId, whId);

                if (insertImPoOrder > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "PO Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "PO Update Failed");
                }
            }
            ArrayList imMaterialMasterDetails = new ArrayList();
            imMaterialMasterDetails = wmsBP.imMaterialMasterDetails(wms);
            request.setAttribute("materialMaster", imMaterialMasterDetails);
            ArrayList imMaterialCustomerMaster = new ArrayList();
            imMaterialCustomerMaster = wmsBP.imMaterialCustomerMaster(wms);
            request.setAttribute("customerMaster", imMaterialCustomerMaster);

            ArrayList imMaterialIndent = new ArrayList();
            imMaterialIndent = wmsBP.imMaterialIndent(wms);
            request.setAttribute("imMaterialIndent", imMaterialIndent);

            ArrayList whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);

            path = "content/wms/imMaterialPO.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView imMaterialGrn(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            if ("save".equals(param)) {
                String grnDate = request.getParameter("grnDate");
                String grnTime = request.getParameter("grnTime");
                String invoiceNo = request.getParameter("invoiceNo");
                String invoiceDate = request.getParameter("invoiceDate");
                String totalQty = request.getParameter("totQty");
                String remarks = request.getParameter("remarks");
                String[] materialId = request.getParameterValues("materialId");
                String[] poQty = request.getParameterValues("poQty");
                String[] qty = request.getParameterValues("receivedQty");
                String[] poStatus = request.getParameterValues("poStatus");
                String[] poId = request.getParameterValues("poNo");
                String[] id = request.getParameterValues("detailsid");
                String[] good = request.getParameterValues("good");
                String[] damage = request.getParameterValues("damage");
                String[] shortage = request.getParameterValues("shortage");
                String[] balance = request.getParameterValues("balance");
                wms.setGrnDate(grnDate);
                wms.setGrnTime(grnTime);
                wms.setInvoiceNo(invoiceNo);
                wms.setInvoiceDate(invoiceDate);
                wms.setRemarks(remarks);
                wms.setTotQty(totalQty);
                wms.setItemIds(materialId);
                int insertImMaterialGrn = wmsBP.insertImMaterialGrn(wms, poQty, qty, poStatus, poId, id, good, damage, shortage, balance);
                int instaGrnSendMail = wmsBP.instaGrnSendMail(wms, userId, hubId);

                if (insertImMaterialGrn > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "GRN Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "GRN Update Failed");
                }
            }
            wms.setWhId(hubId);

            ArrayList imMaterialPo = new ArrayList();
            imMaterialPo = wmsBP.imMaterialPo(wms);
            request.setAttribute("imMaterialPo", imMaterialPo);

            ArrayList imMaterialMasterDetails = new ArrayList();
            imMaterialMasterDetails = wmsBP.imMaterialMasterDetails(wms);
            request.setAttribute("materialMaster", imMaterialMasterDetails);
            ArrayList imMaterialPOView = new ArrayList();
            imMaterialPOView = wmsBP.imMaterialPOView(wms);
            wms.setStatus("4");
            request.setAttribute("imMaterialPO", imMaterialPOView);
            path = "content/wms/imMaterialGrn.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView imMaterialPOView(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        int roleId = (Integer) session.getAttribute("RoleId");
        request.setAttribute("roleId", roleId);
        WmsTO wms = new WmsTO();
        try {
            String whId = request.getParameter("whId");
            if(whId==null||"".equals(whId)){
                whId = hubId;
            }
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(whId);
            ArrayList imMaterialPOView = new ArrayList();
            imMaterialPOView = wmsBP.imMaterialPOView(wms);
            request.setAttribute("poView", imMaterialPOView);
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            Date date = new Date();
            SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
            String curDate = sd.format(date);
            String monthStartDate = "01-"+curDate.split("-")[1]+"-"+curDate.split("-")[2];
            if(fromDate==null||"".equals(fromDate)){
                fromDate = monthStartDate;
            }
            if(toDate==null||"".equals(toDate)){
                toDate = curDate;
            }
            wms.setFromDate(fromDate);
            wms.setToDate(toDate);
            request.setAttribute("fromDate",fromDate);
            request.setAttribute("toDate",toDate);
            request.setAttribute("whId",whId);
            if ("view".equals(param)) {
                String poId = request.getParameter("poId");
                wms.setPoId(poId);
                ArrayList getImMaterialPOViewDetails = wmsBP.getImMaterialPOViewDetails(wms);
                request.setAttribute("poViewDetails", getImMaterialPOViewDetails);
                String authStatus = wmsBP.getAuthStatus(wms);
                request.setAttribute("authStatus", authStatus);
                path = "content/wms/imMaterialPOViewDetails.jsp";
            } else if ("print".equals(param)) {
                String poId = request.getParameter("poId");
                wms.setPoId(poId);
                ArrayList getImMaterialPOViewDetails = wmsBP.getImMaterialPOViewDetails(wms);
                request.setAttribute("poViewDetails", getImMaterialPOViewDetails);
                path = "content/wms/imMaterialPoPrint.jsp";
            } else if ("approve".equals(param)) {
                String poId = request.getParameter("poId");
                System.out.println("poId");
                wms.setPoId(poId);
                int updateApproveStatus = wmsBP.updateApproveStatus(wms, userId);
                imMaterialPOView = new ArrayList();
                imMaterialPOView = wmsBP.imMaterialPOView(wms);
                request.setAttribute("poView", imMaterialPOView);
                path = "content/wms/imMaterialPOView.jsp";
            } else if ("auth".equals(param)) {
                String poId = request.getParameter("poId");
                wms.setPoId(poId);
                int updateAuthStatus = wmsBP.updateAuthStatus(wms, userId);

                imMaterialPOView = new ArrayList();
                imMaterialPOView = wmsBP.imMaterialPOView(wms);
                request.setAttribute("poView", imMaterialPOView);

                path = "content/wms/imMaterialPOView.jsp";
            } else {
                path = "content/wms/imMaterialPOView.jsp";
            }
            ArrayList whList = new ArrayList();
            whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView imCustomerMasterPackMat(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            if ("save".equals(param)) {
                String customerId = request.getParameter("customerId");
                String customerName = request.getParameter("customerName");
                String customerCode = request.getParameter("customerCode");
                String address = request.getParameter("address");
                String address1 = request.getParameter("address1");
                String phoneNo = request.getParameter("mobileNo");
                String phoneNo1 = request.getParameter("mobileNo2");
                String city = request.getParameter("city");
                String state = request.getParameter("state");
                String pincode = request.getParameter("pincode");
                String gstNo = request.getParameter("gstNo");
                String activeType = request.getParameter("activeInd");
                wms.setCustId(customerId);
                wms.setCustName(customerName);
                wms.setCustCode(customerCode);
                wms.setAddress(address);
                wms.setAddress1(address1);
                wms.setMobileNo(phoneNo);
                wms.setPhoneNumber(phoneNo1);
                wms.setCity(city);
                wms.setStateId(state);
                wms.setPincode(pincode);
                wms.setGstinNumber(gstNo);
                wms.setActiveType(activeType);
                int insertMaterialCustomerMaster = wmsBP.insertImMaterialCustomerMaster(wms);
                if (insertMaterialCustomerMaster > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Customer Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Customer Update Failed");
                }
            }
            ArrayList customerMaster = new ArrayList();
            customerMaster = wmsBP.getImMaterialCustomerMaster(wms);
            request.setAttribute("customerMaster", customerMaster);
            path = "content/wms/imCustomerMasterPackMat.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView imMaterialMaster(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            if ("save".equals(param)) {
                String materialName = request.getParameter("materialName");
                String materialCode = request.getParameter("materialCode");
                String materialDescription = request.getParameter("materialDescription");
                String materialId = request.getParameter("materialId");
                String size = request.getParameter("size");
                String uom = request.getParameter("uom");
                String qty = request.getParameter("qty");
                String activeInd = request.getParameter("activeInd");
                wms.setActiveType(activeInd);
                wms.setMaterialId(materialId);
                wms.setMaterial(materialName);
                wms.setMaterialCode(materialCode);
                wms.setMaterialDescription(materialDescription);
                wms.setSize(size);
                wms.setUnit(uom);
                wms.setQty(qty);
                int insertImMaterialMaster = wmsBP.insertImMaterialMaster(wms);
                if (insertImMaterialMaster > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Material Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Material Update Failed");
                }
            }
            wms.setStatus("100");
            ArrayList materialGroup = wmsBP.getImMaterialGroup(wms);
            request.setAttribute("materialGroup", materialGroup);
            ArrayList materialUom = wmsBP.getImMaterialUom(wms);
            request.setAttribute("materialUom", materialUom);
            ArrayList imMaterialMasterDetails = new ArrayList();
            imMaterialMasterDetails = wmsBP.imMaterialMasterDetails(wms);
            request.setAttribute("materialMaster", imMaterialMasterDetails);
            path = "content/wms/imMaterialMaster.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView rpTripPlanningDetails(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        OpsTO opsTO = new OpsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            String[] orderId = request.getParameterValues("orderId");
            String[] selectedStatus = request.getParameterValues("selectedStatus");
            wms.setOrderIdE(orderId);
            wms.setSelectedIndex(selectedStatus);

            String wareHouseLatLong = opsBP.getwarehouseLatLong(userId);
            System.out.println("wareHouseLatLong====" + wareHouseLatLong);

            String[] temp = null;
            temp = wareHouseLatLong.split("~");
            String wareHouseLat = temp[0];
            String wareHouseLong = temp[1];
            System.out.println("wareHouseLat==" + wareHouseLat + " ,wareHouseLong=" + wareHouseLong);

            ArrayList vendorList = wmsBP.getVendorList();
            System.out.println("vendorList.size() = " + vendorList.size());
            request.setAttribute("vendorList", vendorList);
            opsTO.setCustId("1137");
            opsTO.setUserId(userId);
            ArrayList deliveryExecutiveList = opsBP.deliveryExecutiveList(opsTO);
            System.out.println("deliveryExecutiveList.size() = " + deliveryExecutiveList.size());
            request.setAttribute("deliveryExecutiveList", deliveryExecutiveList);

            ArrayList rpTripPlanningDetails = wmsBP.rpTripPlanningDetails(wms);
            request.setAttribute("planningList", rpTripPlanningDetails);
            path = "content/wms/rpTripPlanningDetails.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView rpStockTransfer(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            String stockId = request.getParameter("stockId");
            wms.setStockId(stockId);
            ArrayList getProductList = new ArrayList();
            getProductList = wmsBP.getRpProductList(wms);
            request.setAttribute("productList", getProductList);
            ArrayList whList = new ArrayList();
            whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);
            if ("view".equals(param)) {
                path = "content/wms/rpStockTransferView.jsp";
            } else if ("save".equals(param)) {
                String[] serialNos = request.getParameterValues("serialNos");
                String[] serialIds = request.getParameterValues("serialIds");
                String[] stockIds = request.getParameterValues("stockId");
                String[] productIds = request.getParameterValues("productIds");
                String fromWhId = request.getParameter("fromWhId");
                String toWhId = request.getParameter("toWhId");
                String vehicleNo = request.getParameter("vehicleNo");
                String driverMobile = request.getParameter("driverMobile");
                String driverName = request.getParameter("driverName");
                wms.setVehicleNo(vehicleNo);
                wms.setMobileNo(driverMobile);
                wms.setDriverName(driverName);
                wms.setWarehouseId(toWhId);
//                wms.setWhId(fromWhId);
                wms.setSerialIds(serialIds);
                wms.setSerialNos(serialNos);
                wms.setStockIds(stockIds);
                wms.setItemIds(productIds);
                int status = wmsBP.insertRpStockTransferDetails(wms);
                path = "content/wms/rpStockTransfer.jsp";
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Stock Transfer Done");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Stock Transfer Failed");
                }
            } else {
                path = "content/wms/rpStockTransfer.jsp";
            }
            ArrayList rpStockTransfer = wmsBP.rpStockTransfer(wms);
            request.setAttribute("rpStockTransfer", rpStockTransfer);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView rpStockReceive(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            String stockId = request.getParameter("stockId");
            wms.setStockId(stockId);
            wms.setStatus("0");
            if ("view".equals(param)) {
                String transferId = request.getParameter("transferId");
                wms.setTransferId(transferId);
                ArrayList rpStockReceiveConfirm = new ArrayList();
                rpStockReceiveConfirm = wmsBP.rpStockTransferView(wms);
                request.setAttribute("rpStockReceiveView", rpStockReceiveConfirm);
                path = "content/wms/rpStockReceiveConfirm.jsp";
            } else if ("save".equals(param)) {
                String[] serialIds = request.getParameterValues("serialId");
                String fromWhId = request.getParameter("fromWhId");
                String toWhId = request.getParameter("toWhId");
                String transferId = request.getParameter("transferId");
                String[] serialNo = request.getParameterValues("serialNo");
                String[] productId = request.getParameterValues("productId");
                String[] transferDetailId = request.getParameterValues("transferDetailId");
                String[] selectedStatus = request.getParameterValues("selectedStatus");
                wms.setSerialIds(serialIds);
                wms.setFromWhId(fromWhId);
                wms.setToWhId(toWhId);
                wms.setItemIds(productId);
                wms.setSerialNos(serialNo);
                wms.setSelectedIndex(selectedStatus);
                wms.setTransferId(transferId);
                int status = wmsBP.saveStockReceive(wms, transferDetailId);
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Stock Receive Success");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Stock Receive Failed");
                }
                path = "content/wms/rpStockReceive.jsp";
            } else {
                path = "content/wms/rpStockReceive.jsp";
            }
            ArrayList rpStockReceive = wmsBP.rpStockReceive(wms);
            request.setAttribute("rpStockReceive", rpStockReceive);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView apiFetchTrip(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String apiTripId = request.getParameter("apiTripId");
            request.setAttribute("apiTripId", apiTripId);
            path = "content/wms/apiFetchTrip.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterConnectLrList(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        int roleId = (Integer) session.getAttribute("RoleId");
        System.out.println("whId    " + whId);
        int updateDcGenerate = 0;
        try {
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wmsTO.setRoleId(roleId + "");
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);
            wmsTO.setStatus("2");
            wmsTO.setType("5");

            ArrayList getWareHouse = new ArrayList();
            getWareHouse = wmsBP.getWareHouse(wmsTO);
            request.setAttribute("getWareHouse", getWareHouse);

            ArrayList getConnectVendorList = new ArrayList();
            getConnectVendorList = wmsBP.getConnectVendorList(wmsTO);
            request.setAttribute("vendorList", getConnectVendorList);

            String param = request.getParameter("param");
            String vendorId = request.getParameter("vendorId");
            System.out.println("vendorId------" + vendorId);
            String dispatchId = request.getParameter("dispatchId");
            System.out.println("dispatchId------" + dispatchId);

            if ("update".equals(param)) {
                wmsTO.setDispatchId(dispatchId);
                wmsTO.setVendorId(vendorId);
                int updateConnectLrVendor = wmsBP.updateConnectLrVendor(wmsTO);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            ArrayList getConnectVendorDetails = wmsBP.getDispatchDetails(wmsTO);
            request.setAttribute("getConnectVendorDetails", getConnectVendorDetails);
            path = "content/wms/connectVendorChange.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView connectDeTat(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        String empName = request.getParameter("empName");
        String date = request.getParameter("date");
        String time = request.getParameter("inTime");
        String empId = request.getParameter("empId");
        String desigId = request.getParameter("desigId");

        OpsTO ops = new OpsTO();
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            ops.setWhId(hubId);
            ops.setUserId(userId);
            wms.setDesigId(desigId);

            wms.setEmpName(empName);
            wms.setEmpId(empId);
            wms.setInTime(time);
            wms.setDate(date);

            ArrayList designationName = new ArrayList();
            designationName = wmsBP.designationName(wms);
            request.setAttribute("designationName", designationName);

            ArrayList deliveryExecutiveDetails = new ArrayList();
            if ("insert".equals(param)) {
                int insertDeTat = wmsBP.insertDeTat(wms);
                if (insertDeTat > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Tat Inserted Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Tat Insert Failed");
                }
                deliveryExecutiveDetails = wmsBP.deliveryExecutiveDetails(wms);
                request.setAttribute("deliveryExecutiveDetails", deliveryExecutiveDetails);
            } else if ("update".equals(param)) {
                String tatId = request.getParameter("tatId");
                String outTime = request.getParameter("outTime");
                wms.setTatId(tatId);
                wms.setOutTime(outTime);
                int updateDeTat = wmsBP.updateDeTat(wms);
                if (updateDeTat > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Tat Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Tat Update Failed");
                }
            }

            deliveryExecutiveDetails = wmsBP.deliveryExecutiveDetails(wms);
            request.setAttribute("deliveryExecutiveDetails", deliveryExecutiveDetails);

            path = "content/wms/connectDeTat.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView connectAddStorage(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();

        WmsTO wmsTO = new WmsTO();
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");

        try {
            String storageName = "";
            String binName = "";
            String rackName = "";

            String storageId = "";
            String rackId = "";
            String binId = "";

            storageName = request.getParameter("storageName");
            rackName = request.getParameter("rackName");
            binName = request.getParameter("binName");

            storageId = request.getParameter("storageIdval");
            rackId = request.getParameter("rackIdval");
            binId = request.getParameter("binIdval");

            wmsTO.setStorageName(storageName);
            wmsTO.setRackName(rackName);
            wmsTO.setBinName(binName);
            wmsTO.setBinId(binId);
            wmsTO.setRackId(rackId);
            wmsTO.setStorageId(storageId);

            String param = request.getParameter("param");

            if ("save3".equals(param)) {
                if ((binId != null && !"".equals(binId))) {
                    int updateStorageMaster = wmsBP.updateStorageMaster(wmsTO);
                } else {
                    int insertStorageMaster = wmsBP.insertStorageMaster(wmsTO);
                }
            }

            if ("save2".equals(param)) {
                System.out.println("ins if");
                if ((rackId != null && !"".equals(rackId))) {
                    int updateStorageMaster = wmsBP.updateStorageMaster(wmsTO);
                } else {
                    int insertStorageMaster = wmsBP.insertStorageMaster(wmsTO);
                }
            }
            if ("save1".equals(param)) {
                System.out.println("storgeId    " + storageId + "oiii");
                if ((storageId != null && !"".equals(storageId))) {
                    int updateStorageMaster = wmsBP.updateStorageMaster(wmsTO);
                } else {
                    int insertStorageMaster = wmsBP.insertStorageMaster(wmsTO);
                }
            }

            ArrayList getStorageDetails = new ArrayList();
            getStorageDetails = wmsBP.getStorageDetails();
            request.setAttribute("getStorageDetails", getStorageDetails);

            ArrayList getRackDetails = new ArrayList();
            getRackDetails = wmsBP.getRackDetails2();
            request.setAttribute("getRackDetails", getRackDetails);

            ArrayList getBinDetails = new ArrayList();
            getBinDetails = wmsBP.getBinkDetails2();
            request.setAttribute("getBinDetails", getBinDetails);

            path = "content/wms/AddStorage.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public void checkBinName(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            String binName = request.getParameter("binName");
            String binNameAlreadyExists = "";
            binNameAlreadyExists = wmsBP.binNameExists(binName);
            System.out.println("serialnoexist==========" + binNameAlreadyExists);
            if ("0".equals(binNameAlreadyExists)) {

            } else if ("1".equals(binNameAlreadyExists)) {
                existsStatus = "Already Exists  Bin Name";
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(existsStatus);
            writer.close();
            request.setAttribute("binAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public void checkStorageName(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            String storageName = request.getParameter("storageName");
            String storageNameAlreadyExists = "";
            storageNameAlreadyExists = wmsBP.storageNameAlreadyExists(storageName);
            System.out.println("rackNameAlreadyExists==========" + storageNameAlreadyExists);
            if ("0".equals(storageNameAlreadyExists)) {

            } else if ("1".equals(storageNameAlreadyExists)) {
                existsStatus = "Already Exists  Storage Name";
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(existsStatus);
            writer.close();
            request.setAttribute("storageNameAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public void checkRackName(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTO = new WmsTO();
        try {
            String rackName = request.getParameter("rackName");
            String rackNameAlreadyExists = "";
            rackNameAlreadyExists = wmsBP.rackNameAlreadyExists(rackName);
            System.out.println("rackNameAlreadyExists==========" + rackNameAlreadyExists);
            if ("0".equals(rackNameAlreadyExists)) {

            } else if ("1".equals(rackNameAlreadyExists)) {
                existsStatus = "Already Exists  Rack Name";
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(existsStatus);
            writer.close();
            request.setAttribute("rackNameAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public void getImDealerMaster(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        WmsTO wms2 = new WmsTO();
        try {
            wms.setWhId(whId);
            wms.setUserId(userId + "");
            JSONArray json = new JSONArray();
            ArrayList getImDealerMaster = wmsBP.getImDealerList(wms);
            Iterator itr = getImDealerMaster.iterator();
            while (itr.hasNext()) {
                wms2 = (WmsTO) itr.next();
                JSONObject js = new JSONObject();
                js.put("custId", wms2.getDealerId());
                js.put("custName", wms2.getDealerName());
                js.put("qty", wms2.getQty());
                json.put(js);
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("application/json");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(json);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public ModelAndView instaMartGrnIssue(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            if ("save".equals(param)) {
                String issueDate = request.getParameter("issueDate");
                String issueTime = request.getParameter("issueTime");

                String totalQty = request.getParameter("totQty");
                String remarks = request.getParameter("remarks");
                String receivedBy = request.getParameter("receivedBy");

                String[] materialId = request.getParameterValues("materialId");
                String[] materialName = request.getParameterValues("materialName");
                String[] skuCode = request.getParameterValues("skuCode");
                String[] uom = request.getParameterValues("uom");
                String[] stockQty = request.getParameterValues("stockQty");
                String[] issueQty = request.getParameterValues("issueQty");

                System.out.println(Arrays.toString(skuCode));

                wms.setIssueDate(issueDate);
                wms.setIssueTime(issueTime);
                wms.setRemarks(remarks);
                wms.setReceivedBy(receivedBy);
                wms.setTotQty(totalQty);
                wms.setItemIds(materialId);

                int insertImMaterialGrnIssue = wmsBP.insertImMaterialGrnIssue(wms, skuCode, uom, stockQty, issueQty);
                if (insertImMaterialGrnIssue > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "GRN issue Updated Successfully");
                    wms.setIssueId(insertImMaterialGrnIssue + "");
                    ArrayList getImMaterialIssueMaster = wmsBP.getImMaterialIssueMaster(wms);
                    request.setAttribute("issueMaster", getImMaterialIssueMaster);
                    ArrayList getImMaterialIssueDetails = wmsBP.getImMaterialIssueDetails(wms);
                    request.setAttribute("issueDetails", getImMaterialIssueDetails);
                    path = "content/wms/printInstaMartGrnIssue.jsp";
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "GRN issue Update Failed");
                    path = "content/wms/instaMartGrnIssue.jsp";

                }
            } else {
                path = "content/wms/instaMartGrnIssue.jsp";
            }
            wms.setWhId(hubId);
            wms.setStatus("200");
            ArrayList imMaterialMasterDetails = new ArrayList();
            imMaterialMasterDetails = wmsBP.imMaterialMasterDetails(wms);
            request.setAttribute("materialMaster", imMaterialMasterDetails);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView instaMartGrnIssueView(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        String materialId = request.getParameter("materialId");
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            wms.setFromDate(fromDate);
            wms.setToDate(toDate);
            wms.setMaterialId(materialId);

            String issueId = request.getParameter("issueId");
            wms.setIssueId(issueId);
            if ("print".equals(param)) {
                path = "content/wms/printInstaMartGrnIssue.jsp";
            } else if ("view".equals(param)) {
                path = "content/wms/imGrnIssueViewDetails.jsp";
            } else if ("update".equals(param)) {
                path = "content/wms/imGrnIssueUpdate.jsp";
            } else if ("save".equals(param)) {
                String[] floorQty = request.getParameterValues("floorQty");
                String[] usedQty = request.getParameterValues("usedQty");
                String[] excess = request.getParameterValues("excess");
                String[] shortage = request.getParameterValues("shortage");
                String[] status = request.getParameterValues("status");
                String[] detailId = request.getParameterValues("detailId");
                String[] materialIds = request.getParameterValues("materialId");
                String[] floorPackQty = request.getParameterValues("floorPackQty");
                int updateGrnIssue = wmsBP.updateImGrnIssue(floorQty, usedQty, excess, shortage, status, detailId, floorPackQty, materialIds, wms, userId);
                path = "content/wms/imGrnIssueViewDetails.jsp";
            } else {
                path = "content/wms/imGrnIssueView.jsp";
            }

            ArrayList getImMaterialIssueMaster = wmsBP.getImMaterialIssueMaster(wms);
            request.setAttribute("issueMaster", getImMaterialIssueMaster);
            ArrayList getImMaterialIssueDetails = wmsBP.getImMaterialIssueDetails(wms);
            request.setAttribute("issueDetails", getImMaterialIssueDetails);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView imMaterialGrnView(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            wms.setFromDate(fromDate);
            wms.setToDate(toDate);
            String grnId = request.getParameter("grnId");
            wms.setGrnId(grnId);
            if ("view".equals(param)) {
                path = "content/wms/imMaterialGrnViewDetails.jsp";
            } else {
                path = "content/wms/imMaterialGrnView.jsp";
            }
            ArrayList imMaterialGrnMaster = new ArrayList();
            imMaterialGrnMaster = wmsBP.imMaterialGrnMaster(wms);
            request.setAttribute("grnMaster", imMaterialGrnMaster);
            ArrayList imMaterialGrnDetails = new ArrayList();
            imMaterialGrnDetails = wmsBP.imMaterialGrnDetails(wms);
            request.setAttribute("grnDetails", imMaterialGrnDetails);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView imMaterialStockView(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        String pageTitle = "";
        String menuPath = "Instamart >>  Stock View";
        String param = request.getParameter("param");
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            
            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseList();
            request.setAttribute("getWareHouseList", getWareHouseList);
            
             if ("search".equals(param)){
               
                String whId = (String) session.getAttribute("hubId");
                String warehouseId = request.getParameter("whId");
                if (warehouseId == null || "".equals(warehouseId)) {
                    warehouseId = whId;
                }
                wms.setWhId(warehouseId);
                request.setAttribute("whId", warehouseId);
            
                ArrayList imMaterialStockView = new ArrayList();
                imMaterialStockView = wmsBP.imMaterialStockView(wms);
                request.setAttribute("stockView", imMaterialStockView);
            
               path = "content/wms/imMaterialStockView.jsp";  
               
           }else if ("excel".equals(param)) {
          
              String whId = request.getParameter("whId");
              wms.setWhId(whId);
              request.setAttribute("hubId", whId);
           
              ArrayList imMaterialStockView = new ArrayList();
              imMaterialStockView = wmsBP.imMaterialStockView(wms);
              request.setAttribute("stockView", imMaterialStockView);
              System.out.println("imMaterialStockView=====" + imMaterialStockView.size());
            
            
              path = "content/wms/imMaterialStockViewExcel.jsp";
            
             }else {
                 ArrayList imMaterialStockView = new ArrayList();
                 imMaterialStockView = wmsBP.imMaterialStockView(wms);
                 request.setAttribute("stockView", imMaterialStockView);
                 System.out.println("imMaterialStockView=====" + imMaterialStockView.size());
                 
               
                  path = "content/wms/imMaterialStockView.jsp";  
           
            }
             
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView instaMartProductMaster(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        try {
            String whId = (String) session.getAttribute("hubId");
            wmsTO.setUserId(userId + "");
            wmsTO.setWhId(whId);
            String param = request.getParameter("param");
            if ("save".equals(param)) {

                String productName = request.getParameter("productName");
                wmsTO.setProductName(productName);

                String skuCode = request.getParameter("skuCode");
                wmsTO.setSkuCode(skuCode);

                String productDescription = request.getParameter("productDescription");
                wmsTO.setProductDescription(productDescription);

                String minimumStockQty = request.getParameter("minimumStockQty");
                wmsTO.setMinimumStockQty(minimumStockQty);

                String uom = request.getParameter("uom");
                wmsTO.setUom2(uom);

                String temperature = request.getParameter("temperature");
                wmsTO.setTemperature(temperature);

                String storage = request.getParameter("storage");
                wmsTO.setStorage(storage);

                String activeInd = request.getParameter("activeInd");
                wmsTO.setActiveInd(activeInd);

                String productId = request.getParameter("productId");
                wmsTO.setProductId(productId);

                if ("save".equals(param)) {
                    int insertStatus = wmsBP.getInstaMart(wmsTO);
                }

            }
            ArrayList instaMartProduct = new ArrayList();
            instaMartProduct = wmsBP.getInstaMartProduct(wmsTO);
            request.setAttribute("instaMartProduct", instaMartProduct);

            path = "content/wms/instaMartProductMaster.jsp";

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView imProductMasterExcelUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
//        FileInputStream fis = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = (Integer) session.getAttribute("userId");
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        String ProductName = "";
        String productDescription = "";
        String uom = "";
        String storage = "";
        String skuCode = "";
        String minimumStockQty = "";
        String temperature = "";
        String activeInd = "";
        String productId = "";
        String status = "0";

        int insertStatus = 0;

        int status1 = 0;
        try {
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

//            int deleteUploadTemp = wBP.deleteUploadTemp();
//            System.out.println("deleteUploadTemp----"+deleteUploadTemp);
            if (isMultipart) {

                userId = (Integer) session.getAttribute("userId");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadedxls/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        int len = splitFileNames.length;
                        fileFormat = splitFileNames[len - 1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = uploadedFileName;
                                System.out.println("splitFileName[0]=" + splitFileName[0]);
                                System.out.println("splitFileName[0]=" + splitFileName[1]);

                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                System.out.println("tempPath..." + tempFilePath);
                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath));
                                System.out.println("tempPath = " + tempFilePath);
//                                System.out.println("actPath = " + actualFilePath);
//                                String parts = actualFilePath.substring(actualFilePath.lastIndexOf("//"));
//                                String part1 = parts.replace("//", "");
                                if ("xlsx".equals(fileFormat)) {
                                    FileInputStream fis = new FileInputStream(tempFilePath);
                                    XSSFWorkbook wb = new XSSFWorkbook(fis);
                                    XSSFSheet sheet = wb.getSheetAt(0);
                                    int value = sheet.getLastRowNum();
                                    System.out.println("value  " + value);

                                    int sno = 0;
                                    Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                    while (itr.hasNext()) {
                                        Row row = itr.next();
                                        if (sno > 0) {
                                            WmsTO wmsTo = new WmsTO();

                                            if (row.getCell(0) != null && !"".equals(row.getCell(0))) {
                                                row.getCell(0).setCellType(row.getCell(0).CELL_TYPE_STRING);
                                                ProductName = row.getCell(0).getStringCellValue();
                                                wmsTo.setProductName(ProductName);
                                                System.out.println(ProductName);
                                            }

                                            if (row.getCell(1) != null && !"".equals(row.getCell(1))) {
                                                row.getCell(1).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                productDescription = row.getCell(1).getStringCellValue();
                                                wmsTo.setProductDescription(productDescription);
                                                System.out.println(productDescription);
                                            }

                                            if (row.getCell(2) != null && !"".equals(row.getCell(2))) {
                                                row.getCell(2).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                uom = row.getCell(2).toString();
                                                wmsTo.setUom2(uom);
                                                System.out.println(uom);
                                            }

                                            if (row.getCell(3) != null && !"".equals(row.getCell(3))) {
                                                row.getCell(3).setCellType(row.getCell(3).CELL_TYPE_STRING);
                                                storage = row.getCell(3).getStringCellValue();
                                                System.out.println(storage);
                                                wmsTo.setStorage(storage);
                                            }

                                            if (row.getCell(4) != null && !"".equals(row.getCell(4))) {
                                                row.getCell(4).setCellType(row.getCell(4).CELL_TYPE_STRING);
                                                skuCode = row.getCell(4) + "";
                                                wmsTo.setSkuCode(skuCode);
                                            }

                                            if (row.getCell(5) != null && !"".equals(row.getCell(5))) {
                                                row.getCell(5).setCellType(row.getCell(5).CELL_TYPE_STRING);
                                                minimumStockQty = row.getCell(5).toString();
                                                System.out.println(minimumStockQty);
                                                wmsTo.setMinimumStockQty(minimumStockQty);
                                            }

                                            if (row.getCell(6) != null && !"".equals(row.getCell(6))) {
                                                row.getCell(6).setCellType(row.getCell(6).CELL_TYPE_STRING);
                                                temperature = row.getCell(6).toString();
                                                System.out.println(temperature);
                                                wmsTo.setTemperature(temperature);
                                            }

                                            if (row.getCell(7) != null && !"".equals(row.getCell(7))) {
                                                row.getCell(7).setCellType(row.getCell(7).CELL_TYPE_STRING);
                                                activeInd = row.getCell(7).getStringCellValue();
                                                System.out.println(activeInd);
                                                wmsTo.setActiveInd(activeInd);
                                            }

//                              
                                            wmsTo.setStatus(status);
                                            wmsTo.setUserId(userId + "");

                                            status1 = wmsBP.insertImProductMaster(wmsTo);

                                        }
                                        sno++;

                                    }
                                } else if ("xls".equals(fileFormat)) {
                                    int count = 0;

                                    WorkbookSettings ws = new WorkbookSettings();
                                    ws.setLocale(new Locale("en", "EN"));
                                    Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                    Sheet s = workbook.getSheet(0);
                                    System.out.println("rows" + userId + s.getRows());

                                    for (int i = 1; i < s.getRows(); i++) {
                                        WmsTO wmsTo = new WmsTO();
                                        count++;

                                        ProductName = s.getCell(0, i).getContents();
                                        wmsTo.setProductName(ProductName);

                                        productDescription = s.getCell(1, i).getContents();
                                        wmsTo.setProductDescription(productDescription);

                                        uom = s.getCell(2, i).getContents();
                                        wmsTo.setUom2(uom);

                                        storage = s.getCell(3, i).getContents();
                                        wmsTo.setStorage(storage);

                                        skuCode = s.getCell(4, i).getContents();
                                        wmsTo.setSkuCode(skuCode);

                                        minimumStockQty = s.getCell(5, i).getContents();
                                        wmsTo.setMinimumStockQty(minimumStockQty);

                                        temperature = s.getCell(6, i).getContents();
                                        wmsTo.setTemperature(temperature);

                                        activeInd = s.getCell(7, i).getContents();
                                        wmsTo.setActiveInd(activeInd);

//                                    productId = s.getCell(8, i).getContents();
//                                    wmsTo.setProductId(productId);
                                        wmsTo.setStatus(status);
                                        wmsTo.setUserId(userId + "");

                                        status1 = wmsBP.insertImProductMaster(wmsTo);

                                    }

                                }
                            }
                        } else {
                            request.setAttribute("erruserIdorMessage", "Dealer master order updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            wms.setUserId(userId + "");
            ArrayList getImProductMasterTemp = new ArrayList();
            getImProductMasterTemp = wmsBP.getImProductMasterTemp(wms);
            request.setAttribute("getImProductMasterTemp", getImProductMasterTemp);

            ArrayList instaMartProduct = new ArrayList();
            instaMartProduct = wmsBP.getInstaMartProduct(wms);
            request.setAttribute("instaMartProduct", instaMartProduct);

            path = "content/wms/instaMartProductMaster.jsp";

            if (getImProductMasterTemp.size() > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Im product master Updated Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);

    }

//    imProductMasterToMainTable
    public ModelAndView imProductMasterToMainTable(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        ModelAndView mv1 = new ModelAndView();
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        try {
            wmsTO.setWhId(whId);
            System.out.println("whId    " + whId);

            int imProductMasterToMainTable = wmsBP.imProductMasterToMainTable(wmsTO, userId, whId);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Im Product Master Updated Successfully");

//            path = "content/wms/dealerCategoryMaster.jsp";
            mv1 = instaMartProductMaster(request, response, command);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return mv1;
    }

    public ModelAndView vehicleUtilization(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");

        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            String customer = request.getParameter("customer");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String city = request.getParameter("city");
            String state = request.getParameter("state");
            String warehouse = request.getParameter("Warehouse");

            wms.setStateId(state);
            wms.setCity(city);
            wms.setWhId(warehouse);
            wms.setUserId(userId + "");
            wms.setFromDate(fromDate);
            wms.setToDate(toDate);
            wms.setCustId(customer);
            wms.setWhId(hubId);
            wms.setStatus("0");
            String stockId = request.getParameter("stockId");
            wms.setStockId(stockId);
            ArrayList vehicleUtilizationCust = wmsBP.vehicleUtilizationCust(wms);
            request.setAttribute("vehicleUtilizationCust", vehicleUtilizationCust);
            ArrayList getWareHouseList = wmsBP.getWareHouseList();
            request.setAttribute("getWareHouseList", getWareHouseList);
            ArrayList getCityList = wmsBP.getCityList();
            request.setAttribute("getCityList", getCityList);
            ArrayList stateList = new ArrayList();
            stateList = wmsBP.getStateList();
            request.setAttribute("stateList", stateList);
            if ("search".equals(param)) {
                ArrayList getTripDetailsReport = wmsBP.getTripDetailsReport(wms);
                request.setAttribute("getTripDetailsReport", getTripDetailsReport);
            }

            path = "content/wms/vehicleUtilization.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public void getDeTatEmpId(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();

        ArrayList getEmployeeId = new ArrayList();
        PrintWriter af = response.getWriter();
        try {
            String desingId = "";

            response.setContentType("text/html");
            desingId = request.getParameter("item");
            wmsTO.setDesignationId(desingId);
            System.out.println("desingId=+++++" + desingId);
//            getBinDetails = wmsBP.getBinDetails(wmsTO);
            getEmployeeId = wmsBP.getEmployeeId(wmsTO);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getEmployeeId.iterator();
            int cntr = 0;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                wmsTO = (WmsTO) itr.next();
                jsonObject.put("empIds", wmsTO.getEmpId());
                jsonObject.put("empNames", wmsTO.getEmpName());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            System.out.println("jsonArray = " + jsonArray);

            af.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView imInvoiceUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        WmsTO wmsTO = new WmsTO();
        OpsTO opsTO = new OpsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
            wmsTO.setWhId(whId);
            wmsTO.setUserId(userId + "");
            opsTO.setUserId(userId);
            opsTO.setWhId(whId);

            System.out.println(whId + "===================================== > wmscon");

            String param = request.getParameter("param");
            System.out.println("param===" + param);

            ArrayList getSupplierList = new ArrayList();
            getSupplierList = wmsBP.getSupplierList();
            request.setAttribute("getSupplierList", getSupplierList);

            ArrayList getImVendorList = new ArrayList();
            getImVendorList = wmsBP.getImVendorList(wmsTO);
            request.setAttribute("getImVendorList", getImVendorList);

            ArrayList getImDealerList = new ArrayList();
            getImDealerList = wmsBP.getImDealerList(wmsTO);
            request.setAttribute("getImDealerList", getImDealerList);

            ArrayList getDeviceIdList = opsBP.getDeviceIdList(opsTO);
            request.setAttribute("deviceList", getDeviceIdList);

            ArrayList getVehicleTypeList = new ArrayList();
            getVehicleTypeList = wmsBP.getVehicleTypeList();
            System.out.println("getVehicleTypeList=====" + getVehicleTypeList.size());
            request.setAttribute("getVehicleTypeList", getVehicleTypeList);

            if ("Save".equals(param)) {
                String supplierId = request.getParameter("supplierId");
                wmsTO.setSupplierId(supplierId);

                String lastDeviceId = request.getParameter("lastDeviceId");
                String lastResponse = request.getParameter("lastResponse");
                wmsTO.setReason(lastResponse);
                wmsTO.setMachineId(lastDeviceId);

                String vendorId = request.getParameter("vendorId");
                wmsTO.setVendorId(vendorId);

                String deviceId = request.getParameter("deviceId");
                wmsTO.setDeviceId(deviceId);

                String vehicleNo = request.getParameter("vehicleNo");
                wmsTO.setVehicleNo(vehicleNo);

                String driverName = request.getParameter("driverName");
                wmsTO.setDriverName(driverName);

                String driverMobile = request.getParameter("driverMobile");
                wmsTO.setDriverMobileNo(driverMobile);

                String dispatchDate = request.getParameter("dispatchDate");
                wmsTO.setDispatchDate(dispatchDate);

                String tripStartTime = request.getParameter("tripStartTime");
                wmsTO.setTripStartTime(tripStartTime);

                String remarks = request.getParameter("remarks");
                wmsTO.setRemarks(remarks);
                String vehicleType = request.getParameter("vehicleType");
                wmsTO.setVehicleType(vehicleType);
                String noOfCrates = request.getParameter("noOfCrates");
                wmsTO.setBoxQty(noOfCrates);

                String[] invoiceNos = request.getParameterValues("invoiceNo");
                String[] invoiceDate = request.getParameterValues("invoiceDate");
                String[] customerId = request.getParameterValues("customerId");
                String[] billedQty = request.getParameterValues("billedQty");
                String[] dispatchQty = request.getParameterValues("dispatchQty");
                String[] noOfCrates1 = request.getParameterValues("noOfCrates1");
                String[] invoiceAmt = request.getParameterValues("invoiceAmt");
                String[] remarksInv = request.getParameterValues("remarksInv");
                wmsTO.setInvoiceNoE(invoiceNos);
                wmsTO.setInvoiceDates(invoiceDate);
                wmsTO.setCustomerIds(customerId);
                wmsTO.setQtyE(billedQty);
                wmsTO.setQuantitys(dispatchQty);
                wmsTO.setNoOfCrates1(noOfCrates1);
                wmsTO.setInvoiceAmountL(invoiceAmt);

                int imInvoiceUpload = 0;
                imInvoiceUpload = wmsBP.imInvoiceUpload(wmsTO, userId);
                wmsTO.setLrId(imInvoiceUpload + "");

//                int insertToApiMasterAndDetails = wmsBP.insertToApiMasterAndDetails(wmsTO);
                ArrayList imLrDetails = wmsBP.getImLrDetails(wmsTO);
                request.setAttribute("getImLrDetails", imLrDetails);
                ArrayList imSubLrDetails = wmsBP.getImSubLrDetails(wmsTO);
                request.setAttribute("getImSubLrDetails", imSubLrDetails);
                path = "content/wms/imInvoiceViewPrint.jsp";
            } else if ("crate".equals(param)) {
                String dealerId = request.getParameter("dealerId");
                wmsTO.setDealerId(dealerId);
                request.setAttribute("dealerId", dealerId);
                ArrayList getImGetCrateScanned = wmsBP.getImGetCrateScanned(wmsTO);
                request.setAttribute("getImGetCrateScanned", getImGetCrateScanned);
                path = "content/wms/imViewCrateScanned.jsp";
            } else if ("delete".equals(param)) {
                String scanId = request.getParameter("scanId");
                String dealerId = request.getParameter("dealerId");
                request.setAttribute("dealerId", dealerId);
                wmsTO.setDealerId(dealerId);
                wmsTO.setScanId(scanId);
                int delete = wmsBP.deleteImCrateScanned(wmsTO);
                ArrayList getImGetCrateScanned = wmsBP.getImGetCrateScanned(wmsTO);
                request.setAttribute("getImGetCrateScanned", getImGetCrateScanned);
                path = "content/wms/imViewCrateScanned.jsp";
            } else {
                path = "content/wms/imInvoiceUpload.jsp";
            }

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView imDealerMaster(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");

        try {
            String whId = (String) session.getAttribute("hubId");
            String param = request.getParameter("param");
            wmsTO.setWhId(whId);
            wmsTO.setUserId(userId + "");
            if ("save".equals(param)) {
                String dealerName = request.getParameter("dealerName");
                String dealerCode = request.getParameter("dealerCode");
                String industries = request.getParameter("industries");
                String district = request.getParameter("district");
                String street = request.getParameter("street");
                String address = request.getParameter("address");
                String address2 = request.getParameter("address2");
                String city = request.getParameter("city");
                String pincode = request.getParameter("pincode");
                String phone1 = request.getParameter("phone1");
                String phone2 = request.getParameter("phone2");
                String gstNo = request.getParameter("gstNo");
                String customerName = request.getParameter("customerName");
                String customerPhone = request.getParameter("customerPhone");
                String activeInd = request.getParameter("activeInd");
                String dealerId = request.getParameter("dealerId");

                wmsTO.setDealerCode(dealerCode);
                wmsTO.setDealerName(dealerName);
                wmsTO.setIndustries(industries);
                wmsTO.setDistrict(district);
                wmsTO.setStreet(street);
                wmsTO.setAddress(address);
                wmsTO.setAddress1(address2);
                wmsTO.setCity(city);
                wmsTO.setPincode(pincode);
                wmsTO.setMobileNo(phone1);
                wmsTO.setPhoneNumber(phone2);
                wmsTO.setGstNo(gstNo);
                wmsTO.setCustomerName(customerName);
                wmsTO.setMobile(customerPhone);
                wmsTO.setActiveInd(activeInd);
                wmsTO.setDealerId(dealerId);
                int insertImDealerMaster = wmsBP.insertImDealerMaster(wmsTO);
                if (insertImDealerMaster == 10) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Customer Updated Successfully");
                } else if (insertImDealerMaster == 1000) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Dealer Name Already Exist");
                } else if (insertImDealerMaster == 1001) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Dealer Code Already Exist");
                } else if (insertImDealerMaster == 11) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Customer Inserted Sucessfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Customer Update Failed");
                }
            }
            ArrayList instaMartDealerMaster = new ArrayList();
            instaMartDealerMaster = wmsBP.getImDealerMaster(wmsTO);
            request.setAttribute("instaMartDealerMaster", instaMartDealerMaster);

            path = "content/wms/imDealerMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void imInvoiceDownload(HttpServletRequest request, HttpServletResponse response) throws FPBusinessException {

        String path = "";
        WmsTO wmsTO = new WmsTO();
        try {
            File file = new File("E:\\firstdoc.pdf");
            if (file.createNewFile()) {
                System.out.println("here");
            } else {
                System.out.println("here1");
            };
            String url = "http://localhost:8085/throttle/imInvoicePrint.do?param=print&lrId=1";
            String outputFile = "E:\\firstdoc.pdf";
            FileOutputStream os = new FileOutputStream(outputFile);
//            ITextRenderer renderer = new ITextRenderer();
            System.out.println("here3");
//            renderer.setDocument(url);
//            renderer.layout();
//            renderer.createPDF(os);

            os.close();
        } catch (FPRuntimeException exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
        }
    }

    public ModelAndView imInvoicePrint(HttpServletRequest request, HttpServletResponse response) throws FPBusinessException {

        String path = "";
        WmsTO wmsTO = new WmsTO();
        try {
            System.out.println("checkPrint");
            String param = request.getParameter("param");
            System.out.println("param===" + param);

            ArrayList getImInvoiceMaster = new ArrayList();
            getImInvoiceMaster = wmsBP.getImInvoiceMaster(wmsTO);
            request.setAttribute("getImInvoiceMaster", getImInvoiceMaster);

            if ("view".equals(param)) {
                String lrId = request.getParameter("lrId");
                wmsTO.setLrId(lrId);
                ArrayList getImInvoiceDetails = new ArrayList();
                getImInvoiceDetails = wmsBP.getImInvoiceDetails(wmsTO);
                request.setAttribute("getImInvoiceDetails", getImInvoiceDetails);
                path = "content/wms/imInvoiceViewDetails.jsp";
            } else if ("print".equals(param)) {
                String lrId = request.getParameter("lrId");
                wmsTO.setLrId(lrId);
                ArrayList imLrDetails = wmsBP.getImLrDetails(wmsTO);
                request.setAttribute("getImLrDetails", imLrDetails);
                ArrayList imSubLrDetails = wmsBP.getImSubLrDetails(wmsTO);
                request.setAttribute("getImSubLrDetails", imSubLrDetails);
                path = "content/wms/imInvoiceViewPrintPdf.jsp";
            } else {
                path = "content/wms/imInvoiceView.jsp";
            }
        } catch (FPRuntimeException exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView imInvoiceView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        WmsTO wmsTO = new WmsTO();
        OpsTO opsTO = new OpsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
            wmsTO.setWhId(whId);
            wmsTO.setUserId(userId + "");

            String param = request.getParameter("param");
            System.out.println("param===" + param);

            if ("view".equals(param)) {
                String lrId = request.getParameter("lrId");
                wmsTO.setLrId(lrId);
                ArrayList getImInvoiceDetails = new ArrayList();
                getImInvoiceDetails = wmsBP.getImInvoiceDetails(wmsTO);
                request.setAttribute("getImInvoiceDetails", getImInvoiceDetails);
                path = "content/wms/imInvoiceViewDetails.jsp";
            } else if ("print".equals(param)) {
                String lrId = request.getParameter("lrId");
                wmsTO.setLrId(lrId);
                ArrayList imLrDetails = wmsBP.getImLrDetails(wmsTO);
                request.setAttribute("getImLrDetails", imLrDetails);

                ArrayList imSubLrDetails = wmsBP.getImSubLrDetails(wmsTO);
                request.setAttribute("getImSubLrDetails", imSubLrDetails);

                path = "content/wms/imInvoiceViewPrint.jsp";
            } else {
                String fromDate = request.getParameter("fromDate");
                String toDate = request.getParameter("toDate");
                wmsTO.setFromDate(fromDate);
                wmsTO.setToDate(toDate);
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);

                ArrayList getImInvoiceMaster = new ArrayList();
                getImInvoiceMaster = wmsBP.getImInvoiceMaster(wmsTO);
                request.setAttribute("getImInvoiceMaster", getImInvoiceMaster);

                path = "content/wms/imInvoiceView.jsp";
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView imGrnInbound(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        WmsTO wmsTO = new WmsTO();
        try {
            wmsTO.setWhId(whId);
            wmsTO.setUserId(userId + "");

            ArrayList imVendorList = new ArrayList();
            imVendorList = wmsBP.getImVendorList(wmsTO);
            request.setAttribute("vendorMaster", imVendorList);

            ArrayList imSkuDetails = new ArrayList();
            imSkuDetails = wmsBP.getImSkuDetails(wmsTO);
            request.setAttribute("imSkuDetails", imSkuDetails);
            String param = request.getParameter("param");
            System.out.println("param===" + param);

            if ("save".equals(param)) {
                String vendorId = request.getParameter("vendorId");
                String vendorName = request.getParameter("vendorName");
                String grnDate = request.getParameter("grnDate");
                String grnTime = request.getParameter("grnTime");
                String vehicleNo = request.getParameter("vehicleNo");
                String totalKgQty = request.getParameter("totKgQty");
                String totalPcQty = request.getParameter("totPcQty");
                String totalPacketQty = request.getParameter("totPacketQty");
                String invoiceNo = request.getParameter("invoiceNo");
                String invoiceDate = request.getParameter("invoiceDate");
                String receivedBy = request.getParameter("receivedBy");
                String[] skuId = request.getParameterValues("skuId");
                String[] skuDescription = request.getParameterValues("skuDescription");
                String[] qty = request.getParameterValues("qty");
                String[] uom = request.getParameterValues("uom");
                wmsTO.setVendorId(vendorId);
                wmsTO.setVendorName(vendorName);
                wmsTO.setGrnDate(grnDate);
                wmsTO.setGrnTime(grnTime);
                wmsTO.setVehicleNo(vehicleNo);
                wmsTO.setTotKgQty(totalKgQty);
                wmsTO.setTotPcQty(totalPcQty);
                wmsTO.setTotPacketQty(totalPacketQty);
                wmsTO.setInvoiceNo(invoiceNo);
                wmsTO.setInvoiceDate(invoiceDate);
                wmsTO.setReceivedBy(receivedBy);
                wmsTO.setItemIds(skuId);
                wmsTO.setProductDescriptions(skuDescription);
                wmsTO.setQuantitys(qty);
                wmsTO.setUom(uom);
                int update = wmsBP.saveImGrnInbound(wmsTO);
                if (update > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Grn Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Grn Update Failed");
                }
                path = "content/wms/imGrnInbound.jsp";
            } else {
                path = "content/wms/imGrnInbound.jsp";
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView imGrnInboundView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        WmsTO wmsTO = new WmsTO();
        try {
            wmsTO.setWhId(whId);
            wmsTO.setUserId(userId + "");

            ArrayList getImGrnInbound = new ArrayList();
            getImGrnInbound = wmsBP.getImGrnInbound(wmsTO);
            request.setAttribute("grnMaster", getImGrnInbound);

            String param = request.getParameter("param");
            System.out.println("param===" + param);

            if ("view".equals(param)) {
                String grnId = request.getParameter("grnId");
                wmsTO.setGrnId(grnId);
                ArrayList getImGrnInboundDetails = new ArrayList();
                getImGrnInboundDetails = wmsBP.getImGrnInboundDetails(wmsTO);
                request.setAttribute("grnDetails", getImGrnInboundDetails);
                getImGrnInbound = wmsBP.getImGrnInbound(wmsTO);
                request.setAttribute("grnMaster", getImGrnInbound);
                path = "content/wms/imGrnInboundDetails.jsp";
            } else if ("update".equals(param)) {
                String[] updatedDate = request.getParameterValues("updatedDate");
                String[] selectedStatus = request.getParameterValues("selectedStatus");
                String[] updatedTime = request.getParameterValues("updatedTime");
                String[] detailId = request.getParameterValues("detailId");
                String[] acceptedQty = request.getParameterValues("acceptedQty");
                String[] rejectedQty = request.getParameterValues("rejectedQty");
                wmsTO.setDetailIds(detailId);
                wmsTO.setSelectedIndex(selectedStatus);
                wmsTO.setUpdatedDate(updatedDate);
                wmsTO.setUpdatedTime(updatedTime);
                wmsTO.setAcceptedQty(acceptedQty);
                wmsTO.setRejectedQty(rejectedQty);
                int update = wmsBP.updateImGrnInboundDetails(wmsTO);
                if (update > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Grn Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Grn Update Failed");
                }
                getImGrnInbound = wmsBP.getImGrnInbound(wmsTO);
                request.setAttribute("grnMaster", getImGrnInbound);
                path = "content/wms/imGrnInboundView.jsp";
            } else {
                path = "content/wms/imGrnInboundView.jsp";
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView imCratesReport(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        WmsTO wmsTO = new WmsTO();
        try {
            String hubId = request.getParameter("whId");
            if ("".equals(hubId) || hubId == null) {
                hubId = whId;
            }
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            wmsTO.setWhId(hubId);
            request.setAttribute("whId", hubId);
            wmsTO.setUserId(userId + "");
            String param = request.getParameter("param");
            if ("view".equals(param)) {
                String sublrId = request.getParameter("sublrId");
                wmsTO.setSubLrId(sublrId);
                ArrayList cratesReportView = new ArrayList();
                cratesReportView = wmsBP.imCratesReportView(wmsTO);
                request.setAttribute("cratesReportView", cratesReportView);
                path = "content/wms/imCratesReportView.jsp";
            } else {

                path = "content/wms/imCratesReport.jsp";
            }
            ArrayList whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);
            ArrayList cratesReport = new ArrayList();
            cratesReport = wmsBP.imCratesReport(wmsTO);
            request.setAttribute("cratesReport", cratesReport);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView imAddPackagingDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        WmsTO wmsTO = new WmsTO();
        try {
            wmsTO.setWhId(whId);
            wmsTO.setUserId(userId + "");

            ArrayList getImMachineList = new ArrayList();
            getImMachineList = wmsBP.getImMachineList(wmsTO);
            request.setAttribute("machineList", getImMachineList);

            ArrayList imSkuDetails = new ArrayList();
            imSkuDetails = wmsBP.getImChildSkuDetails(wmsTO);
            request.setAttribute("imSkuDetails", imSkuDetails);

            String param = request.getParameter("param");
            System.out.println("param===" + param);

            if ("save".equals(param)) {
                String vendorName = request.getParameter("empVendor");
                String empName = request.getParameter("empName");
                String machineId = request.getParameter("machineId");
                String packDate = request.getParameter("packDate");
                String packTime = request.getParameter("packTime");
                String startTime = request.getParameter("startTime");
                String endTime = request.getParameter("endTime");
                String[] skuId = request.getParameterValues("skuId");
                String[] skuDescription = request.getParameterValues("skuDescription");
                String[] packedQty = request.getParameterValues("packedQty");
                wmsTO.setVendorName(vendorName);
                wmsTO.setEmpName(empName);
                wmsTO.setMachineId(machineId);
                wmsTO.setDate(packDate);
                wmsTO.setTime(packTime);
                wmsTO.setLoadingStartTime(startTime);
                wmsTO.setLoadingEndTime(endTime);
                wmsTO.setQuantitys(packedQty);
                wmsTO.setItemIds(skuId);
                wmsTO.setProductDescriptions(skuDescription);
                int update = wmsBP.saveImPackagingDetails(wmsTO);
                if (update > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Packaging Data Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Packaging Data Update Failed");
                }
                path = "content/wms/imAddPackagingDetails.jsp";
            } else {
                path = "content/wms/imAddPackagingDetails.jsp";
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView imViewPackagingDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        WmsTO wmsTO = new WmsTO();
        try {
            wmsTO.setWhId(whId);
            wmsTO.setUserId(userId + "");

            ArrayList getImPackagingMaster = new ArrayList();
            getImPackagingMaster = wmsBP.getImPackagingMaster(wmsTO);
            request.setAttribute("packMaster", getImPackagingMaster);

            String param = request.getParameter("param");
            System.out.println("param===" + param);

            if ("view".equals(param)) {
                String packId = request.getParameter("packId");
                wmsTO.setPackId(packId);
                ArrayList getImPackagingDetails = new ArrayList();
                getImPackagingDetails = wmsBP.getImPackagingDetails(wmsTO);
                request.setAttribute("packDetails", getImPackagingDetails);
                getImPackagingMaster = wmsBP.getImPackagingMaster(wmsTO);
                request.setAttribute("packMaster", getImPackagingMaster);
                path = "content/wms/imViewPackagingDetails.jsp";
            } else {
                path = "content/wms/imViewPackagingMaster.jsp";
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView imVendorMaster(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            if ("save".equals(param)) {
                String vendorId = request.getParameter("vendorId");
                String vendorName = request.getParameter("vendorName");
                String vendorCode = request.getParameter("vendorCode");
                String city = request.getParameter("city");
                String state = request.getParameter("state");
                String address = request.getParameter("address");
                String phoneNumber = request.getParameter("phoneNumber");

                String emailId = request.getParameter("emailId");
                String contactPerson = request.getParameter("contactPerson");
                String gstNo = request.getParameter("gstNo");

                wms.setVendorId(vendorId);
                System.out.println("vendorId++++++++++++++++++++" + vendorId);
                wms.setVendorName(vendorName);
                wms.setVendorCode(vendorCode);
                wms.setCity(city);
                wms.setState(state);
                wms.setAddress(address);
                wms.setPhoneNumber(phoneNumber);

                wms.setEmailId(emailId);
                wms.setContactPerson(contactPerson);
                wms.setGstNo(gstNo);
                int insertImVendorMaster = wmsBP.insertImVendorMaster(wms);
                if (insertImVendorMaster > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vendor Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Vendor Update Failed");
                }
            }
            wms.setStatus("100");
            ArrayList imVendorMasterDetails = new ArrayList();
            imVendorMasterDetails = wmsBP.imVendorMasterDetails(wms);
            request.setAttribute("vendorMaster", imVendorMasterDetails);

            ArrayList getCityList = new ArrayList();
            getCityList = wmsBP.getCityList();
            request.setAttribute("city", getCityList);
            ArrayList getStateList = new ArrayList();
            getStateList = wmsBP.getStateList();
            request.setAttribute("state", getStateList);
            path = "content/wms/imVendorMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView updateConnectTranshipment(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            String dispatchId = request.getParameter("dispatchId");
            wms.setDispatchId(dispatchId);
            int status = wmsBP.updateConnectTranshipment(wms);
            path = "content/wms/imVendorMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView imFreightUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        wmsCommand = command;
        String menuPath = "Connect Operation  >> DC Freight Update ";
        int updateFreight = 0;
        int userId = (Integer) session.getAttribute("userId");

        try {
            String whId = (String) session.getAttribute("hubId");
            String warehouseId = request.getParameter("whId");
            if (warehouseId == null || "".equals(warehouseId)) {
                warehouseId = whId;
            }
            String status = request.getParameter("statusType");
            wmsTO.setStatus(status);
            wmsTO.setWhId(warehouseId);
            request.setAttribute("whId", warehouseId);
            request.setAttribute("statusType", status);
            String param = request.getParameter("param");
            System.out.println("parsm===" + param);
            if ("Submit".equals(param)) {
                String[] check = request.getParameterValues("selectedStatus");
                String[] lrnNo = request.getParameterValues("subLr");
                String[] lrNo = request.getParameterValues("lrNo");
                String[] vendorName = request.getParameterValues("vendorName");
                String[] customerName = request.getParameterValues("customerName");
                String[] dispatchQuantity = request.getParameterValues("dispatchQuantity");
                String[] billQuantity = request.getParameterValues("billQuantity");
                String[] freightAmt = request.getParameterValues("freightAmt");
                String[] otherAmt = request.getParameterValues("otherAmt");
                String[] totalAmt = request.getParameterValues("totalAmt");
                updateFreight = wmsBP.updateFreightAmountNew(userId, check, lrnNo, lrNo, vendorName, customerName, dispatchQuantity, billQuantity, freightAmt, otherAmt, totalAmt);
                path = "content/wms/imFreightUpdate.jsp";

            } else if ("exportExcel".equals(param)) {
                path = "content/wms/imFreightUpdateExportExcel.jsp";
            } else {
                path = "content/wms/imFreightUpdate.jsp";
            }
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
            }
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            ArrayList whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);

            ArrayList imFreightDetails = new ArrayList();
            imFreightDetails = wmsBP.imFreightDetails(wmsTO);
            request.setAttribute("imFreightDetails", imFreightDetails);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView msInvoiceUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
//        FileInputStream fis = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = (Integer) session.getAttribute("userId");
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        String ProductName = "";
        String productDescription = "";
        String uom = "";
        String storage = "";
        String skuCode = "";
        String minimumStockQty = "";
        String temperature = "";
        String activeInd = "";
        String productId = "";
        String status = "0";

        int insertStatus = 0;

        int status1 = 0;
        try {
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

//            int deleteUploadTemp = wBP.deleteUploadTemp();
//            System.out.println("deleteUploadTemp----"+deleteUploadTemp);
            if (isMultipart) {

                userId = (Integer) session.getAttribute("userId");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadedxls/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        int len = splitFileNames.length;
                        fileFormat = splitFileNames[len - 1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = uploadedFileName;
//                                System.out.println("splitFileName[0]=" + splitFileName[0]);
//                                System.out.println("splitFileName[0]=" + splitFileName[1]);

                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
//                                System.out.println("tempPath..." + tempFilePath);
//                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
//                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
//                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath));
//                                System.out.println("tempPath = " + tempFilePath);
//                                System.out.println("actPath = " + actualFilePath);
//                                String parts = actualFilePath.substring(actualFilePath.lastIndexOf("//"));
//                                String part1 = parts.replace("//", "");
//                                System.out.println("11111111111111111111111111");
                                if ("xlsx".equals(fileFormat)) {
                                    FileInputStream fis = new FileInputStream(tempFilePath);
                                    XSSFWorkbook wb = new XSSFWorkbook(fis);
                                    XSSFSheet sheet = wb.getSheetAt(0);
                                    int value = sheet.getLastRowNum();
//                                    System.out.println("value  " + value);

                                    int sno = 0;
                                    Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                    while (itr.hasNext()) {
                                        Row row = itr.next();
                                        if (sno > 0) {
                                            System.out.println("cellyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy " + row.getCell(1));
                                            WmsTO wmsTo = new WmsTO();

                                            if (row.getCell(0) != null && !"".equals(row.getCell(0))) {
                                                row.getCell(0).setCellType(row.getCell(0).CELL_TYPE_STRING);
                                                ProductName = row.getCell(0).getStringCellValue();
                                                wmsTo.setProductName(ProductName);

                                            }

                                            if (row.getCell(1) != null && !"".equals(row.getCell(1))) {
                                                row.getCell(1).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                productDescription = row.getCell(1).getStringCellValue();
                                                wmsTo.setProductDescription(productDescription);

                                            }

                                            if (row.getCell(2) != null && !"".equals(row.getCell(2))) {
                                                row.getCell(2).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                uom = row.getCell(2).toString();
                                                wmsTo.setUom2(uom);

                                            }

                                            if (row.getCell(3) != null && !"".equals(row.getCell(3))) {
                                                row.getCell(3).setCellType(row.getCell(3).CELL_TYPE_STRING);
                                                storage = row.getCell(3).getStringCellValue();

                                                wmsTo.setStorage(storage);
                                            }

                                            if (row.getCell(4) != null && !"".equals(row.getCell(4))) {
                                                row.getCell(4).setCellType(row.getCell(4).CELL_TYPE_STRING);
                                                skuCode = row.getCell(4) + "";
                                                wmsTo.setSkuCode(skuCode);
                                            }

                                            if (row.getCell(5) != null && !"".equals(row.getCell(5))) {
                                                row.getCell(5).setCellType(row.getCell(5).CELL_TYPE_STRING);
                                                minimumStockQty = row.getCell(5).toString();
                                                wmsTo.setMinimumStockQty(minimumStockQty);
                                            }

                                            if (row.getCell(6) != null && !"".equals(row.getCell(6))) {
                                                row.getCell(6).setCellType(row.getCell(6).CELL_TYPE_STRING);
                                                temperature = row.getCell(6).toString();
                                                System.out.println(temperature);
                                                wmsTo.setTemperature(temperature);
                                            }

                                            if (row.getCell(7) != null && !"".equals(row.getCell(7))) {
                                                row.getCell(7).setCellType(row.getCell(7).CELL_TYPE_STRING);
                                                activeInd = row.getCell(7).getStringCellValue();
                                                wmsTo.setActiveInd(activeInd);
                                            }

//                              
                                            wmsTo.setStatus(status);
                                            wmsTo.setUserId(userId + "");

                                            status1 = wmsBP.insertImProductMaster(wmsTo);

                                        }
                                        sno++;

                                    }
                                } else if ("xls".equals(fileFormat)) {
                                    int count = 0;

                                    WorkbookSettings ws = new WorkbookSettings();
                                    ws.setLocale(new Locale("en", "EN"));
                                    Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                    Sheet s = workbook.getSheet(0);
//                                    System.out.println("rows" + userId + s.getRows());

                                    for (int i = 1; i < s.getRows(); i++) {
                                        System.out.println("11111111111111111111");
                                        WmsTO wmsTo = new WmsTO();
                                        count++;

                                        ProductName = s.getCell(0, i).getContents();
                                        wmsTo.setProductName(ProductName);

                                        productDescription = s.getCell(1, i).getContents();
                                        wmsTo.setProductDescription(productDescription);

                                        uom = s.getCell(2, i).getContents();
                                        wmsTo.setUom2(uom);

                                        storage = s.getCell(3, i).getContents();
                                        wmsTo.setStorage(storage);

                                        skuCode = s.getCell(4, i).getContents();
                                        wmsTo.setSkuCode(skuCode);

                                        minimumStockQty = s.getCell(5, i).getContents();
                                        wmsTo.setMinimumStockQty(minimumStockQty);

                                        temperature = s.getCell(6, i).getContents();
                                        wmsTo.setTemperature(temperature);

                                        activeInd = s.getCell(7, i).getContents();
                                        wmsTo.setActiveInd(activeInd);

//                                    productId = s.getCell(8, i).getContents();
//                                    wmsTo.setProductId(productId);
                                        wmsTo.setStatus(status);
                                        wmsTo.setUserId(userId + "");

                                        status1 = wmsBP.insertImProductMaster(wmsTo);

                                    }

                                }
                            }
                        } else {
                            request.setAttribute("erruserIdorMessage", "Dealer master order updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            wms.setUserId(userId + "");
            ArrayList getImProductMasterTemp = new ArrayList();
            getImProductMasterTemp = wmsBP.getImProductMasterTemp(wms);
            request.setAttribute("getImProductMasterTemp", getImProductMasterTemp);

            ArrayList instaMartProduct = new ArrayList();
            instaMartProduct = wmsBP.getInstaMartProduct(wms);
            request.setAttribute("instaMartProduct", instaMartProduct);

//            path = "content/wms/instaMartProductMaster.jsp";
            path = "content/wms/msInvoiceUpload.jsp";

            if (getImProductMasterTemp.size() > 0) {
                System.out.println("getImProductMasterTemp====check================status" + getImProductMasterTemp);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Im product master Updated Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);

    }

    public ModelAndView dzProductUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = (Integer) session.getAttribute("userId");
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        String productId = "";
        String productVariantId = "";
        String productName = "";
        String optionName = "";
        String optionValue = "";
        String barCode = "";
        String hsnCode = "";
        String categoryName = "";
        String subCategoryName = "";
        String status = "0";
        String param = "";

        int insertStatus = 0;

        int status1 = 0;

        param = request.getParameter("param");

        try {
            WmsTO wms = new WmsTO();
            wms.setUserId(userId + "");

            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

            if (isMultipart) {

                userId = (Integer) session.getAttribute("userId");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadedxls/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        int len = splitFileNames.length;
                        fileFormat = splitFileNames[len - 1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = uploadedFileName;
                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                File f1 = new File(actualFilePath);
                                f1.renameTo(new File(tempFilePath));
//                               
                                if ("xlsx".equals(fileFormat)) {
                                    FileInputStream fis = new FileInputStream(tempFilePath);
                                    XSSFWorkbook wb = new XSSFWorkbook(fis);
                                    XSSFSheet sheet = wb.getSheetAt(0);
                                    int value = sheet.getLastRowNum();

                                    int sno = 0;
                                    Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                    while (itr.hasNext()) {
                                        Row row = itr.next();
                                        if (sno > 0) {
//                                                System.out.println("print the all the date from the xlxs or xls " + row.getCell(0) + row.getCell(1) + row.getCell(2) + row.getCell(3) + row.getCell(4) + row.getCell(5) + row.getCell(6) + row.getCell(7));
                                            WmsTO wmsTo = new WmsTO();

                                            if (row.getCell(0) != null && !"".equals(row.getCell(0))) {
                                                row.getCell(0).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                productId = row.getCell(0).getStringCellValue();
                                                wmsTo.setProductId(productId);

                                            }

                                            if (row.getCell(1) != null && !"".equals(row.getCell(1))) {
                                                row.getCell(1).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                productVariantId = row.getCell(1).getStringCellValue();
                                                wmsTo.setProductVariantId(productVariantId);

                                            }

                                            if (row.getCell(2) != null && !"".equals(row.getCell(2))) {
                                                row.getCell(2).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                productName = row.getCell(2).getStringCellValue();
                                                wmsTo.setProductName(productName);

                                            }

                                            if (row.getCell(3) != null && !"".equals(row.getCell(3))) {
                                                row.getCell(3).setCellType(row.getCell(3).CELL_TYPE_STRING);
                                                optionName = row.getCell(3).toString();
                                                wmsTo.setOptionName(optionName);

                                            }

                                            if (row.getCell(4) != null && !"".equals(row.getCell(4))) {
                                                row.getCell(4).setCellType(row.getCell(4).CELL_TYPE_STRING);
                                                optionValue = row.getCell(3).getStringCellValue();
                                                wmsTo.setOptionValue(optionValue);
                                            }

                                            if (row.getCell(10) != null && !"".equals(row.getCell(10))) {
                                                row.getCell(10).setCellType(row.getCell(10).CELL_TYPE_STRING);
                                                barCode = row.getCell(10) + "";
                                                wmsTo.setBarCode(barCode);
                                            }

                                            if (row.getCell(11) != null && !"".equals(row.getCell(11))) {
                                                row.getCell(11).setCellType(row.getCell(11).CELL_TYPE_STRING);
                                                hsnCode = row.getCell(11).toString();
                                                wmsTo.setHsnCode(hsnCode);
                                            }

                                            if (row.getCell(13) != null && !"".equals(row.getCell(13))) {
                                                row.getCell(13).setCellType(row.getCell(13).CELL_TYPE_STRING);
                                                categoryName = row.getCell(13).toString();
                                                wmsTo.setCategoryName(categoryName);
                                            }

                                            if (row.getCell(14) != null && !"".equals(row.getCell(14))) {
                                                row.getCell(14).setCellType(row.getCell(14).CELL_TYPE_STRING);
                                                subCategoryName = row.getCell(14).getStringCellValue();
                                                wmsTo.setSubCategoryName(subCategoryName);
                                            }

//                              
                                            wmsTo.setStatus(status);
                                            wmsTo.setUserId(userId + "");

                                            status1 = wmsBP.dzProductUpload(wmsTo);

                                        }
                                        sno++;

                                    }
                                } else if ("xls".equals(fileFormat)) {
                                    int count = 0;

                                    WorkbookSettings ws = new WorkbookSettings();
                                    ws.setLocale(new Locale("en", "EN"));
                                    Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                    Sheet s = workbook.getSheet(0);

                                    for (int i = 1; i < s.getRows(); i++) {
                                        WmsTO wmsTo = new WmsTO();
                                        count++;
                                        productId = s.getCell(0, i).getContents();
                                        wmsTo.setProductId(productId);

                                        productVariantId = s.getCell(1, i).getContents();
                                        wmsTo.setProductVariantId(productVariantId);

                                        productName = s.getCell(2, i).getContents();
                                        wmsTo.setProductName(productName);

                                        optionName = s.getCell(3, i).getContents();
                                        wmsTo.setOptionName(optionName);

                                        optionValue = s.getCell(4, i).getContents();
                                        wmsTo.setOptionValue(optionValue);

                                        barCode = s.getCell(10, i).getContents();
                                        wmsTo.setBarCode(barCode);

                                        hsnCode = s.getCell(11, i).getContents();
                                        wmsTo.setHsnCode(hsnCode);

                                        categoryName = s.getCell(13, i).getContents();
                                        wmsTo.setCategoryName(categoryName);

                                        subCategoryName = s.getCell(14, i).getContents();
                                        wmsTo.setSubCategoryName(subCategoryName);

//                                 
                                        wmsTo.setStatus(status);
                                        wmsTo.setUserId(userId + "");

                                        status1 = wmsBP.dzProductUpload(wmsTo);

                                    }

                                }
                            }
                        } else {
                            request.setAttribute("erruserIdorMessage", "Product Upload updated Failed:");
                        }
                    }
                }
            }
            wms.setUserId(userId + "");
            ArrayList getdzProductUploadTemp = new ArrayList();
            getdzProductUploadTemp = wmsBP.getdzProductUploadTemp(wms);
            request.setAttribute("getdzProductUploadTemp", getdzProductUploadTemp);
            if (getdzProductUploadTemp.size() > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Product Upload Updated Successfully");
            }
            path = "content/wms/dzProductUpload.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);

    }

    public ModelAndView dzInsertProductMaster(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        ModelAndView mv1 = new ModelAndView();
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        try {
            String param = request.getParameter("param");
            wmsTO.setWhId(whId);
            wmsTO.setUserId(userId + "");
            System.out.println("whId    " + whId);
            if ("save".equals(param)) {
                int dzInsertIntoProductMaster = wmsBP.dzInsertIntoProductMaster(wmsTO);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Product Master Updated Successfully");
                path = "content/wms/dzProductUpload.jsp";
            } else {
                int delDzProductUploadTemp = wmsBP.delDzProductUploadTemp(wmsTO);
                path = "content/wms/dzProductUpload.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView dzProductUploadView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {

            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            if ("save".equals(param)) {
                String productVariantId = request.getParameter("productVariantId");
                String productName = request.getParameter("productName");
                String optionName = request.getParameter("optionName");
                String optionValue = request.getParameter("optionValue");
                String barCode = request.getParameter("barCode");
                String hsnCode = request.getParameter("hsnCode");
                String categoryName = request.getParameter("categoryName");
                String subCategoryName = request.getParameter("subCategoryName");

                String productId1 = request.getParameter("productId");

                wms.setProductVariantId(productVariantId);
                wms.setProductName(productName);
                wms.setOptionName(optionName);
                wms.setOptionValue(optionValue);
                wms.setBarCode(barCode);
                wms.setHsnCode(hsnCode);
                wms.setCategoryName(categoryName);
                wms.setSubCategoryName(subCategoryName);

                wms.setProductId(productId1);
                System.out.println("productId1==>" + productId1);
                int editSave = wmsBP.getSaveEditPage(wms);

            }

            ArrayList getdzProductUpload = new ArrayList();
            getdzProductUpload = wmsBP.getdzProductUpload(wms);
            request.setAttribute("getdzProductUpload", getdzProductUpload);

            path = "content/wms/dzProductUploadView.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);

    }

    public ModelAndView dzGrnUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = (Integer) session.getAttribute("userId");
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        String productVariantId = "";
        String status = "";
        String param = "";

        String detailId = "";
        String grnId = "";
        String productLineEntryId = "";
        String category = "";
        String productId = "";
        String productFullName = "";
        String orderedQty = "";
        String receivedQty = "";
        String diffQty = "";
        String price = "";
        String tax = "";
        String poNo = "";
        String orderedTotal = "";
        String receivedTotal = "";
        String diffTotal = "";
        String taxName = "";
        String billToLocationId = "";
        String shipToLocationId = "";
        String billToLocationName = "";
        String shipToLocationName = "";
        String poDate = "";
        String poReferenceNumber = "";
        String poSupplierId = "";
        String poSupplierName = "";
        String poOrderedQty = "";
        String poReceivedQty = "";
        String whId = (String) session.getAttribute("hubId");

        int insertStatus = 0;

        int status1 = 0;

        param = request.getParameter("param");

        try {

            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

            if (isMultipart) {

                userId = (Integer) session.getAttribute("userId");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadedxls/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        int len = splitFileNames.length;
                        fileFormat = splitFileNames[len - 1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = uploadedFileName;
                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                File f1 = new File(actualFilePath);
                                f1.renameTo(new File(tempFilePath));
//                               
                                if ("xlsx".equals(fileFormat)) {
                                    FileInputStream fis = new FileInputStream(tempFilePath);
                                    XSSFWorkbook wb = new XSSFWorkbook(fis);
                                    XSSFSheet sheet = wb.getSheetAt(0);
                                    int value = sheet.getLastRowNum();

                                    int sno = 0;
                                    Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                    while (itr.hasNext()) {
                                        Row row = itr.next();
                                        if (sno > 0) {
//                                                System.out.println("print the all the date from the xlxs or xls " + row.getCell(0) + row.getCell(1) + row.getCell(2) + row.getCell(3) + row.getCell(4) + row.getCell(5) + row.getCell(6) + row.getCell(7));
                                            WmsTO wmsTo = new WmsTO();

                                            if (row.getCell(0) != null && !"".equals(row.getCell(0))) {
                                                row.getCell(0).setCellType(row.getCell(0).CELL_TYPE_STRING);
                                                productLineEntryId = row.getCell(0).getStringCellValue();
                                                wmsTo.setProductLineEntryId(productLineEntryId);

                                            }

                                            if (row.getCell(1) != null && !"".equals(row.getCell(1))) {
                                                row.getCell(1).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                category = row.getCell(1).getStringCellValue();
                                                wmsTo.setCategoryName(category);

                                            }

                                            if (row.getCell(2) != null && !"".equals(row.getCell(2))) {
                                                row.getCell(2).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                productId = row.getCell(2).toString();
                                                wmsTo.setProductId(productId);

                                            }

                                            if (row.getCell(3) != null && !"".equals(row.getCell(3))) {
                                                row.getCell(3).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                productVariantId = row.getCell(3).toString();
                                                wmsTo.setProductVariantId(productVariantId);

                                            }

                                            if (row.getCell(4) != null && !"".equals(row.getCell(4))) {
                                                row.getCell(4).setCellType(row.getCell(4).CELL_TYPE_STRING);
                                                productFullName = row.getCell(4).getStringCellValue();
                                                wmsTo.setName(productFullName);
                                            }

                                            if (row.getCell(5) != null && !"".equals(row.getCell(5))) {
                                                row.getCell(5).setCellType(row.getCell(5).CELL_TYPE_STRING);
                                                orderedQty = row.getCell(5) + "";
                                                wmsTo.setOrderedQty(orderedQty);
//                                                    System.out.println(orderedQty + "  = testin orderedQty's value");
                                            }

                                            if (row.getCell(6) != null && !"".equals(row.getCell(6))) {
                                                row.getCell(6).setCellType(row.getCell(6).CELL_TYPE_STRING);
                                                receivedQty = row.getCell(6).toString();
                                                wmsTo.setReceivedQty(receivedQty);
                                            }

                                            if (row.getCell(7) != null && !"".equals(row.getCell(7))) {
                                                row.getCell(7).setCellType(row.getCell(7).CELL_TYPE_STRING);
                                                diffQty = row.getCell(7).toString();
                                                wmsTo.setDiffQty(diffQty);
                                            }

                                            if (row.getCell(8) != null && !"".equals(row.getCell(8))) {
                                                row.getCell(8).setCellType(row.getCell(8).CELL_TYPE_STRING);
                                                price = row.getCell(8).getStringCellValue();
                                                wmsTo.setPrice(price);
                                            }

                                            if (row.getCell(9) != null && !"".equals(row.getCell(9))) {
                                                row.getCell(9).setCellType(row.getCell(9).CELL_TYPE_STRING);
                                                tax = row.getCell(9).getStringCellValue();
                                                wmsTo.setTaxValue(tax);
                                            }

                                            if (row.getCell(22) != null && !"".equals(row.getCell(22))) {
                                                row.getCell(22).setCellType(row.getCell(22).CELL_TYPE_STRING);
                                                poNo = row.getCell(22).getStringCellValue();
                                                wmsTo.setPoNo(poNo);
                                            }

                                            if (row.getCell(12) != null && !"".equals(row.getCell(12))) {
                                                row.getCell(12).setCellType(row.getCell(12).CELL_TYPE_STRING);
                                                orderedTotal = row.getCell(12).getStringCellValue();
                                                wmsTo.setTotalOrderedQty(orderedTotal);
                                            }

                                            if (row.getCell(13) != null && !"".equals(row.getCell(13))) {
                                                row.getCell(13).setCellType(row.getCell(13).CELL_TYPE_STRING);
                                                receivedTotal = row.getCell(13).getStringCellValue();
                                                wmsTo.setTotalReceivedQty(receivedTotal);
                                            }

                                            if (row.getCell(14) != null && !"".equals(row.getCell(14))) {
                                                row.getCell(14).setCellType(row.getCell(14).CELL_TYPE_STRING);
                                                diffTotal = row.getCell(14).getStringCellValue();
                                                wmsTo.setTotalDiffQty(diffTotal);
                                            }

                                            if (row.getCell(15) != null && !"".equals(row.getCell(15))) {
                                                row.getCell(15).setCellType(row.getCell(15).CELL_TYPE_STRING);
                                                taxName = row.getCell(15).getStringCellValue();
                                                wmsTo.setTaxVol(taxName);
                                            }

                                            if (row.getCell(23) != null && !"".equals(row.getCell(23))) {
                                                row.getCell(23).setCellType(row.getCell(23).CELL_TYPE_STRING);
                                                billToLocationId = row.getCell(23).getStringCellValue();
                                                wmsTo.setBillToLocationId(billToLocationId);
                                            }

                                            if (row.getCell(25) != null && !"".equals(row.getCell(25))) {
                                                row.getCell(25).setCellType(row.getCell(25).CELL_TYPE_STRING);
                                                shipToLocationId = row.getCell(25).getStringCellValue();
                                                wmsTo.setShipToLocationId(shipToLocationId);
                                            }

//                              
                                            if (row.getCell(24) != null && !"".equals(row.getCell(24))) {
                                                row.getCell(24).setCellType(row.getCell(24).CELL_TYPE_STRING);
                                                billToLocationName = row.getCell(24).getStringCellValue();
                                                wmsTo.setBillToLocationName(billToLocationName);
                                            }

                                            if (row.getCell(26) != null && !"".equals(row.getCell(26))) {
                                                row.getCell(26).setCellType(row.getCell(26).CELL_TYPE_STRING);
                                                shipToLocationName = row.getCell(26).getStringCellValue();
                                                wmsTo.setShipToLocationName(shipToLocationName);
                                            }

                                            if (row.getCell(27) != null && !"".equals(row.getCell(27))) {
                                                row.getCell(27).setCellType(row.getCell(27).CELL_TYPE_STRING);
                                                poDate = row.getCell(27).getStringCellValue();
                                                wmsTo.setPoDate(poDate);
                                            }

                                            if (row.getCell(31) != null && !"".equals(row.getCell(31))) {
                                                row.getCell(31).setCellType(row.getCell(31).CELL_TYPE_STRING);
                                                poReferenceNumber = row.getCell(31).getStringCellValue();
                                                wmsTo.setReferenceNo(poReferenceNumber);
                                            }

                                            if (row.getCell(32) != null && !"".equals(row.getCell(32))) {
                                                row.getCell(32).setCellType(row.getCell(32).CELL_TYPE_STRING);
                                                poSupplierId = row.getCell(32).getStringCellValue();
                                                wmsTo.setSupplierId(poSupplierId);
                                            }

                                            if (row.getCell(33) != null && !"".equals(row.getCell(33))) {
                                                row.getCell(33).setCellType(row.getCell(33).CELL_TYPE_STRING);
                                                poSupplierName = row.getCell(33).getStringCellValue();
                                                wmsTo.setSupplierName(poSupplierName);
                                            }

                                            if (row.getCell(37) != null && !"".equals(row.getCell(37))) {
                                                row.getCell(37).setCellType(row.getCell(37).CELL_TYPE_STRING);
                                                poOrderedQty = row.getCell(37).getStringCellValue();
                                                wmsTo.setOrderedQty1(poOrderedQty);
                                            }

                                            if (row.getCell(38) != null && !"".equals(row.getCell(38))) {
                                                row.getCell(38).setCellType(row.getCell(38).CELL_TYPE_STRING);
                                                poReceivedQty = row.getCell(38).getStringCellValue();
                                                wmsTo.setReceivedQty1(poReceivedQty);
                                            }

                                            wmsTo.setStatus(status);
                                            wmsTo.setUserId(userId + "");
                                            wmsTo.setWhId(whId);

                                            status1 = wmsBP.dzGrnUpload(wmsTo);

                                        }
                                        sno++;

                                    }
                                } else if ("xls".equals(fileFormat)) {
                                    int count = 0;

                                    WorkbookSettings ws = new WorkbookSettings();
                                    ws.setLocale(new Locale("en", "EN"));
                                    Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                    Sheet s = workbook.getSheet(0);

                                    for (int i = 1; i < s.getRows(); i++) {
                                        WmsTO wmsTo = new WmsTO();
                                        count++;

                                        productLineEntryId = s.getCell(0, i).getContents();
                                        wmsTo.setProductLineEntryId(productLineEntryId);

                                        category = s.getCell(1, i).getContents();
                                        wmsTo.setCategoryName(category);

                                        productId = s.getCell(2, i).getContents();
                                        wmsTo.setProductId(productId);

                                        productVariantId = s.getCell(3, i).getContents();
                                        wmsTo.setProductVariantId(productVariantId);

                                        productFullName = s.getCell(4, i).getContents();
                                        wmsTo.setName(productFullName);

                                        orderedQty = s.getCell(5, i).getContents();
                                        wmsTo.setOrderedQty(orderedQty);

                                        receivedQty = s.getCell(6, i).getContents();
                                        wmsTo.setReceivedQty(receivedQty);

                                        diffQty = s.getCell(7, i).getContents();
                                        wmsTo.setDiffQty(diffQty);

                                        price = s.getCell(8, i).getContents();
                                        wmsTo.setPrice(price);

                                        tax = s.getCell(9, i).getContents();
                                        wmsTo.setTaxVol(tax);

                                        poNo = s.getCell(22, i).getContents();
                                        wmsTo.setPoNo(poNo);

                                        orderedTotal = s.getCell(12, i).getContents();
                                        wmsTo.setTotalOrderedQty(orderedTotal);

                                        receivedTotal = s.getCell(13, i).getContents();
                                        wmsTo.setTotalReceivedQty(receivedTotal);

                                        diffTotal = s.getCell(14, i).getContents();
                                        wmsTo.setTotalDiffQty(diffTotal);

                                        taxName = s.getCell(15, i).getContents();
                                        wmsTo.setTaxVol(taxName);

                                        billToLocationId = s.getCell(23, i).getContents();
                                        wmsTo.setBillToLocationId(billToLocationId);

                                        shipToLocationId = s.getCell(25, i).getContents();
                                        wmsTo.setShipToLocationId(shipToLocationId);

                                        billToLocationName = s.getCell(24, i).getContents();
                                        wmsTo.setBillToLocationName(billToLocationName);

                                        shipToLocationName = s.getCell(26, i).getContents();
                                        wmsTo.setShipToLocationName(shipToLocationName);

                                        poDate = s.getCell(27, i).getContents();
                                        wmsTo.setPoDate(poDate);

                                        poReferenceNumber = s.getCell(31, i).getContents();
                                        wmsTo.setReferenceNo(poReferenceNumber);

                                        poSupplierId = s.getCell(32, i).getContents();
                                        wmsTo.setSupplierId(poSupplierId);

                                        poSupplierName = s.getCell(33, i).getContents();
                                        wmsTo.setSupplierName(poSupplierName);

                                        poOrderedQty = s.getCell(37, i).getContents();
                                        wmsTo.setOrderedQty1(poOrderedQty);

                                        poReceivedQty = s.getCell(38, i).getContents();
                                        wmsTo.setReceivedQty1(poReceivedQty);

                                        wmsTo.setStatus(status);
                                        wmsTo.setUserId(userId + "");
                                        wmsTo.setWhId(whId);

                                        status1 = wmsBP.dzGrnUpload(wmsTo);
//                                            status1 = wmsBP.dzProductUpload(wmsTo);

                                    }

                                }
                            }
                        } else {
                            request.setAttribute("erruserIdorMessage", "Grn Upload updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            wms.setUserId(userId + "");

            ArrayList getdzGrnDetailsTemp = new ArrayList();
            getdzGrnDetailsTemp = wmsBP.getdzGrnDetailsTemp(wms);
            request.setAttribute("getdzGrnDetailsTemp", getdzGrnDetailsTemp);

            path = "content/wms/dzGrnUpload.jsp";

            if (getdzGrnDetailsTemp.size() > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "GRN Uploaded Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);

    }

    public ModelAndView dzInsertGrnMaster(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        try {
            String param = request.getParameter("param");
            wmsTO.setWhId(whId);
            wmsTO.setUserId(userId + "");

            if ("save".equals(param)) {
                System.out.println("hiiiiiiiiiiiiiiiiiiiii" + userId);
                int insert = wmsBP.insertDzGrnUpload(wmsTO);
                if (insert > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Grn Upload Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Grn Upload Failed");
                }
                path = "content/wms/dzGrnUpload.jsp";
            } else {
                int delDzGrnDetailsTemp = wmsBP.delDzGrnDetailsTemp(wmsTO);
                path = "content/wms/dzGrnUpload.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView dzGrnUploadView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        wmsCommand = command;
        int insertStatus = 0;
        int status1 = 0;
        String param = request.getParameter("param");
        try {
            int userId = (Integer) session.getAttribute("userId");
            String whId = (String) session.getAttribute("hubId");
            String whIdNew = request.getParameter("whId");
            if (whIdNew != null && !"".equals(whIdNew)) {
                whId = whIdNew;
            }
            WmsTO wms = new WmsTO();
            wms.setWhId(whId);
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            
            wms.setFromDate(fromDate);
            wms.setToDate(toDate);
          

            ArrayList getdzGrnUpload = new ArrayList();
            getdzGrnUpload = wmsBP.getdzGrnUpload(wms);
            request.setAttribute("getdzGrnUpload", getdzGrnUpload);
            request.setAttribute("getdzGrnUploadSize", getdzGrnUpload.size());

            Iterator itr = getdzGrnUpload.iterator();

            while (itr.hasNext()) {
                wms = (WmsTO) itr.next();
                System.out.println("supplierName----------------------------------" + wms.getSupplierName());
                System.out.println("po Nooooo-------------"+ wms.getPoNo());

            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);



             
              String grnId = request.getParameter("grnId");
              System.out.println("grnId+++++"+grnId);
               
              
//            String grnId = request.getParameter("grnId");
//            wms.setGrnId(grnId);
//           
//            System.out.println("grnId"+grnId);
//            
            if ("view".equals(param)) {
                wms.setGrnId(grnId);  
                path = "content/wms/dzGrnUploadViewDetails.jsp";
            } else if ("print".equals(param)) {
                wms.setWhId(whId);
                path = "content/wms/dzInWordPrintGrn.jsp";
            } else if ("save".equals(param)) {
                wms.setWhId(whId);
                path = "content/wms/dzInWordPrintGrn.jsp";
            } else if ("export".equals(param)) {
                wms.setWhId(whId);
                path = "content/wms/dzGrnInvoiceViewExport.jsp";
            } else {
                path = "content/wms/dzGrnUploadView.jsp";
          
            }
            
            
             ArrayList getDzGrnDetails = new ArrayList();
            getDzGrnDetails = wmsBP.getDzGrnDetails(wms);
            request.setAttribute("getDzGrnDetails", getDzGrnDetails);    
                
            ArrayList getAllDzGrnDetails = new ArrayList();
            getAllDzGrnDetails = wmsBP.getAllDzGrnDetails(wms);
            request.setAttribute("getAllDzGrnDetails", getAllDzGrnDetails);

            ArrayList getDzGrnDetailsExport = new ArrayList();
            getDzGrnDetailsExport = wmsBP.getDzGrnDetailsExport(wms);
            request.setAttribute("getDzGrnDetailsExport", getDzGrnDetailsExport);

//            path = "content/wms/dzGrnUploadView.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);

    }

    public ModelAndView dzInvoiceUploadExcel(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = (Integer) session.getAttribute("userId");
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        String productVariantId = "";
        String status = "";
        String param = "";
        String whId = (String) session.getAttribute("hubId");
        String detailId = "";
        String grnId = "";
        String productLineEntryId = "";
        String category = "";
        String productCode = "";
        String productFullName = "";
        String sendQty = "";
        String receivedQty = "";
        String diffQty = "";
        String price = "";
        String sendAmt = "";
        String receivedAmt = "";
        String toNo = "";
        String fromLocationId = "";
        String toLocationId = "";
        String fromLocationName = "";
        String toLocationName = "";
        String toSendQty = "";
        String toReceivedQty = "";
        String toSendAmt = "";
        String toReceivedAmt = "";

        int insertStatus = 0;

        int status1 = 0;

        param = request.getParameter("param");

        try {

            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

            if (isMultipart) {

                userId = (Integer) session.getAttribute("userId");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadedxls/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        int len = splitFileNames.length;
                        fileFormat = splitFileNames[len - 1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = uploadedFileName;
                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                File f1 = new File(actualFilePath);
                                f1.renameTo(new File(tempFilePath));
//                               
                                if ("xlsx".equals(fileFormat)) {
                                    FileInputStream fis = new FileInputStream(tempFilePath);
                                    XSSFWorkbook wb = new XSSFWorkbook(fis);
                                    XSSFSheet sheet = wb.getSheetAt(0);
                                    int value = sheet.getLastRowNum();

                                    int sno = 0;
                                    Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                    while (itr.hasNext()) {
                                        Row row = itr.next();
                                        if (sno > 0) {
//                                                System.out.println("print the all the date from the xlxs or xls " + row.getCell(0) + row.getCell(1) + row.getCell(2) + row.getCell(3) + row.getCell(4) + row.getCell(5) + row.getCell(6) + row.getCell(7));
                                            WmsTO wmsTo = new WmsTO();

                                            wmsTo.setWhId(whId);

                                            if (row.getCell(0) != null && !"".equals(row.getCell(0))) {
                                                row.getCell(0).setCellType(row.getCell(0).CELL_TYPE_STRING);
                                                productLineEntryId = row.getCell(0).getStringCellValue();
                                                wmsTo.setProductLineEntryId(productLineEntryId);

                                            }

                                            if (row.getCell(1) != null && !"".equals(row.getCell(1))) {
                                                row.getCell(1).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                category = row.getCell(1).getStringCellValue();
                                                wmsTo.setCategoryName(category);

                                            }

                                            if (row.getCell(2) != null && !"".equals(row.getCell(2))) {
                                                row.getCell(2).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                productCode = row.getCell(2).toString();
                                                wmsTo.setProductCode(productCode);

                                            }

                                            if (row.getCell(3) != null && !"".equals(row.getCell(3))) {
                                                row.getCell(3).setCellType(row.getCell(3).CELL_TYPE_STRING);
                                                productVariantId = row.getCell(3).getStringCellValue();
                                                wmsTo.setName(productVariantId);
                                            }

                                            if (row.getCell(4) != null && !"".equals(row.getCell(4))) {
                                                row.getCell(4).setCellType(row.getCell(4).CELL_TYPE_STRING);
                                                productFullName = row.getCell(4).getStringCellValue();
                                                wmsTo.setName(productFullName);
                                            }

                                            if (row.getCell(5) != null && !"".equals(row.getCell(5))) {
                                                row.getCell(5).setCellType(row.getCell(5).CELL_TYPE_STRING);
                                                sendQty = row.getCell(5) + "";
                                                wmsTo.setOrderedQty(sendQty);
//                                                    System.out.println(orderedQty + "  = testin orderedQty's value");
                                            }

                                            if (row.getCell(6) != null && !"".equals(row.getCell(6))) {
                                                row.getCell(6).setCellType(row.getCell(6).CELL_TYPE_STRING);
                                                receivedQty = row.getCell(6).toString();
                                                wmsTo.setReceivedQty(receivedQty);
                                            }

                                            if (row.getCell(7) != null && !"".equals(row.getCell(7))) {
                                                row.getCell(7).setCellType(row.getCell(7).CELL_TYPE_STRING);
                                                diffQty = row.getCell(7).toString();
                                                wmsTo.setDiffQty(diffQty);
                                            }

                                            if (row.getCell(8) != null && !"".equals(row.getCell(8))) {
                                                row.getCell(8).setCellType(row.getCell(8).CELL_TYPE_STRING);
                                                price = row.getCell(8).getStringCellValue();
                                                wmsTo.setPrice(price);
                                            }

                                            if (row.getCell(9) != null && !"".equals(row.getCell(9))) {
                                                row.getCell(9).setCellType(row.getCell(9).CELL_TYPE_STRING);
                                                sendAmt = row.getCell(9).getStringCellValue();
                                                wmsTo.setSendAmt(sendAmt);
                                            }

                                            if (row.getCell(10) != null && !"".equals(row.getCell(10))) {
                                                row.getCell(10).setCellType(row.getCell(10).CELL_TYPE_STRING);
                                                receivedAmt = row.getCell(10).getStringCellValue();
                                                wmsTo.setReceivedAmt(receivedAmt);
                                            }

                                            if (row.getCell(16) != null && !"".equals(row.getCell(16))) {
                                                row.getCell(16).setCellType(row.getCell(16).CELL_TYPE_STRING);
                                                toNo = row.getCell(16).getStringCellValue();
                                                wmsTo.setInvoiceNo(toNo);
                                            }

                                            if (row.getCell(17) != null && !"".equals(row.getCell(17))) {
                                                row.getCell(17).setCellType(row.getCell(17).CELL_TYPE_STRING);
                                                fromLocationId = row.getCell(17).getStringCellValue();
                                                wmsTo.setFromLocation(fromLocationId);
                                            }

                                            if (row.getCell(18) != null && !"".equals(row.getCell(18))) {
                                                row.getCell(18).setCellType(row.getCell(18).CELL_TYPE_STRING);
                                                fromLocationName = row.getCell(18).getStringCellValue();
                                                wmsTo.setFromWhName(fromLocationName);
                                            }

                                            if (row.getCell(19) != null && !"".equals(row.getCell(19))) {
                                                row.getCell(19).setCellType(row.getCell(19).CELL_TYPE_STRING);
                                                toLocationId = row.getCell(19).getStringCellValue();
                                                wmsTo.setToLocation(toLocationId);
                                            }

                                            if (row.getCell(20) != null && !"".equals(row.getCell(20))) {
                                                row.getCell(20).setCellType(row.getCell(20).CELL_TYPE_STRING);
                                                toLocationName = row.getCell(20).getStringCellValue();
                                                wmsTo.setToWhName(toLocationName);
                                            }

                                            if (row.getCell(27) != null && !"".equals(row.getCell(27))) {
                                                row.getCell(27).setCellType(row.getCell(27).CELL_TYPE_STRING);
                                                toSendQty = row.getCell(27).getStringCellValue();
                                                wmsTo.setTotalOrderedQty(toSendQty);
                                            }

                                            if (row.getCell(28) != null && !"".equals(row.getCell(28))) {
                                                row.getCell(28).setCellType(row.getCell(28).CELL_TYPE_STRING);
                                                toReceivedQty = row.getCell(28).getStringCellValue();
                                                wmsTo.setTotalReceivedQty(toReceivedQty);
                                            }
                                            if (row.getCell(29) != null && !"".equals(row.getCell(29))) {
                                                row.getCell(29).setCellType(row.getCell(29).CELL_TYPE_STRING);
                                                toSendAmt = row.getCell(29).getStringCellValue();
                                                wmsTo.setTotalSendAmt(toSendAmt);
                                            }

                                            if (row.getCell(30) != null && !"".equals(row.getCell(30))) {
                                                row.getCell(30).setCellType(row.getCell(30).CELL_TYPE_STRING);
                                                toReceivedAmt = row.getCell(30).getStringCellValue();
                                                wmsTo.setTotalReceivedAmt(toReceivedAmt);
                                            }

                                            wmsTo.setStatus(status);
                                            wmsTo.setUserId(userId + "");
                                            wmsTo.setUserId(userId + "");

                                            status1 = wmsBP.dzInvoiceUploadExcel(wmsTo);

                                        }
                                        sno++;

                                    }
                                } else if ("xls".equals(fileFormat)) {
                                    int count = 0;

                                    WorkbookSettings ws = new WorkbookSettings();
                                    ws.setLocale(new Locale("en", "EN"));
                                    Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                    Sheet s = workbook.getSheet(0);

                                    for (int i = 1; i < s.getRows(); i++) {
                                        WmsTO wmsTo = new WmsTO();
                                        wmsTo.setWhId(whId);
                                        count++;

                                        productLineEntryId = s.getCell(0, i).getContents();
                                        wmsTo.setProductLineEntryId(productLineEntryId);

                                        category = s.getCell(1, i).getContents();
                                        wmsTo.setCategoryName(category);

                                        productCode = s.getCell(2, i).getContents();
                                        wmsTo.setProductCode(productCode);

                                        productVariantId = s.getCell(3, i).getContents();
                                        wmsTo.setProductVariantId(productVariantId);

                                        productFullName = s.getCell(4, i).getContents();
                                        wmsTo.setName(productFullName);

                                        sendQty = s.getCell(5, i).getContents();
                                        wmsTo.setOrderedQty(sendQty);

                                        receivedQty = s.getCell(6, i).getContents();
                                        wmsTo.setReceivedQty(receivedQty);

                                        diffQty = s.getCell(7, i).getContents();
                                        wmsTo.setDiffQty(diffQty);

                                        price = s.getCell(8, i).getContents();
                                        wmsTo.setPrice(price);

                                        sendAmt = s.getCell(9, i).getContents();
                                        wmsTo.setSendAmt(sendAmt);

                                        receivedAmt = s.getCell(10, i).getContents();
                                        wmsTo.setReceivedAmt(receivedAmt);

                                        toNo = s.getCell(16, i).getContents();
                                        wmsTo.setInvoiceNo(toNo);

                                        fromLocationId = s.getCell(17, i).getContents();
                                        wmsTo.setFromLocation(fromLocationId);

                                        fromLocationName = s.getCell(18, i).getContents();
                                        wmsTo.setFromWhName(fromLocationName);

                                        toLocationId = s.getCell(19, i).getContents();
                                        wmsTo.setToLocation(toLocationId);

                                        toLocationName = s.getCell(20, i).getContents();
                                        wmsTo.setToWhName(toLocationName);

                                        toSendQty = s.getCell(27, i).getContents();
                                        wmsTo.setTotalOrderedQty(toSendQty);

                                        toReceivedQty = s.getCell(28, i).getContents();
                                        wmsTo.setTotalReceivedQty(toReceivedQty);

                                        toSendAmt = s.getCell(29, i).getContents();
                                        wmsTo.setTotalSendAmt(toSendAmt);

                                        toReceivedAmt = s.getCell(30, i).getContents();
                                        wmsTo.setTotalReceivedAmt(toReceivedAmt);

                                        wmsTo.setStatus(status);
                                        wmsTo.setUserId(userId + "");

                                        status1 = wmsBP.dzInvoiceUploadExcel(wmsTo);
//                                            status1 = wmsBP.dzProductUpload(wmsTo);

                                    }

                                }
                            }
                        } else {
                            request.setAttribute("erruserIdorMessage", "Grn Upload updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            wms.setUserId(userId + "");

            ArrayList getDzInvoiceTemp = new ArrayList();
            getDzInvoiceTemp = wmsBP.getDzInvoiceTemp(wms);
            request.setAttribute("getDzInvoiceTemp", getDzInvoiceTemp);

            ArrayList getDzInvoiceTempCount = new ArrayList();
            getDzInvoiceTempCount = wmsBP.getDzInvoiceTempCount(wms);
            request.setAttribute("getDzInvoiceTempCount", getDzInvoiceTempCount);

            ArrayList getDzInvoiceErro = new ArrayList();
            getDzInvoiceErro = wmsBP.getDzInvoiceErro(wms);
            request.setAttribute("getDzInvoiceErro", getDzInvoiceErro);

            path = "content/wms/dzInvoiceUpload.jsp";

            if (getDzInvoiceTemp.size() > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Transfer Order Uploaded Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);

    }

    public ModelAndView dzInvoiceUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        try {
            String param = request.getParameter("param");
            wmsTO.setWhId(whId);
            wmsTO.setUserId(userId + "");
            if ("save".equals(param)) {
                int insert = wmsBP.dzInvoiceUpload(wmsTO);
                if (insert > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Transfer Order Upload Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Transfer Order Upload Failed");
                }
                path = "content/wms/dzInvoiceUpload.jsp";
            } else {
                int delete = wmsBP.delDzInvoiceDetailsTemp(wmsTO);
                path = "content/wms/dzInvoiceUpload.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View PdzInvoiceUploadroductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView dzInvoiceView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        wmsCommand = command;
        int insertStatus = 0;
        int status1 = 0;
        try {
            WmsTO wms = new WmsTO();
            int userId = (Integer) session.getAttribute("userId");
            wms.setUserId(userId + "");
            String whId = (String) session.getAttribute("hubId");
            wms.setUserId(whId);
            String param = request.getParameter("param");

            if ("export".equals(param)) {

                path = "content/wms/dzInvoiceDetailsViewExport.jsp";
            } else if ("search".equals(param)) {

                String hubId = request.getParameter("whId");
                if ("".equals(hubId) || hubId == null) {
                    hubId = whId;
                }

                String fromDate = request.getParameter("fromDate");
                String toDate = request.getParameter("toDate");
                System.out.println("to check from date  ==========================" + fromDate);
                wms.setFromDate(fromDate);
                wms.setToDate(toDate);
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);

                wms.setWhId(hubId);
                request.setAttribute("whId", hubId);
                wms.setUserId(userId + "");

                path = "content/wms/dzInvoiceView.jsp";
            } else {
                path = "content/wms/dzInvoiceView.jsp";
            }

            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseList();
            request.setAttribute("getWareHouseList", getWareHouseList);

            ArrayList getdzInvoiceUpload = new ArrayList();
            getdzInvoiceUpload = wmsBP.getdzInvoiceUpload(wms);
            request.setAttribute("getdzInvoiceUpload", getdzInvoiceUpload);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);

    }

    public ModelAndView dzStockDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        wmsCommand = command;
        int insertStatus = 0;
        int status1 = 0;
        try {

            WmsTO wms = new WmsTO();
            int userId = (Integer) session.getAttribute("userId");
            wms.setUserId(userId + "");

            String param = request.getParameter("param");

            if ("save".equals(param)) {
                String whId = request.getParameter("warehouseId");
                String fromDate = request.getParameter("fromDate");
                String toDate = request.getParameter("toDate");
                wms.setWhId(whId);
                wms.setFromDate(fromDate);
                wms.setToDate(toDate);

                System.out.println("================> SS" + whId);

                path = "content/wms/dzStockDetails.jsp";

            } else if ("export".equals(param)) {

                path = "content/wms/dzStockDetailsViewExport.jsp";
            } else {

                path = "content/wms/dzStockDetails.jsp";
            }

            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseList();
            request.setAttribute("getWareHouseList", getWareHouseList);

            ArrayList getStockDetails = new ArrayList();
            getStockDetails = wmsBP.getDzStockDetails(wms);
            request.setAttribute("getStockDetails", getStockDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);

    }

    public ModelAndView imDailyOutward(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        try {
            String whId = (String) session.getAttribute("hubId");
            wmsTO.setUserId(userId + "");
            wmsTO.setWhId(whId);
            String param = request.getParameter("param");
            if ("save".equals(param)) {

                String warehouse = request.getParameter("warehouse");
                wmsTO.setWhId(warehouse);

                String date = request.getParameter("date");
                wmsTO.setDate(date);

                String packets = request.getParameter("packets");
                wmsTO.setPackets(packets);

                String tons = request.getParameter("tons");
                wmsTO.setTons(tons);

                String milk = request.getParameter("milk");
                wmsTO.setMilk(milk);

                String FandV = request.getParameter("FAndV");
                wmsTO.setFandv(FandV);

                wmsTO.setUserId(userId + "");
                int insertStatus = wmsBP.insertDailyOutward(wmsTO);
            }

            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseList();
            System.out.println("getWareHouseList " + getWareHouseList.size());
            request.setAttribute("getWareHouseList", getWareHouseList);

            ArrayList getDailyOutWard = new ArrayList();
            getDailyOutWard = wmsBP.getDailyOutWard(wmsTO);
            request.setAttribute("getDailyOutWard", getDailyOutWard);

            path = "content/wms/dailyOutward.jsp";

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView imDailyLUDetails(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        try {
            String whId = (String) session.getAttribute("hubId");
            wmsTO.setUserId(userId + "");
            wmsTO.setWhId(whId);
            String param = request.getParameter("param");
            if ("save".equals(param)) {

                String type = request.getParameter("type");
                wmsTO.setType(type);

                String date = request.getParameter("date");
                wmsTO.setDate(date);

                String grnQty = request.getParameter("grnQty");
                wmsTO.setGrnQty(grnQty);

                String remarks = request.getParameter("remarks");
                wmsTO.setRemarks(remarks);
                wmsTO.setUserId(userId + "");

                int insertStatus = wmsBP.insertimDailyLUDetails(wmsTO);
            }

            ArrayList imDailyLUDetails = new ArrayList();
            imDailyLUDetails = wmsBP.imDailyLUDetails(wmsTO);
            request.setAttribute("imDailyLUDetails", imDailyLUDetails);

            path = "content/wms/imDailyLUDetails.jsp";

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView dzPickListMaster(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        wmsCommand = command;
        int insertStatus = 0;
        int status1 = 0;
        String[] type = request.getParameterValues("selectedStatus");
        try {
            WmsTO wms = new WmsTO();
            int userId = (Integer) session.getAttribute("userId");
            String hubId = (String) session.getAttribute("hubId");
            wms.setWhId(hubId + "");
            wms.setUserId(userId + "");
            ArrayList dzPickListMaster = new ArrayList();
            dzPickListMaster = wmsBP.dzPickListMasterPick(wms);
            request.setAttribute("dzPickListMaster", dzPickListMaster);
            path = "content/wms/dzPickListMaster.jsp";

        } catch (FPRuntimeException exception) {

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);

    }

    public ModelAndView dzPickListMasterView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        String whId = (String) session.getAttribute("hubId");
        String invoiceId = request.getParameter("invoiceId");

        request.setAttribute("invoiceIds", invoiceId);
        wmsCommand = command;
        int insertStatus = 0;
        int status1 = 0;
        String[] type = request.getParameterValues("selectedStatus");
        try {

            WmsTO wms = new WmsTO();
            int userId = (Integer) session.getAttribute("userId");
            wms.setUserId(userId + "");
            wms.setWhId(whId);
            wms.setInvoiceId(invoiceId);
            String param = request.getParameter("param");

            if ("print".equals(param)) {
                path = "content/wms/dzPicklistMasterPrint.jsp";
            } else {
                path = "content/wms/dzPickListMasterView.jsp";
            }

            ArrayList dzPickListMasterView = new ArrayList();
            dzPickListMasterView = wmsBP.dzPickListMasterView(wms);
            request.setAttribute("dzPickListMasterView", dzPickListMasterView);

        } catch (FPRuntimeException exception) {

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);

    }

    public ModelAndView dzLrGenerate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        String whId = (String) session.getAttribute("hubId");
        String invoiceId = request.getParameter("invoiceId");

        request.setAttribute("invoiceIds", invoiceId);
        wmsCommand = command;
        int insertStatus = 0;
        int status1 = 0;

        try {

            WmsTO wms = new WmsTO();
            int userId = (Integer) session.getAttribute("userId");

            String param = request.getParameter("param");

//            if ("print".equals(param)){
//                path = "content/wms/dzPicklistMasterPrint.jsp";
//            }
//            else {
            path = "content/wms/dzLrGenerate.jsp";
//            }

        } catch (FPRuntimeException exception) {

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView dzDispatchPlanning(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        String param = request.getParameter("param");

        WmsTO wmsTO = new WmsTO();
        OpsTO opsTO = new OpsTO();
        wmsTO.setUserId(userId + "");
        wmsTO.setWhId(whId);
        opsTO.setWhId(whId);
        try {
            if ("lr".equals(param)) {
                wmsTO.setWhId(whId);
                wmsTO.setUserId(userId + "");

                String invoiceIdList = request.getParameter("invoiceIdList");
                String supplierId = request.getParameter("supplierId");
                String vendorId = request.getParameter("vendorId");
                String deviceId = request.getParameter("deviceId");
                String vehicleNo = request.getParameter("vehicleNo");
                String noOfCrates = request.getParameter("noOfCrates");
                String vehicleType = request.getParameter("vehicleType");
                String driverName = request.getParameter("driverName");
                String driverMobile = request.getParameter("driverMobile");
                String dispatchDate = request.getParameter("dispatchDate");
                String tripStartTime = request.getParameter("tripStartTime");
                String remarks = request.getParameter("remarks");
                String startKm = request.getParameter("startKm");
                wmsTO.setPicklistIds(invoiceIdList);
                wmsTO.setSupplierId(supplierId);
                wmsTO.setVendorId(vendorId);
                wmsTO.setDeviceId(deviceId);
                wmsTO.setVehicleNo(vehicleNo);
                wmsTO.setNoOfCrates(noOfCrates);
                wmsTO.setVehicleType(vehicleType);
                wmsTO.setDriverName(driverName);
                wmsTO.setMobile(driverMobile);
                wmsTO.setDate(dispatchDate);
                wmsTO.setTime(tripStartTime);
                wmsTO.setRemarks(remarks);
                wmsTO.setStartKm(startKm);
                int insertDZLrStatus = wmsBP.insertDZLrStatus(wmsTO);
            } else {
            }

            wmsTO.setWhId(whId);
            wmsTO.setUserId(userId + "");

            ArrayList getSupplierList = new ArrayList();
            getSupplierList = wmsBP.getSupplierList();
            request.setAttribute("getSupplierList", getSupplierList);

            ArrayList getImVendorList = new ArrayList();
            getImVendorList = wmsBP.getImVendorList(wmsTO);
            request.setAttribute("getImVendorList", getImVendorList);

            ArrayList getImDealerList = new ArrayList();
            getImDealerList = wmsBP.getImDealerList(wmsTO);
            request.setAttribute("getImDealerList", getImDealerList);

            ArrayList getDeviceIdList = opsBP.getDeviceIdList(opsTO);
            request.setAttribute("deviceList", getDeviceIdList);

            ArrayList getVehicleTypeList = new ArrayList();
            getVehicleTypeList = wmsBP.getVehicleTypeList();
            System.out.println("getVehicleTypeList=====" + getVehicleTypeList.size());
            request.setAttribute("getVehicleTypeList", getVehicleTypeList);

            ArrayList invoiceMaster = wmsBP.invoiceMaster(wmsTO);
            request.setAttribute("invoiceMaster", invoiceMaster);
            System.out.println("invoiceMaster=====" + invoiceMaster.size());

            path = "content/wms/dzAddDispatchPage.jsp";

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        if ("lr".equals(param)) {
            return new ModelAndView("dzLrView.do");
        } else {
            return new ModelAndView(path);
        }
    }

    public void getDzInvoiceDetailsPickList(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();

        String path = "";
        String whId = (String) session.getAttribute("hubId");
//        String invoiceId = request.getParameter("invoiceId");
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            String invoiceId = request.getParameter("invoiceId");
            System.out.println("invoiceId == > - = >" + invoiceId);

            int userId = (Integer) session.getAttribute("userId");
            wmsTO.setUserId(userId + "");
            wmsTO.setWhId(whId);
            wmsTO.setInvoiceId(invoiceId);

            ArrayList getbinList = wmsBP.getDzLrInvoiceDetails(wmsTO);
            request.setAttribute("driverList", getbinList);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getbinList.iterator();

            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                wmsTO = (WmsTO) itr.next();
                jsonObject.put("id", wmsTO.getId());
                jsonObject.put("prouductName", wmsTO.getProductName());
                jsonObject.put("categoryName", wmsTO.getCategoryName());
                jsonObject.put("qty", wmsTO.getQty());
                jsonObject.put("amount", wmsTO.getAmount());
                jsonObject.put("toNo", wmsTO.getToNo());
                jsonObject.put("partyName", wmsTO.getPartyName());
                jsonObject.put("address", wmsTO.getAddress());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getDzInvoiceDetailsPickListMaster(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();

        String path = "";
        String whId = (String) session.getAttribute("hubId");
//        String invoiceId = request.getParameter("invoiceId");
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            String invoiceId = request.getParameter("invoiceId");
            System.out.println("invoiceId == > - = >" + invoiceId);

            int userId = (Integer) session.getAttribute("userId");
            wmsTO.setUserId(userId + "");
            wmsTO.setWhId(whId);
            wmsTO.setInvoiceId(invoiceId);

            ArrayList getbinList = wmsBP.dzPickListMasterView(wmsTO);
            request.setAttribute("driverList", getbinList);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getbinList.iterator();

            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                wmsTO = (WmsTO) itr.next();
                jsonObject.put("id", wmsTO.getId());
                jsonObject.put("prouductName", wmsTO.getProductName());
                jsonObject.put("categoryName", wmsTO.getCategoryName());
                jsonObject.put("qty", wmsTO.getQty());
                jsonObject.put("amount", wmsTO.getAmount());
                jsonObject.put("toNo", wmsTO.getToNo());
                jsonObject.put("partyName", wmsTO.getPartyName());
                jsonObject.put("address", wmsTO.getAddress());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView dzLrView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        wmsCommand = command;

        WmsTO wmsTO = new WmsTO();
        OpsTO opsTO = new OpsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
            int userId = (Integer) session.getAttribute("userId");
            wmsTO.setUserId(userId + "");
            String whId = (String) session.getAttribute("hubId");

            wmsTO.setUserId(whId);
            wmsTO.setWhId(whId);
            wmsTO.setUserId(userId + "");

            request.setAttribute("whId", whId);
            String param = request.getParameter("param");
            System.out.println("param===" + param);

            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseList();
            request.setAttribute("getWareHouseList", getWareHouseList);

            if ("view".equals(param)) {
                String lrId = request.getParameter("lrId");
                wmsTO.setLrId(lrId);
                ArrayList getDzInvoiceLrDetails = new ArrayList();
                getDzInvoiceLrDetails = wmsBP.getDzInvoiceLrDetails(wmsTO);
                request.setAttribute("getImInvoiceDetails", getDzInvoiceLrDetails);
                path = "content/wms/dzLrViewDetails.jsp";
            } else if ("print".equals(param)) {
                String lrId = request.getParameter("lrId");
                wmsTO.setLrId(lrId);

                ArrayList dzLrDetails = wmsBP.getDzLrDetails(wmsTO);
                request.setAttribute("getImLrDetails", dzLrDetails);

                ArrayList dzSubLrDetails = wmsBP.dzSubLrDetails(wmsTO);
                request.setAttribute("getImSubLrDetails", dzSubLrDetails);
                path = "content/wms/dzLrViewPrint.jsp";
            } else if ("search".equals(param)) {
                String hubId = request.getParameter("whId");
                System.out.println("==================> " + hubId);
                if ("".equals(hubId) || hubId == null) {
                    hubId = whId;
                } else {
                    System.out.println("nt ");
                }

                String fromDate = request.getParameter("fromDate");
                String toDate = request.getParameter("toDate");
                System.out.println("to check from date  ==========================" + fromDate);
                wmsTO.setFromDate(fromDate);
                wmsTO.setToDate(toDate);
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);

                wmsTO.setWhId(hubId);
                request.setAttribute("whId", hubId);
                wmsTO.setUserId(userId + "");

                path = "content/wms/dzLrView.jsp";
            } else {
                path = "content/wms/dzLrView.jsp";
            }

            ArrayList getDzLrMaster = new ArrayList();
            getDzLrMaster = wmsBP.getDzLrMaster(wmsTO);
            request.setAttribute("getDzLrMaster", getDzLrMaster);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView dzCratesReport(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        WmsTO wmsTO = new WmsTO();
        try {
            String hubId = request.getParameter("whId");
            if ("".equals(hubId) || hubId == null) {
                hubId = whId;
            }
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            wmsTO.setWhId(hubId);
            request.setAttribute("whId", hubId);
            wmsTO.setUserId(userId + "");
            String param = request.getParameter("param");
            if ("view".equals(param)) {
                String sublrId = request.getParameter("sublrId");
                wmsTO.setSubLrId(sublrId);
                ArrayList cratesReportView = new ArrayList();
                cratesReportView = wmsBP.dzCratesReportView(wmsTO);
                request.setAttribute("cratesReportView", cratesReportView);

                path = "content/wms/dzCratesReportView.jsp";
            } else {

                path = "content/wms/dzCratesReport.jsp";
            }
            ArrayList whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);
            ArrayList cratesReport = new ArrayList();
            cratesReport = wmsBP.dzCratesReport(wmsTO);
            request.setAttribute("cratesReport", cratesReport);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    public ModelAndView dzFreightUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        wmsCommand = command;
        String menuPath = "Connect Operation  >> DC Freight Update ";
        int updateFreight = 0;
        int userId = (Integer) session.getAttribute("userId");

        try {
            String whId = (String) session.getAttribute("hubId");
            String warehouseId = request.getParameter("whId");
            if (warehouseId == null || "".equals(warehouseId)) {
                warehouseId = whId;
            }
            String status = request.getParameter("statusType");
            wmsTO.setStatus(status);
            wmsTO.setWhId(warehouseId);
            request.setAttribute("whId", warehouseId);
            request.setAttribute("statusType", status);
            String param = request.getParameter("param");
            System.out.println("parsm===" + param);
            if ("Submit".equals(param)) {
                String[] check = request.getParameterValues("selectedStatus");
                String[] lrnNo = request.getParameterValues("subLr");
                String[] lrNo = request.getParameterValues("lrNo");
                String[] vendorName = request.getParameterValues("vendorName");
                String[] customerName = request.getParameterValues("customerName");
                String[] dispatchQuantity = request.getParameterValues("dispatchQuantity");
                String[] billQuantity = request.getParameterValues("billQuantity");
                String[] freightAmt = request.getParameterValues("freightAmt");
                String[] otherAmt = request.getParameterValues("otherAmt");
                String[] totalAmt = request.getParameterValues("totalAmt");
                updateFreight = wmsBP.dzUpdateFreightAmountNew(userId, check, lrnNo, lrNo, vendorName, customerName, dispatchQuantity, billQuantity, freightAmt, otherAmt, totalAmt);
                path = "content/wms/dzFreightUpdate.jsp";

            } else if ("exportExcel".equals(param)) {
                path = "content/wms/dzFreightUpdateExportExcel.jsp";
            } else {
                path = "content/wms/dzFreightUpdate.jsp";
            }
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
            }
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            ArrayList whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);

            ArrayList dzFreightDetails = new ArrayList();
            dzFreightDetails = wmsBP.dzFreightDetails(wmsTO);
            request.setAttribute("imFreightDetails", dzFreightDetails);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView imPodUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        wmsCommand = command;
        String menuPath = "Connect Operation  >> POD Upload ";
        String fromDate = "", toDate = "";
        int updateFreight = 0;
        Part partObj = null;
        boolean isMultipart = false;
        try {
            String whId = (String) session.getAttribute("hubId");
            String param = request.getParameter("param");
            String podStatus = request.getParameter("statusType");
            wmsTO.setPodStatus(podStatus);
            wmsTO.setWhId(whId);
//            String hubId = request.getParameter("whId");
//            System.out.println("hubId----------"+hubId);
//            wmsTO.setWarehouseId(hubId);
//            request.setAttribute("hubId", hubId);

            System.out.println("param===" + param);

            path = "content/wms/imPodUpload.jsp";
            if (wmsCommand.getFromDate() != null && wmsCommand.getFromDate() != "") {
                wmsTO.setFromDate(wmsCommand.getFromDate());
                fromDate = wmsCommand.getFromDate();
            }

            if (wmsCommand.getToDate() != null && wmsCommand.getToDate() != "") {
                wmsTO.setToDate(wmsCommand.getToDate());
                toDate = wmsCommand.getToDate();
            }

            String statusType = request.getParameter("statusType");
            wmsTO.setStatusType(statusType);

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
//          wmsTO.setStatusType("0");
            ArrayList whList = new ArrayList();
            whList = wmsBP.getWareHouse(wmsTO);
            request.setAttribute("whList", whList);

            ArrayList imPodUploadDetails = new ArrayList();
            imPodUploadDetails = wmsBP.imPodUploadDetails(wmsTO);
            request.setAttribute("imPodUploadDetails", imPodUploadDetails);
            if ("save".equals(param)) {
                String deliveryDate = request.getParameter("deliveryDate");
                String lrNo = request.getParameter("lrNo");
                wmsTO.setDeliveryDate(deliveryDate);
                wmsTO.setLrnNo(lrNo);
//                int updateLrNo = wmsBP.updateConnectLrNo(wmsTO);
            } else if ("ExportExcel".equals(param)) {
                path = "content/wms/imPodUploadExcel.jsp";
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView dzOtherStock(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        try {
            String param = request.getParameter("param");
            wmsTO.setWhId(whId);
            wmsTO.setUserId(userId + "");
            if ("save".equals(param)) {
                int insert = wmsBP.insertDzGrnUpload(wmsTO);
                if (insert > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Grn Upload Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Grn Upload Failed");
                }
                path = "content/wms/dzOtherStockUpload.jsp";
            } else {
                int delDzGrnDetailsTemp = wmsBP.delDzGrnDetailsTemp(wmsTO);
                path = "content/wms/dzOtherStockUpload.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView dzOtherStockUploadView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        wmsCommand = command;
        int insertStatus = 0;
        int status1 = 0;
        String whId = (String) session.getAttribute("hubId");
        String param = request.getParameter("param");
        try {

            WmsTO wms = new WmsTO();
            wms.setWhId(whId);

            ArrayList getdzGrnUpload = new ArrayList();
            getdzGrnUpload = wmsBP.getdzGrnUpload(wms);
            request.setAttribute("getdzGrnUpload", getdzGrnUpload);
            String poNo = request.getParameter("poNo");
            wms.setPoNo(poNo);
            if ("view".equals(param)) {
                ArrayList getDzGrnDetails = new ArrayList();
                getDzGrnDetails = wmsBP.getDzGrnDetails(wms);
                request.setAttribute("getDzGrnDetails", getDzGrnDetails);
                path = "content/wms/dzGrnUploadViewDetails.jsp";
            } else if ("print".equals(param)) {
                ArrayList getAllDzGrnDetails = new ArrayList();
                getAllDzGrnDetails = wmsBP.getAllDzGrnDetails(wms);
                request.setAttribute("getAllDzGrnDetails", getAllDzGrnDetails);
                path = "content/wms/dzInWordPrintGrn.jsp";
            } else {
                path = "content/wms/dzGrnUploadView.jsp";
            }
//            path = "content/wms/dzGrnUploadView.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);

    }

    public ModelAndView dzOtherStockUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = (Integer) session.getAttribute("userId");
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        String param = "";

        String category = "";
        String productId = "";
        String productFullName = "";
        String productVariantId = "";
        String Qty = "";

        int insertStatus = 0;

        int status1 = 0;

        param = request.getParameter("param");

        try {

            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

            if (isMultipart) {

                userId = (Integer) session.getAttribute("userId");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadedxls/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        int len = splitFileNames.length;
                        fileFormat = splitFileNames[len - 1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = uploadedFileName;
                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                File f1 = new File(actualFilePath);
                                f1.renameTo(new File(tempFilePath));
//                               
                                if ("xlsx".equals(fileFormat)) {
                                    FileInputStream fis = new FileInputStream(tempFilePath);
                                    XSSFWorkbook wb = new XSSFWorkbook(fis);
                                    XSSFSheet sheet = wb.getSheetAt(0);
                                    int value = sheet.getLastRowNum();

                                    int sno = 0;
                                    Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                    while (itr.hasNext()) {
                                        Row row = itr.next();
                                        if (sno > 0) {
//                                                System.out.println("print the all the date from the xlxs or xls " + row.getCell(0) + row.getCell(1) + row.getCell(2) + row.getCell(3) + row.getCell(4) + row.getCell(5) + row.getCell(6) + row.getCell(7));
                                            WmsTO wmsTo = new WmsTO();

                                            if (row.getCell(0) != null && !"".equals(row.getCell(0))) {
                                                row.getCell(0).setCellType(row.getCell(0).CELL_TYPE_STRING);
//                                                productLineEntryId = row.getCell(0).getStringCellValue();
//                                                wmsTo.setProductLineEntryId(productLineEntryId);

                                            }

                                            if (row.getCell(1) != null && !"".equals(row.getCell(1))) {
                                                row.getCell(1).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                category = row.getCell(1).getStringCellValue();
                                                wmsTo.setCategoryName(category);

                                            }

                                            if (row.getCell(2) != null && !"".equals(row.getCell(2))) {
                                                row.getCell(2).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                productId = row.getCell(2).toString();
                                                wmsTo.setProductId(productId);

                                            }

                                            if (row.getCell(3) != null && !"".equals(row.getCell(3))) {
                                                row.getCell(3).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                productVariantId = row.getCell(3).toString();
                                                wmsTo.setProductVariantId(productVariantId);

                                            }

                                            if (row.getCell(4) != null && !"".equals(row.getCell(4))) {
                                                row.getCell(4).setCellType(row.getCell(4).CELL_TYPE_STRING);
                                                productFullName = row.getCell(4).getStringCellValue();
                                                wmsTo.setName(productFullName);
                                            }

                                            if (row.getCell(5) != null && !"".equals(row.getCell(5))) {
                                                row.getCell(5).setCellType(row.getCell(5).CELL_TYPE_STRING);
//                                                orderedQty = row.getCell(5) + "";
//                                                wmsTo.setOrderedQty(orderedQty);
//                                                    System.out.println(orderedQty + "  = testin orderedQty's value");
                                            }

//                                          wmsTo.setStatus(status);
                                            wmsTo.setUserId(userId + "");

                                            status1 = wmsBP.dzGrnUpload(wmsTo);

                                        }
                                        sno++;

                                    }
                                } else if ("xls".equals(fileFormat)) {
                                    int count = 0;

                                    WorkbookSettings ws = new WorkbookSettings();
                                    ws.setLocale(new Locale("en", "EN"));
                                    Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                    Sheet s = workbook.getSheet(0);

                                    for (int i = 1; i < s.getRows(); i++) {
                                        WmsTO wmsTo = new WmsTO();
                                        count++;

//                                        productLineEntryId = s.getCell(0, i).getContents();
//                                        wmsTo.setProductLineEntryId(productLineEntryId);
                                        category = s.getCell(1, i).getContents();
                                        wmsTo.setCategoryName(category);

                                        productId = s.getCell(2, i).getContents();
                                        wmsTo.setProductId(productId);

                                        productVariantId = s.getCell(3, i).getContents();
                                        wmsTo.setProductVariantId(productVariantId);

                                        productFullName = s.getCell(4, i).getContents();
                                        wmsTo.setName(productFullName);
//
//                                        orderedQty = s.getCell(5, i).getContents();
//                                        wmsTo.setOrderedQty(orderedQty);
//
//                                     
//                                        wmsTo.setStatus(status);
//                                        wmsTo.setUserId(userId + "");

                                        status1 = wmsBP.dzGrnUpload(wmsTo);
//                                            status1 = wmsBP.dzProductUpload(wmsTo);

                                    }

                                }
                            }
                        } else {
                            request.setAttribute("erruserIdorMessage", "Grn Upload updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            wms.setUserId(userId + "");

            ArrayList getdzGrnDetailsTemp = new ArrayList();
            getdzGrnDetailsTemp = wmsBP.getdzGrnDetailsTemp(wms);
            request.setAttribute("getdzGrnDetailsTemp", getdzGrnDetailsTemp);

            path = "content/wms/dzOtherStockUpload.jsp";

            if (getdzGrnDetailsTemp.size() > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "GRN Uploaded Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);

    }

    public ModelAndView imUploadPage(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        OpsTO opsTO = new OpsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        try {
            wmsTO.setWhId(hubId);
            int updateconnectInvoice = 0;
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            String param = request.getParameter("param");
            String subLrId = request.getParameter("subLrId");
            request.setAttribute("subLrId", subLrId);
            String dispatchid = request.getParameter("dispatchid");
            request.setAttribute("dispatchid", dispatchid);
            String lrnNo = request.getParameter("lrnNo");
            System.out.println("lrnNo--------------" + lrnNo);
            wmsTO.setLrnNo(lrnNo);
            String deliveryDate = wmsBP.getImDeliveryDate(wmsTO);
            request.setAttribute("deliveryDate", deliveryDate);
            request.setAttribute("lrnNo", lrnNo);
            String dispatchDetailId = request.getParameter("dispatchDetailId");
            request.setAttribute("dispatchDetailId", dispatchDetailId);
            String lrdate = request.getParameter("lrdate");
            request.setAttribute("lrdate", lrdate);
            String invoiceNo = request.getParameter("invoiceNo");
            request.setAttribute("invoiceNo", invoiceNo);
            String invoiceDate = request.getParameter("invoiceDate");
            request.setAttribute("invoiceDate", invoiceDate);

            String CustomerName = request.getParameter("CustomerName");
            request.setAttribute("CustomerName", CustomerName);

            path = "content/wms/imUploadPage.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleImPODupload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        wmsCommand = command;
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        path = "content/vendor/manageVendor.jsp";
        String pageTitle = "Add Vendor";
        request.setAttribute("pageTitle", pageTitle);
        int vendorId = 0;

        String menuPath = "Vendor  >>  ManageVendor  >> Add";

        //file upload
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;
        int m = 0;
        int n = 0;
        int p = 0;
        int s = 0;
        String tripSheetId1 = "";
        String subLrId1 = "";
        String dispatchid1 = "";
        String lrnNo1 = "";
        String plannedTime1 = "";
        String vendorName1 = "";
        String dispatchDetailId1 = "";
        String plannedDate1 = "";
        String tinNo1 = "";

        //        String[] podRemarks1 = new String[10];
        String[] fileSaved = new String[10];
        String[] remarks = new String[10];
        //        String[] lrNumber1 = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        String[] city = new String[10];

        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            WmsTO wmsTO = new WmsTO();

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
//                System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
//                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/instamart/");
//                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
//                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        Date now = new Date();
                        String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                        String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                        fPart = (FilePart) partObj;
                        uploadedFileName[j] = fPart.getFileName();
//                        System.out.println("fPart.getFileName() = " + fPart.getFileName());

                        if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {
//                            System.out.println("partObj.getName() = " + partObj.getName());
                            String[] splitFileName = uploadedFileName[j].split("\\.");
                            fileSavedAs = uploadedFileName[j];
//                            System.out.println("fileSavedAs = " + fileSavedAs);
                            fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath[j] = tempServerFilePath + "/" + fileSaved[j];
                            actualFilePath = actualServerFilePath + "/" + tempFilePath;
//                            System.out.println("tempPath..." + tempFilePath);
//                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
//                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
//                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath[j]));
//                            System.out.println("tempPath = " + tempFilePath);
//                            System.out.println("actPath = " + actualFilePath);
                            //                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            //                            String part1 = parts.replace("\\", "");
                        }

//                        System.out.println("fileName..." + fileName);
                        String ss = request.getParameter("subLrId");
                        wmsTO.setSubLr(ss);
                        System.out.println("===========>>>>>" + ss);
                        j++;
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("subLrId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            subLrId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("dispatchid")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            dispatchid1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("lrnNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            lrnNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("plannedTime")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            plannedTime1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("deliveryDate")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            plannedDate1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("dispatchDetailId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            dispatchDetailId1 = paramPart.getStringValue();
                        }

                    }
                }
            }
            String whId = (String) session.getAttribute("hubId");
            wmsTO.setWhId(whId);
            int index = j;
            String[] file = new String[index];
//            System.out.println(" " + file);

            String[] saveFile = new String[index];;

            int status = 0;

            //file upload
            String podName1 = request.getParameter("podName1");
            wmsTO.setPodName1(podName1);
            String podName2 = request.getParameter("podName2");
            wmsTO.setPodName2(podName2);
            String podName3 = request.getParameter("podName3");
            wmsTO.setPodName3(podName3);
            String remarks1 = request.getParameter("remarks1");
            wmsTO.setRemarks1(remarks1);
            String remarks2 = request.getParameter("remarks2");
            wmsTO.setRemarks2(remarks2);
            String remarks3 = request.getParameter("remarks3");
            wmsTO.setRemarks3(remarks3);
            wmsTO.setLrnNo(lrnNo1);
            wmsTO.setDispatchDetailId(dispatchDetailId1);
            wmsTO.setPlannedDate(plannedDate1);
            wmsTO.setDeliveryDate(plannedDate1);
            wmsTO.setPlannedTime(plannedTime1);
            wmsTO.setDispatchid(dispatchid1);
            wmsTO.setSubLrId(subLrId1);
            System.out.println("plannedDate1------------------" + plannedDate1);

            System.out.println(subLrId1 + "<<<<<<<<-------->>>>>>>");
            //             String[] mfrids = vendorCommand.getMfrids();
            //             String[] mfrids = vendorCommand.getMfrids();
            //             System.out.println("mfrids222"+mfrids);
            //  String[] selectedIndex = vendorCommand.getSelectedIndex();
            String selectedIndex = "0";
            //            System.out.println("selectedIndexpp"+selectedIndex);

//            System.out.println("j = " + j);
//            System.out.println("file = " + file.length);
//            System.out.println("tempFilePath[0] = " + tempFilePath[0]);
//            System.out.println("tempFilePath[1] = " + tempFilePath[1]);
            for (int x = 0; x < j; x++) {
//                System.out.println("tempFilePath[x] = " + tempFilePath[x]);
                file[x] = tempFilePath[x];
//                System.out.println("saveFile[x] = " + fileSaved[x]);
                saveFile[x] = fileSaved[x];
            }
            vendorId = wmsBP.processInsertImPODDetails(wmsTO, selectedIndex, userId, file, saveFile, remarks);
//            int updateConnectLrNo = wmsBP.updateConnectLrNo(wmsTO);
            request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));
            mv = imPodUpload(request, response, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView imPodUploadExcel(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        wmsCommand = command;
        String fromDate = "", toDate = "", tripCode = "";
        int deliveryDateUpdate = 0;
        try {

            String param = request.getParameter("param");
            System.out.println("parsm===" + param);
            if ("upload".equals(param)) {
                String newFileName = "", actualFilePath = "";
                String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
                boolean isMultipart = false;
                Part partObj = null;
                FilePart fPart = null;
                String[] fileSaved = new String[10];
                String uploadedFileName = "";
                String fileFormat = "";
                String tempFilePath = "";
                isMultipart = ServletFileUpload.isMultipartContent(request);
                if (isMultipart) {
                    MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                    while ((partObj = parser.readNextPart()) != null) {
                        System.out.println("part Name:" + partObj.getName());
                        if (partObj.isFile()) {
                            actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                            System.out.println("Server Path == " + actualServerFilePath);
                            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                            System.out.println("Server Path After Replace== " + tempServerFilePath);
                            fPart = (FilePart) partObj;
                            int userId = (Integer) session.getAttribute("userId");
                            wmsTO.setUserId(userId + "");
                            uploadedFileName = fPart.getFileName();
                            System.out.println("uploadedFileName====" + uploadedFileName);
                            String[] splitFileNames = uploadedFileName.split("\\.");
                            int len = splitFileNames.length;
                            fileFormat = splitFileNames[len - 1];
                            System.out.println("fileFormat==" + fileFormat);
                            if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                                System.out.println("Ses");
                                if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                    String[] splitFileName = uploadedFileName.split("\\.");
                                    fileSavedAs = uploadedFileName;
                                    System.out.println("splitFileName[0]=" + splitFileName[0]);
                                    System.out.println("splitFileName[0]=" + splitFileName[1]);

                                    fileName = fileSavedAs;
                                    tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                    actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                    System.out.println("tempPath..." + tempFilePath);
                                    System.out.println("actPath..." + actualFilePath);
                                    long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                    System.out.println("fileSize..." + fileSize);
                                    File f1 = new File(actualFilePath);
                                    System.out.println("check " + f1.isFile());
                                    f1.renameTo(new File(tempFilePath));
                                    System.out.println("tempPath = " + tempFilePath);
                                    if ("xlsx".equals(fileFormat)) {
                                        FileInputStream fis = new FileInputStream(tempFilePath);
                                        XSSFWorkbook wb = new XSSFWorkbook(fis);
                                        XSSFSheet sheet = wb.getSheetAt(0);
                                        int value = sheet.getLastRowNum();
                                        System.out.println("value  " + value);

                                        String[] check = new String[value];
                                        String[] lrnNo = new String[value];
                                        String[] deliveryDate = new String[value];
                                        String[] remarks = new String[value];
                                        int sno = 0;
                                        Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                        while (itr.hasNext()) {
                                            if (sno > 0) {
                                                Row row = itr.next();
                                                System.out.println("cell " + row.getCell(1));
                                                check[sno - 1] = "1";
                                                lrnNo[sno - 1] = row.getCell(1).getStringCellValue();
                                                deliveryDate[sno - 1] = row.getCell(11).getStringCellValue();
                                                remarks[sno - 1] = row.getCell(12).getStringCellValue();
                                            }
                                            sno++;
                                        }
                                        deliveryDateUpdate = wmsBP.updateConnectDeliveryDate(lrnNo, deliveryDate, remarks, wmsTO);
                                    } else if ("xls".equals(fileFormat)) {
                                        WorkbookSettings ws = new WorkbookSettings();
                                        ws.setLocale(new Locale("en", "EN"));
//                                        HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(new File(actualFilePath)));
//                                        HSSFSheet s = wb.getSheetAt(0);
                                        Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                        Sheet s = workbook.getSheet(0);

                                        int value = s.getRows() - 1;
                                        System.out.println("value  " + value);
                                        String[] check = new String[value];
                                        String[] lrnNo = new String[value];
                                        String[] deliveryDate = new String[value];
                                        String[] remarks = new String[value];
                                        for (int i = 1; i < s.getRows(); i++) {
                                            System.out.println("rows  " + s.getCell(0, i).getContents());
                                            check[i - 1] = "1";
                                            lrnNo[i - 1] = s.getCell(1, i).getContents();
                                            deliveryDate[i - 1] = s.getCell(11, i).getContents();
                                            remarks[i - 1] = s.getCell(12, i).getContents();
                                        }
                                        deliveryDateUpdate = wmsBP.updateConnectDeliveryDate(lrnNo, deliveryDate, remarks, wmsTO);
                                    }
                                }
                            } else {
                                request.setAttribute("errorMessage", "File Upload Failed:");
                            }
                        }
//                        updateFreight = wmsBP.updateFreightAmount(check, lrnNo, freightAmt, haltingAmt, unloadingAmt, otherAmt, totalAmt, remarks);

                    }
                }
            }
            path = "content/wms/imPodUpload.jsp";
            if (wmsCommand.getFromDate() != null && wmsCommand.getFromDate() != "") {
                wmsTO.setFromDate(wmsCommand.getFromDate());
                fromDate = wmsCommand.getFromDate();
            }
            if (wmsCommand.getToDate() != null && wmsCommand.getToDate() != "") {
                wmsTO.setToDate(wmsCommand.getToDate());
                toDate = wmsCommand.getToDate();
            }
            String statusType = request.getParameter("statusType");
            wmsTO.setStatusType(statusType);
            request.setAttribute("statusType", statusType);

            ArrayList whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);

            ArrayList podUploadDetails = new ArrayList();
            podUploadDetails = wmsBP.podUploadDetails(wmsTO);
            request.setAttribute("podUploadDetails", podUploadDetails);

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView crateInventory(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        WmsTO wmsTO = new WmsTO();
        OpsTO opsTO = new OpsTO();
        String param = request.getParameter("param");
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
            wmsTO.setWhId(whId);
            wmsTO.setUserId(userId + "");

            System.out.println("param===" + param);

            ArrayList crateInventoryCount = new ArrayList();
            crateInventoryCount = wmsBP.crateInventoryCount(wmsTO);
            request.setAttribute("crateInventoryCount", crateInventoryCount);

            if ("export".equals(param)) {

                String endTime = request.getParameter("time");
                String date = request.getParameter("datei");
                wmsTO.setEndTime(endTime);
                wmsTO.setDate(date);

                int crateId = wmsBP.insertCrateInventoryMaster(wmsTO);

                wmsTO.setCrateId(crateId + "");

                ArrayList getInventoryExportData = new ArrayList();
                getInventoryExportData = wmsBP.getInventoryExportData(wmsTO);
                request.setAttribute("getTempCurrentDataCrate", getInventoryExportData);

                path = "content/wms/crateExcelExport.jsp";

            } else {
                path = "content/wms/createInventory.jsp";
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public void crateInventorySetGet(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            String barCode = request.getParameter("barCode");
            String currentDate = request.getParameter("currentDate");
            String startTime = request.getParameter("startTime");
            String endTime = request.getParameter("endTime");
            System.out.println("barCode == === === ===" + barCode);
            System.out.println("currentDate == === === ===" + currentDate);
            System.out.println("startTime == === === ===" + startTime);
            System.out.println("endTime == === === ===" + endTime);

            wmsTO.setBarCode(barCode);
            wmsTO.setDate(currentDate);
            wmsTO.setStartTime(startTime);
            wmsTO.setEndTime(endTime);
            wmsTO.setWhId(whId);
            wmsTO.setUserId(userId + "");

            ArrayList crateInventoryGet = wmsBP.crateInventoryGet(wmsTO);
            request.setAttribute("crateInventoryGet", crateInventoryGet);
            JSONArray jsonArray = new JSONArray();

            System.out.println(wmsTO.getStatus() + "ok ok ok");
            request.setAttribute("status", wmsTO.getStatus());
            Iterator itr = crateInventoryGet.iterator();
            String statusOfLog = wmsTO.getStatus();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                wmsTO = (WmsTO) itr.next();

                jsonObject.put("hr", statusOfLog);
                jsonObject.put("totalQty", wmsTO.getTotalQty());
                jsonObject.put("available", wmsTO.getAvailable());
                jsonObject.put("notMap", wmsTO.getNotMap());
                jsonObject.put("inwardPending", wmsTO.getInwardPending());
                jsonObject.put("outwardPending", wmsTO.getOutwardPending());

                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }

            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void imInSetGet(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            String indentId = request.getParameter("indentId");

            System.out.println("barCode == === === ===" + indentId);

            wmsTO.setWhId(whId);
            wmsTO.setInId(indentId);
            wmsTO.setUserId(userId + "");

//            ArrayList crateInventoryGet = wmsBP.crateInventoryGet(wmsTO);
//            request.setAttribute("crateInventoryGet", crateInventoryGet);
            ArrayList imIndentGet = wmsBP.imIndentGet(wmsTO);
            request.setAttribute("imIndentGet", imIndentGet);

            JSONArray jsonArray = new JSONArray();

            System.out.println(wmsTO.getStatus() + "ok ok ok");
            request.setAttribute("status", wmsTO.getStatus());
            Iterator itr = imIndentGet.iterator();
            String statusOfLog = wmsTO.getStatus();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                wmsTO = (WmsTO) itr.next();

                jsonObject.put("hr", statusOfLog);
                jsonObject.put("id", wmsTO.getId());
                jsonObject.put("materialId", wmsTO.getMaterialId());
                jsonObject.put("materialCode", wmsTO.getMaterialCode());
                jsonObject.put("material", wmsTO.getMaterial());
                jsonObject.put("materialDescription", wmsTO.getMaterialDescription());
                jsonObject.put("qty", wmsTO.getQty());
                jsonObject.put("pendingQty", wmsTO.getPendingQty1());
                jsonObject.put("stock", wmsTO.getTotalQty());
                jsonObject.put("uom", wmsTO.getUom2());

                System.out.println("jsonObject af = " + jsonObject);
                jsonArray.put(jsonObject);
                System.out.println(wmsTO.getTotalQty() + "===================================  >>>>>>> ()))");

            }

            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void imGrnSetGet(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            String poId = request.getParameter("poId");

            System.out.println("barCode == === === ===" + poId);

            wmsTO.setWhId(whId);
            wmsTO.setPoId(poId);
            wmsTO.setUserId(userId + "");

            ArrayList imPoMaterialForGrn = wmsBP.imPoMaterialForGrn(wmsTO);
            request.setAttribute("imPoMaterialForGrn", imPoMaterialForGrn);

            JSONArray jsonArray = new JSONArray();

            System.out.println(wmsTO.getStatus() + "ok ok ok");
            request.setAttribute("status", wmsTO.getStatus());
            Iterator itr = imPoMaterialForGrn.iterator();
            String statusOfLog = wmsTO.getStatus();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                wmsTO = (WmsTO) itr.next();

                jsonObject.put("hr", statusOfLog);
                jsonObject.put("id", wmsTO.getId());
                jsonObject.put("materialId", wmsTO.getMaterialId());
                jsonObject.put("poId", wmsTO.getPoId());
                jsonObject.put("material", wmsTO.getMaterial());
                jsonObject.put("poQty", wmsTO.getPoQty1());
                jsonObject.put("status", wmsTO.getStatus());
                jsonObject.put("poNo", wmsTO.getPoNo());
                jsonObject.put("pendingQty1", wmsTO.getPendingQty1());

                System.out.println("jsonObject af = " + jsonObject);
                jsonArray.put(jsonObject);
                System.out.println(wmsTO.getTotalQty() + "===================================  >>>>>>> ()))");

            }

            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView crateInventoryView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        wmsCommand = command;
        String menuPath = "Crate  >> Crate Inventory View Update ";
        int updateFreight = 0;
        int userId = (Integer) session.getAttribute("userId");

        try {
            String whId = (String) session.getAttribute("hubId");
            String warehouseId = request.getParameter("whId");
            if (warehouseId == null || "".equals(warehouseId)) {
                warehouseId = whId;
            }
            String status = request.getParameter("statusType");
            wmsTO.setStatus(status);
            wmsTO.setWhId(warehouseId);
            request.setAttribute("whId", warehouseId);
            request.setAttribute("statusType", status);
            String param = request.getParameter("param");
            System.out.println("parsm===" + param);

            path = "content/wms/crateInventoryView.jsp";
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
            }
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            ArrayList whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);

            ArrayList getCrateDetails = new ArrayList();
            getCrateDetails = wmsBP.getCrateDetails(wmsTO);
            request.setAttribute("getCrateDetails", getCrateDetails);

            path = "content/wms/crateInventoryView.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView imPodApproval(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        int RoleId = (Integer) session.getAttribute("RoleId");
        try {
            wmsTO.setRoleId(RoleId + "");
            wmsTO.setUserId(userId + "");
            String param = request.getParameter("param");
            String whId = request.getParameter("whId");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            } else {
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            } else {
                wmsTO.setToDate(toDate);
            }
            if ("".equals(whId) || whId == null) {
                whId = hubId;
            }
            wmsTO.setWhId(whId);
            ArrayList whList = new ArrayList();
            whList = wmsBP.getWareHouse(wmsTO);
            request.setAttribute("whList", whList);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("whId", whId);
            System.out.println("fromDate == " + fromDate);
            if ("update".equals(param)) {

                String podId = request.getParameter("podId");
                String status = request.getParameter("status");

                int updateImPodApproval = wmsBP.updateImPodApproval(podId, status, wmsTO);
                
                if (updateImPodApproval > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " status Uploaded Successfully.");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "status Upload Failed");

                }
            }else{
                
                String status = request.getParameter("podstatus");
                System.out.println("status-----------"+status);
                wmsTO.setStatus(status);
                request.setAttribute("podstatus", status);
                
            }

            ArrayList getimPodList = wmsBP.getimPodList(wmsTO);
            request.setAttribute("getPodList", getimPodList);

          
            path = "content/wms/imPodApprovalView.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView imIndentPO(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            if ("save".equals(param)) {
//                String custId = request.getParameter("custId");
                String poDate = request.getParameter("inDate");
                String expectedDeliveryDate = request.getParameter("deliveryDate");
                String totQty = request.getParameter("totQty");
                String remarks = request.getParameter("remarks");
                String[] materialId = request.getParameterValues("materialIdTemp");
                String[] materialName = request.getParameterValues("materialName");
                String[] materialDescription = request.getParameterValues("materialDescription");
                String[] uom = request.getParameterValues("uom");
                String[] poQty = request.getParameterValues("inQty");
//                wms.setCustId(custId);
                wms.setPoDate(poDate);
                wms.setDeliveryDate(expectedDeliveryDate);
                wms.setRemarks(remarks);
                wms.setTotQty(totQty);
                wms.setItemIds(materialId);
//                int insertImPoOrder = wmsBP.insertImPoOrder(wms, materialName, materialDescription, poQty);
                int insertImIndentOrder = wmsBP.insertImIndentOrder(wms, materialName, materialDescription, uom, poQty);

                if (insertImIndentOrder > 0) {
                    int instaSendMail = wmsBP.instaSendMail(wms, userId, hubId);

                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Indent Updated Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Indent Update Failed");
                }
            }
            ArrayList imMaterialMasterDetails = new ArrayList();
            imMaterialMasterDetails = wmsBP.imMaterialMasterDetails(wms);
            request.setAttribute("materialMaster", imMaterialMasterDetails);
            wms.setWhId(hubId);
            ArrayList imMaterialCustomerMaster = new ArrayList();
            imMaterialCustomerMaster = wmsBP.imMaterialCustomerMaster(wms);
            request.setAttribute("customerMaster", imMaterialCustomerMaster);
            path = "content/wms/imIndentPO.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView towerLrDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        wmsCommand = command;
        int insertStatus = 0;
        int status1 = 0;
        try {
            WmsTO wms = new WmsTO();
            int userId = (Integer) session.getAttribute("userId");
            wms.setUserId(userId + "");
            String whId = (String) session.getAttribute("hubId");
            wms.setUserId(whId);
            String param = request.getParameter("param");
            request.setAttribute("d", "Day");

            if ("export".equals(param)) {

                path = "content/wms/dzInvoiceDetailsViewExport.jsp";
            } else if ("search".equals(param)) {
                request.setAttribute("d", "Day");

                String hubId = request.getParameter("whId");
                if ("".equals(hubId) || hubId == null) {
                    hubId = whId;
                }

                String dayData = request.getParameter("dayData");

                wms.setWhId(hubId);
                wms.setDate(dayData);
                request.setAttribute("whId", hubId);
                wms.setUserId(userId + "");

                path = "content/wms/towerLrDetails.jsp";
            } else if ("search1".equals(param)) {
                request.setAttribute("d", "Range");

                String hubId = request.getParameter("whId1");
                if ("".equals(hubId) || hubId == null) {
                    hubId = whId;
                }

                String fromDate = request.getParameter("fromThisDay");
                String toDate = request.getParameter("toThisDay");
                wms.setFromDate(fromDate);
                wms.setToDate(toDate);
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);

                wms.setWhId(hubId);
                request.setAttribute("whId", hubId);
                wms.setUserId(userId + "");

                path = "content/wms/towerLrDetails.jsp";
            } else {
                path = "content/wms/towerLrDetails.jsp";
            }

            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseListForTR(wms);
            System.out.println("getWareHouseList ----------------------------------->>>>>>>>>>> " + getWareHouseList.size());
            request.setAttribute("getWareHouseList", getWareHouseList);

            ArrayList towerLRDetails = new ArrayList();
            towerLRDetails = wmsBP.towerLRDetails(wms);
            request.setAttribute("towerLRDetails", towerLRDetails);

            ArrayList towerLRDetails2 = new ArrayList();
            towerLRDetails2 = wmsBP.towerLRDetails2(wms);
            request.setAttribute("towerLRDetails2", towerLRDetails2);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);

    }

    public ModelAndView dzPodUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        wmsCommand = command;
        String menuPath = "Dunzo  >> POD Upload ";
        String fromDate = "", toDate = "";
        int updateFreight = 0;
        Part partObj = null;
        boolean isMultipart = false;
        try {
            String whId = (String) session.getAttribute("hubId");
            String param = request.getParameter("param");
            String podStatus = request.getParameter("statusType");
            wmsTO.setPodStatus(podStatus);
            wmsTO.setWhId(whId);
//            String hubId = request.getParameter("whId");
//            wmsTO.setWarehouseId(hubId);
           
            System.out.println("param===" + param);

            path = "content/wms/dzPodUpload.jsp";
            if (wmsCommand.getFromDate() != null && wmsCommand.getFromDate() != "") {
                wmsTO.setFromDate(wmsCommand.getFromDate());
                fromDate = wmsCommand.getFromDate();
            }

            if (wmsCommand.getToDate() != null && wmsCommand.getToDate() != "") {
                wmsTO.setToDate(wmsCommand.getToDate());
                toDate = wmsCommand.getToDate();
            }

            String statusType = request.getParameter("statusType");
            wmsTO.setStatusType(statusType);

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }
//            wmsTO.setStatusType("0");
            ArrayList whList = new ArrayList();
            whList = wmsBP.getWareHouse(wmsTO);
            request.setAttribute("whList", whList);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            ArrayList dzPodUploadDetails = new ArrayList();
            dzPodUploadDetails = wmsBP.dzPodUploadDetails(wmsTO);
            request.setAttribute("dzPodUploadDetails", dzPodUploadDetails);

            if ("save".equals(param)) {
                String deliveryDate = request.getParameter("deliveryDate");
                String lrNo = request.getParameter("lrNo");
                wmsTO.setDeliveryDate(deliveryDate);
                wmsTO.setLrnNo(lrNo);
//                int updateLrNo = wmsBP.updateConnectLrNo(wmsTO);
            } else if ("ExportExcel".equals(param)) {
                path = "content/wms/dzPodUploadExcel.jsp";
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView dzUploadPage(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        OpsTO opsTO = new OpsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        try {
            wmsTO.setWhId(hubId);
            int updateconnectInvoice = 0;
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            String param = request.getParameter("param");
            String subLrId = request.getParameter("subLrId");
            request.setAttribute("subLrId", subLrId);
            String dispatchid = request.getParameter("dispatchid");
            request.setAttribute("dispatchid", dispatchid);
            String lrnNo = request.getParameter("lrnNo");
            wmsTO.setLrnNo(lrnNo);
            
            String deliveryDate = wmsBP.getDzDeliveryDate(wmsTO);
            request.setAttribute("deliveryDate", deliveryDate);
            
            request.setAttribute("lrnNo", lrnNo);
            String dispatchDetailId = request.getParameter("dispatchDetailId");
            request.setAttribute("dispatchDetailId", dispatchDetailId);
            String lrdate = request.getParameter("lrdate");
            request.setAttribute("lrdate", lrdate);
            String invoiceNo = request.getParameter("invoiceNo");
            request.setAttribute("invoiceNo", invoiceNo);
            String invoiceDate = request.getParameter("invoiceDate");
            request.setAttribute("invoiceDate", invoiceDate);

            String CustomerName = request.getParameter("CustomerName");
            request.setAttribute("CustomerName", CustomerName);

            path = "content/wms/dzUploadPage.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleDzPODupload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        wmsCommand = command;
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        path = "content/vendor/manageVendor.jsp";
        String pageTitle = "Add Vendor";
        request.setAttribute("pageTitle", pageTitle);
        int vendorId = 0;

        String menuPath = "Vendor  >>  ManageVendor  >> Add";

        //file upload
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;
        int m = 0;
        int n = 0;
        int p = 0;
        int s = 0;
        String tripSheetId1 = "";
        String subLrId1 = "";
        String dispatchid1 = "";
        String lrnNo1 = "";
        String plannedTime1 = "";
        String vendorName1 = "";
        String dispatchDetailId1 = "";
        String plannedDate1 = "";
        String tinNo1 = "";

        //        String[] podRemarks1 = new String[10];
        String[] fileSaved = new String[10];
        String[] remarks = new String[10];
        //        String[] lrNumber1 = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        String[] city = new String[10];

        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            WmsTO wmsTO = new WmsTO();

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
//                System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
//                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/dunzo/");
//                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
//                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        Date now = new Date();
                        String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                        String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                        fPart = (FilePart) partObj;
                        uploadedFileName[j] = fPart.getFileName();
//                        System.out.println("fPart.getFileName() = " + fPart.getFileName());

                        if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {
//                            System.out.println("partObj.getName() = " + partObj.getName());
                            String[] splitFileName = uploadedFileName[j].split("\\.");
                            fileSavedAs = uploadedFileName[j];
//                            System.out.println("fileSavedAs = " + fileSavedAs);
                            fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath[j] = tempServerFilePath + "/" + fileSaved[j];
                            actualFilePath = actualServerFilePath + "/" + tempFilePath;
//                            System.out.println("tempPath..." + tempFilePath);
//                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
//                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
//                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath[j]));
//                            System.out.println("tempPath = " + tempFilePath);
//                            System.out.println("actPath = " + actualFilePath);
                            //                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            //                            String part1 = parts.replace("\\", "");
                        }

//                        System.out.println("fileName..." + fileName);
                        String ss = request.getParameter("subLrId");
                        wmsTO.setSubLr(ss);
                        System.out.println("========================>>>>>>>>" + ss);
                        j++;
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("subLrId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            subLrId1 = paramPart.getStringValue();
                            System.out.println("[][][][][]]][[[[][]" + subLrId1);
                        }
                        if (partObj.getName().equals("dispatchid")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            dispatchid1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("lrnNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            lrnNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("plannedTime")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            plannedTime1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("deliveryDate")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            plannedDate1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("dispatchDetailId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            dispatchDetailId1 = paramPart.getStringValue();
                        }

                    }
                }
            }
            String whId = (String) session.getAttribute("hubId");
            wmsTO.setWhId(whId);
            int index = j;
            String[] file = new String[index];
            System.out.println(" " + file);

            String[] saveFile = new String[index];;

            int status = 0;

            //file upload
            String podName1 = request.getParameter("podName1");
            wmsTO.setPodName1(podName1);
            String podName2 = request.getParameter("podName2");
            wmsTO.setPodName2(podName2);
            String podName3 = request.getParameter("podName3");
            wmsTO.setPodName3(podName3);
            String remarks1 = request.getParameter("remarks1");
            wmsTO.setRemarks1(remarks1);
            String remarks2 = request.getParameter("remarks2");
            wmsTO.setRemarks2(remarks2);
            String remarks3 = request.getParameter("remarks3");
            wmsTO.setRemarks3(remarks3);
            wmsTO.setLrnNo(lrnNo1);
            wmsTO.setDispatchDetailId(dispatchDetailId1);
            wmsTO.setPlannedDate(plannedDate1);
            wmsTO.setDeliveryDate(plannedDate1);
            wmsTO.setPlannedTime(plannedTime1);
            wmsTO.setDispatchid(dispatchid1);
            wmsTO.setSubLrId(subLrId1);

            System.out.println(subLrId1 + "");
            //  String[] mfrids = vendorCommand.getMfrids();
            //  String[] mfrids = vendorCommand.getMfrids();
            //  System.out.println("mfrids222"+mfrids);
            //  String[] selectedIndex = vendorCommand.getSelectedIndex();
            String selectedIndex = "0";
            //  System.out.println("selectedIndexpp"+selectedIndex);

            System.out.println("j = " + j);
            System.out.println("file = " + file.length);
            System.out.println("tempFilePath[0] = " + tempFilePath[0]);
            System.out.println("tempFilePath[1] = " + tempFilePath[1]);

            for (int x = 0; x < j; x++) {
                System.out.println("tempFilePath[x] = " + tempFilePath[x]);
                file[x] = tempFilePath[x];
                System.out.println("saveFile[x] = " + fileSaved[x]);
                saveFile[x] = fileSaved[x];
            }
            vendorId = wmsBP.processInsertImPODDetails(wmsTO, selectedIndex, userId, file, saveFile, remarks);
//            int updateConnectLrNo = wmsBP.updateConnectLrNo(wmsTO);
            request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));
            mv = imPodUpload(request, response, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView dzPodApproval(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        int RoleId = (Integer) session.getAttribute("RoleId");
        try {
            wmsTO.setRoleId(RoleId + "");
            wmsTO.setUserId(userId + "");
            String param = request.getParameter("param");
            String whId = request.getParameter("whId");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            } else {
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            } else {
                wmsTO.setToDate(toDate);
            }
            if ("".equals(whId) || whId == null) {
                whId = hubId;
            }
            wmsTO.setWhId(whId);
            ArrayList whList = new ArrayList();
            whList = wmsBP.getWareHouse(wmsTO);
            request.setAttribute("whList", whList);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("whId", whId);
            System.out.println("fromDate == " + fromDate);
            if ("update".equals(param)) {

                String podId = request.getParameter("podId");
                String status = request.getParameter("status");
                
               int updateDzPodApproval = wmsBP.updateDzPodApproval(podId, status, wmsTO);
                
                if (updateDzPodApproval > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " status Uploaded Successfully.");
                } else {
                   request.setAttribute(ParveenErrorConstants.ERROR_KEY, "status Upload Failed");

                }
            }else{
                
                String status = request.getParameter("podstatus");
                System.out.println("status-----------"+status);
                wmsTO.setStatus(status);
                request.setAttribute("podstatus", status);
                
            }

            ArrayList getimPodList = wmsBP.getDzPodList(wmsTO);
            request.setAttribute("getPodList", getimPodList);

            path = "content/wms/dzPodApprovalView.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView imIndentView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        WmsTO wmsTO = new WmsTO();
        wmsCommand = command;
        String menuPath = "Crate  >> Crate Inventory View Update ";
        int updateFreight = 0;
        int userId = (Integer) session.getAttribute("userId");

        try {
            String whId = (String) session.getAttribute("hubId");
            String warehouseId = request.getParameter("whId");
            if (warehouseId == null || "".equals(warehouseId)) {
                warehouseId = whId;
            }
            String status = request.getParameter("statusType");
            wmsTO.setStatus(status);
            wmsTO.setWhId(warehouseId);
            request.setAttribute("whId", warehouseId);
            request.setAttribute("statusType", status);
            String param = request.getParameter("param");
            System.out.println("parsm===" + param);

            path = "content/wms/imIndentView.jsp";
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
            }
            wmsTO.setFromDate(fromDate);
            wmsTO.setToDate(toDate);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            ArrayList whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);

            ArrayList getIndentView = new ArrayList();
            getIndentView = wmsBP.getIndentView(wmsTO);
            request.setAttribute("getIndentView", getIndentView);

            path = "content/wms/imIndentView.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView dashboardPage(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
//        System.out.println("productMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";

        WmsTO wms = new WmsTO();
        try {

            path = "content/wms/wheelocityDashboard.jsp";
        } catch (FPRuntimeException exception) {

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView employeeHiringDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        wmsCommand = command;
        int insertStatus = 0;
        int status1 = 0;
        try {
            WmsTO wms = new WmsTO();
            int userId = (Integer) session.getAttribute("userId");
            wms.setUserId(userId + "");
            String whId = (String) session.getAttribute("hubId");
            wms.setUserId(whId);
            String param = request.getParameter("param");
            request.setAttribute("d", "Day");
            if ("save".equals(param)) {
                System.out.println("hiiiiiiiiiiiiiiiiiiiii" + userId);
//                int insert = wmsBP.insertDzGrnUpload(wms);
                int insert = wmsBP.insertDzHireUpload(wms);
                if (insert > 0) {
                    request.setAttribute("msg", 1);
                } else {
                    request.setAttribute("msg", 2);
                }
                path = "content/wms/employeeHireDetails.jsp";
            } else {
                int delEmpHireTemp = wmsBP.delEmpHireTemp(wms);
                request.setAttribute("msg", 0);
                path = "content/wms/employeeHireDetails.jsp";
            }

            ArrayList employeeHireDetailsTemp = wmsBP.employeeHireDetailsTemp(wms);
            request.setAttribute("employeeHireDetailsTemp", employeeHireDetailsTemp);
            request.setAttribute("employeeHireDetailsTemps", employeeHireDetailsTemp.size());
            System.out.println(employeeHireDetailsTemp.size() + "]]]]]]]]]]]]]]]]]]]]]]]]]]]][[[[[[[[");

        } catch (FPRuntimeException exception) {

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView empHiringDetailsUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = (Integer) session.getAttribute("userId");
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        String productVariantId = "";
        String param = "";

        String empName = "";
        String empId = "";
        String ctc = "";
        String designation = "";
        String client = "";
        String location = "";
        String DOJ = "";
        String DOL = "";
        String type = "";
        String category = "";
        String status = "";

        String whId = (String) session.getAttribute("hubId");

        int insertStatus = 0;

        int status1 = 0;

        param = request.getParameter("param");

        try {

            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

            if (isMultipart) {

                userId = (Integer) session.getAttribute("userId");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadedxls/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        int len = splitFileNames.length;
                        fileFormat = splitFileNames[len - 1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat) || "xlsx".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = uploadedFileName;
                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                File f1 = new File(actualFilePath);
                                f1.renameTo(new File(tempFilePath));
//                               
                                if ("xlsx".equals(fileFormat)) {
                                    FileInputStream fis = new FileInputStream(tempFilePath);
                                    XSSFWorkbook wb = new XSSFWorkbook(fis);
                                    XSSFSheet sheet = wb.getSheetAt(0);
                                    int value = sheet.getLastRowNum();

                                    int sno = 0;
                                    Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
                                    while (itr.hasNext()) {
                                        Row row = itr.next();
                                        if (sno > 0) {
//                                                System.out.println("print the all the date from the xlxs or xls " + row.getCell(0) + row.getCell(1) + row.getCell(2) + row.getCell(3) + row.getCell(4) + row.getCell(5) + row.getCell(6) + row.getCell(7));
                                            WmsTO wmsTo = new WmsTO();

                                            if (row.getCell(0) != null && !"".equals(row.getCell(0))) {
                                                row.getCell(0).setCellType(row.getCell(0).CELL_TYPE_STRING);
                                                empName = row.getCell(0).getStringCellValue();
                                                wmsTo.setName(empName);

                                            }

                                            if (row.getCell(1) != null && !"".equals(row.getCell(1))) {
                                                row.getCell(1).setCellType(row.getCell(1).CELL_TYPE_STRING);
                                                empId = row.getCell(1).getStringCellValue();
                                                wmsTo.setId(empId);

                                            }

                                            if (row.getCell(2) != null && !"".equals(row.getCell(2))) {
                                                row.getCell(2).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                ctc = row.getCell(2).toString();
                                                wmsTo.setCtc(ctc);

                                            }

                                            if (row.getCell(3) != null && !"".equals(row.getCell(3))) {
                                                row.getCell(3).setCellType(row.getCell(2).CELL_TYPE_STRING);
                                                designation = row.getCell(3).toString();
                                                wmsTo.setDesignation(designation);

                                            }

                                            if (row.getCell(4) != null && !"".equals(row.getCell(4))) {
                                                row.getCell(4).setCellType(row.getCell(4).CELL_TYPE_STRING);
                                                client = row.getCell(4).getStringCellValue();
                                                wmsTo.setClient(client);
                                            }

                                            if (row.getCell(5) != null && !"".equals(row.getCell(5))) {
                                                row.getCell(5).setCellType(row.getCell(5).CELL_TYPE_STRING);
                                                location = row.getCell(5) + "";
                                                wmsTo.setLocation(location);
//                                                    System.out.println(orderedQty + "  = testin orderedQty's value");
                                            }

                                            if (row.getCell(6) != null && !"".equals(row.getCell(6))) {
                                                row.getCell(6).setCellType(row.getCell(6).CELL_TYPE_STRING);
                                                DOJ = row.getCell(6).toString();
                                                wmsTo.setDate(DOJ);
                                            }

                                            if (row.getCell(7) != null && !"".equals(row.getCell(7))) {
                                                row.getCell(7).setCellType(row.getCell(7).CELL_TYPE_STRING);
                                                DOL = row.getCell(7).toString();
                                                wmsTo.setLeavingDate(DOL);
                                            }

                                            if (row.getCell(8) != null && !"".equals(row.getCell(8))) {
                                                row.getCell(8).setCellType(row.getCell(8).CELL_TYPE_STRING);
                                                type = row.getCell(8).getStringCellValue();
                                                wmsTo.setType(type);
                                            }

                                            if (row.getCell(9) != null && !"".equals(row.getCell(9))) {
                                                row.getCell(9).setCellType(row.getCell(9).CELL_TYPE_STRING);
                                                category = row.getCell(9).getStringCellValue();
                                                wmsTo.setCategory(category);
                                            }

                                            wmsTo.setStatus(status);
                                            wmsTo.setUserId(userId + "");
                                            wmsTo.setWhId(whId);

                                            status1 = wmsBP.empHireUpload(wmsTo);
                                        }
                                        sno++;

                                    }
                                } else if ("xls".equals(fileFormat)) {
                                    int count = 0;

                                    WorkbookSettings ws = new WorkbookSettings();
                                    ws.setLocale(new Locale("en", "EN"));
                                    Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                    Sheet s = workbook.getSheet(0);

                                    for (int i = 1; i < s.getRows(); i++) {
                                        WmsTO wmsTo = new WmsTO();
                                        count++;

                                        empName = s.getCell(0, i).getContents();
                                        wmsTo.setName(empName);

                                        empId = s.getCell(1, i).getContents();
                                        wmsTo.setId(empId);

                                        ctc = s.getCell(2, i).getContents();
                                        wmsTo.setCtc(ctc);

                                        designation = s.getCell(3, i).getContents();
                                        wmsTo.setDesignation(designation);

                                        client = s.getCell(4, i).getContents();
                                        wmsTo.setClient(client);

                                        location = s.getCell(5, i).getContents();
                                        wmsTo.setLocation(location);

                                        DOJ = s.getCell(6, i).getContents();
                                        wmsTo.setDate(DOJ);

                                        DOL = s.getCell(7, i).getContents();
                                        wmsTo.setLeavingDate(DOL);

                                        type = s.getCell(8, i).getContents();
                                        wmsTo.setType(type);

                                        category = s.getCell(9, i).getContents();
                                        wmsTo.setCategory(category);

                                        wmsTo.setStatus(status);
                                        wmsTo.setUserId(userId + "");
                                        wmsTo.setWhId(whId);

                                        status1 = wmsBP.empHireUpload(wmsTo);
                                    }

                                }
                            }
                        } else {
                            request.setAttribute("erruserIdorMessage", " Upload updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            wms.setUserId(userId + "");

            ArrayList employeeHireDetailsTemp = wmsBP.employeeHireDetailsTemp(wms);
            request.setAttribute("employeeHireDetailsTemp", employeeHireDetailsTemp);
            request.setAttribute("employeeHireDetailsTemps", employeeHireDetailsTemp.size());

            path = "content/wms/employeeHireDetails.jsp";

            if (employeeHireDetailsTemp.size() > 0) {
                request.setAttribute("msg", 1);
            } else {
                request.setAttribute("msg", 2);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);

    }

    public ModelAndView viewCycleCountReport(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        String menuPath = "";
        String path = "";
        WmsTO wmsTO = new WmsTO();
        String fromDate = "", toDate = "";
        wmsCommand = command;
        int insertStatus = 0;
        int status1 = 0;
        try {
            String param = request.getParameter("param");
            WmsTO wms = new WmsTO();
            int userId = (Integer) session.getAttribute("userId");
            System.out.println("userId---------" + userId);
            wms.setUserId(userId + "");
            String whId = (String) session.getAttribute("hubId");
            System.out.println("whId---------" + whId);
            wms.setWhId(whId);

            if (request.getParameter("fromDate") != null && request.getParameter("fromDate") != "") {
                System.out.println("veg" + request.getParameter("fromDate"));
                wmsTO.setFromDate(wmsCommand.getFromDate());
                fromDate = wmsCommand.getFromDate();
            }
            if (request.getParameter("toDate") != null && request.getParameter("toDate") != "") {
                wmsTO.setToDate(wmsCommand.getToDate());
                toDate = wmsCommand.getToDate();
            }

            System.out.println("fromDate-------" + fromDate);
            System.out.println("toDate-------" + toDate);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            System.out.println("fromDate-------" + fromDate);
            System.out.println("toDate-------" + toDate);

            if ("view".equals(param)) {
                fromDate = request.getParameter("fromDate");
                toDate = request.getParameter("toDate");
                System.out.println("fromDate-------" + fromDate);
                System.out.println("toDate-------" + toDate);

                path = "content/wms/viewCycleCountReport.jsp";
            } else if ("Completed Cycle Count Excel".equals(param)) {
                path = "content/wms/viewCycleCountReportExcelTemp.jsp";
            } else if ("Ongoing Cycle Count Excel".equals(param)) {
                path = "content/wms/viewCycleCountReportExcel.jsp";
            } else {
                path = "content/wms/viewCycleCountReport.jsp";
            }

            ArrayList getCrateDetailsReport = wmsBP.getCrateDetailsReport(wms, fromDate, toDate);
            request.setAttribute("getCrateDetailsReport", getCrateDetailsReport);
            request.setAttribute("getCrateDetailsReportsize", getCrateDetailsReport.size());
            System.out.println(getCrateDetailsReport.size() + "getCrateDetailsReport");

            ArrayList getCrateDetailsReportPer = wmsBP.getCrateDetailsReportPer(wms, fromDate, toDate);
            request.setAttribute("getCrateDetailsReportPer", getCrateDetailsReportPer);
            request.setAttribute("getCrateDetailsReportPersize", getCrateDetailsReportPer.size());
            System.out.println(getCrateDetailsReportPer.size() + "getCrateDetailsReportPer");

        } catch (FPRuntimeException exception) {

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView viewStaffAttendanceReport(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Report >>  Hub Report";
        wmsCommand = command;
        WmsTO wms = new WmsTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        int userId = (Integer) session.getAttribute("userId");
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            request.setAttribute("pageTitle", pageTitle);
            String param = request.getParameter("param");
//
//              String fromDate = request.getParameter("fromDate");
//                String toDate = request.getParameter("toDate");
//                wms.setFromDate(fromDate);
//                wms.setToDate(toDate);

            ArrayList AddwhList = new ArrayList();
            AddwhList = wmsBP.AddwhList();

            request.setAttribute("AddwhList", AddwhList);

            if ("search".equals(param)) {

                String whId = (String) session.getAttribute("hubId");
                String warehouseId = request.getParameter("whId");
                if (warehouseId == null || "".equals(warehouseId)) {
                    warehouseId = whId;
                }
                wms.setWhId(warehouseId);
                request.setAttribute("whId", warehouseId);
//                request.setAttribute("hubId", warehouseId);

                String fromDate = request.getParameter("fromDate");
                String toDate = request.getParameter("toDate");

                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date curDate = new Date();
                System.out.println(dateFormat.format(curDate));
                String endDate = dateFormat.format(curDate);
                String startDate = "";
                String[] temp = null;
                if (!"".equals(endDate)) {
                    temp = endDate.split("-");
                    startDate = "01" + "-" + temp[1] + "-" + temp[2];
                }
                if ("".equals(fromDate) || fromDate == null) {
                    fromDate = startDate;
                }
                if ("".equals(toDate) || toDate == null) {
                    toDate = endDate;
                }
                wms.setFromDate(fromDate);
                wms.setToDate(toDate);

                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);

                ArrayList staffAttendanceReport = new ArrayList();
                staffAttendanceReport = wmsBP.staffAttendanceReport(wms);
                request.setAttribute("staffAttendanceReport", staffAttendanceReport);

                path = "content/wms/staffAttendanceReport.jsp";
            } else {
                String whId = request.getParameter("whId");
                wms.setWhId(whId);
                request.setAttribute("hubId", whId);
                ArrayList staffAttendanceReport = new ArrayList();
                staffAttendanceReport = wmsBP.staffAttendanceReport(wms);
                request.setAttribute("staffAttendanceReport", staffAttendanceReport);
                System.out.println("staffAttendanceReport=====" + staffAttendanceReport.size());
                path = "content/wms/staffAttendanceReportExcel.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to get Hub data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView materialReturn(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            if ("save".equals(param)) {
                String returnDate = request.getParameter("returnDate");
                String returnTime = request.getParameter("returnTime");

                String totalQty = request.getParameter("totQty");
                String remarks = request.getParameter("remarks");
                String receivedBy = request.getParameter("receivedBy");

                String[] materialId = request.getParameterValues("materialId");
                String[] materialName = request.getParameterValues("materialName");
                String[] skuCode = request.getParameterValues("skuCode");
                String[] uom = request.getParameterValues("uom");
                String[] stockQty = request.getParameterValues("stockQty");
                String[] returnQty = request.getParameterValues("returnQty");

                System.out.println(Arrays.toString(skuCode));

                wms.setReturnDate(returnDate);
                wms.setReturnTime(returnTime);
                wms.setRemarks(remarks);
                wms.setReceivedBy(receivedBy);
                wms.setTotQty(totalQty);
                wms.setItemIds(materialId);

                String returnNo = wmsBP.insertImMaterialGrnReturn(wms, skuCode, uom, stockQty, returnQty);
                if (returnNo != null && returnNo != "") {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Material Return (" + returnNo + ") Updated Successfully");
                    path = "content/wms/materialReturn.jsp";
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Material Return Update Failed");
                    path = "content/wms/materialReturn.jsp";

                }
            } else {
                path = "content/wms/materialReturn.jsp";
            }
            wms.setWhId(hubId);
            wms.setStatus("200");
            ArrayList imMaterialMasterDetails = new ArrayList();
            imMaterialMasterDetails = wmsBP.imMaterialMasterDetails(wms);
            request.setAttribute("materialMaster", imMaterialMasterDetails);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }
    
      public ModelAndView materialReturnView(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        String materialId = request.getParameter("materialId");
        int userId = (Integer) session.getAttribute("userId");
        String hubId = (String) session.getAttribute("hubId");
        WmsTO wms = new WmsTO();
        try {
            String param = request.getParameter("param");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            wms.setUserId(userId + "");
            wms.setWhId(hubId);
            wms.setFromDate(fromDate);
            wms.setToDate(toDate);
            wms.setMaterialId(materialId);

            String returnId = request.getParameter("returnId");
            wms.setReturnId(returnId);
            
          //  System.out.println("returnId++++++++++"+returnId);
            
            if ("print".equals(param)) {
               path = "content/wms/materialReturnPrint.jsp";
           } else if ("view".equals(param)) {
                path = "content/wms/materialReturnViewDetails.jsp";
            } else if ("update".equals(param)) {
                path = "content/wms/materialReturnUpdate.jsp";
            } else if ("save".equals(param)) {
                String[] floorQty = request.getParameterValues("floorQty");
                String[] usedQty = request.getParameterValues("usedQty");
                String[] excess = request.getParameterValues("excess");
                String[] shortage = request.getParameterValues("shortage");
                String[] status = request.getParameterValues("status");
                String[] detailId = request.getParameterValues("detailId");
                String[] materialIds = request.getParameterValues("materialId");
                String[] floorPackQty = request.getParameterValues("floorPackQty");
                int updateGrnReturn = wmsBP.updateImGrnReturn(floorQty, usedQty, excess, shortage, status, detailId, floorPackQty, materialIds, wms, userId);
               // path = "content/wms/materialReturnViewDetails.jsp";
                 path = "content/wms/materialReturnViewDetails.jsp";
            } else {
                path = "content/wms/materialReturnView.jsp";
            }

            ArrayList getImMaterialReturnMaster = wmsBP.getImMaterialReturnMaster(wms);
            request.setAttribute("issueMaster", getImMaterialReturnMaster);
            ArrayList getImMaterialReturnDetails = wmsBP.getImMaterialReturnDetails(wms);
            request.setAttribute("issueDetails", getImMaterialReturnDetails);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    
}
