package ets.cewolf.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import de.laures.cewolf.DatasetProduceException;
import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.links.CategoryItemLinkGenerator;
import de.laures.cewolf.tooltips.CategoryToolTipGenerator;

public class Horizontalbar3DBean implements DatasetProducer, CategoryToolTipGenerator, CategoryItemLinkGenerator, Serializable {

	private static final long serialVersionUID = 1L;
		private static final Log log = LogFactory.getLog(Horizontalbar3DBean.class);
	    String startDate;
	    String endDate;
	    String strIOU;
	    String region;
	    String[] seriesNames = new String[10];
	    /**
		 *  Produces some random data.
		 */

	    public Object produceDataset(Map params) throws DatasetProduceException {
	    	log.debug("producing data.");

	    	  // Dataset can either be populated by hard-coded or else from a datasource
	    	DefaultCategoryDataset dataset = new DefaultCategoryDataset(){
				/**
				 *
				 */
				private static final long serialVersionUID = 1L;

				/**
				 * @see java.lang.Object#finalize()
				 */
				protected void finalize() throws Throwable {
					super.finalize();
					log.debug(this +" finalized.");
				}
	        };
					   seriesNames[0]="C";
					   seriesNames[1]="C++";
					   seriesNames[2]="Java";
					   seriesNames[3]=".NET";
					   seriesNames[4]="Perl";
					   seriesNames[5]="Python";


	        		   dataset.addValue(10, "C","C");
					   dataset.addValue(15, "C++","C++");
					   dataset.addValue(30, "Java","Java");
					   dataset.addValue(15, ".NET",".Net");
					   dataset.addValue(10, "Perl","Perl");
					   dataset.addValue(15, "Python","Python");


			      return dataset;

	    }

	    /**
	     * This producer's data is invalidated after 5 seconds. By this method the
	     * producer can influence Cewolf's caching behaviour the way it wants to.
	     */
		public boolean hasExpired(Map params, Date since) {
	        log.debug(getClass().getName() + "hasExpired()");
			return (System.currentTimeMillis() - since.getTime())  > 5000;
		}

		/**
		 * Returns a unique ID for this DatasetProducer
		 */
		public String getProducerId() {
			return "PageViewCountData DatasetProducer";
		}

	    /**
	     * Returns a link target for a special data item.
	     */
	    public String generateLink(Object data, int series, Object category) {
	        return seriesNames[series];
	    }

		/**
		 * @see java.lang.Object#finalize()
		 */
		protected void finalize() throws Throwable {
			super.finalize();
			log.debug(this + " finalized.");
		}

		/**
		 * @see org.jfree.chart.tooltips.CategoryToolTipGenerator#generateToolTip(CategoryDataset, int, int)
		 */
		public String generateToolTip(CategoryDataset arg0, int series, int arg2) {
			return seriesNames[series];
		}


	}


