
/*---------------------------------------------------------------------------
 * ProcessAttendance.java
 * Jul 28, 2008
 *
 * Copyright (c) ETS.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ETS ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ETS.
 ----------------------------------------------------------------------------*/
/**
 * ****************************************************************************
 *
 * Modification Log:
 * ---------------------------------------------------------------------------
 * Ver Date Author Change
 * ---------------------------------------------------------------------------
 * 1.0 Jul 28, 2008 SriniR	Created
 *
 *************************************************************************
 */
import java.io.*;
import java.util.*;
import java.text.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;

/**
 * ProcessAttendance: The business process class that processes the attendance
 * data.
 *
 * @author Srinivasan.R
 * @version 1.0 28 Jul 2008
 */
public class updateCustomerRank {

    /**
     * retriveJobStatus method used to get the jobs status from the factory and
     * updates in the factory portal.
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static void main(String[] args) {
        System.out.println("Update Records Example...");
        Connection con = null;
        Statement statement = null;
        ResultSet rs = null;
        String url = "jdbc:mysql://203.124.105.244:3306/";
//        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "throttlebrattle";
        String driverName = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "etsadmin";
        try {
            Class.forName(driverName);

// Connecting to the database
            con = DriverManager.getConnection(url + dbName, userName, password);
            try {
                PreparedStatement updateCustomerRank = null;
                PreparedStatement customerRank = null;
                statement = con.createStatement();
                
                
// updating records
                String sqlUpdate = "update ts_customer_outstanding set Customer_Rank = 0";
                statement.executeUpdate(sqlUpdate);
                String sql = "update ts_customer_outstanding set Customer_Rank = ? where customer_Id = ? ";
                System.out.println("Updated successfully");
                updateCustomerRank = con.prepareStatement(sql);
                    String sql1 = " select "
                    +"  a.customerId,"
                    +"  ifnull(ROUND(SUM(ifnull(estimated_revenue,0)),2),0) as freightAmount, "
                    +"  ifnull(ROUND(SUM(ifnull(total_expenses,0)),2),0) as total_expenses, "
                    +"  ifnull(ROUND(SUM(ifnull(estimated_revenue,0)) - SUM(ifnull(total_expenses,0)),2),0) as profit, "
                    +"  ifnull(ROUND((ROUND(SUM(ifnull(estimated_revenue,0)) - SUM(ifnull(total_expenses,0)),2) / SUM(ifnull(estimated_revenue,0))) * 100,2),0) as profitPercent "
                    +"  from "
                    +"  ts_trip_master a left join ts_trip_closure_details b "
                    +"  on(a.trip_id = b.trip_id) "
                    +"  WHERE a.trip_Id in(SELECT trip_Id FROM ts_trip_closure_details) "
                    + " and a.customerId != 1040 "
                    +"  and a.trip_actual_end_date  BETWEEN STR_TO_DATE('01-08-2014','%d-%m-%Y') AND STR_TO_DATE('31-08-2014','%d-%m-%Y') "
                    +"  GROUP BY a.customerId order by profitPercent desc ";
                rs = statement.executeQuery(sql1);
                System.out.println("----------------------");
                int i=1;
                int updateStatus = 0;
                double oldPRofitPercent = 0.0;
                while (rs.next()) {
                    int customerId = rs.getInt(1);
                    double customerProfit = rs.getDouble(5);
                    System.out.println("customerId = " + customerId);
                    System.out.println("customerProfit = " + customerProfit);
                    System.out.println("customerRank = " + i);

                    updateCustomerRank.setInt(2,customerId);
                    updateCustomerRank.setInt(1,i);
                    updateStatus = updateCustomerRank.executeUpdate();
                    System.out.println("updateStatus = " + updateStatus);
                    if(oldPRofitPercent != customerProfit){
                    oldPRofitPercent = customerProfit;
                    i++;
                    }
                }

            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Table doesn't exist.");
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
