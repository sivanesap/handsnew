
    /*---------------------------------------------------------------------------
     * ProcessAttendance.java
     * Jul 28, 2008
     *
     * Copyright (c) ETS.
     * All Rights Reserved.
     *
     * This software is the confidential and proprietary information of
     * ETS ("Confidential Information"). You shall
     * not disclose such Confidential Information and shall use it only in
     * accordance with the terms of the license agreement you entered into
     * with ETS.
     ----------------------------------------------------------------------------*/
    /**
     * ****************************************************************************
     *
     * Modification Log:
     * ---------------------------------------------------------------------------
     * Ver Date Author Change
     * ---------------------------------------------------------------------------
     * 1.0 Jul 28, 2008 SriniR	Created
     *
     *************************************************************************
     */
    import java.io.*;
    import java.util.*;
    import java.text.*;
    import java.lang.*;
import java.math.BigDecimal;
    import java.sql.Connection;
    import java.sql.DriverManager;
    import java.sql.*;

    /**
     * ProcessAttendance: The business process class that processes the attendance
     * data.
     *
     * @author Srinivasan.R
     * @version 1.0 28 Jul 2008
     */
    public class updateRouteMaster {

        /**
         * retriveJobStatus method used to get the jobs status from the factory and
         * updates in the factory portal.
         *
         * @return boolean
         *
         * @throws Exception
         */
        public static void main(String[] args) {
            System.out.println("Update Records Example...");
            Connection con = null;
            Statement statement = null;
            Statement statement1 = null;
            Statement statement2 = null;
            ResultSet rs = null;
            ResultSet rs1 = null;
            ResultSet rs2 = null;
            ResultSet rs3 = null;
            ResultSet rs4 = null;
            String url = "jdbc:mysql://localhost:3307/";
            String dbName = "throttlebrattle";
            String driverName = "com.mysql.jdbc.Driver";
            String userName = "root";
            String password = "mysql";
            try {
                Class.forName(driverName);

    // Connecting to the database
                con = DriverManager.getConnection(url + dbName, userName, password);
                try {
                   // PreparedStatement updateFuelPrice = null;
                    PreparedStatement getVehicleType = null;
                    PreparedStatement getVehicleMillege = null;
                    PreparedStatement getPerKmCost = null;
                    statement = con.createStatement();
                    statement1 = con.createStatement();
                    statement2 = con.createStatement();
    // updating records
                  //  String sql = "UPDATE ts_route_master SET fuel_price=? WHERE route_Id=?";
                    System.out.println("Updated successfully");
                 //   updateFuelPrice = con.prepareStatement(sql1);
                    String sql1 = "SELECT route_Id FROM ts_route_master order by route_Id";
                    String sql2 = "SELECT vehicle_type_Id FROM ts_route_cost_master where route_id=?";
                    String sql3 = "Select milleage from papl_milleage_config where vehicle_type_id=? and active_ind='Y'";
                    String sql4 = "SELECT fuel_price FROM ts_fuel_price where fule_type=1002 and active_ind='Y'";
                    String sql5 = "SELECT fuel_price FROM ts_fuel_price where fule_type=1003 and active_ind='Y'";
                    getVehicleType = con.prepareStatement(sql2);
                    getVehicleMillege = con.prepareStatement(sql3);
                    rs = statement.executeQuery(sql1);
                    System.out.println("----------------------");
                 //   int updateStatus = 0;
                    while (rs.next()) {
                        int routeId = rs.getInt(1);
                        System.out.println("roouteId = " + routeId);

                        getVehicleType.setInt(1,routeId);

                        rs1 = getVehicleType.executeQuery();
                        while(rs1.next()){
                            double costPerKm=0.0d;
                            double costPerKm1=0.0d;
                            double millege=0.00;
                            double fuelPrice=0.0d;
                            DecimalFormat df2 = new DecimalFormat("##.##n");

                            int vehicleTypeId=rs1.getInt(1);
                             System.out.println("VehicleTypeId = " + vehicleTypeId);
                             getVehicleMillege.setInt(1, vehicleTypeId);
                             rs4=getVehicleMillege.executeQuery();
                             if(rs4.next()){
                                 millege =rs4.getDouble(1);
                              System.out.println("Vehicle Millege = " + millege);
                             }
                             if(vehicleTypeId==1021 || vehicleTypeId==1022 || vehicleTypeId==1023 || vehicleTypeId==1024 ){
                             rs2=statement1.executeQuery(sql4);
                              if(rs2.next()){
                             fuelPrice=rs2.getDouble(1);
                              System.out.println("Diseal Price = " + fuelPrice);
                                 }
                             }else{
                             rs3=statement2.executeQuery(sql5);
                             if(rs3.next()){
                             fuelPrice=rs3.getDouble(1);
                              System.out.println("CNG Price =" + fuelPrice);
                                 }
                             }
                             costPerKm=(Double)(fuelPrice/millege);
                            costPerKm1=Math.round(costPerKm*100)/100;
                             System.out.println("costPerKm =" + costPerKm);
                        }
                      //  if(updateStatus>0){
                      //  System.out.println("fuel price updated...:-) " );
                      //  }

                    }
     System.out.println(" Exit..");
                } catch (SQLException e) {
                    e.printStackTrace();
                    System.out.println("Table doesn't exist.");
                }
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
